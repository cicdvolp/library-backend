package library
import grails.converters.JSON
class SuperAdminController {

    def index() { }
    //Process Super Admin Login
    def superadminLogin()
    {
        println "In super admin Login"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        superAdminService.superadminLogin(request,hm)
        render hm as JSON
    }
    //get Super Admin Link
    def getSuperAdminLink()
    {
        println "In super admin Link"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        superAdminService.getSuperAdminLink(request,hm)
        render hm as JSON
    }
    //get Tenant Data
    def getTenantData()
    {
        println "In Tenant Data"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        superAdminService.getTenantData(request,hm)
        render hm as JSON
    }
    //Saev Tenant Data
    def saveTenantData()
    {
        println "In  save Tenant Data"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        superAdminService.saveTenantData(request,hm,params)
        render hm as JSON
    }
    //Saev Tenant Data
    def geteditTenantData()
    {
        println "In  Edit Tenant Data"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        superAdminService.geteditTenantData(request,hm)
        render hm as JSON
    }
    //Super Admin MSters
    def superadminMasterMenu()
    {
        println "get superadminMasterMenu"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.superadminMasterMenu(request, ip ,hm)
        render hm as JSON
    }
    //Role is Active
    def roleIsactive()
    {
        println " roleIsactive"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.roleIsactive(request ,hm)
        render hm as JSON
    }
    //Edit Supermaster Role
    def editRole()
    {
        println " editRole"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.editRole(request ,hm,ip)
        render hm as JSON
    }
    //Get All Role From Super admin
    def getRole()
    {
        println "get getRole"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.getRole(request ,hm)
        render hm as JSON
    }
    //Save Role in Super Admin
    def addRole()
    {
        println " addRole"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.addRole(request ,hm,ip)
        render hm as JSON
    }
    //Edit Super Master UT
    def editAdminUserTypeOperation()
    {
        println " editAdminUserTypeOperation"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.editAdminUserTypeOperation(request ,hm)
        render hm as JSON
    }
    //Add Super Master UT
    def addUserType()
    {
        println " editAdminUserTypeOperation"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.addUserType(request ,hm,ip)
        render hm as JSON
    }
    //Edit Super Master UT
    def editUsertype()
    {
        println " editUsertype"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.editUsertype(request ,hm,ip)
        render hm as JSON
    }
    //Get All UT
    def getMasterUserType()
    {
        println "get adminMasterMenu"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.getMasterUserType(request ,hm)
        render hm as JSON
    }
    //Get All Book Staus
    def getBookStatus()
    {
        println "getBookStatus"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.getBookStatus(request ,hm)
        render hm as JSON
    }
    //SAve Book Status
    def addBookStatus()
    {
        println " addBookStatus"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.addBookStatus(request ,hm,ip)
        render hm as JSON
    }
    //Active in-active Book Status
    def bookStatusIsactive()
    {
        println " bookStatusIsactive"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.bookStatusIsactive(request ,hm)
        render hm as JSON
    }
    //Edit Book Status
    def editBookStatus()
    {
        println " editBookStatus"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.editBookStatus(request ,hm,ip)
        render hm as JSON
    }
    //Get Reservation Status
    def getReservationStatus()
    {
        println "getReservationStatus"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.getReservationStatus(request ,hm)
        render hm as JSON
    }
    //Save Reservation Status
    def addReservationStatus()
    {
        println " addReservationStatus"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.addReservationStatus(request ,hm,ip)
        render hm as JSON
    }
    //Active in-active Reservation Status
    def reservationStatusIsactive()
    {
        println " ReservationStatusIsactive"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.reservationStatusIsactive(request ,hm)
        render hm as JSON
    }
    //Edit Reservation Status
    def editReservationStatus()
    {
        println " editReservationStatus"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.editReservationStatus(request ,hm,ip)
        render hm as JSON
    }
    //Get Account Status
    def getAccountStatus()
    {
        println "getAccountStatus"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.getAccountStatus(request ,hm)
        render hm as JSON
    }
    //Save Account Status
    def addAccountStatus()
    {
        println " addAccountStatus"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.addAccountStatus(request ,hm,ip)
        render hm as JSON
    }
    //Active or In Active Account Status
    def accountStatusIsactive()
    {
        println " AccountStatusIsactive"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.accountStatusIsactive(request ,hm)
        render hm as JSON
    }
    //Edit Account Status
    def editAccountStatus()
    {
        println " editAccountStatus"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.editAccountStatus(request ,hm,ip)
        render hm as JSON
    }
    //Get Transaction Type
    def getTranType()
    {
        println "getTranType"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.getTranType(request ,hm)
        render hm as JSON
    }
    //Add Transaction Type
    def addTranType()
    {
        println " addTranType"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.addTranType(request ,hm,ip)
        render hm as JSON
    }
    //Change Status
    def tranTypeIsactive()
    {
        println " TranTypeIsactive"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.tranTypeIsactive(request ,hm)
        render hm as JSON
    }
    //Edit Transaction Type
    def editTranType()
    {
        println " editTranType"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.editTranType(request ,hm,ip)
        render hm as JSON
    }
    //Get All Notification Method
    def getNotifyMethodData()
    {
        println " getNotifyMethodData"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.getNotifyMethodData(request ,hm,ip)
        render hm as JSON
    }
    //Save Edit Data Notification Method
    def editNotifyMethod()
    {
        println " editNotifyMethod"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.editNotifyMethod(request ,hm,ip)
        render hm as JSON
    }
    //Add New Notification Method
    def saveNotifyMethod()
    {
        println " saveNotifyMethod"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.saveNotifyMethod(request ,hm,ip)
        render hm as JSON
    }
    //Change Status Active in-active Notification Method
    def nMethodIsactive()
    {
        println " nMethodIsactive"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.nMethodIsactive(request ,hm,ip)
        render hm as JSON
    }
    //Get Notification Type
    def getNotifyType()
    {
        println "getNotifyType"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.getNotifyType(request ,hm)
        render hm as JSON
    }
    //Add new Notification Type
    def addNotifyType()
    {
        println " addNotifyType"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.addNotifyType(request ,hm,ip)
        render hm as JSON
    }
    // Change Status Notification Type
    def notifyTypeIsactive()
    {
        println " NotifyTypeIsactive"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.notifyTypeIsactive(request ,hm)
        render hm as JSON
    }
    //Edit Notification Type
    def editNotifyType()
    {
        println " editNotifyType"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.editNotifyType(request ,hm,ip)
        render hm as JSON
    }
    //Get Library Configuration
    def getLibConfig()
    {
        println "get getLibConfig"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.getLibConfig(request ,hm)
        render hm as JSON
    }
    //Add New Library Configuration
    def addLibConfig()
    {
        println " addLibConfig"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.addLibConfig(request ,hm,ip)
        render hm as JSON
    }
    //Active or In Active Library Configuration
    def LibConfigIsactive()
    {
        println " LibConfigIsactive"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.LibConfigIsactive(request ,hm)
        render hm as JSON
    }
    //Add Edit Data Library Configuration
    def editLibConfig()
    {
        println " editLibConfig"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.editLibConfig(request ,hm,ip)
        render hm as JSON
    }
    //Get Role Link Data
    def adminAddRoleLink()
    {
        println " adminAddRoleLink"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.adminAddRoleLink(request ,hm,ip)
        render hm as JSON
    }
    //Edit Role Link Data
    def editAdminAddRoleLink()
    {
        println " editAdminAddRoleLink"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.editAdminAddRoleLink(request ,hm,ip)
        render hm as JSON
    }
    //Save Link Data
    def saveAdminAddRoleLink()
    {
        println " saveAdminAddRoleLink"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.saveAdminAddRoleLink(request ,hm,ip)
        render hm as JSON
    }
    //Active in-active Link
    def roleLinksIsactive()
    {
        println " roleLinksIsactive"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.roleLinksIsactive(request ,hm)
        render hm as JSON
    }
    //Check New Username
    def checkUsername()
    {
        println "checkUsername"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        superAdminService.findusername(request, hm)
        render hm as JSON
    }
    //Get Login Data
    def fetchEmployeeType()
    {
        println "fetchEmployeeType"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        superAdminService.findusertype(request, hm)
        render hm as JSON
    }
    //Save Userdata
    def saveUsername()
    {
        println "Save Username"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.saveUsername(request, ip ,hm)
        render hm as JSON
    }
    //Edit Login Details
    def updateUtype()
    {
        println "update usertype and registration number / employee code"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.updateUtype(request, ip ,hm)
        render hm as JSON
    }
    //Get All Org Data
    def getOrgData()
    {
        println " getOrgData"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.getOrgData(request ,hm,ip)
        render hm as JSON
    }
    //Edit Added Org Data
    def editOrg()
    {
        println " editOrg"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.editOrg(request ,hm,ip)
        render hm as JSON
    }
    //Add New Org.
    def saveOrg()
    {
        println " saveOrg"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.saveOrg(request ,hm,ip)
        render hm as JSON
    }
    //Change Inactive or Active
    def orgIsactive()
    {
        println " orgIsactive"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.orgIsactive(request ,hm)
        render hm as JSON
    }
    //Import Org using excel File
    def importOrg()
    {
        println " importOrg"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.importOrg(request ,hm,ip,params)
        render hm as JSON
    }

    def getLibraryData()
    {
        println " getLibraryData"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.getLibraryData(request ,hm,ip)
        render hm as JSON
    }

    def editLibrary()
    {
        println " editLibrary"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.editLibrary(request ,hm,ip)
        render hm as JSON
    }

    def saveLibrary()
    {
        println " saveLibrary"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.saveLibrary(request ,hm,ip)
        render hm as JSON
    }

    def libraryIsactive()
    {
        println " libraryIsactive"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.libraryIsactive(request ,hm)
        render hm as JSON
    }
    def importLibrary()
    {
        println " importLibrary"+params
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.importLibrary(request ,hm,ip,params)
        render hm as JSON
    }

    //Push Master TO  Tenant
    def getTenantdata()
    {
        println " getTenantdata"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.getPushTenantdata(request ,hm)
        render hm as JSON
    }
    def pusMaster()
    {
        println " pusMaster"
        SuperAdminService superAdminService=new SuperAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        superAdminService.copy(request ,hm,ip)
        render hm as JSON
    }

}
