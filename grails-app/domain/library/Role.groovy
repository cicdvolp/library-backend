package library
import grails.gorm.MultiTenant
class Role implements MultiTenant<Role>
{
    String name  //Admin, Librarian, Teacher, Member
    String displayname
    boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[usertype:UserType]
    static constraints = {
        usertype nullable:true
        displayname nullable:true
    }
    static mapping = {
        isactive defaultValue: true
    }
}
