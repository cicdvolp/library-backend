package library

import grails.gorm.multitenancy.Tenants
import grails.gorm.transactions.Transactional
import groovy.json.JsonSlurper
import java.text.SimpleDateFormat

@Transactional
class SearchCatalogService {

    def serviceMethod() {

    }
    def dateToStringYY(Date date) {
        if(!date){
            return 'NA'
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = formatter.format(date);
        println strDate
        return strDate
    }
    def getAllBookwithfilterforemployee(request, hm, ip) {
        println("in getAllBookwithfilter:")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        AuthService auth = new AuthService()

        String purpose = "getAllBook"
        auth.checkauth(token, uid, hm, url, request, purpose)

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def count = dataresponse.count
                def loopend = count + 500
                int srno = count + 1
                ArrayList books = new ArrayList()
                User logeduser = User.findByUsername(log.username)

                LibraryDepartment department = null
                Publisher publisher = null
                Series series = null

                if (dataresponse.selecteddepartment != 'ALL')
                    department = LibraryDepartment.findById(dataresponse.selecteddepartment)
                if (dataresponse.selectedpublisher != 'ALL')
                    publisher = Publisher.findById(dataresponse.selectedpublisher)
                if (dataresponse.selectedseries != 'ALL')
                    series = Series.findById(dataresponse.selectedseries)

                def bookList = Book.createCriteria().list {
                    'in'("organization", logeduser.organization)
                    and {
                        if (dataresponse.selectedisbn != 'ALL')
                            'in'('id', Long.parseLong(dataresponse?.selectedisbn?.toString()))
                        if (dataresponse.selectedtitle != 'ALL')
                            'in'('id', Long.parseLong(dataresponse?.selectedtitle?.toString()))
                        if (dataresponse.selectedseries != 'ALL')
                            'in'('series', series)
                        if (dataresponse.selectedpublisher != 'ALL')
                            'in'('publisher', publisher)
                        if (dataresponse.selecteddepartment != 'ALL')
                            'in'("librarydepartment", department)
                    }
                }

                bookList?.sort { it?.id }

                for (def i = count; i < loopend; i++) {
                    if (i < bookList?.size()) {
                        def book = bookList[i]
                        if (book?.organization == null) {
                            book.organization = logeduser.organization
                            book.save(flush: true, failOnError: true)
                        }
                        HashMap temp = new HashMap()
                        temp.put('srno', srno++)
                        temp.put('id', book?.id)
                        temp.put('title', book?.title)
                        temp.put('series', book?.series?.name)
                        temp.put('isbn', book?.isbn)
                        temp.put('edition', book?.edition)
                        temp.put('bookType', book?.booktype?.name)
                        temp.put('category', book?.bookcategory?.name)
                        if (book?.librarydepartment?.name) {
                            temp.put('department', book?.librarydepartment?.name)
                        }
                        temp.put('publisher', book?.publisher?.name)
                        temp.put('organization', book?.organization?.name)
                        temp.put('copies', book?.total_books)
                        BookStatus bookStatus = BookStatus.findByName('Available')
                        def bookItems = BookItem.findAllByBookAndBookstatus(book, bookStatus)
                        temp.put('available', bookItems.size())
                        def arr = ""
                        for (author in book?.author) {
                            arr = arr + " " + author?.name
                        }
                        temp.put('authors', arr)
                        ArrayList itemArray = new ArrayList()
                        def bookItem = BookItem.findAllByBookAndOrganization(book, logeduser.organization)
                        for (item in bookItem) {
                            HashMap itemData = new HashMap()
                            itemData.put('accession_no', item.accession_number)
                            itemData.put('bookstatus', item.bookstatus?.name)
                            itemData.put('rack', item?.rack?.rack_number)
                            itemData.put('block', item?.rack?.block_number)
                            BookLending bookLending = BookLending.findByBookitemAndIsactive(item, true)
                            def member = "--"
                            if (bookLending) {
                                member = Member.findByUserAndOrganization(bookLending.user, logeduser.organization)?.name
                                if (member == null)
                                    member = Employee.findByUserAndOrganization(bookLending.user, logeduser.organization)?.name
                            }
                            itemData.put('name1', member)
                            itemArray.add(itemData)
                        }
                        temp.put('itemdata', itemArray)
                        books.add(temp)
                    }
                }
                hm.put('bookList', books)
                hm.put('booktitlecount', Book.findAllByOrganization(logeduser.organization).size())
                hm.put('bookitemcount', BookItem.findAllByOrganization(logeduser.organization).size())
                hm.put('organizationid', logeduser?.organization?.name)
                hm.put('msg', '200')
            }
            return hm
        }
        return hm
    }

    def getAllBookwithfilterforemployee1(request, hm, ip) {
        println("in getAllBookwithfilterforemployee1:")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println(dataresponse)
        AuthService auth = new AuthService()

        String purpose = "getAllBook"
        auth.checkauth(token, uid, hm, url, request, purpose)

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                User logeduser = User.findByUsername(log.username)
                def count = dataresponse.count
                if (count == 0) {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    Date today = new Date();

                    Calendar c = Calendar.getInstance();
                    c.setTime(new Date()); // Using today's date
                    c.add(Calendar.DATE, 1); // Adding 5 days

                    Date todayWithZeroTime = formatter.parse(formatter.format(c.getTime()));
                    OPACHits opacHits = OPACHits.findByOrganizationAndHitsdate(logeduser.organization, todayWithZeroTime)
                    if (opacHits == null) {
                        opacHits = new OPACHits()
                        opacHits.hitsdate = todayWithZeroTime
                        opacHits.organization = logeduser.organization
                        opacHits.noofhits = 1
                        opacHits.save(flush: true, failOnError: true)

                    } else {
                        opacHits.noofhits = opacHits.noofhits + 1
                        opacHits.save(flush: true, failOnError: true)
                    }
                }
                def loopend = count + 500
                int srno = count + 1
                ArrayList books = new ArrayList()
                LibraryDepartment department = null
                Publisher publisher = null
                Series series = null

                if (dataresponse.selecteddepartment != 'ALL')
                    department = LibraryDepartment.findById(dataresponse.selecteddepartment)
                if (dataresponse.selectedpublisher != 'ALL')
                    publisher = Publisher.findById(dataresponse.selectedpublisher)
                if (dataresponse.selectedseries != 'ALL')
                    series = Series.findById(dataresponse.selectedseries)

                def bookList1 = Book.createCriteria().list {
                    'in'("organization", logeduser.organization)
                    and {
                        if (dataresponse.selectedisbn != 'ALL')
                            'in'('isbn', dataresponse?.selectedisbn)
                        if (dataresponse.selectedtitle != 'ALL')
                            'in'('id', Long.parseLong(dataresponse?.selectedtitle?.toString()))
                        if (dataresponse.selectedseries != 'ALL')
                            'in'('series', series)
                        if (dataresponse.selectedpublisher != 'ALL')
                            'in'('publisher', publisher)
                        if (dataresponse.selecteddepartment != 'ALL')
                            'in'("librarydepartment", department)
                    }
                }

                def bookList = BookItem.createCriteria().list {
                    'in'("organization", logeduser.organization)
                    and {
                        'in'("book", bookList1)
                    }

                }

                bookList?.sort { it?.id }
                bookList?.sort { it?.display_order }

                for (def i = count; i < loopend; i++) {
                    if (i < bookList?.size()) {
                        def book = bookList[i].book
                        def item = bookList[i]
                        if (book?.organization == null) {
                            book.organization = logeduser.organization
                            book.save(flush: true, failOnError: true)
                        }
                        HashMap temp = new HashMap()
                        temp.put('srno', srno++)
                        temp.put('id', book?.id)
                        temp.put('bookitem_id', item?.id)
                        temp.put('title', book?.title)
                        temp.put('series', book?.series?.name)
                        temp.put('isbn', book?.isbn)
                        temp.put('edition', book?.edition)
                        temp.put('bookType', book?.booktype?.name)
                        temp.put('category', book?.bookcategory?.name)
                        if (book?.librarydepartment?.name) {
                            temp.put('department', book?.librarydepartment?.name)
                        }
                        temp.put('publisher', book?.publisher?.name)
                        temp.put('organization', book?.organization?.name)
                        temp.put('copies', book?.total_books)
                        temp.put('rackId', item?.rack?.id)
                        temp.put('titlelist', item?.rack?.id)

                        temp.put('access_no', item.accession_number)
                        temp.put('bookstatus', item.bookstatus?.name)
                        temp.put('rack', item?.rack?.rack_number)
                        temp.put('block', item?.rack?.block_number)
                        BookLending bookLending = BookLending.findByBookitemAndIsactive(item, true)
                        def member = "--"
                        if (bookLending) {
                            member = Member.findByUserAndOrganization(bookLending.user, logeduser.organization)?.name
                            if (member == null)
                                member = Employee.findByUserAndOrganization(bookLending.user, logeduser.organization)?.name
                        }
                        temp.put('name1', member)


                        BookStatus bookStatus = BookStatus.findByName('Available')
                        def bookItems = BookItem.findAllByBookAndBookstatus(book, bookStatus)
                        temp.put('available', bookItems.size())
                        def arr = ""
                        for (author in book?.author) {
                            arr = arr + " " + author?.name
                        }
                        temp.put('authors', arr)
//                        ArrayList itemArray = new ArrayList()
//                        def bookItem = BookItem.findAllByBookAndOrganization(book, logeduser.organization)
//                        for (item in bookItem) {
//                            HashMap itemData = new HashMap()
//                            itemData.put('accession_no', item.accession_number)
//                            itemData.put('bookstatus', item.bookstatus?.name)
//                            itemData.put('rack', item?.rack?.rack_number)
//                            itemData.put('block', item?.rack?.block_number)
//                            BookLending bookLending = BookLending.findByBookitemAndIsactive(item, true)
//                            def member = "--"
//                            if (bookLending) {
//                                member = Member.findByUserAndOrganization(bookLending.user, logeduser.organization)?.name
//                                if (member == null)
//                                    member = Employee.findByUserAndOrganization(bookLending.user, logeduser.organization)?.name
//                            }
//                            itemData.put('name1', member)
//                            itemArray.add(itemData)
//                        }
//                        temp.put('itemdata', itemArray)
                        books.add(temp)
                    }
                }
                hm.put('bookList', books)
                hm.put('utype', logeduser?.usertype?.name)
                hm.put('booktitlecount', Book.findAllByOrganization(logeduser.organization).size())
                hm.put('bookitemcount', BookItem.findAllByOrganization(logeduser.organization).size())
                hm.put('organizationid', logeduser?.organization?.name)
                hm.put('msg', '200')
            }
            return hm
        }
        return hm
    }


    def getAllBookwithfilterfordelete(request, hm, ip) {
        println("in getAllBookwithfilterfordelete:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        AuthService auth = new AuthService()

        String purpose = "getAllBook"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {

                ArrayList books = new ArrayList()
                User logeduser = User.findByUsername(log.username)
                Author author1 = null
                LibraryDepartment department = null
                Publisher publisher = null
                Series series = null
                println(dataresponse)

                if (dataresponse.selecteddepartment != 'ALL') {
                    department = LibraryDepartment.findById(dataresponse.selecteddepartment)
                }

                if (dataresponse.selectedseries != 'ALL') {
                    series = Series.findById(dataresponse.selectedseries)
                }
                def bookList
                if (dataresponse.a_no != "") {
                    bookList = BookItem.createCriteria().list {
                        'in'("organization", logeduser.organization)
                        and {
                            'in'("accession_number", dataresponse.a_no)
                        }
                    }
                } else {
                    bookList = BookItem.createCriteria().list {
                        'in'("organization", logeduser.organization)
                        or {
                            if (dataresponse.selectedseries != 'ALL')
                                'in'('series', series)
                            if (dataresponse.selecteddepartment != 'ALL')
                                'in'("librarydepartment", department)
                        }
                    }
                }


                ArrayList arrayList = new ArrayList()
                int srno = 1
                for (item in bookList) {
                    HashMap temp = new HashMap()
                    temp.put("srno", srno++)
                    temp.put("accession_no", item?.accession_number)
                    temp.put("title", item?.book?.title)
                    temp.put("dept", item?.librarydepartment?.name)
                    temp.put("series", item?.series?.name)
                    temp.put("id", item?.id)
                    temp.put("isdelete", false)
                    arrayList.add(temp)
                }
                hm.put('bookitemdata', arrayList)
                hm.put('msg', '200')
            }
            return hm
        }
        return hm
    }


    def getBookFilter(request, hm, ip) {
        println("in getBookFilter:")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String purpose = "getBookFilter"
        auth.checkauth(token, uid, hm, url, request, purpose)

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                User logeduser = User.findByUsername(log.username)
                HashMap temp2 = new HashMap()
                temp2.put("name", 'ALL')
                temp2.put("id", 'ALL')

                def publisherlist = []
                def departmentlist = []
                def titlelist = []
                def isbnlist = []
                def serieslist = []


                publisherlist.add(temp2)
                departmentlist.add(temp2)
                serieslist.add(temp2)
                titlelist.add(temp2)
                isbnlist.add(temp2)

                def series = Series.findAllByOrganization(logeduser.organization)

                LibraryConfiguration libraryConfiguration1 = LibraryConfiguration.findByIsactiveAndOrganizationAndName(true, logeduser.organization, "Periodicals_In_OPAC")

                if (libraryConfiguration1?.value == 'NO') {
                    BookClassification bookClassification = BookClassification.findByNameAndOrganization("Book", logeduser.organization)
                    series = Series.findAllByOrganizationAndBookclassification(logeduser.organization, bookClassification)
                }


                series.add(temp2)
                hm.put("serieslist", series)
                if (series)
                    serieslist.addAll(series)
                def dept = LibraryDepartment.findAllByOrganization(logeduser.organization)
                if (dept)
                    departmentlist.addAll(dept)
                def authorlist = Author.list()
                authorlist.add(temp2)
                println("authorlist : "+authorlist)
                def publisher = Publisher.list()
                if (publisher)
                    publisherlist.addAll(publisher)
                def isbn = Book.createCriteria().list() {
                    projections {
                        distinct('isbn')
                    }
                    'in'('organization', logeduser.organization)
                }
                for (item in isbn) {
                    HashMap temp = new HashMap()
                    temp.put("name", item)
                    isbnlist.add(temp)
                }
                def title = Book.createCriteria().list() {

                    'in'('organization', logeduser.organization)
                }
                for (item in title) {
                    HashMap temp = new HashMap()
                    temp.put("name", item.title)
                    temp.put("id", item.id)
                    titlelist.add(temp)
                }

                hm.put("departmentlist", departmentlist)
                hm.put("serieslist", serieslist)
                hm.put("publisherlist", publisherlist)
                hm.put('isbnlist', isbnlist)
                hm.put('titlelist', titlelist)
                hm.put('autherlist', authorlist)
                ArrayList rack_list = new ArrayList()
                def racks = Rack.findAllByIsactive(true)
                for (item in racks) {
                    HashMap temp = new HashMap()
                    temp.put("rackId", item.id)
                    temp.put("rackNo", item.rack_number)
                    temp.put("blockNo", item.block_number)
                    temp.put("capacity", item.capacity)
                    rack_list.add(temp)
                }
                LibraryConfiguration libraryConfiguration = LibraryConfiguration.findByNameAndIsactive("new_arrivals_book_no_of_days", true)
                hm.put('arriavl_days', libraryConfiguration?.value)
                hm.put('racklist', rack_list)
                hm.put('bookStatus', BookStatus.findAllByIsactive(true))

                hm.put('msg', '200')
            }
            return hm
        }
        return hm
    }


    def getAllBookwithfilter(request, hm, ip) {
        println("in getAllBookwithfilter:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        AuthService auth = new AuthService()

        String purpose = "getAllBook"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                int srno = 1
                ArrayList books = new ArrayList()
                User logeduser = User.findByUsername(log.username)
                Author author1 = null
                LibraryDepartment department = null
                Publisher publisher = null
                Author authors = null
                println(dataresponse)
                if (dataresponse.selectedisbn != 'ALL') {

                }
                if (dataresponse.selectedtitle != 'ALL') {

                }
//                if(dataresponse.selectedauther!='ALL'){
//                    author1=Author.findById(dataresponse.selectedauther)
//                }
                if (dataresponse.selecteddepartment != 'ALL') {
                    department = LibraryDepartment.findById(dataresponse.selecteddepartment)
                }
                if (dataresponse.selectedpublisher != 'ALL') {
                    publisher = Publisher.findById(dataresponse.selectedpublisher)
                }
                if (dataresponse.selectedauthor != 'ALL'){
                    authors = Author.findById(dataresponse.selectedauthor)
                }
                println("authors "+authors?.name)
                def bookList = Book.createCriteria().list {
                    'in'("organization", logeduser.organization)
                    or {
                        if (dataresponse.selectedisbn != 'ALL')
                            'in'('id', Long.parseLong(dataresponse.selectedisbn.toString()))
                        if (dataresponse.selectedtitle != 'ALL')
                            'in'('id', Long.parseLong(dataresponse.selectedtitle.toString()))
                        if (dataresponse.selectedpublisher != 'ALL')
                            'in'('publisher', publisher)
                        if (dataresponse.selecteddepartment != 'ALL')
                            'in'("librarydepartment", department)
                        if (dataresponse.selectedauthor != 'ALL')
                        {
                            author {
                                'in'('id', authors?.id)
                            }
                        }
                    }
                    
                }

//                def bookList = Book.findAllByOrganization(logeduser.organization)


                for (book in bookList) {
//                    def bookitem = BookItem.findAllByBook(book)
//                    for (item in bookitem) {
                    if (book?.organization == null) {
                        book.organization = logeduser.organization
                        book.save(flush: true, failOnError: true)
                    }
                    /* else
                     {
                         continue
                     }*/
                    HashMap temp = new HashMap()
                    temp.put('srno', srno++)
                    temp.put('id', book?.id)
//                        temp.put('a_no', item?.accession_number)
                    temp.put('title', book?.title)
                    temp.put('series', book?.series?.name)
                    temp.put('isbn', book?.isbn)
                    temp.put('edition', book?.edition)
//                    temp.put('bookFormat',book?.isbn)
                    temp.put('bookType', book?.booktype?.name)
                    temp.put('category', book?.bookcategory?.name)
                    if (book?.librarydepartment?.name) {
                        temp.put('department', book?.librarydepartment?.name)
                    }
                    temp.put('publisher', book?.publisher?.name)
                    temp.put('organization', book?.organization?.name)
                    temp.put('copies', book?.total_books)
                    BookStatus bookStatus = BookStatus.findByName('Available')
                    def bookItems = BookItem.findAllByBookAndBookstatus(book, bookStatus)
                    temp.put('available', bookItems.size())
                    def arr = ""
                    for (author in book?.author) {
                        arr = arr + " " + author?.name
                    }
                    temp.put('authors', arr)
                    ArrayList itemArray = new ArrayList()
                    def bookItem = BookItem.findAllByBookAndOrganization(book, logeduser.organization)
                    //println('bookItem---'+bookItem.size())
                    for (item in bookItem) {
                        HashMap itemData = new HashMap()
                        itemData.put('accession_no', item.accession_number)
                        itemData.put('bookstatus', item.bookstatus?.name)
                        itemData.put('rack', item?.rack?.rack_number)
                        itemData.put('block', item?.rack?.block_number)
                        if (item?.organization == null) {
                            item.organization = logeduser.organization
                            item.save(flush: true, failOnError: true)
                        }
                        /*else
                        {
                            continue
                        }*/
                        itemArray.add(itemData)
                    }
                    temp.put('itemdata', itemArray)
                    books.add(temp)
//                    }

                }
//                ArrayList authList = new ArrayList()
//                def authorsList = Author.findAll()
//                for (author in authorsList) {
//                    HashMap temp = new HashMap()
//                    temp.put('name', author?.name)
//                    temp.put('id', author?.id)
//                    authList.add(temp)
//                }
//                hm.put('authorsList',authList)
                hm.put('bookList', books)
                hm.put('booktitlecount', Book.findAllByOrganization(logeduser.organization).size())
                hm.put('bookitemcount', BookItem.findAllByOrganization(logeduser.organization).size())
                hm.put('organizationid', logeduser?.organization?.name)

                hm.put('msg', '200')
            }
            return hm
        }
        return hm
    }

    def getAllBook(request, hm, ip) {
        println("in getAllBook:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String purpose = "getAllBook"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                int srno = 1
                ArrayList books = new ArrayList()
                User logeduser = User.findByUsername(log.username)

                def bookList = Book.findAllByOrganization(logeduser.organization)


                for (book in bookList) {
//                    def bookitem = BookItem.findAllByBook(book)
//                    for (item in bookitem) {
                    if (book?.organization == null) {
                        book.organization = logeduser.organization
                        book.save(flush: true, failOnError: true)
                    }
                    /* else
                        {
                            continue
                        }*/
                    HashMap temp = new HashMap()
                    temp.put('srno', srno++)
                    temp.put('id', book?.id)
//                        temp.put('a_no', item?.accession_number)
                    temp.put('title', book?.title)
                    temp.put('series', book?.series?.name)
                    temp.put('isbn', book?.isbn)
                    temp.put('edition', book?.edition)
//                    temp.put('bookFormat',book?.isbn)
                    temp.put('bookType', book?.booktype?.name)
                    temp.put('category', book?.bookcategory?.name)
                    if (book?.librarydepartment?.name) {
                        temp.put('department', book?.librarydepartment?.name)
                    }
                    temp.put('publisher', book?.publisher?.name)
                    temp.put('organization', book?.organization?.name)
                    temp.put('copies', book?.total_books)
                    BookStatus bookStatus = BookStatus.findByName('Available')
                    def bookItems = BookItem.findAllByBookAndBookstatus(book, bookStatus)
                    temp.put('available', bookItems.size())
                    def arr = ""
                    for (author in book?.author) {
                        arr = arr + " " + author?.name
                    }
                    temp.put('authors', arr)
                    ArrayList itemArray = new ArrayList()
                    def bookItem = BookItem.findAllByBookAndOrganization(book, logeduser.organization)
                    //println('bookItem---'+bookItem.size())
                    for (item in bookItem) {
                        HashMap itemData = new HashMap()
                        itemData.put('accession_no', item.accession_number)
                        itemData.put('bookstatus', item.bookstatus?.name)
                        itemData.put('rack', item?.rack?.rack_number)
                        itemData.put('block', item?.rack?.block_number)
                        if (item?.organization == null) {
                            item.organization = logeduser.organization
                            item.save(flush: true, failOnError: true)
                        }
                        /*else
                            {
                                continue
                            }*/
                        itemArray.add(itemData)
                    }
                    temp.put('itemdata', itemArray)
                    books.add(temp)
//                    }

                }
//                ArrayList authList = new ArrayList()
//                def authorsList = Author.findAll()
//                for (author in authorsList) {
//                    HashMap temp = new HashMap()
//                    temp.put('name', author?.name)
//                    temp.put('id', author?.id)
//                    authList.add(temp)
//                }
//                hm.put('authorsList',authList)
                hm.put('bookList', books)
                hm.put('booktitlecount', Book.findAllByOrganization(logeduser.organization).size())
                hm.put('bookitemcount', BookItem.findAllByOrganization(logeduser.organization).size())
                hm.put('organizationid', logeduser?.organization?.name)

                hm.put('msg', '200')
            }
            return hm
        }
        return hm
    }

    def increaseopaccount(request, hm, ip) {
        println("in increaseopaccount:")

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String purpose = "increaseopaccount"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                User user = User.findByUsername(log.username)

                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                Date today = new Date();

                Calendar c = Calendar.getInstance();
                c.setTime(new Date()); // Using today's date
                c.add(Calendar.DATE, 1); // Adding 5 days

                Date todayWithZeroTime = formatter.parse(formatter.format(c.getTime()));

                OPACHits opacHits = OPACHits.findByOrganizationAndHitsdate(user.organization, todayWithZeroTime)
                if (opacHits == null) {
                    opacHits = new OPACHits()
                    opacHits.hitsdate = todayWithZeroTime
                    opacHits.organization = user.organization
                    opacHits.noofhits = 1
                    opacHits.save(flush: true, failOnError: true)

                } else {
                    opacHits.noofhits = opacHits.noofhits + 1
                    opacHits.save(flush: true, failOnError: true)
                }

            }

        }

    }

    def getBookItemDataAccOpac(request, hm, ip) {
        println("in getBookItemDataAccOpac:")

        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        String purpose = "getBookItemdata"
        auth.checkauth(token, uid, hm, url, request, purpose)

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                User logeduser = User.findByUsername(log.username)
                int srno = 1
                ArrayList books = new ArrayList()
                def book_item = BookItem.findByAccession_numberAndOrganization(dataresponse.a_no.toString(), logeduser.organization)
                def bookList = book_item?.book

                for (book in bookList) {
                    HashMap temp = new HashMap()
                    temp.put('srno', srno++)
                    temp.put('id', book?.id)
                    temp.put('title', book?.title)
                    temp.put('series', book?.series?.name)
                    temp.put('isbn', book?.isbn.toString())
                    temp.put('edition', book?.edition)
                    temp.put('bookType', book?.booktype?.name)
                    temp.put('category', book?.bookcategory?.name)
                    temp.put('publisher', book?.publisher?.name)
                    temp.put('organization', book?.organization?.name)
                    temp.put('copies', book?.total_books)
                    BookStatus bookStatus = BookStatus.findByName('Available')
                    def bookItems = BookItem.findAllByBookAndBookstatus(book, bookStatus)
                    temp.put('available', bookItems.size())
                    def arr = ""
                    for (author in book?.author) {
                        arr = arr + " " + author?.name
                    }
                    temp.put('authors', arr)
                    ArrayList itemArray = new ArrayList()
                    def bookItem = BookItem.findAllByBook(book)
                    HashMap itemData = new HashMap()
                    BookLending bookLending = BookLending.findByBookitemAndIsactive(book_item, true)
                    def member = "--"
                    if (bookLending) {
                        member = Member.findByUserAndOrganization(bookLending.user, logeduser.organization)?.name
                        if (member == null)
                            member = Employee.findByUserAndOrganization(bookLending.user, logeduser.organization)?.name
                    }

                    itemData.put('name1', member)
                    itemData.put('accession_no', book_item.accession_number.toString())
                    itemData.put('bookstatus', book_item.bookstatus?.name)
                    itemArray.add(itemData)
                    temp.put('itemdata', itemArray)
                    books.add(temp)
                }
                hm.put('bookList', books)
                hm.put('booktitlecount', Book.findAllByOrganization(logeduser.organization).size())
                hm.put('bookitemcount', BookItem.findAllByOrganization(logeduser.organization).size())
                hm.put('organizationid', logeduser?.organization?.name)
                hm.put('msg', '200')
            }
            return hm
        }
        return hm
    }

    def getBookItemDataAccOpac1(request, hm, ip) {
        println("in getBookItemDataAccOpac:")

        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        String purpose = "getBookItemdata"
        auth.checkauth(token, uid, hm, url, request, purpose)

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                User logeduser = User.findByUsername(log.username)
                int srno = 1
                ArrayList books = new ArrayList()
                def book_item = BookItem.findByAccession_numberAndOrganization(dataresponse.a_no.toString(), logeduser.organization)
                def book = book_item?.book

//                for(book in bookList) {
                HashMap temp = new HashMap()
                temp.put('srno', srno++)
                temp.put('id', book?.id)
                temp.put('title', book?.title)
                temp.put('series', book?.series?.name)
                temp.put('isbn', book?.isbn.toString())
                temp.put('edition', book?.edition)
                temp.put('bookType', book?.booktype?.name)
                temp.put('category', book?.bookcategory?.name)
                temp.put('publisher', book?.publisher?.name)
                temp.put('organization', book?.organization?.name)
                temp.put('copies', book?.total_books)
                temp.put('rackNo', book_item?.rack?.rack_number)
                temp.put('block_no', book_item?.rack?.block_number)
                BookStatus bookStatus = BookStatus.findByName('Available')
                def bookItems = BookItem.findAllByBookAndBookstatus(book, bookStatus)
                temp.put('available', bookItems.size())
                def arr = ""
                for (author in book?.author) {
                    arr = arr + " " + author?.name
                }
                temp.put('authors', arr)
                ArrayList itemArray = new ArrayList()
                def bookItem = BookItem.findAllByBook(book)
                HashMap itemData = new HashMap()
                BookLending bookLending = BookLending.findByBookitemAndIsactive(book_item, true)
                def member = "--"
                if (bookLending) {
                    member = Member.findByUserAndOrganization(bookLending.user, logeduser.organization)?.name
                    if (member == null)
                        member = Employee.findByUserAndOrganization(bookLending.user, logeduser.organization)?.name
                }

                temp.put('name1', member)
                temp.put('access_no', book_item?.accession_number.toString())
                temp.put('bookstatus', book_item?.bookstatus?.name)
//                    itemArray.add(itemData)
                temp.put('itemdata', itemArray)
                books.add(temp)
//                }
                hm.put('bookList', books)
                hm.put('utype', logeduser?.usertype?.name)
                println("usertype--------------------------------------------")
                println("hm.utype " + logeduser?.usertype?.name)
                hm.put('booktitlecount', Book.findAllByOrganization(logeduser.organization).size())
                hm.put('bookitemcount', BookItem.findAllByOrganization(logeduser.organization).size())
                hm.put('organizationid', logeduser?.organization?.name)
                hm.put('msg', '200')
            }
            return hm
        }
        return hm
    }

    def getnewarrivalbooksopac(request, hm, ip) {
        println("in getBookItemDataAccOpac:")

        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        String purpose = "getBookItemdata"
        auth.checkauth(token, uid, hm, url, request, purpose)

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                User logeduser = User.findByUsername(log.username)
                int srno = 1

                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
//                String dateInString = "15-10-2015";
                Date date1 = sdf1.parse(dataresponse.firstDate);
                Date date2 = sdf1.parse(dataresponse.lastDate);

                def bookList = BookItem.createCriteria().list {
                    'in'("organization", logeduser.organization)

                    and {
                        between("date_of_entry", date1, date2)
                    }
                }
                println(" dfshs djf sbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb")
                println(bookList)
                println(date1)
                println(date2)
                ArrayList books = new ArrayList()
//                def book_item = BookItem.findByAccession_numberAndOrganization(dataresponse.a_no.toString(),logeduser.organization)


                for (book_item in bookList) {
                    def book = book_item?.book
                    HashMap temp = new HashMap()
                    temp.put('srno', srno++)
                    temp.put('id', book?.id)
                    temp.put('bookitem_id', book_item?.id)
                    temp.put('title', book?.title)
                    temp.put('series', book?.series?.name)
                    temp.put('isbn', book?.isbn.toString())
                    temp.put('edition', book?.edition)
                    temp.put('bookType', book?.booktype?.name)
                    temp.put('category', book?.bookcategory?.name)
                    temp.put('publisher', book?.publisher?.name)
                    temp.put('organization', book?.organization?.name)
                    temp.put('copies', book?.total_books)
                    temp.put('rackNo', book_item?.rack?.rack_number)
                    temp.put('block_no', book_item?.rack?.block_number)
                    BookStatus bookStatus = BookStatus.findByName('Available')
                    def bookItems = BookItem.findAllByBookAndBookstatus(book, bookStatus)
                    temp.put('available', bookItems.size())
                    def arr = ""
                    for (author in book?.author) {
                        arr = arr + " " + author?.name
                    }
                    temp.put('authors', arr)
                    ArrayList itemArray = new ArrayList()
                    def bookItem = BookItem.findAllByBook(book)
                    HashMap itemData = new HashMap()
                    BookLending bookLending = BookLending.findByBookitemAndIsactive(book_item, true)
                    def member = "--"
                    if (bookLending) {
                        member = Member.findByUserAndOrganization(bookLending.user, logeduser.organization)?.name
                        if (member == null)
                            member = Employee.findByUserAndOrganization(bookLending.user, logeduser.organization)?.name
                    }

                    temp.put('name1', member)
                    temp.put('access_no', book_item.accession_number.toString())
                    temp.put('bookstatus', book_item.bookstatus?.name)
//                    itemArray.add(itemData)
                    temp.put('itemdata', itemArray)
                    books.add(temp)
                }
                hm.put('bookList', books)
                hm.put('booktitlecount', Book.findAllByOrganization(logeduser.organization).size())
                hm.put('bookitemcount', BookItem.findAllByOrganization(logeduser.organization).size())
                hm.put('organizationid', logeduser?.organization?.name)
                hm.put('msg', '200')
            }
            return hm
        }
        return hm
    }

    // Home OPAC
//    def getBookFilter(request, hm, ip) {
//        println("in getBookFilter:")
//
//        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
//        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
//        String url = request.getHeader("router-path")
//
//        AuthService auth = new AuthService()
//
//        String purpose = "getBookFilter"
//        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
//
//        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
//            Login log = Login.findByUsername(uid)
//            Tenants.withId(log.tenantclient.tenantid) {
//                User logeduser = User.findByUsername(log.username)
//               def publisherlist=[]
//                def departmentlist=[]
//                def autherlist=[]
//                def titlelist=[]
//                def isbnlist=[]
//                HashMap temp2 =new HashMap()
//                temp2.put("name",'ALL')
//                temp2.put("id",'ALL')
//
//                titlelist.add(temp2)
//                isbnlist.add(temp2)
//
//                def booklist=Book.findAllByOrganization(logeduser.organization)
//                for(x in booklist)
//                {
//                    HashMap temp =new HashMap()
//                    temp.put("name",x.isbn)
//                    temp.put("id",x.id)
//                    isbnlist.add(temp)
//
//                    HashMap temp1 =new HashMap()
//                    temp1.put("name",x.title)
//                    temp1.put("id",x.id)
//                    titlelist.add(temp1)
//
//                }
//
//                def publisher=Publisher.findAll()
//                publisher.add(temp2)
//                hm.put("publisherlist",publisher)
//
//                def libdept=LibraryDepartment.findAllByOrganization(logeduser.organization)
//                libdept.add(temp2)
//                hm.put("departmentlist",libdept)
//
//                def series=Series.findAllByOrganization(logeduser.organization)
//                series.add(temp2)
//                hm.put("serieslist",series)
//
//                def author=Author.list()
//                author.add(temp2)
//                hm.put("autherlist",author)
//
//                hm.put('isbnlist',isbnlist)
//                hm.put('titlelist',titlelist)
//
//                hm.put('msg','200')
//            }
//            return hm
//        }
//        return hm
//    }
    def getBookdeleteFilter(request, hm, ip) {
        println("in getBookdeleteFilter:")

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String purpose = "getBookFilter"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                User logeduser = User.findByUsername(log.username)
                HashMap temp2 = new HashMap()
                temp2.put("name", 'ALL')
                temp2.put("id", 'ALL')
                def libdept = LibraryDepartment.findAllByOrganization(logeduser.organization)
                libdept.add(temp2)
                hm.put("departmentlist", libdept)
                def series = Series.findAllByOrganization(logeduser.organization)
                series.add(temp2)
                hm.put("serieslist", series)
                hm.put('msg', '200')
            }
            return hm
        }
        return hm
    }

    def getAllOrganizationHome(request, hm, ip) {
        println("in getAllOrganizationHome:")

        def tenantlist = TenantClient.findAllByIsactive(true)
        ArrayList organization_data = new ArrayList()
        for (item in tenantlist) {
            Tenants.withId(item.tenantid) {
                def organization = Organization.findAllByIsactive(true)
                for (org in organization) {
                    HashMap temp = new HashMap()
                    temp.put('name', org?.name)
                    temp.put('id', org?.id)
                    temp.put('tenantid', item.id)
                    organization_data.add(temp)
                }
            }
        }
        if (organization_data) {
            hm.put('organizationid', organization_data[0]?.id)
            hm.put('tenantid', organization_data[0]?.tenantid)
        } else {
            hm.put('organizationid', "")
            hm.put('tenantid', "")
        }
        hm.put('organization_data', organization_data)
        hm.put('msg', '200')
        return hm
    }

    def getAllBookHome(request, hm, ip) {
        println("in getAllBookHome:")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        def tenantid = dataresponse["tenantid"]
        TenantClient tenantclient = TenantClient.findById(tenantid)

        def organizationid = dataresponse["organizationid"]

        Tenants.withId(tenantclient.tenantid) {
            def organization = Organization.findById(organizationid)

            int srno = 1
            ArrayList books = new ArrayList()
            def bookList = Book.findAllByOrganization(organization)
            for (book in bookList) {
                HashMap temp = new HashMap()
                temp.put('srno', srno++)
                temp.put('id', book?.id)
                temp.put('title', book?.title)
                temp.put('isbn', book?.isbn)
                temp.put('edition', book?.edition)
                temp.put('bookType', book?.booktype?.name)
                temp.put('category', book?.bookcategory?.name)
                if (book?.program?.name) {
                    temp.put('department', book?.program?.name)
                }
                temp.put('publisher', book?.publisher?.name)
                temp.put('organization', book?.organization?.name)
                temp.put('copies', book?.total_books)
                BookStatus bookStatus = BookStatus.findByName('Available')
                def bookItems = BookItem.findAllByBookAndBookstatusAndOrganization(book, bookStatus, organization)
                temp.put('available', bookItems.size())
                def arr = ""
                for (author in book?.author) {
                    arr = arr + " " + author?.name
                }
                temp.put('authors', arr)
                ArrayList itemArray = new ArrayList()
                def bookItem = BookItem.findAllByBookAndOrganization(book, organization)
                for (item in bookItem) {
                    HashMap itemData = new HashMap()
                    itemData.put('accession_no', item.accession_number)
                    itemData.put('bookstatus', item.bookstatus?.name)
                    itemData.put('rack', item?.rack?.rack_number)
                    itemData.put('block', item?.rack?.block_number)
                    itemArray.add(itemData)
                }
                temp.put('itemdata', itemArray)
                books.add(temp)
            }
//            ArrayList authList = new ArrayList()
//            def authorsList = Author.findAll()
//            for (author in authorsList) {
//                HashMap temp = new HashMap()
//                temp.put('name', author?.name)
//                temp.put('id', author?.id)
//                authList.add(temp)
//            }
//            hm.put('authorsList',authList)
            hm.put('bookList', books)
            hm.put('msg', '200')
        }
        return hm
    }

    def getBookItemdataAcc(request, hm, ip) {
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        def tenantid = dataresponse["tenantid"]
        TenantClient tenantclient = TenantClient.findById(tenantid)

        def organizationid = dataresponse["organizationid"]

        Tenants.withId(tenantclient.tenantid) {
            def organization = Organization.findById(organizationid)

            int srno = 1
            ArrayList books = new ArrayList()
            def book_item = BookItem.findByAccession_number(dataresponse.a_no)
            def bookList = book_item?.book

            for (book in bookList) {
                HashMap temp = new HashMap()
                temp.put('srno', srno++)
                temp.put('id', book?.id)
                temp.put('title', book?.title)
                temp.put('isbn', book?.isbn)
                temp.put('edition', book?.edition)
                temp.put('bookType', book?.booktype?.name)
                temp.put('category', book?.bookcategory?.name)
                temp.put('publisher', book?.publisher?.name)
                temp.put('copies', book?.total_books)
                BookStatus bookStatus = BookStatus.findByName('Available')
                def bookItems = BookItem.findAllByBookAndBookstatusAndOrganization(book, bookStatus, organization)
                temp.put('available', bookItems.size())
                def arr = ""
                for (author in book?.author) {
                    arr = arr + " " + author?.name
                }
                temp.put('authors', arr)
                ArrayList itemArray = new ArrayList()
                def bookItem = BookItem.findAllByBookAndOrganization(book, organization)
                HashMap itemData = new HashMap()
                itemData.put('accession_no', book_item.accession_number)
                itemData.put('bookstatus', book_item.bookstatus?.name)
                itemArray.add(itemData)
                temp.put('itemdata', itemArray)
                books.add(temp)
            }
//            ArrayList authList = new ArrayList()
//            def authorsList = Author.findAll()
//            for (author in authorsList) {
//                HashMap temp = new HashMap()
//                temp.put('name', author?.name)
//                temp.put('id', author?.id)
//                authList.add(temp)
//            }
//            hm.put('authorsList',authList)
            hm.put('bookList', books)
            hm.put('msg', '200')
        }
        return hm
    }

    def getBookItemdataHome(request, hm, ip) {
        println("in getBookItemData:")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        def tenantid = dataresponse["tenantid"]
        TenantClient tenantclient = TenantClient.findById(tenantid)

        def organizationid = dataresponse["organizationid"]

        Tenants.withId(tenantclient.tenantid) {
            def organization = Organization.findById(organizationid)

            int srno = 1
            ArrayList bookItemlist = new ArrayList()
            def book = Book.findById(dataresponse.bookId)
            HashMap bookData = new HashMap()
            bookData.put('isbn', book.isbn)
            bookData.put('title', book.title)
            bookData.put('edition', book?.edition)
            bookData.put('volume', book?.volume)
            bookData.put('bookFormat', 'format')
            bookData.put('bookType', book?.booktype?.name)
            bookData.put('category', book?.bookcategory?.name)
            bookData.put('publisher', book?.publisher?.name)
            bookData.put('copies', book?.total_books)
            def authorArr = book?.author
            ArrayList arr = new ArrayList()
            for (author in authorArr) {
                def authorName = Author.findById(author?.id)
                arr.add(authorName?.name)
            }
            bookData.put('authors', arr)

            def items = BookItem.findAllByBookAndOrganization(book, organization)
            for (item in items) {
                HashMap temp = new HashMap()
                temp.put("srno", srno++)
                temp.put("id", item?.id)
                temp.put("access_no", item?.accession_number)
                temp.put("book_status", item?.bookstatus?.displayname)
                temp.put("book_statusId", item?.bookstatus?.id)
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                def b_date
                def d_date
                if (item?.borrowed_date) {
                    temp.put("borrowed_date", dateToString(item?.borrowed_date))
                    BookLending bookLending = BookLending.findByBookitemAndIsactiveAndBorrowing_date(item, true, item?.borrowed_date)
                    temp.put("member", bookLending?.user?.username)
                }

                if (item?.due_date)
                    temp.put("due_date", dateToString(item?.due_date))

                if (item?.withdrawal_date)
                    temp.put("w_date", dateToString(item?.withdrawal_date))

                temp.put("w_no", item?.withdrawal_no)
                temp.put("p_place", item?.publication_place)
                temp.put("rackNo", item?.rack?.rack_number)
                temp.put("rackId", item?.rack?.id)
                temp.put("block_no", item?.rack?.block_number)
                bookItemlist.add(temp)
            }

            ArrayList bookStatuslist = new ArrayList()
            def bookStatus = BookStatus.findAllByIsactive(true)
            for (ut in bookStatus) {
                HashMap temp = new HashMap()
                temp.put("bookStatus", ut.name)
                temp.put("bookStatusIsactive", ut.isactive)
                temp.put("book_statusId", ut.id)
                temp.put("displayname", ut.displayname)
                bookStatuslist.add(temp)
            }

            ArrayList rack_list = new ArrayList()
            def racks = Rack.findAllByIsactive(true)
            for (item in racks) {
                HashMap temp = new HashMap()
                temp.put("rackId", item.id)
                temp.put("rackNo", item.rack_number)
                temp.put("blockNo", item.block_number)
                temp.put("capacity", item.capacity)
                rack_list.add(temp)
            }

            hm.put('bookStatus', bookStatuslist)
            hm.put('bookItemlist', bookItemlist)
            hm.put('bookData', bookData)
            hm.put('racklist', rack_list)
            hm.put('msg', '200')
        }
        return hm
    }

    def dateToString(Date date) {
        if (!date) {
            return 'NA'
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String strDate = formatter.format(date);
        println strDate
        return strDate
    }


    // New OPAC System - Komal
    def getBookFilterSeries(request, hm, ip) {
        println("in getBookFilterSeries:")

        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "getBookFilterSeries"
        auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                User logeduser = User.findByUsername(log.username)

                HashMap temp2 = new HashMap()
                temp2.put("name", 'ALL')
                temp2.put("id", 'ALL')

                def departmentlist = []
                def libdept = LibraryDepartment.findAllByOrganization(logeduser.organization)
                libdept.add(temp2)
                hm.put("departmentlist", libdept)


                def series = Series.findAllByOrganization(logeduser.organization)

                LibraryConfiguration libraryConfiguration1 = LibraryConfiguration.findByIsactiveAndOrganizationAndName(true, logeduser.organization, "Periodicals_In_OPAC")

                if (libraryConfiguration1?.value == 'NO') {
//                    BookClassification bookClassification=BookClassification.findByNameAndOrganization("Book",logeduser.organization)
                    def bookClassification = BookClassification.createCriteria().list {
                        'in'("organization", logeduser.organization)
                        and {
                            'in'("name", ["Book", "Book With CD"])

                        }
                    }
                    series = Series.createCriteria().list {
                        'in'("organization", logeduser.organization)
                        and {
                            'in'("bookclassification", bookClassification)
                            'in'("isactive", true)
                        }
                    }
//                    series=Series.findAllByOrganizationAndBookclassification(logeduser.organization,bookClassification)
                }
                series.add(temp2)
                println("series -----------------"+series)
                hm.put("serieslist", series)
                hm.put("mediumlist", Language.findAllByIsactiveAndOrganization(true,logeduser.organization))
                hm.put('msg', '200')
            }
            return hm
        }
        return hm
    }

    def getBookFilterSeriesHOME(request, hm, ip) {
        println("in getBookFilterSeriesHOME:")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        def tenantid = dataresponse["tenantid"]
        TenantClient tenantclient = TenantClient.findById(tenantid)
        Tenants.withId(tenantclient.tenantid) {
            Organization org = Organization.findById(dataresponse.organizationid)
            HashMap temp2 = new HashMap()
            temp2.put("name", 'ALL')
            temp2.put("id", 'ALL')
            def departmentlist = []
            def libdept = LibraryDepartment.findAllByOrganization(org)
            libdept.add(temp2)
            hm.put("departmentlist", libdept)
            def series = Series.findAllByOrganization(org)
            LibraryConfiguration libraryConfiguration1 = LibraryConfiguration.findByIsactiveAndOrganizationAndName(true, org, "Periodicals_In_OPAC")
            if (libraryConfiguration1?.value == 'NO') {
//                BookClassification bookClassification = BookClassification.findByNameAndOrganization("Book", org)
//                series = Series.findAllByOrganizationAndBookclassification(org, bookClassification)

                def bookClassification = BookClassification.createCriteria().list {
                    'in'("organization", org)
                    and {
                        'in'("name", ["Book", "Book With CD"])

                    }
                }
                series = Series.createCriteria().list {
                    'in'("organization",org)
                    and {
                        'in'("bookclassification", bookClassification)

                    }
                }

            }
            series.add(temp2)
            hm.put("serieslist", series)
            hm.put("mediumlist",  Language.findAllByIsactiveAndOrganization(true,org))
            hm.put('msg', '200')
        }
        hm.put('msg', '200')
        return hm

    }

    def getBookItemDataSeries(request, hm, ip) {
        println("in getBookItemDataSeries:")

        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println(dataresponse)
        String purpose = "getBookItemDataSeries"
        auth.checkauth(token, uid, hm, url, request, purpose)

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                User logeduser = User.findByUsername(log.username)
                def count = dataresponse.count
                if (count == 0) {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
//                    Date today = new Date().plus(1);

                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    Calendar c = Calendar.getInstance();
                    c.setTime(new Date()); // Using today's date
                    c.add(Calendar.DATE, 1); // Adding 5 days
//                    String output = sdf.format(c.getTime());
//                    System.out.println(output);
//
                    Date todayWithZeroTime = formatter.parse(formatter.format(c.getTime()));
                    OPACHits opacHits = OPACHits.findByOrganizationAndHitsdate(logeduser.organization, todayWithZeroTime)
                    if (opacHits == null) {
                        opacHits = new OPACHits()
                        opacHits.hitsdate = todayWithZeroTime
                        opacHits.organization = logeduser.organization
                        opacHits.noofhits = 1
                        opacHits.save(flush: true, failOnError: true)

                    } else {
                        opacHits.noofhits = opacHits.noofhits + 1
                        opacHits.save(flush: true, failOnError: true)
                    }
                }

                def loopend = count + 500
                int srno = count + 1
                ArrayList books = new ArrayList()
                LibraryDepartment department = null
                Series series = null
                if (dataresponse.selecteddepartment != 'ALL')
                    department = LibraryDepartment.findById(dataresponse.selecteddepartment)
                if (dataresponse.selectedseries != 'ALL')
                    series = Series.findById(dataresponse.selectedseries)
                def author1
                if (dataresponse.picked == 'Author')
                    author1 = Author.findAllByNameIlike("%" + dataresponse?.keyword?.toString() + "%")
                println(author1)
                def publisher
                if (dataresponse?.keyword && (dataresponse?.picked == 'Publisher'))
                    publisher = Publisher.findAllByNameIlike("%" + dataresponse?.keyword?.toString() + "%")
                def language
                if (dataresponse?.keyword && (dataresponse?.picked == 'Medium' ))
                    language = Language.findAllByOrganizationAndNameIlike(logeduser.organization,"%" + dataresponse?.keyword?.toString() + "%")
               println language
                def serieskeyword
                def booktypekeyword
                def categorykeyword
                def departmentkeyword
                if (dataresponse?.keyword && dataresponse.picked == 'Keyword') {
                    serieskeyword = Series.findAllByOrganizationAndNameIlike(logeduser.organization, "%" + dataresponse?.keyword?.toString() + "%")
                    booktypekeyword = BookType.findAllByOrganizationAndNameIlike(logeduser.organization, "%" + dataresponse?.keyword?.toString() + "%")
                    categorykeyword = BookCategory.findAllByOrganizationAndNameIlike(logeduser.organization, "%" + dataresponse?.keyword?.toString() + "%")
                    departmentkeyword = LibraryDepartment.findAllByOrganizationAndNameIlike(logeduser.organization, "%" + dataresponse?.keyword?.toString() + "%")
                }
                def bookList
                if (author1 &&  dataresponse.picked == 'Author') {
                     bookList = BooksAuthors.createCriteria().list {
                        'in'("organization", logeduser.organization)
                        and {
                        'in'('author',author1)
                        }
                    }?.book

                }
                else {
                     bookList = Book.createCriteria().list {
                        'in'("organization", logeduser.organization)
                        and {
                            or {
                                if (dataresponse?.keyword && (dataresponse?.picked == 'Call No' ))
                                    like('classificationno', "%" + dataresponse?.keyword?.toString() + "%")
                                if (dataresponse?.keyword && (dataresponse?.picked == 'Book Title'))
                                    like('title', "%" + dataresponse?.keyword?.toString() + "%")
                                if (publisher)
                                    'in'('publisher', publisher)
                                if (language)
                                    'in'('language', language)

                            }
                        }
                    }
                }


                def bookItem = []
                if (bookList) {
                    bookItem = BookItem.createCriteria().list {
                        'in'("organization", logeduser.organization)
                        and {
                            'in'("book", bookList)
                            if (dataresponse?.keyword && dataresponse?.picked == 'Accession No')
                                like('accession_number', dataresponse?.keyword?.toString() + "%")
                            if (dataresponse?.keyword && dataresponse?.picked == 'Keyword') {
                                like('keyword', "%" + dataresponse?.keyword?.toString() + "%")
                            }
                            if (dataresponse?.keyword && dataresponse?.picked == 'Subject') {
                                like('subject', "%" + dataresponse?.keyword?.toString() + "%")
                            }
                            if (dataresponse.selectedseries != 'ALL')
                                'in'('series', series)

                            if (dataresponse.selecteddepartment != 'ALL')
                                'in'("librarydepartment", department)
                        }
                    }
                }


                for (def i = count; i < loopend; i++) {
                    if (i < bookItem?.size()) {
                        def item = bookItem[i]
                        def book = item?.book
                        def arr = ""
                        def authors=BooksAuthors.findAllByOrganizationAndBook(logeduser.organization,book)?.author
                        for (author in authors)
                            arr = arr + author?.name + ","

//                        arr = arr.replaceAll(',$', "")
//                        arr = arr.replaceAll(",,", ",")
//                        if (dataresponse.picked == 'Author')
//                            if (dataresponse.keyword && !arr?.contains(dataresponse.keyword))
//                                continue

                        HashMap temp = new HashMap()
                        temp.put('srno', srno++)
                        temp.put('id', item?.id)
                        temp.put('display_order', item?.display_order)
                        temp.put('title', book?.title)
                        temp.put('cno', book?.classificationno)
                        temp.put('series', book?.series?.name)
                        temp.put('isbn', book?.isbn)
                        temp.put('edition', book?.edition)
                        temp.put('bookType', book?.booktype?.name)
                        temp.put('category', book?.bookcategory?.name)
                        if (book?.librarydepartment?.name) {
                            temp.put('department', book?.librarydepartment?.name)
                        }
                        temp.put('publisher', book?.publisher?.name)
                        temp.put('organization', book?.organization?.name)
                        temp.put('authors', arr)
                        temp.put('access_no', item?.accession_number)
                        temp.put('acc_no', item?.accession_number)
                        def is_img=false
                        if(item?.filename && item?.filepath)
                            is_img=true
                        temp.put('is_img', is_img)
                        temp.put('date_of_entry',item?.date_of_entry )
                        temp.put('keyword', item?.keyword)
                        temp.put('discount_cost', item?.price)
                        temp.put('copy_cost', item?.book_price)
                        temp.put('subject', item?.subject)
                        temp.put('medium', book?.language?.name)
                        temp.put('book_status', item.bookstatus?.displayname)
                        temp.put("book_statusId", item?.bookstatus?.id)
                        if (item?.borrowed_date)
                            temp.put("borrowed_date", dateToString(item?.borrowed_date))
                        if (item?.due_date)
                            temp.put("due_date", dateToString(item?.due_date))
                        if (item?.withdrawal_date)
                            temp.put("w_date", dateToString(item?.withdrawal_date))
                        temp.put("w_no", item?.withdrawal_no)
                        temp.put("p_place", item?.publication_place)
                        temp.put("rackNo", item?.rack?.rack_number)
                        temp.put("rackId", item?.rack?.id)
                        temp.put("block_no", item?.rack?.block_number)
                        temp.put("Keyword", item?.keyword)

                        BookLending bookLending = BookLending.findByBookitemAndIsactive(item, true)
                        def member = "--"
                        if (bookLending) {
                            member = Member.findByUserAndOrganization(bookLending.user, logeduser.organization)?.name
                            if (member == null)
                                member = Employee.findByUserAndOrganization(bookLending.user, logeduser.organization)?.name
                        }
                        temp.put('issuepersonname', member)

                        books.add(temp)
                    }
                }
                books?.sort { it?.display_order }

                ArrayList bookStatuslist = new ArrayList()
                def bookStatus = BookStatus.findAllByIsactive(true)
                for (ut in bookStatus) {
                    HashMap temp = new HashMap()
                    temp.put("bookStatus", ut.name)
                    temp.put("bookStatusIsactive", ut.isactive)
                    temp.put("book_statusId", ut.id)
                    temp.put("displayname", ut.displayname)
                    bookStatuslist.add(temp)
                }

                ArrayList rack_list = new ArrayList()
                def racks = Rack.findAllByIsactive(true)
                for (item in racks) {
                    HashMap temp = new HashMap()
                    temp.put("rackId", item.id)
                    temp.put("rackNo", item.rack_number)
                    temp.put("blockNo", item.block_number)
                    temp.put("capacity", item.capacity)
                    rack_list.add(temp)
                }
                hm.put('bookStatus', bookStatuslist)
                hm.put('racklist', rack_list)
                hm.put('bookList', books)
                hm.put('utype', logeduser?.usertype?.name)
                hm.put('organizationid', logeduser?.organization?.name)

                hm.put('msg', '200')
            }
            return hm
        }
        return hm
    }

    def getBookItemDataSeriesHOME(request, hm, ip) {
        println("in getBookItemDataSeriesHOME:")

        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println(dataresponse)


        def tenantid = dataresponse["tenantid"]
        TenantClient tenantclient = TenantClient.findById(tenantid)

        Tenants.withId(tenantclient.tenantid) {
            Organization org = Organization.findById(dataresponse.organizationid)

            def count = dataresponse.count
            if (count == 0) {
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
//                    Date today = new Date().plus(1);

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                Calendar c = Calendar.getInstance();
                c.setTime(new Date()); // Using today's date
                c.add(Calendar.DATE, 1); // Adding 5 days
//                    String output = sdf.format(c.getTime());
//                    System.out.println(output);
//
                Date todayWithZeroTime = formatter.parse(formatter.format(c.getTime()));
                OPACHits opacHits = OPACHits.findByOrganizationAndHitsdate(org, todayWithZeroTime)
                if (opacHits == null) {
                    opacHits = new OPACHits()
                    opacHits.hitsdate = todayWithZeroTime
                    opacHits.organization = org
                    opacHits.noofhits = 1
                    opacHits.save(flush: true, failOnError: true)

                } else {
                    opacHits.noofhits = opacHits.noofhits + 1
                    opacHits.save(flush: true, failOnError: true)
                }
            }

            def loopend = count + 500
            int srno = count + 1
            ArrayList books = new ArrayList()
            LibraryDepartment department = null
            Series series = null
            if (dataresponse.selecteddepartment != 'ALL')
                department = LibraryDepartment.findById(dataresponse.selecteddepartment)
            if (dataresponse.selectedseries != 'ALL')
                series = Series.findById(dataresponse.selectedseries)
            def author1
            if (dataresponse.picked == 'Author')
                author1 = Author.findAllByNameIlike("%" + dataresponse?.keyword?.toString() + "%")
            println(author1)
            def publisher
            if (dataresponse?.keyword && (dataresponse?.picked == 'Publisher'))
                publisher = Publisher.findAllByNameIlike("%" + dataresponse?.keyword?.toString() + "%")
            def language
            if (dataresponse?.keyword && (dataresponse?.picked == 'Medium' ))
                language = Language.findAllByOrganizationAndNameIlike(org,"%" + dataresponse?.keyword?.toString() + "%")
            def serieskeyword
            def booktypekeyword
            def categorykeyword
            def departmentkeyword

            def bookList
            if (author1 &&  dataresponse.picked == 'Author') {
                bookList = BooksAuthors.createCriteria().list {
                    'in'("organization",org)
                    and {
                        'in'('author',author1)
                    }
                }?.book

            }
            else {
                bookList = Book.createCriteria().list {
                    'in'("organization", org)
                    and {
                        or {
                            if (dataresponse?.keyword && (dataresponse?.picked == 'Call No' ))
                                like('classificationno', "%" + dataresponse?.keyword?.toString() + "%")
                            if (dataresponse?.keyword && (dataresponse?.picked == 'Book Title'))
                                like('title', "%" + dataresponse?.keyword?.toString() + "%")
                            if (publisher)
                                'in'('publisher', publisher)
                            if (language)
                                'in'('language', language)

                        }
                    }
                }
            }

            def bookItem = []
            if (bookList) {
                bookItem = BookItem.createCriteria().list {
                    'in'("organization", org)
                    and {
                        'in'("book", bookList)
                        if (dataresponse?.keyword && dataresponse?.picked == 'Accession No')
                            like('accession_number', dataresponse?.keyword?.toString() + "%")
                        if (dataresponse?.keyword && dataresponse?.picked == 'Keyword') {
                            like('keyword', "%" + dataresponse?.keyword?.toString() + "%")
                        }
                        if (dataresponse?.keyword && dataresponse?.picked == 'Subject') {
                            like('subject', "%" + dataresponse?.keyword?.toString() + "%")
                        }
                        if (dataresponse.selectedseries != 'ALL')
                            'in'('series', series)

                        if (dataresponse.selecteddepartment != 'ALL')
                            'in'("librarydepartment", department)
                    }
                }
            }


            for (def i = count; i < loopend; i++) {
                if (i < bookItem?.size()) {
                    def item = bookItem[i]
                    def book = item?.book
                    def arr = ""
                    def authors=BooksAuthors.findAllByOrganizationAndBook(org,book)?.author
                    for (author in authors)
                        arr = arr + author?.name + ","

                    HashMap temp = new HashMap()
                    temp.put('srno', srno++)
                    temp.put('id', item?.id)
                    temp.put('display_order', item?.display_order)
                    temp.put('title', book?.title)
                    temp.put('cno', book?.classificationno)
                    temp.put('series', book?.series?.name)
                    temp.put('isbn', book?.isbn)
                    temp.put('edition', book?.edition)
                    temp.put('bookType', book?.booktype?.name)
                    temp.put('category', book?.bookcategory?.name)
                    if (book?.librarydepartment?.name) {
                        temp.put('department', book?.librarydepartment?.name)
                    }
                    temp.put('publisher', book?.publisher?.name)
                    temp.put('organization', book?.organization?.name)
                    temp.put('authors', arr)
                    temp.put('access_no', item?.accession_number)
                    temp.put('acc_no', item?.accession_number)
                    def is_img=false
                    if(item?.filename && item?.filepath)
                        is_img=true
                    temp.put('is_img', is_img)
                    temp.put('date_of_entry',item?.date_of_entry )
                    temp.put('keyword', item?.keyword)
                    temp.put('discount_cost', item?.price)
                    temp.put('copy_cost', item?.book_price)
                    temp.put('subject', item?.subject)
                    temp.put('medium', book?.language?.name)
                    temp.put('book_status', item.bookstatus?.displayname)
                    temp.put("book_statusId", item?.bookstatus?.id)
                    if (item?.borrowed_date)
                        temp.put("borrowed_date", dateToString(item?.borrowed_date))
                    if (item?.due_date)
                        temp.put("due_date", dateToString(item?.due_date))
                    if (item?.withdrawal_date)
                        temp.put("w_date", dateToString(item?.withdrawal_date))
                    temp.put("w_no", item?.withdrawal_no)
                    temp.put("p_place", item?.publication_place)
                    temp.put("rackNo", item?.rack?.rack_number)
                    temp.put("rackId", item?.rack?.id)
                    temp.put("block_no", item?.rack?.block_number)
                    temp.put("Keyword", item?.keyword)

                    BookLending bookLending = BookLending.findByBookitemAndIsactive(item, true)
                    def member = "--"
                    if (bookLending) {
                        member = Member.findByUserAndOrganization(bookLending.user, org)?.name
                        if (member == null)
                            member = Employee.findByUserAndOrganization(bookLending.user, org)?.name
                    }
                    temp.put('issuepersonname', member)

                    books.add(temp)
                }
            }

            ArrayList bookStatuslist = new ArrayList()
            def bookStatus = BookStatus.findAllByIsactive(true)
            for (ut in bookStatus) {
                HashMap temp = new HashMap()
                temp.put("bookStatus", ut.name)
                temp.put("bookStatusIsactive", ut.isactive)
                temp.put("book_statusId", ut.id)
                temp.put("displayname", ut.displayname)
                bookStatuslist.add(temp)
            }

            ArrayList rack_list = new ArrayList()
            def racks = Rack.findAllByIsactive(true)
            for (item in racks) {
                HashMap temp = new HashMap()
                temp.put("rackId", item.id)
                temp.put("rackNo", item.rack_number)
                temp.put("blockNo", item.block_number)
                temp.put("capacity", item.capacity)
                rack_list.add(temp)
            }
            hm.put('bookStatus', bookStatuslist)
            hm.put('racklist', rack_list)
            hm.put('bookList', books)
            hm.put('utype', "Student")
            hm.put('organizationid', org?.name)

            hm.put('msg', '200')

            return hm
        }
    }




    def getSeriesBookCount(request, hm,ip){
        println("in getSeriesBookCount:")

        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        String purpose = "getSeriesBookCount"
        auth.checkauth(token, uid, hm, url, request, purpose)

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                User logeduser = User.findByUsername(log.username)
                def series=null
                if(dataresponse.selectedseries!='ALL')
                    series=Series.findById(dataresponse.selectedseries)
                else{
                    LibraryConfiguration libraryConfiguration1=LibraryConfiguration.findByIsactiveAndOrganizationAndName(true,logeduser.organization,"Periodicals_In_OPAC")

                    if(libraryConfiguration1?.value=='NO')
                    {
                        println("j dfssdjhfjksh djfh sjd hfjsh djfhj sdfj jsdhfj ")
                        BookClassification bookClassification=BookClassification.findByNameAndOrganization("Book",logeduser.organization)
                        series=Series.findAllByOrganizationAndBookclassification(logeduser.organization,bookClassification)
                    }
                }




                def seriesbookcount = 0

                if(series) {
                    seriesbookcount = BookItem.createCriteria().list {
                        'in'("organization", logeduser.organization)
                        and {
                            'in'('series', series)
                        }
                    }?.size()
                }
                hm.put('seriesbookcount',seriesbookcount)
                hm.put('msg','200')
            }
            return hm
        }
        return hm
    }
    def getSeriesBookCountHOME(request, hm,ip){
        println("in getSeriesBookCount:")

        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")


        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        def tenantid = dataresponse["tenantid"]
        TenantClient tenantclient = TenantClient.findById(tenantid)

            Tenants.withId(tenantclient.tenantid) {
                Organization org=Organization.findById(dataresponse.organizationid)
                def series=null
                if(dataresponse.selectedseries!='ALL')
                    series=Series.findById(dataresponse.selectedseries)
                else{
                    LibraryConfiguration libraryConfiguration1=LibraryConfiguration.findByIsactiveAndOrganizationAndName(true,org,"Periodicals_In_OPAC")

                    if(libraryConfiguration1?.value=='NO')
                    {
                        println("j dfssdjhfjksh djfh sjd hfjsh djfhj sdfj jsdhfj ")
                        BookClassification bookClassification=BookClassification.findByNameAndOrganization("Book",org)
                        series=Series.findAllByOrganizationAndBookclassification(org,bookClassification)
                    }
                }




                def seriesbookcount = 0

                if(series) {
                    seriesbookcount = BookItem.createCriteria().list {
                        'in'("organization", org)
                        and {
                            'in'('series', series)
                        }
                    }?.size()
                }
                hm.put('seriesbookcount',seriesbookcount)
                hm.put('msg','200')
            }
            return hm

    }

    def getDepartmentBookCount(request, hm,ip){
        println("in getDepartmentBookCount:")

        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        String purpose = "getDepartmentBookCount"
        auth.checkauth(token, uid, hm, url, request, purpose)

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                User logeduser = User.findByUsername(log.username)
                LibraryDepartment department =null
                if(dataresponse.selecteddepartment!='ALL')
                    department=LibraryDepartment.findById(dataresponse.selecteddepartment)
                def departmentbook = Book.createCriteria().list {
                    'in'("organization", logeduser.organization)
                    and{
                        if(dataresponse.selecteddepartment != 'ALL' && department)
                            'in'("librarydepartment", department)
                    }
                }

                def departmentbookcount = 0
                if(departmentbook) {
                    departmentbookcount = BookItem.createCriteria().list {
                        'in'("organization", logeduser.organization)
                        and {
                            'in'('book', departmentbook)
                        }
                    }?.size()
                }

                hm.put('departmentbookcount',departmentbookcount)
                hm.put('msg','200')
            }
            return hm
        }
        return hm
    }
    def getDepartmentBookCountHOME(request, hm,ip){
        println("in getDepartmentBookCountHOME:")

        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")


        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        String purpose = "getDepartmentBookCountHOME"

        def tenantid = dataresponse["tenantid"]
        TenantClient tenantclient = TenantClient.findById(tenantid)

            Tenants.withId(tenantclient.tenantid) {
                Organization org=Organization.findById(dataresponse.organizationid)
                LibraryDepartment department =null
                if(dataresponse.selecteddepartment!='ALL')
                    department=LibraryDepartment.findById(dataresponse.selecteddepartment)
                def departmentbook = Book.createCriteria().list {
                    'in'("organization", org)
                    and{
                        if(dataresponse.selecteddepartment != 'ALL' && department)
                            'in'("librarydepartment", department)
                    }
                }

                def departmentbookcount = 0
                if(departmentbook) {
                    departmentbookcount = BookItem.createCriteria().list {
                        'in'("organization", org)
                        and {
                            'in'('book', departmentbook)
                        }
                    }?.size()
                }

                hm.put('departmentbookcount',departmentbookcount)
                hm.put('msg','200')
            }
            return hm

    }
}
