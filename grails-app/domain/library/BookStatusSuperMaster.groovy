package library
class BookStatusSuperMaster
{
    String name   //Available/Reserved/Loaned/Lost/Write-Off
    String displayname
    boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
        displayname nullable:true
    }
    static mapping = {
        isactive defaultValue: true
    }
}
