package library
import grails.gorm.MultiTenant
class NotificationMethod implements MultiTenant<NotificationMethod>
{
    String name   //Email/SMS/Postal
    String from_user
    String from_username
    String from_password
    boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
        name nullable: true
        from_user nullable: true
        from_username nullable: true
        from_password nullable: true
    }
    static mapping = {
        isactive defaultValue: true
    }
}
