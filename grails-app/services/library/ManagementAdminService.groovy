package library
import grails.gorm.multitenancy.Tenants
import groovy.json.JsonSlurper
import grails.gorm.transactions.Transactional

import java.text.SimpleDateFormat

@Transactional
class ManagementAdminService {

    def serviceMethod() {

    }

    def reports(request, ip, hm) {
        println "i am in management reports"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String purpose = "Getting reports management  Menu."
        //auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList routerLinkList = new ArrayList()
                int col_size = 3
                HashMap temp = new HashMap()
                temp.put('name', 'management-accession-report')
                temp.put('textval', 'Accession Report')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'A')
                temp.put('flex', col_size)

                routerLinkList.add(temp)

//                temp = new HashMap()
//                temp.put('name', 'management-over-due')
//                temp.put('textval', 'Over Due Member Report')
//                temp.put('icon', 'mdi-alpha-e-circle')
//                temp.put('icontext', 'ODM')
//                temp.put('flex', col_size)
//
//                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'management-daily-transaction')
                temp.put('textval', 'Daily Check-In/Check-Out Report')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'DR')
                temp.put('flex', col_size)

                routerLinkList.add(temp)
//
                temp = new HashMap()
                temp.put('name', 'management-memberwise-report')
                temp.put('textval', 'Member Wise Library Utilization Report')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'MWU')
                temp.put('flex', col_size)

                routerLinkList.add(temp)

//                temp = new HashMap()
//                temp.put('name', 'title-report')
//                temp.put('textval', 'Tttle Report')
//                temp.put('icon', 'mdi-alpha-e-circle')
//                temp.put('icontext', 'TR')
//                temp.put('flex', col_size)
//
//                routerLinkList.add(temp)

                hm.put('routerLinkList', routerLinkList)


                return hm

            }
        }
        return hm
    }

    def getOrg(request, ip, hm) {
        println "i am in management reports"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String purpose = "Getting reports management  Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList orgList = new ArrayList()

                def organizations = Organization.findAllByIsactive(true)
                for(org in organizations){
                    HashMap temp = new HashMap()
                    temp.put('name',org?.name)
                    temp.put('id',org?.id)
                    orgList.add(temp)
                }
                HashMap all = new HashMap()
                all.put('name','ALL')
                all.put('id','ALL')
                orgList.add(0,all)
                hm.put('orgList', orgList)


                return hm

            }
        }
        return hm
    }

    def accessionReport(request, ip, hm) {
        println "i am in  management accession Report"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
       // println dataresponse
        String purpose = "Getting reports management accessionReport."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def books
if(dataresponse?.org == 'ALL') {
     books = Book.findAll()
}else {
    Organization org = Organization.findById(dataresponse?.org)
    println('org--------'+org)
     books = Book.findAllByOrganization(org)
}
              //  println('books========='+books.size())
    def only_one = false

    ArrayList report_data = new ArrayList()
    ArrayList table_header_data = new ArrayList()
    ArrayList csv_header_data = new ArrayList()
    int srno = 1;
    for (book in books) {
        def items = BookItem.findAllByBook(book)
        for (bookItem in items) {
            HashMap temp = new HashMap()
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            String strDate = formatter.format(bookItem?.creation_date);

            temp.put('srno', srno++)

            if (dataresponse?.AccessionNo) {
                temp.put('acc_no', bookItem?.accession_number)

                if (only_one == false) {
                    HashMap table_header = new HashMap()
                    table_header.put('text', 'Accession No')
                    table_header.put('value', 'acc_no')
                    table_header_data.add(table_header)

                    HashMap csv_header = new HashMap()
                    csv_header.put('title', 'Accession No')
                    csv_header.put('dataKey', 'acc_no')
                    csv_header_data.add(csv_header)
                }

            }

            if (dataresponse?.EntryDate) {
                temp.put('entry_date', strDate)

                if (only_one == false) {
                    HashMap table_header = new HashMap()
                    table_header.put('text', 'Date of Entry')
                    table_header.put('value', 'entry_date')
                    table_header_data.add(table_header)

                    HashMap csv_header = new HashMap()
                    csv_header.put('title', 'Date of Entry')
                    csv_header.put('dataKey', 'entry_date')
                    csv_header_data.add(csv_header)
                }

            }

            if (dataresponse?.Title) {
                temp.put('title', bookItem?.book?.title)

                if (only_one == false) {
                    HashMap table_header = new HashMap()
                    table_header.put('text', 'Title')
                    table_header.put('value', 'title')
                    table_header_data.add(table_header)

                    HashMap csv_header = new HashMap()
                    csv_header.put('title', 'Title')
                    csv_header.put('dataKey', 'title')
                    csv_header_data.add(csv_header)
                }

            }

            if (dataresponse?.Edition) {
                temp.put('edition', bookItem?.book?.edition)

                if (only_one == false) {
                    HashMap table_header = new HashMap()
                    table_header.put('text', 'Edition')
                    table_header.put('value', 'edition')
                    table_header_data.add(table_header)

                    HashMap csv_header = new HashMap()
                    csv_header.put('title', 'Edition')
                    csv_header.put('dataKey', 'edition')
                    csv_header_data.add(csv_header)
                }

            }

            if (dataresponse?.Publisher) {
                temp.put('pub_year', bookItem?.book?.publisher?.name + ',' + bookItem?.publication_year)

                if (only_one == false) {
                    HashMap table_header = new HashMap()
                    table_header.put('text', 'Publisher')
                    table_header.put('value', 'pub_year')
                    table_header_data.add(table_header)

                    HashMap csv_header = new HashMap()
                    csv_header.put('title', 'Publisher')
                    csv_header.put('dataKey', 'pub_year')
                    csv_header_data.add(csv_header)
                }

            }

            if (dataresponse?.org) {
                temp.put('org', bookItem?.book?.organization?.name)

                if (only_one == false) {
                    HashMap table_header = new HashMap()
                    table_header.put('text', 'Organization')
                    table_header.put('value', 'org')
                    table_header_data.add(table_header)

                    HashMap csv_header = new HashMap()
                    csv_header.put('title', 'Organization')
                    csv_header.put('dataKey', 'org')
                    csv_header_data.add(csv_header)
                }

            }

//                        if(dataresponse.data?.AccessionNo) {
//                            temp.put('publication', bookItem?.book?.publisher?.name)
//                        }

            if (dataresponse?.Pages) {
                temp.put('pages', bookItem?.numberofpages)

                if (only_one == false) {
                    HashMap table_header = new HashMap()
                    table_header.put('text', 'Pages')
                    table_header.put('value', 'pages')
                    table_header_data.add(table_header)

                    HashMap csv_header = new HashMap()
                    csv_header.put('title', 'Pages')
                    csv_header.put('dataKey', 'pages')
                    csv_header_data.add(csv_header)
                }

            }

            if (dataresponse?.BillNo) {
                temp.put('bill_no', bookItem?.bill_number)

                if (only_one == false) {
                    HashMap table_header = new HashMap()
                    table_header.put('text', 'Bill No')
                    table_header.put('value', 'bill_no')
                    table_header_data.add(table_header)

                    HashMap csv_header = new HashMap()
                    csv_header.put('title', 'Bill No')
                    csv_header.put('dataKey', 'bill_no')
                    csv_header_data.add(csv_header)
                }

            }

            if (dataresponse?.BookCost) {
                temp.put('cost', bookItem?.book_price)

                if (only_one == false) {
                    HashMap table_header = new HashMap()
                    table_header.put('text', 'Book Cost')
                    table_header.put('value', 'cost')
                    table_header_data.add(table_header)

                    HashMap csv_header = new HashMap()
                    csv_header.put('title', 'Book Cost')
                    csv_header.put('dataKey', 'cost')
                    csv_header_data.add(csv_header)
                }

            }

            if (dataresponse?.BookDiscount) {
                temp.put('discount', bookItem?.discount_percentage)

                if (only_one == false) {
                    HashMap table_header = new HashMap()
                    table_header.put('text', 'Discount %')
                    table_header.put('value', 'discount')
                    table_header_data.add(table_header)

                    HashMap csv_header = new HashMap()
                    csv_header.put('title', 'Discount %')
                    csv_header.put('dataKey', 'discount')
                    csv_header_data.add(csv_header)
                }

            }

            if (dataresponse?.netAmt) {
                temp.put('purchase_price', bookItem?.price)

                if (only_one == false) {
                    HashMap table_header = new HashMap()
                    table_header.put('text', 'Book Net Amount')
                    table_header.put('value', 'purchase_price')
                    table_header_data.add(table_header)

                    HashMap csv_header = new HashMap()
                    csv_header.put('title', 'Book Net Amount')
                    csv_header.put('dataKey', 'purchase_price')
                    csv_header_data.add(csv_header)
                }

            }

            if (dataresponse?.BookFormat) {
                temp.put('book_format', bookItem?.bookformat?.name)

                if (only_one == false) {
                    HashMap table_header = new HashMap()
                    table_header.put('text', 'Book Format')
                    table_header.put('value', 'book_format')
                    table_header_data.add(table_header)

                    HashMap csv_header = new HashMap()
                    csv_header.put('title', 'Book Format')
                    csv_header.put('dataKey', 'book_format')
                    csv_header_data.add(csv_header)
                }

            }

            if (dataresponse?.BookType) {
                temp.put('book_type', book?.booktype?.name)

                if (only_one == false) {
                    HashMap table_header = new HashMap()
                    table_header.put('text', 'Book Type')
                    table_header.put('value', 'book_type')
                    table_header_data.add(table_header)

                    HashMap csv_header = new HashMap()
                    csv_header.put('title', 'Book Type')
                    csv_header.put('dataKey', 'book_type')
                    csv_header_data.add(csv_header)
                }

            }

            if (dataresponse?.Category) {
                temp.put('book_cat', book?.bookcategory?.name)

                if (only_one == false) {
                    HashMap table_header = new HashMap()
                    table_header.put('text', 'Book Category')
                    table_header.put('value', 'book_cat')
                    table_header_data.add(table_header)

                    HashMap csv_header = new HashMap()
                    csv_header.put('title', 'Book Category')
                    csv_header.put('dataKey', 'book_cat')
                    csv_header_data.add(csv_header)
                }

            }

            if (dataresponse?.Volume) {
                temp.put('volume', book?.volume)

                if (only_one == false) {
                    HashMap table_header = new HashMap()
                    table_header.put('text', 'Volume')
                    table_header.put('value', 'volume')
                    table_header_data.add(table_header)

                    HashMap csv_header = new HashMap()
                    csv_header.put('title', 'Volume')
                    csv_header.put('dataKey', 'volume')
                    csv_header_data.add(csv_header)
                }

            }

            if (dataresponse?.ISBN) {
                temp.put('isbn', book?.isbn)

                if (only_one == false) {
                    HashMap table_header = new HashMap()
                    table_header.put('text', 'ISBN')
                    table_header.put('value', 'isbn')
                    table_header_data.add(table_header)

                    HashMap csv_header = new HashMap()
                    csv_header.put('title', 'ISBN')
                    csv_header.put('dataKey', 'isbn')
                    csv_header_data.add(csv_header)
                }

            }
//                        temp.put('allcost',bookItem?.book_price+','+bookItem?.discount_percentage+'%,'+bookItem?.price)

            if (dataresponse?.Author) {
                def authorArr = bookItem?.book?.author
                ArrayList arr = new ArrayList()
                for (author in authorArr) {
                    def authorName = Author.findById(author?.id)
                    arr.add(authorName?.name)
                }
                temp.put('authors', arr)

                if (only_one == false) {
                    HashMap table_header = new HashMap()
                    table_header.put('text', 'Authors')
                    table_header.put('value', 'authors')
                    table_header_data.add(table_header)

                    HashMap csv_header = new HashMap()
                    csv_header.put('title', 'Authors')
                    csv_header.put('dataKey', 'authors')
                    csv_header_data.add(csv_header)
                }

            }
            only_one = true
            report_data.add(temp)
        }

    }
    hm.put('report_data', report_data)
    hm.put('csv_header_data', csv_header_data)
    hm.put('table_header_data', table_header_data)
    return hm

            }
        }
        return hm
    }

    def overdueData(request, ip, hm) {
        println "i am in overdueData Report"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        ArrayList table_header_data = new ArrayList()
        ArrayList csv_header_data = new ArrayList()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        String purpose = "Getting reports  overdueData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList fetchdata = new ArrayList()

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)

//                BookCategory book_cat = BookCategory.findById(dataresponse.dept_id)
                Program book_cat = Program.findById(dataresponse.dept_id)
                println('book_cat==============='+book_cat)
                UserType user_type = UserType.findById(dataresponse.user_id)
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-M-yyyy");
//                String dateInString = "15-10-2015";
                Date date1 = sdf1.parse(dataresponse.firstDate);
                Date date2 = sdf1.parse(dataresponse.lastDate);

                def bl = BookLending.createCriteria()
                def bl_data
if(dataresponse?.org == 'ALL'){
    print('in all oeg+++++++++++')
     bl_data = bl.list {
        eq("isactive", true)
        and {
//                        between("borrowing_date", date1,date2)
            between("due_date", date1, date2)
        }
        lt("due_date", date2)

    }
}else {
    Organization org = Organization.findById(dataresponse?.org)
     bl_data = bl.list {
        eq("isactive", true)
        eq("organization", org)
        and {
//                        between("borrowing_date", date1,date2)
            between("due_date", date1, date2)
        }
        lt("due_date", date2)

    }
}
                println('bl_data----------' + bl_data)
                def only_one = false
                int srno = 1
                for (item in bl_data) {
                    HashMap temp = new HashMap()
//                    temp.put('srno',srno++)

                    if (dataresponse?.colData?.AccessionNo) {
                        temp.put('acc_no', item?.bookitem?.accession_number)
                        if (only_one == false) {
                            HashMap table_header = new HashMap()
                            table_header.put('text', 'Accession No')
                            table_header.put('value', 'acc_no')
                            table_header_data.add(table_header)

                            HashMap csv_header = new HashMap()
                            csv_header.put('title', 'Accession No')
                            csv_header.put('dataKey', 'acc_no')
                            csv_header_data.add(csv_header)
                        }
                    }

                    if (dataresponse?.colData?.org) {
                        temp.put('org', item?.bookitem?.book?.organization?.name)
                        if (only_one == false) {
                            HashMap table_header = new HashMap()
                            table_header.put('text', 'Organization')
                            table_header.put('value', 'org')
                            table_header_data.add(table_header)

                            HashMap csv_header = new HashMap()
                            csv_header.put('title', 'Organization')
                            csv_header.put('dataKey', 'org')
                            csv_header_data.add(csv_header)
                        }
                    }

                    if (dataresponse?.colData?.Category) {
                        temp.put('cat', item?.bookitem?.book?.bookcategory?.name)
                        if (only_one == false) {
                            HashMap table_header = new HashMap()
                            table_header.put('text', 'Category')
                            table_header.put('value', 'cat')
                            table_header_data.add(table_header)

                            HashMap csv_header = new HashMap()
                            csv_header.put('title', 'Category')
                            csv_header.put('dataKey', 'cat')
                            csv_header_data.add(csv_header)
                        }
                    }

                    if (dataresponse?.colData?.Title) {
                        temp.put('title', item?.bookitem?.book?.title)
                        if (only_one == false) {
                            HashMap table_header = new HashMap()
                            table_header.put('text', 'Title')
                            table_header.put('value', 'title')
                            table_header_data.add(table_header)

                            HashMap csv_header = new HashMap()
                            csv_header.put('title', 'Title')
                            csv_header.put('dataKey', 'title')
                            csv_header_data.add(csv_header)
                        }
                    }

                    if (dataresponse?.colData?.Publisher) {
                        temp.put('publisher', item?.bookitem?.book?.publisher?.name)
                        if (only_one == false) {
                            HashMap table_header = new HashMap()
                            table_header.put('text', 'Publisher')
                            table_header.put('value', 'publisher')
                            table_header_data.add(table_header)

                            HashMap csv_header = new HashMap()
                            csv_header.put('title', 'Publisher')
                            csv_header.put('dataKey', 'publisher')
                            csv_header_data.add(csv_header)
                        }
                    }
                    def mem
                    UserType member = UserType.findByName('Member')
                    UserType employee = UserType.findByName('Employee')

                    println('item?.user?.usertype?.name==========>'+item?.user?.usertype?.name)
                    println('member?.name==========>'+member?.name)
                    if (item?.user?.usertype?.name == member?.name) {
                        mem = Member.findByUser(item?.user)

                        if (dataresponse?.colData?.memberName) {
                            temp.put('member_name', mem?.name)
                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Name')
                                table_header.put('value', 'member_name')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Name')
                                csv_header.put('dataKey', 'member_name')
                                csv_header_data.add(csv_header)
                            }
                        }
                    }
                    def emp
                    if (item?.user?.usertype?.name == employee?.name) {
                        emp = Employee.findByUser(item?.user)

                        if (dataresponse?.colData?.memberName) {
                            temp.put('member_name', emp?.name)
                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Name')
                                table_header.put('value', 'member_name')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Name')
                                csv_header.put('dataKey', 'member_name')
                                csv_header_data.add(csv_header)
                            }
                        }
                    }
//                    Employee emp = Employee.findByUser(item?.user)
//                    temp.put('member_name',emp?.name)
                    SimpleDateFormat sdformat = new SimpleDateFormat("dd-MM-yyyy");
                    String d1 = sdformat.format(item?.borrowing_date);
                    String d2 = sdformat.format(item?.due_date);

                    if (dataresponse?.colData?.startDate) {
                        temp.put('startdate', d1)
                        if (only_one == false) {
                            HashMap table_header = new HashMap()
                            table_header.put('text', 'Start Date')
                            table_header.put('value', 'startdate')
                            table_header_data.add(table_header)

                            HashMap csv_header = new HashMap()
                            csv_header.put('title', 'Start Date')
                            csv_header.put('dataKey', 'startdate')
                            csv_header_data.add(csv_header)
                        }
                    }

                    if (dataresponse?.colData?.dueDate) {
                        temp.put('duedate', d2)
                        if (only_one == false) {
                            HashMap table_header = new HashMap()
                            table_header.put('text', 'Due Date')
                            table_header.put('value', 'duedate')
                            table_header_data.add(table_header)

                            HashMap csv_header = new HashMap()
                            csv_header.put('title', 'Due Date')
                            csv_header.put('dataKey', 'duedate')
                            csv_header_data.add(csv_header)
                        }
                    }

                    def authorArr = item?.bookitem?.book?.author
                    ArrayList arr = new ArrayList()
                    for (author in authorArr) {
                        def authorName = Author.findById(author?.id)
                        arr.add(authorName?.name)
                    }

                    if (dataresponse?.colData?.AccessionNo) {
                        temp.put('authors', arr)
                        if (only_one == false) {
                            HashMap table_header = new HashMap()
                            table_header.put('text', 'Author')
                            table_header.put('value', 'authors')
                            table_header_data.add(table_header)

                            HashMap csv_header = new HashMap()
                            csv_header.put('title', 'Author')
                            csv_header.put('dataKey', 'authors')
                            csv_header_data.add(csv_header)
                        }
                    }


                    def lastWeek = item?.due_date;
                    def today = new Date()

                    if (dataresponse?.colData?.lateDays) {
                        temp.put('lateDays', daysBetween(lastWeek, today))
                        if (only_one == false) {
                            HashMap table_header = new HashMap()
                            table_header.put('text', 'Late Days')
                            table_header.put('value', 'lateDays')
                            table_header_data.add(table_header)

                            HashMap csv_header = new HashMap()
                            csv_header.put('title', 'Late Days')
                            csv_header.put('dataKey', 'lateDays')
                            csv_header_data.add(csv_header)
                        }
                    }
                    only_one = true

    if (item?.user?.usertype?.name == member?.name) {
        if (book_cat?.name == mem?.program.name && user_type?.name == item?.user?.usertype?.name) {
            temp.put('srno', srno++)
            fetchdata.add(temp)
        }
    }

    if (item?.user?.usertype?.name == employee?.name) {
        if (book_cat?.name == emp?.program.name && user_type?.name == item?.user?.usertype?.name) {
            temp.put('srno', srno++)
            fetchdata.add(temp)
        }
    }


//                    if(book_cat?.name == item?.user && user_type?.name == item?.user?.usertype?.name ){
////                    if(book_cat?.name == item?.bookitem?.book?.bookcategory?.name && user_type?.name == item?.user?.usertype?.name ){
//                        temp.put('srno',srno++)
//                        fetchdata.add(temp)
//                    }
                }

                hm.put('dept', book_cat?.name)
                hm.put('usertype', user_type?.name)

            }

            hm.put('csv_header_data', csv_header_data)
            hm.put('table_header_data', table_header_data)
            hm.put('overdue_data', fetchdata)
            return hm
//            }
        }
        return hm
    }

    def daysBetween(def startDate, def endDate) {
        use(groovy.time.TimeCategory) {
            def duration = endDate - startDate
            return duration.days
        }
    }

    def getdeptData(request, ip, hm) {
        println "i am in getdeptData "

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String purpose = "Getting getdeptData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)

                ArrayList dept_data = new ArrayList()
                ArrayList usertype_data = new ArrayList()
                ArrayList orgList = new ArrayList()

                def dept = Program.findAll()
                for (item in dept) {
                    HashMap temp = new HashMap()
                    temp.put('name', item?.name)
                    temp.put('id', item?.id)
                    dept_data.add(temp)
                }

                ArrayList dept_data_all =  new ArrayList()

                HashMap temp1 = new HashMap()
                temp1.put('name', 'All')
                temp1.put('id', 'All')
                dept_data_all.add(0,temp1)
                for (item in dept) {
                    HashMap temp = new HashMap()
                    temp.put('name', item?.name)
                    temp.put('id', item?.id)
                    dept_data_all.add(temp)
                }


                def usertype = UserType.findAll()
                for (item in usertype) {
                    HashMap temp = new HashMap()
                    temp.put('name', item?.name)
                    temp.put('id', item?.id)
                    usertype_data.add(temp)
                }

                def organizations = Organization.findAllByIsactive(true)
                for(org in organizations){
                    HashMap temp = new HashMap()
                    temp.put('name',org?.name)
                    temp.put('id',org?.id)
                    orgList.add(temp)
                }
                HashMap all = new HashMap()
                all.put('name','ALL')
                all.put('id','ALL')
                orgList.add(0,all)
                hm.put('orgList', orgList)
                hm.put('dept_data', dept_data)
                hm.put('usertype_data', usertype_data)
                hm.put('dept_data_all', dept_data_all)
            }
        }
        return hm
    }

    def dailyData(request, ip, hm) {
        println "i am in dailyData Report"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

//        ArrayList table_header_data = new ArrayList()
//        ArrayList csv_header_data = new ArrayList()

        HashSet<HashMap> table_header_data = new HashSet<HashMap>()
        HashSet<HashMap> csv_header_data = new HashSet<HashMap>()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        HashSet<HashMap> set = new HashSet<HashMap>()


        String purpose = "Getting reports  dailyData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList fetchdata = new ArrayList()

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)

                BookCategory book_cat = BookCategory.findById(dataresponse.dept_id)
                UserType user_type = UserType.findById(dataresponse.user_id)
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-M-yyyy");
//                String dateInString = "15-10-2015";
                Date date1 = sdf1.parse(dataresponse.firstDate);
                Date date2 = sdf1.parse(dataresponse.lastDate);
                def bl = BookLending.createCriteria()
                def bl_data
                if(dataresponse?.org == 'ALL'){
                     bl_data = bl.list {
                        or {
                            between("borrowing_date", date1, date2)
                            between("due_date", date1, date2)
                        }
//                    lt("due_date", date2)

                    }
                }else{
                    Organization org = Organization.findById(dataresponse?.org)
                     bl_data = bl.list {
                        eq("organization", org)
                        or {
                            between("borrowing_date", date1, date2)
                            between("due_date", date1, date2)
                        }
//                    lt("due_date", date2)

                    }
                }

                println('bl_data----------' + bl_data)
                def only_one = false
                int srno = 1
                def dept = Program.findAll()
//                for(item in bl_data){
                for (department in dept) {

                    HashMap temp = new HashMap()
                    int StudOutBooks = 1
                    int StudInBooks = 1
                    int StaffInBooks = 1
                    int StaffOutBooks = 1


                    for (item in bl_data) {
                        def user1
//                    for(department in dept){

                        UserType emp_ut = UserType.findByName('Employee')
                        UserType mem_ut = UserType.findByName('Member')
                        if (item?.user?.usertype.name == mem_ut?.name) {
                            user1 = Member.findByUser(item?.user)
                        } else {
                            user1 = Employee.findByUser(item?.user)
                        }

                        if (user1?.program == department) {
//                                    && !table_header_data.isEmpty()
                            if (only_one == false) {
                                temp.put('department', department?.name)

//                                HashMap table_header = new HashMap()
//                                table_header.put('text', 'Department')
//                                table_header.put('value', 'department')
//                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Department')
                                csv_header.put('dataKey', 'department')
                                csv_header_data.add(csv_header)
                            }

                            println('member---------')
                            println(dataresponse?.colData?.StaffOutBooks && item?.user?.usertype.name == mem_ut?.name)
                            if (dataresponse?.colData?.StudOutBooks && item?.user?.usertype.name == mem_ut?.name) {
                                temp.put('StudOutBooks', StudOutBooks++)
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Student Out Books')
                                table_header.put('value', 'StudOutBooks')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Student Out Books')
                                csv_header.put('dataKey', 'StudOutBooks')
                                csv_header_data.add(csv_header)
                            } else {
                                temp.put('StudOutBooks', StudOutBooks)
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Student Out Books')
                                table_header.put('value', 'StudOutBooks')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Student Out Books')
                                csv_header.put('dataKey', 'StudOutBooks')
                                csv_header_data.add(csv_header)
                            }


                            if (dataresponse?.colData?.StudInBooks && item?.user?.usertype.name == mem_ut?.name) {
                                if (item?.isactive == false) {
                                    temp.put('StudInBooks', StudInBooks++)
                                } else {
                                    temp.put('StudInBooks', 0)
                                }
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Student In Books')
                                table_header.put('value', 'StudInBooks')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Student In Books')
                                csv_header.put('dataKey', 'StudInBooks')
                                csv_header_data.add(csv_header)
                            } else if (dataresponse?.colData?.StudInBooks) {
                                temp.put('StudInBooks', 0)
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Student In Books')
                                table_header.put('value', 'StudInBooks')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Student In Books')
                                csv_header.put('dataKey', 'StudInBooks')
                                csv_header_data.add(csv_header)
                            }

                            if (dataresponse?.colData?.StaffOutBooks && item?.user?.usertype.name == emp_ut?.name) {
                                temp.put('StaffOutBooks', StaffOutBooks++)
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Staff Out Books')
                                table_header.put('value', 'StaffOutBooks')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Staff Out Books')
                                csv_header.put('dataKey', 'StaffOutBooks')
                                csv_header_data.add(csv_header)
                            } else {
                                temp.put('StaffOutBooks', 0)
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Staff Out Books')
                                table_header.put('value', 'StaffOutBooks')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Staff Out Books')
                                csv_header.put('dataKey', 'StaffOutBooks')
                                csv_header_data.add(csv_header)
                            }

                            if (dataresponse?.colData?.StaffInBooks && item?.user?.usertype.name == emp_ut?.name) {
                                if (item?.isactive == false) {
                                    temp.put('StaffInBooks', StaffInBooks++)
                                } else {
                                    temp.put('StaffInBooks', 0)
                                }
//                                if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Staff In Books')
                                table_header.put('value', 'StaffInBooks')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Staff In Books')
                                csv_header.put('dataKey', 'StaffInBooks')
                                csv_header_data.add(csv_header)
//                                }
                            } else if (dataresponse?.colData?.StaffInBooks) {
                                temp.put('StaffInBooks', 0)
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Staff In Books')
                                table_header.put('value', 'StaffInBooks')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Staff In Books')
                                csv_header.put('dataKey', 'StaffInBooks')
                                csv_header_data.add(csv_header)
                            }


                            if (dataresponse?.colData?.org) {
                                temp.put('org', item?.bookitem?.book?.organization?.name)
                                    HashMap table_header = new HashMap()
                                    table_header.put('text', 'Organization')
                                    table_header.put('value', 'org')
                                    table_header_data.add(table_header)

                                    HashMap csv_header = new HashMap()
                                    csv_header.put('title', 'Organization')
                                    csv_header.put('dataKey', 'org')
                                    csv_header_data.add(csv_header)
                            }
//                            only_one = true
                        }

                    }
//                    if(temp.isEmpty() == false){
                    fetchdata.add(temp)
                    if (temp.isEmpty() == false) {
                        set.add(temp)
                    }

                }

            }

            for (item in set) {
                item?.totalOut = item?.StaffOutBooks + item?.StudOutBooks
                item?.totalBooks = (item?.StaffOutBooks) + (item?.StudOutBooks) + (item?.StaffInBooks) + (item?.StudInBooks)
            }
            HashMap table_header = new HashMap()
            table_header.put('text', 'Out Books')
            table_header.put('value', 'totalOut')
            table_header_data.add(table_header)

            HashMap csv_header = new HashMap()
            csv_header.put('title', 'Out Books')
            csv_header.put('dataKey', 'totalOut')
            csv_header_data.add(csv_header)


            HashMap table_header1 = new HashMap()
            table_header1.put('text', 'Total Books')
            table_header1.put('value', 'totalBooks')
            table_header_data.add(table_header1)

            HashMap csv_header1 = new HashMap()
            csv_header1.put('title', 'Total Books')
            csv_header1.put('dataKey', 'totalBooks')
            csv_header_data.add(csv_header1)

//            hm.put('csv_header_data',csv_header_data)
//            hm.put('table_header_data',table_header_data)
//            hm.put('overdue_data',fetchdata)
            hm.put('csv_header_data', csv_header_data)
            hm.put('table_header_data', table_header_data)
            hm.put('overdue_data', set)


            return hm
//            }
        }
        return hm
    }

    def memberWise(request, ip, hm) {
        println "i am in memberWise Report"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

//        ArrayList table_header_data = new ArrayList()
//        ArrayList csv_header_data = new ArrayList()

        HashSet<HashMap> table_header_data = new HashSet<HashMap>()
        HashSet<HashMap> csv_header_data = new HashSet<HashMap>()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        String purpose = "Getting reports  memberWise."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
//            ArrayList fetchdata = new ArrayList()
            HashSet<HashMap> fetchdata = new HashSet<HashMap>()
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)

                Program book_cat = Program.findById(dataresponse.dept_id)
                UserType user_type = UserType.findById(dataresponse.user_id)
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
//                String dateInString = "15-10-2015";
                Date date1 = sdf1.parse(dataresponse.firstDate);
                Date date2 = sdf1.parse(dataresponse.lastDate);
//                def bl = BookLending.createCriteria()
//                def bl_data = bl.list{
////                    eq("isactive", true)
//                    and {
//                        between("borrowing_date", date1,date2)
//                        between("due_date", date1,date2)
//                    }
//                }
                def only_one = false

                def useradata
                UserType ut = UserType.findById(dataresponse.user_id)
                if(dataresponse?.org == 'ALL'){
                     useradata = User.findAllByUsertype(ut)
                    println('useradata-------in all'+useradata)
                }
                if(dataresponse?.org != 'ALL') {
                    Organization org = Organization.findById(dataresponse?.org)
                     useradata = User.findAllByUsertypeAndOrganization(ut,org)
                    println('useradata-------in org'+useradata)

                }

                for (usr in useradata) {
                    println('----------usr' + usr)
                    def bl = BookLending.createCriteria()

                    def bl_data = bl.list {
                        eq("user", usr)
                        and {
                            between("borrowing_date", date1, date2)
                            between("due_date", date1, date2)
                        }
                    }
                    int issueCount = 0
                    int index = 0

                    for (item in bl_data) {
//        int srno = 1

                        index++
                        HashMap temp = new HashMap()
//                    temp.put('srno',srno++)
                        def user_data
                        UserType member = UserType.findByName('Member')
                        UserType employee = UserType.findByName('Employee')
                        if (item?.user?.usertype?.name == member?.name) {
                            Member mem = Member.findByUser(item?.user)
                            user_data = mem
                            if (dataresponse?.colData?.memberName) {
                                temp.put('member_name', mem?.name)
                                if (only_one == false) {
                                    HashMap table_header = new HashMap()
                                    table_header.put('text', 'Name')
                                    table_header.put('value', 'member_name')
                                    table_header_data.add(table_header)

                                    HashMap csv_header = new HashMap()
                                    csv_header.put('title', 'Name')
                                    csv_header.put('dataKey', 'member_name')
                                    csv_header_data.add(csv_header)
                                }
                            }
                        }

                        if (item?.user?.usertype?.name == employee?.name) {
                            Employee emp = Employee.findByUser(item?.user)
                            user_data = emp
                            if (dataresponse?.colData?.memberName) {
                                temp.put('member_name', emp?.name)
                                if (only_one == false) {
                                    HashMap table_header = new HashMap()
                                    table_header.put('text', 'Name')
                                    table_header.put('value', 'member_name')
                                    table_header_data.add(table_header)

                                    HashMap csv_header = new HashMap()
                                    csv_header.put('title', 'Name')
                                    csv_header.put('dataKey', 'member_name')
                                    csv_header_data.add(csv_header)
                                }
                            }
                        }
                        if (dataresponse?.colData?.outDate) {
                            def outDate = dateToString(item?.borrowing_date)
                            temp.put('outDate', outDate)
                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Out Date')
                                table_header.put('value', 'outDate')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Out Date')
                                csv_header.put('dataKey', 'outDate')
                                csv_header_data.add(csv_header)
                            }
                        }

                        if (dataresponse?.colData?.org) {
                            temp.put('org', item?.organization?.name)
                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Organization')
                                table_header.put('value', 'org')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Organization')
                                csv_header.put('dataKey', 'org')
                                csv_header_data.add(csv_header)
                            }
                        }

                        int count = issueCount++
                        if (dataresponse?.colData?.bookIssuedCount) {
                            temp.put('bookIssuedCount', count + 1)
                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'No.of Times Books Issued')
                                table_header.put('value', 'bookIssuedCount')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'No.of Times Books Issued')
                                csv_header.put('dataKey', 'bookIssuedCount')
                                csv_header_data.add(csv_header)
                            }
                        }

                        only_one = true
                        Program pg = Program.findById(dataresponse.dept_id)


                        if (bl_data.size() == index && user_data?.program?.name == pg?.name) {
                            temp.put('srno', fetchdata.size() + 1)
                            fetchdata.add(temp)
                        }
//                }
                    }
                    hm.put('dept', book_cat?.name)
                    hm.put('usertype', user_type?.name)

                }

                hm.put('csv_header_data', csv_header_data)
                hm.put('table_header_data', table_header_data)
                hm.put('overdue_data', fetchdata)
                return hm
            }
        }
        return hm
    }
    def dateToString(Date date) {
        if(!date){
            return 'NA'
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String strDate = formatter.format(date);
        println strDate
        return strDate
    }

    //get All Org getAllOrgFineData
    def getAllOrgFineData(request, hm) {
        println "i in get All Org, fine Data :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "Org Book Data."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList fineData = new ArrayList()
                int i=0
                int all_totalFine = 0
                def allOrg=[]
                if(user.organization.ismanagement)
                    allOrg=Organization.list()
                else
                    allOrg=user.organization
                for(org in allOrg)
                {
                    i++
                    HashMap temp=new HashMap()
                    def books=BookItem.findAllByOrganization(org)
                    temp.put("orgName",org?.displayname+" ("+org?.name+")")

                    def fineTransaction = FineTransaction.findAllByOrganizationAndIsactive(org,false)
                    int totalFine = 0
                    for(fine in fineTransaction){
                        totalFine= totalFine + fine?.amount
                    }
                    all_totalFine = all_totalFine + totalFine
                    temp.put("fine",totalFine)
                    temp.put("srno",i)
                    fineData.add(temp)
                }
                HashMap temp = new HashMap()
                temp.put("fine",all_totalFine)
                temp.put("orgName","Total in Rs.")
                fineData.add(temp)
                hm.put("fineData",fineData)
//                hm.put("totalFine",all_totalFine)
                hm.put("msg",'200')
            }
        }
        return hm
    }
}
