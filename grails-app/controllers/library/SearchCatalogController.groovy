package library

import grails.converters.JSON

class SearchCatalogController {

    def index() { }

    def getAllBook()
    {
        println "getAllBook"
        SearchCatalogService searchCatelogService = new SearchCatalogService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        searchCatelogService.increaseopaccount(request, hm,ip)
        searchCatelogService.getAllBook(request, hm,ip)
        render hm as JSON
    }

    def getAllBookwithfilterforemployee()
    {
        println "getAllBookwithfilterforemployee"
        SearchCatalogService searchCatelogService = new SearchCatalogService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        searchCatelogService.getAllBookwithfilterforemployee1(request, hm,ip)
        render hm as JSON
    }
    def getAllBookwithfilterfordelete()
    {
        println "getAllBookwithfilterfordelete"
        SearchCatalogService searchCatelogService = new SearchCatalogService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        searchCatelogService.getAllBookwithfilterfordelete(request, hm,ip)
        render hm as JSON
    }

    def getAllBookwithfilter()
    {
        println "getAllBookwithfilter"
        SearchCatalogService searchCatelogService = new SearchCatalogService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        searchCatelogService.increaseopaccount(request, hm,ip)
        searchCatelogService.getAllBookwithfilter(request, hm,ip)
        render hm as JSON
    }

    def getBookItemData()
    {
        println "getBookItemData"
        SearchCatalogService searchCatelogService = new SearchCatalogService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        searchCatelogService.increaseopaccount(request, hm,ip)
        searchCatelogService.getBookItemDataAccOpac1(request, hm,ip)
        render hm as JSON
    }
    def getnewarrivalbooksopac()
    {
        println "getBookItemData"
        SearchCatalogService searchCatelogService = new SearchCatalogService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        searchCatelogService.increaseopaccount(request, hm,ip)
        searchCatelogService.getnewarrivalbooksopac(request, hm,ip)
        render hm as JSON
    }

    def getBookItemDataSeries()
    {
        println "getBookItemDataSeries"
        SearchCatalogService searchCatelogService = new SearchCatalogService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        searchCatelogService.getBookItemDataSeries(request, hm,ip)
        render hm as JSON
    }
    def getBookItemDataSeriesHOME()
    {
        println "getBookItemDataSeries"
        SearchCatalogService searchCatelogService = new SearchCatalogService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        searchCatelogService.getBookItemDataSeriesHOME(request, hm,ip)
        render hm as JSON
    }

    def getBookFilter()
    {
        println "getBookFilter"
        SearchCatalogService searchCatelogService = new SearchCatalogService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        searchCatelogService.getBookFilter(request, hm,ip)
        render hm as JSON
    }

    def getBookFilterSeries()
    {
        println "getBookFilterSeries"
        SearchCatalogService searchCatelogService = new SearchCatalogService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        searchCatelogService.getBookFilterSeries(request, hm,ip)
        render hm as JSON
    }
    def getBookFilterSeriesHOME()
    {
        println "getBookFilterSeries"
        SearchCatalogService searchCatelogService = new SearchCatalogService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        searchCatelogService.getBookFilterSeriesHOME(request, hm,ip)
        hm.put('msg', '200')
        render hm as JSON
    }


    def getBookdeleteFilter()
    {
        println "getBookFilter"
        SearchCatalogService searchCatelogService = new SearchCatalogService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        searchCatelogService.getBookdeleteFilter(request, hm,ip)
        render hm as JSON
    }

    // OPAC HOME
    def getAllOrganizationHome()
    {
        println "getAllOrganizationHome"
        SearchCatalogService searchCatelogService = new SearchCatalogService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        searchCatelogService.getAllOrganizationHome(request, hm,ip)
        render hm as JSON
    }

    def getAllBookHome()
    {
        println "getAllBookHome"
        SearchCatalogService searchCatelogService = new SearchCatalogService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        searchCatelogService.increaseopaccount(request, hm,ip)
        searchCatelogService.getAllBookHome(request, hm,ip)
        render hm as JSON
    }

    def getBookItemdataAcc()
    {
        println "getBookItemdataAcc"
        SearchCatalogService searchCatelogService = new SearchCatalogService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        searchCatelogService.increaseopaccount(request, hm,ip)
        searchCatelogService.getBookItemdataAcc(request, hm,ip)
        render hm as JSON
    }

    def getBookItemdataHome()
    {
        println "getBookItemdataHome"
        SearchCatalogService searchCatelogService = new SearchCatalogService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        searchCatelogService.getBookItemdataHome(request, hm,ip)
        render hm as JSON
    }

    def getSeriesBookCount()
    {
        println "getSeriesBookCount"
        SearchCatalogService searchCatelogService = new SearchCatalogService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        searchCatelogService.getSeriesBookCount(request, hm,ip)
        render hm as JSON
    }

    def getSeriesBookCountHOME()
    {
        println "getSeriesBookCount"
        SearchCatalogService searchCatelogService = new SearchCatalogService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        searchCatelogService.getSeriesBookCountHOME(request, hm,ip)
        render hm as JSON
    }

    def getDepartmentBookCount()
    {
        println "getDepartmentBookCount"
        SearchCatalogService searchCatelogService = new SearchCatalogService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        searchCatelogService.getDepartmentBookCount(request, hm,ip)
        render hm as JSON
    }
    def getDepartmentBookCountHOME()
    {
        println "getDepartmentBookCount"
        SearchCatalogService searchCatelogService = new SearchCatalogService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        searchCatelogService.getDepartmentBookCountHOME(request, hm,ip)
        render hm as JSON
    }
}
