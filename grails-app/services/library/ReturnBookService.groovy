package library

import grails.converters.JSON
import grails.gorm.multitenancy.Tenants
import grails.gorm.transactions.Transactional
import groovy.json.JsonSlurper

import java.text.SimpleDateFormat

@Transactional
class ReturnBookService {

    def serviceMethod() {

    }
    def getBookData(request, hm,ip) {
        println("in getBookData:>>>")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String purpose = "getBookData"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            String body = request.getReader().text
            def jsonSluper = new JsonSlurper()
            def dataresponse = jsonSluper.parseText(body)
            println "dataresponse"+dataresponse

            Tenants.withId(log.tenantclient.tenantid) {
                //BookItem bookItem = BookItem.findByAccession_number(dataresponse.a_no)
                User loged_user = User.findByUsername(log.username)
                BookItem bookItem = BookItem.findByAccession_numberAndOrganization(dataresponse.a_no,loged_user.organization)
                HashMap temp = new HashMap()
                temp.put('bookItemId',bookItem?.id)
                temp.put('title',bookItem?.book?.title)
                temp.put('category',bookItem?.book?.bookcategory?.name)
                temp.put('publisher',bookItem?.book?.publisher?.name)
                temp.put('bookType',bookItem?.book?.booktype?.name)
                temp.put('bookFormat',bookItem?.bookformat?.name)
                temp.put('price',bookItem?.price)
                temp.put('copiesAvailable','')
                int conut
                boolean renewbtn
                BookLending bookL = BookLending.findByBookitemAndIsactive(bookItem,true)
                Employee employee =Employee.findByUser(bookL?.user)
                if(employee==null){
                    Member member =Member.findByUser(bookL?.user)
                    temp.put("name",member?.name)
                    temp.put("grno_empid",member?.registration_number)
                }
                else {
                    temp.put("name",employee?.name)
                    temp.put("grno_empid",employee?.employee_code)
                }

                temp.put('username',bookL?.user?.username)
                LibraryConfiguration lc=LibraryConfiguration.findByNameAndIsactive("RENEW",true)
                UserBookLog userBookLog=UserBookLog.findByBookitem(bookItem)
                if (userBookLog){
                    conut=userBookLog.renew_counter
                }
                println("conut >>>:"+conut)
                println("lc?.value ::"+lc?.value)
                int lc1=Integer.parseInt(lc?.value)
                if (conut == lc1) {
                    renewbtn = true
                    temp.put("renewbtn", renewbtn)
                } else {
                    renewbtn = false
                        temp.put("renewbtn", renewbtn)
                }
                SimpleDateFormat sdformat = new SimpleDateFormat("dd-MM-yyyy");
                if(bookL?.due_date!=null)
                {
                    Date d1 = sdformat.parse(dateToString(bookL?.due_date));
                    Date d2 = sdformat.parse(dateToString(new Date()));
                    temp.put('bookLId',bookL?.id)

                    FineTransaction FT = FineTransaction.findByBooklending(bookL)
//                println('ft============'+FT)
//                println(FT == null)
//                println(FT)
//                println(FT == 'null')

                    if(d1.compareTo(d2) > 0 || FT != null)
                    {
                        temp.put("fine", "NA")
                    }
                    else{
                        LibraryPolicy libraryPolicy
                        if(bookL?.user.role.size()>=1)
                        {
                            libraryPolicy = LibraryPolicy.findByRoleAndOrganization(bookL?.user.role[0],bookL?.user?.organization)
                        }
                        long difference_In_Time =  d2.getTime() - d1.getTime();
                        long difference_In_Days = (difference_In_Time/ (1000 * 60 * 60 * 24))
                        long fine = libraryPolicy.fine_rate_per_day * difference_In_Days
                        bookL.fine_amount = fine
                        bookL.save(flush:true,failOnError: true)
//                        temp.put("fine",fine)
                        temp.put("fine", "NA")
                    }

}
                else
                {

                    temp.put("fine", "NA")

                }

                if(bookL?.due_date)
                    temp.put('dueDate',dateToString(bookL?.due_date))
                if(bookL?.borrowing_date)
                    temp.put('borrowed_date',dateToString(bookL?.borrowing_date))
                def authorArr = bookItem?.book?.author
                ArrayList arr = new ArrayList()
                for(author in authorArr){
                    arr.add(author?.name)
                }
                temp.put('authors',arr)
                hm.put('book_info',temp)
                hm.put('msg','200')
            }
            return hm
        }
        return hm
    }

    def dateToString(Date date)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String strDate = formatter.format(date);
        println strDate
        return strDate
    }

    def getAccessionList(request, hm,ip) {
        println("in getAccessionList:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String purpose = "getAccessionList"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(uid)
                int srno = 1
                ArrayList a_list = new ArrayList()
                BookStatus bs = BookStatus.findByName('Issued')
                BookStatus renew = BookStatus.findByName('Renew')
//                def itemlist = BookItem.findAllByBookstatus(bs)
                def c = BookItem.createCriteria()
                def itemlist = c.list {
                    or {
                        eq("bookstatus", bs)
                        eq("bookstatus", renew)
                    }
                    like("organization",user?.organization)
                }



//                def itemlist = BookItem.findAll()
                println('a_list-------'+a_list)
                for(b_item in itemlist){
                    HashMap temp = new HashMap()
                    temp.put('bookItemId',b_item?.id)
                    temp.put('a_no',b_item?.accession_number)
                    temp.put('bookName',b_item?.accession_number+':'+b_item?.barocde+':'+ b_item?.book?.title)
                    a_list.add(temp)
                }
                hm.put('msg','200')
                hm.put('accessionNoList',a_list)


            }//println"hm: "+hm
            return hm

        }
        return hm
    }

    def receiveBook(request, hm,ip) {
        println("in receiveBook:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String purpose = "getBookData"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            String body = request.getReader().text
            def jsonSluper = new JsonSlurper()
            def dataresponse = jsonSluper.parseText(body)
            println dataresponse
            Tenants.withId(log.tenantclient.tenantid) {
                User loged_user = User.findByUsername(log.username)
                BookItem bookItem = BookItem.findByBarocdeAndOrganization(dataresponse.a_no,loged_user.organization)
                if(bookItem==null)
                {
                    bookItem = BookItem.findByAccession_numberAndOrganization(dataresponse.a_no,loged_user.organization)
                }
                if(!bookItem)
                {
                    hm.put("msg", "Book not Found")
                    return hm
                }

                BookStatus status = BookStatus.findByName('Available')
                bookItem?.bookstatus = status
                bookItem.save(flush: true, failOnError: true)

                BookLending bookL = BookLending.findByBookitemAndIsactive(bookItem,true)
                bookL?.isactive = false
                if(dataresponse.backdate){
                    println("backdate present ")
                    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
                    Date date1 = formatter1.parse(dataresponse?.backdate);
                    Calendar c1 = Calendar.getInstance();
                    c1.setTime(date1)
                    c1.add(Calendar.DATE, 1);
                    println("date1 : "+c1.getTime())
                    bookL?.return_date = c1.getTime()
                }else{
                    bookL?.return_date = new Date()
                }

                bookL.save(flush: true, failOnError: true)

                def userLog = UserBookLog.findByBookitemAndUser(bookItem,bookL?.user)
                println('userLog----'+ userLog)
                userLog.renew_counter = 0
                userLog.save(flush: true, failOnError: true)
                hm.put('msg','200')

                //check reservation status if waiting than convert to pending
                //book available student request in pending (notification)
                ReservationStatus status_waiting  = ReservationStatus.findByName('Waiting')
                ReservationStatus status_pending = ReservationStatus.findByName('Pending')
                def reservation_request = BookReservation.findAllByBookAndReservationstatusAndIsactive(bookItem?.book,status_waiting,true)
                if(reservation_request){
                    reservation_request.sort({it?.reservation_date})
                    reservation_request[0].reservationstatus = status_pending
                    reservation_request[0]?.updation_username = uid
                    reservation_request[0]?.updation_date = new Date()
                    reservation_request[0]?.updation_ip_address = ip
                    reservation_request[0].save(flush: true, failOnError: true)

                    BookStatus status_reserved = BookStatus.findByName('Reserved')
                    bookItem?.bookstatus = status_reserved
                    bookItem?.updation_username = uid
                    bookItem?.updation_date = new Date()
                    bookItem?.updation_ip_address = ip
                    bookItem.save(flush: true, failOnError: true)
                }

            }
            return hm

        }
        return hm
    }

    def payFineData(request, hm,ip) {
        println("in payFineData::")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        String purpose = "payFineData"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(uid)

                BookLending bl = BookLending.findById(dataresponse.bookLId)
                HashMap temp = new HashMap()

                if(bl?.user?.usertype?.name == "Employee"){
                    Member member = Member.findByUser(bl?.user)
                    temp.put('memberName',member?.name)
                    temp.put('memberCode',member?.registration_number)
                }

                if(bl?.user?.usertype?.name == "Employee"){
                    Employee employee = Employee.findByUser(bl?.user)
                    temp.put('memberName',employee?.name)
                    temp.put('memberCode',employee?.employee_code)
                }
                Date today = new Date()

                temp.put('title',bl?.bookitem?.book.title)
                temp.put('checkOut_date',dateToString(bl?.borrowing_date));
                temp.put('due_date',dateToString(bl?.due_date));
                temp.put('return_date',dateToString(today));

                def transaction_type = TransactionType.findAllByIsactive(true)
                ArrayList arr = new ArrayList()
                for(item in transaction_type){
                    HashMap data = new HashMap()
                    data.put('name',item?.name)
                    data.put('id',item?.id)
                    arr.add(data)
                }

                hm.put('transactionType_data',arr)
                hm.put('data',temp)
                hm.put('msg','200')
            }
            return hm

        }
        return hm
    }

    def payfine(request, hm,ip) {
        println("in payfine::")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        String purpose = "payfine"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(uid)

                BookLending bl = BookLending.findById(dataresponse.bookLId)
                HashMap temp = new HashMap()

                TransactionType tran_type = TransactionType.findById(dataresponse?.pay_method)


                if(dataresponse?.fine_settle == false && dataresponse?.clear_all_fine == false){
                    FineTransaction FT = new FineTransaction()
                    def indexConfig = IndexConfig.findAll()
                    def receipt_no = indexConfig[0]?.receipt_no

                    if(dataresponse?.transactioNo){
                        FT.transaction_number = dataresponse?.transactioNo
                    }
                    if(dataresponse?.bankName){
                        FT.bank_name = dataresponse?.bankName
                    }
                    if(dataresponse?.branchName){
                        FT.bank_branch = dataresponse?.branchName
                    }
                    FT.booklending = bl
                    FT.organization = bl?.user?.organization
                    FT.isactive = false
                    FT.amount = dataresponse?.amount
                    FT.transaction_date = new Date()
                    FT.transactiontype = tran_type
                    FT.receipt_no = receipt_no + 1
                    FT.updation_ip_address = ip
                    FT.creation_ip_address = ip
                    FT.updation_date = new Date()
                    FT.creation_date = new Date()
                    FT.updation_username = uid
                    FT.creation_username = uid
                    FT.save(flush: true, failOnError: true)


                    indexConfig[0].receipt_no = receipt_no + 1
                    indexConfig[0].save(flush: true, failOnError: true)

                    bl.fine_amount = 0
                    bl.updation_username = uid
                    bl.updation_date = new Date()
                    bl.updation_ip_address = ip
                    bl.save(flush: true, failOnError: true)
                }

                if(dataresponse?.fine_settle == true && dataresponse?.clear_all_fine == false){
                    println('in fine settlement')
                    FineTransaction FT = new FineTransaction()
                    def indexConfig = IndexConfig.findAll()
                    def receipt_no = indexConfig[0]?.receipt_no

                    def remaining_paid_amount = dataresponse?.amount -  dataresponse?.paid_amount

                    if(dataresponse?.transactioNo){
                        FT.transaction_number = dataresponse?.transactioNo
                    }
                    if(dataresponse?.bankName){
                        FT.bank_name = dataresponse?.bankName
                    }
                    if(dataresponse?.branchName){
                        FT.bank_branch = dataresponse?.branchName
                    }
                    FT.booklending = bl
                    FT.organization = bl?.user?.organization
                    FT.isactive = false
                    FT.amount = dataresponse?.paid_amount
                    FT.transaction_date = new Date()
                    FT.transactiontype = tran_type
                    FT.receipt_no = receipt_no + 1
                    FT.updation_ip_address = ip
                    FT.creation_ip_address = ip
                    FT.updation_date = new Date()
                    FT.creation_date = new Date()
                    FT.updation_username = uid
                    FT.creation_username = uid
                    FT.save(flush: true, failOnError: true)


                    indexConfig[0].receipt_no = receipt_no + 1
                    indexConfig[0].save(flush: true, failOnError: true)

                    bl.fine_amount = 0
                    bl.updation_username = uid
                    bl.updation_date = new Date()
                    bl.updation_ip_address = ip
                    bl.save(flush: true, failOnError: true)

                    if(remaining_paid_amount > 0 ){
                        FineTransaction FT1 = new FineTransaction()

                        FT1.booklending = bl
                        FT1.organization = bl?.user?.organization
                        FT1.isactive = true
                        FT1.amount = remaining_paid_amount
                        FT1.transaction_date = new Date()
                        FT1.transactiontype = tran_type
//                        FT1.receipt_no = receipt_no + 1
                        FT1.updation_ip_address = ip
                        FT1.creation_ip_address = ip
                        FT1.updation_date = new Date()
                        FT1.creation_date = new Date()
                        FT1.updation_username = uid
                        FT1.creation_username = uid
                        FT1.save(flush: true, failOnError: true)

                        bl.fine_amount = remaining_paid_amount
                        bl.updation_username = uid
                        bl.updation_date = new Date()
                        bl.updation_ip_address = ip
                        bl.save(flush: true, failOnError: true)
                    }


                    HashMap trackdata = new HashMap()
                    def total_fine_amt = dataresponse?.amount
                    def paid_amt_data = FineTransaction.findAllByBooklendingAndIsactive(bl,false)
                    def paid_amt = 0
                    for(data in paid_amt_data){
                        paid_amt = paid_amt + data?.amount
                    }

                    def unpaid_amt_data = FineTransaction.findAllByBooklendingAndIsactive(bl,true)
                    def unpaid_amt = 0
                    for(data in unpaid_amt_data){
                        unpaid_amt = unpaid_amt + data?.amount
                    }
                    trackdata.put('paid_amt',paid_amt)
                    trackdata.put('unpaid_amt',unpaid_amt)
                    trackdata.put('title',bl?.bookitem?.book?.title)
                    trackdata.put('acc_no',bl?.bookitem?.accession_number)
                    hm.put('trackdata',trackdata)

                }



                if(dataresponse?.fine_settle == false && dataresponse?.clear_all_fine == true){
                    FineTransaction FT = new FineTransaction()
                    def indexConfig = IndexConfig.findAll()
                    def receipt_no = indexConfig[0]?.receipt_no

                    if(dataresponse?.transactioNo){
                        FT.transaction_number = dataresponse?.transactioNo
                    }
                    if(dataresponse?.bankName){
                        FT.bank_name = dataresponse?.bankName
                    }
                    if(dataresponse?.branchName){
                        FT.bank_branch = dataresponse?.branchName
                    }
                    FT.booklending = bl
                    FT.organization = bl?.user?.organization
                    FT.isactive = false
                    FT.amount = dataresponse?.paid_amount
                    FT.transaction_date = new Date()
                    FT.transactiontype = tran_type
                    FT.clearFine = true
                    FT.receipt_no = receipt_no + 1
                    FT.updation_ip_address = ip
                    FT.creation_ip_address = ip
                    FT.updation_date = new Date()
                    FT.creation_date = new Date()
                    FT.updation_username = uid
                    FT.creation_username = uid
                    FT.save(flush: true, failOnError: true)


                    indexConfig[0].receipt_no = receipt_no + 1
                    indexConfig[0].save(flush: true, failOnError: true)

                    bl.fine_amount = 0
                    bl.updation_username = uid
                    bl.updation_date = new Date()
                    bl.updation_ip_address = ip
                    bl.save(flush: true, failOnError: true)
                }
                hm.put('msg','200')
            }
            return hm

        }
        return hm
    }
}
