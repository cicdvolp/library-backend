package library

import com.opencsv.CSVReader
import grails.converters.JSON
import grails.gorm.multitenancy.Tenants
import javax.servlet.http.Part

import grails.io.IOUtils
import groovy.json.JsonSlurper

import javax.servlet.http.Part
import java.nio.file.Paths

class AdminController {

    def fetchEmployeeType()
    {
        println "fetchEmployeeType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        adminService.findusertype(request, hm)
        render hm as JSON
    }
    def fetchEmployeeType_new()
    {
        println "fetchEmployeeType_new"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        adminService.findusertype_new(request, hm)
        render hm as JSON
    }
    def checkUsername()
    {
        println "checkUsername"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        adminService.findusername(request, hm)
        render hm as JSON
    }
    def checkUsername_new()
    {
        println "checkUsername_new"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        adminService.findusername_new(request, hm)
        render hm as JSON
    }
    def saveUsername()
    {
        println "Save Username"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.saveUsername(request, ip ,hm)
        render hm as JSON
    }
    def saveUsername_new()
    {
        println " save Username_new"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.saveUsername_new(request, ip ,hm)
        render hm as JSON
    }
    def updateUtype()
    {
        println "update usertype and registration number / employee code"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.updateUtype(request, ip ,hm)
        render hm as JSON
    }
    def updateUtype_new()
    {
        println "update usertype and registration number / employee code NEw"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.updateUtype_new(request, ip ,hm)
        render hm as JSON
    }

    def adminMasterMenu()
    {
        println "get adminMasterMenu"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()

        adminService.adminMasterMenu(request, ip ,hm)
        render hm as JSON
    }

    def getMasterUserType()
    {
        println "get adminMasterMenu"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getMasterUserType(request ,hm)
        render hm as JSON
    }

    def getMasterPublisher()
    {
        println "get getMasterPublisher"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getMasterPublisher(request ,hm,ip)
        render hm as JSON
    }

    def editAdminUserTypeOperation()
    {
        println " editAdminUserTypeOperation"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editAdminUserTypeOperation(request ,hm)
        render hm as JSON
    }

    def addUserType()
    {
        println " editAdminUserTypeOperation"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addUserType(request ,hm,ip)
        render hm as JSON
    }

    def editUsertype()
    {
        println " editUsertype"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editUsertype(request ,hm,ip)
        render hm as JSON
    }
    def deleteUserType()
    {
        println " deleteUserType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.deleteUserType(request ,hm)
        render hm as JSON
    }
//madhuri
    def bookclassification(){
        println "book classification"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        //String ip = request.getRemoteAddr()
        adminService.bookclassification(request ,hm)
        render hm as JSON
    }
    def savebookclassification(){
        println "save book classification"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.savebookclassification(request ,hm,ip)
        render hm as JSON
    }
    def editbookclassification(){
        println "edit bookclassification"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editbookclassification(request ,hm ,ip)
        render hm as JSON
    }
    def deletebookclassification(){
        println "delete bookclassification"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.deletebookclassification(request ,hm ,ip)
        render hm as JSON
    }
    def bookclassificationIsactive(){
        println "Isactive bookclassification"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.bookclassificationIsactive(request ,hm ,ip)
        render hm as JSON
    }
    //end
    def deletesaveTemplateer(){
        println " deletePublisher"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.deletePublisher(request ,hm)
        render hm as JSON
    }
    def deletePublisher(){
        println " deletePublisher"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.deletePublisher(request ,hm)
        render hm as JSON
    }

    def fetchAllUser()
    {
        println "fetchAllUser"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        //String ip = request.getRemoteAddr()
        adminService.fetchAllUser(request ,hm)
        render hm as JSON
    }
    def fetchAllUserwithname()
    {
        println "fetchAllUserwithname"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        //String ip = request.getRemoteAddr()
        adminService.fetchAllUserwithname(request ,hm)
        render hm as JSON
    }
    def fetchUserType()
    {
        println "fetch user type"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        //String ip = request.getRemoteAddr()
        adminService.fetchUserType(request ,hm)
        render hm as JSON
    }
    def assignRole()
    {
        println "Assignment of role"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.assignRole(request ,hm ,ip)
        render hm as JSON
    }
    def deleteUserRole()
    {
        println "Delete the role of user---->>>>>>>"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.deleteUserRole(request ,hm ,ip)
        render hm as JSON
    }
    def getPublisher()
    {
        println "get getPublisher"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getPublisher(request ,hm)
        render hm as JSON
    }

    def addPublisher()
    {
        println " addPublisher"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addPublisher(request ,hm,ip)
        render hm as JSON
    }

    def publisherIsactive()
    {
        println " publisherIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.publisherIsactive(request ,hm)
        render hm as JSON
    }

    def editPublisher()
    {
        println " editPublisher"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editPublisher(request ,hm,ip)
        render hm as JSON
    }

    def getAuthor()
    {
        println "get getAuthor"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getAuthor(request ,hm)
        render hm as JSON
    }

    def addAuthor()
    {
        println " addAuthor"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addAuthor(request ,hm,ip)
        render hm as JSON
    }

    def authorIsactive()
    {
        println " authorIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.authorIsactive(request ,hm)
        render hm as JSON
    }

    def editAuthor()
    {
        println " edit author"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editAuthor(request ,hm,ip)
        render hm as JSON
    }

    def deleteAuthor(){
        println " deleteAuthor "
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
       // String ip = request.getRemoteAddr()
        adminService.deleteAuthor(request ,hm)
        render hm as JSON
    }

    def importAuthor()
    {
        println "importAuthor"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.importAuthor(request, ip ,hm)
        render hm as JSON
    }

    def getBookType()
    {
        println "get getBookType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getBookType(request ,hm)
        render hm as JSON
    }

    def addBookType()
    {
        println " addBookType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addBookType(request ,hm,ip)
        render hm as JSON
    }

    def bookTypeIsactive()
    {
        println " bookTypeIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.bookTypeIsactive(request ,hm)
        render hm as JSON
    }

    def editBookType()
    {
        println " editBookType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editBookType(request ,hm,ip)
        render hm as JSON
    }
    def deleteBookType(){
        println " deleteBookType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        //String ip = request.getRemoteAddr()
        adminService.deleteBookType(request ,hm)
        render hm as JSON
    }

    def getLanguage()
    {
        println "get getLanguage"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getLanguage(request ,hm)
        render hm as JSON
    }

    def getBookSeries()
    {
        println "get getBookSeries"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getBookSeries(request ,hm)
        render hm as JSON
    }

    def addLanguage()
    {
        println " addLanguage"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addLanguage(request ,hm,ip)
        render hm as JSON
    }

    def languageIsactive()
    {
        println " languageIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.languageIsactive(request ,hm)
        render hm as JSON
    }

    def editLanguage()
    {
        println " editLanguage"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editLanguage(request ,hm,ip)
        render hm as JSON
    }

    def deleteLanguage()
    {
        println " deleteLanguage"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        //String ip = request.getRemoteAddr()
        adminService.deleteLanguage(request ,hm)
        render hm as JSON
    }
    def addBookSeries()
    {
        println " addBookSeries"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addBookSeries(request ,hm,ip)
        render hm as JSON
    }

    def bookSeriesIsactive()
    {
        println " bookSeriesIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.bookSeriesIsactive(request ,hm)
        render hm as JSON
    }

    def editBookSeries()
    {
        println " editBookSeries"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editBookSeries(request ,hm,ip)
        render hm as JSON
    }

    def getRole()
    {
        println "get getRole"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getRole(request ,hm)
        render hm as JSON
    }

    def addRole()
    {
        println " addRole"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addRole(request ,hm,ip)
        render hm as JSON
    }

    def roleIsactive()
    {
        println " roleIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.roleIsactive(request ,hm)
        render hm as JSON
    }

    def editRole()
    {
        println " editRole"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editRole(request ,hm,ip)
        render hm as JSON
    }
    def deleteRole(){
        println " deleteRole"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        //String ip = request.getRemoteAddr()
        adminService.deleteRole(request ,hm)
        render hm as JSON
    }

    def getBookCat()
    {
        println "get getBookCat"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getBookCat(request ,hm)
        render hm as JSON
    }

    def addBookCat()
    {
        println " addBookCat"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addBookCat(request ,hm,ip)
        render hm as JSON
    }

    def bookCatIsactive()
    {
        println " bookCatIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.bookCatIsactive(request ,hm)
        render hm as JSON
    }

    def editBookCat()
    {
        println " BookCat"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editBookCat(request ,hm,ip)
        render hm as JSON
    }

    def deleteBookCategory()
        {
            println " deleteBookCategory"
            AdminService adminService = new AdminService()
            HashMap hm = new HashMap()
            //String ip = request.getRemoteAddr()
            adminService.deleteBookCategory(request ,hm)
            render hm as JSON
        }

    def adminAddRoleLink()
    {
        println " adminAddRoleLink"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.adminAddRoleLink(request ,hm,ip)
        render hm as JSON
    }

    def editAdminAddRoleLink()
    {
        println " editAdminAddRoleLink"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editAdminAddRoleLink(request ,hm,ip)
        render hm as JSON
    }

    def saveAdminAddRoleLink()
    {
        println " saveAdminAddRoleLink"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.saveAdminAddRoleLink(request ,hm,ip)
        render hm as JSON
    }

    def getBookFormat()
    {
        println "getBookFormat"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getBookFormat(request ,hm)
        render hm as JSON
    }

    def addBookFormat()
    {
        println " addBookFormat"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addBookFormat(request ,hm,ip)
        render hm as JSON
    }

    def bookFormatIsactive()
    {
        println " bookFormatIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.bookFormatIsactive(request ,hm)
        render hm as JSON
    }

    def editBookFormat()
    {
        println " editBookFormat"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editBookFormat(request ,hm,ip)
        render hm as JSON
    }

    def getBookStatus()
    {
        println "getBookStatus"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getBookStatus(request ,hm)
        render hm as JSON
    }

    def deleteBookFormat(){
        println "deleteBookFormat"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
       // String ip = request.getRemoteAddr()
        adminService.deleteBookFormat(request ,hm)
        render hm as JSON
    }


    def addBookStatus()
    {
        println " addBookStatus"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addBookStatus(request ,hm,ip)
        render hm as JSON
    }

    def bookStatusIsactive()
    {
        println " bookStatusIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.bookStatusIsactive(request ,hm)
        render hm as JSON
    }

    def editBookStatus()
    {
        println " editBookStatus"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editBookStatus(request ,hm,ip)
        render hm as JSON
    }

    def deleteBookstatus()
    {
        println " deleteBookstatus"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
       // String ip = request.getRemoteAddr()
        adminService.deleteBookstatus(request ,hm)
        render hm as JSON
    }

    //madhuri
def getWeedoutcode(){
    println " editBookStatus"
    AdminService adminService = new AdminService()
    HashMap hm = new HashMap()
    //String ip = request.getRemoteAddr()
    adminService.getWeedoutcode(request ,hm)
    render hm as JSON
}
    def addWeedoutcode()
    {
        println " addBookStatus"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addWeedoutcode(request ,hm,ip)
        render hm as JSON
    }
    def editWeedoutcode()
    {
        println " editWeedoutcode"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editWeedoutcode(request ,hm,ip)
        render hm as JSON
    }
    def deleteWeedoutcode()
    {
        println " deleteWeedoutcode"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.deleteWeedoutcode(request ,hm,ip)
        render hm as JSON
    }
    //end

    def getReservationStatus()
    {
        println "getReservationStatus"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getReservationStatus(request ,hm)
        render hm as JSON
    }

    def addReservationStatus()
    {
        println " addReservationStatus"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addReservationStatus(request ,hm,ip)
        render hm as JSON
    }

    def reservationStatusIsactive()
    {
        println " ReservationStatusIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.reservationStatusIsactive(request ,hm)
        render hm as JSON
    }

    def editReservationStatus()
    {
        println " editReservationStatus"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editReservationStatus(request ,hm,ip)
        render hm as JSON
    }

    def deleteReservationStatus()
    {
        println " deleteReservationStatus"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        //String ip = request.getRemoteAddr()
        adminService.deleteReservationStatus(request ,hm)
        render hm as JSON
    }

    def getAccountStatus()
    {
        println "getAccountStatus"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getAccountStatus(request ,hm)
        render hm as JSON
    }

    def addAccountStatus()
    {
        println " addAccountStatus"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addAccountStatus(request ,hm,ip)
        render hm as JSON
    }

    def accountStatusIsactive()
    {
        println " AccountStatusIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.accountStatusIsactive(request ,hm)
        render hm as JSON
    }

    def editAccountStatus()
    {
        println " editAccountStatus"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editAccountStatus(request ,hm,ip)
        render hm as JSON
    }

    def deleteAccountStatus()
    {
        println " deleteAccountStatus"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        //String ip = request.getRemoteAddr()
        adminService.deleteAccountStatus(request ,hm)
        render hm as JSON
    }

    def getCurrency()
    {
        println "getCurrency"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getCurrency(request ,hm)
        render hm as JSON
    }

    def addCurrency()
    {
        println " addCurrency"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addCurrency(request ,hm,ip)
        render hm as JSON
    }

    def currencyIsactive()
    {
        println " CurrencyIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.currencyIsactive(request ,hm)
        render hm as JSON
    }

    def editCurrency()
    {
        println " editCurrency"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editCurrency(request ,hm,ip)
        render hm as JSON
    }
    def deleteCurrency()
    {
        println " deleteCurrency"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        //String ip = request.getRemoteAddr()
        adminService.deleteCurrency(request ,hm)
        render hm as JSON
    }

    def getBookCondition()
    {
        println "getBookCondition"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getBookCondition(request ,hm)
        render hm as JSON
    }

    def addBookCondition()
    {
        println " addBookCondition"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addBookCondition(request ,hm,ip)
        render hm as JSON
    }

    def bookConditionIsactive()
    {
        println " BookConditionIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.bookConditionIsactive(request ,hm)
        render hm as JSON
    }

    def editBookCondition()
    {
        println " editBookCondition"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editBookCondition(request ,hm,ip)
        render hm as JSON
    }

    def deleteBookCondition()
    {
        println " deleteBookCondition"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        //String ip = request.getRemoteAddr()
        adminService.deleteBookCondition(request ,hm)
        render hm as JSON
    }

    def getNotifyType()
    {
        println "getNotifyType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getNotifyType(request ,hm)
        render hm as JSON
    }

    def addNotifyType()
    {
        println " addNotifyType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addNotifyType(request ,hm,ip)
        render hm as JSON
    }

    def notifyTypeIsactive()
    {
        println " NotifyTypeIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.notifyTypeIsactive(request ,hm)
        render hm as JSON
    }

    def editNotifyType()
    {
        println " editNotifyType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editNotifyType(request ,hm,ip)
        render hm as JSON
    }

    def deleteNotificationType()
    {
        println " deleteNotificationType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        //String ip = request.getRemoteAddr()
        adminService.deleteNotificationType(request ,hm)
        render hm as JSON
    }

    def getTranType()
    {
        println "getTranType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getTranType(request ,hm)
        render hm as JSON
    }

    def addTranType()
    {
        println " addTranType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addTranType(request ,hm,ip)
        render hm as JSON
    }

    def tranTypeIsactive()
    {
        println " TranTypeIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.tranTypeIsactive(request ,hm)
        render hm as JSON
    }

    def editTranType()
    {
        println " editTranType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editTranType(request ,hm,ip)
        render hm as JSON
    }

    def deleteTransactionType()
    {
        println " deleteTransactionType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
       // String ip = request.getRemoteAddr()
        adminService.deleteTransactionType(request ,hm)
        render hm as JSON
    }

    def getRackData()
    {
        println " getRackData"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getRackData(request ,hm,ip)
        render hm as JSON
    }

    def editRack()
    {
        println " editRack"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editRack(request ,hm,ip)
        render hm as JSON
    }

    def saveRack()
    {
        println " saveRack"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.saveRack(request ,hm,ip)
        render hm as JSON
    }

    def rackIsactive()
    {
        println " rackIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.rackIsactive(request ,hm,ip)
        render hm as JSON
    }

    def deleteRack()
    {
        println " deleteRack"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.deleteRack(request ,hm,ip)
        render hm as JSON

    }
    def fetchAllUsertype()
    {
        println " fetch All User type"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.fetchAllUsertype(request,hm)
        render hm as JSON
    }
    def savelibrarypolicy()
    {
        println " save library policy"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.savelibrarypolicy(request,hm,ip)
        render hm as JSON
    }
    def updateLibraryPolicy()
    {
        println " update library policy"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.updateLibraryPolicy(request,hm,ip)
        render hm as JSON
    }
    def isactivelibrarypolicy()
    {
        println " isactive library policy"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.isactivelibrarypolicy(request,hm,ip)
        render hm as JSON
    }
    def getTemplateData()
    {
        println " getTemplateData"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getTemplateData(request ,hm,ip)
        render hm as JSON
    }

    def editTemplate()
    {
        println " editTemplate"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editTemplate(request ,hm,ip)
        render hm as JSON
    }

    def saveTemplate()
    {
        println " saveTemplate"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.saveTemplate(request ,hm,ip)
        render hm as JSON
    }

    def templateIsactive()
    {
        println " templateIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.templateIsactive(request ,hm,ip)
        render hm as JSON
    }

    def deleteNotificationTemp()
    {
        println " deleteNotificationTemp"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        //String ip = request.getRemoteAddr()
        adminService.deleteNotificationTemp(request ,hm)
        render hm as JSON
    }

    def getNotifyMethodData()
    {
        println " getNotifyMethodData"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getNotifyMethodData(request ,hm,ip)
        render hm as JSON
    }

    def editNotifyMethod()
    {
        println " editNotifyMethod"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editNotifyMethod(request ,hm,ip)
        render hm as JSON
    }

    def deleteNotificationMethod()
    {
        println " deleteNotificationMethod"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
       // String ip = request.getRemoteAddr()
        adminService.deleteNotificationMethod(request ,hm)
        render hm as JSON
    }

    def saveNotifyMethod()
    {
        println " saveNotifyMethod"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.saveNotifyMethod(request ,hm,ip)
        render hm as JSON
    }
    def nMethodIsactive()
    {
        println " nMethodIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.nMethodIsactive(request ,hm,ip)
        render hm as JSON
    }

    def getLibConfig()
    {
        println "get getLibConfig"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getLibConfig(request ,hm)
        render hm as JSON
    }

    def addLibConfig()
    {
        println " addLibConfig"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addLibConfig(request ,hm,ip)
        render hm as JSON
    }

    def LibConfigIsactive()
    {
        println " LibConfigIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.LibConfigIsactive(request ,hm)
        render hm as JSON
    }

    def editLibConfig()
    {
        println " editLibConfig"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editLibConfig(request ,hm,ip)
        render hm as JSON
    }
    def getOrgData()
    {
        println " getOrgData"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getOrgData(request ,hm,ip)
        render hm as JSON
    }

    def editOrg()
    {
        println " editOrg"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editOrg(request ,hm,ip)
        render hm as JSON
    }

    def saveOrg()
    {
        println " saveOrg"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.saveOrg(request ,hm,ip)
        render hm as JSON
    }

    def orgIsactive()
    {
        println " orgIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.orgIsactive(request ,hm)
        render hm as JSON
    }
    def importOrg()
    {
        println " importOrg"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.importOrg(request ,hm,ip)
        render hm as JSON
    }


    def getLibraryData()
    {
        println " getLibraryData"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getLibraryData(request ,hm,ip)
        render hm as JSON
    }

    def editLibrary()
    {
        println " editLibrary"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editLibrary(request ,hm,ip)
        render hm as JSON
    }

    def saveLibrary()
    {
        println " saveLibrary"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.saveLibrary(request ,hm,ip)
        render hm as JSON
    }

    def libraryIsactive()
    {
        println " libraryIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.libraryIsactive(request ,hm)
        render hm as JSON
    }
    def importLibrary()
    {
        println " importLibrary"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.importLibrary(request ,hm,ip)
        render hm as JSON
    }

    def roleLinksIsactive()
    {
        println " roleLinksIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.roleLinksIsactive(request ,hm)
        render hm as JSON
    }

    def addProgramType()
    {
        println " add ProgramType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addProgramType(request ,hm,ip)
        render hm as JSON
    }
    def saveacademicyear()
    {
        println " saveacademicyear"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.saveacademicyear(request ,hm,ip)
        render hm as JSON
    }
    //madhuri
    def academicyearIsactive(){
        println " Isactive academicyear"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.academicyearIsactive(request ,hm,ip)
        render hm as JSON
    }
    def academicyearIscurrent(){
        println " Iscurrent academicyear"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.academicyearIscurrent(request ,hm,ip)
        render hm as JSON
    }
//end
    def getProgramType()
    {
        println "getProgramType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getProgramType(request ,hm)
        render hm as JSON
    }
    def getacademicyearlist_data()
    {
        println "getProgramType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getacademicyearlist(request ,hm)
        adminService.getacademicyeardata(request ,hm)
        render hm as JSON
    }
    def get_ay_sem_year_program_list()
    {
        println "get_ay_sem_year_program_list"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getacademicyearlist(request ,hm)
        adminService.getprogramlist(request ,hm)
        adminService.getyearlist(request ,hm)
        adminService.getsemesterlist(request ,hm)
        adminService.getalllearnerdivision(request ,hm)
        render hm as JSON
    }
    def getyeardata()
    {
        println "getyeardata"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getyeardata(request ,hm)
        render hm as JSON
    }
    def getsemesterdata()
    {
        println "getsemesterdata"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getsemesterdata(request ,hm)
        render hm as JSON
    }

    def getshiftdata()
    {
        println "getshiftdata"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getshiftdata(request ,hm)
        render hm as JSON
    }

    def editacademicyear()
    {
        println " editacademicyear"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editacademicyear(request ,hm,ip)
        render hm as JSON
    }

    def edityear()
    {
        println " edityear"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.edityear(request ,hm,ip)
        render hm as JSON
    }
    def editsemester()
    {
        println " editsemester"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editsemester(request ,hm,ip)
        render hm as JSON
    }
    def editshift()
    {
        println " editshift"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editshift(request ,hm,ip)
        render hm as JSON
    }
    def saveyear()
    {
        println " saveyear"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.saveyear(request ,hm,ip)
        render hm as JSON
    }
    def savesemester()
    {
        println " saveyear"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.savesemester(request ,hm,ip)
        render hm as JSON
    }
    def saveshift()
    {
        println " saveshift"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.saveshift(request ,hm,ip)
        render hm as JSON
    }

    def editProgramtype()
    {
        println " editProgramtype"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editProgramtype(request ,hm,ip)
        render hm as JSON
    }


    def deleteProgramType(){
        println " deleteProgramType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.deleteProgramType(request ,hm)
        render hm as JSON
    }
    def deleteAcademicyear(){
        println " deleteProgramType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.deleteAcademicyear(request ,hm)
        render hm as JSON
    }

    def deleteyear(){
        println " deleteyear"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.deleteyear(request ,hm)
        render hm as JSON
    }

    def deletesemester(){
        println " deleteyear"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.deletesemester(request ,hm)
        render hm as JSON
    }
    def deleteshift(){
        println " deleteyear"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.deleteshift(request ,hm)
        render hm as JSON
    }
    def addProgram()
    {
        println " add addProgram"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addProgram(request ,hm,ip)
        render hm as JSON
    }

    def getProgram()
    {
        println "getProgram"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getProgram(request ,hm)
        render hm as JSON
    }

    def editProgram()
    {
        println " editProgram"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editProgram(request ,hm,ip)
        render hm as JSON
    }

    def deleteprogram(){
        println " deleteprogram"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.deleteprogram(request ,hm,ip)
        render hm as JSON
    }
//    def saveOrg() {
//        HashMap hm = new HashMap()
//        String ip = request.getRemoteAddr()
//
//        println("in saveOrg:")
//        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
//        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
//        String url = request.getHeader("router-path")
//
//        println('params----'+params)
//        println("request")
//        println(request)
//
//        AuthService auth = new AuthService()
//
//        String purpose = "saveOrg from admin  menu"
//        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
//        println("request")
//        println(request)
//
//        def tenant = TenantClient.findByName(params.tenant_name)
//        println('tenant----'+tenant)
//        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
//            Login log = Login.findByUsername(uid)
//            Tenants.withId(log.tenantclient.tenantid) {
//
//                def folderPath
//                def fileName
//                def filePath
//                def fileExtention
//                for (Part filePart : request.getParts()) {
//                    InputStream fs = filePart.getInputStream()
//                String filePartName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
//                int indexOfDot = filePartName.lastIndexOf('.')
//                if (indexOfDot > 0) {
//                    fileExtention = filePartName.substring(indexOfDot + 1)
//                }
//                String fileGR = filePartName.substring(0, filePartName.lastIndexOf("."));
//                String filename = fileGR
//                String username = filename
////                  folderPath = log.tenantclient.tenantid+"/Student_photo/"+user.organization.id+'/'
//                folderPath = Library_Organization_logo + '/'
//                fileName = filename + "." + fileExtention // question no
//                println('filename----' + filename)
//
//                filePath = folderPath + fileName
//                println('filePath----' + filePath)
//                println('folderPath----' + folderPath)
//
////                    AWSUploadPhotosService awsUploadDocumentsService = new AWSUploadPhotosService()
////                        boolean isUpload = awsUploadDocumentsService.uploadPhoto(filePart, folderPath, fileName, filePath)
//                }
//
//
//
//
//
//                def org = Organization.findByName(params.name)
//                if (org == null) {
//                    org = new Organization()
//                    if(tenant){
//                        org.tenantid = tenant?.id
//                    }
//
////                    if (isUpload) {
//                    org.profilephotofilename=fileName
//                    org.profilephotofilepath=folderPath
////                        println "Image uploaded....."
////                    } else {
////                        println 'Image not uploaded'
////                    }
//                    org.tenantname = params.tenant_name
//                    org.name = params.name
//                    org.displayname = params.display_name
//                    org.address = params.address
//                    org.contact_person_name = params.cp_name
//                    org.isactive = true
//                    org.email = params.email
//                    org.contact = params.contact
//                    org.website = params.website
//                    org.country = params.country
//                    org.loginattachementdomain = params.domain
//                    org.logofilepath = 'NA'
//                    org.logofilename = 'NA'
//                    org.creation_username = uid
//                    org.updation_username = uid
//                    org.creation_date = new java.util.Date()
//                    org.updation_date = new java.util.Date()
//                    org.creation_ip_address = ip
//                    org.updation_ip_address = ip
//                    org.save(failOnError: true, flush: true)
//
//                    HashMap status = new HashMap()
//                    status.put("code", "SUCCESS")
//                    status.put("message", "Organization added successfully...")
//                    hm.put("status", status)
//                    return hm
//                } else {
//                    HashMap status = new HashMap()
//                    status.put("code", "NA")
//                    status.put("message", "Organization Already present!!!")
//                    hm.put("status", status)
//                }
//            }
////            }
//            render hm as JSON
////            return hm
//
//        }
//        render hm as JSON
////        return hm
//    }

    def getPhysiacalDetails()
    {
        println "getPhysiacalDetails"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getPhysiacalDetails(request ,hm)
        render hm as JSON
    }

    def addPhysiacalDetails()
    {
        println " addLanguage"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addPhysiacalDetails(request ,hm,ip)
        render hm as JSON
    }

    def physicaldetailsIsactive()
    {
        println " physicaldetailsIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.physicaldetailsIsactive(request ,hm)
        render hm as JSON
    }

    def editPhysiacalDetails()
    {
        println " editPhysiacalDetails"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editPhysiacalDetails(request ,hm,ip)
        render hm as JSON
    }

    def deletePhysicalDetails()
    {
        println " deletePhysicalDetails"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        //String ip = request.getRemoteAddr()
        adminService.deletePhysicalDetails(request ,hm)
        render hm as JSON
    }
    def getAccompanyingMaterials()
    {
        println "getAccompanyingMaterials"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getAccompanyingMaterials(request ,hm)
        render hm as JSON
    }

    def addAccompanyingMaterials()
    {
        println " addLanguage"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addAccompanyingMaterials(request ,hm,ip)
        render hm as JSON
    }

    def accompanyingMaterialsIsactive()
    {
        println " physicaldetailsIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.accompanyingMaterialsIsactive(request ,hm)
        render hm as JSON
    }

    def editAccompanyingMaterials()
    {
        println " editPhysiacalDetails"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editAccompanyingMaterials(request ,hm,ip)
        render hm as JSON
    }

    def deleteAccompanyingMaterials()
    {
        println " deleteAccompanyingMaterials"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.deleteAccompanyingMaterials(request ,hm,ip)
        render hm as JSON
    }
    //
    def getSeries()
    {
        println "getSeries"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getSeries(request ,hm)
        render hm as JSON
    }

    def addSeries()
    {
        println " addLanguage"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addSeries(request ,hm,ip)
        render hm as JSON
    }

    def seriesIsactive()
    {
        println " seriesIsactive"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.seriesIsactive(request ,hm)
        render hm as JSON
    }

    def editSeries()
    {
        println " editSeries"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editSeries(request ,hm,ip)
        render hm as JSON
    }

    def deleteSeries()
    {
        println " deleteSeries"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.deleteSeries(request ,hm,ip)
        render hm as JSON
    }

    def deleteBookSeries()
    {
        println " deleteBookSeries"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.deleteBookSeries(request ,hm ,ip)
        render hm as JSON
    }

    def getErpDepartment(){
        println "getErpDepartment in controller"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getErpDepartment(request ,hm)
        render hm as JSON
    }
    def saveERPDepartment(){
        println " saveERPDepartment in controller"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.saveERPDepartment(request ,hm,ip)
        render hm as JSON
    }
    def editERPDepartment(){
        println " editERPDepartment in controller"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editERPDepartment(request ,hm,ip)
        render hm as JSON
    }
    def deleteErpDepartment(){
        println " deleteErpDepartment in controller"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.deleteErpDepartment(request ,hm ,ip)
        render hm as JSON
    }

    //Book vendor type
    def getVendorType()
    {
        println "get getVendorType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getVendorType(request ,hm)
        render hm as JSON
    }
    def addVendorType()
    {
        println " addVendorType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addVendorType(request ,hm,ip)
        render hm as JSON
    }
    def editVendorType()
    {
        println " editVendorType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editVendorType(request ,hm,ip)
        render hm as JSON
    }
    def deleteVendorType(){
        println " deleteVendorType"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.deleteVendorType(request ,hm, ip)
        render hm as JSON
    }

    //Book vendor
    def getBookVendor()
    {
        println "get getVendor"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getBookVendor(request ,hm)
        render hm as JSON
    }
    def addBookVendor()
    {
        println " addBookVendor"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addBookVendor(request ,hm,ip)
        render hm as JSON
    }
    def editBookVendor()
    {
        println " editBookVendor"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editBookVendor(request ,hm,ip)
        render hm as JSON
    }
    def deleteBookVendor(){
        println " deleteVendor"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.deleteBookVendor(request ,hm, ip)
        render hm as JSON
    }

    //
    def getLibraryDepartment()
    {
        println "get getLibraryDepartment"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getLibraryDepartment(request ,hm)
        render hm as JSON
    }
    def addLibraryDepartment()
    {
        println " addLibraryDepartment"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.addLibraryDepartment(request ,hm,ip)
        render hm as JSON
    }
    def deleteLibraryDepartment()
    {
        println " deleteLibraryDepartment"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.deleteLibraryDepartment(request ,hm, ip)
        render hm as JSON
    }
    def editLibraryDepartment()
    {
        println " editLibraryDepartment"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editLibraryDepartment(request ,hm,ip)
        render hm as JSON
    }

    def importLibraryDepartment()
    {
        println "importAuthor"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.importLibraryDepartment(request, ip ,hm)
        render hm as JSON
    }

    ///------Frequency
    def getFrequency(){
        println "getFrequency"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.getFrequency(request, ip ,hm)
        render hm as JSON
    }

    def saveFrequency(){
        println "saveFrequency"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.saveFrequency(request, ip ,hm)
        render hm as JSON
    }
    def editFrequency(){
        println "editFrequency"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.editFrequency(request, ip ,hm)
        render hm as JSON
    }
    def deleteFrequency(){
        println "deleteFrequency"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.deleteFrequency(request, ip ,hm)
        render hm as JSON
    }
    def FrequencyIsactive(){
        println "Isactive Frequency"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminService.frequencyIsactive(request, ip ,hm)
        render hm as JSON
    }

}
