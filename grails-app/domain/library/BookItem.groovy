package library
import grails.gorm.MultiTenant
class BookItem implements MultiTenant<BookItem>
{
    String accession_number	  // book sequence number
    String barocde
    Long display_order//bar code, QR code // book sequence number
    String filename
    String filepath
    String publication_year
    Date publication_date
    int numberofpages
    double price // purchase cost
    double book_price// actual cost
    double discount_percentage // discount percentage
    Date date_of_purchase
    Date date_of_entry
    Date withdrawal_date
    String withdrawal_no
    String publication_place
    String keyword

    String location // Location of book purchase
    String bill_number
    boolean is_received_from_donation
    Date borrowed_date
    Date due_date
    boolean isactive

    boolean ill
    String withdrawal_approval_no_with_date
    String remarks
    String subject
    String vendor_name

    String weedout_remark
    String weedout_date
    String weedout_receiptnumber
    String weedout_receiptdate

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[book:Book,librarydepartment:LibraryDepartment,bookformat:BookFormat,bookstatus:BookStatus,rack:Rack,organization:Organization,series:Series,weedoutcode:Weedoutcode]

    static constraints = {
        weedoutcode nullable:true
        vendor_name nullable:true
        weedout_receiptnumber nullable:true
        weedout_receiptdate nullable:true
        weedout_remark nullable:true
        weedout_date nullable:true
        subject nullable:true
        publication_year nullable: true
        filename nullable: true
        filepath nullable: true
        keyword nullable: true
        publication_date nullable: true
        date_of_purchase nullable: true
        librarydepartment nullable: true
        date_of_entry nullable: true
        location nullable: true
        bill_number nullable: true
        is_received_from_donation defaultValue: false
        borrowed_date nullable: true
        due_date nullable: true
        withdrawal_date nullable: true
        withdrawal_no nullable: true
        publication_place nullable: true
        rack nullable: true
        bookformat nullable: true
        series nullable: true
        withdrawal_approval_no_with_date nullable: true
        remarks nullable: true
        bookstatus nullable: true
        barocde nullable: true
        display_order nullable: true
    }

    static mapping = {
        ill defaultValue: false
        display_order defaultValue: 9999999
        isactive defaultValue: true
        numberofpages defaultValue: 0
        price defaultValue: 0
        book_price defaultValue: 0
        discount_percentage defaultValue: 0
    }
}
