package library
import grails.gorm.MultiTenant

class Series implements MultiTenant<Series>
{
    String name
    boolean isactive
    int display_order
    String short_name
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[librarydepartment:LibraryDepartment,booktype:BookType,bookclassification:BookClassification,organization:Organization]
    static constraints = {
        display_order nullable: true
        bookclassification nullable: true
        short_name nullable: true
        librarydepartment nullable: true
        booktype nullable: true
    }

    static mapping = {
        isactive defaultValue: true
    }
}
