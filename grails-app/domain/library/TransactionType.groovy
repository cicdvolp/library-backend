package library
import grails.gorm.MultiTenant
class TransactionType implements MultiTenant<TransactionType>
{
    String name   //Online/Cheque/Cash/Card
    int display_order
    String short_name
    boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
        short_name nullable: true
    }
    static mapping = {
        isactive defaultValue: true
        display_order defaultValue: 0
    }
}

