package library
import grails.converters.JSON
import grails.gorm.multitenancy.Tenants

//sending
import groovy.json.JsonSlurper//receiving
class LoginController {
    def index() { }
    def process(){

        AuthService auth = new AuthService()
        // Read the data from the header it contains
        String body =  request.getReader().text
        def device =  request.getHeader("device")
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse"+dataresponse
        def uid =  dataresponse["uid"]
        def pass =  dataresponse["pass"]
        def dns =  dataresponse["dns"]
        def logindevice =  dataresponse["logindevice"]
        HashMap hm = new HashMap()
        //def log = Login.findByUsernameAndPasswordAndIsloginblocked(uid,pass,false)
        def log
        if(!(uid.toString().contains('@')))
        {
println("in")
            log = Login.findByGrno_empidAndPassword(uid,pass)
println(log)
        }
        else
        {
            println("out")
            log = Login.findByUsernameAndPassword(uid,pass)
        }

        if(log){
            log.lastlogindate = new Date()
            log.logindevice = logindevice
            log.save(failOnError:true, flush:true)
        }
        if(log == null){
            hm.put('msg',"Please check your credentials")
            render hm as JSON
            return
        }
        if(log.isloginblocked==true){
            hm.put('msg',"Login-Blocked")
            render hm as JSON
            return
        }

        if(log?.tenantclient?.isactive==true) {
            //block for organization to be decided
            hm.put("policy_read", true)
            if (log) {
                hm.put('msg', "200")
                hm.put('token', auth.gentoken(log.id))
                hm.put('uid', log.username)
               /* if(log.isfirstlogin == true){
                    hm.put('firstlogin', "1")
                }*/
                Tenants.withId(log.tenantclient.tenantid) {
                    User user = User.findByUsername(log.username)
                    hm.put("policy_read", true)
                }
            }
            else
                hm.put('msg',"NAK")
        }
        else
            hm.put('msg',"Tenant-Blocked")
        render hm as JSON
    }

    def forgotpassword(){
      //  println("In forgotpassword "+params)
        //AuthService auth = new AuthService()
        String body =  request.getReader().text

        NotificationService notification = new NotificationService()
        // println("body :: " + body)
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
       // println"dataresponce: "+dataresponse
        def uid =  dataresponse["uid"]
        //def dns =  dataresponse["dns"]

        HashMap hm = new HashMap()
        Login login = Login.findByUsername(uid)
        if(login) {
            OTP otp = notification.generateOTPforforgotPassword(uid,request.getRemoteAddr())
            // send email to user logic here
            String email_msg = "Forgot your Password?<br>"+"Dear User We received a password reset request for your EasyPariksha account. <br />"+"Your OTP to change the password is:"+"<b>"+otp.email_otp+"</b>"
            notification.sesmail("noreply@edupluscampus.com",uid,email_msg,"Reset your EasyPariksha Password")
            hm.put('msg', "200")
            hm.put('uid', uid)
            hm.put('otp', otp.email_otp)
           // println"hm: "+hm
        } else {
            hm.put('msg', "NAK")
        }
        render hm as JSON
        return
    }

    def checkpassword(){
        println("In checkpassword "+params)

        //AuthService auth = new AuthService()
        String body =  request.getReader().text

        // println("body :: " + body)
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        def uid =  dataresponse["uid"]
        def pass =  dataresponse["oldpassword"]
        def dns =  dataresponse["dns"]
        // println("pass :: " + pass + "uid :: " + uid)
        HashMap hm = new HashMap()
        def login = Login.findByUsernameAndPasswordAndIsloginblocked(uid, pass, false)
        if(login){
            hm.put('msg',"200")
            hm.put('uid',login.username)
        } else {
            hm.put('msg', "NAK")
        }
        //  println("res :: " + hm.msg)
        render hm as JSON
    }

    def changepassword() {
        println("In Login/changepassword " + params)
        //AuthService auth = new AuthService()
        String body = request.getReader().text

        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        def uid = dataresponse["uid"]
        def pass = dataresponse["password"]
        def confirmpass = dataresponse["confirmpassword"]
        //def dns =  dataresponse["dns"]
        // println("pass :: " + pass + "confirmpass ::" + confirmpass + "uid :: " + uid)
        HashMap hm = new HashMap()
        def login = Login.findByUsername(uid)
        if (pass.equals(confirmpass)) {
            if (login) {
                if(login.password == pass){
                    hm.put('msg', "samepassword")
                } else {
                    login.password = pass
                    login.password_update_date = new java.util.Date()
                    login.updation_username = uid
                    login.updation_date = new java.util.Date()
                    login.updation_ip_address = request.getRemoteAddr()
//                    login.isfirstlogin = false
                    login.save(flush: true, failOnError: true)
                    hm.put('msg', "200")
                    hm.put('uid', login.username)
                }
            } else {
                hm.put('msg', "NAK")
            }
        }
        render hm as JSON
    }

    def verifyOTPandChangePassword(){
        println("In verifyOTPandChangePassword: "+params)
        HashMap hm = new HashMap()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
       // println "dataresponse:" + dataresponse
        def uid =  dataresponse["uid"]
        def userOtp= dataresponse["otp"]
        def password= dataresponse["newpwd"]
        Login login=Login.findByUsername(uid)
        if(login){
            OTP otp = OTP.findByEmail(uid)
            //println"otp::: " +otp
            if(otp){
                NotificationService notification = new NotificationService()
                if(notification.verifyFPOTP(otp,userOtp,hm)){
                    login.password = password
                    login.updation_username = uid
                    login.updation_date = new java.util.Date()
                    login.updation_ip_address = request.getRemoteAddr()
                    login.save(flush: true, failOnError: true)
                    if(login){
                        hm.put('msg',"200")
                        hm.put('uid',login.username)
                    }
                    else{
                        hm.put('msg', "LNF")//Login Not Found
                    }
                    render hm as JSON
                    return
                }
                hm.put('msg',"ONM")//OTP Doesn't match
                render hm as JSON
                return
            }
            else{
                hm.put('msg', "ONA")//OTP not available
                render hm as JSON
                return
            }
        }
        else{
            hm.put("msg","LNF")//Login Not Found
            render hm as JSON
            return
        }
    }
}
