package library
import grails.gorm.MultiTenant
class Person implements MultiTenant<Person>
{
    String mobile_number
    String email
    String address
    String city
    String state
    String country
    String pin

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[user:User]
    static constraints = {
        mobile_number nullable:true
        email nullable:true
        address nullable:true
        city nullable:true
        state nullable:true
        country nullable:true
        pin nullable:true
    }
    static mapping = {
    }
}
