package library

import grails.gorm.MultiTenant

class Semester implements MultiTenant<Semester> {
    String sem
    String display_name
    int sequence
    boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
    }
    static mapping = {
        sequence defaultValue: 0
        isactive defaultValue: true
    }
}
