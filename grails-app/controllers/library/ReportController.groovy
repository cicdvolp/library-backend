package library

import grails.converters.JSON

class ReportController {

    def index() { }
    def getAllBook()
    {
        println "getAll issued Book"
        BookReportService bookReportService = new BookReportService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        bookReportService.getAllBook(request, hm,ip)
        render hm as JSON
    }
}
