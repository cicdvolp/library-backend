package library
import grails.converters.JSON
import grails.gorm.multitenancy.Tenants
import groovy.json.JsonSlurper
import org.apache.http.HttpResponse
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.json.JSONArray;
import org.json.JSONObject;
class ImportDataController {



    def synchlearner(){

        println("in synchlearner")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()
        ArrayList AluminiRecord = new ArrayList()
        HashMap hm = new HashMap()
        String purpose = "Synch Leaner From ERP"
        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization

                JSONObject json = new JSONObject();
                json.put("org", org?.name);
                CloseableHttpClient httpClient = HttpClientBuilder.create().build();
//                try {
                def erpurl = org?.erpwebsiteurl+"ERPToTOPSynch/getLearnerDataTPO"
                println("erpurl : "+erpurl)

                HttpPost request = new HttpPost(erpurl);
                StringEntity params = new StringEntity(json.toString());
                request.addHeader("content-type", "application/json");
                request.setEntity(params);
                HttpResponse response = httpClient.execute(request);
                int statusCode = response.getStatusLine().getStatusCode();
                println"statusCode : "+statusCode
                if (statusCode == 200) {
                    BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                    String line;
                    String line1;
                    int i = 0
                    while ((line = rd.readLine()) != null) {
                        if (i == 0) {
                            line1 = line
                        } else {
                            line1 += line
                        }
                        i++
                    }

                    JSONObject jobj = new JSONObject(line1)
                    String list = jobj.getString("list")
                    JSONArray studJsonArray = new JSONArray(list)
                    int stud_count = studJsonArray.length()

                    for (int j=0; j<stud_count; j++) {
                        def stud = studJsonArray.get(j)

                        if (stud != "") {
                            def grno = ""
                            def personalemail = ""
                            def officalemail = ""
                            def fullname = ""
                            def contact = ""

                            if (!(stud?.grno == null || stud?.grno == "" || stud?.grno == 'null' || !stud?.grno))
                                grno = stud?.grno?.trim()
                            if (!(stud?.personalemail == null || stud?.personalemail == "" || stud?.personalemail == 'null' || !stud?.personalemail))
                                personalemail = stud?.personalemail?.trim()
                            if (!(stud?.officalemail == null || stud?.officalemail == "" || stud?.officalemail == 'null' || !stud?.officalemail))
                                officalemail = stud?.officalemail?.trim()
                            if (!(stud?.fullname == null || stud?.fullname == "" || stud?.fullname == 'null' || !stud?.fullname))
                                fullname = stud?.fullname?.trim()

                            if (!(stud?.contact == null || stud?.contact == "" || stud?.contact == 'null' || !stud?.contact))
                                contact = stud?.contact?.trim()


                            if (grno) {
                                String username = grno + user?.organization?.loginattachementdomain

                                Login login = Login.findByUsername( username )

                                if (login == null) {
                                    login = new Login()

                                        login.username = username
                                        login.password = username
                                        login.grno_empid = grno

                                    login.creation_username = uid
                                    login.updation_username = uid
                                    login.creation_date = new Date()
                                    login.updation_date = new Date()
                                    login.creation_ip_address = 'ERP Synch'
                                    login.updation_ip_address = 'ERP Synch'
                                    login.tenantclient = log.tenantclient
                                    login.save(flush: true, failOnError: true)
                                }
                                UserType ut = UserType.findByName('Member')
                                User usr = User.findByUsernameAndOrganization( username, user?.organization )
                                println( "usr  :: " + usr )
                                if (usr == null) {
                                    usr = new User()
                                    usr.username = username
                                    usr.total_issued_books = 0
                                    usr.creation_username = uid
                                    usr.updation_username = uid
                                    usr.creation_date = new Date()
                                    usr.updation_date = new Date()
                                    usr.creation_ip_address = 'ERP Synch'
                                    usr.updation_ip_address = 'ERP Synch'
                                    usr.organization = user.organization
                                    usr.usertype = ut
                                    usr.save(flush: true, failOnError: true)
                                    Role role = Role.findByName("Member")
                                    usr.addToRole(role)
                                    usr.save(flush: true,failOnError: true)
                                }

                                Person person = Person.findByUser( usr )
                                //println( "person  :" + person )

                                if (person == null) {

                                     person = new Person()

                                    person.mobile_number = contact
                                    person.email = personalemail
                                    person.creation_username = user.username
                                    person.updation_username = user.username
                                    person.creation_date = new Date()
                                    person.updation_date = new Date()
                                    person.creation_ip_address = 'ERP Synch'
                                    person.updation_ip_address = 'ERP Synch'
                                    person.user=usr
                                    person.save(flush: true, failOnError: true)
                                }
                                else {
                                    println("else person save")
                                    person.mobile_number = contact
                                    person.email = personalemail
                                    person.updation_username = user.username
                                    person.updation_date = new java.util.Date()
                                    person.updation_ip_address = 'ERP Synch'
                                    person.save( failOnError: true, flush: true )
                                }
                                Program program = Program.findByOrganizationAndName( user.organization, stud?.program )
                                 Member member = Member.findByUser( usr )
                                if(member==null)
                                     member = Member.findByRegistration_number( grno )

                                if (member == null) {
                                     member = new Member()
                                    member.registration_number = grno
                                    member.name = fullname
                                    member.date_of_membership = new Date()
                                    member.creation_username = uid
                                    member.updation_username = uid
                                    member.creation_date = new Date()
                                    member.updation_date = new Date()
                                    member.creation_ip_address = 'ERP Synch'
                                    member.updation_ip_address = 'ERP Synch'
                                    member.organization = user.organization
                                    member.user = usr
                                    member.person = person
                                    member.program=program
                                    member.save(flush: true, failOnError: true)
                                }else {
                                    member.name = fullname
                                    member.user = usr
                                    member.person = person
                                    member.program=program
                                    member.save(flush: true, failOnError: true)
                                }
                            }
                        }
                    }
                }
//                } catch (Exception ex) {
//
//                } finally {
//                    httpClient.close();
//                }

                render hm as JSON
                return
            }
        }
        render hm as JSON
        return
    }

    def getphotfromerp(){

        println("in getphotfromerp")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()
        ArrayList AluminiRecord = new ArrayList()
        HashMap hm = new HashMap()
        String purpose = "Synch Leaner From ERP"
        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization

                String body = request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                println dataresponse

                JSONObject json = new JSONObject();
                json.put("org", org?.name);
                json.put("prn", dataresponse.prn);
                CloseableHttpClient httpClient = HttpClientBuilder.create().build();
//                try {
                def erpurl = org?.erpwebsiteurl+"ERPToTOPSynch/getphotfromerp"
                println("erpurl : "+erpurl)

                HttpPost request = new HttpPost(erpurl);
                StringEntity params = new StringEntity(json.toString());
                request.addHeader("content-type", "application/json");
                request.setEntity(params);
                HttpResponse response = httpClient.execute(request);
                int statusCode = response.getStatusLine().getStatusCode();
                println"statusCode : "+statusCode

                    BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                    String line;
                    String line1;
                    int i = 0
                    while ((line = rd.readLine()) != null) {
                        if (i == 0) {
                            line1 = line
                        } else {
                            line1 += line
                        }
                        i++
                    }

                    JSONObject jobj = new JSONObject(line1)
                    String list = jobj.getString("data")
                def data1=jsonSluper.parseText(list)
                    println data1
hm.put("isphoto",data1.photo)
hm.put("url",data1.url)
                hm.put("msg","200")
//                    JSONArray studJsonArray = new JSONArray(list)
//                    int stud_count = studJsonArray.length()
//                    def stud = studJsonArray.get(j)





                render hm as JSON
                return
            }
        }
        render hm as JSON
        return
    }

    def synchlearnerdivision(){

        println("in synchlearnerdivision")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()
        ArrayList AluminiRecord = new ArrayList()
        HashMap hm = new HashMap()
        String purpose = "Synch Leaner From ERP"
        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization


                String body = request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                println dataresponse
                AcademicYear academicYear = AcademicYear.findById(dataresponse.ay)
//                Program program1 = Program.findById(dataresponse.program_name)
//                Semester semester = Semester.findById(dataresponse.semester)
//                Year yr = Year.findById(dataresponse.year)
                JSONObject json = new JSONObject();
                json.put("org", org?.name);
                json.put("ay", academicYear?.ay);
//                json.put("program_name", program1?.name);
//                json.put("semester", semester?.sem);
//                json.put("year", yr?.year);
                CloseableHttpClient httpClient = HttpClientBuilder.create().build();
//                try {
                def erpurl = org?.erpwebsiteurl+"ERPToTOPSynch/getLearnerDivisionlibrary"
                println("erpurl : "+erpurl)

                HttpPost request = new HttpPost(erpurl);
                StringEntity params = new StringEntity(json.toString());
                request.addHeader("content-type", "application/json");
                request.setEntity(params);
                HttpResponse response = httpClient.execute(request);
                int statusCode = response.getStatusLine().getStatusCode();
                println"statusCode : "+statusCode
//                if (statusCode == 200) {
                    BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                    String line;
                    String line1;
                    int i = 0
                    while ((line = rd.readLine()) != null) {
                        if (i == 0) {
                            line1 = line
                        } else {
                            line1 += line
                        }
                        i++
                    }

                    JSONObject jobj = new JSONObject(line1)
                    String list = jobj.getString("list")

                    JSONArray studJsonArray = new JSONArray(list)
                    int stud_count = studJsonArray.length()
                def stud1 = studJsonArray.get(0)
                println stud1
                if(stud1.code=="404")
                {
                    hm.put("msg",stud1.error)
                    render hm as JSON
                    return
                }else {

                    for (int j=1; j<stud_count; j++) {
                        def stud = studJsonArray.get(j)

                        if (stud != "") {
                            def grno = ""
                            def division = ""
                            def rollno = ""
                            def year = ""
                            def program = ""
                            def shift = ""
                            def ispassout = false
                            def semester = ""

                            if (!(stud?.grno == null || stud?.grno == "" || stud?.grno == 'null' || !stud?.grno))
                                grno = stud?.grno?.trim()
                            if (!(stud?.division == null || stud?.division == "" || stud?.division == 'null' || !stud?.division))
                                division = stud?.division
                            if (!(stud?.rollno == null || stud?.rollno == "" || stud?.rollno == 'null' || !stud?.rollno))
                                rollno = stud?.rollno
                            if (!(stud?.year == null || stud?.year == "" || stud?.year == 'null' || !stud?.year))
                                year = stud?.year
                            if (!(stud?.ispassout == null || stud?.ispassout == "" || stud?.ispassout == 'null' || !stud?.ispassout))
                                ispassout = stud?.ispassout
                            if (!(stud?.program == null || stud?.program == "" || stud?.program == 'null' || !stud?.program))
                                program = stud?.program
                            if (!(stud?.shift == null || stud?.shift == "" || stud?.shift == 'null' || !stud?.shift))
                                shift = stud?.shift
                            if (!(stud?.sem == null || stud?.sem == "" || stud?.sem == 'null' || !stud?.sem))
                                semester = stud?.sem

                            if (grno) {

                                Member member=Member.findByRegistration_numberAndOrganization(grno,org)
                                if(Member!=null)
                                {
                                    Year year1=Year.findByYearAndOrganization(year,org)
                                    Shift shift1=Shift.findByShiftAndOrganization(shift,org)
                                    Program program1=Program.findByNameAndOrganization(program,org)
                                    Semester semester1=Semester.findBySemAndOrganization(semester,org)
                                    LearnerDivision learnerDivision=LearnerDivision.findByMemberAndSemesterAndAcademicAndYearAndProgramAndOrganization(member,semester1,academicYear,year1,program1,org)
                                    if(learnerDivision==null )
                                    {learnerDivision=new LearnerDivision()
                                        learnerDivision. rollno=rollno
                                        learnerDivision. division=division
                                        learnerDivision. isactive=true
                                        learnerDivision. creation_username=uid
                                        learnerDivision. updation_username=uid
                                        learnerDivision. creation_date= new Date()
                                        learnerDivision. updation_date= new Date()
                                        learnerDivision. creation_ip_address="ERP SYNCH"
                                        learnerDivision. updation_ip_address="ERP SYNCH"
                                        learnerDivision.member=member
                                        learnerDivision.organization=org
                                        learnerDivision.academic=academicYear

                                        learnerDivision.semester=semester1

                                        learnerDivision.year=year1
                                        learnerDivision.shift=shift1

                                        learnerDivision.program=program1

                                      learnerDivision.save(failOnError: true,flush: true)

                                    }
                                }

                            }
                        }
                    }

                }

                hm.put("msg","200")
                render hm as JSON
                return
                }


            }
        }



    def synchemployee(){
        println("in synchemployee")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")


        AuthService auth = new AuthService()
        ArrayList AluminiRecord = new ArrayList()
        HashMap hm = new HashMap()
        String purpose = "Synch employee From ERP"
        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)

                def org = user.organization

                JSONObject json = new JSONObject();
                json.put("org", org?.name);
                CloseableHttpClient httpClient = HttpClientBuilder.create().build();
//                try {
                def erpurl = org?.erpwebsiteurl+"ERPToTOPSynch/getEmpDataTPO"
                println("erpurl : "+erpurl)

                HttpPost request = new HttpPost(erpurl);
                StringEntity params = new StringEntity(json.toString());
                request.addHeader("content-type", "application/json");
                request.setEntity(params);
                HttpResponse response = httpClient.execute(request);
                int statusCode = response.getStatusLine().getStatusCode();
                println"statusCode : "+statusCode
                if (statusCode == 200) {
                    BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                    String line;
                    String line1;
                    int i = 0
                    while ((line = rd.readLine()) != null) {
                        if (i == 0) {
                            line1 = line
                        } else {
                            line1 += line
                        }
                        i++
                    }

                    JSONObject jobj = new JSONObject(line1)
                    String list = jobj.getString("list")
                    JSONArray studJsonArray = new JSONArray(list)
                    int stud_count = studJsonArray.length()

                    for (int j=0; j<stud_count; j++) {
                        def stud = studJsonArray.get(j)
                        println("stud : "+stud)
                        if (stud != "") {
                            def Employee_Code = ""
                            def Full_Name = ""
                            def Email_Id = ""
                            def Mobile_No = ""
                            def Prog = ""
                            def Department = ""
                            println("stud?.grno : "+stud?.Employee_Code)
                            if (!(stud?.Employee_Code == null || stud?.Employee_Code == "" || stud?.Employee_Code == 'null' || !stud?.Employee_Code))
                                Employee_Code = stud?.Employee_Code?.trim()
                            if (!(stud?.Full_Name == null || stud?.Full_Name == "" || stud?.Full_Name == 'null' || !stud?.Full_Name))
                                Full_Name = stud?.Full_Name?.trim()
                            if (!(stud?.Email_Id == null || stud?.Email_Id == "" || stud?.Email_Id == 'null' || !stud?.Email_Id))
                                Email_Id = stud?.Email_Id?.trim()
                            if (!(stud?.Mobile_No == null || stud?.Mobile_No == "" || stud?.Mobile_No == 'null' || !stud?.Mobile_No))
                                Mobile_No = stud?.Mobile_No?.trim()
                            if (!(stud?.Program == null || stud?.Program == "" || stud?.Program == 'null' || !stud?.Program))
                                Prog = stud?.Program?.trim()
                            if (!(stud?.Department == null || stud?.Department == "" || stud?.Department == 'null' || !stud?.Department))
                                Department = stud?.Department?.trim()

                            println("Employee_Code  "+Employee_Code)
                            if (Employee_Code) {
                                HashMap temp=new HashMap()
                                String username=Employee_Code.toString().trim()+org.loginattachementdomain
                                Login login=Login.findByUsername(username)
                                if(login==null) {
                                    login = new Login()

                                    login.username = username
                                    login.password = username
                                    login.grno_empid = Employee_Code
                                    login.creation_username = uid
                                    login.updation_username = uid
                                    login.creation_date = new Date()
                                    login.updation_date = new Date()
                                    login.creation_ip_address = 'ERP Synch'
                                    login.updation_ip_address = 'ERP Synch'
                                    login.tenantclient = log.tenantclient
                                    login.save(flush: true, failOnError: true)
                                }
                                else{

                                }
                                UserType ut = UserType.findByName('Employee')
                                User usr = User.findByUsernameAndOrganization( username, user?.organization )
                                println( "usr  :: " + usr )
                                if (usr == null) {
                                    usr = new User()
                                    usr.username = username
                                    usr.total_issued_books = 0
                                    usr.creation_username = uid
                                    usr.updation_username = uid
                                    usr.creation_date = new Date()
                                    usr.updation_date = new Date()
                                    usr.creation_ip_address = 'ERP Synch'
                                    usr.updation_ip_address = 'ERP Synch'
                                    usr.organization = user.organization
                                    usr.usertype = ut
                                    usr.save(flush: true, failOnError: true)
                                    Role role = Role.findByName("Member")
                                    usr.addToRole(role)
                                    usr.save(flush: true,failOnError: true)
                                }
                               else {

                                }
                                Person person = Person.findByUser( usr )
                                println( "person  :" + person )

                                if (person == null) {

                                    person = new Person()

                                    person.mobile_number = Mobile_No
                                    person.email = Email_Id
                                    person.creation_username = user.username
                                    person.updation_username = user.username
                                    person.creation_date = new Date()
                                    person.updation_date = new Date()
                                    person.creation_ip_address = 'ERP Synch'
                                    person.updation_ip_address = 'ERP Synch'
                                    person.user=usr
                                    person.save(flush: true, failOnError: true)
                                }
                                else {
                                    println("else person save")
                                    person.mobile_number = Mobile_No
                                    person.email = Email_Id
                                    person.updation_username = user.username
                                    person.updation_date = new java.util.Date()
                                    person.updation_ip_address = 'ERP Synch'
                                    person.save( failOnError: true, flush: true )
                                }
                                Program program = Program.findByOrganizationAndName( user.organization, Prog )
                                Employee employee = Employee.findByUser( usr )
                                if(employee==null)
                                    employee = Employee.findByEmployee_code( Employee_Code )
                                println( "employee code  " + employee )
                                if (employee == null) {
                                    employee=new Employee()
                                    employee.employee_code = Employee_Code
                                    employee.name = Full_Name
                                    if(program){
                                        employee.program = program
                                    }
                                    employee.creation_username = uid
                                    employee.updation_username = uid
                                    employee.creation_date = new Date()
                                    employee.updation_date = new Date()
                                    employee.creation_ip_address = 'ERP Synch'
                                    employee.updation_ip_address = 'ERP Synch'
                                    employee.organization = user.organization
                                    employee.user = usr
                                    employee.person = person
                                    employee.save(flush: true, failOnError: true)


                                    Role role
                                    role = Role.findByNameIlike("%Teacher%")
                                    usr.addToRole(role)
                                    usr.save(flush: true,failOnError: true)
                                }else {
                                    employee.name = Full_Name
                                    employee.user = usr
                                    employee.person = person
                                    employee.program=program
                                    employee.save(flush: true, failOnError: true)
                                }



                            }
                        }
                    }
                }
//

                render hm as JSON
                return
            }
        }
        render hm as JSON
        return
    }
    def synchprog(){
        println("in synchprog")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")


        AuthService auth = new AuthService()
        ArrayList AluminiRecord = new ArrayList()
        HashMap hm = new HashMap()
        String purpose = "Synch employee From ERP"
        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)

                def org = user.organization

                JSONObject json = new JSONObject();
                json.put("org", org?.name);
                CloseableHttpClient httpClient = HttpClientBuilder.create().build();
//                try {
                def erpurl = org?.erpwebsiteurl+"ERPToTOPSynch/getProgramlib"
                println("erpurl : "+erpurl)

                HttpPost request = new HttpPost(erpurl);
                StringEntity params = new StringEntity(json.toString());
                request.addHeader("content-type", "application/json");
                request.setEntity(params);
                HttpResponse response = httpClient.execute(request);
                int statusCode = response.getStatusLine().getStatusCode();
                println"statusCode : "+statusCode
                if (statusCode == 200) {
                    BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                    String line;
                    String line1;
                    int i = 0
                    while ((line = rd.readLine()) != null) {
                        if (i == 0) {
                            line1 = line
                        } else {
                            line1 += line
                        }
                        i++
                    }

                    JSONObject jobj = new JSONObject(line1)
                    String list = jobj.getString("list")
                    JSONArray studJsonArray = new JSONArray(list)
                    int stud_count = studJsonArray.length()

                    for (int j=0; j<stud_count; j++) {
                        def stud = studJsonArray.get(j)
                        println("stud : "+stud)
                        if (stud != "") {
                            def program = ""
                            def programtype = ""
                            def department = ""

                            if (!(stud?.program == null || stud?.program == "" || stud?.program == 'null' || !stud?.program))
                                program = stud?.program
                            if (!(stud?.programtype == null || stud?.programtype == "" || stud?.programtype == 'null' || !stud?.programtype))
                                programtype = stud?.programtype
                            if (!(stud?.department == null || stud?.department == "" || stud?.department == 'null' || !stud?.department))
                                department = stud?.department


                            if (program) {
                                HashMap temp=new HashMap()

                                ProgramType programType1 =ProgramType.findByNameAndOrganization(programtype,org)
                                if(programType1==null){
                                    programType1=new ProgramType()
                                    programType1.name=programtype
                                    programType1.creation_username=uid
                                    programType1.updation_username=uid
                                    programType1.creation_date = new Date()
                                    programType1.updation_date = new Date()
                                    programType1.creation_ip_address = 'ERP Synch'
                                    programType1.updation_ip_address = 'ERP Synch'
                                    programType1.organization = org
                                    programType1.save(flush: true, failOnError: true)
                                }
                                ERPDepartment erpDepartment= ERPDepartment.findByNameAndOrganization(department,org)
                                if(erpDepartment==null)
                                {
                                    erpDepartment=new ERPDepartment()
                                    erpDepartment.name=department
                                    erpDepartment.creation_username=uid
                                    erpDepartment.updation_username=uid
                                    erpDepartment.creation_date = new Date()
                                    erpDepartment.updation_date = new Date()
                                    erpDepartment.creation_ip_address = 'ERP Synch'
                                    erpDepartment.updation_ip_address = 'ERP Synch'
                                    erpDepartment.organization = org
                                    erpDepartment.save(flush: true, failOnError: true)
                                }
                                Program program1 =Program.findByNameAndOrganization(program,org)
                                if(program1==null){
                                    program1=new Program()
                                    program1. name=program
                                    program1. creation_username=uid
                                    program1. updation_username=uid
                                    program1. creation_date=new Date()
                                    program1. updation_date=new Date()
                                    program1. creation_ip_address='ERP Synch'
                                    program1. updation_ip_address='ERP Synch'

                                    program1.programType=programType1
                                    program1.erpdepartment=erpDepartment
                                    program1.organization=org
                                    program1.save(flush: true, failOnError: true)
                                }
                            }
                        }
                    }
                }
//

                render hm as JSON
                return
            }
        }
        render hm as JSON
        return
    }
    def ImportMenu()
    {
        println "get import menu"
        ImportCsvService adminimportService = new ImportCsvService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminimportService.importMenu(request, ip ,hm)
        render hm as JSON
    }

    def bulkdelete()
    {
        println "bulkdelete"
        ImportCsvService adminimportService = new ImportCsvService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminimportService.bulkdelete(request, hm ,ip)
        render hm as JSON
    }

    def importEmployeeData()
    {
        println "get import employee Data"
        ImportCsvService adminimportService = new ImportCsvService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminimportService.importEmployeeData(request, ip ,hm)
        render hm as JSON
    }
    def importMemberData()
    {
        println "get import Member Data"
        ImportCsvService adminimportService = new ImportCsvService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminimportService.importMemberData(request, ip ,hm)
        render hm as JSON
    }
    def editInstructor()
    {
        println "edit employee Data"
        ImportCsvService adminimportService = new ImportCsvService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminimportService.editInstructor(request, ip ,hm)
        render hm as JSON
    }
    def editMember()
    {
        println "edit Member Data"
        ImportCsvService adminimportService = new ImportCsvService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminimportService.editMember(request, ip ,hm)
        render hm as JSON
    }
    def addEmployee()
    {
        println "add employee Data"
        ImportCsvService adminimportService = new ImportCsvService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminimportService.addEmployee(request, ip ,hm)
        render hm as JSON
    }
    def addMember()
    {
        println "add member Data"
        ImportCsvService adminimportService = new ImportCsvService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminimportService.addMember(request, ip ,hm)
        render hm as JSON
    }
    def importEmployee()
    {
        println "import employee Data"
        ImportCsvService adminimportService = new ImportCsvService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminimportService.importEmployee(request, ip ,hm)
        render hm as JSON
    }
    def importMember()
    {
        println "import employee Data"
        ImportCsvService adminimportService = new ImportCsvService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        adminimportService.importMember(request, ip ,hm)
        render hm as JSON
    }

    def importBook()
    {
        println "import book Data"+params
        ImportCsvService importService = new ImportCsvService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        importService.importBook1(request,hm,ip,params)
        render hm as JSON
    }

    def importIssueBook1()
    {
        println "import book issue Data"
        ImportCsvService importService = new ImportCsvService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        importService.importIssueBook1(request,hm,ip)
        render hm as JSON
    }
}
