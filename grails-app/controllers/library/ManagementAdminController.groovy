package library
import grails.converters.JSON

class ManagementAdminController {

    def index() { }

    def reports()
    {
        println "management reports"
        ManagementAdminService managementService = new ManagementAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        managementService.reports(request,ip, hm,)
        render hm as JSON
    }

    def getOrg()
    {
        println "get organization"
        ManagementAdminService managementService = new ManagementAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        managementService.getOrg(request,ip, hm,)
        render hm as JSON
    }

    def accessionReport()
    {
        println "management accessionReport"
        ManagementAdminService managementService = new ManagementAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        managementService.accessionReport(request,ip, hm,)
        render hm as JSON
    }

    def overdueData()
    {
        println "management overdueData"
        ManagementAdminService managementService = new ManagementAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        managementService.overdueData(request,ip, hm,)
        render hm as JSON
    }

    def getdeptData()
    {
        println "getdeptData"
        ManagementAdminService managementService = new ManagementAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        managementService.getdeptData(request,ip, hm,)
        render hm as JSON
    }

    def dailyData()
    {
        println " management dailyData report"
        ManagementAdminService managementService = new ManagementAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        managementService.dailyData(request,ip, hm,)
        render hm as JSON
    }
    def memberWise()
    {
        println "management memberWise report"
        ManagementAdminService managementService = new ManagementAdminService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        managementService.memberWise(request,ip, hm,)
        render hm as JSON
    }

    def getAllOrgFineData()
    {
        println "In getAllOrgfine data"
        ManagementAdminService managementService = new ManagementAdminService()
        HashMap hm = new HashMap()
        managementService.getAllOrgFineData(request,hm)
        render hm as JSON
    }
}
