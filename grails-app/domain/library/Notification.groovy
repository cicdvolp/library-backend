package library
import grails.gorm.MultiTenant
class Notification implements MultiTenant<Notification>
{
    String message
    String subject
    Date sent_date
    boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[notificationtype:NotificationType,notificationmethod:NotificationMethod,user:User]
    static mapping = {
        isactive defaultValue: true
    }
}
