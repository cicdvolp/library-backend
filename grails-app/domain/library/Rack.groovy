package library
import grails.gorm.MultiTenant
class Rack implements MultiTenant<Rack>
{
    int display_order
    String  rack_number
    String block_number
    String location_identifier
    int capacity
    int available
    boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[bookcategory:BookCategory]
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
        capacity defaultValue: 0
        available defaultValue: 0
        bookcategory nullable: true
    }
}
