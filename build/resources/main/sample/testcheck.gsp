<html>
<head>
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
</head>
<body>
<table id="table_id" class="display">
    <thead>
        <tr>
            <th>Srno</th>
            <th>Student</th>
            <th>UID</th>
            <th>Standard</th>
            <th>Course</th>
            <th>Test Name</th>
            <th>Marks</th>
            <th>Start</th>
            <th>End</th>
        </tr>
    </thead>
    <tbody>
    <g:each in="${submissions}" var="sub" status="i">
        <tr>
            <td>${i+1}</td>
            <td>${sub?.learner?.person?.firstName} ${sub?.learner?.person?.lastName}</td>
            <td>${sub?.learner?.uid}</td>
            <td>${sub?.learner?.standard?.standard}</td>
            <td>${sub?.courseoffering?.course?.course_name}</td>
            <td>${sub?.quizoffering?.title}</td>
            <td>${sub?.marks}</td>
            <td><g:formatDate format="dd-MM-yyyy" date="${sub?.start_date}"/></td>
            <td><g:formatDate format="dd-MM-yyyy" date="${sub?.end_date}"/></td>
        </tr>
    </g:each>
    </tbody>
</table>
</body>
<script>
$(document).ready( function () {
    $('#table_id').DataTable(
    {
        paging: false
    }
    );
} );
</script>
</html>