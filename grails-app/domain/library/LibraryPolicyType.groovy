package library
import grails.gorm.MultiTenant
class LibraryPolicyType implements MultiTenant<LibraryPolicyType>
{
    String name
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization]

    static mapping = {
        isactive defaultValue: true
        organization nullable: true
    }
}
