package library

import grails.gorm.multitenancy.Tenants
import grails.gorm.transactions.Transactional

import java.security.SecureRandom

@Transactional
class AuthService {

    def serviceMethod() {

    }
    def tokencreate(){
        final SecureRandom secureRandom = new SecureRandom(); //threadsafe
        final Base64.Encoder base64Encoder = Base64.getUrlEncoder(); //threadsafe
        byte[] randomBytes = new byte[128];
        secureRandom.nextBytes(randomBytes);
        return base64Encoder.encodeToString(randomBytes);
    }
    def gentoken(logid){
        println("in gentoken")
        def login = Login.findById(logid)
        Date now = new Date()
        final long HOUR = 3600*1000;
        login.tokenexpiry = new Date(now.getTime() + HOUR)
        login.token = tokencreate()
        login.save(failOnError:true)
        return login.token
    }
    def settoken(){

    }
    def checkauth(token,uid,hm,url,request,purpose){

        hm.put("msg","200")
        
    }
//    def checkauth(token,uid,hm,url,request,purpose){
//        Login log = Login.findByUsernameAndToken(uid,token)
//        hm.put("token_status","NC")
//        if(log==null){
//            hm.put("msg","401")
//        }
//        else{
//
//            Tenants.withId(log?.tenantclient?.tenantid) {
//                User user = User.findByUsername(uid)
//                // println "user:"+user
//                if (user != null) {
//                    /*UserLogs logs = new UserLogs()
//                    logs.activity = purpose
//                    logs.date = new Date()
//                    logs.organization = user.organization
//                    logs.user = user
//
//                    logs.creation_date = new Date()
//                    logs.username = uid
//                    logs.creation_ip_address = request.getRemoteAddr()
//                    logs.updation_ip_address = request.getRemoteAddr()
//                    logs.updation_date = new Date()
//                    logs.updation_username = uid
//                    logs.save(flush: true, failOnError: true)*/
//                }
//            }
//
//            //hm.put("msg","200")
//            //return
//            //////////////////////////////////////////////////////////////////////will check later
//            if(!rolelinkauth(uid,url))
//            {
//                hm.put("msg","403")
//                return
//            }
//            Date now = new Date()
//            if(log.tokenexpiry < now){
//                hm.put("msg","200")
//                hm.put("token_status","token-refreshed")
//                hm.put("token",gentoken(log.id))
//                hm.put("uid",uid)
//            }
//            else{
//                hm.put("msg","200")
//            }
//        }
//    }
    def rolelinkauth(uid,url){
        println " rolelinkauth url:"+url
        String[] segments = url.split("/");
        String router_path = segments[segments.length-1]
        println "router_path:"+router_path
        Login log = Login.findByUsername(uid)
        println "uid---"+uid
        //for admin skip
        if(log.tenantclient==null){
            return true
        }
        //
        println "log.username -----> " +log.username
        Tenants.withId(log.tenantclient.tenantid) {
            def user = User.findByUsername(log.username)
            def rl = RoleLinks.findAllWhere(
                    linkname:router_path       //--> /app-link  for app
            )
            println rl
            if(rl.size()==0){
                return true
            }
            rl=rl.findAll{it.isactive==true}
            Set from_rl = rl?.role?.id
            Set from_user = user?.role?.id
            def common = from_rl.intersect(from_user)
            //check link active
            if(common.size()>0) {
                //check module is activated
                /*for(roleid in common){
                    if(Role.findById(roleid).roletype.isactive==true){
                        return true
                    }
                }
                return false*/
                return true
            }
            else
                return false
        }

    }
    def createFileName(){
        int len = 10
        String numbers = "0123456789qwertyuiopasdfghjklzxcvbnm";
        Random rndm_method = new Random();
        char[] filename = new char[len];
        for (int i = 0; i < len; i++)
        {
            filename[i] =numbers.charAt(rndm_method.nextInt(numbers.length()));
        }
        return filename;
    }
    def createCode(){
        int len = 6
        String numbers = "0123456789QWERTYUIOPASDFGHJKLZXCVBNM";
        Random rndm_method = new Random();
        char[] code = new char[len];
        for (int i = 0; i < len; i++)
        {
            code[i] =numbers.charAt(rndm_method.nextInt(numbers.length()));
        }
        return code;
    }

}
