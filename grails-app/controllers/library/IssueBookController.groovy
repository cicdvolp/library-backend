package library
import grails.converters.JSON
import grails.gorm.multitenancy.Tenants
import groovy.json.JsonSlurper

class IssueBookController {

    def index() { }

    def getEmpMemData()
    {
        println "get all employee student Data"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        adminService.fetchAllUser(request ,hm)
        render hm as JSON
    }
    def getbooksinfo()
    {
        println "get all employee student Data"

        HashMap hm = new HashMap()

        BookOperationService bookOperationService = new BookOperationService()
        String ip = request.getRemoteAddr()
        def booksinfo = bookOperationService.getAllBookInformation(request,ip)
        hm.put("allBookInfo",booksinfo)
        hm.put("msg", "200")
        render hm as JSON
    }
    def getbooksinfo1()
    {
        println "getbooksinfo1"

        HashMap hm = new HashMap()

        BookOperationService bookOperationService = new BookOperationService()
        String ip = request.getRemoteAddr()
        def booksinfo = bookOperationService.getAllBookInformation1(request,ip)
        hm.put("allBookInfo",booksinfo)
        hm.put("msg", "200")
        render hm as JSON
    }

    def getbooksinfo1HOME()
    {
        println "get all employee student Data"

        HashMap hm = new HashMap()

        BookOperationService bookOperationService = new BookOperationService()
        String ip = request.getRemoteAddr()
        def booksinfo = bookOperationService.getAllBookInformation1HOME(request,ip)
        hm.put("allBookInfo",booksinfo)
        hm.put("msg", "200")
        render hm as JSON
    }
    def getauthor_new()
    {
        println "get getauthor_new"

        HashMap hm = new HashMap()

        BookOperationService bookOperationService = new BookOperationService()
        String ip = request.getRemoteAddr()
        def booksinfo = bookOperationService.getauthor_new(request,ip)
        hm.put("allBookInfo",booksinfo)
        hm.put("msg", "200")
        render hm as JSON
    }
    def getauthor_newHOME()
    {
        println "get getauthor_newhome"

        HashMap hm = new HashMap()

        BookOperationService bookOperationService = new BookOperationService()
        String ip = request.getRemoteAddr()
        def booksinfo = bookOperationService.getauthor_newHOME(request,ip)
        hm.put("allBookInfo",booksinfo)
        hm.put("msg", "200")
        render hm as JSON
    }
    def getkeyword()
    {
        println "get getkeyword"
        BookOperationService bookOperationService = new BookOperationService()
        HashMap hm = new HashMap()
        bookOperationService.getkeyword(request ,hm)

        render hm as JSON
    }
    def getkeywordHOME()
    {
        println "get getkeyword"
        BookOperationService bookOperationService = new BookOperationService()
        HashMap hm = new HashMap()
        bookOperationService.getkeywordHOME(request ,hm)

        render hm as JSON
    }
    def getsubject()
    {
        println "get getsubject"

        HashMap hm = new HashMap()

        BookOperationService bookOperationService = new BookOperationService()
        String ip = request.getRemoteAddr()
        def booksinfo = bookOperationService.getsubject(request,ip)
        hm.put("allBookInfo",booksinfo)
        hm.put("msg", "200")
        render hm as JSON
    }
    def getsubjecthome()
    {
        println "get getsubject"

        HashMap hm = new HashMap()

        BookOperationService bookOperationService = new BookOperationService()
        String ip = request.getRemoteAddr()
        def booksinfo = bookOperationService.getsubjecthome(request,ip)
        hm.put("allBookInfo",booksinfo)
        hm.put("msg", "200")
        render hm as JSON
    }
    def getbooksinfo2()
    {
        println "get all employee student Data"

        HashMap hm = new HashMap()

        BookOperationService bookOperationService = new BookOperationService()
        String ip = request.getRemoteAddr()
        def booksinfo = bookOperationService.getAllBookInformation2(request,ip)
        hm.put("allBookInfo",booksinfo)
        hm.put("msg", "200")
        render hm as JSON
    }
    def getEmpMemInformation()
    {
        println "get Emp Mem Information Data"
        BookOperationService bookOperationService = new BookOperationService()
        HashMap hm = new HashMap()
        bookOperationService.getEmpMemInformation1(request ,hm)
        render hm as JSON
    }
    def getEmpMemInformationbyUser_ID()
    {
        println "get Emp Mem getEmpMemInformationbyUser_ID Data"
        BookOperationService bookOperationService = new BookOperationService()
        HashMap hm = new HashMap()
        bookOperationService.getEmpMemInformationbyUser_ID(request ,hm)
        render hm as JSON
    }
    def saveUserActiveOrNot()
    {
        println "saveUserActiveOrNot"
        BookOperationService bookOperationService = new BookOperationService()
        HashMap hm = new HashMap()
        bookOperationService.saveUserActiveOrNot(request ,hm)
        render hm as JSON
    }
    def getEmpMemInformationbyprn()
    {
        println "get Emp Mem Information Data by PRN"
        BookOperationService bookOperationService = new BookOperationService()
        HashMap hm = new HashMap()
        bookOperationService.getEmpMemInformationbyprn(request ,hm)
        render hm as JSON
    }
    def getemployeehistory()
    {
        println "get getemployeehistory"
        BookOperationService bookOperationService = new BookOperationService()
        HashMap hm = new HashMap()
        bookOperationService.getemployeehistory(request ,hm)
        render hm as JSON
    }

    def editMemberIssueBook()
    {
        println "in editMemberIssueBook"
        BookOperationService bookOperationService = new BookOperationService()
        HashMap hm = new HashMap()
        bookOperationService.editMemberIssueBook(request ,hm)
        render hm as JSON
    }

    def getemployeehistorybyprn()
    {
        println "get getemployeehistorybyprn"
        BookOperationService bookOperationService = new BookOperationService()
        HashMap hm = new HashMap()
        bookOperationService.getemployeehistorybyprn(request ,hm)
        render hm as JSON
    }
    def getBookItmeInformation()
    {
        println "get get Book Itme Information"
        BookOperationService bookOperationService = new BookOperationService()
        HashMap hm = new HashMap()
        bookOperationService.getBookItmeInformation(request ,hm)
        render hm as JSON
    }
    def getBookItmeInformationbyaccno()
    {
        println "get get Book Itme Information"
        BookOperationService bookOperationService = new BookOperationService()
        HashMap hm = new HashMap()
        bookOperationService.getBookItmeInformationbyaccno(request ,hm)
        render hm as JSON
    }
    def memberIssueBook()
    {
        println "member Issue Book"
        BookOperationService bookOperationService = new BookOperationService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        bookOperationService.memberIssueBook(request ,hm , ip)
        render hm as JSON
    }
    def renewBook()
    {
        println "member renew Book"
        BookOperationService bookOperationService = new BookOperationService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        bookOperationService.renewBook(request ,hm , ip)
        render hm as JSON
    }
    def getReserveBookA_no()
    {
        println "get Reserve Book A_no"
        BookOperationService bookOperationService = new BookOperationService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        bookOperationService.getReserveBookA_no(request ,hm , ip)
        render hm as JSON
    }
    def getBookA_no()
    {
        println "get  Book A_no"
        BookOperationService bookOperationService = new BookOperationService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        bookOperationService.getBookA_no(request ,hm , ip)
        render hm as JSON
    }

    def getBookData()
    {
        println "get Books DATA"
        BookOperationService bookOperationService = new BookOperationService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        bookOperationService.getBookData(request,hm)
        render hm as JSON
    }
    def getOrgData()
    {
        println "get Org DATA"
        BookOperationService bookOperationService = new BookOperationService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        bookOperationService.getOrgData(request,hm)
        render hm as JSON
    }
    def refreshstatus()
    {
        println "get Org DATA"
        BookOperationService bookOperationService = new BookOperationService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        bookOperationService.refreshstatus(request,hm)
        render hm as JSON
    }
    def getOrgList() {
        println "I am in getOrgList"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        //def userid =  dataresponse["userid"]
        HashMap hm = new HashMap()
        String purpose = "fetch org data"
        //auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def orgainization=Organization.findById(dataresponse.selected_org_id)
              //  print"orgainization>>>>"+orgainization

                def program=Program.findAllByOrganization(orgainization)
                def name=program?.name
                hm.put("department",name)
                //println"department >>>"+hm.department
                render hm as JSON
                return
            }
        }
        render hm as JSON
        return
    }

}
