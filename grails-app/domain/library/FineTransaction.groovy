package library
import grails.gorm.MultiTenant
class FineTransaction implements MultiTenant<FineTransaction>
{
    String receipt_no
    Date transaction_date
    double amount

    String transaction_number //cheque number, online transaction number
    String bank_name
    String bank_branch
    boolean isactive
    boolean clearFine

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[booklending:BookLending,transactiontype:TransactionType,organization:Organization]
    static constraints = {
        receipt_no nullable:true
        transaction_number nullable:true
        bank_name nullable:true
        bank_branch nullable:true
        organization nullable: true
    }
    static mapping = {
        isactive defaultValue: true
        clearFine defaultValue: false

    }
}
