package library

import grails.gorm.multitenancy.Tenants
import grails.gorm.transactions.Transactional
import groovy.json.JsonSlurper


@Transactional
class ReserveBookMemberService {

    def serviceMethod() {

    }

    def reserveBookInit(request, hm,ip)
    {
        println("reserveBookInit")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String purpose = "reserve Book Init stud"
        //auth.checkauth(token, uid, url, request, purpose)//401;200;token-refreshed ->msg
        def bookinfo = []
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {

                ReserveBookService reserveBookS = new ReserveBookService()
                def index_config = IndexConfig.list()
                def isReservationSink = index_config[0].isReservationSink
                if(!isReservationSink) {
                    reserveBookS.sinkReservationDaysLimit(uid,ip,index_config[0])
                }

                int srno = 1
                ArrayList books = new ArrayList()

                User user = User.findByUsername(uid)
                Role role = Role.findByName('Member')
                LibraryPolicy policy = LibraryPolicy.findByRoleAndOrganization(role,user.organization)
                def reservation_daysLimit = policy?.reservation_validity_days
                def book_reservation_limit = policy?.book_reservation_limit

//                def status = ReservationStatus.findByName('Pending')
//                def reserveList = BookReservation.findAllByUserAndIsactiveAndReservationstatus(user,true,status)
                def reserveList = BookReservation.findAllByUserAndIsactive(user,true)
                for(reserve_book in reserveList) {
                    HashMap temp = new HashMap()

                    def book = Book.findById(reserve_book?.book?.id)

                    temp.put('srno', srno++)
                    temp.put('id', book?.id)
                    temp.put('title', book?.title)
                    temp.put('isbn', book?.isbn)
                    temp.put('edition', book?.edition)
                    temp.put('bookType', book?.booktype?.name)
                    temp.put('category', book?.bookcategory?.name)
                    temp.put('publisher', book?.publisher?.name)
                    temp.put('copies', book?.total_books)
                    temp.put('isReserve', true)
                    temp.put('reservation_status', reserve_book?.reservationstatus?.name)
                    BookStatus bookStatus = BookStatus.findByName('Available')
                    def bookItems = BookItem.findAllByBookAndBookstatus(book, bookStatus)
                    temp.put('available', bookItems.size())
                    def authorArr = book?.author
                    ArrayList arr = new ArrayList()
                    for (author in authorArr) {
                        def authorName = Author.findById(author?.id)
                        arr.add(authorName?.name)
                    }

                    temp.put('authors',arr)
                    books.add(temp)
                }
                hm.put("reserved_count",books.size())

                if(book_reservation_limit > books.size()){
                    hm.put('isReservation_limit_exeed',false)
                    hm.put('Reservation_limit',book_reservation_limit)
                }else {
                    hm.put('isReservation_limit_exeed',true)
                    hm.put('Reservation_limit',book_reservation_limit)
                }


                def bookList = Book.findAllByOrganization(user.organization)
                println('bookList============'+bookList)
                for(book in bookList){
                    HashMap temp = new HashMap()
                    temp.put('srno',srno++)
                    temp.put('id',book?.id)
                    temp.put('title',book?.title)
                    temp.put('isbn',book?.isbn)
                    temp.put('edition',book?.edition)
//                    temp.put('bookFormat',book?.isbn)
                    temp.put('bookType',book?.booktype?.name)
                    temp.put('category',book?.bookcategory?.name)
                    temp.put('publisher',book?.publisher?.name)
                    temp.put('copies',book?.total_books)
                    temp.put('reservation_status', '-')
                    temp.put('isReserve', false)
                    BookStatus bookStatus = BookStatus.findByName('Available')
                    def bookItems = BookItem.findAllByBookAndBookstatus(book,bookStatus)
                    temp.put('available',bookItems.size())
                    def authorArr = book?.author
                    ArrayList arr = new ArrayList()
                    for(author in authorArr){
                        def authorName = Author.findById(author?.id)
                        arr.add(authorName?.name)
                    }
                    temp.put('authors',arr)
                    books.add(temp)

                }
                books = books.unique{it.isbn}

                hm.put('bookList',books)
                hm.put('msg','200')
            }//println"hm: "+hm
            return hm

        }

    }

    def reserveBook(request, hm,ip)
    {
        println("reserve Book")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        String purpose = "reserve Book"
        //auth.checkauth(token, uid, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                for(int i=0;i< dataresponse.bookIds.size();i++) {

                    Book book = Book.findById(dataresponse.bookIds[i])
                    BookStatus b_status = BookStatus.findByName('Available')
                    def bookItem_list = BookItem.findAllByBookAndBookstatus(book, b_status)
                    def bookItem
                    if(bookItem_list.size() > 0){
                        bookItem = bookItem_list[0]
                    }
                    if (bookItem_list.size() > 0) {

                        BookStatus b_status_reserve = BookStatus.findByName('Reserved')
                        println('b_status_reserve----' + b_status_reserve)
                        bookItem.bookstatus = b_status_reserve
                        bookItem.updation_username = uid
                        bookItem.updation_date = new Date()
                        bookItem.updation_ip_address = ip
                        bookItem.save(flush: true, failOnError: true)

                        BookReservation book_reservation = new BookReservation()

                        ReservationStatus reservation_status = ReservationStatus.findByName('Pending')
                        book_reservation.reservation_date = new Date()
                        book_reservation.user = user
                        book_reservation.book = book
                        book_reservation.isactive = true
                        book_reservation.reservationstatus = reservation_status
                        book_reservation.creation_username = uid
                        book_reservation.updation_username = uid
                        book_reservation.creation_date = new Date()
                        book_reservation.updation_date = new Date()
                        book_reservation.creation_ip_address = ip
                        book_reservation.updation_ip_address = ip
                        book_reservation.save(flush: true, failOnError: true)
                    }else{

                        BookReservation book_reservation = new BookReservation()
                        ReservationStatus reservation_status = ReservationStatus.findByName('Waiting')
                        book_reservation.reservation_date = new Date()
                        book_reservation.user = user
                        book_reservation.book = book
                        book_reservation.isactive = true
                        book_reservation.reservationstatus = reservation_status
                        book_reservation.creation_username = uid
                        book_reservation.updation_username = uid
                        book_reservation.creation_date = new Date()
                        book_reservation.updation_date = new Date()
                        book_reservation.creation_ip_address = ip
                        book_reservation.updation_ip_address = ip
                        book_reservation.save(flush: true, failOnError: true)
                    }
                }

                hm.put('msg','200')
            }
            return hm
        }
    }
}
