package library

import grails.converters.JSON

class ReturnBookController {

    def index() { }
    def getBookData()
    {
        println "getBookData"
        ReturnBookService returnBookService = new ReturnBookService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        returnBookService.getBookData(request, hm,ip)
        render hm as JSON
    }
    def getAccessionList()
    {
        println "getAccessionList"
        ReturnBookService returnBookService = new ReturnBookService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        returnBookService.getAccessionList(request, hm,ip)
        render hm as JSON
    }

    def receiveBook()
    {
        println "receiveBook"
        ReturnBookService returnBookService = new ReturnBookService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        returnBookService.receiveBook(request, hm,ip)
        render hm as JSON
    }

    def payFineData()
    {
        println "payFineData"
        ReturnBookService returnBookService = new ReturnBookService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        returnBookService.payFineData(request, hm,ip)
        render hm as JSON
    }

    def payfine()
    {
        println "payfine"
        ReturnBookService returnBookService = new ReturnBookService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        returnBookService.payfine(request, hm,ip)
        render hm as JSON
    }
}
