package library

import grails.gorm.multitenancy.Tenants
import grails.gorm.transactions.Transactional

@Transactional
class BookReportService {

    def serviceMethod() {

    }
    def getAllBook(request, hm,ip) {
        println("in getAllBook:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String purpose = "getAllBook"
        //auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                int srno = 1
                BookStatus bookStatus = BookStatus.findByName("Issued")
                ArrayList bookdata = new ArrayList()
                def bookList = BookItem.findAllByBookstatus(bookStatus)
                for(books in bookList){
                    HashMap temp = new HashMap()
                    temp.put('srno',srno++)
                    temp.put('id',books?.book?.id)
                    temp.put('a_no',books?.accession_number)
                    temp.put('title',books?.book?.title)
                    temp.put('isbn',books?.book?.isbn)
                    temp.put('edition',books?.book?.edition)
//                    temp.put('bookFormat',book?.isbn)
                    temp.put('bookType',books?.book?.booktype?.name)
                    temp.put('category',books?.book?.bookcategory?.name)
                    temp.put('publisher',books?.book?.publisher?.name)
                    temp.put('copies',books?.book?.total_books)
                    BookStatus bookStat = BookStatus.findByName('Available')
                    def bookItems = BookItem.findAllByBookAndBookstatus(books?.book,bookStat)
                    temp.put('available',bookItems.size())
                    BookLending bookLending = BookLending.findByBookitem(books)
                    temp.put('borrower_name',bookLending?.user?.username)
                    def authorArr = books?.book?.author
                    ArrayList arr = new ArrayList()
                    for(author in authorArr){
                        def authorName = Author.findById(author?.id)
                        arr.add(authorName?.name)
                    }
                    temp.put('authors',arr)
                    bookdata.add(temp)
                }
                ArrayList authList = new ArrayList()
                def authorsList = Author.findAll()
                for(author in authorsList){
                    HashMap temp = new HashMap()
                    temp.put('name',author?.name)
                    temp.put('id',author?.id)
                    authList.add(temp)
                }
                hm.put('authorsList',authList)
                hm.put('bookList',bookdata)
              //  println "book list"+bookdata
                hm.put('msg','200')
            }//println"hm: "+hm
            return hm

        }
        return hm
    }
}
