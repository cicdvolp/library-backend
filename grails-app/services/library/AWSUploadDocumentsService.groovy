package library

import javax.servlet.http.Part
import java.nio.file.Paths


class AWSUploadDocumentsService {

    AWSBucketService awsBucketService = new AWSBucketService()

    def serviceMethod() {}

    boolean uploadDocument(Part filePart, def folderPath, def fileName, def existsFilePath) {

        AWSBucket awsBucket = AWSBucket.findByContent("documents")

        def filePath //= folderPath + fileName
        def fileExtention
        boolean filePathIsExists = false
        boolean isUploadSuccess = false

        InputStream fs = filePart.getInputStream()
        String contentType = filePart.getContentType();
        /*String filePartName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
        int indexOfDot = filePartName.lastIndexOf('.')
        if (indexOfDot > 0) {
            fileExtention = filePartName.substring(indexOfDot + 1)
        }*/

        if(existsFilePath != null){
            filePathIsExists = true
        } else {
            filePathIsExists = false
        }

        filePath =  folderPath + fileName //+ "."+fileExtention

        // check folder is exists
        if(awsBucketService.doesObjectExist(awsBucket.bucketname, awsBucket.region, folderPath)){
            if(filePathIsExists) {
                if (awsBucketService.doesObjectExist(awsBucket.bucketname, awsBucket.region, existsFilePath)) {
                    boolean deleteSuccess = awsBucketService.deleteObject(awsBucket.bucketname, awsBucket.region, existsFilePath)
                    if (!deleteSuccess) {
                        boolean uploadSuccess = awsBucketService.putObjectToBucket(awsBucket.bucketname, awsBucket.region, filePath, fs, contentType)
                        if (uploadSuccess) {
                            isUploadSuccess = true
                        }
                    }
                } else {
                    boolean uploadSuccess = awsBucketService.putObjectToBucket(awsBucket.bucketname, awsBucket.region, filePath, fs, contentType)
                    if (uploadSuccess) {
                        isUploadSuccess = true
                    }
                }
            }
            else {
                boolean uploadSuccess = awsBucketService.putObjectToBucket(awsBucket.bucketname, awsBucket.region, filePath, fs, contentType)
                if (uploadSuccess) {
                    isUploadSuccess = true
                }
            }
        }
        else {
            //  println("folder not available")
            boolean isExist = awsBucketService.createFolder(awsBucket.bucketname, awsBucket.region, folderPath)
            if(isExist){
                boolean uploadSuccess = awsBucketService.putObjectToBucket(awsBucket.bucketname, awsBucket.region, filePath, fs, contentType)
                if(uploadSuccess){
                    isUploadSuccess = true
                }
            } else {
                //  println("folder not created....")
            }
        }
        return isUploadSuccess
    }

    boolean copyObjectToSameBucket(def folderPathnew, def fileNamenew, def folderPathold, def fileNameold, def existsFilePath) {
        AWSBucket awsBucket = AWSBucket.findByContent("documents")

        def filePathnew
        def filePathold
        boolean isUploadSuccess = false
        boolean filePathIsExists = false

        if(existsFilePath != ""){
            filePathIsExists = true
        } else {
            filePathIsExists = false
        }

        filePathnew =  folderPathnew + fileNamenew
        filePathold =  folderPathold + fileNameold

        if(awsBucketService.doesObjectExist(awsBucket.bucketname, awsBucket.region, filePathnew)){
            if(filePathIsExists) {
                if (awsBucketService.doesObjectExist(awsBucket.bucketname, awsBucket.region, existsFilePath)) {
                    boolean deleteSuccess = awsBucketService.deleteObject(awsBucket.bucketname, awsBucket.region, existsFilePath)
                    if (!deleteSuccess) {
                        boolean uploadSuccess = awsBucketService.CopyObjectToBucket(awsBucket.bucketname, awsBucket.region, filePathnew, filePathold)
                        if (uploadSuccess) {
                            isUploadSuccess = true
                        }
                    }
                } else {
                    boolean uploadSuccess = awsBucketService.CopyObjectToBucket(awsBucket.bucketname, awsBucket.region, filePathnew, filePathold)
                    if (uploadSuccess) {
                        isUploadSuccess = true
                    }
                }
            }else {
                boolean uploadSuccess = awsBucketService.CopyObjectToBucket(awsBucket.bucketname, awsBucket.region, filePathnew, filePathold)
                if (uploadSuccess) {
                    isUploadSuccess = true
                }
            }
        }
        else {
            boolean isExist = awsBucketService.createFolder(awsBucket.bucketname, awsBucket.region, filePathnew)
            if(isExist){
                boolean uploadSuccess = awsBucketService.CopyObjectToBucket(awsBucket.bucketname, awsBucket.region, filePathnew, filePathold)
                if(uploadSuccess){
                    isUploadSuccess = true
                }
            }
        }
        return isUploadSuccess
    }



    def downloadDocument(def filePath, def fileName) {
//        def filePath = "test/d1.docx"
        AWSBucket awsBucket = AWSBucket.findByContent("documents")

        response.setContentType("text/html; charset=UTF-8")
        response.setHeader("Content-Disposition", "Attachment;Filename=\""+fileName+"\"")
        InputStream object = awsBucketService.downloadContentFromBucket(awsBucket.bucketname, awsBucket.region, filePath)

        File tmp = File.createTempFile("s3test", "");
        Files.copy(object, tmp.toPath(), StandardCopyOption.REPLACE_EXISTING);
        def fileInputStream = new FileInputStream(tmp)
        def outputStream = response.getOutputStream()
        byte[] buffer = new byte[4096];
        int len;
        while ((len = fileInputStream.read(buffer)) > 0) {
            outputStream.write(buffer, 0, len);
        }
        outputStream.flush()
        outputStream.close()
        fileInputStream.close()
    }

    boolean deleteDocument(def filePath) {
        // println("deleteDocument :: filePath : " + filePath)
        boolean isDeleteSuccess = false
        AWSBucket awsBucket = AWSBucket.findByContent("documents")
        //  println("awsBucket :: " +awsBucket)
        if (awsBucketService.doesObjectExist(awsBucket.bucketname, awsBucket.region, filePath)) {
            try {
                awsBucketService.deleteObject(awsBucket.bucketname, awsBucket.region, filePath)
                isDeleteSuccess = true
            } catch (Exception e) {
                //render "<h2>File Does not deleted</h2>"
                return isDeleteSuccess
            }
        } else {
            //render "<h2>File Does not exists</h2>"
            return isDeleteSuccess
        }
        //     println("isDeleteSuccess :: " + isDeleteSuccess)
        return isDeleteSuccess
    }



}
