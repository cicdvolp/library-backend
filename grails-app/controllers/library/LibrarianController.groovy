package library
import grails.converters.JSON
import grails.gorm.multitenancy.Tenants
import groovy.json.JsonSlurper

import javax.servlet.http.Part
import java.nio.file.Paths

class LibrarianController {

    def index() { }
    def uploadbookphoto()
    {
        println(params)
        def uid = request.getHeader("EPS-uid")
        Login log = Login.findByUsername(uid)
        HashMap hm =new HashMap()
        Tenants.withId(log.tenantclient.tenantid) {
            def user = User.findByUsername(log.username)
            AWSUploadDocumentsService awsUploadDocumentsService = new AWSUploadDocumentsService()
            BookItem bookItem=BookItem.findByAccession_numberAndOrganization(params.accession_number,user.organization)
            if(bookItem==null)
            {
                hm.put("msg","Book Not Found.")
                render hm as JSON
                return
            }
           else if (params.file != null) {
                def check_file = request.getFile('file')
                Part filePart = request.getPart('file')
                println('In File Upload')
                if (check_file) {
                    def folderPath
                    def fileName
                    def filePath
                    def fileExtention
                    InputStream fs = filePart.getInputStream()
                    String contentType = filePart.getContentType();
                    String filePartName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
                    int indexOfDot = filePartName.lastIndexOf('.')
                    if (indexOfDot > 0) {
                        fileExtention = filePartName.substring(indexOfDot + 1)
                    }
                    String filename = filePartName.substring(0, filePartName.lastIndexOf("."));
                    println('filePart : ' + filePart)

                    AWSBucket awsBucket = AWSBucket.findByContent("documents")
                    println("s dfjsk dfk skdf ks dfs kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk")
                    println(awsBucket)
                    folderPath = 'cloud/' + 'Book_photo/' + user?.organization?.name+'/'
                    fileName = params.accession_number + "." + fileExtention
                    filePath = folderPath + fileName
                    String existsFilePath =null
                            println(filePart)
                            println(folderPath)
                            println(fileName)
                            println(existsFilePath)
                    boolean isUploaded = awsUploadDocumentsService.uploadDocument(filePart, folderPath, fileName,existsFilePath)
                    println "isUploaded :" + isUploaded
                    if (isUploaded) {
                        bookItem.filename = filename
                        bookItem.filepath = folderPath + fileName
                        bookItem.save(failOnError: true, flush: true)

                        AWSBucketService awsBucketService = new AWSBucketService()
                        String photourl = awsBucketService.getPresignedUrl(awsBucket.bucketname, filename, awsBucket.region)
                        hm.put("msg","200")
                        hm.put("url",photourl)
                        render hm as JSON
                        return
                    }
                }

            }
            else {
                hm.put("msg","File Not Uploaded Something Wrong.")
                render hm as JSON
                return
            }
        }
        hm.put("msg","File Not Uploaded Something Wrong.")
        render hm as JSON
        return
    }

    def getimageurl()
    {

        def uid = request.getHeader("EPS-uid")
        Login log = Login.findByUsername(uid)
        HashMap hm =new HashMap()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println(dataresponse)
        Tenants.withId(log.tenantclient.tenantid) {
            def user = User.findByUsername(log.username)
            AWSUploadDocumentsService awsUploadDocumentsService = new AWSUploadDocumentsService()
            BookItem bookItem=BookItem.findByAccession_numberAndOrganization(dataresponse.acc_no,user.organization)
            if(bookItem==null)
            {
                hm.put("msg","Book Not Found.")
                render hm as JSON
                return
            }
            else {
                AWSBucketService awsBucketService1 = new AWSBucketService()
                AWSBucket awsBucket = AWSBucket.findByContent("documents")
                def path=bookItem.filepath
                String photourl = awsBucketService1.getPresignedUrl(awsBucket?.bucketname, path, awsBucket?.region)
                hm.put("msg","200")
                hm.put("url",photourl)
                println(photourl)
                render hm as JSON
                return
            }

        }
        hm.put("msg","Internal Server error")
        render hm as JSON
        return
    }
    def getimageurlHOME()
    {

        def uid = request.getHeader("EPS-uid")
        Login log = Login.findByUsername(uid)
        HashMap hm =new HashMap()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println(dataresponse)
        def tenantid = dataresponse["tenantid"]
        TenantClient tenantclient = TenantClient.findById(tenantid)
        Tenants.withId(tenantclient.tenantid) {
            Organization org = Organization.findById(dataresponse.organizationid)
            AWSUploadDocumentsService awsUploadDocumentsService = new AWSUploadDocumentsService()
            BookItem bookItem=BookItem.findByAccession_numberAndOrganization(dataresponse.acc_no,org)
            if(bookItem==null)
            {
                hm.put("msg","Book Not Found.")
                render hm as JSON
                return
            }
            else {
                AWSBucketService awsBucketService1 = new AWSBucketService()
                AWSBucket awsBucket = AWSBucket.findByContent("documents")
                def path=bookItem.filepath
                String photourl = awsBucketService1.getPresignedUrl(awsBucket?.bucketname, path, awsBucket?.region)
                hm.put("msg","200")
                hm.put("url",photourl)
                println(photourl)
                render hm as JSON
                return
            }

        }
        hm.put("msg","Internal Server error")
        render hm as JSON
        return
    }

    def bulkbookedit()
    {
        println "bulkbookedit"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        librarianService.bulkbookedit(request, hm)
        render hm as JSON
    }
    def editbulkbook()
    {
        println "editbulkbook"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.editbulkbook(request, hm,ip,params)
        render hm as JSON
    }
    def markstusentforbookbank()
    {
        println "markstusentforbookbank"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.markstusentforbookbank(request, hm,ip)
        render hm as JSON
    }

    def getprogramtype()
    {
        println "getprogramtype"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        librarianService.getprogramtype(request, hm)
        render hm as JSON
    }
    def getprogram()
    {
        println "getprogram"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getprogram(request, hm,ip)
        render hm as JSON
    }

    def getstudent()
    {
        println "getstudent"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getstudent(request, hm,ip)
        render hm as JSON
    }

    def getBookData()
    {
        println "getBookData"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        println( new Date())
        librarianService.getBookData(request, hm)
        println( new Date())
        render hm as JSON
    }
    def getBookDataWithfilter()
    {
        println "getBookDataWithfilter"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getBookDataWithfilter(request, hm ,ip)
        render hm as JSON
    }

    def deleteselectedbook()
    {
        println "deleteselectedbook"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.deleteselectedbook(request, hm ,ip)
        render hm as JSON
    }

    def getnewacc_no()
    {
        println "getnewacc_no"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getnewacc_no(request, hm ,ip)
        render hm as JSON
    }

    def saveBook()
    {
        println "saveBook"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.saveBook(request, hm,ip)
        render hm as JSON
    }

    def editBook()
    {
        println "editBook"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.editBook(request, hm,ip)
        render hm as JSON
    }
    def fetchfilter()
    {
        println "editBook"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.fetchfilter(request, hm,ip)
        render hm as JSON
    }

    def getBookItemData()
    {
        println "getBookItemData"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getBookItemData(request, hm,ip)
        render hm as JSON
    }
    def getBookassectionnumber()
    {
        println "getBookItemData"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getBookassectionnumber(request, hm,ip)
        render hm as JSON
    }

    def opachitsreport()
    {
        println "opachitsreport"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.opachitsreport(request, hm)
        render hm as JSON
    }

    def getBoohistory()
    {
        println "getBoohistory"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getBoohistory(request, hm)
        render hm as JSON
    }
    def getissuereport()
    {
        println "getissuereport"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getissuereport(request, hm)
        render hm as JSON
    }



    def getissuebookdata()
    {
        println "getissuebookdata"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getissuebookdata(request, hm)
        render hm as JSON
    }
    def getRackBlockNo()
    {
        println "get RackBlockNo"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getRackBlockNo(request, hm,ip)
        render hm as JSON
    }

    def editBookItem()
    {
        println " edit BookItem"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.editBookItem(request, hm,ip)
        render hm as JSON
    }
    def editperiodicalsItem()
    {
        println " edit BookItem"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.editperiodicalsItem(request, hm,ip)
        render hm as JSON
    }

    def getBookRackData()
    {
        println "getBookRackData"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getBookRackData(request, hm,ip)
        render hm as JSON
    }

    def mapBookLocation()
    {
        println "mapBookLocation"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.mapBookLocation(request, hm,ip)
        render hm as JSON
    }

    def getA_noList()
    {
        println "mapBookLocation"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getA_noList(request, hm,ip)
        render hm as JSON
    }
    def getBookInfo()
    {
        println "getBookData"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getBookInfo(request, hm,ip)
        render hm as JSON
    }

    def addBookLocation()
    {
        println "addBookLocation"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.addBookLocation(request, hm,ip)
        render hm as JSON
    }
    def checkRackAvailability()
    {
        println "checkRackAvailability"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.checkRackAvailability(request, hm,ip)
        render hm as JSON
    }

    def reports()
    {
        println "reports"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.reports(request,ip, hm,)
        render hm as JSON
    }

    def accessionReport()
    {
        println "accessionReport"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.accessionReport(request,ip, hm,)
        render hm as JSON
    }

    def overdueData()
    {
        println "overdueData"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.overdueData(request,ip, hm)
        render hm as JSON
    }

    def getdeptData()
    {
        println "getdeptData"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getdeptData(request,ip, hm,)
        render hm as JSON
    }

    def getseriesData()
    {
        println "getdeptData"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getseriesData(request,ip, hm,)
        render hm as JSON
    }
    def getERPDeprtmentData()
    {
        println "getERPDeprtmentData"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getERPDeprtmentData(request,ip, hm,)
        render hm as JSON
    }
    def new_arrival_data()
    {
        println "dailyData"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.new_arrival_data(request,ip, hm,)
        render hm as JSON
    }
    def title_wise_data()
    {
        println "dailyData"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.title_wise_data(request,ip, hm,)
        render hm as JSON
    }
    def member_wise_Library_util_data()
    {
        println "dailyData"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.member_wise_Library_util_data(request,ip, hm,)
        render hm as JSON
    }
    def getserieswiseaccno()
    {
        println "getserieswiseaccno"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getserieswiseaccno(request,ip, hm,)
        render hm as JSON
    }
    def get_acc_register()
    {
        println "get_acc_register"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.get_acc_register(request,ip, hm,)
        render hm as JSON
    }


    def dailyData()
    {
        println "dailyData"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.dailyDatanew(request,ip, hm,)
        render hm as JSON
    }
    def getmembers()
    {
        println "getmembers"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getmembers(request,ip, hm)
        render hm as JSON

    }



    def memberWise()
    {
        println "memberWise"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.memberWise(request,ip, hm,)
        render hm as JSON
    }

    def titleReport()
    {
        println "titleReport"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.titleReport(request,ip, hm,)
        render hm as JSON
    }

    def getFineRecord()
    {
        println "getFineRecord"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getFineRecord(request,hm,ip)
        render hm as JSON
    }

    def payFineData()
    {
        println "payFineData"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.payFineData(request,hm,ip)
        render hm as JSON
    }
    def getBookData1()
    {
        println "getBookData1"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getBookData1(request,hm,ip)
        render hm as JSON
    }

    def getSeriesreport()
    {
        println "getSeriesreport"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getSeriesreport(request,hm,ip)
        render hm as JSON
    }


    def getPeriodicalReportList()
    {
        println "getPeriodicalReportList"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getPeriodicalReportList(request,hm,ip)
        render hm as JSON
    }
    def periodics_report_data()
    {
        println "periodics_report_data"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.periodics_report_data(request,hm,ip)
        render hm as JSON
    }

    //status report
    def getstatusData()
    {
        println "getstatusData"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getstatusData(request,ip, hm,)
        render hm as JSON
    }
    def get_statuswise_data(){
        println "get_statuswise_data"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.get_statuswise_data(request,ip, hm,)
        render hm as JSON
    }

    // medium wise report
    def getmediumReportData(){
        println "getmediumReportData"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getmediumReportData(request,ip, hm)
        render hm as JSON
    }
    def fetchmediumReport(){
        println "fetchmediumReport"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.fetchmediumReport(request,ip, hm)
        render hm as JSON
    }

    //getWeedoutpagedata
    def getWeedoutpagedata(){
        println "getWeedoutpagedata"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getWeedoutpagedata(request,ip, hm)
        render hm as JSON
    }
    def getbookdataWeedout(){
        println "getbookdataWeedout"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getbookdataWeedout(request, hm)
        render hm as JSON
    }
    def approveweedout(){
        println "approveweedout"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.approveweedout(request,ip, hm)
        render hm as JSON
    }
    def getallWeedoutbooks(){
        println "getallWeedoutbooks"
        LibrarianService librarianService = new LibrarianService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        librarianService.getallWeedoutbooks(request,ip, hm)
        render hm as JSON
    }
}
