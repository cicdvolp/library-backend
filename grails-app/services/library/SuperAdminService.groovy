package library

import grails.gorm.transactions.Transactional
import grails.gorm.multitenancy.Tenants
import grails.converters.JSON
import groovy.json.JsonSlurper
import com.opencsv.CSVReader
import javax.servlet.http.Part;
import grails.io.IOUtils
import java.nio.file.Paths
import java.nio.file.Files
import java.nio.file.StandardCopyOption
@Transactional
class SuperAdminService {

    def serviceMethod() {}
    //Super Admin Login
    def superadminLogin(request, hm)
    {
        println("Super Admin Login Service :")
        AuthService auth = new AuthService()
        String body =  request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse ::"+dataresponse)
        Login login=Login.findByUsernameAndPasswordAndIsloginblocked(dataresponse.uid,dataresponse.password,false,)
        println("Login:"+login)
        if (login){
            hm.put('token', auth.gentoken(login?.id))
            hm.put('uid', login?.username)
            hm.put("status",'YES')
            return hm
        }else {
            hm.put("status",'NO')
            return hm
        }

    }
    //Get Super Admin Link
    def getSuperAdminLink(request, hm) {
        println("Super Admin Links :")
        def token =  request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid =  request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        /*String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)*/
        String purpose = "get Link"
        auth.checkauth(token,uid,hm,url,request,purpose)
        Login login = Login.findByUsername(uid)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            println("Login:" + login)
            def links = []
            boolean link=true
            if (login) {
                if(link) {
                    HashMap temp = new HashMap()
                    temp.put("linkname", 'Super-Master')
                    temp.put("router_link", 'super-master-main')
                    temp.put("icon", 'mdi-clipboard-plus')
                    links.add(temp)
                }
                if(link) {
                    HashMap temp = new HashMap()
                    temp.put("linkname", 'Add Tenant')
                    temp.put("router_link", 'add-tenant')
                    temp.put("icon", 'mdi-book-plus')
                    links.add(temp)
                }
                if(link) {
                    HashMap temp = new HashMap()
                    temp.put("linkname", 'Push Master to Tenant')
                    temp.put("router_link", 'push-master-to-tenant')
                    temp.put("icon", 'mdi-briefcase-upload')
                    links.add(temp)
                }
                if(link){
                    HashMap temp = new HashMap()
                    temp.put("linkname", 'Link Master')
                    temp.put("router_link", 'link-master')
                    temp.put("icon", 'mdi-cellphone-link')
                    links.add(temp)
                }
                if(link) {

                    HashMap temp = new HashMap()
                    temp.put("linkname", 'Add User')
                    temp.put("router_link", 'add-new-user-super-admin')
                    temp.put("icon", 'mdi-account-box')
                    links.add(temp)
                }
               /* if(link) {
                    HashMap temp = new HashMap()
                    temp.put("linkname", 'Assign Roles to User')
                    temp.put("router_link", 'assign-role-to-user')
                    temp.put("icon", 'mdi-clipboard-account')
                    links.add(temp)
                }*/
                if(link) {
                    HashMap temp = new HashMap()
                    temp.put("linkname", 'Add Organization')
                    temp.put("router_link", 'add-organization')
                    temp.put("icon", 'mdi-home-circle')
                    links.add(temp)
                }
                if(link){
                    HashMap temp = new HashMap()
                    temp.put("linkname", 'Add Library')
                    temp.put("router_link", 'add-libray')
                    temp.put("icon", 'mdi-book-open-variant')
                    links.add(temp)
                }
            }
            hm.put('links', links)
            return hm
        }
    }
    //Get Tenant Data
    def getTenantData(request, hm) {
        println("Tenant Data :")
        def token =  request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid =  request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        /*String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)*/
        String purpose = "get Tenant Data"
        auth.checkauth(token,uid,hm,url,request,purpose)
        Login login = Login.findByUsername(uid)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            def tenantList = []
            def alltenant=TenantClient.list()
            println("TenantClient ::"+alltenant.size())
            AWSBucketService photoUrl = new AWSBucketService()
            AWSBucket awsBucket = AWSBucket.findByContent("documents")
            for(tc in alltenant)
            {
                HashMap temp=new HashMap()
                temp.put("tc_id",tc?.id)
                temp.put("name",tc?.name)
                temp.put("tenantid",tc?.tenantid)
                temp.put("teanantdbname",tc?.teanantdbname)
                temp.put("domainname",tc?.domainname)
                temp.put("logopath",tc?.logopath)
                temp.put("logoname",tc?.logoname)
                temp.put("isactive",tc?.isactive)
                String logo=null
                if(tc?.logopath!=null && tc?.logoname!=null)
                {
                    String awsimagelink =tc?.logopath+tc?.logoname
                    logo = photoUrl.getPresignedUrl(awsBucket.bucketname,awsimagelink ,awsBucket.region)
                }
                temp.put("logo",logo)
                tenantList.add(temp)
            }
            hm.put('tenantList', tenantList)
            hm.put("status", 'YES')
            return hm
        }
    }
    //Save Tenant Data
    def saveTenantData(request, hm,params) {
        println("Save Tenant Data :"+params)
        def token =  request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid =  request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "save Tenant Data"
        auth.checkauth(token,uid,hm,url,request,purpose)
        Login login = Login.findByUsername(uid)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            def tenantList = []
            if (params.tcid!='null') {
                TenantClient tc = TenantClient.findById(params.tcid)
                println("TenantClient ::" + tc)
                if (tc == null) {
                    tc = new TenantClient()
                    tc.name = params.tenant_name
                    tc.tenantid = params.tenant_id
                    tc.isactive = true
                    tc.teanantdbname = params.tenant_id
                    tc.domainname = params.domain_name
                    tc.creation_username = login.username
                    tc.updation_username = login.username
                    tc.creation_date = new java.util.Date()
                    tc.updation_date = new java.util.Date()
                    tc.creation_ip_address = request.getRemoteAddr()
                    tc.updation_ip_address = request.getRemoteAddr()
                    //Logo Upload
                    def check_file = request.getFile('logo_file')
                    Part filePart = request.getPart('logo_file')
                    if (check_file) {
                        def folderPath
                        def fileName
                        def filePath
                        fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
                        folderPath = "tenantlogos/"
                        filePath = folderPath + fileName
                        AWSUploadDocumentsService awsUploadDocumentsService = new AWSUploadDocumentsService()
                        boolean isUpload = awsUploadDocumentsService.uploadDocument(filePart, folderPath, fileName, filePath)
                        println "isUpload:" + isUpload
                        if (isUpload) {
                            tc.logopath = folderPath
                            tc.logoname = fileName
                        } else {
                            hm.put("status", "NO")
                        }
                    }
                    tc.save(flush: true, failOnError: true)
                    hm.put("status", 'YES')
                } else {
                    tc.name = params.tenant_name
                    tc.tenantid = params.tenant_id
                    if (params.isactive == 'false') {
                        tc.isactive = false
                    } else {
                        tc.isactive = true
                    }
                    tc.teanantdbname = params.tenant_id
                    tc.domainname = params.domain_name
                    tc.creation_username = login.username
                    tc.updation_username = login.username
                    tc.creation_date = new java.util.Date()
                    tc.updation_date = new java.util.Date()
                    tc.creation_ip_address = request.getRemoteAddr()
                    tc.updation_ip_address = request.getRemoteAddr()
                    //Logo Upload
                    def check_file = request.getFile('logo_file')
                    Part filePart = request.getPart('logo_file')
                    if (check_file) {
                        def folderPath
                        def fileName
                        def filePath
                        fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
                        folderPath = "tenantlogos/"
                        filePath = folderPath + fileName
                        AWSUploadDocumentsService awsUploadDocumentsService = new AWSUploadDocumentsService()
                        boolean isUpload = awsUploadDocumentsService.uploadDocument(filePart, folderPath, fileName, filePath)
                        println "isUpload:" + isUpload
                        if (isUpload) {
                            tc.logopath = folderPath
                            tc.logoname = fileName
                        } else {
                            hm.put("status", "NO")
                        }
                    }
                    tc.save(flush: true, failOnError: true)
                    hm.put("status", 'YES')
                }
            }else
            {
                TenantClient tc = new TenantClient()
                tc.name = params.tenant_name
                tc.tenantid = params.tenant_id
                tc.isactive = true
                tc.teanantdbname = params.tenant_id
                tc.domainname = params.domain_name
                tc.creation_username = login.username
                tc.updation_username = login.username
                tc.creation_date = new java.util.Date()
                tc.updation_date = new java.util.Date()
                tc.creation_ip_address = request.getRemoteAddr()
                tc.updation_ip_address = request.getRemoteAddr()
                //Logo Upload
                def check_file = request.getFile('logo_file')
                Part filePart = request.getPart('logo_file')
                if (check_file) {
                    def folderPath
                    def fileName
                    def filePath
                    fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
                    folderPath = "tenantlogos/"
                    filePath = folderPath + fileName
                    AWSUploadDocumentsService awsUploadDocumentsService = new AWSUploadDocumentsService()
                    boolean isUpload = awsUploadDocumentsService.uploadDocument(filePart, folderPath, fileName, filePath)
                    println "isUpload:" + isUpload
                    if (isUpload) {
                        tc.logopath = folderPath
                        tc.logoname = fileName
                    } else {
                        hm.put("status", "NO")
                    }
                }
                tc.save(flush: true, failOnError: true)
                hm.put("status", 'YES')
            }
            return hm
        }
    }
    //Get Edit Tenant Data
    def geteditTenantData(request, hm) {
        println("Edit Tenant Data :")
        def token =  request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid =  request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body =  request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse ::"+dataresponse)
        String purpose = "edit Tenant Data"
        auth.checkauth(token,uid,hm,url,request,purpose)
        Login login = Login.findByUsername(uid)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            def tenantList = []
            TenantClient tc=TenantClient.findById(dataresponse.tcid)
            println("TenantClient ::"+tc)
            if (tc!=null)
            {
                hm.put("tc_id",tc?.id)
                hm.put("name",tc?.name)
                hm.put("tenantid",tc?.tenantid)
                hm.put("domainname",tc?.domainname)
                hm.put("isactive",tc?.isactive)
            }
            hm.put("status", 'YES')
            return hm
        }
    }
    //Super Admin Menu
    def superadminMasterMenu(request, ip, hm) {
        println "i am in AdminMasterMenu"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "Getting Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
                ArrayList routerLinkList = new ArrayList()
                int col_size = 3
            HashMap temp = new HashMap()
            temp.put('name', 'add-user-type-super-admin')
            temp.put('textval', 'User Type')
            temp.put('icon', 'mdi-alpha-e-circle')
            temp.put('icontext', 'UT')
            temp.put('flex', col_size)
            temp.put('size', UserTypeSuperMaster.list().size())
            routerLinkList.add(temp)

            temp = new HashMap()
            temp.put('name', 'add-role-super-admin')
            temp.put('textval', 'Role')
            temp.put('icon', 'mdi-alpha-e-circle')
            temp.put('icontext', 'P')
            temp.put('flex', col_size)
            temp.put('size', RoleSuperMaster    .list().size())
            routerLinkList.add(temp)

            temp = new HashMap()
            temp.put('name', 'book-status-super-admin')
            temp.put('textval', 'Book Status')
            temp.put('icon', 'mdi-alpha-e-circle')
            temp.put('icontext', 'A')
            temp.put('flex', col_size)
            temp.put('size', BookStatusSuperMaster.list().size())
            routerLinkList.add(temp)

            temp = new HashMap()
            temp.put('name', 'reservation-status-super-admin')
            temp.put('textval', 'Reservation Status')
            temp.put('icon', 'mdi-alpha-e-circle')
            temp.put('icontext', 'BT')
            temp.put('flex', col_size)
            temp.put('size', ReservationStatusSuperMaster.list().size())
            routerLinkList.add(temp)

            temp = new HashMap()
            temp.put('name', 'account-status-super-admin')
            temp.put('textval', 'Account Status')
            temp.put('icon', 'mdi-alpha-e-circle')
            temp.put('icontext', 'L')
            temp.put('flex', col_size)
            temp.put('size', AccountStatusSuperMaster.list().size())
            routerLinkList.add(temp)

            temp = new HashMap()
            temp.put('name', 'transaction-type-super-admin')
            temp.put('textval', 'Transaction Type')
            temp.put('icon', 'mdi-alpha-e-circle')
            temp.put('icontext', 'R')
            temp.put('flex', col_size)
            temp.put('size', TransactionTypeSuperMaster.list().size())
            routerLinkList.add(temp)

            temp = new HashMap()
            temp.put('name', 'notification-method-super-admin')
            temp.put('textval', 'Notification Method')
            temp.put('icon', 'mdi-alpha-e-circle')
            temp.put('icontext', 'BC')
            temp.put('flex', col_size)
            temp.put('size', NotificationMethodSuperMaster.list().size())
            routerLinkList.add(temp)

            temp = new HashMap()
            temp.put('name', 'notification-type-super-admin')
            temp.put('textval', 'Notification Type')
            temp.put('icon', 'mdi-alpha-e-circle')
            temp.put('icontext', 'RL')
            temp.put('flex', col_size)
            temp.put('size', NotificationTypeSuperMaster.list().size())
            routerLinkList.add(temp)

            temp = new HashMap()
            temp.put('name', 'library-configration-super-admin')
            temp.put('textval', 'Library-Configration')
            temp.put('icon', 'mdi-alpha-e-circle')
            temp.put('icontext', 'BF')
            temp.put('flex', col_size)
            temp.put('size', LibraryConfigurationSuperMaster.list().size())
            routerLinkList.add(temp)
            hm.put('routerLinkList', routerLinkList)

            return hm
        }
        return hm
    }
    //Get All Roles
    def getRole(request, hm) {
        println("Inside getRole")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def rolelist = []
                def roles = RoleSuperMaster.findAll()
                int srno = 1
                def allut=UserTypeSuperMaster.findAll()
                def utall=[]
                for(ut in allut)
                {
                    HashMap temp=new HashMap()
                    temp.put("tu_id",ut?.id)
                    temp.put("name",ut?.name)
                    utall.add(temp)
                }
                for (role in roles) {
                    HashMap temp = new HashMap()
                    temp.put("role", role.name)
                    temp.put("roleId", role.id)
                    temp.put("roleIsactive", role.isactive)
                    temp.put("srno", srno++)
                    rolelist.add(temp)
                }
                hm.put('role', rolelist)
                hm.put('utall', utall)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm

        }
    }
    //Save Role In Super master
    def addRole(request, hm, ip) {
        println "i am in addRole Super Master"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addRole from Super Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def rolelist = []
            UserTypeSuperMaster  ut=UserTypeSuperMaster.findById(dataresponse?.ut)
            RoleSuperMaster role = RoleSuperMaster.findByNameAndUsertypesupermaster(dataresponse?.roleName,ut)
                if (role != null) {
                    hm.put("msg", "Role already exist!!")
                } else {
                    role = new RoleSuperMaster()
                    role.isactive = true
                    role.name = dataresponse?.role_name
                    role.updation_ip_address = ip
                    role.creation_ip_address = ip
                    role.updation_date = new Date()
                    role.creation_date = new Date()

                    role.usertypesupermaster =ut
                    role.updation_username = uid
                    role.creation_username = uid
                    role.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def allrole_List = RoleSuperMaster.findAll()
                int srno = 1
                for (roledata in allrole_List) {
                    HashMap temp = new HashMap()
                    temp.put("role", roledata.name)
                    temp.put("roleId", roledata.id)
                    temp.put("roleIsactive", roledata.isactive)
                    temp.put("srno", srno++)
                    rolelist.add(temp)
                }
                hm.put("role_list", rolelist)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm

        }
        return hm
    }
    //Active in Active
    def roleIsactive(request, hm) {
        println "i am in roleIsactive Super Master"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "roleIsactive from Super Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
                def role = RoleSuperMaster.findById(dataresponse?.roleId)
                if (role != null) {
                    if (role?.isactive == false) {
                        role.isactive = true
                        role.updation_ip_address = request.getRemoteAddr()
                        role.updation_date = new Date()
                        role.updation_username = uid
                        role.save(flush: true, failOnError: true)
                    } else {
                        role.isactive = false
                        role.updation_ip_address = request.getRemoteAddr()
                        role.updation_date = new Date()
                        role.updation_username = uid
                        role.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm

        }
        return hm
    }
    //Edit Role
    def editRole(request, hm, ip) {
        println "i am in editRole Super Master"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editRole from Super Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            RoleSuperMaster role = RoleSuperMaster.findById(dataresponse?.roleId)
            UserTypeSuperMaster  ut=UserTypeSuperMaster.findById(dataresponse?.ut)
                def exist = RoleSuperMaster.findByNameAndUsertypesupermaster(dataresponse?.roleName,ut)
                if (role != null)
                {
                 /*   hm.put("msg", "Role already exist!!")
                } else {*/
                    role.name = dataresponse?.roleName
                    role.updation_ip_address = ip
                    role.updation_date = new Date()
                    role.updation_username = uid

                    role.usertypesupermaster =ut
                    role.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm
                }
        }
        return hm
    }
    //Edit Admin UT
    def editAdminUserTypeOperation(request, hm) {
        println "i am in editAdminUserTypeOperation"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "Edit User Types from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
                def userType = UserTypeSuperMaster.findById(dataresponse?.userTypeId)
                if (userType != null) {
                    if (userType?.isactive == false) {
                        userType.isactive = true
                        userType.updation_ip_address = request.getRemoteAddr()
                        userType.updation_date = new Date()
                        userType.updation_username = uid
                        userType.save(flush: true, failOnError: true)
                    } else {
                        userType.isactive = false
                        userType.updation_ip_address = request.getRemoteAddr()
                        userType.updation_date = new Date()
                        userType.updation_username = uid
                        userType.save(flush: true, failOnError: true)
                    }
                }

                def usertype = UserTypeSuperMaster.findAll()
                int srno = 1
                for (ut in usertype) {
                    HashMap temp = new HashMap()
                    temp.put("userType", ut.name)
                    temp.put("userTypeId", ut.id)
                    temp.put("userTypeIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    utlist.add(temp)
                }
                hm.put("user_list", utlist)
                hm.put("msg", "200")
                println('hm---' + hm)
                return hm

        }
        return hm
    }
    //Save Super UT
    def addUserType(request, hm, ip) {
        println "i am in addUserType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "Edit User Types from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            UserTypeSuperMaster userType = UserTypeSuperMaster.findByName(dataresponse?.userType_name)
                if (userType != null) {
                    hm.put("msg", "User type already exist!!")
                } else {
                    userType = new UserTypeSuperMaster()
                    userType.isactive = true
                    userType.name = dataresponse?.userType_name
                    userType.updation_ip_address = ip
                    userType.creation_ip_address = ip
                    userType.updation_date = new Date()
                    userType.creation_date = new Date()
                    userType.updation_username = uid
                    userType.creation_username = uid
                    userType.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def usertypeList = UserTypeSuperMaster.findAll()
                int srno = 1
                for (ut in usertypeList) {
                    HashMap temp = new HashMap()
                    temp.put("userType", ut.name)
                    temp.put("userTypeId", ut.id)
                    temp.put("userTypeIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    utlist.add(temp)
                }
                hm.put("user_list", utlist)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm

        }
        return hm
    }
    //edit Super UT
    def editUsertype(request, hm, ip) {
        println "i am in addUserType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "Edit User Types from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
                UserTypeSuperMaster userType = UserTypeSuperMaster.findById(dataresponse?.userTypeId)

                def utype = UserTypeSuperMaster.findByName(dataresponse?.usertType)
                println('dataresponse?.usertType---' + dataresponse?.usertType)
                println('dataresponse?.id---' + dataresponse?.userTypeId)

                if (utype != null) {
                    hm.put("msg", "User type already exist!!")
                } else {
                    userType.name = dataresponse?.usertType
                    userType.updation_ip_address = ip
                    userType.updation_date = new Date()
                    userType.updation_username = uid
                    userType.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def usertypeList = UserTypeSuperMaster.findAll()
                int srno = 1
                for (ut in usertypeList) {
                    HashMap temp = new HashMap()
                    temp.put("userType", ut.name)
                    temp.put("userTypeId", ut.id)
                    temp.put("userTypeIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    utlist.add(temp)
                }
                hm.put("user_list", utlist)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm

        }
        return hm
    }
    //get Super UT
    def getMasterUserType(request, hm) {
        println("Inside AdminService/findusertype...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)
        String purpose = "Edit User Types from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
                def usertype = UserTypeSuperMaster.findAll()
                int srno = 1
                for (ut in usertype) {
                    HashMap temp = new HashMap()
                    temp.put("userType", ut.name)
                    temp.put("userTypeId", ut.id)
                    temp.put("userTypeIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    utlist.add(temp)
                }
                hm.put('userType', utlist)
                println "hm:--- " + hm
                hm.put("msg", "200")
                return hm

        }
    }
    //Get Book Data
    def getBookStatus(request, hm) {
        println("Inside getBookStatus")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = 'Get Book Status'
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def bookStatuslist = []
                def bookStatus = BookStatusSuperMaster.findAll()
                int srno = 1
                for (ut in bookStatus) {
                    HashMap temp = new HashMap()
                    temp.put("bookStatus", ut?.name)
                    temp.put("bookStatusId", ut?.id)
                    temp.put("display", ut?.displayname)
                    temp.put("bookStatusIsactive", ut?.isactive)
                    temp.put("srno", srno++)
                    bookStatuslist.add(temp)
                }
                hm.put('bookStatus', bookStatuslist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm

        }
    }
    //Add New Book Data Status
    def addBookStatus(request, hm, ip) {
        println "i am in addBookStatus"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addBookStatus from super Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def bookStatuslist = []
                BookStatusSuperMaster bookStatus = BookStatusSuperMaster.findByNameAndDisplayname(dataresponse?.bookStatus_name.trim(),dataresponse?.displaybookStatus.trim())
                if (bookStatus != null) {
                    hm.put("msg", "Book Status already exist!!")
                } else {
                    bookStatus = new BookStatusSuperMaster()
                    bookStatus.isactive = true
                    bookStatus.name = dataresponse?.bookStatus_name
                    bookStatus.displayname = dataresponse?.displaybookStatus
                    bookStatus.updation_ip_address = ip
                    bookStatus.creation_ip_address = ip
                    bookStatus.updation_date = new Date()
                    bookStatus.creation_date = new Date()
                    bookStatus.updation_username = uid
                    bookStatus.creation_username = uid
                    bookStatus.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def allP_List = BookStatusSuperMaster.findAll()
                int srno = 1
                for (pub in allP_List) {
                    HashMap temp = new HashMap()
                    temp.put("bookStatus", pub.name)
                    temp.put("bookStatusId", pub.id)
                    temp.put("bookStatusIsactive", pub.isactive)
                    temp.put("srno", srno++)
                    bookStatuslist.add(temp)
                }
                hm.put("bookStatus_list", bookStatuslist)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm

        }
        return hm
    }
    //Active or In-Active BookStatus
    def bookStatusIsactive(request, hm) {
        println "i am in bookStatusIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "bookStatusIsactive from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
                def bookStatus = BookStatusSuperMaster.findById(dataresponse?.bookStatusId)
                if (bookStatus != null) {
                    if (bookStatus?.isactive == false) {
                        bookStatus.isactive = true
                        bookStatus.updation_ip_address = request.getRemoteAddr()
                        bookStatus.updation_date = new Date()
                        bookStatus.updation_username = uid
                        bookStatus.save(flush: true, failOnError: true)
                    } else {
                        bookStatus.isactive = false
                        bookStatus.updation_ip_address = request.getRemoteAddr()
                        bookStatus.updation_date = new Date()
                        bookStatus.updation_username = uid
                        bookStatus.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm

        }
        return hm
    }
    //Edit Book Status
    def editBookStatus(request, hm, ip) {
        println "i am in editBookStatus"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editBookStatus from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            BookStatusSuperMaster bookStatus = BookStatusSuperMaster.findById(dataresponse?.bookStatusId)

                def exist = BookStatusSuperMaster.findByNameAndDisplayname(dataresponse?.bookStatusName.trim(),dataresponse?.disbookStatusName.trim())
                if (exist != null) {
                    hm.put("msg", "Book status already exist!!")
                } else {
                    bookStatus.name = dataresponse?.bookStatusName
                    bookStatus.displayname = dataresponse?.disbookStatusName
                    bookStatus.updation_ip_address = ip
                    bookStatus.updation_date = new Date()
                    bookStatus.updation_username = uid
                    bookStatus.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm

            }
        }
        return hm
    }
    //Get Reservation Status Data
    def getReservationStatus(request, hm) {
        println("Inside getBookStatus")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        String purpose = 'Get Reservation Status Data'
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def reservationStatuslist = []
                def reservationStatus = ReservationStatusSuperMaster.findAll()
                int srno = 1
                for (ut in reservationStatus) {
                    HashMap temp = new HashMap()
                    temp.put("reservationStatus", ut.name)
                    temp.put("reservationStatusId", ut.id)
                    temp.put("reservationStatusIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    reservationStatuslist.add(temp)
                }
                hm.put('reservationStatus', reservationStatuslist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm

        }
    }
    //Save Reservation Status
    def addReservationStatus(request, hm, ip) {
        println "i am in addReservationStatus"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addReservationStatus from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def reservationStatuslist = []
            ReservationStatusSuperMaster reservationStatus = ReservationStatusSuperMaster.findByName(dataresponse?.reservationStatus_name)
                if (reservationStatus != null) {
                    hm.put("msg", "Book Status already exist!!")
                } else {
                    reservationStatus = new ReservationStatusSuperMaster()
                    reservationStatus.isactive = true
                    reservationStatus.name = dataresponse?.reservationStatus_name
                    reservationStatus.updation_ip_address = ip
                    reservationStatus.creation_ip_address = ip
                    reservationStatus.updation_date = new Date()
                    reservationStatus.creation_date = new Date()
                    reservationStatus.updation_username = uid
                    reservationStatus.creation_username = uid
                    reservationStatus.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def allP_List = ReservationStatusSuperMaster.findAll()
                int srno = 1
                for (pub in allP_List) {
                    HashMap temp = new HashMap()
                    temp.put("reservationStatus", pub.name)
                    temp.put("reservationStatusId", pub.id)
                    temp.put("reservationStatusIsactive", pub.isactive)
                    temp.put("srno", srno++)
                    reservationStatuslist.add(temp)
                }
                hm.put("reservationStatus_list", reservationStatuslist)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm

        }
        return hm
    }
    //Active or In Active  ReservationS tatus
    def reservationStatusIsactive(request, hm) {
        println "i am in reservationStatusIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "reservationStatusIsactive from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []

                def reservationStatus = ReservationStatusSuperMaster.findById(dataresponse?.reservationStatusId)
                if (reservationStatus != null) {
                    if (reservationStatus?.isactive == false) {
                        reservationStatus.isactive = true
                        reservationStatus.updation_ip_address = request.getRemoteAddr()
                        reservationStatus.updation_date = new Date()
                        reservationStatus.updation_username = uid
                        reservationStatus.save(flush: true, failOnError: true)
                    } else {
                        reservationStatus.isactive = false
                        reservationStatus.updation_ip_address = request.getRemoteAddr()
                        reservationStatus.updation_date = new Date()
                        reservationStatus.updation_username = uid
                        reservationStatus.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm

        }
        return hm
    }
    //Edit Data Reservation Status
    def editReservationStatus(request, hm, ip) {
        println "i am in editReservationStatus"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editReservationStatus from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ReservationStatusSuperMaster reservationStatus = ReservationStatusSuperMaster.findById(dataresponse?.reservationStatusId)

                def exist = ReservationStatusSuperMaster.findByName(dataresponse?.reservationStatusName)
                if (exist != null) {
                    hm.put("msg", "Reservation Status already exist!!")
                } else {
                    reservationStatus.name = dataresponse?.reservationStatusName
                    reservationStatus.updation_ip_address = ip
                    reservationStatus.updation_date = new Date()
                    reservationStatus.updation_username = uid
                    reservationStatus.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm
                }

        }
        return hm
    }
    //Get Data Account Status
    def getAccountStatus(request, hm) {
        println("Inside getAccountStatus")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def accountStatuslist = []
                def accountStatus = AccountStatusSuperMaster.findAll()
                int srno = 1
                for (ut in accountStatus) {
                    HashMap temp = new HashMap()
                    temp.put("accountStatus", ut.name)
                    temp.put("accountStatusId", ut.id)
                    temp.put("accountStatusIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    accountStatuslist.add(temp)
                }
                hm.put('accountStatus', accountStatuslist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm

        }
    }
    //Save Account Status
    def addAccountStatus(request, hm, ip) {
        println "i am in addAccountStatus"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addAccountStatus from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def accountStatuslist = []
                AccountStatusSuperMaster accountStatus = AccountStatusSuperMaster.findByName(dataresponse?.accountStatus_name)
                if (accountStatus != null) {
                    hm.put("msg", "Account Status already exist!!")
                } else {
                    accountStatus = new AccountStatusSuperMaster()
                    accountStatus.isactive = true
                    accountStatus.name = dataresponse?.accountStatus_name
                    accountStatus.updation_ip_address = ip
                    accountStatus.creation_ip_address = ip
                    accountStatus.updation_date = new Date()
                    accountStatus.creation_date = new Date()
                    accountStatus.updation_username = uid
                    accountStatus.creation_username = uid
                    accountStatus.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def allP_List = AccountStatusSuperMaster.findAll()
                int srno = 1
                for (pub in allP_List) {
                    HashMap temp = new HashMap()
                    temp.put("accountStatus", pub.name)
                    temp.put("accountStatusId", pub.id)
                    temp.put("accountStatusIsactive", pub.isactive)
                    temp.put("srno", srno++)
                    accountStatuslist.add(temp)
                }
                hm.put("accountStatus_list", accountStatuslist)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm

        }
        return hm
    }
    //Change Status Account Status
    def accountStatusIsactive(request, hm) {
        println "i am in accountStatusIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "accountStatusIsactive from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
                def accountStatus = AccountStatusSuperMaster.findById(dataresponse?.accountStatusId)
                if (accountStatus != null) {
                    if (accountStatus?.isactive == false) {
                        accountStatus.isactive = true
                        accountStatus.updation_ip_address = request.getRemoteAddr()
                        accountStatus.updation_date = new Date()
                        accountStatus.updation_username = uid
                        accountStatus.save(flush: true, failOnError: true)
                    } else {
                        accountStatus.isactive = false
                        accountStatus.updation_ip_address = request.getRemoteAddr()
                        accountStatus.updation_date = new Date()
                        accountStatus.updation_username = uid
                        accountStatus.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm

        }
        return hm
    }
    //Save Edit Data Account Status
    def editAccountStatus(request, hm, ip) {
        println "i am in editAccountStatus"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editAccountStatus from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            AccountStatusSuperMaster accountStatus = AccountStatusSuperMaster.findById(dataresponse?.accountStatusId)

                def exist = AccountStatusSuperMaster.findByName(dataresponse?.accountStatusName)
                if (exist != null) {
                    hm.put("msg", "Account Status already exist!!")
                } else {
                    accountStatus.name = dataresponse?.accountStatusName
                    accountStatus.updation_ip_address = ip
                    accountStatus.updation_date = new Date()
                    accountStatus.updation_username = uid
                    accountStatus.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm

            }
        }
        return hm
    }
    //Get Data TransactionT ype
    def getTranType(request, hm) {
        println("Inside getTranType")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def tranTypelist = []
                def tranType = TransactionTypeSuperMaster.findAll()
                int srno = 1
                for (ut in tranType) {
                    HashMap temp = new HashMap()
                    temp.put("tranType", ut.name)
                    temp.put("tranTypeId", ut.id)
                    temp.put("tranTypeIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    tranTypelist.add(temp)
                }
                hm.put('tranType', tranTypelist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm

        }
    }
    //Save TransactionType
    def addTranType(request, hm, ip) {
        println "i am in addTranType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addTranType from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def tranTypelist = []
//
            TransactionTypeSuperMaster tranType = TransactionTypeSuperMaster.findByName(dataresponse?.tranType_name)
                if (tranType != null) {
                    hm.put("msg", "Account Status already exist!!")
                } else {
                    tranType = new TransactionTypeSuperMaster()
                    tranType.isactive = true
                    tranType.name = dataresponse?.tranType_name
                    tranType.updation_ip_address = ip
                    tranType.creation_ip_address = ip
                    tranType.updation_date = new Date()
                    tranType.creation_date = new Date()
                    tranType.updation_username = uid
                    tranType.creation_username = uid
                    tranType.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def allP_List = TransactionTypeSuperMaster.findAll()
                int srno = 1
                for (pub in allP_List) {
                    HashMap temp = new HashMap()
                    temp.put("tranType", pub.name)
                    temp.put("tranTypeId", pub.id)
                    temp.put("tranTypeIsactive", pub.isactive)
                    temp.put("srno", srno++)
                    tranTypelist.add(temp)
                }
                hm.put("tranType_list", tranTypelist)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm

        }
        return hm
    }
    //Active In Active TransactionType
    def tranTypeIsactive(request, hm) {
        println "i am in tranTypeIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "tranTypeIsactive from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []

                def transType = TransactionTypeSuperMaster.findById(dataresponse?.tranTypeId)
                if (transType != null) {
                    if (transType?.isactive == false) {
                        transType.isactive = true
                        transType.updation_ip_address = request.getRemoteAddr()
                        transType.updation_date = new Date()
                        transType.updation_username = uid
                        transType.save(flush: true, failOnError: true)
                    } else {
                        transType.isactive = false
                        transType.updation_ip_address = request.getRemoteAddr()
                        transType.updation_date = new Date()
                        transType.updation_username = uid
                        transType.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm

        }
        return hm
    }
    //Save Edit Data TransactionType
    def editTranType(request, hm, ip) {
        println "i am in editTranType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editTranType from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            TransactionTypeSuperMaster transType = TransactionTypeSuperMaster.findById(dataresponse?.tranTypeId)

                def exist = TransactionTypeSuperMaster.findByName(dataresponse?.tranTypeName)
                if (exist != null) {
                    hm.put("msg", "Transaction Type already exist!!")
                } else {
                    transType.name = dataresponse?.tranTypeName
                    transType.updation_ip_address = ip
                    transType.updation_date = new Date()
                    transType.updation_username = uid
                    transType.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm
                }

        }
        return hm
    }
    //get All Data NotificationMethod
    def getNotifyMethodData(request, hm, ip) {
        println("in getNotifyMethodData:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String purpose = "getNotifyMethodData from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList methods_list = new ArrayList()
                int srno = 1
                for (item in NotificationMethodSuperMaster.findAll()) {
                    HashMap temp = new HashMap()
                    temp.put("srno", srno++)
                    temp.put("id", item.id)
                    temp.put("nm", item.name)
                    temp.put("isactive", item.isactive)
                    methods_list.add(temp)


                hm.put("methods_list", methods_list)
            }//println"hm: "+hm
            return hm

        }
        return hm
    }
    //Save Edit Data NotificationMethod
    def editNotifyMethod(request, hm, ip) {
        println("in editNotifyMethod:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:===" + dataresponse

        AuthService auth = new AuthService()

        String purpose = "editNotifyMethod  from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
                def notifyMethod = NotificationMethodSuperMaster.findById(dataresponse.nmId)
                if (notifyMethod != null) {
                    notifyMethod.name = dataresponse.nmName
                    notifyMethod.updation_username = uid
                    notifyMethod.updation_date = new java.util.Date()
                    notifyMethod.updation_ip_address = ip
                    notifyMethod.save(failOnError: true, flush: true)

                    HashMap status = new HashMap()
                    status.put("code", "SUCCESS")
                    hm.put("status", 'YES')

                return hm
            }
        }
        return hm
    }
    //Save New Data NotificationMethod
    def saveNotifyMethod(request, hm, ip) {
        println("in saveNotifyMethod:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:" + dataresponse

        AuthService auth = new AuthService()

        String purpose = "saveNotifyMethod from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        println("dataresponse")
        println(dataresponse)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
                def notifyMethod = NotificationMethodSuperMaster.findByName(dataresponse.nm_name)
                if (notifyMethod == null) {
                    notifyMethod = new NotificationMethodSuperMaster()
                    notifyMethod.name = dataresponse.nm_name
                    notifyMethod.isactive = true
                    notifyMethod.creation_username = uid
                    notifyMethod.updation_username = uid
                    notifyMethod.creation_date = new java.util.Date()
                    notifyMethod.updation_date = new java.util.Date()
                    notifyMethod.creation_ip_address = ip
                    notifyMethod.updation_ip_address = ip
                    notifyMethod.save(failOnError: true, flush: true)

                    HashMap status = new HashMap()
                    status.put("code", "SUCCESS")
                    status.put("message", "Notification method added successfully...")
                    hm.put("status", "YES")
                    return hm
                } else {
                    HashMap status = new HashMap()
                    status.put("code", "NA")
                    status.put("message", "Notification method Already present!!!")
                    hm.put("status", 'NO')
                }

//
            return hm

        }
        return hm
    }
    //Active or in Active NotificationMethod
    def nMethodIsactive(request, hm, ip) {
        println "i am in nMethodIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "nMethodIsactive from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
                def method = NotificationMethodSuperMaster.findById(dataresponse?.id)
                if (method != null) {
                    if (method?.isactive == false) {
                        method.isactive = true
                        method.updation_ip_address = request.getRemoteAddr()
                        method.updation_date = new Date()
                        method.updation_username = uid
                        method.save(flush: true, failOnError: true)
                    } else {
                        method.isactive = false
                        method.updation_ip_address = request.getRemoteAddr()
                        method.updation_date = new Date()
                        method.updation_username = uid
                        method.save(flush: true, failOnError: true)
                    }

                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }
    //Get All Data NotificationType
    def getNotifyType(request, hm) {
        println("Inside getNotifyType")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def notifyTypelist = []
                def notifyType = NotificationTypeSuperMaster.findAll()
                int srno = 1
                for (ut in notifyType) {
                    HashMap temp = new HashMap()
                    temp.put("notifyType", ut.name)
                    temp.put("notifyTypeId", ut.id)
                    temp.put("notifyTypeIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    notifyTypelist.add(temp)
                }
                hm.put('notifyType', notifyTypelist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm

        }
    }
    //Save New NotificationType
    def addNotifyType(request, hm, ip) {
        println "i am in addNotifyType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addNotifyType from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def notifyTypelist = []//                def org = user.organization
//                hm.put("org",org)
                NotificationTypeSuperMaster notifyType = NotificationTypeSuperMaster.findByName(dataresponse?.notifyType_name)
                if (notifyType != null) {
                    hm.put("msg", "Account Status already exist!!")
                } else {
                    notifyType = new NotificationTypeSuperMaster()
                    notifyType.isactive = true
                    notifyType.name = dataresponse?.notifyType_name
                    notifyType.updation_ip_address = ip
                    notifyType.creation_ip_address = ip
                    notifyType.updation_date = new Date()
                    notifyType.creation_date = new Date()
                    notifyType.updation_username = uid
                    notifyType.creation_username = uid
                    notifyType.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }
                def allP_List = NotificationTypeSuperMaster.findAll()
                int srno = 1
                for (pub in allP_List) {
                    HashMap temp = new HashMap()
                    temp.put("notifyType", pub.name)
                    temp.put("notifyTypeId", pub.id)
                    temp.put("notifyTypeIsactive", pub.isactive)
                    temp.put("srno", srno++)
                    notifyTypelist.add(temp)
                }
                hm.put("notifyType_list", notifyTypelist)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm

        }
        return hm
    }
    //Chenge Active or In-active
    def notifyTypeIsactive(request, hm) {
        println "i am in notifyTypeIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "notifyTypeIsactive from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []

                def notifyType = NotificationTypeSuperMaster.findById(dataresponse?.notifyTypeId)
                if (notifyType != null) {
                    if (notifyType?.isactive == false) {
                        notifyType.isactive = true
                        notifyType.updation_ip_address = request.getRemoteAddr()
                        notifyType.updation_date = new Date()
                        notifyType.updation_username = uid
                        notifyType.save(flush: true, failOnError: true)
                    } else {
                        notifyType.isactive = false
                        notifyType.updation_ip_address = request.getRemoteAddr()
                        notifyType.updation_date = new Date()
                        notifyType.updation_username = uid
                        notifyType.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm

        }
        return hm
    }
    //Save Edit Data NotificationType
    def editNotifyType(request, hm, ip) {
        println "i am in editNotifyType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editNotifyType from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
                NotificationTypeSuperMaster notifyType = NotificationTypeSuperMaster.findById(dataresponse?.notifyTypeId)

                def exist = NotificationTypeSuperMaster.findByName(dataresponse?.notifyTypeName)
                if (exist != null) {
                    hm.put("msg", "Notification ype already exist!!")
                } else {
                    notifyType.name = dataresponse?.notifyTypeName
                    notifyType.updation_ip_address = ip
                    notifyType.updation_date = new Date()
                    notifyType.updation_username = uid
                    notifyType.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm
                }

        }
        return hm
    }
    //Get Data LibraryConfiguration
    def getLibConfig(request, hm) {
        println("Inside getLibConfig.")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def libConfiglist = []
                def libConfig = LibraryConfigurationSuperMaster.findAll()
                int srno = 1
                for (ut in libConfig) {
                    HashMap temp = new HashMap()
                    temp.put("libConfig", ut.name)
                    temp.put("libConfigId", ut.id)
                    temp.put("libConfigValue", ut.value)
                    temp.put("libConfigIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    libConfiglist.add(temp)
                }
                hm.put('libConfig', libConfiglist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm

        }
    }
    //Save New LibraryConfiguration
    def addLibConfig(request, hm, ip) {
        println "i am in addLibConfig"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "Edit addLibConfig from Admin  Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def libConfiglist = []
//                def org = user.organization
//                hm.put("org",org
            LibraryConfigurationSuperMaster libConfig = LibraryConfigurationSuperMaster.findByName(dataresponse?.libConfig_name)
                if (libConfig != null) {
                    hm.put("msg", "Library Configuration already exist!!")
                } else {
                    libConfig = new LibraryConfigurationSuperMaster()
                    libConfig.isactive = true
                    libConfig.name = dataresponse?.libConfig_name
                    libConfig.value = dataresponse?.libConfigValue
                    libConfig.updation_ip_address = ip
                    libConfig.creation_ip_address = ip
                    libConfig.updation_date = new Date()
                    libConfig.creation_date = new Date()
                    libConfig.updation_username = uid
                    libConfig.creation_username = uid
                    libConfig.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def allP_List = LibraryConfigurationSuperMaster.findAll()
                int srno = 1
                for (config in allP_List) {
                    HashMap temp = new HashMap()
                    temp.put("libConfig", config.name)
                    temp.put("libConfigId", config.id)
                    temp.put("libConfigValue", config.value)
                    temp.put("libConfigIsactive", config.isactive)
                    temp.put("srno", srno++)
                    libConfiglist.add(temp)
                }
                hm.put("libConfig_list", libConfiglist)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm

        }
        return hm
    }
    //Change Active or In Active LibraryConfiguration
    def LibConfigIsactive(request, hm) {
        println "i am in LibConfigIsactive  "

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "Edit User Types from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []

                def libConfig = LibraryConfigurationSuperMaster.findById(dataresponse?.libConfigId)
                if (libConfig != null) {
                    if (libConfig?.isactive == false) {
                        libConfig.isactive = true
                        libConfig.updation_ip_address = request.getRemoteAddr()
                        libConfig.updation_date = new Date()
                        libConfig.updation_username = uid
                        libConfig.save(flush: true, failOnError: true)
                    } else {
                        libConfig.isactive = false
                        libConfig.updation_ip_address = request.getRemoteAddr()
                        libConfig.updation_date = new Date()
                        libConfig.updation_username = uid
                        libConfig.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm

        }
        return hm
    }
    //Save Edit Data LibraryConfiguration
    def editLibConfig(request, hm, ip) {
        println "i am in editLibConfig"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editLibConfig from Admin  Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            LibraryConfigurationSuperMaster libConfig = LibraryConfigurationSuperMaster.findById(dataresponse?.libConfigId)
                libConfig.name = dataresponse?.libConfigName
                libConfig.value = dataresponse?.libConfigValue
                libConfig.updation_ip_address = ip
                libConfig.updation_date = new Date()
                libConfig.updation_username = uid
                libConfig.save(flush: true, failOnError: true)
                hm.put("msg", "success")
                hm.put("status", "200")
                return hm

        }
        return hm
    }
    //Get Role Link Data
    def adminAddRoleLink(request, hm, ip) {
        println("in adminAddRole:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String purpose = "Getting Role Link from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList role_list = new ArrayList()
                for (item in LinkSuperMaster.findAll()) {
                    HashMap temp = new HashMap()
                    temp.put("roleLinkId", item?.id)
                    temp.put("roleLinkName", item?.link_name)
                    temp.put("roleLink", item?.link_display_name)
                    temp.put("roleLinkIcon", item?.icon)
                    temp.put("roleLinkSortOrder", item?.sort_order)
                    if (item?.rolesupermaster?.id!=0)
                        temp.put("roleId", item?.rolesupermaster?.id)
                        temp.put("role", item?.rolesupermaster?.name)
                    if (item?.usertypesupermaster?.id!=0)
                        temp.put("ut_id", item?.usertypesupermaster?.id)
                        temp.put("ut", item?.usertypesupermaster?.name)
                    //temp.put("roleLinkDescription", item.linkdescription)
                    //temp.put("roleLinkSortOrder", item.sort_order)
                    //temp.put("role", item?.role?.name)
                    temp.put("actionName", item?.action_name)
                    temp.put("controllerName", item?.controller_name)
                    temp.put("isactive", item.isactive)
                    role_list.add(temp)
                }
                def allroles=RoleSuperMaster.list()
                def roles=[]
                for(rl in allroles){
                    HashMap role=new HashMap()
                    role.put("role_id",rl?.id)
                    role.put("role",rl?.name)
                    roles.add(role)
                }
                def allut=UserTypeSuperMaster.list()
                def uts=[]
                for(u in allut){
                    HashMap ut=new HashMap()
                    ut.put("ut_id",u?.id)
                    ut.put("u_type",u?.name)
                    uts.add(ut)
                }

                hm.put("uts", uts)
                hm.put("roles", roles)
                hm.put("roleLink_list", role_list)

            }//println"hm: "+hm
            return hm


        return hm
    }
    //Save Edit Role Link Data
    def editAdminAddRoleLink(request, hm, ip) {
        println("in editAdminAddRole:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:" + dataresponse

        AuthService auth = new AuthService()

        String purpose = "Editing Role from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            LinkSuperMaster roleObj = LinkSuperMaster.findById(dataresponse.roleLinkId)
                if (roleObj != null) {
                    String oldName = roleObj.link_name
                    roleObj.link_name = dataresponse.roleLinkName
                    roleObj.link_display_name = dataresponse.roleLink
                    roleObj.icon = dataresponse.roleLinkIcon
                    roleObj.controller_name = dataresponse.controllerName
                    roleObj.action_name = dataresponse.actionName
                    roleObj.isactive = dataresponse.isactive
                    if (roleObj.sort_order==dataresponse.roleLinkSortOrder){
                        roleObj.sort_order = dataresponse.roleLinkSortOrder
                    }else {
                        roleObj.sort_order = Integer.parseInt(dataresponse.roleLinkSortOrder)
                    }
                    roleObj.rolesupermaster = RoleSuperMaster.findById(dataresponse.roleId)
                    roleObj.usertypesupermaster = UserTypeSuperMaster.findById(dataresponse.ut_id)
                    roleObj.updation_username = uid
                    roleObj.updation_date = new java.util.Date()
                    roleObj.updation_ip_address = request.getRemoteAddr()
                    roleObj.save(failOnError: true, flush: true)

                    def listForDuplicate = LinkSuperMaster.findAllByLink_nameAndIsactive(dataresponse.roleLinkName, dataresponse.isactive)
                    if (listForDuplicate.size() > 1) {
                        println "1"
                        roleObj.link_name = oldName
                        roleObj.updation_username = uid
                        roleObj.updation_date = new java.util.Date()
                        roleObj.updation_ip_address = request.getRemoteAddr()
                        roleObj.save(failOnError: true, flush: true)

                        HashMap status = new HashMap()
                        status.put("code", "NA")
                        hm.put("status", status)
                    } else {
                        println "2"

                        HashMap status = new HashMap()
                        status.put("code", "SUCCESS")
                        hm.put("status", status)
                    }
                }
                return hm

        }
        return hm
    }
    //Save Link Data
    def saveAdminAddRoleLink(request, hm, ip) {
        println("in save Link:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:" + dataresponse

        AuthService auth = new AuthService()

        String purpose = "Saving  Link"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        println("dataresponse")
        println(dataresponse)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            RoleSuperMaster roleSuperMaster=RoleSuperMaster.findById(dataresponse.roleId)
            UserTypeSuperMaster userTypeSuperMaster=UserTypeSuperMaster.findById(dataresponse.ut_id)
            //LinkSuperMaster rolelink = LinkSuperMaster.findByLink_name(dataresponse.roleLinkName)
            LinkSuperMaster rolelink = LinkSuperMaster.findByLink_nameAndRolesupermasterAndUsertypesupermaster(dataresponse.roleLinkName,roleSuperMaster,userTypeSuperMaster)
            if (rolelink == null) {
                    rolelink = new LinkSuperMaster()
                    rolelink.link_name = dataresponse.roleLinkName
                    rolelink.link_display_name = dataresponse.roleLink
                    rolelink.icon = dataresponse.roleLinkIcon
                    rolelink.isactive = dataresponse.isactive
                    rolelink.controller_name = dataresponse.controllerName
                    rolelink.action_name = dataresponse.actionName
                    rolelink.isactive = true
                    rolelink.sort_order = Integer.parseInt(dataresponse.roleLinkSortOrder)
                    rolelink.rolesupermaster = RoleSuperMaster.findById(dataresponse.roleId)
                    rolelink.usertypesupermaster = UserTypeSuperMaster.findById(dataresponse.ut_id)
                    rolelink.creation_username = uid
                    rolelink.updation_username = uid
                    rolelink.creation_date = new java.util.Date()
                    rolelink.updation_date = new java.util.Date()
                    rolelink.creation_ip_address = ip
                    rolelink.updation_ip_address = ip
                    rolelink.save(failOnError: true, flush: true)
                    HashMap status = new HashMap()
                    status.put("code", "SUCCESS")
                    status.put("message", "Role added successfully...")
                    hm.put("status", status)
                } else {
                    HashMap status = new HashMap()
                    status.put("code", "NA")
                    status.put("message", "Role Already present!!!")
                    hm.put("status", status)
                }

            return hm

        }
        return hm
    }
    //Actie In-Active
    def roleLinksIsactive(request, hm) {
        println "i am in roleLinksIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "roleLinks Isactive from Admin  Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
                def roleLink = LinkSuperMaster.findById(dataresponse?.roleLinkId)
                if (roleLink != null) {
                    if (roleLink?.isactive == false) {
                        roleLink.isactive = true
                        roleLink.updation_ip_address = request.getRemoteAddr()
                        roleLink.updation_date = new Date()
                        roleLink.updation_username = uid
                        roleLink.save(flush: true, failOnError: true)
                    } else {
                        roleLink.isactive = false
                        roleLink.updation_ip_address = request.getRemoteAddr()
                        roleLink.updation_date = new Date()
                        roleLink.updation_username = uid
                        roleLink.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm

        }
        return hm
    }
    //Find Username in Login Table
    def findusername(request, hm) {
        println("Inside AdminService/find username...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        def username = dataresponse.username
        def memberId = dataresponse.selectedtenant.tc_id
        println(username)
        println(memberId)
        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            TenantClient tc=TenantClient.findById(memberId)
            println("TC ::"+tc)
            Tenants.withId(tc.tenantid) {
                Organization org=Organization.findByTenantid(tc.tenantid)
                println("loginattachementdomain ::"+org.loginattachementdomain)
                println("username ::"+username+org.loginattachementdomain)
                String newid=username+org.loginattachementdomain
                def user = Login.findByUsername(newid)
                println("user ::"+user)
                boolean status=false
                if (user) {
                    hm.put('message', 'User name is not available!')
                    hm.put('code', 1)
                    hm.put('status', status=true)
                } else {

                        hm.put('message', 'User name is available')
                        hm.put('code', 2)
                        hm.put('status', status=false)

                }
            }
                println "hm: " + hm
                hm.put("msg", "200")
                return hm

        }
    }
    //Save New User Data
    def saveUsername(request, ip, hm) {
        println("Inside AdminService/save username...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        String purpose = 'New Login'
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        def username = dataresponse.username
        def selectedtenant = dataresponse.selectedtenant.tc_id
        println("dataresponse ::"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            TenantClient tc=TenantClient.findById(selectedtenant)
            println("TC ::"+tc)
            Tenants.withId(tc.tenantid) {
                Organization org=Organization.findByTenantid(tc.tenantid)
                String newid=username+org.loginattachementdomain
                def login = Login.findByUsername(newid)
                if(login==null) {
                    login = new Login()
                    login.username = newid
                    login.password = newid
                    login.grno_empid = username
                    login.tenantclient = tc
                    login.isloginblocked = false
                    login.password_update_date = new Date()
                    login.lastlogindate = new Date()
                    login.tokenexpiry = new Date()
                    login.creation_username = uid
                    login.updation_username = uid
                    login.creation_date = new Date()
                    login.updation_date = new Date()
                    login.creation_ip_address = ip
                    login.updation_ip_address = ip
                    login.save(flush: true, failOnError: true)
                }
                UserType usertype = UserType.findByName('Employee')
                println("usertype :"+usertype)
                User user = User.findByUsernameAndOrganization(newid, org)
                if (user) {
                    hm.put('message', 'User already available!')
                    hm.put('code', 1)
                }
                else {
                    user = new User()
                    user.username = newid
                    user.total_issued_books = 0
                    user.creation_username = uid
                    user.updation_username = uid
                    user.creation_date = new Date()
                    user.updation_date = new Date()
                    user.creation_ip_address = ip
                    user.updation_ip_address = ip
                    user.organization = org
                    user.usertype = usertype
                    user.save(flush: true, failOnError: true)

                    Person person = new Person()
                    person.creation_username = uid
                    person.updation_username = uid
                    person.creation_date = new Date()
                    person.updation_date = new Date()
                    person.creation_ip_address = ip
                    person.updation_ip_address = ip
                    person.user = user
                    person.save(flush: true, failOnError: true)

                    Employee employee = new Employee()
                    employee.employee_code = username
                    employee.name = newid
                    employee.creation_username = uid
                    employee.updation_username = uid
                    employee.creation_date = new Date()
                    employee.updation_date = new Date()
                    employee.creation_ip_address = ip
                    employee.updation_ip_address = ip
                    employee.organization = org
                    employee.user = user
                    employee.person = person
                    employee.save(flush: true, failOnError: true)
                    //Role role = Role.findByNameAndUsertype
                    Role role
                    if (usertype.name.equalsIgnoreCase("Employee")) {
                        //role = Role.findByNameIlikeAndUsertype("%Teacher%",usertype)
                        role = Role.findByNameIlike("%Admin%")
                    } else {
                        role = Role.findByNameIlike("%Member%")
                        //role = Role.findByNameIlikeAndUsertype("%Member%",usertype)
                    }
                    user.addToRole(role)
                    user.save(flush: true, failOnError: true)
                }
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }
    //User Login Data Admin
    def findusertype(request, hm) {
        println("Inside AdminService/findusertype...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
                def tenantlist = TenantClient.list()
                for (ut in tenantlist) {
                    HashMap temp = new HashMap()
                    temp.put("name", ut.name)
                    temp.put("tc_id", ut.id)
                    utlist.add(temp)
                }
                hm.put('userType', utlist)

                // find all user of the organization
                def alluser = Login.list()
                def userlist = []
                for (usr in alluser) {
                    HashMap temp = new HashMap()
                    temp.put("userid", usr.id)
                    temp.put("user", usr.username)
                    userlist.add(temp)
                }
                hm.put('userlist', userlist)
                hm.put("msg", "200")
                return hm

        }
    }
    //Save Edit Login Data
    def updateUtype(request, ip, hm) {
        println("Inside AdminService/update utype registration number and emp code...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse ::"+dataresponse)
        String purpose = "Edit Login"
        auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            TenantClient tc=TenantClient.findById(dataresponse.selectedtenant.tc_id)
            println("TC ::"+tc)
            Tenants.withId(tc.tenantid)  {
                def login = Login.findById(dataresponse.userid)
                Organization org=Organization.findByTenantid(tc?.tenantid)
                println("Login ::"+login)
                if(login!=null) {
                    login.username = dataresponse.user
                    login.tenantclient = tc
                    login.updation_username = uid
                    login.updation_date = new Date()
                    login.updation_ip_address = ip
                    login.save(flush: true, failOnError: true)
                }
                UserType usertype = UserType.findByName('Employee')
                println("usertype :"+usertype)
                User user = User.findByUsernameAndOrganization(login?.username, org)
                if (user!=null) {
                    user.username = dataresponse.user
                    user.total_issued_books = 0
                    user.creation_username = uid
                    user.updation_username = uid
                    user.creation_date = new Date()
                    user.updation_date = new Date()
                    user.creation_ip_address = ip
                    user.updation_ip_address = ip
                    user.organization = org
                    user.usertype = usertype
                    user.save(flush: true, failOnError: true)

                    Person person = Person.findUser(user)
                    if (person!=null) {
                        person.creation_username = uid
                        person.updation_username = uid
                        person.creation_date = new Date()
                        person.updation_date = new Date()
                        person.creation_ip_address = ip
                        person.updation_ip_address = ip
                        person.user = user
                        person.save(flush: true, failOnError: true)
                    }
                    Employee employee = Employee.findByUserAndOrganizationAndPerson(login?.username, org,person)
                    if (employee!=null) {
                        employee.employee_code = dataresponse.user
                        employee.name = dataresponse.user
                        employee.updation_username = uid
                        employee.updation_date = new Date()
                        employee.updation_ip_address = ip
                        employee.organization = org
                        employee.user = user
                        employee.person = person
                        employee.save(flush: true, failOnError: true)
                    }
                    Role role
                    if (usertype.name.equalsIgnoreCase("Employee")) {
                        //role = Role.findByNameIlikeAndUsertype("%Teacher%",usertype)
                        role = Role.findByNameIlike("%Admin%")
                    } else {
                        role = Role.findByNameIlike("%Member%")
                        //role = Role.findByNameIlikeAndUsertype("%Member%",usertype)
                    }
                    user.addToRole(role)
                    user.save(flush: true, failOnError: true)
                }
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }
    //Get All Org. Data
    def getOrgData(request, hm, ip) {
        println("in getOrgData:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:--" + dataresponse
        AuthService auth = new AuthService()

        String purpose = "getTemplateData from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList org_list = new ArrayList()
            if (dataresponse?.tenant!=null) {
                TenantClient tc = TenantClient.findById(dataresponse?.tenant)
                println("TC::" + tc)
                Tenants.withId(tc.tenantid) {
                    int srno = 1
                    for (item in Organization.findAll()) {
                        HashMap temp = new HashMap()
                        temp.put("srno", srno++)
                        temp.put("id", item.id)
                        temp.put("name", item.name)
                        temp.put("display_name", item.displayname)
                        temp.put("tenant_name", item.tenantname)
                        temp.put("address", item.address)
                        temp.put("cp_name", item?.contact_person_name)
                        temp.put("email", item?.email)
                        temp.put("contact", item?.contact)
                        temp.put("domain", item?.loginattachementdomain)
                        temp.put("website", item?.website)
                        temp.put("country", item?.country)
                        temp.put("isactive", item?.isactive)
                        org_list.add(temp)
                    }
                }

            }
            def tclist=[]
            def tenantlist = TenantClient.list()
            for (ut in tenantlist) {
                HashMap temp = new HashMap()
                temp.put("name", ut?.name)
                temp.put("tc_id", ut?.id)
                tclist.add(temp)
            }
            hm.put('tclist', tclist)
            hm.put("org_list", org_list)
            return hm
        }
        return hm
    }
    //Save Edit org. Data
    def editOrg(request, hm, ip) {
        println("in editOrg:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:" + dataresponse

        AuthService auth = new AuthService()

        String purpose = "edit Org from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            TenantClient tc=TenantClient.findById(dataresponse.selectedtenant.tc_id)
            println("TC ::"+tc)
            Tenants.withId(tc.tenantid)  {
                Organization org = Organization.findById(dataresponse.id)
                if (org != null) {
                    org.tenantname = dataresponse.tenant_name
                    org.name = dataresponse.name
                    org.displayname = dataresponse.display_name
                    org.address = dataresponse.address
                    org.contact_person_name = dataresponse.cp_name
                    org.isactive = true
                    org.email = dataresponse.email
                    org.contact = dataresponse.contact
                    org.website = dataresponse.website
                    org.country = dataresponse.country
                    org.loginattachementdomain = dataresponse.domain
                    org.updation_username = uid
                    org.updation_date = new java.util.Date()
                    org.updation_ip_address = ip
                    org.save(failOnError: true, flush: true)

                    HashMap status = new HashMap()
                    status.put("code", "SUCCESS")
                    hm.put("status", status)
                }
                return hm
            }
        }
        return hm
    }
    //Save New Org.
    def saveOrg(request, hm, ip) {
        println("in saveOrg:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:" + dataresponse
        AuthService auth = new AuthService()
        String purpose = "saveOrg from admin  menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        println("request")
        println(request)

        def tenant = TenantClient.findById(dataresponse.selectedtenant.tc_id)
        println('tenant----'+tenant)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(tenant.tenantid) {
                def org = Organization.findByName(dataresponse.name)
                if (org == null) {
                    org = new Organization()
                    if (tenant) {
                        org.tenantid = tenant?.tenantid
                    }
                    org.tenantname = tenant?.tenantid
                    org.name = dataresponse.name
                    org.displayname = dataresponse.display_name
                    org.address = dataresponse.address
                    org.contact_person_name = dataresponse.cp_name
                    org.isactive = true
                    org.email = dataresponse.email
                    org.contact = dataresponse.contact
                    org.website = dataresponse.website
                    org.country = dataresponse.country
                    org.library = null
                    org.loginattachementdomain = dataresponse.domain
                    org.logofilepath = 'NA'
                    org.logofilename = 'NA'
                    org.creation_username = uid
                    org.updation_username = uid
                    org.creation_date = new java.util.Date()
                    org.updation_date = new java.util.Date()
                    org.creation_ip_address = ip
                    org.updation_ip_address = ip
                    org.save(failOnError: true, flush: true)

                    HashMap status = new HashMap()
                    status.put("code", "SUCCESS")
                    status.put("message", "Organization added successfully...")
                    hm.put("status", status)
                    return hm
                } else {
                    HashMap status = new HashMap()
                    status.put("code", "NA")
                    status.put("message", "Organization Already present!!!")
                    hm.put("status", status)
                }
            }
            return hm

        }
        return hm
    }
    //Change Status Active And In-Active
    def orgIsactive(request, hm) {
        println "i am in orgIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "orgIsactive from Admin  Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            TenantClient tc=TenantClient.findById(dataresponse.tenant)
            println("TC ::"+tc)
            Tenants.withId(tc.tenantid) {
                def org = Organization.findById(dataresponse?.item.id)
                if (org != null) {
                    if (org?.isactive == false) {
                        org.isactive = true
                        org.updation_ip_address = request.getRemoteAddr()
                        org.updation_date = new Date()
                        org.updation_username = uid
                        org.save(flush: true, failOnError: true)
                    } else {
                        org.isactive = false
                        org.updation_ip_address = request.getRemoteAddr()
                        org.updation_date = new Date()
                        org.updation_username = uid
                        org.save(flush: true, failOnError: true)
                    }

                    hm.put("status", "200")
                    println('hm---' + hm)
                    return hm
                }
            }
        }
        return hm
    }
    //Import Multiplale Org
    def importOrg(request, hm, ip,params) {
        println(' in  importOrg :'+params)
        def uid = request.getHeader("EPS-uid")
        Part filePart = request.getPart('file')
        InputStream fs = filePart.getInputStream()
        File file = File.createTempFile("s3test", "");
        OutputStream outputStream = new FileOutputStream(file)
        IOUtils.copy(fs, outputStream);
        def reader = new CSVReader(new FileReader(file))

        println('fs----'+ fs)

        String[] nextRecord;
        boolean header = true
        ArrayList failList = new ArrayList()
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            TenantClient tc=TenantClient.findById(params.tenant)
            println("TC::"+tc)
            Tenants.withId(tc.tenantid) {
                while ((nextRecord = reader.readNext()) != null) {
                    def tenant = TenantClient.findByName(nextRecord[2].trim())
                    String reason = 'Data is Missing '
                    HashMap temp = new HashMap()
                    if (header) //skip headers
                    {
                        header = false
                        continue
                    }
                    Tenants.withId(log.tenantclient.tenantid) {
//                while ((nextRecord = reader.readNext()) != null) {
//                    String reason = 'Data is Missing '
//                    HashMap temp = new HashMap()
//                    if (header) //skip headers
//                    {
//                        header = false
//                        continue
//                    }
                        println('7--------' + nextRecord[7])
                        if (nextRecord[0] != '' && nextRecord[1] != '' && nextRecord[2] != '' && nextRecord[4] != '' && nextRecord[7] != '') {
                            Organization org = Organization.findByName(nextRecord[0].trim())
                            println("org :" + org)
                            if (org == null) {
                                org = new Organization()

                                if (tenant) {
                                    org.tenantid = tenant?.id
                                }
                                org.tenantname = nextRecord[2].trim()
                                org.name = nextRecord[0].trim()
                                org.displayname = nextRecord[1].trim()
                                org.address = nextRecord[3].trim()
                                org.contact_person_name = nextRecord[4].trim()
                                org.isactive = true
                                org.email = nextRecord[5].trim()
                                org.contact = nextRecord[6].trim()
                                org.website = nextRecord[8].trim()
                                org.country = nextRecord[9].trim()
                                org.loginattachementdomain = nextRecord[7].trim()
                                org.logofilepath = 'NA'
                                org.logofilename = 'NA'
                                org.creation_username = uid
                                org.updation_username = uid
                                org.creation_date = new java.util.Date()
                                org.updation_date = new java.util.Date()
                                org.creation_ip_address = ip
                                org.updation_ip_address = ip
                                org.save(flush: true, failOnError: true)
                                hm.put("status", "200")
                            } else {
                                reason = "organization already present"
                                temp.put('reason', reason)
                                temp.put('name', nextRecord[0].trim())
                                temp.put('displayname', nextRecord[1].trim())
                                temp.put('tenantname', nextRecord[2].trim())
                                temp.put('domain', nextRecord[7].trim())
                                temp.put('cp_name', nextRecord[4].trim())
                                failList.add(temp)
                            }
                        } else {
                            if (nextRecord[0]) {
                                temp.put('name', nextRecord[0].trim())
                            } else {
                                temp.put('name', 'Missing')
                            }

                            if (nextRecord[1]) {
                                temp.put('displayname', nextRecord[1].trim())
                            } else {
                                temp.put('displayname', 'Missing')
                            }

                            if (nextRecord[2]) {
                                temp.put('tenantname', nextRecord[2].trim())
                            } else {
                                temp.put('tenantname', 'Missing')
                            }

                            if (nextRecord[4]) {
                                temp.put('cp_name', nextRecord[4].trim())
                            } else {
                                temp.put('cp_name', 'Missing')
                            }

                            if (nextRecord[7]) {
                                temp.put('domain', nextRecord[7].trim())
                            } else {
                                temp.put('domain', 'Missing')
                            }

                            temp.put('reason', reason)
                            failList.add(temp)
                        }
                    }
                }
            }
        }
        outputStream.flush()
        outputStream.close()
        fs.close()
        file.delete()
        hm.put('failList',failList)
        return hm
    }
    //Get Lib Data
    def getLibraryData(request, hm, ip) {
        println("in getLibraryData:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:--" + dataresponse
        AuthService auth = new AuthService()

        String purpose = "getLibraryData from admin  menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList lib_list = new ArrayList()
            if (dataresponse?.tenant!=null) {
                TenantClient tc = TenantClient.findById(dataresponse?.tenant)
                println("TC::" + tc)
                Tenants.withId(tc.tenantid) {
                    int srno = 1
                    for (item in Library.findAll()) {
                        HashMap temp = new HashMap()
                        temp.put("srno", srno++)
                        temp.put("id", item.id)
                        temp.put("name", item.name)
                        temp.put("city", item.city)
                        temp.put("state", item.state)
                        temp.put("pin", item.pin)
                        temp.put("address", item.address)
                        temp.put("contact", item?.contact)
                        temp.put("country", item?.country)
                        temp.put("isactive", item?.isactive)
                        lib_list.add(temp)
                    }
                }
            }
                def tclist=[]
                def tenantlist = TenantClient.list()
                for (ut in tenantlist) {
                    HashMap temp = new HashMap()
                    temp.put("name", ut?.name)
                    temp.put("tc_id", ut?.id)
                    tclist.add(temp)
                }
                hm.put('tclist', tclist)
                hm.put("lib_list", lib_list)

            return hm

        }
        return hm
    }
    //Save Edit Data
    def editLibrary(request, hm, ip) {
        println("in editLibrary:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:--" + dataresponse

        AuthService auth = new AuthService()

        String purpose = "edit Library from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            TenantClient tc=TenantClient.findById(dataresponse.selectedtenant.tc_id)
            println("TC::::"+tc)
            Tenants.withId(tc.tenantid) {
                //def lib = Library.findByName(dataresponse.name)
                def lib = Library.findById(dataresponse.id)
                if (lib != null) {
                    lib.city = dataresponse.city
                    lib.state = dataresponse.state
                    lib.pin = dataresponse.pin
                    lib.name = dataresponse.name
                    lib.address = dataresponse.address
                    lib.contact = dataresponse.contact
                    lib.country = dataresponse.country
                    lib.updation_username = uid
                    lib.updation_date = new java.util.Date()
                    lib.updation_ip_address = ip
                    lib.save(failOnError: true, flush: true)

                    HashMap status = new HashMap()
                    status.put("code", "SUCCESS")
                    hm.put("status", status)
                }
                return hm
            }
        }
        return hm
    }
    //save  lib data
    def saveLibrary(request, hm, ip) {
        println("in saveLibrary:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:" + dataresponse

        AuthService auth = new AuthService()

        String purpose = "saveLibrary from admin  menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        println("dataresponse")
        println(dataresponse)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            TenantClient tc=TenantClient.findById(dataresponse.selectedtenant.tc_id)
            println("TC::"+tc)
            Tenants.withId(tc.tenantid) {
                def lib = Library.findByName(dataresponse.name)
                if (lib == null) {
                    lib = new Library()

                    lib.city = dataresponse.city
                    lib.state = dataresponse.state
                    lib.pin = dataresponse.pin
                    lib.name = dataresponse.name
                    lib.address = dataresponse.address
                    lib.isactive = true
                    lib.organization = null
                    lib.contact = dataresponse.contact
                    lib.country = dataresponse.country
                    lib.creation_username = uid
                    lib.updation_username = uid
                    lib.creation_date = new java.util.Date()
                    lib.updation_date = new java.util.Date()
                    lib.creation_ip_address = ip
                    lib.updation_ip_address = ip
                    lib.save(failOnError: true, flush: true)

                    HashMap status = new HashMap()
                    status.put("code", "SUCCESS")
                    status.put("message", "Library added successfully...")
                    hm.put("status", status)
                    return hm
                } else {
                    HashMap status = new HashMap()
                    status.put("code", "NA")
                    status.put("message", "Library Already present!!!")
                    hm.put("status", status)
                }
            }
//            }
            return hm

        }
        return hm
    }
    //Active Lib Data
    def libraryIsactive(request, hm) {
        println "i am in orgIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "orgIsactive from Admin  Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            TenantClient tc=TenantClient.findById(dataresponse.tenant)
            println("TC:"+tc)
            Tenants.withId(tc.tenantid) {
                def user = User.findByUsername(log.username)
                def lib = Library.findById(dataresponse?.item?.id)
                if (lib != null) {
                    if (lib?.isactive == false) {
                        lib.isactive = true
                        lib.updation_ip_address = request.getRemoteAddr()
                        lib.updation_date = new Date()
                        lib.updation_username = uid
                        lib.save(flush: true, failOnError: true)
                    } else {
                        lib.isactive = false
                        lib.updation_ip_address = request.getRemoteAddr()
                        lib.updation_date = new Date()
                        lib.updation_username = uid
                        lib.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }
    //Import Lib Data
    def importLibrary(request, hm, ip,params) {
        println(' in  importLibrary :'+params)
        def uid = request.getHeader("EPS-uid")
        Part filePart = request.getPart('file')
        InputStream fs = filePart.getInputStream()
        File file = File.createTempFile("s3test", "");
        OutputStream outputStream = new FileOutputStream(file)
        IOUtils.copy(fs, outputStream);
        def reader = new CSVReader(new FileReader(file))

        println('fs----'+ fs)

        String[] nextRecord;
        boolean header = true
        ArrayList failList = new ArrayList()
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            TenantClient tc=TenantClient.findById(params.tenant)
            println("TC::"+tc)
            Tenants.withId(tc.tenantid) {
                while ((nextRecord = reader.readNext()) != null) {
                    String reason = 'Data is Missing'
                    HashMap temp = new HashMap()
                    if (header) //skip headers
                    {
                        header = false
                        continue
                    }
                    if (nextRecord[0] != '' && nextRecord[1] != '' && nextRecord[2] != '' && nextRecord[3] != '' && nextRecord[4] != '' && nextRecord[5] != '' && nextRecord[6] != '') {
                        Library lib = Library.findByName(nextRecord[0].trim())
                        println("lib :" + lib)
                        if (lib == null) {
                            lib = new Library()
                            lib.name = nextRecord[0].trim()
                            lib.address = nextRecord[1].trim()
                            lib.city = nextRecord[2].trim()
                            lib.state = nextRecord[3].trim()
                            lib.country = nextRecord[4].trim()
                            lib.pin = nextRecord[5].trim()
                            lib.contact = nextRecord[6].trim()
                            lib.isactive = true
                            lib.creation_username = uid
                            lib.updation_username = uid
                            lib.creation_date = new java.util.Date()
                            lib.updation_date = new java.util.Date()
                            lib.creation_ip_address = ip
                            lib.updation_ip_address = ip
                            lib.save(flush: true, failOnError: true)
                            hm.put("status", "200")
                        } else {
                            temp.put('name', nextRecord[0].trim())
                            temp.put('address', nextRecord[1].trim())
                            temp.put('city', nextRecord[2].trim())
                            temp.put('state', nextRecord[3].trim())
                            temp.put('country', nextRecord[4].trim())
                            temp.put('pin', nextRecord[5].trim())
                            temp.put('contact', nextRecord[6].trim())
                            temp.put('reason', 'Library already exist!!')

                            failList.add(temp)
                        }
                    } else {
                        if(nextRecord[0]){
                            temp.put('name', nextRecord[0].trim())
                        }else {
                            temp.put('name','Missing')
                        }

                        if(nextRecord[1]){
                            temp.put('address', nextRecord[1].trim())
                        }else {
                            temp.put('address','Missing')
                        }

                        if(nextRecord[2]){
                            temp.put('city', nextRecord[2].trim())
                        }else {
                            temp.put('city','Missing')
                        }

                        if(nextRecord[3]){
                            temp.put('state', nextRecord[3].trim())
                        }else {
                            temp.put('state','Missing')
                        }

                        if(nextRecord[4]){
                            temp.put('country', nextRecord[4].trim())
                        }else {
                            temp.put('country','Missing')
                        }

                        if(nextRecord[5]){
                            temp.put('pin', nextRecord[5].trim())
                        }else {
                            temp.put('pin','Missing')
                        }

                        if(nextRecord[6]){
                            temp.put('contact', nextRecord[6].trim())
                        }else {
                            temp.put('contact','Missing')
                        }
                        temp.put('reason', reason)
                        failList.add(temp)
                    }
                }
            }
        }
        outputStream.flush()
        outputStream.close()
        fs.close()
        file.delete()
        hm.put('failList',failList)
        return hm
    }

    //Push Master to Tenant

    def getPushTenantdata(request, hm) {
        println "i am in getPushTenantdata"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

      /*  String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse*/

        String purpose = "getPushTenantdata"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            def tc=TenantClient.list()
            println("TC:"+tc)
            def tc_list=[]
            for (t in tc){
                HashMap temp=new HashMap()
                temp.put("tc_id",t?.id)
                temp.put("tc_name",t?.name)
                tc_list.add(temp)
            }
            hm.put("tc_list", tc_list)
            hm.put("status", "200")
            println('hm---' + hm)
            return hm

        }
        return hm
    }
    //Import Lib Data
    //Copy Data Master to Tenant
    def copy(request, hm, ip)
    {
        println("I am in Admin Controller:copy")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        String body =  request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse::"+dataresponse)
        TenantClient tc=TenantClient.findById(dataresponse.tenant)
        println("TC ::"+tc)
        Tenants.withId(tc.tenantid) {
            SuperAdminPushMastersService superAdminPushMastersService=new SuperAdminPushMastersService()
            //Organization org=Organization.findByIdAndTenantid(dataresponse.org,tc.tenantid)
            Organization org=Organization.findByTenantid(tc.tenantid)
            String tenantdb=tc.tenantid
            println("org::"+org)
            println("tenantdb::"+tenantdb)
            hm.put("list1",superAdminPushMastersService.pushAllMasters(org,uid,request))
            //hm.put("list",superAdminPushMastersService.getPushMastersStatus(org))
            hm.put("message","Masters Pushed Successfully!")
            hm.put("msg","success")
            println("hm::"+hm)
            return hm

        }

    }
}
