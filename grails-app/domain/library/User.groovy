package library
import grails.gorm.MultiTenant
class User implements MultiTenant<User>
{

    String username    //Login username
    int total_issued_books
    String profilephotofilepath
    String profilephotofilename
    int number_of_books
    boolean isactive
    Date isactive_date
    String isactive_reson
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,usertype:UserType,accountstatus:AccountStatus]
    static hasMany =[role:Role]
    static constraints = {
        isactive_reson nullable:true
        isactive_date nullable:true
        organization nullable:true
        profilephotofilepath nullable:true
        profilephotofilename nullable:true
        accountstatus nullable:true
    }
    static mapping = {
        total_issued_books defaultValue: 0
        isactive  defaultValue: true
        number_of_books defaultValue: 0
    }
}
