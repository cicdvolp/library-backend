package library
import grails.gorm.MultiTenant
class LibraryPolicy implements MultiTenant<LibraryPolicy>
{
    int number_of_books   //Maximum number of books can be borrowed
    int number_of_days  // Maximum allowed days before returning
    double fine_rate_per_day  //After due date, fine rate per day
    int reservation_validity_days  //After book available after reservation, validity days
    int book_reservation_limit
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,role:Role]
    static mapping = {
        isactive defaultValue: true
        organization nullable: true
    }
}
