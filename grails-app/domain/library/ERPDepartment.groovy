package library
import grails.gorm.MultiTenant

class ERPDepartment implements MultiTenant<ERPDepartment>
{

    String name
    int display_order
    String short_name
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization]
    static constraints = {
        short_name nullable: true
    }
    static mapping = {
        display_order defaultValue: 0
    }
}
