<table>
<tr>
    <th>UID</th>
    <th>COL</th>
</tr>
<g:each in="${cols}" var="col">
    <tr>
        <td>${col?.learner?.uid}</td>
        <td>${col?.id}</td>
    </tr>
</g:each>
</table>