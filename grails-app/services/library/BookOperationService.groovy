package library

import grails.gorm.transactions.Transactional
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import grails.gorm.multitenancy.Tenants
import grails.converters.JSON
import groovy.json.JsonSlurper
import grails.gorm.transactions.Transactional
import com.opencsv.CSVReader
import javax.servlet.http.Part;
import grails.io.IOUtils

@Transactional
class BookOperationService {

    def serviceMethod() {

    }

    def getEmpMemInformation(request, hm) {
        println("in BookOperationService member information")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        def userid = dataresponse["userid"]

        String purpose = "fetch employee data"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def usr = User.findById(userid)
                LibraryPolicy libraryPolicy
                def maxCount = null
                if(usr.number_of_books>0)
                {
                    maxCount=usr.number_of_books
                }
                else {
                    if (usr.role.size() > 1) {
                        maxCount = null
                        def mantainId = null
                        for (item in usr.role) {
                            libraryPolicy = LibraryPolicy.findByRoleAndOrganization(item, usr.organization)
                            if (maxCount == null) {
                                maxCount = libraryPolicy?.number_of_books
                                mantainId = libraryPolicy?.id
                            } else {
                                if (maxCount < libraryPolicy?.number_of_books) {
                                    maxCount = libraryPolicy?.number_of_books
                                    mantainId = libraryPolicy?.id
                                }
                            }
                        }
                        libraryPolicy = LibraryPolicy.findById(mantainId)
                    } else {
                        libraryPolicy = LibraryPolicy.findByRoleAndOrganization(usr.role[0], usr.organization)
                    }
                }

                HashMap temp = new HashMap()
                temp.put("userid", usr?.id)
                temp.put("user", usr?.username)
                temp.put("usertype", usr?.usertype?.name)
                temp.put("date_of_membership", usr?.creation_date)
                def usrbooks = BookLending.findAllByUserAndIsactive(usr, true)
                temp.put("total_checked_out_book", usrbooks.size())
                Employee employee = Employee.findByUserAndOrganization(usr, user.organization)
                temp.put("empid", employee?.id)
                temp.put("employee_code", employee?.employee_code)
                temp.put("empname", employee?.name)
                Person person = Person.findByUser(usr)
                temp.put("mobile_number", person?.mobile_number)
                temp.put("email", person?.email)
                temp.put("address", person?.address)
                temp.put("city", person?.city)
                temp.put("state", person?.state)
                temp.put("country", person?.country)
                temp.put("pin", person?.pin)
                temp.put("max_number_book_allowed", maxCount)
                def booklendinginformation = getEmpMemBookLendingInformation(request, userid)
                def bookreservationinformation = bookReservationInfo(request, userid)
                hm.put("memberinfo", temp)
                hm.put("booklendinginformation", booklendinginformation)
                hm.put("bookreservationinformation", bookreservationinformation)

                def usrbooks1 = BookLending.findAllByUser(usr)
                println('usrbooks1==========' + usrbooks1)
                ArrayList fineRecord = new ArrayList()
                for (item in usrbooks1) {
                    HashMap finedata = new HashMap()
                    def fine_transaction = FineTransaction.findByBooklendingAndIsactive(item, true)
                    def fine_transaction_false = FineTransaction.findByBooklendingAndIsactive(item, false)
                    println('fine_transaction_false============' + fine_transaction_false)
                    if (fine_transaction) {
                        println('in ifffff')
                        def fine_transaction1 = FineTransaction.findByBooklendingAndIsactive(item, false)
                        finedata.put('title', item?.bookitem?.book?.title)
                        finedata.put('acc_no', item?.bookitem?.accession_number)
                        finedata.put('paid_amt', fine_transaction1?.amount)
                        finedata.put('unpaid_amt', fine_transaction?.amount)

                        fineRecord.add(finedata)
                    } else if (fine_transaction_false == null) {
                        println('in elseeeeeeeeeeeeeeee')
                        if (item?.fine_amount > 0) {
                            HashMap finedata1 = new HashMap()

                            finedata1.put('title', item?.bookitem?.book?.title)
                            finedata1.put('acc_no', item?.bookitem?.accession_number)
                            finedata1.put('paid_amt', '-')
                            finedata1.put('unpaid_amt', item?.fine_amount)
                            fineRecord.add(finedata1)

                        }
                    }


                }

                println hm
                hm.put("msg", "200")
                hm.put("fineRecord", fineRecord)
                return hm
            }
            return hm
        }
    }

    def getemployeehistory(request, hm) {
        println("in getemployeehistory ")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        def userid = dataresponse["userid"]

        String purpose = "fetch employee data"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def usr = User.findByUsernameAndOrganization(userid, user.organization)
                if (usr == null) {
                    hm.put("msg", "user Not Found")
                    return hm
                }

                Member me = Member.findByUser(usr)
                hm.put("user", usr?.username)
                if (me == null) {
                    Employee employee = Employee.findByUser(usr)
                    hm.put("name", employee?.name)
                } else {
                    hm.put("name", me.name)
                }
                def array = []
                def bookLending = BookLending.findAllByUserAndOrganization(usr, user.organization)
                if (bookLending.isEmpty()) {
                    hm.put("msg", "No History Found")
                    return hm
                }
                println("acsdsdfs employehistoryheader")
                println(bookLending)
                int x = 1;
                for (i in bookLending) {
                    HashMap temp = new HashMap()
                    temp.put("srno", x++)
                    temp.put("title", i?.bookitem?.book?.title)
                    temp.put("Assection_No", i?.bookitem?.accession_number)
                    temp.put("iscurrent", i?.isactive)
                    temp.put("borrowing_date", i?.borrowing_date)
                    temp.put("return_date", i?.return_date)
                    array.add(temp)
                }
                hm.put("data", array)
                hm.put("msg", "200")

                return hm
            }
            return hm
        }
    }

    def getemployeehistorybyprn(request, hm) {
        println("in BookOperationService member information")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        def userid = dataresponse["userid"]
        println(dataresponse)
        println("nfsj dfj sjd fj shjdfh shd fhs d jjjjjjjjjj")
        String purpose = "fetch employee data"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def member = null

                member = Member.findByRegistration_numberAndOrganization(userid, user.organization)

                if (member == null) {
                    member = Employee.findByEmployee_codeAndOrganization(userid, user.organization)

                }


                def usr = member?.user

                if (usr == null) {
                    hm.put("msg", "user Not Found")
                    return hm
                }

                Member me = Member.findByUser(usr)
                hm.put("user", usr?.username)
                if (me == null) {
                    Employee employee = Employee.findByUser(usr)
                    hm.put("name", employee?.name)
                } else {
                    hm.put("name", me.name)
                }
                def array = []
                def bookLending = BookLending.findAllByUserAndOrganization(usr, user.organization)
                if (bookLending.isEmpty()) {
                    hm.put("msg", "No History Found")
                    return hm
                }
                println("acsdsdfs employehistoryheader")
                println(bookLending)
                int x = 1;
                for (i in bookLending) {
                    HashMap temp = new HashMap()
                    temp.put("srno", x++)
                    temp.put("title", i?.bookitem?.book?.title)
                    temp.put("Assection_No", i?.bookitem?.accession_number)
                    temp.put("iscurrent", i?.isactive)
                    temp.put("borrowing_date", i?.borrowing_date)
                    temp.put("return_date", i?.return_date == null ? "" : i?.return_date)
                    array.add(temp)
                }
                hm.put("data", array)
                hm.put("msg", "200")

                return hm

            }
            return hm
        }
    }

    def getEmpMemInformationbyprn(request, hm) {
        println("in getEmpMemInformationbyprn service")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        def userid = dataresponse["userid"]
        println(dataresponse)
        println("nfsj dfj sjd fj shjdfh shd fhs d jjjjjjjjjj")
        String purpose = "fetch employee data"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def member = null
                member = Member.findByRegistration_numberAndOrganization(userid, user.organization)
                if (member == null) {
                    member = Employee.findByEmployee_codeAndOrganization(userid, user.organization)
                }
                def usr = member?.user
                LibraryPolicy libraryPolicy
                if (usr == null) {
                    hm.put("msg", "User Not Found...")
                    return hm
                }
                def maxCount = null
                println(usr.number_of_books)
                println(usr.role.size())
                if(usr.number_of_books>0)
                {
                    maxCount=usr.number_of_books
                }
                else {
                    if (usr.role.size() > 1) {
                         maxCount = null
                        def mantainId = null
                        for (item in usr.role) {
                            libraryPolicy = LibraryPolicy.findByRoleAndOrganization(item, usr.organization)
                            if (maxCount == null) {
                                maxCount = libraryPolicy?.number_of_books
                                mantainId = libraryPolicy?.id
                            } else {
                                if (maxCount < libraryPolicy?.number_of_books) {
                                    maxCount = libraryPolicy?.number_of_books
                                    mantainId = libraryPolicy?.id
                                }
                            }
                        }
                        libraryPolicy = LibraryPolicy.findById(mantainId)
                        maxCount=libraryPolicy?.number_of_books
                    } else {
                        libraryPolicy = LibraryPolicy.findByRoleAndOrganization(usr.role[0], usr.organization)
                        maxCount=libraryPolicy?.number_of_books
                    }
                }
                HashMap temp = new HashMap()
                Employee employee = Employee.findByUserAndOrganization(usr, user.organization)
                temp.put("userid", usr?.id)
                Member me = Member.findByUser(usr)
                temp.put("user", usr?.username)
                def rollno = ""
                def division = ""
                def year = ""
                def program = ""
                if (me == null)
                    temp.put("name", employee?.name)
                else {

                    temp.put("name", me.name)
                    AcademicYear academicYear = AcademicYear.findByIscurrent(true)
                    if (academicYear != null) {
                        LearnerDivision learnerDivision = LearnerDivision.findByMemberAndOrganizationAndAcademic(me, user.organization, academicYear)
                        if (learnerDivision != null) {
                            rollno = learnerDivision?.rollno
                            division = learnerDivision?.division
                            year = learnerDivision?.year
                            program = learnerDivision?.program.name
                            temp.put("ld", learnerDivision)
                        }
                    }

                }
                UserType utype = UserType.findByName('Member')
                println("utype : " + utype.id + "  member.user.usertype : " + member.user.usertype.id)
                if (member.user.usertype.id != utype.id) {
                    println("in if ")
                    temp.put("reg_no", member?.employee_code)
                    temp.put("isoutoforg",member?.user?.isactive)
                    println("isoutoforg emp ------------------------------"+employee?.isretired)
                } else {
                    println("in else ")
                    temp.put("isoutoforg", member?.user?.isactive)
                    temp.put("reg_no", member?.registration_number)
                    println("isoutoforg member ------------------------------"+member?.ispassout)
                }

                temp.put("rollno", rollno)
                temp.put("division", division)

                println("member " + member)
                def uertype = UserType.findByName('Member')
                println(" if wqual " + member?.user?.usertype + " " + utype)
                if (member?.user?.usertype.id == utype.id) {
                    // println("equal  "+member?.user?.usertype)
                    LearnerDivision ldv = LearnerDivision.findByMember(member)
                    temp.put("year", ldv?.year?.year)
                    temp.put("yid", ldv?.year?.id)
                    temp.put("date_of_membership", member?.date_of_membership)
                    temp.put("program", member?.program?.name)
                    temp.put("pid", member?.program?.id)
                } else {
                    temp.put("program", employee?.program?.name)
                    temp.put("pid", employee?.program?.id)

                }

                temp.put("usertype", usr?.usertype?.name)
                def usrbooks = BookLending.findAllByUserAndIsactiveAndOrganization(usr, true, user.organization)
                temp.put("total_checked_out_book", usrbooks.size())
                temp.put("empid", employee?.id)
                temp.put("employee_code", employee?.employee_code)
                temp.put("empname", employee?.name)
                temp.put("userid", usr?.id)
                Person person = Person.findByUser(usr)
                temp.put("mobile_number", person?.mobile_number)
                temp.put("email", person?.email)
                temp.put("address", person?.address)
                temp.put("city", person?.city)
                temp.put("state", person?.state)
                temp.put("country", person?.country)
                temp.put("pin", person?.pin)
                temp.put("max_number_book_allowed", maxCount)
                def booklendinginformation = getEmpMemBookLendingInformation1(request, usr.username)
                def bookreservationinformation = bookReservationInfo1(request, usr.username)
                hm.put("memberinfo", temp)
                hm.put("booklendinginformation", booklendinginformation)
                hm.put("bookreservationinformation", bookreservationinformation)

                def usrbooks1 = BookLending.findAllByUserAndOrganization(usr, usr.organization)
                println('usrbooks1==========' + usrbooks1)
                ArrayList fineRecord = new ArrayList()
                for (item in usrbooks1) {
                    HashMap finedata = new HashMap()
                    def fine_transaction = FineTransaction.findByBooklendingAndIsactiveAndOrganization(item, true, usr.organization)
                    def fine_transaction_false = FineTransaction.findByBooklendingAndIsactiveAndOrganization(item, false, usr.organization)
                    println('fine_transaction_false============' + fine_transaction_false)
                    if (fine_transaction) {
                        println('in ifffff')
                        def fine_transaction1 = FineTransaction.findByBooklendingAndIsactiveAndOrganization(item, false, usr.organization)
                        finedata.put('title', item?.bookitem?.book?.title)
                        finedata.put('acc_no', item?.bookitem?.accession_number)
                        finedata.put('paid_amt', fine_transaction1?.amount)
                        finedata.put('unpaid_amt', fine_transaction?.amount)

                        fineRecord.add(finedata)
                    } else if (fine_transaction_false == null) {
                        println('in elseeeeeeeeeeeeeeee')
                        if (item?.fine_amount > 0) {
                            HashMap finedata1 = new HashMap()

                            finedata1.put('title', item?.bookitem?.book?.title)
                            finedata1.put('acc_no', item?.bookitem?.accession_number)
                            finedata1.put('paid_amt', '-')
                            finedata1.put('unpaid_amt', item?.fine_amount)
                            fineRecord.add(finedata1)

                        }
                    }


                }


                hm.put("msg", "200")
                hm.put("fineRecord", fineRecord)
                return hm
            }
            return hm
        }
    }

    def getEmpMemInformation1(request, hm) {
        println("in getEmpMemInformation1")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        def userid = dataresponse["userid"]
        println(dataresponse)
        println("nfsj dfj sjd fj shjdfh shd fhs d jjjjjjjjjj")
        String purpose = "fetch employee data"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def usr = User.findByUsername(userid)
                LibraryPolicy libraryPolicy
                if (usr == null) {
                    hm.put("msg", "User Not Found...")

                    return hm
                }
                def maxCount = null
                if(usr.number_of_books>0)
                {
                    maxCount=usr.number_of_books
                }
                else {
                    if (usr.role.size() > 1) {
                        maxCount = null
                        def mantainId = null
                        for (item in usr.role) {
                            libraryPolicy = LibraryPolicy.findByRoleAndOrganization(item, usr.organization)
                            if (maxCount == null) {
                                maxCount = libraryPolicy?.number_of_books
                                mantainId = libraryPolicy?.id
                            } else {
                                if (maxCount < libraryPolicy?.number_of_books) {
                                    maxCount = libraryPolicy?.number_of_books
                                    mantainId = libraryPolicy?.id
                                }
                            }
                        }
                        libraryPolicy = LibraryPolicy.findById(mantainId)
                    } else {
                        libraryPolicy = LibraryPolicy.findByRoleAndOrganization(usr.role[0], usr.organization)
                    }
                }
                HashMap temp = new HashMap()
                Employee employee = Employee.findByUserAndOrganization(usr, user.organization)
                temp.put("userid", usr?.id)
                Member me = Member.findByUser(usr)
                temp.put("user", usr?.username)
                if (me == null)
                    temp.put("name", employee?.name)
                else
                    temp.put("name", me.name)
                temp.put("usertype", usr?.usertype?.name)
                temp.put("date_of_membership", usr?.creation_date)
                def usrbooks = BookLending.findAllByUserAndIsactiveAndOrganization(usr, true, user.organization)
                temp.put("total_checked_out_book", usrbooks.size())

                temp.put("empid", employee?.id)
                temp.put("employee_code", employee?.employee_code)
                temp.put("empname", employee?.name)
                Person person = Person.findByUser(usr)
                temp.put("mobile_number", person?.mobile_number)
                temp.put("email", person?.email)
                temp.put("address", person?.address)
                temp.put("city", person?.city)
                temp.put("state", person?.state)
                temp.put("country", person?.country)
                temp.put("pin", person?.pin)
                temp.put("max_number_book_allowed", maxCount)
                def booklendinginformation = getEmpMemBookLendingInformation1(request, userid)
                def bookreservationinformation = bookReservationInfo1(request, userid)
                hm.put("memberinfo", temp)
                hm.put("booklendinginformation", booklendinginformation)
                hm.put("bookreservationinformation", bookreservationinformation)

                def usrbooks1 = BookLending.findAllByUserAndOrganization(usr, usr.organization)
                println('usrbooks1==========' + usrbooks1)
                ArrayList fineRecord = new ArrayList()
                for (item in usrbooks1) {
                    HashMap finedata = new HashMap()
                    def fine_transaction = FineTransaction.findByBooklendingAndIsactiveAndOrganization(item, true, usr.organization)
                    def fine_transaction_false = FineTransaction.findByBooklendingAndIsactiveAndOrganization(item, false, usr.organization)
                    println('fine_transaction_false============' + fine_transaction_false)
                    if (fine_transaction) {
                        println('in ifffff')
                        def fine_transaction1 = FineTransaction.findByBooklendingAndIsactiveAndOrganization(item, false, usr.organization)
                        finedata.put('title', item?.bookitem?.book?.title)
                        finedata.put('acc_no', item?.bookitem?.accession_number)
                        finedata.put('paid_amt', fine_transaction1?.amount)
                        finedata.put('unpaid_amt', fine_transaction?.amount)

                        fineRecord.add(finedata)
                    } else if (fine_transaction_false == null) {
                        println('in elseeeeeeeeeeeeeeee')
                        if (item?.fine_amount > 0) {
                            HashMap finedata1 = new HashMap()

                            finedata1.put('title', item?.bookitem?.book?.title)
                            finedata1.put('acc_no', item?.bookitem?.accession_number)
                            finedata1.put('paid_amt', '-')
                            finedata1.put('unpaid_amt', item?.fine_amount)
                            fineRecord.add(finedata1)

                        }
                    }


                }


                hm.put("msg", "200")
                hm.put("fineRecord", fineRecord)
                return hm
            }
            return hm
        }
    }
    def getEmpMemInformationbyUser_ID(request, hm) {
        println("in getEmpMemInformationbyUser_ID")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        def userid = dataresponse["userid"]
        println(dataresponse)
        println("nfsj dfj sjd fj shjdfh shd fhs d jjjjjjjjjj")
        String purpose = "fetch employee data"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def usr = User.findById(userid)
                LibraryPolicy libraryPolicy
                if (usr == null) {
                    hm.put("msg", "User Not Found...")

                    return hm
                }
                def maxCount = null
                if(usr.number_of_books>0)
                {
                    maxCount=usr.number_of_books
                }
                else {
                    if (usr.role.size() > 1) {
                        maxCount = null
                        def mantainId = null
                        for (item in usr.role) {
                            libraryPolicy = LibraryPolicy.findByRoleAndOrganization(item, usr.organization)
                            if (maxCount == null) {
                                maxCount = libraryPolicy?.number_of_books
                                mantainId = libraryPolicy?.id
                            } else {
                                if (maxCount < libraryPolicy?.number_of_books) {
                                    maxCount = libraryPolicy?.number_of_books
                                    mantainId = libraryPolicy?.id
                                }
                            }
                        }
                        libraryPolicy = LibraryPolicy.findById(mantainId)
                    } else {
                        libraryPolicy = LibraryPolicy.findByRoleAndOrganization(usr.role[0], usr.organization)
                    }
                }
                HashMap temp = new HashMap()
                Employee employee = Employee.findByUserAndOrganization(usr, user.organization)
                temp.put("userid", usr?.id)
                Member me = Member.findByUser(usr)
                temp.put("user", usr?.username)

                if (me == null){
                    temp.put("name", employee?.name)
                    temp.put("uid_no", employee?.employee_code)
                }
                else
                {
                    temp.put("name", me.name)
                    temp.put("uid_no", me?.registration_number)
                }

                temp.put("usertype", usr?.usertype?.name)
                temp.put("date_of_membership", usr?.creation_date)
                def usrbooks = BookLending.findAllByUserAndIsactiveAndOrganization(usr, true, user.organization)
                temp.put("total_checked_out_book", usrbooks.size())



                temp.put("empid", employee?.id)

                temp.put("empname", employee?.name)
                Person person = Person.findByUser(usr)
                temp.put("mobile_number", person?.mobile_number)
                temp.put("email", person?.email)
                temp.put("address", person?.address)
                temp.put("city", person?.city)
                temp.put("state", person?.state)
                temp.put("country", person?.country)
                temp.put("pin", person?.pin)
                temp.put("max_number_book_allowed", maxCount)
                temp.put("isactive", usr?.isactive)

                hm.put("memberinfo", temp)



                hm.put("msg", "200")

                return hm
            }
            return hm
        }
    }
    def saveUserActiveOrNot(request, hm) {
        println("in saveUserActiveOrNot")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        def userid = dataresponse["userid"]
        println(dataresponse)
        println("nfsj dfj sjd fj shjdfh shd fhs d jjjjjjjjjj")
        String purpose = "fetch employee data"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def usr = User.findById(userid)
             if(usr==null) {
                 hm.put("msg", "User Not Found")
                 return hm
             }
                if(usr.isactive)
                {
                    usr.isactive_date=new Date()
                    usr.isactive_reson=dataresponse.reson
                    usr.isactive=dataresponse.isactive
                }else {
                    usr.isactive=dataresponse.isactive
                }
                usr.save(flush: true, failOnError: true)
                hm.put("msg","200")

            }

            return hm
        }
    }

    def getAllBookInformation(request, ip) {
        println("in all Book information")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String purpose = "fetch book data"
        HashMap hm = new HashMap()
        //auth.checkauth(token, uid, url, request, purpose)//401;200;token-refreshed ->msg
        def bookinfo = []
        //if (hm.get("msg") != "401" && hm.get("msg") != "403") {
        Login log = Login.findByUsername(uid)
        Tenants.withId(log.tenantclient.tenantid) {
            def user = User.findByUsername(log.username)
            //def usr = User.findById(userid)

            //sync book reservation validity
            def index_config = IndexConfig.list()
            def isReservationSink = index_config[0].isReservationSink
            if (!isReservationSink) {
                ReserveBookService reserveBookService = new ReserveBookService()
                //reserveBookService.sinkReservationDaysLimit(uid, ip,index_config[0])
                reserveBookService.syncReservationDaysLimit(uid, ip, index_config[0], user.organization)
            }

            def bookinformation = Book.findAllByOrganization(user?.organization)
            for (b in bookinformation) {
                def bookItem = BookItem.findAllByBookAndOrganization(b, user?.organization)
                for (bi in bookItem) {
                    if (bi.bookstatus.name != "Available")
                        continue
                    HashMap temp = new HashMap()
//                    temp.put("accession_number", bi?.accession_number)
                    temp.put('accession_number', bi?.accession_number)
                    temp.put('displayname', bi?.accession_number + ":" + bi?.book?.title)
                    temp.put("blfid", bi?.id)
                    temp.put("title", bi?.book?.title)

                    temp.put("bookcategory", b?.bookcategory?.name)
                    String authornames = ""
                    for (author in bi?.book?.author) {
                        authornames = author?.name + ","
                    }
                    temp.put("author", authornames)
                    temp.put("publisher", bi?.book?.publisher?.name)
                    temp.put("booktype", bi?.book?.booktype?.name)
                    temp.put("bookformat", bi?.bookformat?.name)
                    temp.put("bookprice", bi?.price)
                    temp.put("total_books", b?.total_books)
                    bookinfo.add(temp)
                }
            }
            hm.put("allbookinfo", bookinfo)
            return bookinfo
        }
        return bookinfo

        // }

    }

    def getAllBookInformation1(request, ip) {
        println("getAllBookInformation1")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dtrs" + dataresponse)
        String purpose = "fetch book data"
        HashMap hm = new HashMap()
        //auth.checkauth(token, uid, url, request, purpose)//401;200;token-refreshed ->msg
        def bookinfo = []
        //if (hm.get("msg") != "401" && hm.get("msg") != "403") {
        Login log = Login.findByUsername(uid)
        Tenants.withId(log.tenantclient.tenantid) {
            def user = User.findByUsername(log.username)
            //def usr = User.findById(userid)


            def bookitem
            BookStatus Bs = BookStatus.findByName("Available")
            if (dataresponse.seaechbyacc == 'title') {
                bookitem = Book.createCriteria().list {
                    'in'("organization", user?.organization)
                    and {

                        'like'("title", dataresponse.text + "%")
                    }
                }
            } else if (dataresponse.seaechbyacc) {
                bookitem = BookItem.createCriteria().list {
                    'in'("organization", user?.organization)
                    and {
                        'in'("bookstatus", Bs)
                        'like'("accession_number", dataresponse.text + "%")
                    }
                }

            } else {
                def book = Book.createCriteria().list {
                    'in'("organization", user?.organization)
                    and {
                        'like'("title", dataresponse.text + "%")
                    }
                }

                if (book) {
                    bookitem = BookItem.createCriteria().list {
                        'in'("organization", user?.organization)
                        and {
                            'in'("bookstatus", Bs)
                            'in'("book", book)
                        }
                    }

                }
            }

            if (dataresponse.seaechbyacc == 'title') {
                for (bi in bookitem) {
                    HashMap temp = new HashMap()
                    temp.put('displayname', bi?.title)
                    temp.put("blfid", bi?.id)
                    bookinfo.add(temp)
                }
            } else {
                for (bi in bookitem) {
                    HashMap temp = new HashMap()
//                    temp.put("accession_number", bi?.accession_number)
                    temp.put('accession_number', bi?.accession_number)
                    temp.put('displayname', bi?.accession_number + ":" + bi?.barocde + ":" + bi?.book?.title)
                    temp.put("blfid", bi?.id)
                    bookinfo.add(temp)
                }
            }


        }
        return bookinfo

        // }

    }

    def getAllBookInformation1HOME(request, ip) {

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println(dataresponse)
        String purpose = "fetch book data"
        HashMap hm = new HashMap()
        //auth.checkauth(token, uid, url, request, purpose)//401;200;token-refreshed ->msg
        def bookinfo = []
        //if (hm.get("msg") != "401" && hm.get("msg") != "403") {
        def tenantid = dataresponse["tenantid"]
        TenantClient tenantclient = TenantClient.findById(tenantid)
        Tenants.withId(tenantclient.tenantid) {
            Organization org = Organization.findById(dataresponse.organizationid)
            Series series
            if (dataresponse.series != 'ALL')
                series = Series.findById(dataresponse.series)

            def bookitem
            BookStatus Bs = BookStatus.findByName("Available")
            if (dataresponse.seaechbyacc == 'title') {
                bookitem = Book.createCriteria().list {
                    'in'("organization", org)
                    and {
                        if (dataresponse.series != 'ALL')
                            'in'("series", series)
                        'like'("title", dataresponse.text + "%")
                    }
                }
            } else if (dataresponse.seaechbyacc) {
                bookitem = BookItem.createCriteria().list {
                    'in'("organization", org)
                    and {
                        if (dataresponse.series != 'ALL')
                            'in'("series", series)
                        'in'("bookstatus", Bs)
                        'like'("accession_number", dataresponse.text + "%")
                    }
                }

            } else {
                def book = Book.createCriteria().list {
                    'in'("organization", org)
                    and {
                        'like'("title", dataresponse.text + "%")
                    }
                }

                if (book) {
                    bookitem = BookItem.createCriteria().list {
                        'in'("organization", org)
                        and {
                            'in'("bookstatus", Bs)
                            'in'("book", book)
                        }
                    }

                }
            }

            if (dataresponse.seaechbyacc == 'title') {
                for (bi in bookitem) {
                    HashMap temp = new HashMap()
                    temp.put('displayname', bi?.title)
                    temp.put("blfid", bi?.id)
                    bookinfo.add(temp)
                }
            } else {
                for (bi in bookitem) {
                    HashMap temp = new HashMap()
//                    temp.put("accession_number", bi?.accession_number)
                    temp.put('accession_number', bi?.accession_number)
                    temp.put('displayname', bi?.accession_number + ":" + bi?.barocde + ":" + bi?.book?.title)
                    temp.put("blfid", bi?.id)
                    bookinfo.add(temp)
                }
            }


            return bookinfo

        }

    }

    def getauthor_new(request, ip) {

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println(dataresponse)
        String purpose = "fetch book data"
        HashMap hm = new HashMap()
        //auth.checkauth(token, uid, url, request, purpose)//401;200;token-refreshed ->msg
        def bookinfo = []
        //if (hm.get("msg") != "401" && hm.get("msg") != "403") {
        Login log = Login.findByUsername(uid)
        Tenants.withId(log.tenantclient.tenantid) {
            def user = User.findByUsername(log.username)
            //def usr = User.findById(userid)
            def author
            def author1

            if (dataresponse.seaechbyacc == 'author') {
                author = Author.createCriteria().list {
                    and {
                        'like'("name", dataresponse.text + "%")
                    }
                }
                if(author){
                author1 = BooksAuthors.createCriteria().list {
                    projections{
                        distinct('author')
                    }
                    and{
                        'in'("author", author)
                        'in'("organization", user.organization)
                    }
                }
                }
            }

//                for(bi in bookitem)
//                {
//                    HashMap temp = new HashMap()
////                    temp.put("accession_number", bi?.accession_number)
//                    temp.put('accession_number',  bi?.accession_number)
//                    temp.put('displayname',  bi?.accession_number+":"+bi?.book?.title)
//                    temp.put("blfid", bi?.id)
//                    bookinfo.add(temp)
//                }
            return author1

        }

    }

    def getkeyword(request, hm) {
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println(dataresponse)
        String purpose = "fetch book data"

        //auth.checkauth(token, uid, url, request, purpose)//401;200;token-refreshed ->msg
        def bookinfo = []
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def usr = User.findById(userid)
                def keywords




                keywords = BookItem.createCriteria().list {
                        projections{
                            distinct('keyword')
                        }
                        and {
                            'in'("organization", user?.organization)
                            'like'("keyword", dataresponse.text + "%")
                        }

                    }

                for (key in keywords) {
                    println key
                    HashMap temp = new HashMap()

                    temp.put('displayname', key)

                    bookinfo.add(temp)
                }
                hm.put("allBookInfo", bookinfo)
                hm.put("msg", "200")
            }

            return hm
        }
    }
    def getkeywordHOME(request, hm) {
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println(dataresponse)
        String purpose = "fetch book data"

        //auth.checkauth(token, uid, url, request, purpose)//401;200;token-refreshed ->msg

        def bookinfo = []
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            def tenantid = dataresponse["tenantid"]
            TenantClient tenantclient = TenantClient.findById(tenantid)
            Tenants.withId(tenantclient.tenantid) {
                Organization org = Organization.findById(dataresponse.organizationid)
                //def usr = User.findById(userid)
                def keywords




                keywords = BookItem.createCriteria().list {
                        projections{
                            distinct('keyword')
                        }
                        and {
                            'in'("organization", org)
                            'like'("keyword", dataresponse.text + "%")
                        }

                    }

                for (key in keywords) {
                    println key
                    HashMap temp = new HashMap()

                    temp.put('displayname', key)

                    bookinfo.add(temp)
                }
                hm.put("allBookInfo", bookinfo)
                hm.put("msg", "200")
            }

            return hm
        }
    }

    def getsubject(request, ip) {
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println(dataresponse)
        String purpose = "fetch book data"
        HashMap hm = new HashMap()
        //auth.checkauth(token, uid, url, request, purpose)//401;200;token-refreshed ->msg
        def bookinfo = []
        //if (hm.get("msg") != "401" && hm.get("msg") != "403") {
        Login log = Login.findByUsername(uid)
        Tenants.withId(log.tenantclient.tenantid) {
            def user = User.findByUsername(log.username)
            //def usr = User.findById(userid)
            def bookitem
            bookitem = BookItem.createCriteria().list {
                'in'("organization", user?.organization)
                and {
                    'like'("subject", dataresponse.text + "%")
                   }
              }
                for (bi in bookitem) {
                    HashMap temp = new HashMap()
                    temp.put('accession_number', bi?.accession_number)
                    temp.put('displayname', bi?.subject)
                    temp.put("blfid", bi?.id)
                    bookinfo.add(temp)
                }
            }
            return bookinfo
        }
    def getsubjecthome(request, ip) {
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println(dataresponse)
        String purpose = "fetch book data"
        HashMap hm = new HashMap()
        //auth.checkauth(token, uid, url, request, purpose)//401;200;token-refreshed ->msg
        def bookinfo = []
        //if (hm.get("msg") != "401" && hm.get("msg") != "403") {
//        Login log = Login.findByUsername(uid)
        def tenantid = dataresponse["tenantid"]
        TenantClient tenantclient = TenantClient.findById(tenantid)
        Tenants.withId(tenantclient.tenantid) {
            Organization org = Organization.findById(dataresponse.organizationid)
//            def user = User.findByUsername(log.username)
            //def usr = User.findById(userid)
            def bookitem
            bookitem = BookItem.createCriteria().list {
                'in'("organization", org)
                and {
                    'like'("subject", dataresponse.text + "%")
                }
            }
            for (bi in bookitem) {
                HashMap temp = new HashMap()
                temp.put('accession_number', bi?.accession_number)
                temp.put('displayname', bi?.subject)
                temp.put("blfid", bi?.id)
                bookinfo.add(temp)
            }
        }
        return bookinfo
    }
        def getauthor_newHOME(request, ip) {

            def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
            def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
            String url = request.getHeader("router-path")
            AuthService auth = new AuthService()
            String body = request.getReader().text
            def jsonSluper = new JsonSlurper()
            def dataresponse = jsonSluper.parseText(body)
            println(dataresponse)
            String purpose = "fetch book data"
            HashMap hm = new HashMap()
            //auth.checkauth(token, uid, url, request, purpose)//401;200;token-refreshed ->msg
            def bookinfo = []
            //if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            def tenantid = dataresponse["tenantid"]
            TenantClient tenantclient = TenantClient.findById(tenantid)
            Tenants.withId(tenantclient.tenantid) {
                Organization org = Organization.findById(dataresponse.organizationid)
                def author
                def author1

                if (dataresponse.seaechbyacc == 'author') {
                    author = Author.createCriteria().list {
                        and {
                            'like'("name", dataresponse.text + "%")
                        }
                    }
                    if(author){
                        author1 = BooksAuthors.createCriteria().list {
                            projections{
                                distinct('author')
                            }
                            and{
                                'in'("author", author)
                                'in'("organization", org)
                            }
                        }
                    }
                }

                return author1

            }}


        def getAllBookInformation2(request, ip) {
            print("getAllBookInformation2 ")
            def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
            def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
            String url = request.getHeader("router-path")
            AuthService auth = new AuthService()
            String body = request.getReader().text
            def jsonSluper = new JsonSlurper()
            def dataresponse = jsonSluper.parseText(body)
            println(dataresponse)
            String purpose = "fetch book data"
            HashMap hm = new HashMap()
            //auth.checkauth(token, uid, url, request, purpose)//401;200;token-refreshed ->msg
            def bookinfo = []
            //if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def usr = User.findById(userid)


                def bookitem
                Series series
                if (dataresponse.series_id != 'ALL')
                    series = Series.findById(dataresponse.series_id)
                else
                    series = Series.findAllByOrganization(dataresponse.series_id, user.organization)
                def book = Book.createCriteria().list {
                    'in'("organization", user?.organization)
                    and {
                        'like'("title", dataresponse.text + "%")
                    }
                }

                if (book) {
                    bookitem = BookItem.createCriteria().list {
                        'in'("organization", user?.organization)
                        and {
                            'in'("series", series)
                            'in'("book", book)
                        }
                    }

                }

                for (bi in bookitem) {
                    HashMap temp = new HashMap()
//                    temp.put("accession_number", bi?.accession_number)
                    temp.put('accession_number', bi?.accession_number)
                    temp.put('displayname', bi?.accession_number + ":" + bi?.book?.title)
                    temp.put("blfid", bi?.id)
                    bookinfo.add(temp)
                }

            }
            return bookinfo

            // }

        }
        def dateToString(Date date) {
            if (!date) {
                return 'NA'
            }
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            String strDate = formatter.format(date);
            println strDate
            return strDate
        }

        def getEmpMemBookLendingInformation1(request, userid) {
            println("in Book lending member information")
            def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
            def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
            String url = request.getHeader("router-path")
            AuthService auth = new AuthService()

            String purpose = "fetch employee data"
            HashMap hm = new HashMap()
            //auth.checkauth(token, uid, url, request, purpose)//401;200;token-refreshed ->msg
            def bookinfo = []
            //if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def usr = User.findByUsername(userid)
                def booklendinginformation = BookLending.findAllByUserAndIsactiveAndOrganization(usr, true, usr.organization)
                int count = 1
                for (blf in booklendinginformation) {
                    HashMap temp = new HashMap()
                    temp.put("srno", count++)
                    temp.put("blfid", blf?.id)
                    temp.put("accession_number", blf?.bookitem?.accession_number)
                    temp.put("title", blf?.bookitem?.book?.title)
                    String authornames = ""
                    for (author in blf?.bookitem?.book?.author) {
                        println "author.name" + author.name
                        authornames = authornames + author.name + ","
                    }
                    temp.put("author", authornames)
                    temp.put("publisher", blf?.bookitem?.book?.publisher?.name)
                    temp.put("booktype", blf?.bookitem?.book?.booktype?.name)
                    temp.put("bookformat", blf?.bookitem?.bookformat?.name)
                    temp.put("borrowed_date", blf?.borrowing_date)
                    temp.put("return_date", dateToString(blf?.return_date))
                    //temp.put("empname", employee?.name)
                    temp.put("due_date", dateToString(blf?.due_date))
                    SimpleDateFormat sdformat = new SimpleDateFormat("dd-MM-yyyy");
                    println(blf?.due_date)
                    if (blf?.due_date != null) {
                        Date d1 = sdformat.parse(dateToString(blf?.due_date));
                        Date d2 = sdformat.parse(dateToString(new Date()));
//                    System.out.println("The date 1 is: " + sdformat.format(d1));
//                    System.out.println("The date 2 is: " + sdformat.format(d2));
//                    if(d1.compareTo(d2) > 0) {
//                        System.out.println("Date 1 occurs after Date 2");
//                    } else if(d1.compareTo(d2) < 0) {
//                        System.out.println("Date 1 occurs before Date 2");
//                    } else if(d1.compareTo(d2) == 0) {
//                        System.out.println("Both dates are equal");
//                    }
                        if (d1.compareTo(d2) > 0) {
                            temp.put("fine", "NA")
                        } else {
                            LibraryPolicy libraryPolicy
                            if (usr.role.size() >= 1) {
                                libraryPolicy = LibraryPolicy.findByRoleAndOrganization(usr.role[0], usr.organization)
                            }
                            if (libraryPolicy?.fine_rate_per_day) {
                                long difference_In_Time = d2.getTime() - d1.getTime()
                                long difference_In_Days = (difference_In_Time / (1000 * 60 * 60 * 24))
                                long fine = libraryPolicy.fine_rate_per_day * difference_In_Days
                                blf.fine_amount = fine
                                blf.save(flush: true, failOnError: true)
                                temp.put("fine", fine)
                            } else
                                temp.put("fine", "Fine rate not set/ either it is zero...")

                        }

                    }
                    bookinfo.add(temp)

                }
                hm.put("booklendinginfo", bookinfo)
                return bookinfo
            }
            return bookinfo
        }
        def getEmpMemBookLendingInformation(request, userid) {
            println("in Book lending member information")
            def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
            def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
            String url = request.getHeader("router-path")
            AuthService auth = new AuthService()

            String purpose = "fetch employee data"
            HashMap hm = new HashMap()
            //auth.checkauth(token, uid, url, request, purpose)//401;200;token-refreshed ->msg
            def bookinfo = []
            //if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def usr = User.findById(userid)
                def booklendinginformation = BookLending.findAllByUserAndIsactiveAndOrganization(usr, true, user.organization)
                int count = 1
                for (blf in booklendinginformation) {
                    HashMap temp = new HashMap()
                    temp.put("srno", count++)
                    temp.put("blfid", blf?.id)
                    temp.put("accession_number", blf?.bookitem?.accession_number)
                    temp.put("title", blf?.bookitem?.book?.title)
                    String authornames = ""
                    for (author in blf?.bookitem?.book?.author) {
                        println "author.name" + author.name
                        authornames = authornames + author.name + ","
                    }
                    temp.put("author", authornames)
                    temp.put("publisher", blf?.bookitem?.book?.publisher?.name)
                    temp.put("booktype", blf?.bookitem?.book?.booktype?.name)
                    temp.put("bookformat", blf?.bookitem?.bookformat?.name)
                    temp.put("borrowed_date", blf?.borrowing_date)
                    temp.put("return_date", dateToString(blf?.return_date))
                    //temp.put("empname", employee?.name)
                    temp.put("due_date", blf?.due_date)
                    SimpleDateFormat sdformat = new SimpleDateFormat("dd-MM-yyyy");



                    Date d1 = sdformat.parse(dateToString(blf?.due_date))
                    Date d2 = sdformat.parse(dateToString(new Date()))
//                    System.out.println("The date 1 is: " + sdformat.format(d1));
//                    System.out.println("The date 2 is: " + sdformat.format(d2));
//                    if(d1.compareTo(d2) > 0) {
//                        System.out.println("Date 1 occurs after Date 2");
//                    } else if(d1.compareTo(d2) < 0) {
//                        System.out.println("Date 1 occurs before Date 2");
//                    } else if(d1.compareTo(d2) == 0) {
//                        System.out.println("Both dates are equal");
//                    }
                    if (d1.compareTo(d2) > 0) {
                        temp.put("fine", "NA")
                    } else {
                        LibraryPolicy libraryPolicy
                        if (usr.role.size() >= 1) {
                            libraryPolicy = LibraryPolicy.findByRoleAndOrganization(usr.role[0], usr.organization)
                        }
                        if (libraryPolicy?.fine_rate_per_day) {
                            long difference_In_Time = d2.getTime() - d1.getTime()
                            long difference_In_Days = (difference_In_Time / (1000 * 60 * 60 * 24))
                            long fine = libraryPolicy.fine_rate_per_day * difference_In_Days
                            blf.fine_amount = fine
                            blf.save(flush: true, failOnError: true)
                            temp.put("fine", fine)
                        } else
                            temp.put("fine", "Fine rate not set/ either it is zero...")

                    }
                    bookinfo.add(temp)
                }
                hm.put("booklendinginfo", bookinfo)
                return bookinfo
            }
            return bookinfo
        }

        def getBookItmeInformationbyaccno(request, hm) {
            println("in getBookItmeInformationbyaccno")
            def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
            def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
            String url = request.getHeader("router-path")
            AuthService auth = new AuthService()

            String body = request.getReader().text
            def jsonSluper = new JsonSlurper()
            def dataresponse = jsonSluper.parseText(body)
            println "data response ------>" + dataresponse
            def bookitemid = dataresponse["blfid"]

            String purpose = "fetch book data"
            auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

            if (hm.get("msg") != "401" && hm.get("msg") != "403") {
                Login log = Login.findByUsername(uid)
                Tenants.withId(log.tenantclient.tenantid) {
                    def user = User.findByUsername(log.username)

                    def bi = BookItem.findByBarocdeAndOrganization(bookitemid, user.organization)
                    if (bi == null) {
                        hm.put("msg", "Book Not Found")
                        return hm
                    }
                    BookLending bookLending2 = BookLending.findByBookitemAndOrganizationAndIsactive(bi, user.organization, true)
                    if (bookLending2 != null) {
                        def me = Member.findByUserAndOrganization(bookLending2.user, user.organization)
                        if (me == null)
                            me = Employee.findByUserAndOrganization(bookLending2.user, user.organization)
                        hm.put("msg", "Book Already Issue TO " + me.name)
                        return hm
                    }

                    def allBook = BookItem.findAllByBook(bi?.book)
                    HashMap temp = new HashMap()
                    temp.put("accession_number", bi?.accession_number)
                    def is_img = false
                    if (bi?.filename && bi?.filepath)
                        is_img = true
                    temp.put('is_img', is_img)
                    temp.put("blfid", bi?.id)
                    temp.put("title", bi?.book?.title)

                    temp.put("bookcategory", bi?.book?.bookcategory?.name)
                    String authornames = ""
                    def booksAuthors=BooksAuthors.findAllByOrganizationAndBook(user.organization,bi?.book)?.author
                    for (author in booksAuthors) {
                        authornames += author.name + "\n"
                    }
                    temp.put("author", authornames)
                    temp.put("publisher", bi?.book?.publisher?.name)
                    temp.put("booktype", bi?.book?.booktype?.name)
                    temp.put("bookformat", bi?.bookformat?.name)
                    temp.put("bookprice", bi?.price)
                    //temp.put("total_books", bi?.book?.total_books)
                    temp.put("total_books", allBook.findAll { it?.bookstatus?.name == "Available" }.size())
                    BookLending bookLending = BookLending.findByBookitem(bi)
                    temp.put("member", user?.username)
                    def bookhistory = []
                    def bookLending1 = BookLending.findAllByBookitem(bi)
                    bookLending1 = bookLending1.sort({ it.return_date })
                    for (i in bookLending1) {
                        HashMap hashMap = new HashMap()
                        hashMap.put("username", i.user.username)
                        hashMap.put("return_date", i.return_date)
                        Employee employee = Employee.findByUser(i?.user)
                        if (employee == null) {
                            Member member = Member.findByUser(i?.user)
                            hashMap.put("name", member?.name)
                            hashMap.put("grno_empid", member?.registration_number)
                        } else {
                            hashMap.put("name", employee?.name)
                            hashMap.put("grno_empid", employee?.employee_code)
                        }
                        bookhistory.add(hashMap)


                    }
                    hm.put("bookhistory", bookhistory)
                    hm.put("bookinformation", temp)
                    hm.put("bfid", bi.id)
                    println hm
                    hm.put("msg", "200")
                    return hm
                }
                return hm
            }
        }

        def getBookItmeInformation(request, hm) {
            println("in BookOperationService member information")
            def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
            def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
            String url = request.getHeader("router-path")
            AuthService auth = new AuthService()

            String body = request.getReader().text
            def jsonSluper = new JsonSlurper()
            def dataresponse = jsonSluper.parseText(body)
            println "data response ------>" + dataresponse
            def bookitemid = dataresponse["blfid"]

            String purpose = "fetch book data"
            auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

            if (hm.get("msg") != "401" && hm.get("msg") != "403") {
                Login log = Login.findByUsername(uid)
                Tenants.withId(log.tenantclient.tenantid) {
                    def user = User.findByUsername(log.username)

                    def bi = BookItem.findById(bookitemid)
                    if (bi == null) {
                        hm.put("msg", "Book Not Found")
                        return hm
                    }


                    def allBook = BookItem.findAllByBook(bi?.book)
                    HashMap temp = new HashMap()
                    temp.put("accession_number", bi?.accession_number)
                    temp.put("blfid", bi?.id)
                    temp.put("title", bi?.book?.title)

                    temp.put("bookcategory", bi?.book?.bookcategory?.name)
                    String authornames = ""
                    for (author in bi?.book?.author) {
                        authornames += author.name + "\n"
                    }
                    temp.put("author", authornames)
                    temp.put("publisher", bi?.book?.publisher?.name)
                    temp.put("booktype", bi?.book?.booktype?.name)
                    temp.put("bookformat", bi?.bookformat?.name)
                    temp.put("bookprice", bi?.price)
                    //temp.put("total_books", bi?.book?.total_books)
                    temp.put("total_books", allBook.findAll { it.bookstatus.name == "Available" }.size())
                    BookLending bookLending = BookLending.findByBookitem(bi)
                    temp.put("member", user?.username)
                    def bookhistory = []
                    def bookLending1 = BookLending.findAllByBookitem(bi)
                    bookLending1 = bookLending1.sort({ it.return_date })
                    for (i in bookLending1) {
                        HashMap hashMap = new HashMap()
                        hashMap.put("username", i.user.username)
                        hashMap.put("return_date", i.return_date)
                        Employee employee = Employee.findByUser(i?.user)
                        if (employee == null) {
                            Member member = Member.findByUser(i?.user)
                            hashMap.put("name", member?.name)
                            hashMap.put("grno_empid", member?.registration_number)
                        } else {
                            hashMap.put("name", employee?.name)
                            hashMap.put("grno_empid", employee?.employee_code)
                        }
                        bookhistory.add(hashMap)


                    }
                    hm.put("bookhistory", bookhistory)
                    hm.put("bookinformation", temp)
                    println hm
                    hm.put("msg", "200")
                    return hm
                }
                return hm
            }
        }

        def memberIssueBook(request, hm, ip) {
            println("in BookOperationService memberIssueBook")
            def token = request.getHeader("EPS-token")
            def uid = request.getHeader("EPS-uid")
            String url = request.getHeader("router-path")
            AuthService auth = new AuthService()

            String body = request.getReader().text
            def jsonSluper = new JsonSlurper()
            def dataresponse = jsonSluper.parseText(body)
            def bookitemid = dataresponse["bookitemid"]
            def userid = dataresponse["userid"]
            println(dataresponse)
            println(dataresponse.backdate)
            println("bookitemid  " + bookitemid)
            String purpose = "fetch book data"
            auth.checkauth(token, uid, hm, url, request, purpose)

            if (hm.get("msg") != "401" && hm.get("msg") != "403") {
                Login log = Login.findByUsername(uid)
                Tenants.withId(log.tenantclient.tenantid) {
                    def user = User.findByUsername(log.username)
                    def usr = User.findByUsername(userid)
                    println("usr : "+usr)
                    if (usr == null)
                        usr = User.findById(userid)

                    if(!usr?.isactive)
                    {
                        hm.put("msg", "Member not Active To Issue Book Activate User First.")
                        return hm
                    }
                    Member m = Member.findByUser(usr)
                    println(" m ------"+m)
                    if (m == null) {
                        Employee e = Employee.findByUser(usr)
                        println(" e ------"+e)
                        if (e.isretired == true) {
                            hm.put("msg", "Employee does not belongs to Organization anymore")
                            return hm
                        } else {
                            println("in else")
                            def bi = BookItem.findById(bookitemid)
                            BookLending bookLending = BookLending.findByBookitemAndUserAndIsactiveAndOrganization(bi, usr, true, user.organization)
                            LibraryPolicy libraryPolicy
                            if (usr.role.size() >= 1) {
                                libraryPolicy = LibraryPolicy.findByRoleAndOrganization(usr.role[0], usr.organization)
                            }
                            if (libraryPolicy) {
                                Calendar c = Calendar.getInstance();
                                if (bookLending == null) {
                                    bookLending = new BookLending()
                                    if (dataresponse.backdate) {
                                        println("backdate present ")
                                        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
                                        Date date1 = formatter1.parse(dataresponse?.backdate);
                                        Calendar c1 = Calendar.getInstance();
                                        c1.setTime(date1)
                                        c1.add(Calendar.DATE, 1);
                                        println("date1 : " + c1.getTime())
                                        bookLending.borrowing_date = c1.getTime()

                                        c.setTime(c1.getTime());
                                        println("c.getTime().  " + c.getTime())
                                        c.add(Calendar.DATE, libraryPolicy.number_of_days);
                                        bookLending.due_date = c.getTime()
                                    } else {
                                        println("backdate not present ")
                                        bookLending.borrowing_date = new Date()
                                        c.setTime(new Date());
                                        c.add(Calendar.DATE, libraryPolicy.number_of_days);
                                        bookLending.due_date = c.getTime()
                                    }
                                    // bookLending.return_date = c.getTime()
                                    bookLending.fine_amount = 0.0
                                    bookLending.isactive = true
                                    bookLending.creation_username = uid
                                    bookLending.updation_username = uid
                                    bookLending.creation_date = new Date()
                                    bookLending.updation_date = new Date()
                                    bookLending.creation_ip_address = ip
                                    bookLending.updation_ip_address = ip
                                    bookLending.bookitem = bi
                                    bookLending.user = usr
                                    bookLending.organization = usr?.organization

                                    BookStatus bookStatus = BookStatus.findByName("Issued")
                                    bi.bookstatus = bookStatus
                                    bi.borrowed_date = new Date()
                                    bi.due_date = c.getTime()
                                    bi.updation_username = uid
                                    bi.updation_date = new Date()
                                    bi.updation_ip_address = ip
                                    bookLending.save(flush: true, failOnError: true)
                                    bi.save(flush: true, failOnError: true)

                                    println("bookLending.due_date  : " + bookLending.due_date)

                                    ReservationStatus reservationstatus = ReservationStatus.findByName('Completed')
                                    BookReservation bookReservation = BookReservation.findByBookAndUserAndIsactive(bi?.book, usr, true)
                                    if (bookReservation != null) {
                                        bookReservation.isactive = false
                                        bookReservation.reservationstatus = reservationstatus
                                        bookReservation.updation_username = uid
                                        bookReservation.updation_date = new Date()
                                        bookReservation.updation_ip_address = ip
                                        bookReservation.save(flush: true, failOnError: true)

                                        BookStatus reserved = BookStatus.findByName('Reserved')
                                        BookStatus available = BookStatus.findByName('Available')
                                        def b_item = BookItem.findByBookAndBookstatus(bookReservation?.book, reserved)
                                        if (b_item) {
                                            if (b_item?.bookstatus != bookStatus)
                                                b_item?.bookstatus = available
                                            b_item.updation_username = uid
                                            b_item.updation_date = new Date()
                                            b_item.updation_ip_address = ip
                                            b_item.save(flush: true, failOnError: true)
                                        }
                                    }

                                    // maintain userbooklog renew count set to 1 if member issue book
                                    UserBookLog userBookLog = UserBookLog.findByUserAndBookitem(usr, bi)
                                    if (!userBookLog) {
                                        userBookLog = new UserBookLog()

                                        userBookLog.renew_counter = 1
                                        userBookLog.creation_username = uid
                                        userBookLog.updation_username = uid
                                        userBookLog.creation_date = new Date()
                                        userBookLog.updation_date = new Date()
                                        userBookLog.creation_ip_address = ip
                                        userBookLog.updation_ip_address = ip
                                        userBookLog.user = usr
                                        userBookLog.bookitem = bi
                                        userBookLog.save(flush: true, failOnError: true)
                                    } else {
                                        userBookLog.renew_counter = 1
                                        userBookLog.updation_username = uid
                                        userBookLog.user = usr
                                        userBookLog.updation_date = new Date()
                                        userBookLog.updation_ip_address = ip
                                        userBookLog.save(flush: true, failOnError: true)
                                    }
                                } else {
                                    if (bookLending?.isactive) {
                                        hm.put("message", "Already book issued")
                                    } else {
                                        BookStatus bookStatus = BookStatus.findByName("Issued")
                                        bi.bookstatus = bookStatus
                                        bi.borrowed_date = new Date()
                                        bi.due_date = c.getTime()

                                        bi.updation_username = uid
                                        bi.updation_date = new Date()
                                        bi.updation_ip_address = ip

                                        bookLending.updation_username = uid
                                        bookLending.updation_date = new Date()
                                        bookLending.updation_ip_address = ip
                                        bookLending.isactive = true
                                        bookLending.due_date = c.getTime()
                                        bookLending.save(flush: true, failOnError: true)
                                        bi.save(flush: true, failOnError: true)
                                    }
                                }

                            } else {
                                hm.put("policy", "Library policy is not defined !!")
                            }
                            HashMap temp = new HashMap()
                            def booklendinginformation = getEmpMemBookLendingInformation(request, usr.id)
                            hm.put("booklendinginformation", booklendinginformation)
//                def allBookInfo = getAllBookInformation(request, ip)
//                hm.put("allBookInfo", allBookInfo)
                            println "member issue : " + hm
                            hm.put("msg", "200")
                            return hm
                        }
                    } else if (m != null) {
                        if (m.ispassout == true) {
                            hm.put("msg", "Student does not belongs to Organization anymore")
                            return hm
                        }else {
                            println("in else")
                            def bi = BookItem.findById(bookitemid)
                            BookLending bookLending = BookLending.findByBookitemAndUserAndIsactiveAndOrganization(bi, usr, true, user.organization)
                            LibraryPolicy libraryPolicy
                            if (usr.role.size() >= 1) {
                                libraryPolicy = LibraryPolicy.findByRoleAndOrganization(usr.role[0], usr.organization)
                            }
                            if (libraryPolicy) {
                                Calendar c = Calendar.getInstance();
                                if (bookLending == null) {
                                    bookLending = new BookLending()
                                    if (dataresponse.backdate) {
                                        println("backdate present ")
                                        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
                                        Date date1 = formatter1.parse(dataresponse?.backdate);
                                        Calendar c1 = Calendar.getInstance();
                                        c1.setTime(date1)
                                        c1.add(Calendar.DATE, 1);
                                        println("date1 : " + c1.getTime())
                                        bookLending.borrowing_date = c1.getTime()

                                        c.setTime(c1.getTime());
                                        println("c.getTime().  " + c.getTime())
                                        c.add(Calendar.DATE, libraryPolicy.number_of_days);
                                        bookLending.due_date = c.getTime()
                                    } else {
                                        println("backdate not present ")
                                        bookLending.borrowing_date = new Date()
                                        c.setTime(new Date());
                                        c.add(Calendar.DATE, libraryPolicy.number_of_days);
                                        bookLending.due_date = c.getTime()
                                    }
                                    // bookLending.return_date = c.getTime()
                                    bookLending.fine_amount = 0.0
                                    bookLending.isactive = true
                                    bookLending.creation_username = uid
                                    bookLending.updation_username = uid
                                    bookLending.creation_date = new Date()
                                    bookLending.updation_date = new Date()
                                    bookLending.creation_ip_address = ip
                                    bookLending.updation_ip_address = ip
                                    bookLending.bookitem = bi
                                    bookLending.user = usr
                                    bookLending.organization = usr?.organization

                                    BookStatus bookStatus = BookStatus.findByName("Issued")
                                    bi.bookstatus = bookStatus
                                    bi.borrowed_date = new Date()
                                    bi.due_date = c.getTime()
                                    bi.updation_username = uid
                                    bi.updation_date = new Date()
                                    bi.updation_ip_address = ip
                                    bookLending.save(flush: true, failOnError: true)
                                    bi.save(flush: true, failOnError: true)

                                    println("bookLending.due_date  : " + bookLending.due_date)

                                    ReservationStatus reservationstatus = ReservationStatus.findByName('Completed')
                                    BookReservation bookReservation = BookReservation.findByBookAndUserAndIsactive(bi?.book, usr, true)
                                    if (bookReservation != null) {
                                        bookReservation.isactive = false
                                        bookReservation.reservationstatus = reservationstatus
                                        bookReservation.updation_username = uid
                                        bookReservation.updation_date = new Date()
                                        bookReservation.updation_ip_address = ip
                                        bookReservation.save(flush: true, failOnError: true)

                                        BookStatus reserved = BookStatus.findByName('Reserved')
                                        BookStatus available = BookStatus.findByName('Available')
                                        def b_item = BookItem.findByBookAndBookstatus(bookReservation?.book, reserved)
                                        if (b_item) {
                                            if (b_item?.bookstatus != bookStatus)
                                                b_item?.bookstatus = available
                                            b_item.updation_username = uid
                                            b_item.updation_date = new Date()
                                            b_item.updation_ip_address = ip
                                            b_item.save(flush: true, failOnError: true)
                                        }
                                    }

                                    // maintain userbooklog renew count set to 1 if member issue book
                                    UserBookLog userBookLog = UserBookLog.findByUserAndBookitem(usr, bi)
                                    if (!userBookLog) {
                                        userBookLog = new UserBookLog()

                                        userBookLog.renew_counter = 1
                                        userBookLog.creation_username = uid
                                        userBookLog.updation_username = uid
                                        userBookLog.creation_date = new Date()
                                        userBookLog.updation_date = new Date()
                                        userBookLog.creation_ip_address = ip
                                        userBookLog.updation_ip_address = ip
                                        userBookLog.user = usr
                                        userBookLog.bookitem = bi
                                        userBookLog.save(flush: true, failOnError: true)
                                    } else {
                                        userBookLog.renew_counter = 1
                                        userBookLog.updation_username = uid
                                        userBookLog.user = usr
                                        userBookLog.updation_date = new Date()
                                        userBookLog.updation_ip_address = ip
                                        userBookLog.save(flush: true, failOnError: true)
                                    }
                                } else {
                                    if (bookLending?.isactive) {
                                        hm.put("message", "Already book issued")
                                    } else {
                                        BookStatus bookStatus = BookStatus.findByName("Issued")
                                        bi.bookstatus = bookStatus
                                        bi.borrowed_date = new Date()
                                        bi.due_date = c.getTime()

                                        bi.updation_username = uid
                                        bi.updation_date = new Date()
                                        bi.updation_ip_address = ip

                                        bookLending.updation_username = uid
                                        bookLending.updation_date = new Date()
                                        bookLending.updation_ip_address = ip
                                        bookLending.isactive = true
                                        bookLending.due_date = c.getTime()
                                        bookLending.save(flush: true, failOnError: true)
                                        bi.save(flush: true, failOnError: true)
                                    }
                                }

                            } else {
                                hm.put("policy", "Library policy is not defined !!")
                            }
                            HashMap temp = new HashMap()
                            def booklendinginformation = getEmpMemBookLendingInformation(request, usr.id)
                            hm.put("booklendinginformation", booklendinginformation)
//                def allBookInfo = getAllBookInformation(request, ip)
//                hm.put("allBookInfo", allBookInfo)
                            println "member issue : " + hm
                            hm.put("msg", "200")
                            return hm
                        }
                    }
//                    else {
//                        println("in else")
//                        def bi = BookItem.findById(bookitemid)
//                        BookLending bookLending = BookLending.findByBookitemAndUserAndIsactiveAndOrganization(bi, usr, true, user.organization)
//                        LibraryPolicy libraryPolicy
//                        if (usr.role.size() >= 1) {
//                            libraryPolicy = LibraryPolicy.findByRoleAndOrganization(usr.role[0], usr.organization)
//                        }
//                        if (libraryPolicy) {
//                            Calendar c = Calendar.getInstance();
//                            if (bookLending == null) {
//                                bookLending = new BookLending()
//                                if (dataresponse.backdate) {
//                                    println("backdate present ")
//                                    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
//                                    Date date1 = formatter1.parse(dataresponse?.backdate);
//                                    Calendar c1 = Calendar.getInstance();
//                                    c1.setTime(date1)
//                                    c1.add(Calendar.DATE, 1);
//                                    println("date1 : " + c1.getTime())
//                                    bookLending.borrowing_date = c1.getTime()
//
//                                    c.setTime(c1.getTime());
//                                    println("c.getTime().  " + c.getTime())
//                                    c.add(Calendar.DATE, libraryPolicy.number_of_days);
//                                    bookLending.due_date = c.getTime()
//                                } else {
//                                    println("backdate not present ")
//                                    bookLending.borrowing_date = new Date()
//                                    c.setTime(new Date());
//                                    c.add(Calendar.DATE, libraryPolicy.number_of_days);
//                                    bookLending.due_date = c.getTime()
//                                }
//                                // bookLending.return_date = c.getTime()
//                                bookLending.fine_amount = 0.0
//                                bookLending.isactive = true
//                                bookLending.creation_username = uid
//                                bookLending.updation_username = uid
//                                bookLending.creation_date = new Date()
//                                bookLending.updation_date = new Date()
//                                bookLending.creation_ip_address = ip
//                                bookLending.updation_ip_address = ip
//                                bookLending.bookitem = bi
//                                bookLending.user = usr
//                                bookLending.organization = usr?.organization
//
//                                BookStatus bookStatus = BookStatus.findByName("Issued")
//                                bi.bookstatus = bookStatus
//                                bi.borrowed_date = new Date()
//                                bi.due_date = c.getTime()
//                                bi.updation_username = uid
//                                bi.updation_date = new Date()
//                                bi.updation_ip_address = ip
//                                bookLending.save(flush: true, failOnError: true)
//                                bi.save(flush: true, failOnError: true)
//
//                                println("bookLending.due_date  : " + bookLending.due_date)
//
//                                ReservationStatus reservationstatus = ReservationStatus.findByName('Completed')
//                                BookReservation bookReservation = BookReservation.findByBookAndUserAndIsactive(bi?.book, usr, true)
//                                if (bookReservation != null) {
//                                    bookReservation.isactive = false
//                                    bookReservation.reservationstatus = reservationstatus
//                                    bookReservation.updation_username = uid
//                                    bookReservation.updation_date = new Date()
//                                    bookReservation.updation_ip_address = ip
//                                    bookReservation.save(flush: true, failOnError: true)
//
//                                    BookStatus reserved = BookStatus.findByName('Reserved')
//                                    BookStatus available = BookStatus.findByName('Available')
//                                    def b_item = BookItem.findByBookAndBookstatus(bookReservation?.book, reserved)
//                                    if (b_item) {
//                                        if (b_item?.bookstatus != bookStatus)
//                                            b_item?.bookstatus = available
//                                        b_item.updation_username = uid
//                                        b_item.updation_date = new Date()
//                                        b_item.updation_ip_address = ip
//                                        b_item.save(flush: true, failOnError: true)
//                                    }
//                                }
//
//                                // maintain userbooklog renew count set to 1 if member issue book
//                                UserBookLog userBookLog = UserBookLog.findByUserAndBookitem(usr, bi)
//                                if (!userBookLog) {
//                                    userBookLog = new UserBookLog()
//
//                                    userBookLog.renew_counter = 1
//                                    userBookLog.creation_username = uid
//                                    userBookLog.updation_username = uid
//                                    userBookLog.creation_date = new Date()
//                                    userBookLog.updation_date = new Date()
//                                    userBookLog.creation_ip_address = ip
//                                    userBookLog.updation_ip_address = ip
//                                    userBookLog.user = usr
//                                    userBookLog.bookitem = bi
//                                    userBookLog.save(flush: true, failOnError: true)
//                                } else {
//                                    userBookLog.renew_counter = 1
//                                    userBookLog.updation_username = uid
//                                    userBookLog.user = usr
//                                    userBookLog.updation_date = new Date()
//                                    userBookLog.updation_ip_address = ip
//                                    userBookLog.save(flush: true, failOnError: true)
//                                }
//                            } else {
//                                if (bookLending?.isactive) {
//                                    hm.put("message", "Already book issued")
//                                } else {
//                                    BookStatus bookStatus = BookStatus.findByName("Issued")
//                                    bi.bookstatus = bookStatus
//                                    bi.borrowed_date = new Date()
//                                    bi.due_date = c.getTime()
//
//                                    bi.updation_username = uid
//                                    bi.updation_date = new Date()
//                                    bi.updation_ip_address = ip
//
//                                    bookLending.updation_username = uid
//                                    bookLending.updation_date = new Date()
//                                    bookLending.updation_ip_address = ip
//                                    bookLending.isactive = true
//                                    bookLending.due_date = c.getTime()
//                                    bookLending.save(flush: true, failOnError: true)
//                                    bi.save(flush: true, failOnError: true)
//                                }
//                            }
//
//                        } else {
//                            hm.put("policy", "Library policy is not defined !!")
//                        }
//                        HashMap temp = new HashMap()
//                        def booklendinginformation = getEmpMemBookLendingInformation(request, usr.id)
//                        hm.put("booklendinginformation", booklendinginformation)
////                def allBookInfo = getAllBookInformation(request, ip)
////                hm.put("allBookInfo", allBookInfo)
//                        println "member issue : " + hm
//                        hm.put("msg", "200")
//                        return hm
//                    }
                }
                return hm
            }
        }

        def renewBook_old(request, hm, ip) {
            println("in BookOperationService memberIssueBook")
            def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
            def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
            String url = request.getHeader("router-path")
            AuthService auth = new AuthService()

            String body = request.getReader().text
            def jsonSluper = new JsonSlurper()
            def dataresponse = jsonSluper.parseText(body)
            println "data response ------>" + dataresponse
            def accession = dataresponse["a_no"]

            //def userid =  dataresponse["userid"]

            String purpose = "renew book data"
            auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

            if (hm.get("msg") != "401" && hm.get("msg") != "403") {
                Login log = Login.findByUsername(uid)
                Tenants.withId(log.tenantclient.tenantid) {
                    def user = User.findByUsername(log.username)
                    BookItem bi = BookItem.findByAccession_number(accession)
                    BookLending bookLending = BookLending.findByBookitemAndIsactive(bi, true)
                    println "booklending information " + bookLending
                    //
                    LibraryPolicy libraryPolicy
                    if (bookLending.user.role.size() >= 1) {
                        libraryPolicy = LibraryPolicy.findByRoleAndOrganization(bookLending?.user?.role[0], bookLending?.user.organization)
                    }
                    // println libraryPolicy.number_of_days
                    if (libraryPolicy) {
                        Calendar c = Calendar.getInstance();
                        c.setTime(new Date()); // Now use today date.
                        c.add(Calendar.DATE, libraryPolicy.number_of_days); // Adding 5 days
                        if (bookLending) {
                            println "new book"
                            bookLending.borrowing_date = new Date()
                            bookLending.due_date = c.getTime()
                            //bookLending.return_date = c.getTime()
                            //bookLending.fine_amount = 0.0
                            bookLending.isactive = true
                            bookLending.updation_username = uid
                            bookLending.updation_date = new Date()
                            bookLending.updation_ip_address = ip
                            bookLending.bookitem = bi
                            BookStatus bookStatus = BookStatus.findByName("Issued")
                            bi.bookstatus = bookStatus
                            bi.borrowed_date = new Date()
                            bi.due_date = c.getTime()
                            bi.updation_username = uid
                            bi.updation_date = new Date()
                            bi.updation_ip_address = ip
                            bookLending.save(flush: true, failOnError: true)
                            bi.save(flush: true, failOnError: true)
                        } else {
                            //println 'already book issued'
//                        if(bookLending?.isactive){
//                            hm.put("message","Already book issued")
//                        }else
//                        {
//                            BookStatus bookStatus = BookStatus.findByName("Issued")
//                            bi.bookstatus = bookStatus
//                            bi.borrowed_date = new Date()
//                            bi.due_date = c.getTime()
//
//                            bi.updation_username = uid
//                            bi.updation_date = new Date()
//                            bi.updation_ip_address = ip
//
//                            bookLending.updation_username = uid
//                            bookLending.updation_date = new Date()
//                            bookLending.updation_ip_address = ip
//                            bookLending.isactive = true
//                            bookLending.due_date = c.getTime()
//                            bookLending.return_date = c.getTime()
//                            bookLending.save(flush: true, failOnError: true)
//                            bi.save(flush: true, failOnError: true)
//                        }
                            hm.put("message", "book not available")
                        }
                    } else {
                        hm.put("policy", "Library policy is not defined !!")
                    }
                    HashMap temp = new HashMap()
//                def booklendinginformation = getEmpMemBookLendingInformation(request , userid)
//                hm.put("booklendinginformation",booklendinginformation)
//                def allBookInfo = getAllBookInformation(request)
//                hm.put("allBookInfo",allBookInfo)
                    println "renew book issue : " + hm
                    hm.put("msg", "200")
                    return hm
                }
                return hm
            }
        }

        def renewBook(request, hm, ip) {
            println("in BookOperationService memberIssueBook")
            def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
            def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
            String url = request.getHeader("router-path")
            AuthService auth = new AuthService()

            String body = request.getReader().text
            def jsonSluper = new JsonSlurper()
            def dataresponse = jsonSluper.parseText(body)
            println "data response ------>" + dataresponse
            def accession = dataresponse["a_no"]
            //def userid =  dataresponse["userid"]
            String purpose = "renew book data"
            auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
            if (hm.get("msg") != "401" && hm.get("msg") != "403") {
                Login log = Login.findByUsername(uid)
                Tenants.withId(log.tenantclient.tenantid) {
                    def user = User.findByUsername(log.username)
                    BookItem bi = BookItem.findByAccession_number(accession)
                    BookLending bookLending = BookLending.findByBookitemAndIsactive(bi, true)
                    println "booklending information " + bookLending
                    LibraryPolicy libraryPolicy
                    UserBookLog userBookLog = UserBookLog.findByUserAndBookitem(bookLending?.user, bi)
                    if (userBookLog) {
                        userBookLog.renew_counter = userBookLog.renew_counter + 1
                        userBookLog.updation_username = log.username
                        userBookLog.updation_date = new java.util.Date()
                        userBookLog.updation_ip_address = request.getRemoteAddr()
                        userBookLog.save(flush: true, failOnError: true)
                    }
                    if (bookLending.user.role.size() >= 1) {
                        libraryPolicy = LibraryPolicy.findByRoleAndOrganization(bookLending?.user?.role[0], bookLending?.user?.organization)
                    }
                    if (libraryPolicy) {
                        Calendar c = Calendar.getInstance();
                        // Adding 5 days
                        if (bookLending != null) {
                            bookLending.isactive = true
                            //bookLending.return_date = new java.util.Date()
                            println("bookLending.due_date : " + bookLending.due_date)
                            c.setTime(bookLending.due_date); // Now use today date.
                            c.add(Calendar.DATE, libraryPolicy.number_of_days);
                            bookLending.due_date = c.getTime()
                            bookLending.updation_username = log.username
                            bookLending.updation_date = new java.util.Date()
                            bookLending.updation_ip_address = request.getRemoteAddr()
                            bookLending.save(flush: true, failOnError: true)
                        }
//                    BookLending bookLending1 = new BookLending()
//                    println "new book"
//                    bookLending1.borrowing_date = new Date()
//                    bookLending1.due_date = c.getTime()
//                    bookLending1.isactive = true
//                    bookLending1.bookitem = bi
//                    bookLending1.user = user
//                    bookLending1.organization = user?.organization
//                    bookLending1.fine_amount = 0.0
//                    bookLending1.creation_username = log.username
//                    bookLending1.updation_username = log.username
//                    bookLending1.creation_date = new java.util.Date()
//                    bookLending1.updation_date = new java.util.Date()
//                    bookLending1.creation_ip_address = request.getRemoteAddr()
//                    bookLending1.updation_ip_address = request.getRemoteAddr()
//                    bookLending1.save(flush: true, failOnError: true)
                    } else {
                        hm.put("policy", "Library policy is not defined !!")
                    }
                    println "renew book issue : " + hm
                    hm.put("msg", "200")
                    return hm
                }
                return hm
            }
        }

        def bookReservationInfo1(request, userid) {
            int srno = 1
            ArrayList books = new ArrayList()
            HashMap hm = new HashMap()
            println("userid : " + userid)
            User member = User.findByUsername(userid)
            println "member" + member
            Role role = Role.findByName('Member')
            LibraryPolicy policy = LibraryPolicy.findByRoleAndOrganization(role, member.organization)
            def reservation_daysLimit = policy?.reservation_validity_days
            println('reservation_daysLimit------' + reservation_daysLimit)

            def status = ReservationStatus.findByName('Pending')
            def reserveList = BookReservation.findAllByUserAndIsactiveAndReservationstatus(member, true, status)
//        def reserveList = BookReservation.findAllByUserAndIsactive(member,true)
            println "reserveList -----" + reserveList
            def book_reservation_limit = policy?.book_reservation_limit
            for (reserve_book in reserveList) {
                HashMap temp1 = new HashMap()

                def book = Book.findById(reserve_book?.book?.id)

                temp1.put('srno', srno++)
                temp1.put('id', book?.id)
                temp1.put('title', book?.title)
                temp1.put('isbn', book?.isbn)
                temp1.put('edition', book?.edition)
                temp1.put('bookType', book?.booktype?.name)
                temp1.put('category', book?.bookcategory?.name)
                temp1.put('publisher', book?.publisher?.name)
                temp1.put('copies', book?.total_books)
                temp1.put('isReserve', true)
//            BookStatus bookStatus = BookStatus.findByName('Available')
//            def bookItems = BookItem.findAllByBookAndBookstatus(book, bookStatus)
//            temp1.put('available', bookItems.size())
//            def authorArr = book?.author
//            ArrayList arr = new ArrayList()
//            for (author in authorArr) {
//                def authorName = Author.findById(author?.id)
//                arr.add(authorName?.name)
//            }

//            temp1.put('authors',arr)
                books.add(temp1)
            }
//        if(book_reservation_limit > books.size()){
//            hm.put('isReservation_limit_exeed',false)
//        }else {
//            hm.put('isReservation_limit_exeed',true)
//        }
            return books

        }
        def bookReservationInfo(request, userid) {
            int srno = 1
            ArrayList books = new ArrayList()
            HashMap hm = new HashMap()
            User member = User.findById(userid)
            println "member" + member
            Role role = Role.findByName('Member')
            LibraryPolicy policy = LibraryPolicy.findByRoleAndOrganization(role, user.organization)
            def reservation_daysLimit = policy?.reservation_validity_days
            println('reservation_daysLimit------' + reservation_daysLimit)

            def status = ReservationStatus.findByName('Pending')
            def reserveList = BookReservation.findAllByUserAndIsactiveAndReservationstatus(member, true, status)
//        def reserveList = BookReservation.findAllByUserAndIsactive(member,true)
            println "reserveList----" + reserveList
            def book_reservation_limit = policy?.book_reservation_limit
            for (reserve_book in reserveList) {
                HashMap temp1 = new HashMap()

                def book = Book.findById(reserve_book?.book?.id)

                temp1.put('srno', srno++)
                temp1.put('id', book?.id)
                temp1.put('title', book?.title)
                temp1.put('isbn', book?.isbn)
                temp1.put('edition', book?.edition)
                temp1.put('bookType', book?.booktype?.name)
                temp1.put('category', book?.bookcategory?.name)
                temp1.put('publisher', book?.publisher?.name)
                temp1.put('copies', book?.total_books)
                temp1.put('isReserve', true)
//            BookStatus bookStatus = BookStatus.findByName('Available')
//            def bookItems = BookItem.findAllByBookAndBookstatus(book, bookStatus)
//            temp1.put('available', bookItems.size())
//            def authorArr = book?.author
//            ArrayList arr = new ArrayList()
//            for (author in authorArr) {
//                def authorName = Author.findById(author?.id)
//                arr.add(authorName?.name)
//            }

//            temp1.put('authors',arr)
                books.add(temp1)
            }
//        if(book_reservation_limit > books.size()){
//            hm.put('isReservation_limit_exeed',false)
//        }else {
//            hm.put('isReservation_limit_exeed',true)
//        }
            return books

        }

        def getReserveBookA_no(request, hm, ip) {
            println('get book item information')
            def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
            def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
            String url = request.getHeader("router-path")
            AuthService auth = new AuthService()

            String body = request.getReader().text
            def jsonSluper = new JsonSlurper()
            def dataresponse = jsonSluper.parseText(body)
            println "data response ------>" + dataresponse
            String purpose = "fetch book data"
            auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

            if (hm.get("msg") != "401" && hm.get("msg") != "403") {
                Login log = Login.findByUsername(uid)
                Tenants.withId(log.tenantclient.tenantid) {
                    def user = User.findByUsername(log.username)

                    Book book = Book.findById(dataresponse.bookId)
                    BookStatus status = BookStatus.findByName('Available')
                    BookStatus reserve = BookStatus.findByName('Reserved')

//                def bookItems = BookItem.findAllByBookAndBookstatus(book,status)
                    def c = BookItem.createCriteria()
                    def bookItems = c.list {
                        like("book", book)
                        or {
                            eq("bookstatus", status)
                            eq("bookstatus", reserve)
                        }

                    }
                    ArrayList acc_noList = new ArrayList()
                    for (item in bookItems) {
                        HashMap temp = new HashMap()
                        temp.put('accession_number', item?.accession_number)
                        temp.put('displayname', item?.accession_number + ":" + item?.book?.title)
                        temp.put("blfid", item?.id)
//                    temp.put("blfid", item?.id)
//                    temp.put('a_no', item?.accession_number)
//                    temp.put('bookName', '[ ' + item?.accession_number + ' ]' + ' : ' + item?.book?.title)
//                    temp.put('accession_number', '[ ' + item?.accession_number + ' ]' + ' : ' + item?.book?.title)
                        acc_noList.add(temp)
                    }
                    hm.put("acc_noList", acc_noList)
                    hm.put("msg", "200")
                    println("hm.acc_noList : " + hm.acc_noList)
                    return hm
                }

            }
        }

        def getBookA_no(request, hm, ip) {
            println('get book item information')
            def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
            def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
            String url = request.getHeader("router-path")
            AuthService auth = new AuthService()

            String body = request.getReader().text
            def jsonSluper = new JsonSlurper()
            def dataresponse = jsonSluper.parseText(body)
            println "data response ------>" + dataresponse
            String purpose = "fetch book data"
            auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

            if (hm.get("msg") != "401" && hm.get("msg") != "403") {
                Login log = Login.findByUsername(uid)
                Tenants.withId(log.tenantclient.tenantid) {
                    def user = User.findByUsername(log.username)

//                Book book = Book.findById(dataresponse.bookId)
                    def book = Book.findAll()
                    BookStatus status = BookStatus.findByName('Available')
                    ArrayList acc_noList = new ArrayList()
                    for (single_book in book) {
                        println('book------' + book)
                        println('status------' + status)
                        def bookItems = BookItem.findAllByBookAndBookstatus(single_book, status)
                        for (item in bookItems) {
                            HashMap temp = new HashMap()
                            temp.put("blfid", item?.id)
                            temp.put('a_no', item?.accession_number)
                            temp.put('bookName', '[ ' + item?.accession_number + ' ]' + ' : ' + item?.book?.title)
                            temp.put('accession_number', '[ ' + item?.accession_number + ' ]' + ' : ' + item?.book?.title)
                            acc_noList.add(temp)
                        }
                    }
                    hm.put("acc_noList", acc_noList)
                    hm.put("msg", "200")
                    return hm
                }
                return hm
            }
        }
        //Chart
        def getBookData(request, hm) {
            println("in getBookData")
            def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
            def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
            String url = request.getHeader("router-path")
            AuthService auth = new AuthService()

            String body = request.getReader().text
            def jsonSluper = new JsonSlurper()
            //def dataresponse = jsonSluper.parseText(body)

            //def userid =  dataresponse["userid"]

            String purpose = "fetch employee data"
            auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

            if (hm.get("msg") != "401" && hm.get("msg") != "403") {
                Login log = Login.findByUsername(uid)
                Tenants.withId(log.tenantclient.tenantid) {
                    def user = User.findByUsername(log.username)
                    //def usr = User.findById(userid)
                    BookStatus bookStatAvb = BookStatus.findByName('Available')
                    BookStatus bookStatusLoan = BookStatus.findByName("Issued")
                    BookStatus bookStatusLost = BookStatus.findByName("Lost")
                    BookStatus bookStatusReserved = BookStatus.findByName("Reserved")
                    def data = []
                    def allavbBooks = BookItem.findAllByOrganizationAndBookstatus(user?.organization, bookStatAvb)
                    data.add(allavbBooks.size())
                    def allreservedBooks = BookItem.findAllByOrganizationAndBookstatus(user?.organization, bookStatusReserved)
                    data.add(allreservedBooks.size())
                    def allloanBooks = BookItem.findAllByOrganizationAndBookstatus(user?.organization, bookStatusLoan)
                    data.add(allloanBooks.size())
                    println("allloanBooks : " + allloanBooks.size())
                    //def alllostBooks = BookItem.findAllByOrganizationAndBookstatus(user?.organization, bookStatusLost)
                    // data.add(alllostBooks.size())

                    ArrayList bookStatuslist = new ArrayList()
                    ArrayList bookStatusname = new ArrayList()
                    def bookStatus = BookStatus.findAllByIsactive(true)
                    for (ut in bookStatus) {
                        HashMap temp = new HashMap()
                        temp.put("book_statusId", ut.id)
                        temp.put("displayname", ut.displayname)
                        bookStatuslist.add(temp)
                        bookStatusname.add(ut.displayname)
                    }
                    hm.put("msg", "200")
                    hm.put("data", data)
                    hm.put("bookstatuslist", bookStatuslist)
                    hm.put("bookStatusname", bookStatusname)
                    return hm
                }
                return hm
            }
        }

        def getOrgData(request, hm) {
            println("in getOrgData")
            def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
            def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
            String url = request.getHeader("router-path")
            AuthService auth = new AuthService()

            String body = request.getReader().text
            def jsonSluper = new JsonSlurper()
            //def dataresponse = jsonSluper.parseText(body)

            //def userid =  dataresponse["userid"]

            String purpose = "fetch org data"
            auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

            if (hm.get("msg") != "401" && hm.get("msg") != "403") {
                Login log = Login.findByUsername(uid)
                ArrayList org_list = new ArrayList()
                Tenants.withId(log.tenantclient.tenantid) {
                    def user = User.findByUsername(log.username)
                    //def usr = User.findById(userid)
                    for (item in Organization.findAllByIsactive(true)) {

                        HashMap temp = new HashMap()
                        temp.put("id", item.id)
                        temp.put("organization", item.name)


                        org_list.add(temp)
                    }
                    ArrayList pgmList = new ArrayList()
                    def programList = Program.findAll()
                    for (program in programList) {
                        HashMap temp = new HashMap()
                        temp.put('department', program?.name)
                        temp.put('orgid', program?.organization?.id)
                        temp.put('id', program?.id)
                        pgmList.add(temp)
                    }
                    hm.put("pgmList", pgmList)
                    //println"pgmList"+pgmList
                    hm.put("org_list", org_list)
                    //println"org_list"+org_list

                    hm.put("msg", "200")
                    return hm
                }
                return hm
            }
        }

        def refreshstatus(request, hm) {
            println("in refreshstatus")
            def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
            def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
            String url = request.getHeader("router-path")
            AuthService auth = new AuthService()

            String body = request.getReader().text
            def jsonSluper = new JsonSlurper()
            //def dataresponse = jsonSluper.parseText(body)

            //def userid =  dataresponse["userid"]

            String purpose = "fetch org data"
            auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

            if (hm.get("msg") != "401" && hm.get("msg") != "403") {
                Login log = Login.findByUsername(uid)
                ArrayList org_list = new ArrayList()
                Tenants.withId(log.tenantclient.tenantid) {
                    def user = User.findByUsername(log.username)
                    def org = user.organization
                    BookStatus bookStatus = BookStatus.findByName("Issued")
                    BookStatus bookStatusava = BookStatus.findByName("Available")
                    if (bookStatus != null && bookStatusava != null) {
                        def bookItem = BookItem.findAllByBookstatusAndOrganization(bookStatus, org)
                        for (i in bookItem) {
                            BookLending bookLending = BookLending.findByBookitemAndIsactive(i, true)
                            if (!bookLending) {
                                BookItem bookItem1 = BookItem.findById(i.id)
                                if (bookItem1?.bookstatus?.id == bookStatus?.id) {
                                    bookItem1.bookstatus = bookStatusava
                                    bookItem1.save(failOnError: true, flush: true)
                                }
                            }
                        }
                    }
                    hm.put("msg", "200")
                    return hm
                }
                return hm
            }
        }

        //==========editMemberIssueBook
        def editMemberIssueBook(request, hm) {
            println("in editMemberIssueBook memberIssueBook")
            def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
            def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
            String url = request.getHeader("router-path")
            AuthService auth = new AuthService()

            String body = request.getReader().text
            def jsonSluper = new JsonSlurper()
            def dataresponse = jsonSluper.parseText(body)
            println "data response ------>" + dataresponse
            String purpose = "editMemberIssueBook data"
            auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
            if (hm.get("msg") != "401" && hm.get("msg") != "403") {
                Login log = Login.findByUsername(uid)
                Tenants.withId(log.tenantclient.tenantid) {
                    def user = User.findByUsername(log.username)
                    Program pgm
                    if (dataresponse.value == 'program') {
                        pgm = Program.findById(dataresponse.pgm)
                    }
                    Member member = Member.findByRegistration_number(dataresponse.regNo)
                    if (member == null) {
                        Employee emp = Employee.findByEmployee_code(dataresponse.regNo)
                        if (dataresponse.value == 'name') {
                            emp.name = dataresponse.name
                            emp.save(flush: true, failOnError: true)
                            hm.put("msg", "name")
                            return hm
                        } else if (dataresponse.value == 'program' && pgm != '' || pgm != null) {
                            emp.program = pgm
                            emp.save(flush: true, failOnError: true)
                            hm.put("msg", "program")
                            return hm
                        }else if (dataresponse.value =='isoutoforg') {
                            User user1=emp?.user
                            user1.isactive = dataresponse.isoutoforg
                            user1.save(flush: true, failOnError: true)
                            hm.put("msg", "isoutoforg")
                            return hm
                        }
                        Person person = Person.findByUser(emp?.user)
                        if (dataresponse.value == 'contact') {
                            person.mobile_number = dataresponse.contact
                            person.save(flush: true, failOnError: true)
                            hm.put("msg", "contact")
                            return hm
                        } else if (dataresponse.value == 'email') {
                            person.email = dataresponse.email
                            person.save(flush: true, failOnError: true)
                            hm.put("msg", "email")
                            return hm
                        }else if (dataresponse.value == 'totalcheck') {

                            User user1=User.findById( emp?.user?.id)
                            int totalcheck
                            if(dataresponse.totalbooks.getClass().getSimpleName()=='String')
                                totalcheck=Integer.parseInt(dataresponse.totalbooks)
                            else
                                totalcheck=dataresponse.totalbooks
                            println(user1)
                            println(totalcheck)
                            user1.number_of_books = totalcheck
                            user1.save(flush: true, failOnError: true)
                            hm.put("msg", "totalbooks")
                            return hm
                        }

                    } else {
                        if (dataresponse.value == 'membershipdate') {
                            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(dataresponse.date)
                            Date ndate = new Date(date.getTime() + (1000 * 60 * 60 * 24));
                            //println("ndate :"+ndate)
                            member.date_of_membership = ndate
                            member.save(flush: true, failOnError: true)
                            hm.put("msg", "date")
                            return hm
                        } else if (dataresponse.value == 'name') {
                            member.name = dataresponse.name
                            member.save(flush: true, failOnError: true)
                            hm.put("msg", "name")
                            return hm
                        } else if (dataresponse.value == 'program') {
                            member.program = pgm
                            member.save(flush: true, failOnError: true)
                            hm.put("msg", "program")
                            return hm
                        } else if (dataresponse.value == 'isoutoforg') {
                            User user1=member?.user
                            user1.isactive = dataresponse.isoutoforg
                            user1.save(flush: true, failOnError: true)
                            hm.put("msg", "isoutoforg")
                            return hm
                        }
                        Person p = Person.findByUser(member?.user)
                        if (dataresponse.value == 'contact') {
                            p.mobile_number = dataresponse.contact
                            p.save(flush: true, failOnError: true)
                            hm.put("msg", "contact")
                            return hm
                        } else if (dataresponse.value == 'email') {
                            p.email = dataresponse.email
                            p.save(flush: true, failOnError: true)
                            hm.put("msg", "email")
                            return hm
                        }
                        else if (dataresponse.value == 'totalcheck') {
                            User user1=User.findById( member?.user?.id)
                            int totalcheck
                            if(dataresponse.totalbooks.getClass().getSimpleName()=='String')
                                totalcheck=Integer.parseInt(dataresponse.totalbooks)
                            else
                                totalcheck=dataresponse.totalbooks
                            user1.number_of_books = totalcheck
                            user1.save(flush: true, failOnError: true)
                            hm.put("msg", "totalbooks")
                            return hm
                        }
                        AcademicYear academicYear = AcademicYear.findByIscurrent(true)
                        LearnerDivision ld = LearnerDivision.findByMemberAndOrganizationAndAcademic(member, user.organization, academicYear)

                        println('ld : ' + ld)
                        if (ld) {
                            if (dataresponse.value == 'year') {
                                Year year = Year.findById(dataresponse.year)
                                ld.year = year
                                hm.put("msg", "year")
                                return hm
                            } else if (dataresponse.value == 'division') {
                                ld.division = dataresponse.div
                                hm.put("msg", "division")
                                return hm
                            } else if (dataresponse.value == 'rollno') {
                                ld.rollno = dataresponse.rollNo
                                hm.put("msg", "rollno")
                                return hm
                            } else if (dataresponse.value == 'program' && pgm != '' || pgm != null) {
                                ld.program = pgm
                                ld.save(flush: true, failOnError: true)
                                hm.put("msg", "program")
                                return hm
                            }
                        }
                    }
                }

            }

        }
    }
