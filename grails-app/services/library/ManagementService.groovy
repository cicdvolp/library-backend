package library
import grails.gorm.transactions.Transactional
import grails.gorm.multitenancy.Tenants
import grails.converters.JSON
import groovy.json.JsonSlurper
import com.opencsv.CSVReader
import javax.servlet.http.Part;
import grails.io.IOUtils
import java.nio.file.Paths
import java.nio.file.Files
import java.nio.file.StandardCopyOption
import java.text.SimpleDateFormat
import java.text.DateFormat;
import java.util.Date
@Transactional

class ManagementService {

    def serviceMethod() { }
    //Get Management Link
    def getManagementLink(request, hm) {
        println "i am in AdminMasterMenu"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String purpose = "Getting Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList routerLinkList = new ArrayList()
                int col_size = 3
                HashMap temp = new HashMap()


                if(user.organization.ismanagement)
                {
                    temp.put('name', 'total-books')
                    temp.put('textval', 'Total Books')
                    temp.put('icon', 'mdi-book-open-page-variant')
                    temp.put('icontext', 'TB')
                    temp.put('flex', col_size)
                    def bookClassification =BookClassification.findAllByName("Book")
//                    def series=Series.findAllByBookclassification(bookClassification)
                    def series=Series.createCriteria().list(){
                        'in'('bookclassification', bookClassification)
                        and{
                            'in'('isactive', true)
                        }
                    }
                    def bookitem=BookItem.createCriteria().list(){
                        'in'('series', series)
                        and{

                            'in'('isactive', true)
                        }
                    }.size()
                    temp.put('size',bookitem)
                }
                else
                {
                    temp.put('name', 'total-books')
                    temp.put('textval', 'Total Books')
                    temp.put('icon', 'mdi-book-open-page-variant')
                    temp.put('icontext', 'TB')
                    temp.put('flex', col_size)
                    BookClassification bookClassification =BookClassification.findByNameAndOrganization("Book",user.organization)
                    def series=Series.findAllByBookclassificationAndOrganization(bookClassification,user.organization)
                    def bookitem=BookItem.createCriteria().list(){
                        'in'('organization',user.organization)
                        and{
                            'in'('series', series)
                            'in'('isactive', true)
                        }
                    }.size()
                    temp.put('size',bookitem)
                }

                routerLinkList.add(temp)

                if(user.organization.ismanagement)
                {

                    temp.put('name', 'total-books')
                    temp.put('textval', 'Total Books')
                    temp.put('icon', 'mdi-book-open-page-variant')
                    temp.put('icontext', 'TB')
                    temp.put('flex', col_size)
                    def bookClassification =BookClassification.findAllByName("Book With CD")

                    def series=Series.createCriteria().list(){
                        'in'('bookclassification', bookClassification)
                        and{
                            'in'('isactive', true)
                        }
                    }
                    def bookitem=BookItem.createCriteria().list(){
                        'in'('series', series)
                        and{

                            'in'('isactive', true)
                        }
                    }.size()
                    temp.put('size',bookitem)

                }else {
                    BookClassification bookClassification1 =BookClassification.findByNameAndOrganization("Book With CD",user.organization)

                    if(bookClassification1)
                    {
                        def series1=Series.findAllByBookclassificationAndOrganization(bookClassification1,user.organization)
                        temp = new HashMap()
                        temp.put('name', 'TotalbookWithCD')
                        temp.put('textval', 'Total Book With CD')
                        temp.put('icon', 'mdi-book-open-page-variant')
                        temp.put('icontext', 'BCD')
                        temp.put('flex', col_size)
                        if(series1){
                            def bookitem=BookItem.createCriteria().list(){
                                'in'('organization', user.organization)
                                and{
                                    'in'('series', series1)
                                    'in'('isactive', true)
                                }
                            }.size()

                            temp.put('size',bookitem)
                        }
                        else
                            temp.put('size',0)

                        routerLinkList.add(temp)
                    }


                }


                if(user.organization.ismanagement)
                {
                    def bookClassification1 =BookClassification.findAllByName("Periodic")
//                    def series1=Series.findAllByBookclassification(bookClassification1)
                    def series1=Series.createCriteria().list(){
                        'in'('bookclassification', bookClassification1)
                        and{
                            'in'('isactive', true)
                        }
                    }
                    def bookitem=BookItem.createCriteria().list(){
                        'in'('series', series1)
                        and{

                            'in'('isactive', true)
                        }
                    }.size()
                    temp = new HashMap()
                    temp.put('name', 'total-periodicals')
                    temp.put('textval', 'Total Periodicals')
                    temp.put('icon', 'mdi-book-open-page-variant')
                    temp.put('icontext', 'TB')
                    temp.put('flex', col_size)
                    temp.put('size',bookitem)
                }
                else
                {
                    BookClassification bookClassification1 =BookClassification.findByNameAndOrganization("Periodic",user.organization)
                    def series1=Series.findAllByBookclassificationAndOrganization(bookClassification1,user.organization)
                    temp = new HashMap()
                    temp.put('name', 'total-periodicals')
                    temp.put('textval', 'Total Periodicals')
                    temp.put('icon', 'mdi-book-open-page-variant')
                    temp.put('icontext', 'TB')
                    temp.put('flex', col_size)
                    if(series1){
                        def bookitem=BookItem.createCriteria().list(){
                            'in'('organization', user.organization)
                            and{
                                'in'('series', series1)
                                'in'('isactive', true)
                            }
                        }.size()

                        temp.put('size',bookitem)
                    }
                    else
                        temp.put('size',0)

                }

                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'total-employee')
                temp.put('textval', 'Employees')
                temp.put('icon', 'mdi-account-multiple-outline')
                temp.put('icontext', 'EP')
                temp.put('flex', col_size)
                if(user.organization.ismanagement)
                temp.put('size',Employee.findAll().size())
                else
                    temp.put('size', Employee.findAllWhere(
                            organization:user.organization
                    ).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'total-students')
                temp.put('textval', 'Students')
                temp.put('icon', 'mdi-school')
                temp.put('icontext', 'ST')
                temp.put('flex', col_size)
                if(user.organization.ismanagement)
                temp.put('size',Member.findAll().size())
                else
                    temp.put('size', Member.findAllWhere(
                            organization:user.organization
                    ).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'total-publisher')
                temp.put('textval', 'Books Publishers')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'BP')
                temp.put('flex', col_size)
                temp.put('size', Publisher.findAllWhere(
                        isactive: true
                ).size())

                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'total-authors')
                temp.put('textval', 'Books Authors')
                temp.put('icon', 'mdi-account-edit')
                temp.put('icontext', 'BA')
                temp.put('flex', col_size)
                temp.put('size', Author.findAllWhere(
                        isactive: true
                ).size())

                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'total-titles')
                temp.put('textval', 'Books cost- Titlewise')
                //temp.put('icon', 'mdi-book-plus')
                temp.put('icon', 'mdi-tooltip-text')
                temp.put('icontext', 'BT')
                temp.put('flex', col_size)
                if(user.organization.ismanagement)
                temp.put('size', Book.findAllWhere(
                        isactive: true
                ).size())
                else
                    temp.put('size', Book.findAllWhere(
                            isactive: true,
                            organization:user.organization
                    ).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'total-book-categories')
                temp.put('textval', 'Book Categories')
                temp.put('icon', 'mdi-book-multiple')
                temp.put('icontext', 'BC')
                temp.put('flex', col_size)
                if(user.organization.ismanagement)
                temp.put('size', BookCategory.findAllWhere(
                        isactive: true
                ).size())
                else
                    temp.put('size', BookCategory.findAllWhere(
                            isactive: true,
                            organization:user.organization
                    ).size())

                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'total-book-donations-request')
                temp.put('textval', 'Book Donation Request')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'BDR')
                temp.put('flex', col_size)
                if(user.organization.ismanagement)
                temp.put('size', BookDonationRequest.findAllWhere(
                        isactive: true
                ).size())
                else
                    temp.put('size', BookDonationRequest.findAllWhere(
                            isactive: true,
                            organization:user.organization
                    ).size())
                routerLinkList.add(temp)

                temp = new HashMap()

                def allOrg=[]
                if(user.organization.ismanagement)
                    allOrg=Organization.list()
                else
                    allOrg=user.organization
                int i=0
                int all_totalFine = 0
                for(org in allOrg)
                {
                    i++
                    def fineTransaction = FineTransaction.findAllByOrganizationAndIsactive(org,false)
                    int totalFine = 0
                    for(fine in fineTransaction){
                        totalFine= totalFine + fine?.amount
                    }
                    all_totalFine = all_totalFine + totalFine
                }


                temp.put('name', 'total-fine-collected')
                temp.put('textval', 'Fine Collected')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'FC')
                temp.put('flex', col_size)
                if(user.organization.ismanagement)
                temp.put('size', all_totalFine)
                else
                routerLinkList.add(temp)
                hm.put('routerLinkList', routerLinkList)
                return hm
            }
        }
        return hm
    }
    //get All Org BooksData
    def getAllOrgBooksData(request, hm) {
        println "i in get All Org, Books Data :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "Org Book Data."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList orgbooklist = new ArrayList()
                def allOrg=[]
                if(user.organization.ismanagement)
                 allOrg=Organization.list()
                else
                    allOrg=user.organization
                int i=0
                for(org in allOrg)
                {
                    i++
                    HashMap temp=new HashMap()
                    def bookType=BookClassification.findByOrganizationAndName(org,"Book")
                    def books
                    def series =null
                    if(bookType==null)
                    {
                         series=Series.findAllByOrganization(org)
                        books=BookItem.findAllByOrganization(org)
                    }

                    else
                    {

                        series=Series.findAllByOrganizationAndBookclassification(org,bookType)
                        println(series)
                       def series1=Series.findAllByBookclassificationAndOrganization(bookType,org)

                        if(series1){
                            books=BookItem.createCriteria().list(){
                                'in'('organization', org)
                                and{
                                    'in'('series', series1)
                                    'in'('isactive', true)
                                }
                            }
                        }



                    }
                    temp.put("orgName",org?.displayname+" ("+org?.name+")")
                    temp.put("totslBooks",books==null?0:books.size())
                    temp.put("srno",i)
                    temp.put("orgid",org?.id)
                    def libraryDepartment=LibraryDepartment.findAllByOrganization(org)
                    temp.put("Dept_Count",libraryDepartment.size())

                    temp.put("Series_Count",series.size())
                    orgbooklist.add(temp)
                }
                hm.put("orgbooklist",orgbooklist)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    def getAllOrgPeriodicalsData(request, hm) {
        println "i in get All Org, Books Data :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "Org Book Data."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList orgbooklist = new ArrayList()
                def allOrg=[]
                if(user.organization.ismanagement)
                 allOrg=Organization.list()
                else
                    allOrg=user.organization
                int i=0
                BookStatus bookStatus=BookStatus.findByName("Coming")
                for(org in allOrg)
                {
                    i++
                    HashMap temp=new HashMap()
                    def bookType=BookClassification.findByOrganizationAndName(org,"Periodic")
                    def books
                    def series =null
                    if(bookType==null)
                    {
                         series=Series.findAllByOrganization(org)
                        books=BookItem.findAllByOrganizationAndBookstatusNotEqual(org,bookStatus)
                    }

                    else
                    {

                        series=Series.findAllByOrganizationAndBookclassification(org,bookType)
                        println(series)
                       def series1=Series.findAllByBookclassificationAndOrganization(bookType,org)
                        if(series1){
                            books=BookItem.createCriteria().list(){
                                'in'('organization', org)
                                and{
                                    'in'('series', series1)
                                    'in'('isactive', true)
                                    ne('bookstatus',bookStatus)
                                }
                            }
                        }



                    }

                    temp.put("orgName",org?.displayname+" ("+org?.name+")")
                    temp.put("totslBooks",books!=null?books.size():0)
                    temp.put("srno",i)
                    temp.put("orgid",org?.id)
                    def libraryDepartment=LibraryDepartment.findAllByOrganization(org)
                    temp.put("Dept_Count",libraryDepartment.size())

                    temp.put("Series_Count",series.size())
                    orgbooklist.add(temp)
                }
                hm.put("orgbooklist",orgbooklist)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    def getAllOrgBookWithCDData(request, hm) {
        println "i in get All Org, Books Data :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "Org Book Data."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList orgbooklist = new ArrayList()
                def allOrg=[]
                if(user.organization.ismanagement)
                 allOrg=Organization.list()
                else
                    allOrg=user.organization
                int i=0
                for(org in allOrg)
                {
                    i++
                    HashMap temp=new HashMap()
                    def bookType=BookClassification.findByOrganizationAndName(org,"Book With CD")
                    def books
                    def series =null
                    if(bookType==null)
                    {
                         series=Series.findAllByOrganization(org)
                        books=BookItem.findAllByOrganization(org)
                    }

                    else
                    {

                        series=Series.findAllByOrganizationAndBookclassification(org,bookType)
                        println(series)
                       def series1=Series.findAllByBookclassificationAndOrganization(bookType,org)
                        if(series1){
                            books=BookItem.createCriteria().list(){
                                'in'('organization', org)
                                and{
                                    'in'('series', series1)
                                    'in'('isactive', true)
                                }
                            }
                        }



                    }

                    temp.put("orgName",org?.displayname+" ("+org?.name+")")
                    temp.put("totslBooks",books!=null?books.size():0)
                    temp.put("srno",i)
                    temp.put("orgid",org?.id)
                    def libraryDepartment=LibraryDepartment.findAllByOrganization(org)
                    temp.put("Dept_Count",libraryDepartment.size())

                    temp.put("Series_Count",series.size())
                    orgbooklist.add(temp)
                }
                hm.put("orgbooklist",orgbooklist)
                hm.put("msg",'200')
            }
        }
        return hm
    }

    def getorglist(request, hm) {
        println "org list"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "Org list."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList orgbooklist = new ArrayList()
                def allOrg=[]
                if(user.organization.ismanagement)
                    allOrg=Organization.list()
                else
                    allOrg=user.organization
                int i=0
                for(org in allOrg)
                {
                    i++
                    HashMap temp=new HashMap()

                    temp.put("orgName",org?.displayname+" ("+org?.name+")")
                    temp.put("srno",i)
                    temp.put("orgid",org?.id)
                    orgbooklist.add(temp)
                }
                HashMap temp=new HashMap()
                if(user.organization.ismanagement){
                    temp.put("orgName","ALL")
                    temp.put("srno",++i)
                    temp.put("orgid",'ALL')
                    orgbooklist.add(temp)
                }

                hm.put("orglist",orgbooklist)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    def orgdeptdetails(request, hm) {
        println "i in get orgdeptdetails:: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "dept Details."
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        // println("dataresponse ::"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList orgbookdetails = new ArrayList()
                Organization org=Organization.findById(dataresponse.orgid)
                hm.put("orgName", org?.displayname+" ("+org?.name+")")
                int i=0
              def dept=LibraryDepartment.findAllByOrganization(org)
                for(d in dept) {
                    HashMap temp=new HashMap()
                    i++
                    temp.put("srno",i)

                    temp.put("deptname", d?.name)
                    temp.put("dept_id", d?.id)
                    temp.put("orgid", org?.id)
                    def bookitem=BookItem.findAllByOrganizationAndLibrarydepartment(org,d)
                    temp.put("Book_count", bookitem.size())
                    orgbookdetails.add(temp)
                }
                println(orgbookdetails)
                //}
                hm.put("orgdeptdetails",orgbookdetails)
                hm.put("msg",'200')
            }
        }
        return hm
    }

    def getorgseriesdetails(request, hm) {
        println "i in get orgdeptdetails:: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "dept Details."
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        // println("dataresponse ::"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList orgbookdetails = new ArrayList()
                Organization org=Organization.findById(dataresponse.orgid)
                hm.put("orgName", org?.displayname+" ("+org?.name+")")
                int i=0

                BookClassification bookType=BookClassification.findByOrganizationAndName(org,"Book")
                def dept
                if(bookType==null)
                    dept=Series.findAllByOrganization(org)
                else
                {
                     dept=Series.findAllByBookclassificationAndOrganization(bookType,org)

                }

                for(d in dept) {
                    HashMap temp=new HashMap()
                    i++
                    temp.put("srno",i)

                    temp.put("seriesname", d?.name)
                    def bookitem=BookItem.findAllByOrganizationAndSeries(org,d)
                    temp.put("Book_count", bookitem.size())
                    temp.put("series_id", d?.id)
                    temp.put("orgid", org?.id)

                    orgbookdetails.add(temp)
                }

                //}
                hm.put("orgseriesdetails",orgbookdetails)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    def getorgseriesdetailsperiodicals(request, hm) {
        println "i in get orgdeptdetails:: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "dept Details."
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        // println("dataresponse ::"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList orgbookdetails = new ArrayList()
                Organization org=Organization.findById(dataresponse.orgid)
                hm.put("orgName", org?.displayname+" ("+org?.name+")")
                int i=0
                BookStatus bookStatus=BookStatus.findByName("Coming")
                BookClassification bookType=BookClassification.findByOrganizationAndName(org,"Periodic")
                def dept
                if(bookType==null)
                    dept=Series.findAllByOrganization(org)
                else
                {
                     dept=Series.findAllByBookclassificationAndOrganization(bookType,org)

                }

                for(d in dept) {
                    HashMap temp=new HashMap()
                    i++
                    temp.put("srno",i)

                    temp.put("seriesname", d?.name)
                    def bookitem=BookItem.findAllByOrganizationAndSeriesAndBookstatusNotEqual(org,d,bookStatus)
                    temp.put("Book_count", bookitem.size())
                    temp.put("series_id", d?.id)
                    temp.put("orgid", org?.id)

                    orgbookdetails.add(temp)
                }

                //}
                hm.put("orgseriesdetails",orgbookdetails)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    def getorgseriesdetailsBookWithCD(request, hm) {
        println "i in get orgdeptdetails:: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "dept Details."
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        // println("dataresponse ::"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList orgbookdetails = new ArrayList()
                Organization org=Organization.findById(dataresponse.orgid)
                hm.put("orgName", org?.displayname+" ("+org?.name+")")
                int i=0

                BookClassification bookType=BookClassification.findByOrganizationAndName(org,"Book With CD")
                def dept
                if(bookType==null)
                    dept=Series.findAllByOrganization(org)
                else
                {
                     dept=Series.findAllByBookclassificationAndOrganization(bookType,org)

                }

                for(d in dept) {
                    HashMap temp=new HashMap()
                    i++
                    temp.put("srno",i)

                    temp.put("seriesname", d?.name)
                    def bookitem=BookItem.findAllByOrganizationAndSeries(org,d)
                    temp.put("Book_count", bookitem.size())
                    temp.put("series_id", d?.id)
                    temp.put("orgid", org?.id)

                    orgbookdetails.add(temp)
                }

                //}
                hm.put("orgseriesdetails",orgbookdetails)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    //get All Org Books Details
    def getOrgBooksDetails(request, hm) {
        println "i in get All Org, Books Details :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "Book Details."
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
       // println("dataresponse ::"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList orgbookdetails = new ArrayList()
                Organization org=Organization.findById(dataresponse.orgid)
                hm.put("orgName", org?.displayname+" ("+org?.name+")")
                int i=0
                def books=Book.findAllByOrganization(org)
                //for(bk in Book)
                //{
                    //def booksItems=BookItem.findAllByOrganizationAndBook(org,bk)
//                    def booksItems=BookItem.findAllByOrganization(org)
                BookClassification bookClassification =BookClassification.findByNameAndOrganization("Book",user.organization)
                def series=Series.findAllByBookclassificationAndOrganization(bookClassification,user.organization)
                int maxauther=0
                for( se in  series){


                def booksItems=BookItem.createCriteria().list(){
                    'in'('organization', org)
                    and{
                        'in'('series', se)
                        'in'('isactive', true)
                    }
                }

                if(!booksItems.isEmpty())
                    booksItems=booksItems.sort{it?.display_order}

                    for(bi in booksItems) {
                        def booksAuthors=BooksAuthors.findAllByOrganizationAndBook(user.organization,bi?.book)?.author
                        if(booksAuthors && booksAuthors.size()>maxauther)
                        {
                            maxauther=booksAuthors?.size()
                        }
                        HashMap temp=new HashMap()
                        i++
                        temp.put("srno",i)
                        int y=1
                        for(x in booksAuthors)
                        {
                            temp.put("author"+y,x?.name)
                            y=y+1
                        }
                        temp.put("orgName", bi?.organization?.name)
                        temp.put("date_of_entry", bi?.date_of_entry)
                        temp.put("accession_number", bi?.accession_number)
                        temp.put("subject", bi?.subject?bi?.subject:"")
                        temp.put("isbn", bi?.book?.isbn)
                        temp.put("title", bi?.book?.title)
                        temp.put("author", bi?.book?.author?.name)
                        temp.put("edition", bi?.book?.edition)
                        temp.put("medium", bi?.book?.language?.name)
                        temp.put("bookformat", bi?.bookformat?.name)
                        temp.put("booktype", bi?.book?.booktype?.name)
                        temp.put("bookcategory", bi?.book?.bookcategory?.name)
                        temp.put("publisher", bi?.book?.publisher?.name)
                        temp.put("Series", bi?.series?.name)
                        temp.put("actualprice", bi?.book_price)
                        temp.put("price", bi?.price)
                        temp.put("discount", bi?.discount_percentage)
                        temp.put("publisher_year", bi?.publication_year)
                        temp.put("localtion", bi?.location)
                        temp.put("billno", bi?.bill_number)
                        temp.put("cno", bi?.book?.classificationno)
                        temp.put("dept", bi?.librarydepartment?.name)
                        temp.put("pages", bi?.numberofpages)
                        temp.put("barcode", bi?.barocde)
                        temp.put("remark", bi?.remarks)
                        temp.put("vendor", bi?.vendor_name)
                        orgbookdetails.add(temp)
                    }
                }
                hm.put("orgbookdetails",orgbookdetails)
                hm.put("maxauther",maxauther)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    def getOrgPeriodicalsDetails(request, hm) {
        println "i in get All Org, Books Details :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "Book Details."
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
       // println("dataresponse ::"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList orgbookdetails = new ArrayList()
                Organization org=Organization.findById(dataresponse.orgid)
                hm.put("orgName", org?.displayname+" ("+org?.name+")")
                int i=0
                def books=Book.findAllByOrganization(org)
                //for(bk in Book)
                //{
                    //def booksItems=BookItem.findAllByOrganizationAndBook(org,bk)
//                    def booksItems=BookItem.findAllByOrganization(org)
                BookStatus bookStatus=BookStatus.findByName("Coming")
                BookClassification bookClassification =BookClassification.findByNameAndOrganization("Periodic",user.organization)
                def series=Series.findAllByBookclassificationAndOrganization(bookClassification,user.organization)
                def booksItems=BookItem.createCriteria().list(){
                    'in'('organization', org)
                    and{
                        'in'('series', series)
                        'in'('isactive', true)
                        ne('bookstatus',bookStatus)

                    }
                }
                if(!booksItems.isEmpty())
                    booksItems=booksItems.sort{it?.display_order}
                int maxauther=0
                    for(bi in booksItems) {
                        def booksAuthors=BooksAuthors.findAllByOrganizationAndBook(user.organization,bi?.book)?.author
                        if(booksAuthors && booksAuthors?.size()>maxauther)
                        {
                            maxauther=booksAuthors?.size()
                        }
                        HashMap temp=new HashMap()
                        i++
                        temp.put("srno",i)
                        int y=1
                        for(x in booksAuthors)
                        {
                            temp.put("author"+y,x?.name)
                            y=y+1
                        }
                        temp.put("orgName", bi?.organization?.name)
                        temp.put("date_of_entry", bi?.date_of_entry)
                        temp.put("accession_number", bi?.accession_number)
                        temp.put("subject", bi?.subject?bi?.subject:"")
                        temp.put("isbn", bi?.book?.isbn)
                        temp.put("title", bi?.book?.title)
                        temp.put("author", bi?.book?.author?.name)
                        temp.put("edition", bi?.book?.edition)
                        temp.put("bookformat", bi?.bookformat?.name)
                        temp.put("booktype", bi?.book?.booktype?.name)
                        temp.put("bookcategory", bi?.book?.bookcategory?.name)
                        temp.put("publisher", bi?.book?.publisher?.name)
                        temp.put("Series", bi?.series?.name)
                        temp.put("actualprice", bi?.book_price)
                        temp.put("price", bi?.price)
                        temp.put("discount", bi?.discount_percentage)
                        temp.put("publisher_year", bi?.publication_year)
                        temp.put("localtion", bi?.location)
                        temp.put("billno", bi?.bill_number)
                        temp.put("cno", bi?.book?.classificationno)
                        temp.put("dept", bi?.librarydepartment?.name)
                        temp.put("pages", bi?.numberofpages)
                        temp.put("barcode", bi?.barocde)
                        orgbookdetails.add(temp)
                    }
                //}
                hm.put("orgbookdetails",orgbookdetails)
                hm.put("maxauther",maxauther)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    def getOrgBookWithCDDetails(request, hm) {
        println "i in get All Org, Books Details :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "Book Details."
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
       // println("dataresponse ::"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList orgbookdetails = new ArrayList()
                Organization org=Organization.findById(dataresponse.orgid)
                hm.put("orgName", org?.displayname+" ("+org?.name+")")
                int i=0
                def books=Book.findAllByOrganization(org)
                //for(bk in Book)
                //{
                    //def booksItems=BookItem.findAllByOrganizationAndBook(org,bk)
//                    def booksItems=BookItem.findAllByOrganization(org)
                BookClassification bookClassification =BookClassification.findByNameAndOrganization("Book With CD",user.organization)
                def series=Series.findAllByBookclassificationAndOrganization(bookClassification,user.organization)
                def booksItems=BookItem.createCriteria().list(){
                    'in'('organization', org)
                    and{
                        'in'('series', series)
                        'in'('isactive', true)
                    }
                }
                if(!booksItems.isEmpty())
                    booksItems=booksItems.sort{it.display_order}
                int maxauther=0
                    for(bi in booksItems) {
                        if(bi?.book?.author && bi?.book?.author?.size()>maxauther)
                        {
                            maxauther=bi?.book?.author?.size()
                        }
                        HashMap temp=new HashMap()
                        i++
                        temp.put("srno",i)
                        int y=1
                        for(x in bi?.book?.author)
                        {
                            temp.put("author"+y,x?.name)
                            y=y+1
                        }
                        temp.put("orgName", bi?.organization?.name)
                        temp.put("date_of_entry", bi?.date_of_entry)
                        temp.put("accession_number", bi?.accession_number)
                        temp.put("subject", bi?.subject?bi?.subject:"")
                        temp.put("isbn", bi?.book?.isbn)
                        temp.put("title", bi?.book?.title)
                        temp.put("author", bi?.book?.author?.name)
                        temp.put("edition", bi?.book?.edition)
                        temp.put("bookformat", bi?.bookformat?.name)
                        temp.put("booktype", bi?.book?.booktype?.name)
                        temp.put("bookcategory", bi?.book?.bookcategory?.name)
                        temp.put("publisher", bi?.book?.publisher?.name)
                        temp.put("Series", bi?.series?.name)
                        temp.put("actualprice", bi?.book_price)
                        temp.put("price", bi?.price)
                        temp.put("discount", bi?.discount_percentage)
                        temp.put("publisher_year", bi?.publication_year)
                        temp.put("localtion", bi?.location)
                        temp.put("billno", bi?.bill_number)
                        temp.put("cno", bi?.book?.classificationno)
                        temp.put("dept", bi?.librarydepartment?.name)
                        temp.put("pages", bi?.numberofpages)
                        temp.put("barcode", bi?.barocde)
                        orgbookdetails.add(temp)
                    }
                //}
                hm.put("orgbookdetails",orgbookdetails)
                hm.put("maxauther",maxauther)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    def getorgseriesdata(request, hm) {
        println "i in get All Org, Books Details :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "Book Details."
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse ::"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList orgbookdetails = new ArrayList()
                Organization org=Organization.findById(dataresponse.orgid)
                hm.put("orgName", org?.displayname+" ("+org?.name+")")
                int i=0
                def books=Book.findAllByOrganization(org)
                //for(bk in Book)
                //{
                    //def booksItems=BookItem.findAllByOrganizationAndBook(org,bk)
                Series series=Series.findById(dataresponse.series_id)
                BookStatus bookStatus=BookStatus.findByName("Coming")
                    def booksItems=BookItem.findAllByOrganizationAndSeriesAndBookstatusNotEqual(org,series,bookStatus)
                if(!booksItems.isEmpty())
                booksItems=booksItems.sort{it?.display_order}
                int maxauther=0
                    for(bi in booksItems) {
                        def booksAuthors=BooksAuthors.findAllByOrganizationAndBook(user.organization,bi?.book)?.author
                        if(booksAuthors && booksAuthors?.size()>maxauther)
                        {
                            maxauther=booksAuthors?.size()
                        }
                        HashMap temp=new HashMap()
                        i++
                        temp.put("srno",i)
                        int y=1

                        for(x in booksAuthors)
                        {
                            temp.put("author"+y,x?.name)
                            y=y+1
                        }
                        temp.put("orgName", bi?.organization?.name)
                        temp.put("accession_number", bi?.accession_number)
                        temp.put("subject", bi?.subject?bi?.subject:"")
                        temp.put("date_of_entry", bi?.date_of_entry)
                        temp.put("isbn", bi?.book?.isbn)
                        temp.put("title", bi?.book?.title)
                        temp.put("author", bi?.book?.author?.name)
                        temp.put("edition", bi?.book?.edition)
                        temp.put("medium", bi?.book?.language?.name)
                        temp.put("bookformat", bi?.bookformat?.name)
                        temp.put("booktype", bi?.book?.booktype?.name)
                        temp.put("bookcategory", bi?.book?.bookcategory?.name)
                        temp.put("publisher", bi?.book?.publisher?.name)
                        temp.put("Series", bi?.series?.name)
                        temp.put("actualprice", bi?.book_price)
                        temp.put("price", bi?.price)
                        temp.put("discount", bi?.discount_percentage)
                        temp.put("publisher_year", bi?.publication_year)
                        temp.put("localtion", bi?.location)
                        temp.put("billno", bi?.bill_number)
                        temp.put("cno", bi?.book?.classificationno)
                        temp.put("dept", bi?.librarydepartment?.name)
                        temp.put("pages", bi?.numberofpages)
                        temp.put("barcode", bi?.barocde)
                        orgbookdetails.add(temp)
                    }
                //}
                hm.put("orgbookdetails",orgbookdetails)

                hm.put("maxauther",maxauther)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    def getorgdeptdata(request, hm) {
        println "i in get All Org, Books Details :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "Book Details."
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse ::"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList orgbookdetails = new ArrayList()
                Organization org=Organization.findById(dataresponse.orgid)
                hm.put("orgName", org?.displayname+" ("+org?.name+")")
                int i=0
                def books=Book.findAllByOrganization(org)
                //for(bk in Book)
                //{
                    //def booksItems=BookItem.findAllByOrganizationAndBook(org,bk)
                LibraryDepartment dept=LibraryDepartment.findById(dataresponse.dept_id)
                BookStatus bookStatus=BookStatus.findByName("Coming")
                    def booksItems=BookItem.findAllByOrganizationAndLibrarydepartmentAndBookstatusNotEqual(org,dept,bookStatus)
                if(!booksItems.isEmpty())
                booksItems=booksItems.sort{it?.display_order}
                int maxauther=0
                    for(bi in booksItems) {
                        def booksAuthors=BooksAuthors.findAllByOrganizationAndBook(user.organization,bi?.book)?.author
                        if(booksAuthors &&booksAuthors?.size()>maxauther)
                        {
                            maxauther=booksAuthors.size()
                        }
                        HashMap temp=new HashMap()
                        i++
                        temp.put("srno",i)
                        int y=1
                        for(x in booksAuthors)
                        {
                            temp.put("author"+y,x?.name)
                            y=y+1
                        }
                        temp.put("orgName", bi?.organization?.name)
                        temp.put("accession_number", bi?.accession_number)
                        temp.put("subject", bi?.subject?bi?.subject:"")
                        temp.put("date_of_entry", bi?.date_of_entry)
                        temp.put("isbn", bi?.book?.isbn)
                        temp.put("title", bi?.book?.title)
                        temp.put("author", bi?.book?.author?.name)
                        temp.put("edition", bi?.book?.edition)
                        temp.put("medium", bi?.book?.language?.name)
                        temp.put("bookformat", bi?.bookformat?.name)
                        temp.put("booktype", bi?.book?.booktype?.name)
                        temp.put("bookcategory", bi?.book?.bookcategory?.name)
                        temp.put("publisher", bi?.book?.publisher?.name)
                        temp.put("Series", bi?.series?.name)
                        temp.put("actualprice", bi?.book_price)
                        temp.put("price", bi?.price)
                        temp.put("discount", bi?.discount_percentage)
                        temp.put("publisher_year", bi?.publication_year)
                        temp.put("localtion", bi?.location)
                        temp.put("billno", bi?.bill_number)
                        temp.put("cno", bi?.book?.classificationno)
                        temp.put("dept", bi?.librarydepartment?.name)
                        temp.put("pages", bi?.numberofpages)
                        temp.put("barcode", bi?.barocde)
                        orgbookdetails.add(temp)
                    }
                //}
                hm.put("orgbookdetails",orgbookdetails)

                hm.put("maxauther",maxauther)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    //get All Org Emp data
    def getAllOrgEmpData(request, hm) {
        println "i getAllOrgEmpData :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "getAllOrgEmpData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList epmlist = new ArrayList()
                def allOrg=[]
                if(user.organization.ismanagement)
                    allOrg=Organization.list()
                else
                    allOrg=user.organization
                int i=0
                for(org in allOrg)
                {
                    i++
                    HashMap temp=new HashMap()
                    def totalemp=Employee.findAllByOrganization(org)
                    temp.put("orgName",org?.displayname+" ("+org?.name+")")
                    temp.put("totalemp",totalemp.size())
                    temp.put("srno",i)
                    temp.put("orgid",org?.id)
                    epmlist.add(temp)
                }
                hm.put("epmlist",epmlist)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    //get All Emp Details
    def getEmployeeDetails(request, hm) {
        println "i in getEmployeeDetails :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "getEmployeeDetails."
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
       // println("dataresponse ::"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList empdetails = new ArrayList()
                Organization org=Organization.findById(dataresponse.orgid)
                hm.put("orgName", org?.displayname+" ("+org?.name+")")
                int i=0

                    def employees=Employee.findAllByOrganization(org)
                    for(item in employees) {
                        HashMap temp=new HashMap()
                        i++
                        temp.put("srno",i)
                        temp.put("orgName", item?.organization?.displayname+" ("+item?.organization?.name+")")
                        temp.put("emp_code", item?.employee_code)
                        temp.put("name", item?.name)
                        temp.put("email", item?.person?.email)
                        temp.put("mobile_number", item?.person?.mobile_number)
                        temp.put("address", item?.person?.address)
                        temp.put("city", item?.person?.city)
                        temp.put("state", item?.person?.state)
                        temp.put("country", item?.person?.country)
                        temp.put("pin", item?.person?.pin)
                        empdetails.add(temp)
                    }

                hm.put("empdetails",empdetails)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    //get All Org Students data
    def getAllOrgStudData(request, hm) {
        println "i getAllOrgStudData :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "getAllOrgStudData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList datalist = new ArrayList()
                def allOrg=[]
                if(user.organization.ismanagement)
                    allOrg=Organization.list()
                else
                    allOrg=user.organization
                int i=0
                for(org in allOrg)
                {
                    i++
                    HashMap temp=new HashMap()
                    def total=Member.findAllByOrganization(org)
                    temp.put("orgName",org?.displayname+" ("+org?.name+")")
                    temp.put("total",total.size())
                    temp.put("srno",i)
                    temp.put("orgid",org?.id)
                    datalist.add(temp)
                }
                hm.put("datalist",datalist)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    //get All Emp Details
    def getStudentsDetails(request, hm) {
        println "i in ggetStudentsDetails :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "getStudentsDetails."
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse ::"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList detailslist = new ArrayList()
                Organization org=Organization.findById(dataresponse.orgid)
                hm.put("orgName", org?.displayname+" ("+org?.name+")")
                int i=0
                    def members=Member.findAllByOrganization(org)
                    for(item in members) {
                        HashMap temp=new HashMap()
                        i++
                        temp.put("srno",i)
                        temp.put("orgName", item?.organization?.displayname+" ("+item?.organization?.name+")")
                        temp.put("reg_no", item?.registration_number)
                        temp.put("name", item?.name)
                        temp.put("email", item?.person?.email)
                        temp.put("mobile_number", item?.person?.mobile_number)
                        temp.put("address", item?.person?.address)
                        temp.put("city", item?.person?.city)
                        temp.put("state", item?.person?.state)
                        temp.put("country", item?.person?.country)
                        temp.put("pin", item?.person?.pin)
                        detailslist.add(temp)
                    }
                hm.put("detailslist",detailslist)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    //get All Org Author data
    def getAllOrgData(request, hm) {
        println "i auther :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "getAllOrgData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList datalist = new ArrayList()
                ArrayList authdatalist = new ArrayList()
                def allOrg=Organization.list()

                int i=0

             def autho=Author.list()
                for(au in autho){
                    i++
                    HashMap temp=new HashMap()
                    temp.put("srno",i);
                    temp.put("author_name",au?.name);
                    temp.put("author_desc",au?.description);
                    authdatalist.add(temp)
                }

                hm.put("authdatalist",authdatalist)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    //get All Emp Details
    def getDetails(request, hm) {
        println "i in getDetails :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "getDetails."
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse ::"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList authdatalist = new ArrayList()
                Organization org=Organization.findById(dataresponse.orgid)
                def total=Book.findAllByOrganization(org)
                def autho=[]
                for(b in total)
                {
                    for(au in b?.author){
                        autho.add(au)
                    }
                }
                int i=0

                for(au in autho.unique{it.name}){
                    i++
                    HashMap temp=new HashMap()
                    temp.put("srno",i);
                    temp.put("author_name",au?.name);
                    temp.put("author_desc",au?.description);
                    authdatalist.add(temp)
                }
                hm.put("authdatalist",authdatalist)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    //get All Pub. data
    def getAllPubData(request, hm) {
        println "i getAllPubData :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "getAllPubData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList datalist = new ArrayList()
                ArrayList pubdatalist = new ArrayList()
                int i=0
                def pub=[]
                     def allOrg   =Organization.list()
                for(org in allOrg)
                {
                    HashMap temp=new HashMap()
                    def total=Book.findAllByOrganization(org)
                    for(b in total)
                    {
                        pub.add(b?.publisher)
                    }
                    temp.put("orgName",org?.displayname+" ("+org?.name+")")
                    temp.put("orgid",org?.id)
                    datalist.add(temp)
                }
                for(pb in pub){
                    i++
                    HashMap temp=new HashMap()
                    temp.put("srno",i);
                    temp.put("pub_name",pb?.name);
                    pubdatalist.add(temp)
                }
                hm.put("datalist",datalist)
                hm.put("pubdatalist",pubdatalist)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    //get All Pub Details
    def getPubDetails(request, hm) {
        println "i in getPubDetails :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "getPubDetails."
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse ::"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList pubdatalist = new ArrayList()
                Organization org=Organization.findById(dataresponse.orgid)
                def total=Book.findAllByOrganization(org)
                def pub=[]
                for(b in total)
                {
                    pub.add(b?.publisher)
                }
                int i=0
                for(pb in pub){
                    i++
                    HashMap temp=new HashMap()
                    temp.put("srno",i);
                    temp.put("pub_name",pb?.name);
                    pubdatalist.add(temp)
                }
                hm.put("pubdatalist",pubdatalist)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    //get All Title. data
    def getAllTitleData(request, hm) {
        println "i getAllTitleData :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "getAllPubData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList datalist = new ArrayList()
                ArrayList titledatalist = new ArrayList()
                def allOrg=Organization.list()
                int i=0
                def gradndtotal=0
                for(org in allOrg)
                {
                    HashMap temp=new HashMap()
                        def total = Book.findAllByOrganization(org)
                        for (b in total) {
                            i++
                            HashMap data = new HashMap()
                            data.put("srno", i)
                            data.put("title", b?.title)
                            data.put("publisher", b?.publisher?.name)
                            data.put("total_books", b?.total_books)
                            data.put("entry_date", new SimpleDateFormat("dd-MM-yyyy").format(b?.creation_date))
                            def bookItem = BookItem.findAllByBookAndOrganization(b, org)
                            int sum=0
                            if(b?.total_books!=0 && b?.total_books!=null && b?.total_books>0 && bookItem[0]?.price!=0 && bookItem[0]?.price!=null && bookItem[0])
                            {
                                sum=bookItem[0]?.price*b?.total_books
                                gradndtotal=gradndtotal+sum
                            }
                            else {
                                sum =0
                            }
                            data.put("totalprice",sum)
                            data.put("cost",bookItem[0]?.price)
                            data.put("price", bookItem[0]?.book_price)
                            for (pb in b?.author.unique { it.name }) {
                                data.put("author", pb?.name)
                            }
                            titledatalist.add(data)

                        }
                    temp.put("orgName",org?.displayname+" ("+org?.name+")")
                    temp.put("orgid",org?.id)
                    datalist.add(temp)
                }

                hm.put("gradndtotal",gradndtotal)
                hm.put("datalist",datalist)
                hm.put("titledatalist",titledatalist)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    //get All Title Details
    def getTitleDetails(request, hm) {
        println "i in getPubDetails :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "getPubDetails."
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse ::"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList titledatalist = new ArrayList()
                Organization org=Organization.findById(dataresponse.orgid)
                def pub=[]
                def total = Book.findAllByOrganization(org)
                int i=0
                int gradndtotal=0
                for (b in total) {
                    i++
                    HashMap data = new HashMap()
                    data.put("srno", i)
                    data.put("title", b?.title)
                    data.put("publisher", b?.publisher?.name)
                    data.put("total_books", b?.total_books)
                    data.put("entry_date", new SimpleDateFormat("dd-MM-yyyy").format(b?.creation_date))
                    def bookItem = BookItem.findAllByBookAndOrganization(b, org)
                    data.put("cost",bookItem[0]?.price)
                    int sum=0
                    if(b?.total_books!=0 && b?.total_books!=null && b?.total_books>0 && bookItem[0]?.price!=0 && bookItem[0]?.price!=null && bookItem[0])
                    {
                        sum=bookItem[0]?.price*b?.total_books
                        gradndtotal=gradndtotal+sum
                    }
                    else {
                        sum =0
                    }
                    data.put("totalprice",sum)
                    data.put("price", bookItem[0]?.book_price)
                    for (pb in b?.author.unique { it.name }) {
                        data.put("author", pb?.name)
                    }
                    titledatalist.add(data)
                }
                hm.put("titledatalist",titledatalist)
                hm.put("gradndtotal",gradndtotal)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    //get All Org Books Donation Data
    def getAllDonationData(request, hm) {
        println "getAllDonationData :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "Org Book Data."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList bookDonationRequestlist = new ArrayList()
                def allOrg=[]
                if(user.organization.ismanagement)
                    allOrg=Organization.list()
                else
                    allOrg=user.organization
                int i=0
                for(org in allOrg)
                {
                    i++
                    HashMap temp=new HashMap()
                    def booksDonation=BookDonationRequest.findAllByOrganization(org)
                    temp.put("orgName",org?.displayname+" ("+org?.name+")")
                    temp.put("totalbooksDonation",booksDonation.size())
                    temp.put("srno",i)
                    temp.put("orgid",org?.id)
                    bookDonationRequestlist.add(temp)
                }
                hm.put("bookDonationRequestlist",bookDonationRequestlist)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    //get All Donation Books Details
    def getDonationDetails(request, hm) {
        println "i in getDonationDetails :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "getDonationDetails."
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse ::"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList requestdate = new ArrayList()
                Organization org=Organization.findById(dataresponse.orgid)
                hm.put("orgName", org?.displayname+" ("+org?.name+")")
                int i=0
                def booksDonation=BookDonationRequest.findAllByOrganization(org)
                for(bi in booksDonation) {
                    HashMap temp=new HashMap()
                    i++
                    temp.put("srno",i)
                    temp.put("orgName", bi?.organization?.name)
                    Member member=Member.findByOrganizationAndUser(org,bi?.user)
                    temp.put("name", member?.name)
                    temp.put("user", bi?.user?.username)
                    temp.put("user_type", bi?.user?.usertype?.name)
                    temp.put("book_condition", bi?.bookcondition?.name)
                    temp.put("request_date", new SimpleDateFormat("dd-MM-yyyy").format(bi?.request_date))
                    temp.put("delivery_date", new SimpleDateFormat("dd-MM-yyyy").format(bi?.expected_delivery_date))
                    temp.put("copys", bi?.number_of_copies)
                    requestdate.add(temp)
                }
                hm.put("requestdate",requestdate)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    def getAllOrgCategoryData(request, hm) {
        println "i getAllOrgCategoryData :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "getAllOrgCategoryData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList datalist = new ArrayList()
                ArrayList categorylist = new ArrayList()
                def allOrg=Organization.list()
                int i=0
                def bCat=[]
                for(org in allOrg)
                {
                    HashMap temp=new HashMap()
                    def total=Book.findAllByOrganization(org)
                    for(b in total)
                    {
                        for(bcategory in b?.bookcategory){
                            bCat.add(bcategory)
                            //print"bCat"+bCat
                        }
                    }
                    temp.put("orgName",org?.displayname+" ("+org?.name+")")
                    temp.put("orgid",org?.id)
                    datalist.add(temp)
                }
                for(bookCat in bCat.unique{it.name}){
                    i++
                    HashMap temp=new HashMap()
                    temp.put("srno",i);
                    temp.put("bookCat_name",bookCat?.name);
                    categorylist.add(temp)
                }
                hm.put("datalist",datalist)
                hm.put("categorylist",categorylist)
                hm.put("msg",'200')
            }
        }
        return hm
    }
    def getCatDetails(request, hm) {
        println "i in getCatDetails :: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "getCatDetails."
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse ::"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList categorylist = new ArrayList()
                Organization org=Organization.findById(dataresponse.orgid)
                print"org"+org
                def total=Book.findAllByOrganization(org)
                def bCat=[]
                for(b in total)
                { for(bcategory in b?.bookcategory){
                    bCat.add(bcategory)
                    //print"bCat"+bCat
                }
                }
                int i=0

                for(bookCat in bCat.unique{it.name}){
                    i++
                    HashMap temp=new HashMap()
                    temp.put("srno",i);
                    temp.put("bookCat_name",bookCat?.name);
                    categorylist.add(temp)
                }
                hm.put("categorylist",categorylist)
                hm.put("msg",'200')
            }
        }
        return hm
    }
}
