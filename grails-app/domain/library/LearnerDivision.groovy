package library

import grails.gorm.MultiTenant
class LearnerDivision implements MultiTenant<LearnerDivision> {
    String rollno
    String division
    Boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[member:Member, organization:Organization,academic:AcademicYear,semester:Semester,year:Year,shift:Shift,program:Program]
    static constraints = {
        shift nullable:true
    }
    static mapping = {
        isactive defaultValue: true
    }
}
