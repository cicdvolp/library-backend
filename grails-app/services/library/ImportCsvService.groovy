package library
import grails.gorm.multitenancy.Tenants
import grails.converters.JSON
import groovy.json.JsonSlurper
import grails.gorm.transactions.Transactional
import com.opencsv.CSVReader
import javax.servlet.http.Part;
import grails.io.IOUtils
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import java.text.SimpleDateFormat

@Transactional
class ImportCsvService {

    def serviceMethod() {

    }

    def importMenu(request, ip, hm) {
        println "i am in ImportMenu"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String purpose = "Getting Admin Import Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList routerLinkList = new ArrayList()
                int col_size = 3
                HashMap temp = new HashMap()
                temp.put('name', 'employee')
                temp.put('textval', 'Import Employee')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'IE')
                temp.put('flex', col_size)
                UserType userType = UserType.findByNameIlike("%Employee%")
                def allemp = Employee.findAllByOrganization(user.organization)
                int i = 1
                for (e in allemp) {
                    if (e?.user?.usertype?.id == userType?.id) {
                        i++
                    }
                }
                i = i - 1
                temp.put('size', i)
                routerLinkList.add(temp)
                temp = new HashMap()
                temp.put('name', 'member')
                temp.put('textval', 'Import Member')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'IM')
                temp.put('flex', col_size)
                UserType student = UserType.findByNameIlike("%Member%")
                def allstudent = Member.findAllByOrganization(user.organization)
                int j = 1
                for (e in allstudent) {
                    if (e.user.usertype.id == student.id) {
                        j++
                    }
                }
                j = j - 1
                temp.put('size', j)
                routerLinkList.add(temp)



                temp = new HashMap()
                temp.put('name', 'import-book')
                temp.put('textval', 'Import Book')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'IB')
                temp.put('flex', col_size)
                def book = BookItem.findAllByOrganization(user.organization).size()
                temp.put('size',book )

                routerLinkList.add(temp)


                temp = new HashMap()
                temp.put('name', 'import-issue-book')
                temp.put('textval', 'Import Issue Book')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'IIB')
                temp.put('flex', col_size)
                def status = BookLending.findAllByOrganizationAndIsactive(user.organization,true).size()
                println("status : "+status)
                temp.put('size', status)
                routerLinkList.add(temp)


                hm.put('routerLinkList', routerLinkList)
                return hm
            }
        }
        return hm
    }

    def importEmployeeData(request, ip, hm) {
        println("in import service importEmployeeData")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "fetch employee data"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                UserType userType = UserType.findByNameIlike("%Employee%")
                def alluser = User.findAllByOrganization(user.organization)
                def userlist = []
                int count = 1
                for (usr in alluser) {
                    HashMap temp = new HashMap()
                    if (usr.usertype.id == userType.id) {
                        temp.put("sr_no", count)
                        temp.put("userid", usr?.id)
                        temp.put("user", usr?.username)
                        Employee employee = Employee.findByUserAndOrganization(usr, user.organization)
                        temp.put("empid", employee?.id)
                        temp.put("employee_code", employee?.employee_code)
                        temp.put("pt", employee?.program?.id)
                        temp.put("program", employee?.program?.name)
                        temp.put("empname", employee?.name)
                        Person person = Person.findByUser(usr)
                        temp.put("mobile_number", person?.mobile_number)
                        temp.put("email", person?.email)
                        temp.put("address", person?.address)
                        temp.put("city", person?.city)
                        temp.put("state", person?.state)
                        temp.put("country", person?.country)
                        temp.put("pin", person?.pin)
                        userlist.add(temp)
                        count++
                    }
                }

                ArrayList pt = new ArrayList()
                def ptype = Program.findAll()
                for (item in ptype) {
                    HashMap temp = new HashMap()
                    temp.put("name", item.name)
                    temp.put("id", item.id)
                    pt.add(temp)
                }
                hm.put('programtypelist', pt)
                println userlist
                hm.put("emplist", userlist)
                hm.put("msg", "200")
                return hm
            }
            return hm
        }
    }

    def importMemberData(request, ip, hm) {
        println("in import service importMemberData")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "fetch employee data"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                UserType userType = UserType.findByNameIlike("%Member%")
                def alluser = User.findAllByOrganization(user.organization)
                def userlist = []
                int count = 1
                for (usr in alluser) {
                    HashMap temp = new HashMap()
                    if (usr.usertype.id == userType.id) {
                        temp.put("sr_no", count)
                        temp.put("userid", usr?.id)
                        temp.put("user", usr?.username)
//                        Employee employee = Employee.findByUserAndOrganization(usr,user.organization)
                        Member member = Member.findByUserAndOrganization(usr, user.organization)
                        temp.put("empid", member?.id)
                        temp.put("employee_code", member?.registration_number)
                        temp.put("program", member?.program?.id)
                        temp.put("pg", member?.program?.name)
                        temp.put("empname", member?.name)
                        Person person = Person.findByUser(usr)
                        temp.put("mobile_number", person?.mobile_number)
                        temp.put("email", person?.email)
                        temp.put("address", person?.address)
                        temp.put("city", person?.city)
                        temp.put("state", person?.state)
                        temp.put("country", person?.country)
                        temp.put("pin", person?.pin)
                        userlist.add(temp)
                        count++
                    }
                }
                ArrayList program = new ArrayList()
                def program_list = Program.findAll()
                for (item in program_list) {
                    HashMap temp = new HashMap()
                    temp.put("name", item.name)
                    temp.put("id", item.id)
                    program.add(temp)
                }
                hm.put('program_list', program)
                println userlist
                hm.put("emplist", userlist)
                hm.put("msg", "200")
                return hm
            }
            return hm
        }
    }

    def editInstructor(request, ip, hm) {
        println("Inside import service/update employee information...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        String loginattachmentdomainname = ""

        def userid = dataresponse["userid"]
        def empid = dataresponse["empid"]
        def paramuser = dataresponse["user"]
        def employee_code = dataresponse["employee_code"]
        def pin = dataresponse["pin"]
        def country = dataresponse["country"]
        def state = dataresponse["state"]
        def city = dataresponse["city"]
        def address = dataresponse["address"]
        def mobile_number = dataresponse["mobile_number"]
        def empname = dataresponse["empname"]
        def email = dataresponse["email"]
        def pt = dataresponse["pt"]

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            //def organization = user.organization
            Tenants.withId(log.tenantclient.tenantid) {
                def logeduser = User.findByUsername(log.username)
//                UserType usertype = UserType.findById(selectedusertype.userTypeId)
                loginattachmentdomainname = logeduser.organization.loginattachementdomain
                User user = User.findByUsernameAndOrganization(paramuser, logeduser.organization)
                user.updation_username = uid
                user.updation_date = new Date()
                user.updation_ip_address = ip
                user.organization = logeduser.organization
                user.save(flush: true, failOnError: true)

                Person person = Person.findByUser(user)
                person.address = address
                person.mobile_number = mobile_number
                person.email = email
                person.city = city
                person.state = state
                person.country = country
                person.pin = pin
                person.updation_date = new Date()
                person.updation_ip_address = ip
                person.user = user
                person.save(flush: true, failOnError: true)

                Employee employee = Employee.findByUserAndPersonAndOrganization(user, person, logeduser.organization)
                employee.employee_code = employee_code
                employee.name = empname
                if (pt) {
                    Program program = Program.findById(pt)
                    employee.program = program
                }
                employee.updation_username = uid
                employee.updation_date = new Date()
                employee.creation_ip_address = ip
                employee.updation_ip_address = ip
                employee.user = user
                employee.person = person
                employee.save(flush: true, failOnError: true)

                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def editMember(request, ip, hm) {
        println("Inside import service/update employee information...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        String loginattachmentdomainname = ""

        def userid = dataresponse["userid"]
        def empid = dataresponse["empid"]
        def paramuser = dataresponse["user"]
        def employee_code = dataresponse["employee_code"]
        def pin = dataresponse["pin"]
        def country = dataresponse["country"]
        def state = dataresponse["state"]
        def city = dataresponse["city"]
        def address = dataresponse["address"]
        def mobile_number = dataresponse["mobile_number"]
        def empname = dataresponse["empname"]
        def email = dataresponse["email"]
        def program = dataresponse["program"]

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            //def organization = user.organization
            Tenants.withId(log.tenantclient.tenantid) {
                def logeduser = User.findByUsername(log.username)
//                UserType usertype = UserType.findById(selectedusertype.userTypeId)
                loginattachmentdomainname = logeduser.organization.loginattachementdomain
                User user = User.findByUsernameAndOrganization(paramuser, logeduser.organization)
                user.updation_username = uid
                user.updation_date = new Date()
                user.updation_ip_address = ip
                user.organization = logeduser.organization
                user.save(flush: true, failOnError: true)

                Person person = Person.findByUser(user)
                person.address = address
                person.mobile_number = mobile_number
                person.email = email
                person.city = city
                person.state = state
                person.country = country
                person.pin = pin
                person.updation_date = new Date()
                person.updation_ip_address = ip
                person.user = user
                person.save(flush: true, failOnError: true)

                Member employee = Member.findByUserAndPersonAndOrganization(user, person, logeduser.organization)
                employee.registration_number = employee_code
                if (program) {
                    employee.program = Program.findById(program)
                }
                employee.name = empname
                employee.updation_username = uid
                employee.updation_date = new Date()
                employee.creation_ip_address = ip
                employee.updation_ip_address = ip
                employee.user = user
                employee.person = person
                employee.save(flush: true, failOnError: true)

                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def addEmployee(request, ip, hm) {
        println("Inside import service/update employee information...")
        AdminService adminServicev = new AdminService()
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        String loginattachmentdomainname = ""

//        def userid =  dataresponse["userid"]
//        def empid =  dataresponse["empid"]
//        def paramuser =  dataresponse["user"]
        def employee_code = dataresponse["employee_code"]
        def pin = dataresponse["pin"]
        def country = dataresponse["country"]
        def state = dataresponse["state"]
        def city = dataresponse["city"]
        def address = dataresponse["address"]
        def mobile_number = dataresponse["mobile_number"]
        def empname = dataresponse["empname"]
        def email = dataresponse["email"]
        def pt = dataresponse["pt"]

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            //def organization = user.organization
            Tenants.withId(log.tenantclient.tenantid) {
                def logeduser = User.findByUsername(log.username)
                UserType usertype = UserType.findByNameIlike("%Employee%")
                loginattachmentdomainname = logeduser.organization.loginattachementdomain
                adminServicev.createLogin(uid, empname, ip, loginattachmentdomainname, log.tenantclient,employee_code)
                User user = User.findByUsernameAndOrganization(empname + logeduser.organization.loginattachementdomain, logeduser.organization)
                if (user) {
                    hm.put('message', 'User already available!')
                    hm.put('code', 1)
                } else {
                    user = new User()
                    user.username = empname + loginattachmentdomainname
                    user.total_issued_books = 0
                    user.creation_username = uid
                    user.updation_username = uid
                    user.creation_date = new Date()
                    user.updation_date = new Date()
                    user.creation_ip_address = ip
                    user.updation_ip_address = ip
                    user.organization = logeduser.organization
                    user.usertype = usertype
                    user.save(flush: true, failOnError: true)

                    Person person = new Person()
                    person.address = address
                    person.mobile_number = mobile_number
                    person.email = email
                    person.city = city
                    person.state = state
                    person.country = country
                    person.pin = pin
                    person.creation_username = uid
                    person.updation_username = uid
                    person.creation_date = new Date()
                    person.updation_date = new Date()
                    person.creation_ip_address = ip
                    person.updation_ip_address = ip
                    person.user = user
                    person.save(flush: true, failOnError: true)

                    Employee employee = new Employee()
                    employee.employee_code = employee_code
                    employee.name = empname
                    employee.creation_username = uid
                    employee.updation_username = uid
                    employee.creation_date = new Date()
                    employee.updation_date = new Date()
                    employee.creation_ip_address = ip
                    employee.updation_ip_address = ip
                    if (pt) {
                        Program program = Program.findById(pt)
                        employee.program = program
                    }
                    employee.organization = logeduser.organization
                    employee.user = user
                    employee.person = person
                    employee.save(flush: true, failOnError: true)

                    //Role role = Role.findByNameAndUsertype
                    Role role = Role.findByNameIlike("%Teacher%")
                    user.addToRole(role)
                    user.save(flush: true, failOnError: true)
                }
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def addMember(request, ip, hm) {
        println("Inside import service/update employee information...")
        AdminService adminServicev = new AdminService()
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        String loginattachmentdomainname = ""

//        def userid =  dataresponse["userid"]
//        def empid =  dataresponse["empid"]
//        def paramuser =  dataresponse["user"]
        def employee_code = dataresponse["employee_code"]
        def pin = dataresponse["pin"]
        def country = dataresponse["country"]
        def state = dataresponse["state"]
        def city = dataresponse["city"]
        def address = dataresponse["address"]
        def mobile_number = dataresponse["mobile_number"]
        def empname = dataresponse["empname"]
        println "empname in add member" + empname
        def email = dataresponse["email"]

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            //def organization = user.organization
            Tenants.withId(log.tenantclient.tenantid) {
                def logeduser = User.findByUsername(log.username)
                UserType usertype = UserType.findByNameIlike("%Member%")
                loginattachmentdomainname = logeduser.organization.loginattachementdomain
                adminServicev.createLogin(uid, empname, ip, loginattachmentdomainname, log.tenantclient,employee_code)
                User user = User.findByUsernameAndOrganization(empname + logeduser.organization.loginattachementdomain, logeduser.organization)
                if (user) {
                    hm.put('message', 'User already available!')
                    hm.put('code', 1)
                } else {
                    user = new User()
                    user.username = empname + loginattachmentdomainname
                    user.total_issued_books = 0
                    user.creation_username = uid
                    user.updation_username = uid
                    user.creation_date = new Date()
                    user.updation_date = new Date()
                    user.creation_ip_address = ip
                    user.updation_ip_address = ip
                    user.organization = logeduser.organization
                    user.usertype = usertype
                    user.save(flush: true, failOnError: true)

                    Person person = new Person()
                    person.address = address
                    person.mobile_number = mobile_number
                    person.email = email
                    person.city = city
                    person.state = state
                    person.country = country
                    person.pin = pin
                    person.creation_username = uid
                    person.updation_username = uid
                    person.creation_date = new Date()
                    person.updation_date = new Date()
                    person.creation_ip_address = ip
                    person.updation_ip_address = ip
                    person.user = user
                    person.save(flush: true, failOnError: true)

//                    Employee employee = new Employee()
//                    employee.employee_code = employee_code
//                    employee.name = empname
//                    employee.creation_username = uid
//                    employee.updation_username = uid
//                    employee.creation_date = new Date()
//                    employee.updation_date = new Date()
//                    employee.creation_ip_address = ip
//                    employee.updation_ip_address = ip
//                    employee.organization = logeduser.organization
//                    employee.user = user
//                    employee.person = person
//                    employee.save(flush: true, failOnError: true)

                    Member member = new Member()
                    member.registration_number = employee_code
                    member.name = empname
                    member.date_of_membership = new Date()
                    member.creation_username = uid
                    member.updation_username = uid
                    member.creation_date = new Date()
                    member.updation_date = new Date()
                    member.creation_ip_address = ip
                    member.updation_ip_address = ip
                    member.organization = logeduser.organization
                    member.user = user
                    member.person = person
                    member.save(flush: true, failOnError: true)

                    //Role role = Role.findByNameAndUsertype
                    Role role = Role.findByNameIlike("%Member%")
                    user.addToRole(role)
                    user.save(flush: true, failOnError: true)
                }
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def importEmployee(request, ip, hm) {
        println(' in  import employee :')
        def uid = request.getHeader("EPS-uid")
        Part filePart = request.getPart('file')
        InputStream fs = filePart.getInputStream()
        File file = File.createTempFile("s3test", "");
        OutputStream outputStream = new FileOutputStream(file)
        IOUtils.copy(fs, outputStream);
        def reader = new CSVReader(new FileReader(file))
        AdminService adminServicev = new AdminService()
        println('fs----' + fs)
        String loginattachmentdomainname = ""
        String[] nextRecord;
        boolean header = true
        ArrayList failList = new ArrayList()

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def logeduser = User.findByUsername(log.username)
                UserType usertype = UserType.findByNameIlike("%Employee%")
                loginattachmentdomainname = logeduser.organization.loginattachementdomain

                while ((nextRecord = reader.readNext()) != null) {
                    String reason = 'Data is Missing - '
                    HashMap temp = new HashMap()
                    if (header) //skip headers
                    {
                        header = false
                        continue
                    }
                    if (nextRecord[2] != '') {
//                        User user = User.findByUsernameAndOrganization(nextRecord[0].trim()+logeduser.organization.loginattachementdomain,logeduser.organization)
                        User user = User.findByUsernameAndOrganization(nextRecord[2].trim(), logeduser.organization)
                        if (user == null) {
//                            adminServicev.createLogin(uid, nextRecord[0].trim(), ip, loginattachmentdomainname,log.tenantclient)
                            adminServicev.createLogin(uid, nextRecord[2].trim(), ip, loginattachmentdomainname, log.tenantclient,nextRecord[0].trim())
                            user = new User()
//                            user.username = nextRecord[0].trim() + loginattachmentdomainname
                            user.username = nextRecord[2].trim()
                            user.total_issued_books = 0
                            user.creation_username = uid
                            user.updation_username = uid
                            user.creation_date = new Date()
                            user.updation_date = new Date()
                            user.creation_ip_address = ip
                            user.updation_ip_address = ip
                            user.organization = logeduser.organization
                            user.usertype = usertype
                            user.save(flush: true, failOnError: true)

                            Person person = new Person()
                            //person.address = address
                            person.mobile_number = nextRecord[3].trim()
                            person.email = nextRecord[2].trim()
//                            person.city = city
//                            person.state = state
//                            person.country = country
//                            person.pin = pin
                            person.creation_username = uid
                            person.updation_username = uid
                            person.creation_date = new Date()
                            person.updation_date = new Date()
                            person.creation_ip_address = ip
                            person.updation_ip_address = ip
                            person.user = user
                            person.save(flush: true, failOnError: true)

                            Employee employee = new Employee()
                            employee.employee_code = nextRecord[0].trim()
                            employee.name = nextRecord[1].trim()
                            if (nextRecord[4]) {
                                Program program = Program.findByName(nextRecord[4])
                                employee.program = program
                            }
                            employee.creation_username = uid
                            employee.updation_username = uid
                            employee.creation_date = new Date()
                            employee.updation_date = new Date()
                            employee.creation_ip_address = ip
                            employee.updation_ip_address = ip
                            employee.organization = logeduser.organization
                            employee.user = user
                            employee.person = person
                            employee.save(flush: true, failOnError: true)

                            //Role role = Role.findByNameAndUsertype
                            Role role
                            role = Role.findByNameIlike("%Teacher%")

                            user.addToRole(role)
                            user.save(flush: true, failOnError: true)
                            hm.put('status', 200)
                        } else {
                            reason = "User already present"
                            temp.put('reason', reason)
                            temp.put('name', nextRecord[2].trim())
                            failList.add(temp)
                        }
                    }
                }
            }
        }
        outputStream.flush()
        outputStream.close()
        fs.close()
        file.delete()

        hm.put('failList', failList)
        return hm
    }

    def importMember(request, ip, hm) {
        println(' in  import Member :')
        def uid = request.getHeader("EPS-uid")
        Part filePart = request.getPart('file')
        InputStream fs = filePart.getInputStream()
        File file = File.createTempFile("s3test", "");
        OutputStream outputStream = new FileOutputStream(file)
        IOUtils.copy(fs, outputStream);
        def reader = new CSVReader(new FileReader(file))
        AdminService adminServicev = new AdminService()
        println('fs----' + fs)
        String loginattachmentdomainname = ""
        String[] nextRecord;
        boolean header = true
        ArrayList failList = new ArrayList()

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def logeduser = User.findByUsername(log.username)
                UserType usertype = UserType.findByNameIlike("%Member%")
                loginattachmentdomainname = logeduser.organization.loginattachementdomain

                while ((nextRecord = reader.readNext()) != null) {
                    String reason = 'Data is Missing - '
                    HashMap temp = new HashMap()
                    if (header) //skip headers
                    {
                        header = false
                        continue
                    }
                    if (nextRecord[0] != '') {
                        User user = User.findByUsernameAndOrganization(nextRecord[0].trim() + logeduser.organization.loginattachementdomain, logeduser.organization)
                        if (user == null) {
                            adminServicev.createLogin(uid, nextRecord[0].trim(), ip, loginattachmentdomainname, log.tenantclient)
                            user = new User()
                            user.username = nextRecord[0].trim() + loginattachmentdomainname
                            user.total_issued_books = 0
                            user.creation_username = uid
                            user.updation_username = uid
                            user.creation_date = new Date()
                            user.updation_date = new Date()
                            user.creation_ip_address = ip
                            user.updation_ip_address = ip
                            user.organization = logeduser.organization
                            user.usertype = usertype
                            user.save(flush: true, failOnError: true)

                            Person person = new Person()
                            //person.address = address
                            person.mobile_number = nextRecord[3].trim()
                            person.email = nextRecord[2].trim()
//                            person.city = city
//                            person.state = state
//                            person.country = country
//                            person.pin = pin
                            person.creation_username = uid
                            person.updation_username = uid
                            person.creation_date = new Date()
                            person.updation_date = new Date()
                            person.creation_ip_address = ip
                            person.updation_ip_address = ip
                            person.user = user
                            person.save(flush: true, failOnError: true)

                            Member employee = new Member()
                            employee.registration_number = nextRecord[0].trim()
                            if (nextRecord[4]) {
                                Program program = Program.findByName(nextRecord[4])
                                employee.program = program
                            }
                            employee.name = nextRecord[1].trim()
                            employee.creation_username = uid
                            employee.updation_username = uid
                            employee.creation_date = new Date()
                            employee.updation_date = new Date()
                            employee.creation_ip_address = ip
                            employee.updation_ip_address = ip
                            employee.organization = logeduser.organization
                            employee.user = user
                            employee.person = person
                            employee.save(flush: true, failOnError: true)

                            //Role role = Role.findByNameAndUsertype
                            Role role = Role.findByNameIlike("%Member%")
                            user.addToRole(role)
                            user.save(flush: true, failOnError: true)
                            hm.put('status', 200)
                        } else {
                            reason = "User already present"
                            temp.put('reason', reason)
                            temp.put('name', nextRecord[0].trim())
                            failList.add(temp)
                        }
                    }
                }
            }
        }
        outputStream.flush()
        outputStream.close()
        fs.close()
        file.delete()
        hm.put('failList', failList)
        return hm
    }

    def importBook(request, hm, ip) {
        println(' in  import book:')
        def uid = request.getHeader("EPS-uid")
        println('---------uid' + uid)
        Part filePart = request.getPart('file')
        InputStream fs = filePart.getInputStream()
        File file = File.createTempFile("s3test", "");
        OutputStream outputStream = new FileOutputStream(file)
        IOUtils.copy(fs, outputStream);
        def reader = new CSVReader(new FileReader(file))
        println('fs----' + fs)

        String[] nextRecord;
        boolean header = true
        ArrayList failList = new ArrayList()
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)

            Tenants.withId(log.tenantclient.tenantid) {
                def headerArray = []
                while ((nextRecord = reader.readNext()) != null) {
                    String reason = 'Data is Missing'

                    HashMap temp = new HashMap()
                    if (header) {
                        headerArray = nextRecord
                        header = false
                        continue
                    }

                    def keyindex = 0
                    HashMap tempdata = new HashMap()
                    for (item in headerArray) {
                        tempdata.put(item, nextRecord[keyindex])
                        keyindex++
                    }
                    println("tempdata  :" + tempdata)
                    def user = User.findByUsername(uid)
                    def accession_no = tempdata?."Accession No."
                    def isbn = tempdata?."ISBN"
                    def dateofentry = tempdata?."Date of Entry"
                    def series = tempdata?."Series"
                    def title_subtitle = tempdata?."Title - Subtitle"
                    def author = tempdata?."Author"
                    def edition = tempdata?."Edition"
                    def year_of_publication = tempdata?."Year of Publication"
                    def publisher_name = tempdata?."Publisher"
                    def place_of_publication = tempdata?."Place of publication"
                    def date_of_publication = tempdata?."Date of Publication"
                    def date_of_purchase = tempdata?."Date of Purchase"
                    def pages = tempdata?."Pages"
                    def department = tempdata?."Department"
                    def subject = tempdata?."Subject"
                    def physical_details = tempdata?."Physical Details"
                    def accompanying_materials = tempdata?."Accompanying Materials"
                    def key_words = tempdata?."Key Words"
                    def item_types = tempdata?."Item Types"
                    def media_link = tempdata?."Media Link"
                    def classification_no = tempdata?."Classification No."
                    def source_vendor = tempdata?."Source / Vendor"
                    def bill_no = tempdata?."Bill No."
                    def price = tempdata?."Price"
                    def discount = tempdata?."Discount (%)"
                    def net_cost = tempdata?."Net Cost Rs."
                    def withdrawal_approval_no_with_date = tempdata?."Withdrawal Approval No. with date"
                    def location = tempdata?."Location"
                    def ILL = tempdata?."ILL"
                    def remarks = tempdata?."Remarks"

                    if (accession_no && isbn) {
                        println('in if all mandatory...')
                        def user_org1 = User.findByUsername(uid)
                        Book book = Book.findByIsbnAndOrganization(isbn, user_org1?.organization)
                        if (book) {
                            println('in if book already in database')

                            BookItem bookItem = BookItem.findByAccession_numberAndOrganization(accession_no, user_org1?.organization)
                            if (bookItem == null) {
                                bookItem = new BookItem()

                                if (year_of_publication)
                                    bookItem.publication_year = year_of_publication

                                try {
                                    if (date_of_publication && date_of_publication != 'Invalid date') {
                                        SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");
                                        Date date1 = formatter.parse(date_of_publication);
                                        bookItem.publication_date = date1
                                    }

                                    if (withdrawal_approval_no_with_date)
                                        bookItem.withdrawal_approval_no_with_date = withdrawal_approval_no_with_date

                                    if (date_of_purchase && date_of_purchase != 'Invalid date') {
                                        SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");
                                        Date date2 = formatter.parse(date_of_purchase);
                                        bookItem.date_of_purchase = date2
                                    }

                                    if (dateofentry && dateofentry != 'Invalid date') {
                                        SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");
                                        Date date3 = formatter.parse(dateofentry);
                                        bookItem.date_of_entry = date3
                                    }

                                } catch (Exception e) {
                                }

                                if (pages)
                                    bookItem.numberofpages = Integer.parseInt(pages)

                                if (price)
                                    bookItem.price = Double.parseDouble(price)

                                if (net_cost)
                                    bookItem.book_price = Double.parseDouble(net_cost)

                                if (remarks)
                                    bookItem.remarks = remarks

                                if (location)
                                    bookItem.location = location


                                if (ILL == 'Yes')
                                    bookItem.ill = true
                                else
                                    bookItem.ill = false

                                if (discount) {
                                    def discount_per = discount
                                    discount_per = discount_per.replace("%", "");
                                    bookItem.discount_percentage = Double.parseDouble(discount_per)
                                }

                                bookItem.accession_number = accession_no

                                def bs = BookStatus.findByName('Available')
                                bookItem.bookstatus = bs

                                bookItem.is_received_from_donation = 0
                                bookItem.isactive = true
                                bookItem.book = book

                                if (bill_no)
                                    bookItem.bill_number = bill_no
                                if (key_words)
                                    bookItem.keyword = key_words
                                if (place_of_publication)
                                    bookItem.publication_place = place_of_publication

                                def user_org = User.findByUsername(uid)
                                bookItem.organization = user_org?.organization
//                                ERPDepartment erpDepartment
//
//                                if(department){
//                                     erpDepartment = ERPDepartment.findByNameAndOrganization(department, user?.organization)
//                                }
//
                                if(series){
                                    Series bs1 = Series.findByNameAndOrganization(series?.trim(),user.organization)
                                    if (bs1 != null) {
                                        bookItem.series =bs1
                                    }
                                }

                                bookItem.updation_ip_address = ip
                                bookItem.creation_ip_address = ip
                                bookItem.updation_date = new Date()
                                bookItem.creation_date = new Date()
                                bookItem.updation_username = uid
                                bookItem.creation_username = uid
                                bookItem.save(flush: true, failOnError: true)
                                book.total_books = book.total_books + 1
                                book.updation_ip_address = ip
                                book.updation_date = new Date()
                                book.updation_username = uid
                                book.save(flush: true, failOnError: true)
                                println('book.total_books after+++++++++' + book.total_books)

                            } else {
                                println('book item already present')
                            }

                        } else {
                            println('in if book save ')

                            book = new Book()
                            book.isbn = isbn
                            book.isactive = true
                            if (title_subtitle)
                                book.title = title_subtitle
                            if (subject)
                                book.subject = subject
                            if (classification_no)
                                book.classificationno = classification_no

                            if (media_link)
                                book.medialink = media_link

                            if (edition)
                                book.edition = edition


                            if (physical_details) {
                                PhysicalDetails pd = PhysicalDetails.findByName(physical_details?.trim())
                                if (pd != null) {
                                    book.physicaldetails = pd
                                } else {
                                    pd = new PhysicalDetails()
                                    pd.isactive = true
                                    pd.name = physical_details?.trim()
                                    pd.updation_ip_address = ip
                                    pd.creation_ip_address = ip
                                    pd.updation_date = new Date()
                                    pd.creation_date = new Date()
                                    pd.updation_username = uid
                                    pd.creation_username = uid
                                    pd.save(flush: true, failOnError: true)
                                    book.physicaldetails = pd
                                }
                            }

                            if (accompanying_materials) {
                                AccompanyingMaterials amat = AccompanyingMaterials.findByName(accompanying_materials?.trim())
                                if (amat != null) {
                                    book.accompanyingmaterials = amat
                                } else {
                                    amat = new AccompanyingMaterials()
                                    amat.isactive = true
                                    amat.name = accompanying_materials?.trim()
                                    amat.updation_ip_address = ip
                                    amat.creation_ip_address = ip
                                    amat.updation_date = new Date()
                                    amat.creation_date = new Date()
                                    amat.updation_username = uid
                                    amat.creation_username = uid
                                    amat.save(flush: true, failOnError: true)
                                    book.accompanyingmaterials = amat
                                }
                            }

                            String[] authorArray = author.trim().split("\\s*,\\s*");
                            for (int author_i = 0; author_i < authorArray.size(); author_i++) {
                                println('author_i=====' + author_i)
                                println('authorArray[author_i]=======' + authorArray[author_i])
                                def authorId = Author.findByName(authorArray[author_i])
                                if (authorId != null) {
                                    book.addToAuthor(authorId)
                                } else {
                                    authorId = new Author()
                                    authorId.isactive = true
                                    authorId.name = authorArray[author_i]
                                    authorId.updation_ip_address = ip
                                    authorId.creation_ip_address = ip
                                    authorId.updation_date = new Date()
                                    authorId.creation_date = new Date()
                                    authorId.updation_username = uid
                                    authorId.creation_username = uid
                                    authorId.save(flush: true, failOnError: true)

                                    book.addToAuthor(authorId)
                                }
                            }

                            if (item_types) {
                                def bt = BookType.findByNameAndOrganization(item_types, user.organization)
                                if (bt != null) {
                                    book.booktype = bt
                                } else {
                                    bt = new BookType()
                                    bt.isactive = true
                                    bt.name = item_types
                                    bt.updation_ip_address = ip
                                    bt.creation_ip_address = ip
                                    bt.updation_date = new Date()
                                    bt.creation_date = new Date()
                                    bt.updation_username = uid
                                    bt.creation_username = uid
                                    bt.organization = user.organization
                                    bt.save(flush: true, failOnError: true)

                                    book.booktype = bt
                                }
                            }

//                            if(department){
//                                erpDepartment = ERPDepartment.findByNameAndOrganization(department, user?.organization)
//                            }
//                            if(series){
//                                Series bs1 = Series.findByNameAndOrganization(series?.trim(), user?.organization)
//                                if (bs1 != null) {
//                                    book.series =bs1
//                                } else {
//                                    bs1 = new Series()
//                                    bs1.isactive = true
//                                    bs1.name = series?.trim()
//                                    bs1.updation_ip_address = ip
//                                    bs1.creation_ip_address = ip
//                                    bs1.updation_date = new Date()
//                                    bs1.creation_date = new Date()
//                                    bs1.updation_username = uid
//                                    bs1.creation_username = uid
//                                    bs1.erpdepartment = erpDepartment
//                                    bs1.organization = user.organization
//                                    bs1.save(flush: true, failOnError: true)
//                                    book.series =bs1
//                                }
//                            }

                            def lang = Language.findByName('English')
                            book.language = lang

                            if (publisher_name) {
                                Publisher publisher = Publisher.findByName(publisher_name)
                                if (publisher) {
                                    book.publisher = publisher
                                } else {
                                    publisher = new Publisher()
                                    publisher.isactive = true
                                    publisher.name = publisher_name?.trim()
                                    publisher.updation_ip_address = ip
                                    publisher.creation_ip_address = ip
                                    publisher.updation_date = new Date()
                                    publisher.creation_date = new Date()
                                    publisher.updation_username = uid
                                    publisher.creation_username = uid
                                    publisher.save(flush: true, failOnError: true)

                                    book.publisher = publisher
                                }
                            }


                            if (department) {
                                LibraryDepartment program = LibraryDepartment.findByNameAndOrganization(department, user?.organization)
                                book.librarydepartment = program
                            }

                            book.organization = user?.organization
                            book.library = user?.organization?.library
                            book.updation_ip_address = ip
                            book.creation_ip_address = ip
                            book.updation_date = new Date()
                            book.creation_date = new Date()
                            book.updation_username = uid
                            book.creation_username = uid
                            book.save(flush: true, failOnError: true)

                            BookItem bookItem = BookItem.findByAccession_numberAndOrganization(accession_no, user.organization)
                            if (bookItem == null) {

                                bookItem = new BookItem()

                                if (year_of_publication)
                                    bookItem.publication_year = year_of_publication

                                try {
                                    if (date_of_publication && date_of_publication != 'Invalid date') {
                                        SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");
                                        Date date1 = formatter.parse(date_of_publication);
                                        bookItem.publication_date = date1
                                    }

                                    if (withdrawal_approval_no_with_date)
                                        bookItem.withdrawal_approval_no_with_date = withdrawal_approval_no_with_date

                                    if (date_of_purchase && date_of_purchase != 'Invalid date') {
                                        SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");
                                        Date date2 = formatter.parse(date_of_purchase);
                                        bookItem.date_of_purchase = date2
                                    }

                                    if (dateofentry && dateofentry != 'Invalid date') {
                                        SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");
                                        Date date3 = formatter.parse(dateofentry);
                                        bookItem.date_of_entry = date3
                                    }

                                } catch (Exception e) {
                                }
                                if (source_vendor)
                                    bookItem.vendor_name = source_vendor
                                if (pages)
                                    bookItem.numberofpages = Integer.parseInt(pages)

                                if (price)
                                    bookItem.price = Double.parseDouble(price)

                                if (net_cost)
                                    bookItem.book_price = Double.parseDouble(net_cost)

                                if (remarks)
                                    bookItem.remarks = remarks

                                if (location)
                                    bookItem.location = location


                                if (ILL == 'Yes')
                                    bookItem.ill = true
                                else
                                    bookItem.ill = false

                                if (discount) {
                                    def discount_per = discount
                                    discount_per = discount_per.replace("%", "");
                                    bookItem.discount_percentage = Double.parseDouble(discount_per)
                                }

                                bookItem.accession_number = accession_no

                                def bs = BookStatus.findByName('Available')
                                bookItem.bookstatus = bs

                                if (key_words)
                                    bookItem.keyword = key_words

                                bookItem.is_received_from_donation = 0
                                bookItem.isactive = true
                                bookItem.book = book

                                if (bill_no)
                                    bookItem.bill_number = bill_no

                                if (place_of_publication)
                                    bookItem.publication_place = place_of_publication

                                def user_org = User.findByUsername(uid)
                                bookItem.organization = user_org?.organization
                                bookItem.updation_ip_address = ip
                                bookItem.creation_ip_address = ip
                                bookItem.updation_date = new Date()
                                bookItem.creation_date = new Date()
                                bookItem.updation_username = uid
                                bookItem.creation_username = uid
                                bookItem.save(flush: true, failOnError: true)

                                println('book.total_books+++++++++' + book.total_books)

                                book.total_books = book.total_books + 1

                                book.updation_ip_address = ip
                                book.updation_date = new Date()
                                book.updation_username = uid
                                book.save(flush: true, failOnError: true)

                                println('book.total_books after+++++++++' + book.total_books)

                            }
                        }

                        hm.put("status", "200")
                    } else {
                        if (isbn) {
                            temp.put('ISBN', isbn?.trim())
                        } else {
                            temp.put('ISBN', 'Missing')
                        }

                        if (accession_no) {
                            temp.put('accession_No', accession_no?.trim())
                        } else {
                            temp.put('accession_No', 'Missing')
                        }

                        temp.put('reason', reason)
                        failList.add(temp)
                    }

                }
            }
            outputStream.flush()
            outputStream.close()
            fs.close()
            file.delete()
            hm.put('failList', failList)
        }
        return hm
    }
    def bulkdelete(request, hm, ip) {
        println(' bulkdelete')
        def uid = request.getHeader("EPS-uid")
        println('---------uid' + uid)
        Part filePart = request.getPart('file')
        InputStream fs = filePart.getInputStream()
        File file = File.createTempFile("s3test", "");
        OutputStream outputStream = new FileOutputStream(file)
        IOUtils.copy(fs, outputStream);
        def reader = new CSVReader(new FileReader(file))
        println('fs----' + fs)

        String[] nextRecord;
        boolean header = true
        ArrayList failList = new ArrayList()
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(uid)
                def headerArray = []
                int i=0
                while ((nextRecord = reader.readNext()) != null) {
                    i++
                    String reason = 'Data is Missing'

                    HashMap temp = new HashMap()
                    if (header) {
                        headerArray = nextRecord
                        header = false
                        continue
                    }

                    def keyindex = 0
                    HashMap tempdata = new HashMap()
                    for (item in headerArray) {
                        tempdata.put(item, nextRecord[keyindex])
                        keyindex++
                    }


                    def accession_no = tempdata?."assection_no"


                    if (accession_no ) {
                        try{
                        BookItem bookItem=BookItem.findByAccession_numberAndOrganization(accession_no,user.organization)
                        if(bookItem==null)
                        {
                            HashMap error =new HashMap()
                            error.put("rowno",i)
                            error.put("error","Book Item Not Found "+accession_no)
                            failList.add(error)
                            continue
                        }

                        BookLending bookLending =BookLending.findByOrganizationAndBookitem(user.organization,bookItem)
                        if(bookLending!=null)
                        {
                            println("deletring book landing")
                            bookLending.delete(flush: true)
                        }
                        UserBookLog userBookLog =UserBookLog.findByBookitem(bookItem)
                        if(userBookLog!=null)
                        {
                            println("deletring bookuser log")
                            userBookLog.delete(flush: true)
                        }
                        println("deletring bookitem")
                        def book=bookItem.book
                            book.total_books=book.total_books-1
                            book.save(flush: true,failOnError: true)
                        bookItem.delete(flush:true)
                        BookItem bookItem1= BookItem.findByBookAndOrganization(book,user.organization)
                        if(bookItem1==null)
                        {
                            println("deletring book")
//                            Book book=Book.findById(bookItem1.book.id)
                            BookReservation bookReservation =BookReservation.findByBook(book)
                            if(bookReservation!=null)
                            {
                                println("deletring book reservation")
                                bookReservation.delete(flush: true)
                            }
                            book.delete(flush:true)
                        }}catch(Exception e)
                        {
                            HashMap error =new HashMap()
                            error.put("rowno",i)
                            error.put("error","Please enter valid accesstion no")
                            failList.add(error)
                            continue
                        }




                    }
                    else {
                        HashMap error =new HashMap()
                        error.put("rowno",i)
                        error.put("error","Please enter valid accesstion no")
                        failList.add(error)
                        continue
                    }

                }
                hm.put("status", "200")
            }
            outputStream.flush()
            outputStream.close()
            fs.close()
            file.delete()
            hm.put('failList', failList)
        }
        return hm
    }

    def importBook1(request, hm, ip,params) {
        println(' in  import booknew :')
        def uid = request.getHeader("EPS-uid")
        println('---------uid' + uid)
        ArrayList failList = new ArrayList()
        ArrayList successList = new ArrayList()
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def file = request.getFile("file")
                println("file")
                println(file)
                int count=0
                if(params.count)
                    count= Integer.parseInt( params.count)
                println("counr")
                println(count)
                def sheetheader = []
                def values = []
                def fileinput=file.getInputStream()
                def workbook = new XSSFWorkbook(fileinput)
                def sheet = workbook.getSheetAt(0)
                for (cell in sheet.getRow(0).cellIterator()) {
                    sheetheader << cell.stringCellValue
                }
                def headerFlag = true
                for (row in sheet.rowIterator()) {
                    if (headerFlag) {
                        headerFlag = false
                        continue
                    }
                    def value=''
                    def map = [:]
//                        int z = 0
                    for (cell in row.cellIterator()) {

                        switch (cell.cellType) {
                            case 1:
                                value = cell.stringCellValue
                                map["${sheetheader[cell.columnIndex]}"] = value
                                break
                            case 0:
                                cell.setCellType(cell.CELL_TYPE_STRING)
                                value = cell.stringCellValue
                                map["${sheetheader[cell.columnIndex]}"] = value
                                break
                            case 2:
                                cell.setCellType(cell.CELL_TYPE_STRING)
                                value = cell.stringCellValue
                                map["${sheetheader[cell.columnIndex]}"] = value
                                break
                            case 3:
                                value = cell.stringCellValue
                                map["${sheetheader[cell.columnIndex]}"] = value
                                break
                            case 4:
                                value = cell.stringCellValue
                                map["${sheetheader[cell.columnIndex]}"] = value
                                break
                        }

                    }

                    if (values.size() > 0) {
                        if (values[0].keySet().size() == map.size())
                            values.add(map)
                    } else {
                        values.add(map)
                    }
                }
                if(count==-1){
                    hm.put("count",count+1)
                    hm.put("successrecord",0)
                    hm.put("isdone",false)
                    hm.put("failrecord",0)
                    hm.put("totalbooksize",values.size())
                    hm.put("status", "200")
                    return
                }
                int x=1
                if(count<values.size()){
                for (int i = count; i < values.size(); i++) {
                        println(i)
                    x=x+1;
                    count=count+1;
                    if(x==1000)
                    {

                        break
                    }

//                    if(count>=values.size())
//                    {
//                        count=count+x
//                        break
//                    }

                        
                        HashMap temp=new HashMap()
                        def accession_no = values[i].get("Accession No.")
                        def isbn = values[i].get("ISBN")
                        def dateofentry = values[i].get("Date of Entry")
                        def series = values[i].get("Series")
                        def title_subtitle = values[i].get("Title - Subtitle")
                        def author = values[i].get("Author")
                        def edition = values[i].get("Edition")
                        def year_of_publication = values[i].get("Year of Publication")
                        def publisher_name = values[i].get("Publisher")
                        def place_of_publication = values[i].get("Place of publication")
                        def date_of_publication = values[i].get("Date of Publication")
                        def date_of_purchase = values[i].get("Date of Purchase")
                        def pages = values[i].get("Pages")
                        def department = values[i].get("Department")
                        def subject = values[i].get("Subject")
                        def physical_details = values[i].get("Physical Details")
                        def accompanying_materials = values[i].get("Accompanying Materials")
                        def key_words = values[i].get("Key Words")
                        def item_types = values[i].get("Item Types")
                        def media_link = values[i].get("Media Link")
                        def classification_no = values[i].get("Classification No.")
                        def source_vendor = values[i].get("Source / Vendor")
                        def bill_no = values[i].get("Bill No.")
                        def price = values[i].get("Price")
                        def discount = values[i].get("Discount (%)")
                        def net_cost = values[i].get("Net Cost Rs.")
                        def withdrawal_approval_no_with_date = values[i].get("Withdrawal Approval No. with date")
                        def location = values[i].get("Location")
                        def ILL = values[i].get("ILL")
                        def remarks = values[i].get("Remarks")
                        def display_order = values[i].get("Display Order")
                        def barcode = values[i].get("Barcode")

                        println("author : "+author)

                        def row = [accession_no	: 	accession_no,
                                   isbn : isbn,
                                   dateofentry   : dateofentry,
                                   series: series,
                                   title_subtitle      : title_subtitle,
                                   author: author,
                                   edition : edition,
                                   year_of_publication: year_of_publication,
                                   publisher_name : publisher_name,
                                   place_of_publication: place_of_publication,
                                   date_of_publication : date_of_publication,
                                   date_of_purchase: date_of_purchase,
                                   pages: pages,
                                   department: department,
                                   subject: subject,
                                   physical_details : physical_details,
                                   accompanying_materials: accompanying_materials,
                                   key_words: key_words,
                                   item_types : item_types,
                                   media_link: media_link,
                                   classification_no: classification_no,
                                   source_vendor: source_vendor,
                                   bill_no : bill_no,
                                   price : price,
                                   discount: discount,
                                   net_cost : net_cost,
                                   withdrawal_approval_no_with_date : withdrawal_approval_no_with_date,
                                   location: location,
                                   ILL : ILL,
                                   remarks: remarks,
                                   display_order:display_order
                        ]


                        def user = User.findByUsername(uid)

                        if (accession_no && isbn) {
                            println('in if all mandatory...')
                            def user_org1 = User.findByUsername(uid)
                            Book book = Book.findByIsbnAndOrganization(isbn, user_org1?.organization)
                            if (book) {
                                println('in if book already in database')

                                BookItem bookItem = BookItem.findByAccession_numberAndOrganization(accession_no, user_org1?.organization)
                                if (bookItem == null) {
                                    bookItem = new BookItem()

                                    if (year_of_publication)
                                        bookItem.publication_year = year_of_publication

                                    if (withdrawal_approval_no_with_date)
                                        bookItem.withdrawal_approval_no_with_date = withdrawal_approval_no_with_date

                                    try {
                                        if (date_of_publication && date_of_publication != 'Invalid date') {
                                            SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");
                                            Date date1 = formatter.parse(date_of_publication);
                                            bookItem.publication_date = date1
                                        }
                                    } catch (Exception e) {
                                        temp.put("rowno",i+2)
                                        temp.put("row",row)
                                        temp.put("error","Date of Publication Is Invalid...")
                                        failList.add(temp)
                                        continue

                                    }

                                    try {
                                        if (date_of_purchase && date_of_purchase != 'Invalid date') {
                                            SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");
                                            Date date2 = formatter.parse(date_of_purchase);
                                            bookItem.date_of_purchase = date2
                                        }
                                    } catch (Exception e) {
                                        temp.put("rowno",i+2)
                                        temp.put("row",row)
                                        temp.put("error","Date of Purchase Is Invalid...")
                                        failList.add(temp)
                                        continue

                                    }

                                    try {
                                        if (dateofentry && dateofentry != 'Invalid date') {
                                            SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");
                                            Date date3 = formatter.parse(dateofentry);
                                            bookItem.date_of_entry = date3
                                        }
                                    } catch (Exception e) {
                                        temp.put("rowno",i+2)
                                        temp.put("row",row)
                                        temp.put("error","Date of Entry Is Invalid...")
                                        failList.add(temp)
                                        continue
                                    }

                                    if (pages)
                                    {
                                        try{
                                            bookItem.numberofpages = Integer.parseInt(pages)
                                        }
                                        catch (Exception e) {
                                            temp.put("rowno",i+2)
                                            temp.put("row",row)
                                            temp.put("error", 'Please Enter No. of Pages...')
                                            failList.add(temp)
                                            continue
                                        }
                                    }


                                    if (price){
                                        try{
                                            bookItem.price = Double.parseDouble(price)
                                        }
                                        catch (Exception e) {
                                            temp.put("rowno",i+2)
                                            temp.put("row",row)
                                            temp.put("error", 'Please Enter Price in Number Only..')
                                            failList.add(temp)
                                            continue
                                        }
                                    }


                                    if (net_cost){
                                        try{
                                            bookItem.book_price = Double.parseDouble(net_cost)
                                        }
                                        catch (Exception e) {
                                            temp.put("rowno",i+2)
                                            temp.put("row",row)
                                            temp.put("error", 'Please Enter Net Cost in Number Only..')
                                            failList.add(temp)
                                            continue
                                        }
                                    }


                                    if (remarks){
                                        try{
                                            bookItem.remarks = remarks
                                        }
                                        catch (Exception e) {
                                            temp.put("rowno",i+2)
                                            temp.put("row",row)
                                            temp.put("error","Please Enter Remark..")
                                            failList.add(temp)
                                            continue
                                        }
                                    }

                                    if (location){
                                        try{
                                            bookItem.location = location
                                        }
                                        catch (Exception e) {
                                            temp.put("rowno",i+2)
                                            temp.put("row",row)
                                            temp.put("error", "Please Enter Location..")
                                            failList.add(temp)
                                            continue
                                        }
                                    }



                                    if (ILL == 'Yes')
                                        bookItem.ill = true
                                    else
                                        bookItem.ill = false

                                    if (discount) {
                                        def discount_per = discount
                                        discount_per = discount_per.replace("%", "");
                                        bookItem.discount_percentage = Double.parseDouble(discount_per)
                                    }
                                    if(barcode)
                                    {
                                        bookItem.barocde = barocde
                                    }
                                    if(display_order)
                                    bookItem.display_order = Long.parseLong(display_order)

                                    bookItem.accession_number = accession_no

                                    def bs = BookStatus.findByName('Available')
                                    bookItem.bookstatus = bs

                                    bookItem.is_received_from_donation = 0
                                    bookItem.isactive = true
                                    bookItem.book = book

                                    if (bill_no)
                                        bookItem.bill_number = bill_no
                                    if (key_words)
                                        bookItem.keyword = key_words
                                    if (place_of_publication)
                                        bookItem.publication_place = place_of_publication

                                    def user_org = User.findByUsername(uid)
                                    bookItem.organization = user_org?.organization

                                    LibraryDepartment program
                                    if (department) {
                                        program = LibraryDepartment.findByNameAndOrganization(department, user?.organization)
                                        if(program!=null)
                                            bookItem.librarydepartment = program
                                    }

                                    if(series){
                                        Series bs1 = Series.findByNameAndOrganization(series?.trim(),user.organization)
                                        if (bs1 != null) {
                                            bookItem.series =bs1
                                        }
                                    }

                                    bookItem.updation_ip_address = ip
                                    bookItem.creation_ip_address = ip
                                    bookItem.updation_date = new Date()
                                    bookItem.creation_date = new Date()
                                    bookItem.updation_username = uid
                                    bookItem.creation_username = uid
                                    bookItem.save(flush: true, failOnError: true)
                                    book.total_books = book.total_books + 1
                                    book.is_considered_in_opac=true
                                    book.updation_ip_address = ip
                                    book.updation_date = new Date()
                                    book.updation_username = uid
                                    book.save(flush: true, failOnError: true)



                                    println('book.total_books after+++++++++' + book.total_books)

                                    temp.put("rowno",i+2)
                                    temp.put("row",row)
                                    successList.add(temp)

                                }
                                else {
                                    println("error found")
                                    temp.put("rowno",i+2)
                                    temp.put("row",row)
                                    temp.put("error","Dublicate asseection no.")
                                    failList.add(temp)
                                    continue
                                }

                            }else {
                                println('in if book save ')

                                book = new Book()
                                book.isbn = isbn
                                book.isactive = true
                                if (title_subtitle)
                                    book.title = title_subtitle

                                if (classification_no)
                                    book.classificationno = classification_no

                                if (media_link)
                                    book.medialink = media_link

                                if (edition)
                                    book.edition = edition


                                if (physical_details) {
                                    PhysicalDetails pd = PhysicalDetails.findByName(physical_details?.trim())
                                    if (pd != null) {
                                        book.physicaldetails = pd
                                    } else {
                                        pd = new PhysicalDetails()
                                        pd.isactive = true
                                        pd.name = physical_details?.trim()
                                        pd.updation_ip_address = ip
                                        pd.creation_ip_address = ip
                                        pd.updation_date = new Date()
                                        pd.creation_date = new Date()
                                        pd.updation_username = uid
                                        pd.creation_username = uid
                                        pd.save(flush: true, failOnError: true)
                                        book.physicaldetails = pd
                                    }
                                }

                                if(series){
                                    Series bs1 = Series.findByNameAndOrganization(series,user.organization)
                                    if (bs1 != null) {
                                        book.series =bs1
                                    }
                                }

                                if (accompanying_materials) {
                                    AccompanyingMaterials amat = AccompanyingMaterials.findByName(accompanying_materials?.trim())
                                    if (amat != null) {
                                        book.accompanyingmaterials = amat
                                    } else {
                                        amat = new AccompanyingMaterials()
                                        amat.isactive = true
                                        amat.name = accompanying_materials?.trim()
                                        amat.updation_ip_address = ip
                                        amat.creation_ip_address = ip
                                        amat.updation_date = new Date()
                                        amat.creation_date = new Date()
                                        amat.updation_username = uid
                                        amat.creation_username = uid
                                        amat.save(flush: true, failOnError: true)
                                        book.accompanyingmaterials = amat
                                    }
                                }





                                if (item_types) {
                                    def bt = BookType.findByNameAndOrganization(item_types, user.organization)
                                    if (bt != null) {
                                        book.booktype = bt
                                    } else {
                                        bt = new BookType()
                                        bt.isactive = true
                                        bt.name = item_types?.trim()
                                        bt.updation_ip_address = ip
                                        bt.creation_ip_address = ip
                                        bt.updation_date = new Date()
                                        bt.creation_date = new Date()
                                        bt.updation_username = uid
                                        bt.creation_username = uid
                                        bt.organization = user.organization
                                        bt.save(flush: true, failOnError: true)

                                        book.booktype = bt
                                    }
                                }


                                def lang = Language.findByName('English')
                                book.language = lang

                                if (publisher_name) {
                                    Publisher publisher = Publisher.findByName(publisher_name)
                                    if (publisher) {
                                        book.publisher = publisher
                                    } else {
                                        publisher = new Publisher()
                                        publisher.isactive = true
                                        publisher.name = publisher_name
                                        publisher.updation_ip_address = ip
                                        publisher.creation_ip_address = ip
                                        publisher.updation_date = new Date()
                                        publisher.creation_date = new Date()
                                        publisher.updation_username = uid
                                        publisher.creation_username = uid
                                        publisher.save(flush: true, failOnError: true)

                                        book.publisher = publisher
                                    }
                                }

                                LibraryDepartment program
                                if (department) {
                                     program = LibraryDepartment.findByNameAndOrganization(department, user?.organization)
                                    if(program!=null)
                                    book.librarydepartment = program
                                }

                                book.organization = user?.organization
                                book.library = user?.organization?.library
                                book.is_considered_in_opac=true
                                book.updation_ip_address = ip
                                book.creation_ip_address = ip
                                book.updation_date = new Date()
                                book.creation_date = new Date()
                                book.updation_username = uid
                                book.creation_username = uid
                                book.save(flush: true, failOnError: true)


                                BookItem bookItem = BookItem.findByAccession_numberAndOrganization(accession_no, user.organization)
                                if (bookItem == null) {

                                    bookItem = new BookItem()

                                    if (year_of_publication)
                                        bookItem.publication_year = year_of_publication

                                    if(barcode)
                                    {
                                        bookItem.barocde = barocde
                                    }

                                    if (withdrawal_approval_no_with_date)
                                        bookItem.withdrawal_approval_no_with_date = withdrawal_approval_no_with_date

                                    try {
                                        if (date_of_publication && date_of_publication != 'Invalid date') {
                                            SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");
                                            Date date1 = formatter.parse(date_of_publication);
                                            bookItem.publication_date = date1
                                        }
                                    } catch (Exception e) {
                                        temp.put("rowno",i+2)
                                        temp.put("row",row)
                                        temp.put("error", "Date of Publication Is Invalid...")
                                        failList.add(temp)
                                        continue
                                    }

                                    try {
                                        if (date_of_purchase && date_of_purchase != 'Invalid date') {
                                            SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");
                                            Date date2 = formatter.parse(date_of_purchase);
                                            bookItem.date_of_purchase = date2
                                        }
                                    } catch (Exception e) {
                                        temp.put("rowno",i+2)
                                        temp.put("row",row)
                                        temp.put("error","Date of Purchase Is Invalid...")
                                        failList.add(temp)
                                        continue
                                    }

                                    try {
                                        if (dateofentry && dateofentry != 'Invalid date') {
                                            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                                            Date date3 = formatter.parse(dateofentry);
                                            bookItem.date_of_entry = date3
                                        }
                                    } catch (Exception e) {
                                        temp.put("rowno",i+2)
                                        temp.put("row",row)
                                        temp.put("error","Date of Entry Is Invalid...")
                                        failList.add(temp)
                                        continue
                                    }



                                    if (pages)
                                    {
                                        try{
                                            bookItem.numberofpages = Integer.parseInt(pages)
                                        }
                                        catch (Exception e) {
                                            temp.put("rowno",i+2)
                                            temp.put("row",row)
                                            temp.put("error",'Please Enter No. of Pages...')
                                            failList.add(temp)
                                            continue
                                        }

                                    }

                                    LibraryDepartment dept
                                    if (department) {
                                        dept = LibraryDepartment.findByNameAndOrganization(department, user?.organization)
                                        if(dept!=null)
                                            bookItem.librarydepartment = dept
                                    }

                                    if (price){
                                        try{
                                            bookItem.price = Double.parseDouble(price)
                                        }
                                        catch (Exception e) {
                                            temp.put("rowno",i+2)
                                            temp.put("row",row)
                                            temp.put("error",'Please Enter Price in Number Only..')
                                            failList.add(temp)
                                            continue
                                        }

                                    }


                                    if (net_cost){
                                        try{
                                            bookItem.book_price = Double.parseDouble(net_cost)
                                        }
                                        catch (Exception e) {
                                            temp.put("rowno",i+2)
                                            temp.put("row",row)
                                            temp.put("error",'Please Enter Net Cost in Number Only..')
                                            failList.add(temp)
                                            continue
                                        }

                                    }


                                    if (remarks){
                                        try{
                                            bookItem.remarks = remarks
                                        }
                                        catch (Exception e) {
                                            temp.put("rowno",i+2)
                                            temp.put("row",row)
                                            temp.put("error","Please Enter Remark..")
                                            failList.add(temp)
                                            continue
                                        }

                                    }


                                    if (location){
                                        try{
                                            bookItem.location = location
                                        }
                                        catch (Exception e) {
                                            temp.put("rowno",i+2)
                                            temp.put("row",row)
                                            temp.put("error", "Please Enter Location..")
                                            failList.add(temp)
                                            continue
                                        }
                                    }

                                    if (subject)
                                        bookItem.subject = subject

                                    if (key_words)
                                        bookItem.keyword = key_words

                                    if(series){
                                        Series bs1 = Series.findByNameAndOrganization(series,user.organization)
                                        if (bs1 != null) {
                                            bookItem.series =bs1
                                        }
                                    }

                                    if (ILL == 'Yes')
                                        bookItem.ill = true
                                    else
                                        bookItem.ill = false

                                    if (discount) {
                                        try {
                                            def discount_per = discount
                                            discount_per = discount_per.replace("%", "");
                                            bookItem.discount_percentage = Double.parseDouble(discount_per)
                                        }
                                        catch (Exception e) {
                                            temp.put("rowno",i+2)
                                            temp.put("row",row)
                                            temp.put("error","Please Enter Discount Percentage...")
                                            failList.add(temp)
                                            continue
                                        }
                                    }

                                    bookItem.accession_number = accession_no

                                    def bs = BookStatus.findByName('Available')
                                    bookItem.bookstatus = bs

                                    if(display_order)
                                        bookItem.display_order = Long.parseLong(display_order)

                                    bookItem.is_received_from_donation = 0
                                    bookItem.isactive = true
                                    bookItem.book = book

                                    if (source_vendor)
                                        bookItem.vendor_name = source_vendor

                                    if (bill_no)
                                        bookItem.bill_number = bill_no

                                    if (place_of_publication)
                                        bookItem.publication_place = place_of_publication

                                    def user_org = User.findByUsername(uid)
                                    bookItem.organization = user_org?.organization
                                    bookItem.updation_ip_address = ip
                                    bookItem.creation_ip_address = ip
                                    bookItem.updation_date = new Date()
                                    bookItem.creation_date = new Date()
                                    bookItem.updation_username = uid
                                    bookItem.creation_username = uid
                                    bookItem.save(flush: true, failOnError: true)

                                    println('book.total_books+++++++++' + book.total_books)

                                    book.total_books = book.total_books + 1

                                    book.updation_ip_address = ip
                                    book.updation_date = new Date()
                                    book.updation_username = uid
                                    book.save(flush: true, failOnError: true)

                                    if(author){
                                        String[] authorArray = author.trim().split(",");
                                        for(int author_i=0;author_i< authorArray.size();author_i++){
                                            println('author_i====='+author_i)
                                            println('authorArray[author_i]======='+authorArray[author_i])
                                            def authorId = Author.findByName(authorArray[author_i])
                                            if(authorId != null){
                                                BooksAuthors bookAuthor=new BooksAuthors()
                                                bookAuthor. creation_username=uid
                                                bookAuthor. updation_username=uid
                                                bookAuthor. creation_date=new Date()
                                                bookAuthor. updation_date=new Date()
                                                bookAuthor. creation_ip_address=ip
                                                bookAuthor. updation_ip_address=ip
                                                bookAuthor. organization=user.organization
                                                bookAuthor.book=book
                                                bookAuthor.author=authorId
                                                bookAuthor.save(flush: true, failOnError: true)
                                            }else{
                                                authorId = new Author()
                                                authorId.isactive = true
                                                authorId.name = authorArray[author_i]
                                                authorId.updation_ip_address = ip
                                                authorId.creation_ip_address = ip
                                                authorId.updation_date = new Date()
                                                authorId.creation_date = new Date()
                                                authorId.updation_username = uid
                                                authorId.creation_username = uid
                                                authorId.save(flush: true, failOnError: true)

                                                BooksAuthors bookAuthor=new BooksAuthors()
                                                bookAuthor. creation_username=uid
                                                bookAuthor. updation_username=uid
                                                bookAuthor. creation_date=new Date()
                                                bookAuthor. updation_date=new Date()
                                                bookAuthor. creation_ip_address=ip
                                                bookAuthor. updation_ip_address=ip
                                                bookAuthor. organization=user.organization
                                                bookAuthor.book=book
                                                bookAuthor.author=authorId
                                                bookAuthor.save(flush: true, failOnError: true)
                                            }
                                        }
                                    }


                                    temp.put("rowno",i+2)
                                    temp.put("row",row)
                                    successList.add(temp)

                                    println('book.total_books after+++++++++' + book.total_books)
                                }
                            }
                            hm.put("status", "200")
                        }
                        else {
                            println("error found")
                            temp.put("rowno",i+2)
                            temp.put("row",row)
                            temp.put("error","Please enter Assection and ISBN NO..")
                            failList.add(temp)
                            continue
                        }

                    }
                }
                if(count>=values.size())
                    hm.put('isdone', true)
                else
                {

                    hm.put('isdone', false)
                }

                hm.put('successList', successList)
                hm.put('failList', failList)
                hm.put("count",count==-1?0:count)
                hm.put("totalbooksize",values.size())
                hm.put("status", "200")
                fileinput.close()
                }

            }

            return hm
        }


        def importIssueBook(request, hm, ip) {
            println(' in  import book:')
            def uid = request.getHeader("EPS-uid")

            Part filePart = request.getPart('file')
            InputStream fs = filePart.getInputStream()
            File file = File.createTempFile("s3test", "");
            OutputStream outputStream = new FileOutputStream(file)
            IOUtils.copy(fs, outputStream);
            def reader = new CSVReader(new FileReader(file))

            String[] nextRecord;
            boolean header = true
            ArrayList failList = new ArrayList()
            if (hm.get("msg") != "401" && hm.get("msg") != "403") {
                Login log = Login.findByUsername(uid)
                Tenants.withId(log.tenantclient.tenantid) {
                    User logeduser = User.findByUsername(log.username)
                    def headerArray = []
                    while ((nextRecord = reader.readNext()) != null) {
                        String reason = 'Data is Missing'

                        HashMap temp = new HashMap()
                        if (header) {
                            headerArray = nextRecord
                            header = false
                            continue
                        }

                        def keyindex = 0
                        HashMap tempdata = new HashMap()
                        for (item in headerArray) {
                            tempdata.put(item, nextRecord[keyindex])
                            keyindex++
                        }

                        def employee_member_code = tempdata?."PRN No./Employee Code"
                        def accession_no = tempdata?."Accession No."
                        def borrowed_date = tempdata?."Borrowed Date "
                        def due_date = tempdata?."Due Date "
                        def remark = tempdata?."Remark"

                        def empployeemember = Employee.findByEmployee_codeAndOrganization(employee_member_code, logeduser.organization)
                        if (!empployeemember) {
                            empployeemember = Member.findByRegistration_numberAndOrganization(employee_member_code, logeduser.organization)
                        }

                        def usr = empployeemember?.user
                        def bi = BookItem.findByAccession_numberAndOrganization(accession_no,logeduser.organization)

                        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                        Date borrowedDate
                        if (borrowed_date && borrowed_date != 'Invalid date') {
                            borrowedDate = formatter.parse(borrowed_date);
                        }

                        Date dueDate
                        if (due_date && due_date != 'Invalid date') {
                            dueDate = formatter.parse(due_date);
                        }
                        else {
                            println dueDate
                        }

                        int flag=1

                        BookLending bookLending1 = BookLending.findByBookitemAndIsactiveAndOrganization(bi,true,logeduser.organization)
                        if(bookLending1==null)
                        {
                            flag=0;
                        }
                        println(usr)
                        println(bi)
                        println(borrowedDate)
                        println(flag)

                        if (usr && bi && borrowedDate && flag==0) {
                            BookLending bookLending = BookLending.findByBookitemAndUserAndBorrowing_dateAndOrganization(bi, usr, borrowedDate,logeduser.organization)
                            if (!bookLending) {
                                bookLending = new BookLending()
                                bookLending.borrowing_date = borrowedDate
                                bookLending.remark = remark

                                if (due_date && due_date != 'Invalid date') {
                                    bookLending.due_date = dueDate
                                    bookLending.return_date = dueDate
                                    bookLending.isactive = false
                                }
                                else {
                                    bookLending.isactive = true
                                }
                                bookLending.fine_amount = 0.0

                                bookLending.creation_username = uid
                                bookLending.updation_username = uid
                                bookLending.creation_date = new Date()
                                bookLending.updation_date = new Date()
                                bookLending.creation_ip_address = ip
                                bookLending.updation_ip_address = ip
                                bookLending.bookitem = bi
                                bookLending.user = usr
                                bookLending.organization = usr?.organization

                                BookStatus bookStatus = BookStatus.findByName("Issued")
                                bi.bookstatus = bookStatus
                                bi.borrowed_date = borrowedDate
                                bi.due_date = dueDate
                                bi.updation_username = uid
                                bi.updation_date = new Date()
                                bi.updation_ip_address = ip
                                bookLending.save(flush: true, failOnError: true)
                                bi.save(flush: true, failOnError: true)

                                ReservationStatus reservationstatus = ReservationStatus.findByName('Completed')
                                BookReservation bookReservation = BookReservation.findByBookAndUserAndIsactive(bi?.book, usr, true)
                                if (bookReservation != null) {
                                    bookReservation.isactive = false
                                    bookReservation.reservationstatus = reservationstatus
                                    bookReservation.updation_username = uid
                                    bookReservation.updation_date = new Date()
                                    bookReservation.updation_ip_address = ip
                                    bookReservation.save(flush: true, failOnError: true)

                                    BookStatus reserved = BookStatus.findByName('Reserved')
                                    BookStatus available = BookStatus.findByName('Available')
                                    def b_item = BookItem.findByBookAndBookstatus(bookReservation?.book, reserved)
                                    if (b_item) {
                                        if (b_item?.bookstatus != bookStatus)
                                            b_item?.bookstatus = available
                                        b_item.updation_username = uid
                                        b_item.updation_date = new Date()
                                        b_item.updation_ip_address = ip
                                        b_item.save(flush: true, failOnError: true)
                                    }
                                }

                                // maintain userbooklog renew count set to 1 if member issue book
                                UserBookLog userBookLog = UserBookLog.findByUserAndBookitem(usr, bi)
                                if (!userBookLog) {
                                    userBookLog = new UserBookLog()

                                    userBookLog.renew_counter = 1
                                    userBookLog.creation_username = uid
                                    userBookLog.updation_username = uid
                                    userBookLog.creation_date = new Date()
                                    userBookLog.updation_date = new Date()
                                    userBookLog.creation_ip_address = ip
                                    userBookLog.updation_ip_address = ip
                                    userBookLog.user = usr
                                    userBookLog.bookitem = bi
                                    userBookLog.save(flush: true, failOnError: true)
                                } else {
                                    userBookLog.renew_counter = 1
                                    userBookLog.updation_username = uid
                                    userBookLog.user = usr
                                    userBookLog.updation_date = new Date()
                                    userBookLog.updation_ip_address = ip
                                    userBookLog.save(flush: true, failOnError: true)
                                }
                            } else {
                                if (bookLending?.isactive) {
                                    BookStatus bookStatus = BookStatus.findByName("Issued")
                                    bi.bookstatus = bookStatus
                                    bi.borrowed_date = borrowedDate
                                    bi.due_date = dueDate

                                    bi.updation_username = uid
                                    bi.updation_date = new Date()
                                    bi.updation_ip_address = ip

                                    bookLending.remark = remark
                                    bookLending.updation_username = uid
                                    bookLending.updation_date = new Date()
                                    bookLending.updation_ip_address = ip
                                    bookLending.isactive = true
                                    bookLending.due_date = dueDate
                                    bookLending.save(flush: true, failOnError: true)
                                    bi.save(flush: true, failOnError: true)
                                }else{
                                    BookStatus bookStatus = BookStatus.findByName("Available")
                                    bi.bookstatus = bookStatus
                                    bi.borrowed_date = borrowedDate
                                    bi.due_date = dueDate

                                    bi.updation_username = uid
                                    bi.updation_date = new Date()
                                    bi.updation_ip_address = ip

                                    bookLending.remark = remark
                                    bookLending.updation_username = uid
                                    bookLending.updation_date = new Date()
                                    bookLending.updation_ip_address = ip
                                    bookLending.isactive = true
                                    bookLending.due_date = dueDate
                                    bookLending.save(flush: true, failOnError: true)
                                    bi.save(flush: true, failOnError: true)
                                }
                            }
                            hm.put("status", "200")
                        } else
                        {
                            if (employee_member_code) {
                                temp.put('Member_Code', employee_member_code?.trim())
                            } else {
                                temp.put('Member_Code', 'Missing')
                            }

                            if (accession_no) {
                                temp.put('accession_No', accession_no?.trim())
                            } else {
                                temp.put('accession_No', 'Missing')
                            }

                            if (borrowed_date) {
                                temp.put('Borrowed_Date', borrowed_date?.trim())
                            } else {
                                temp.put('Borrowed_Date', 'Missing')
                            }

                            if (!usr)
                                reason = "Employee/Member Not Found !!"
                            if (!bi)
                                reason = "Employee/Member and Book With Given Accession No Not Found !!"

                            if (!usr && !bi)
                                reason = "Book With Given Accession No Not Found !!"
                            if(flag==1)
                            {
                                reason = "book already issue"
                            }
                            println(reason)
                            temp.put('reason', reason)
                            failList.add(temp)
                        }
                    }
                }
                outputStream.flush()
                outputStream.close()
                fs.close()
                file.delete()
                hm.put('failList', failList)
            }
            return hm
        }

    def importIssueBook1(request, hm, ip) {
        println(' in  import book:')
        def uid = request.getHeader("EPS-uid")

        Part filePart = request.getPart('file')
        InputStream fs = filePart.getInputStream()
        File file = File.createTempFile("s3test", "");
        OutputStream outputStream = new FileOutputStream(file)
        IOUtils.copy(fs, outputStream);
        def reader = new CSVReader(new FileReader(file))

        String[] nextRecord;
        boolean header = true
        ArrayList failList = new ArrayList()
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                User logeduser = User.findByUsername(log.username)
                def headerArray = []
                BookStatus bookStatusIssued = BookStatus.findByName("Issued")
                BookStatus bookStatusAvailable = BookStatus.findByName("Available")
                while ((nextRecord = reader.readNext()) != null) {
                    String reason = 'Data is Missing'

                    HashMap temp = new HashMap()
                    if (header) {
                        headerArray = nextRecord
                        header = false
                        continue
                    }

                    def keyindex = 0
                    HashMap tempdata = new HashMap()
                    for (item in headerArray) {
                        tempdata.put(item, nextRecord[keyindex])
                        keyindex++
                    }

                    def employee_member_code = tempdata?."PRN No./Employee Code"
                    def accession_no = tempdata?."Accession No."
                    def borrowed_date = tempdata?."Borrowed Date "
                    def due_date = tempdata?."Due Date "
                    def remark = tempdata?."Remark"

                    def empployeemember = Employee.findByEmployee_codeAndOrganization(employee_member_code, logeduser.organization)
                    if (!empployeemember) {
                        empployeemember = Member.findByRegistration_numberAndOrganization(employee_member_code, logeduser.organization)
                    }

                    def usr = empployeemember?.user
                    def bi = BookItem.findByAccession_numberAndOrganization(accession_no,logeduser.organization)

                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                    Date borrowedDate
                    if (borrowed_date && borrowed_date != 'Invalid date') {

                        Calendar c = Calendar.getInstance();
                        c.setTime(formatter.parse(borrowed_date))
                        c.add(Calendar.DATE, 1);  // number of days to add
                        borrowedDate = c.getTime()
                    }

                    Date dueDate
                    if (due_date && due_date != 'Invalid date') {
                        Calendar c = Calendar.getInstance();
                        c.setTime(formatter.parse(due_date))
                        c.add(Calendar.DATE, 1);  // number of days to add
                        dueDate = c.getTime()
                    }
                    else {
                        println dueDate
                    }


                    println(usr)
                    println(bi)
                    println(borrowedDate)


                    if (usr && bi && borrowedDate ) {
                        BookLending bookLending = BookLending.findByBookitemAndUserAndBorrowing_dateAndOrganization(bi, usr, borrowedDate,logeduser.organization)
                        if (!bookLending) {
                            bookLending = new BookLending()
                            bookLending.borrowing_date = borrowedDate
                            bookLending.remark = remark

                            if (due_date && due_date != 'Invalid date') {
                                bookLending.due_date = dueDate
                                bookLending.return_date = dueDate
                                bookLending.isactive = false
                                bi.bookstatus = bookStatusAvailable
                                bi.due_date = dueDate
                            }
                            else {
                                bookLending.isactive = true
                                bi.bookstatus = bookStatusIssued
                            }
                            bookLending.fine_amount = 0.0

                            bookLending.creation_username = uid
                            bookLending.updation_username = uid
                            bookLending.creation_date = new Date()
                            bookLending.updation_date = new Date()
                            bookLending.creation_ip_address = ip
                            bookLending.updation_ip_address = ip
                            bookLending.bookitem = bi
                            bookLending.user = usr
                            bookLending.organization = usr?.organization


                            bi.borrowed_date = borrowedDate

                            bi.updation_username = uid
                            bi.updation_date = new Date()
                            bi.updation_ip_address = ip
                            bookLending.save(flush: true, failOnError: true)
                            bi.save(flush: true, failOnError: true)


                            // maintain userbooklog renew count set to 1 if member issue book
                            UserBookLog userBookLog = UserBookLog.findByUserAndBookitem(usr, bi)
                            if (!userBookLog) {
                                userBookLog = new UserBookLog()

                                userBookLog.renew_counter = 1
                                userBookLog.creation_username = uid
                                userBookLog.updation_username = uid
                                userBookLog.creation_date = new Date()
                                userBookLog.updation_date = new Date()
                                userBookLog.creation_ip_address = ip
                                userBookLog.updation_ip_address = ip
                                userBookLog.user = usr
                                userBookLog.bookitem = bi
                                userBookLog.save(flush: true, failOnError: true)
                            } else {
                                userBookLog.renew_counter = 1
                                userBookLog.updation_username = uid
                                userBookLog.user = usr
                                userBookLog.updation_date = new Date()
                                userBookLog.updation_ip_address = ip
                                userBookLog.save(flush: true, failOnError: true)
                            }
                        }
                        else {
                            if (bookLending?.isactive) {

                                bi.bookstatus = bookStatusIssued
                                bi.borrowed_date = borrowedDate
                                if (due_date && due_date != 'Invalid date') {
                                    bi.due_date = dueDate
                                    bookLending.due_date = dueDate
                                    bookLending.return_date = dueDate
                                }
                                bi.updation_username = uid
                                bi.updation_date = new Date()
                                bi.updation_ip_address = ip

                                bookLending.remark = remark
                                bookLending.updation_username = uid
                                bookLending.updation_date = new Date()
                                bookLending.updation_ip_address = ip
                                bookLending.isactive = true

                                bookLending.save(flush: true, failOnError: true)
                                bi.save(flush: true, failOnError: true)

                            }else{

                                bi.bookstatus = bookStatusAvailable
                                bi.borrowed_date = borrowedDate

                                if (due_date && due_date != 'Invalid date') {
                                    bi.due_date = dueDate

                                    bookLending.due_date = dueDate
                            }
                                bi.updation_username = uid
                                bi.updation_date = new Date()
                                bi.updation_ip_address = ip

                                bookLending.remark = remark
                                bookLending.updation_username = uid
                                bookLending.updation_date = new Date()
                                bookLending.updation_ip_address = ip
                                bookLending.isactive = false

                                bookLending.save(flush: true, failOnError: true)
                                bi.save(flush: true, failOnError: true)
                            }
                        }
                        hm.put("status", "200")
                    } else
                    {
                        if (employee_member_code) {
                            temp.put('Member_Code', employee_member_code?.trim())
                        } else {
                            temp.put('Member_Code', 'Missing')
                        }

                        if (accession_no) {
                            temp.put('accession_No', accession_no?.trim())
                        } else {
                            temp.put('accession_No', 'Missing')
                        }

                        if (borrowed_date) {
                            temp.put('Borrowed_Date', borrowed_date?.trim())
                        } else {
                            temp.put('Borrowed_Date', 'Missing')
                        }

                        if (!usr)
                            reason = "Employee/Member Not Found !!"
                        if (!bi)
                            reason = "Employee/Member and Book With Given Accession No Not Found !!"

                        if (!usr && !bi)
                            reason = "Book With Given Accession No Not Found !!"


                        temp.put('reason', reason)
                        failList.add(temp)
                    }
                }
            }
            outputStream.flush()
            outputStream.close()
            fs.close()
            file.delete()
            hm.put('failList', failList)
        }
        return hm
    }
    }

