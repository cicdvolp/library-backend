package library
import grails.gorm.MultiTenant
class Program implements MultiTenant<Program>{
    String name
    int display_order
    String short_name
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[programType:ProgramType,organization:Organization,erpdepartment:ERPDepartment]
    static constraints = {
        short_name nullable: true
        erpdepartment nullable: true
    }
    static mapping = {
        display_order defaultValue: 0
    }
}
