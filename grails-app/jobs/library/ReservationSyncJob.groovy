package library

import grails.gorm.multitenancy.Tenants

class ReservationSyncJob {
    static triggers = {
//      simple repeatInterval: 5000l // execute job once in 5 seconds
//        simple name: 'mySimpleTrigger', startDelay: 5000, repeatInterval: 5000
    }

    def group = "MyGroup"
    def description = "Example job with Simple Trigger"
    def execute(){
        print "Job run!"

        println(System.getProperty("os.name").toLowerCase());
        if(System.getProperty("os.name").toLowerCase()=='windows 10')
            println("yes")
        else
            println("no")
//        def uid = request.getHeader("EPS-uid")
//        Login log = Login.findByUsername(uid)
//        Tenants.withId(log.tenantclient.tenantid) {
//            def index_config = IndexConfig.list()
//            def isReservationSink = index_config[0].isReservationSink
//            index_config[0].isReservationSink = false
//            index_config[0].save(flush: true, failOnError: true)
//        }
    }
}
