package library
import grails.gorm.MultiTenant
class BookReservation implements MultiTenant<BookReservation>
{
    Date reservation_date
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[user:User,book:Book,reservationstatus:ReservationStatus]
    static mapping = {
        isactive defaultValue: true
    }
}
