package library

class Login
{
    String username
    String password
    String grno_empid
    boolean isloginblocked

    String token
    Date password_update_date
    Date lastlogindate
    Date tokenexpiry
    String logindevice

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[tenantclient:TenantClient]
    static constraints = {
        password nullable: false, blank: false, password: true
        username nullable: false, blank: false, unique: true
        lastlogindate nullable: true
        grno_empid nullable: true
        tokenexpiry nullable: true
        logindevice nullable: true
        token nullable: true
        password_update_date nullable: true
    }

    static mapping = {
        password column: '`password`'
        token type:'text'
        isloginblocked defaultValue: false
    }
}
