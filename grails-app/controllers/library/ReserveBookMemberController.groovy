package library

import grails.converters.JSON

class ReserveBookMemberController {

    def index() { }

    def reserveBookInit()
    {
        println " reserveBookInit"
        HashMap hm = new HashMap()
        ReserveBookMemberService reserveBookService = new ReserveBookMemberService()
        String ip = request.getRemoteAddr()
        reserveBookService.reserveBookInit(request, hm,ip)
        render hm as JSON
    }

    def reserveBook()
    {
        println " reserve Book"
        HashMap hm = new HashMap()
        ReserveBookMemberService reserveBookService = new ReserveBookMemberService()
        String ip = request.getRemoteAddr()
        reserveBookService.reserveBook(request, hm,ip)
        render hm as JSON
    }


}
