package library
import grails.gorm.MultiTenant

class BookVendor implements MultiTenant<BookVendor>
{

    String name
    int display_order
    String short_name
    String description
    String place
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[bookvendortype:BookVendorType]
    static constraints = {
        short_name nullable: true
        description nullable: true
        place nullable: true
    }
    static mapping = {
        display_order defaultValue: 0
    }
}
