package library
import grails.gorm.MultiTenant
class BookDonationRequest implements MultiTenant<BookDonationRequest>
{
    int number_of_copies
    Date request_date
    Date expected_delivery_date
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[bookcondition:BookCondition,user:User,organization:Organization]
    static mapping = {
        isactive defaultValue: true
    }
}
