package library


import grails.gorm.transactions.Transactional
import grails.converters.JSON//sending
import groovy.json.JsonSlurper//receiving
import grails.gorm.multitenancy.*
@Transactional
class SuperAdminPushMastersService {

    def serviceMethod() {
    }


    def pushAllMasters(org,uid,request){
        println("org ::"+org)
        println("uid ::"+uid)
      //  println("request ::"+request)
        ArrayList list = new ArrayList()
        HashMap pushed = new HashMap()
        boolean done=false
        //if(org!=null) {
            // 1-Copy Ueser Type
            def user_type_list = UserTypeSuperMaster.list()
            for( type in user_type_list){
                //UserType userType = UserType.findByOrganizationAndName(org,type?.name)
                UserType userType = UserType.findByName(type?.name)
                if(userType==null){
                    userType = new UserType()
                    userType.name = type.name
                    userType.isactive = true
                    //userType.organization = org
                    userType.creation_username = uid
                    userType.updation_username = uid
                    userType.creation_date = new java.util.Date()
                    userType.updation_date = new java.util.Date()
                    userType.creation_ip_address = request.getRemoteAddr()
                    userType.updation_ip_address = request.getRemoteAddr()
                    userType.save(failOnError: true, flush: true)
                    done=true
                }else {
                    done=true
                }
            }
            pushed = new HashMap()
            pushed.put('super_master_name','User Type Super Master')
            pushed.put('master_name','User Type')
            pushed.put('status',done)
            list.add(pushed)
            done=false

            // 2-Copy Role
            def role_list = RoleSuperMaster.list()
            for( rl in role_list){
                //Role role = Role.findByOrganizationAndName(org,rl?.name)
                Role role = Role.findByName(rl?.name)
                if(role==null){
                    role = new Role()
                    role.name = rl.name
                    role.isactive = true
                    //role.organization = org
                    if(rl.name == "Member")
                    {
                        role.usertype = UserType.findByName("Member")
                    }
                    else{
                        role.usertype = UserType.findByName("Employee")
                    }
                    role.creation_username = uid
                    role.updation_username = uid
                    role.creation_date = new java.util.Date()
                    role.updation_date = new java.util.Date()
                    role.creation_ip_address = request.getRemoteAddr()
                    role.updation_ip_address = request.getRemoteAddr()
                    role.save(failOnError: true, flush: true)
                    done=true
                }else {
                    done=true
                }
            }
            pushed = new HashMap()
            pushed.put('super_master_name','Role Super Master')
            pushed.put('master_name','Role')
            pushed.put('status',done)
            list.add(pushed)
            done=false

            // 3-Copy Ueser Type
            def bookStatusall = BookStatusSuperMaster.list()
            for( type in bookStatusall){
                BookStatus bookStatus = BookStatus.findByName(type?.name)
                if(bookStatus==null){
                    bookStatus = new BookStatus()
                    bookStatus.name = type.name
                    bookStatus.displayname = type.displayname
                    bookStatus.isactive = true
                    bookStatus.creation_username = uid
                    bookStatus.updation_username = uid
                    bookStatus.creation_date = new java.util.Date()
                    bookStatus.updation_date = new java.util.Date()
                    bookStatus.creation_ip_address = request.getRemoteAddr()
                    bookStatus.updation_ip_address = request.getRemoteAddr()
                    bookStatus.save(failOnError: true, flush: true)
                    done=true
                }else {
                    done=true
                }
            }
            pushed = new HashMap()
            pushed.put('super_master_name','Book Status Super Master')
            pushed.put('master_name','Book Status')
            pushed.put('status',done)
            list.add(pushed)
            done=false

            // 4-Copy Reservation Status
            def reservationStatusSuperMaster = ReservationStatusSuperMaster.list()
            for( type in reservationStatusSuperMaster){
                ReservationStatus reservationStatus = ReservationStatus.findByName(type?.name)
                if(reservationStatus==null){
                    reservationStatus = new ReservationStatus()
                    reservationStatus.name = type.name
                    reservationStatus.isactive = true
                    reservationStatus.creation_username = uid
                    reservationStatus.updation_username = uid
                    reservationStatus.creation_date = new java.util.Date()
                    reservationStatus.updation_date = new java.util.Date()
                    reservationStatus.creation_ip_address = request.getRemoteAddr()
                    reservationStatus.updation_ip_address = request.getRemoteAddr()
                    reservationStatus.save(failOnError: true, flush: true)
                    done=true
                }else {
                    done=true
                }
            }
            pushed = new HashMap()
            pushed.put('super_master_name','Reservation Status Super Master')
            pushed.put('master_name','Reservation Status')
            pushed.put('status',done)
            list.add(pushed)
            done=false

            // 5-Copy Account Status
            def accountStatusSuperMaster = AccountStatusSuperMaster.list()
            for( type in accountStatusSuperMaster){
                AccountStatus accountStatus = AccountStatus.findByName(type?.name)
                if(accountStatus==null){
                    accountStatus = new AccountStatus()
                    accountStatus.name = type.name
                    accountStatus.isactive = true
                    accountStatus.creation_username = uid
                    accountStatus.updation_username = uid
                    accountStatus.creation_date = new java.util.Date()
                    accountStatus.updation_date = new java.util.Date()
                    accountStatus.creation_ip_address = request.getRemoteAddr()
                    accountStatus.updation_ip_address = request.getRemoteAddr()
                    accountStatus.save(failOnError: true, flush: true)
                    done=true
                }else {
                    done=true
                }
            }
            pushed = new HashMap()
            pushed.put('super_master_name','Account Status Super Master')
            pushed.put('master_name','Account Status')
            pushed.put('status',done)
            list.add(pushed)
            done=false

            // 6-Copy Transaction Type
            def transactionTypeSuperMaster = TransactionTypeSuperMaster.list()
            for( type in transactionTypeSuperMaster){
                TransactionType transactionType = TransactionType.findByName(type?.name)
                if(transactionType==null){
                    transactionType = new TransactionType()
                    transactionType.name = type.name
                    transactionType.isactive = true
                    transactionType.creation_username = uid
                    transactionType.updation_username = uid
                    transactionType.creation_date = new java.util.Date()
                    transactionType.updation_date = new java.util.Date()
                    transactionType.creation_ip_address = request.getRemoteAddr()
                    transactionType.updation_ip_address = request.getRemoteAddr()
                    transactionType.save(failOnError: true, flush: true)
                    done=true
                }else {
                    done=true
                }
            }
            pushed = new HashMap()
            pushed.put('super_master_name','Transaction Type Super Master')
            pushed.put('master_name','Transaction Type')
            pushed.put('status',done)
            list.add(pushed)
            done=false

            // 7-Copy Notification Method
            def notificationMethodSuperMaster = NotificationMethodSuperMaster.list()
            for( type in notificationMethodSuperMaster){
                NotificationMethod notificationMethod = NotificationMethod.findByName(type?.name)
                if(notificationMethod==null){
                    notificationMethod = new NotificationMethod()
                    notificationMethod.name = type.name
                    notificationMethod.isactive = true
                    notificationMethod.creation_username = uid
                    notificationMethod.updation_username = uid
                    notificationMethod.creation_date = new java.util.Date()
                    notificationMethod.updation_date = new java.util.Date()
                    notificationMethod.creation_ip_address = request.getRemoteAddr()
                    notificationMethod.updation_ip_address = request.getRemoteAddr()
                    notificationMethod.save(failOnError: true, flush: true)
                    done=true
                }else {
                    done=true
                }
            }
            pushed = new HashMap()
            pushed.put('super_master_name','Notification Method Super Master')
            pushed.put('master_name','Notification Method')
            pushed.put('status',done)
            list.add(pushed)
            done=false

            // 8-Copy Notification Method
            def notificationTypeSuperMaster = NotificationTypeSuperMaster.list()
            for( type in notificationTypeSuperMaster){
                NotificationType notificationType = NotificationType.findByName(type?.name)
                if(notificationType==null){
                    notificationType = new NotificationType()
                    notificationType.name = type.name
                    notificationType.isactive = true
                    notificationType.creation_username = uid
                    notificationType.updation_username = uid
                    notificationType.creation_date = new java.util.Date()
                    notificationType.updation_date = new java.util.Date()
                    notificationType.creation_ip_address = request.getRemoteAddr()
                    notificationType.updation_ip_address = request.getRemoteAddr()
                    notificationType.save(failOnError: true, flush: true)
                    done=true
                }else {
                    done=true
                }
            }
            pushed = new HashMap()
            pushed.put('super_master_name','Notification Type Super Master')
            pushed.put('master_name','Notification Type')
            pushed.put('status',done)
            list.add(pushed)
            done=false

            // 9-Copy Notification Method
            def libraryConfigurationSuperMaster = LibraryConfigurationSuperMaster.list()
            for( type in libraryConfigurationSuperMaster){
                LibraryConfiguration libraryConfiguration = LibraryConfiguration.findByName(type?.name)
                if(libraryConfiguration==null){
                    libraryConfiguration = new LibraryConfiguration()
                    libraryConfiguration.name = type.name
                    libraryConfiguration.value = type.value
                    libraryConfiguration.isactive = true
                    libraryConfiguration.creation_username = uid
                    libraryConfiguration.updation_username = uid
                    libraryConfiguration.creation_date = new java.util.Date()
                    libraryConfiguration.updation_date = new java.util.Date()
                    libraryConfiguration.creation_ip_address = request.getRemoteAddr()
                    libraryConfiguration.updation_ip_address = request.getRemoteAddr()
                    libraryConfiguration.save(failOnError: true, flush: true)
                    done=true
                }else {
                    done=true
                }
            }
            pushed = new HashMap()
            pushed.put('super_master_name','Library Configuration Super Master')
            pushed.put('master_name','Library Configuration')
            pushed.put('status',done)
            list.add(pushed)
            done=false
            // 10-Copy Link Mastre
            def  linkSuperMaster = LinkSuperMaster.list()
            for( type in linkSuperMaster){
                RoleLinks roleLinks = RoleLinks.findByLinkname(type?.action_name)
                if(roleLinks==null){
                    roleLinks = new RoleLinks()
                    roleLinks.linkname = type?.action_name
                    roleLinks.sort_order = type?.sort_order
                    roleLinks.controller_name = type?.controller_name
                    roleLinks.action_name = type?.action_name
                    roleLinks.icon = type?.icon
                    roleLinks.link_display_name = type?.link_display_name
                    roleLinks.isactive = true
                    roleLinks.creation_username = uid
                    roleLinks.updation_username = uid
                    roleLinks.creation_date = new java.util.Date()
                    roleLinks.updation_date = new java.util.Date()
                    roleLinks.creation_ip_address = request.getRemoteAddr()
                    roleLinks.updation_ip_address = request.getRemoteAddr()
                    RoleSuperMaster super_role=RoleSuperMaster.findById(type?.rolesupermaster?.id)
                    Role role=Role.findByName(super_role?.name)
                    roleLinks.role = role
                    UserTypeSuperMaster super_ut=UserTypeSuperMaster.findById(type?.usertypesupermaster?.id)
                    UserType userType=UserType.findByName(super_ut?.name)
                    roleLinks.usertype = userType
                    roleLinks.save(failOnError: true, flush: true)
                    done=true
                }else {
                    done=true
                }
            }
            pushed = new HashMap()
            pushed.put('super_master_name','Link Master')
            pushed.put('master_name','Role Link')
            pushed.put('status',done)
            list.add(pushed)
            done=false
        //}
        return list
    }

    /*def getPushMastersStatus(org){
        ArrayList list = new ArrayList()
        if(org!=null) {
            //1
            HashMap temp = new HashMap()
            def com_mode_list = CommunicationModeSuperMaster.list()
            temp.put("name", "CommunicationMode")
            temp.put("super", com_mode_list.size())
            temp.put("master", CommunicationMode.findAllByOrganization(org).size())
            list.add(temp)

            temp = new HashMap()
            def course_auth_operation = CourseAuthorityOperationSuperMaster.list()
            temp.put("name","CourseAuthorityOperation")
            temp.put("super",course_auth_operation.size())
            temp.put("master",CourseAuthorityOperation.findAllByOrganization(org).size())
            list.add(temp)

            temp = new HashMap()
            def exam_configuration_list = ExamConfigurationSuperMaster.list()
            temp.put("name","ExamConfiguration")
            temp.put("super",exam_configuration_list.size())
            temp.put("master",ExamConfiguration.findAllByOrganization(org).size())
            list.add(temp)

            temp = new HashMap()
            def exam_environment_type_list = ExamEnviornmentTypeSuperMaster.list()
            temp.put("name","ExamEnviornmentType")
            temp.put("super",exam_environment_type_list.size())
            temp.put("master",ExamEnviornmentType.findAllByOrganization(org).size())
            list.add(temp)

            temp = new HashMap()
            def exam_status_list = ExamStatusSuperMaster.list()
            temp.put("name","ExamStatus")
            temp.put("super",exam_status_list.size())
            temp.put("master",ExamStatus.findAllByOrganization(org).size())
            list.add(temp)

            temp = new HashMap()
            def exam_type_list = ExamTypeSuperMaster.list()
            temp.put("name","ExamType")
            temp.put("super",exam_type_list.size())
            temp.put("master",ExamType.findAllByOrganization(org).size())
            list.add(temp)

            temp = new HashMap()
            def language_list = LanguageSuperMaster.list()
            temp.put("name","Language")
            temp.put("super",language_list.size())
            temp.put("master",Language.findAllByOrganization(org).size())
            list.add(temp)

            temp = new HashMap()
            def lower_sec_mode_list = LowerSecurityModeOptionsSuperMaster.list()
            temp.put("name","LowerSecurityModeOptions")
            temp.put("super",lower_sec_mode_list.size())
            temp.put("master",LowerSecurityModeOptions.findAllByOrganization(org).size())
            list.add(temp)

            temp = new HashMap()
            def proctor_status_list = ProctoringStatusSuperMaster.list()
            temp.put("name","ProctoringStatus")
            temp.put("super",proctor_status_list.size())
            temp.put("master",ProctoringStatus.findAllByOrganization(org).size())
            list.add(temp)

            temp = new HashMap()
            def proctor_type_list = ProctoringTypeSuperMaster.list()
            temp.put("name","ProctoringType")
            temp.put("super",proctor_type_list.size())
            temp.put("master",ProctoringType.findAllByOrganization(org).size())
            list.add(temp)

            temp = new HashMap()
            def question_type_list = QuestionTypeSuperMaster.list()
            temp.put("name","QuestionType")
            temp.put("super",question_type_list.size())
            temp.put("master",QuestionType.findAllByOrganization(org).size())
            list.add(temp)

            temp = new HashMap()
            def answer_status_list = StudentAnsweredStatusSuperMaster.list()
            temp.put("name","StudentAnsweredStatus")
            temp.put("super",answer_status_list.size())
            temp.put("master",StudentAnsweredStatus.findAllByOrganization(org).size())
            list.add(temp)

            temp = new HashMap()
            def stud_attr_list = StudentAttributeSuperMaster.list()
            temp.put("name","StudentAttribute")
            temp.put("super",stud_attr_list.size())
            temp.put("master",StudentAttribute.findAllByOrganization(org).size())
            list.add(temp)

            temp = new HashMap()
            def sunject_tools_list = SubjectToolSuperMaster.list()
            temp.put("name","SubjectTool")
            temp.put("super",sunject_tools_list.size())
            temp.put("master",SubjectTool.findAllByOrganization(org).size())
            list.add(temp)

            temp = new HashMap()
            def user_type_list = UserTypeSuperMaster.list()
            temp.put("name","UserType")
            temp.put("super",user_type_list.size())
            temp.put("master",UserType.findAllByOrganization(org).size())
            list.add(temp)

            temp = new HashMap()
            def omr_list = OMRSetSuperMaster.list()
            temp.put("name","OMRSet")
            temp.put("super",omr_list.size())
            temp.put("master",OMRSet.findAllByOrganization(org).size())
            list.add(temp)

            temp = new HashMap()
            def role_list = RoleSuperMaster.list()
            temp.put("name","Role")
            temp.put("super",role_list.size())
            temp.put("master",Role.findAllByOrganization(org).size())
            list.add(temp)


        }
        return list
    }*/


}
