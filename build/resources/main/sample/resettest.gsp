<table id="table_id" class="display">
    <thead>
        <tr>
            <th>LQFS-ID</th>
            <th>Learner</th>
            <th>Course</th>
            <th>Test</th>
            <th>Start Time</th>
            <th>End Time</th>
        <tr>
    </thead>
    <tbody>
        <g:each in="${lqfs}" var="sub">
            <tr>
                <td>${sub?.id}</td>
                <td>${sub?.learner?.person?.firstName} ${sub?.learner?.person?.lastName}</td>
                <td>${sub?.courseoffering?.course?.course_name}</td>
                <td>${sub?.quizoffering?.title}</td>
                <td><g:formatDate format="dd-MM-yyyy hh:mm:ss" date="${sub?.start_date}"/></td>
                <td>
                    <g:form action="saveTime">
                        <input type="text" value="${sub?.id}" name="lqfsid">
                        <g:formatDate format="dd-MM-yyyy hh:mm:ss" date="${sub?.end_date}"/>
                        <input type="submit" value="Save" />
                    </g:form>
                </td>
            </tr>
        </g:each>
    </tbody>
</table>