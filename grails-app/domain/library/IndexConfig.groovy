package library

import grails.gorm.MultiTenant

class IndexConfig implements MultiTenant<IndexConfig>
{
    int last_accession_number
    int receipt_no
    boolean isReservationSink

    static constraints = {
    }
    static mapping = {
        isReservationSink defaultValue: false
    }
}
