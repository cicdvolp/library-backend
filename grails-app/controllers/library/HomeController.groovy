package library
import grails.gorm.multitenancy.*
import grails.converters.JSON
import groovy.json.JsonSlurper

class HomeController {

    def index() { }
    //find different role of the login user.
    def dashboardsidebar(){

        println("in dashboardsidebar...")
        def token = request.getHeader("EPS-token")//"123"//
        def uid =  request.getHeader("EPS-uid")//"admin@veis.com"//
        String url = request.getHeader("router-path")
        println "url:"+url
        AuthService auth = new AuthService()
        HashMap hm = new HashMap()
        ArrayList app_modules = new ArrayList()
        String purpose = "Enter dashboard showing roles."
        auth.checkauth(token,uid,hm,url,request,purpose)//401;200;token-refreshed ->msg
        if(hm.get("msg")!="401" && hm.get("msg")!="403"){
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def roles = user?.role
                ArrayList list = new ArrayList()
                for(role in roles) {
                    HashMap x = new HashMap()
                    x.put('name', role?.name)
                    //x.put('icon', role?.icon)
                    x.put('model', false)
                    list.add(x)
                }
                list.sort()
               // println list
                hm.put("links",list)
                render hm as JSON
                return
            }
        }
        render hm as JSON
    }
    def getusername(){

        println("in getusername...")
        def token = request.getHeader("EPS-token")//"123"//
        def uid =  request.getHeader("EPS-uid")//"admin@veis.com"//
        String url = request.getHeader("router-path")
        println "url:"+url
        AuthService auth = new AuthService()
        HashMap hm = new HashMap()
        ArrayList app_modules = new ArrayList()
        String purpose = "Enter dashboard showing roles."
        auth.checkauth(token,uid,hm,url,request,purpose)//401;200;token-refreshed ->msg
        if(hm.get("msg")!="401" && hm.get("msg")!="403"){
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                if(user?.usertype?.name=='Employee')
                {
                    Employee employee=Employee.findByUserAndOrganization(user,user?.organization)
                    hm.put("name",employee?.name)
                    hm.put("org",user?.organization?.displayname)
                }else
                {
                    Member member=Member.findByUserAndOrganization(user,user?.organization)
                    hm.put("name",member?.name)
                    hm.put("org",user?.organization?.displayname)
                }
                render hm as JSON
                return
            }
        }
        render hm as JSON
    }
    def getlinks(){
        println"in getlinks:"
        //header info
        def token =  request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid =  request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //
        //body
        String body =  request.getReader().text

        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        //println "dataresponse:"+dataresponse
        def role_name = dataresponse["role"]//["Role-Management"//role_type
        AuthService auth = new AuthService()
        HashMap hm = new HashMap()
        String purpose = "Getting links of "+role_name
        auth.checkauth(token,uid,hm,url,request,purpose)//401;200;token-refreshed ->msg
        if(hm.get("msg")!="401" && hm.get("msg")!="403"){
            //
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def role = Role.findByNameAndOrganizationAndIsactive(role_name,user.organization,true)
                def role = Role.findByNameAndIsactive(role_name,true)
                //def links = RoleLinks.findAllByRoleAndOrganizationAndIsactive(role,user.organization,true).sort{it.sort_order}
                def links = RoleLinks.findAllByRoleAndIsactive(role,true).sort{it.sort_order}
                hm.put("links",links)
                println ("links"+links)
                render hm as JSON
                return
            }
        }
        render hm as JSON
        return
    }

}
