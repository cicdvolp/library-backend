package library
import grails.gorm.MultiTenant
class Book implements MultiTenant<Book>
{
    String isbn
    String title
    String edition
    String edition_year
    String volume
    String description
    String vendor_name
    int total_books
    boolean isactive

    Boolean is_considered_in_opac//true: show in OPAC, false: DO NOT show in OPAC
    Date subscription_from
    Date subscription_to
    String volume_from
    String volume_to
    String issue_from
    String issue_to

    String placeofpublication
    String medialink
    String keywords

    String classificationno

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address


    static belongsTo=[booktype:BookType,bookcategory:BookCategory,language:Language,publisher:Publisher,organization:Organization,
                      library:Library,program:Program,librarydepartment:LibraryDepartment, physicaldetails:PhysicalDetails, accompanyingmaterials:AccompanyingMaterials,
                      series:Series,frequency:Frequency]

    static hasMany = [author:Author]

    static constraints = {
        edition_year nullable: true
        frequency nullable: true
        subscription_from nullable: true
        subscription_to nullable: true
        volume_from nullable: true
        volume_to nullable: true
        issue_from nullable: true
        issue_to nullable: true
        bookcategory nullable: true
        language nullable: true
        publisher nullable: true
        isbn nullable: true
        booktype nullable: true
        vendor_name nullable: true
        edition nullable: true
        description nullable: true
        volume nullable: true
        classificationno nullable:true
        program nullable:true
        librarydepartment nullable:true
        title nullable:true
        series nullable:true
        physicaldetails nullable:true
        accompanyingmaterials nullable:true
        placeofpublication nullable:true
        keywords nullable:true

        medialink nullable:true
    }

    static mapping = {
        isactive defaultValue: true
        is_considered_in_opac defaultValue: true
        total_books defaultValue: 0
    }
}
