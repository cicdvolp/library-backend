package library
import grails.gorm.MultiTenant
class LibraryCard implements MultiTenant<LibraryCard>
{
    String member_id
    String barcode
    Date issuedAt
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[user:User]
    static mapping = {
        isactive defaultValue: true
    }
}
