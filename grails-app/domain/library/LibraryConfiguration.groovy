package library
import grails.gorm.MultiTenant
class LibraryConfiguration implements MultiTenant<LibraryConfiguration>
{
    String name        ///reservation  enable/disable
    String value
    boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
        organization nullable: true
    }
    static mapping = {
        isactive defaultValue: true
    }
}
