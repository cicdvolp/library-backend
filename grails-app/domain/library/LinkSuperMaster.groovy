package library

class LinkSuperMaster
{
    String controller_name   //component_name
    String action_name   // activity/call
    String link_name
    String link_display_name
    String icon
    int sort_order
    boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[rolesupermaster:RoleSuperMaster,usertypesupermaster: UserTypeSuperMaster]
    static constraints = {
    }
    String toString(){
        controller_name+","+action_name+","+link_name
    }
    static mapping = {
        isactive defaultValue: true
    }
}
