package library

import grails.gorm.transactions.Transactional
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@Transactional
class NotificationService {

    def serviceMethod() {

    }

    def sesmail(String from,String to,String body,String subject){
        println "in sesmail msg:"+body
        final String FROM = from//"noreply@volp.in";   // This has been verified on the Amazon SES setup
        final String TO = to//to"nandakishor@eduplusnow.com";  // I have production access
        AWSCredential awsc=AWSCredential.findByName("SES")
        final String BODY = body//"This email was sent through the Amazon SES SMTP interface by using Java.";
        final String SUBJECT = subject//"Amazon SES test (SMTP interface accessed using Java)";

        final String SMTP_USERNAME = awsc.accesskey//"AKIAWLBOOLH4YZ2LESU5";
        final String SMTP_PASSWORD = awsc.secretkey//"BAD6/eRL3RFw5AxncD8Oit3c8A9RAg94Ih1Istxfzzyp";

        final String HOST = awsc.host//"email-smtp.us-east-1.amazonaws.com";

        final int PORT = awsc.port//587;

        Properties props = System.getProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.port", PORT);

        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.starttls.required", "true");

        Session session = Session.getDefaultInstance(props);

        MimeMessage msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(FROM));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress(TO));
        msg.setSubject(SUBJECT);
        msg.setContent(BODY,"text/html");

        Transport transport = session.getTransport();

        try
        {
            //System.out.//println("Attempting to send an email through the Amazon SES SMTP interface...");
            transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
            transport.sendMessage(msg, msg.getAllRecipients());
            println("Email sent!");
        }
        catch (Exception ex) {
            System.out.println("The email was not sent.");
            System.out.println("Error message: " + ex.getMessage());
        }
        finally
        {
            transport.close();

        }
    }

    def generateOTPforforgotPassword(email,ip){
        //println"In generateOTPforforgotPassword...."
        Date now = new Date()
        OTP otp = OTP.findByEmail(email)
        if(otp==null){
            otp = new OTP()
            otp.creation_username = email
            otp.creation_date= now
            otp.creation_ip_address=ip
        }
        if(email!=null && email!="")
            otp.email = email

        final long Day = 3600*1000*24;
        if(email!=null)
            otp.email_otp = createOTP(4)
        otp.email_otp_generation_date = now
        otp.email_otp_expiry_date = new Date(now.getTime() + Day)

        otp.updation_username = email
        otp.updation_date=now
        otp.updation_ip_address=ip

        otp.save(flush: true, failOnError: true)
        return otp
    }

    def verifyFPOTP(otp,userOtp,hm){
        //println"In verifyFPOTP..."+otp
        Date now = new Date()
        if(otp.email_otp_expiry_date<now){
            hm.put("status_reason","Email OTP Expired")
            return false
        }
        if(otp.email_otp!=userOtp){
            hm.put("status_reason","Email OTP Not Matched")
            return false
        }
        hm.put("status_reason","OTP verified")
        return  true
    }

    def createOTP(len){
        String numbers = "0123456789";
        Random rndm_method = new Random();
        char[] otp = new char[len];
        for (int i = 0; i < len; i++)
        {
            otp[i] =numbers.charAt(rndm_method.nextInt(numbers.length()));
        }
        return otp;
    }
}
