package library
import grails.gorm.MultiTenant
class RoleLinks implements MultiTenant<RoleLinks>
{
    String linkname
    String linkdescription
    int sort_order
    boolean isactive
    String controller_name   //component_name
    String action_name   // activity/call
    String link_display_name
    String icon_file_path
    String icon_file_name
    String icon
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[role:Role,usertype: UserType]
    static constraints = {
        linkdescription nullable:true
        link_display_name nullable:true
        icon_file_path nullable:true
        icon_file_name nullable:true
        icon nullable:true
    }
    static mapping = {
        isactive defaultValue: true
    }
}
