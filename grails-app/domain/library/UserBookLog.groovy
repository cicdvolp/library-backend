package library
import grails.gorm.MultiTenant

class UserBookLog implements MultiTenant<UserBookLog>{
    int renew_counter
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[user:User,bookitem:BookItem]
    static constraints = {
    }
    static mapping = {
        renew_counter defaultValue: 0
    }
}
