<html>
<head>
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
</head>
<body>
<table id="table_id" class="display">
    <thead>
        <tr>
            <th>Srno</th>
            <th>Uni-code</th>
            <th>Colid</th>
            <th>Learner</th>
            <th>Course</th>
        </tr>
    </thead>
    <tbody>
    <g:each in="${cols}" var="col" status="i">
        <tr>
            <td>${col.srno}</td>
            <td>${col.code}</td>
            <td>${col.colid}</td>
            <td>${col.learner}</td>
            <td>${col.course}</td>
        </tr>
    </g:each>
    </tbody>
</table>
</body>
<script>
$(document).ready( function () {
    $('#table_id').DataTable(
    {
        paging: false
    }
    );
} );
</script>
</html>