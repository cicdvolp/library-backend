import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_library_back_sampleresettest_gsp extends org.grails.gsp.GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/sample/resettest.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
for( sub in (lqfs) ) {
printHtmlPart(1)
expressionOut.print(sub?.id)
printHtmlPart(2)
expressionOut.print(sub?.learner?.person?.firstName)
printHtmlPart(3)
expressionOut.print(sub?.learner?.person?.lastName)
printHtmlPart(2)
expressionOut.print(sub?.courseoffering?.course?.course_name)
printHtmlPart(2)
expressionOut.print(sub?.quizoffering?.title)
printHtmlPart(2)
invokeTag('formatDate','g',19,['format':("dd-MM-yyyy hh:mm:ss"),'date':(sub?.start_date)],-1)
printHtmlPart(4)
createTagBody(2, {->
printHtmlPart(5)
expressionOut.print(sub?.id)
printHtmlPart(6)
invokeTag('formatDate','g',23,['format':("dd-MM-yyyy hh:mm:ss"),'date':(sub?.end_date)],-1)
printHtmlPart(7)
})
invokeTag('form','g',25,['action':("saveTime")],2)
printHtmlPart(8)
}
printHtmlPart(9)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1636605186493L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
