package library

import grails.gorm.multitenancy.Tenants
import grails.gorm.transactions.Transactional
import groovy.json.JsonSlurper
import org.grails.datastore.mapping.query.Query

import java.text.SimpleDateFormat;

@Transactional
class ReserveBookService {

    def serviceMethod() {

    }
    def reserveBookInit(request, hm,ip)
    {
        println("reserveBookInit in service")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String purpose = "reserveBookInit"
        //auth.checkauth(token, uid, url, request, purpose)//401;200;token-refreshed ->msg
        def bookinfo = []
        //if (hm.get("msg") != "401" && hm.get("msg") != "403") {
        Login log = Login.findByUsername(uid)
        Tenants.withId(log.tenantclient.tenantid) {
            def user = User.findByUsername(log.username)

            def index_config = IndexConfig.list()
            def isReservationSink = index_config[0].isReservationSink
//            if(!isReservationSink) {
                syncReservationDaysLimit(uid, ip,index_config[0],user.organization)
//            }

            int srno = 1
            ArrayList books = new ArrayList()
            def bookList = Book.findAllByOrganization(user?.organization)
          //  println"TotalBooks-->"+bookList.size() + " in "+user?.organization?.displayname + "<-----"
            for(book in bookList){
                HashMap temp = new HashMap()
                temp.put('srno',srno++)
                temp.put('id',book?.id)
                temp.put('title',book?.title)
                temp.put('isbn',book?.isbn)
                temp.put('edition',book?.edition)
//                    temp.put('bookFormat',book?.isbn)
                temp.put('bookType',book?.booktype?.name)
                temp.put('category',book?.bookcategory?.name)
                temp.put('publisher',book?.publisher?.name)
                temp.put('copies',book?.total_books)
                BookStatus bookStatus = BookStatus.findByName('Available')
                def bookItems = BookItem.findAllByBookAndBookstatus(book,bookStatus)
                temp.put('available',bookItems.size())
                def authorArr = book?.author
                ArrayList arr = new ArrayList()
                for(author in authorArr){
                    def authorName = Author.findById(author?.id)
                    arr.add(authorName?.name)
                }
                temp.put('authors',arr)
                books.add(temp)

            }
            //books = books.unique{it.isbn}
            hm.put('bookList',books)
            hm.put('msg','200')
            println("----------------------------")
           // println("bookinfo  :: "+bookinfo)
            println("----------------------------")
            return bookinfo
        }
        return bookinfo

        // }

    }

    def reserveBook(request, hm,ip)
    {
        println("reserve Book")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        //println "dataresponse--------------------"+dataresponse
        String purpose = "reserve Book"
        //auth.checkauth(token, uid, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
        Login log = Login.findByUsername(uid)
        Tenants.withId(log.tenantclient.tenantid) {
            def user = User.findByUsername(log.username)
            def member = User.findByUsername(dataresponse.memberId)
            //print"member-------------"+member
            BookStatus b_status = BookStatus.findByName('Available')
            BookStatus b_status_reserve = BookStatus.findByName('Reserved')
            ReservationStatus reservation_status = ReservationStatus.findByName('Pending')
            ReservationStatus reservation_status1 = ReservationStatus.findByName('Waiting')
            if(!b_status)
            {
                hm.put('msg','Please add reserved book status- '+"Available")
                return
            }
            if(!b_b_status_reservestatus)
            {
                hm.put('msg','Please add reserved book status- '+"Reserved")
                return
            }
            if(!reservation_status)
            {
                hm.put('msg','Please add reserved book status- '+"Pending")
                return
            }
            if(!reservation_status1)
            {
                hm.put('msg','Please add reserved book status- '+"Waiting")
                return
            }


            for(int i=0;i< dataresponse.bookIds.size();i++) {

                Book book = Book.findById(dataresponse.bookIds[i])
                def bookItem_list = BookItem.findAllByBookAndBookstatus(book, b_status)
                def bookItem
                if(bookItem_list.size() > 0){
                     bookItem = bookItem_list[0]
                }
                if (bookItem_list.size() > 0) {


//                    println('b_status_reserve----' + b_status_reserve)
                    bookItem.bookstatus = b_status_reserve
                    bookItem.updation_username = uid
                    bookItem.updation_date = new Date()
                    bookItem.updation_ip_address = ip
                    bookItem.save(flush: true, failOnError: true)

                    BookReservation book_reservation = new BookReservation()


                    book_reservation.reservation_date = new Date()
                    book_reservation.user = member
                    book_reservation.book = book
                    book_reservation.isactive = true
                    book_reservation.reservationstatus = reservation_status
                    book_reservation.creation_username = uid
                    book_reservation.updation_username = uid
                    book_reservation.creation_date = new Date()
                    book_reservation.updation_date = new Date()
                    book_reservation.creation_ip_address = ip
                    book_reservation.updation_ip_address = ip
                    book_reservation.save(flush: true, failOnError: true)
                }else{

                    BookReservation book_reservation = new BookReservation()

                    book_reservation.reservation_date = new Date()
                    book_reservation.user = member
                    book_reservation.book = book
                    book_reservation.isactive = true
                    book_reservation.reservationstatus = reservation_status1
                    book_reservation.creation_username = uid
                    book_reservation.updation_username = uid
                    book_reservation.creation_date = new Date()
                    book_reservation.updation_date = new Date()
                    book_reservation.creation_ip_address = ip
                    book_reservation.updation_ip_address = ip
                    book_reservation.save(flush: true, failOnError: true)
                }
            }

            hm.put('msg','200')
            }
            return hm
        }
    }

    def getEmpMemInformation(request,hm)
    {
        println("in BookOperationService member information")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        def userid =  dataresponse["userid"]

        String purpose = "fetch employee data"
        //auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def usr = User.findById(userid)
                LibraryPolicy libraryPolicy
                if(usr.role.size()>=1)
                {
                    libraryPolicy = LibraryPolicy.findByRoleAndOrganization(usr.role[0],usr.organization)
                }
                HashMap temp = new HashMap()
                temp.put("userid",usr?.id)
                temp.put("user",usr?.username)
                temp.put("usertype",usr?.usertype?.name)
                temp.put("date_of_membership",usr?.creation_date)
                def usrbooks = BookLending.findAllByUserAndIsactive(usr,true)
                temp.put("total_checked_out_book",usrbooks.size())
                Employee employee = Employee.findByUserAndOrganization(usr,user.organization)
                temp.put("empid",employee?.id)
                temp.put("employee_code",employee?.employee_code)
                temp.put("empname",employee?.name)
                Person person = Person.findByUser(usr)
                temp.put("mobile_number",person?.mobile_number)
                temp.put("email",person?.email)
                temp.put("address",person?.address)
                temp.put("city",person?.city)
                temp.put("state",person?.state)
                temp.put("country",person?.country)
                temp.put("pin",person?.pin)
                temp.put("max_number_book_allowed",libraryPolicy?.number_of_books)
//                def booklendinginformation = getEmpMemBookLendingInformation(request , userid)
                hm.put("memberinfo",temp)
//                hm.put("booklendinginformation",booklendinginformation)


                int srno = 1
                ArrayList books = new ArrayList()


                User member = User.findById(userid)
                Role role = Role.findByName('Member')
                LibraryPolicy policy = LibraryPolicy.findByRoleAndOrganization(role,user.organization)
                def reservation_daysLimit = policy?.reservation_validity_days
//                println('reservation_daysLimit------'+reservation_daysLimit)

//                def status = ReservationStatus.findByName('Pending')
                def reserveList = BookReservation.findAllByUserAndIsactive(member,true)
                def book_reservation_limit = policy?.book_reservation_limit
                for(reserve_book in reserveList) {
                    HashMap temp1 = new HashMap()

                    def book = Book.findById(reserve_book?.book?.id)

                    temp1.put('srno', srno++)
                    temp1.put('id', book?.id)
                    temp1.put('title', book?.title)
                    temp1.put('isbn', book?.isbn)
                    temp1.put('edition', book?.edition)
                    temp1.put('bookType', book?.booktype?.name)
                    temp1.put('category', book?.bookcategory?.name)
                    temp1.put('publisher', book?.publisher?.name)
                    temp1.put('copies', book?.total_books)
                    temp1.put('reservation_status', reserve_book?.reservationstatus?.name)
                    temp1.put('isReserve', true)
                    BookStatus bookStatus = BookStatus.findByName('Available')
                    def bookItems = BookItem.findAllByBookAndBookstatus(book, bookStatus)
                    temp1.put('available', bookItems.size())
                    def authorArr = book?.author
                    ArrayList arr = new ArrayList()
                    for (author in authorArr) {
                        def authorName = Author.findById(author?.id)
                        arr.add(authorName?.name)
                    }

                    temp1.put('authors',arr)
                    books.add(temp1)
                }
                hm.put("reserved_count",books.size())

                if(book_reservation_limit > books.size()){
                    hm.put('isReservation_limit_exeed',false)
                    hm.put('Reservation_limit',book_reservation_limit)
                }else {
                    hm.put('isReservation_limit_exeed',true)
                    hm.put('Reservation_limit',book_reservation_limit)
                }



                def bookList = Book.findAll()
//                println('bookList============'+bookList)
                for(book in bookList){
                    HashMap temp2 = new HashMap()
                    temp2.put('srno',srno++)
                    temp2.put('id',book?.id)
                    temp2.put('title',book?.title)
                    temp2.put('isbn',book?.isbn)
                    temp2.put('edition',book?.edition)
//                    temp.put('bookFormat',book?.isbn)
                    temp2.put('bookType',book?.booktype?.name)
                    temp2.put('category',book?.bookcategory?.name)
                    temp2.put('publisher',book?.publisher?.name)
                    temp2.put('copies',book?.total_books)
                    temp2.put('isReserve', false)
                    temp2.put('reservation_status', '-')
                    BookStatus bookStatus = BookStatus.findByName('Available')
                    def bookItems = BookItem.findAllByBookAndBookstatus(book,bookStatus)
                    temp2.put('available',bookItems.size())
                    def authorArr = book?.author
                    ArrayList arr = new ArrayList()
                    for(author in authorArr){
                        def authorName = Author.findById(author?.id)
                        arr.add(authorName?.name)
                    }
                    temp2.put('authors',arr)
                    books.add(temp2)

                }
                books = books.unique{it.isbn}

                hm.put('bookList',books)

//                println hm
                hm.put("msg", "200")
                return hm
            }
            return hm
        }
    }
    def getEmpMemInformation1(request,hm)
    {
        println("in BookOperationService member information")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        def userid =  dataresponse["userid"]

        String purpose = "fetch employee data"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def usr = User.findByUsername(userid)

                LibraryPolicy libraryPolicy
                if(usr.role.size()>=1)
                {
                    libraryPolicy = LibraryPolicy.findByRoleAndOrganization(usr.role[0],usr.organization)
                }
                HashMap temp = new HashMap()
                temp.put("userid",usr?.id)
                temp.put("user",usr?.username)
                temp.put("usertype",usr?.usertype?.name)
                temp.put("date_of_membership",usr?.creation_date)
                def usrbooks = BookLending.findAllByUserAndIsactive(usr,true)
                temp.put("total_checked_out_book",usrbooks.size())
                Employee employee = Employee.findByUserAndOrganization(usr,user.organization)

                if(employee!=null){
                    temp.put("empid",employee?.id)
                    temp.put("employee_code",employee?.employee_code)
                    temp.put("empname",employee?.name)
                }else {
                    Member member = Member.findByUserAndOrganization(usr,user.organization)
                    temp.put("empid",member?.id)
                    temp.put("employee_code",member?.registration_number)
                    println("employeee  fjs hdjf hs jh dfj sdf j sdf s dh jsh fjd jshd fsj hdf hsdfjhsjd")
                    println(member?.name);
                    temp.put("empname",member?.name)
                }

                Person person = Person.findByUser(usr)
                temp.put("mobile_number",person?.mobile_number)
                temp.put("email",person?.email)
                temp.put("address",person?.address)
                temp.put("city",person?.city)
                temp.put("state",person?.state)
                temp.put("country",person?.country)
                temp.put("pin",person?.pin)
                temp.put("max_number_book_allowed",libraryPolicy?.number_of_books)
//                def booklendinginformation = getEmpMemBookLendingInformation(request , userid)
                hm.put("memberinfo",temp)
//                hm.put("booklendinginformation",booklendinginformation)


                int srno = 1
                ArrayList books = new ArrayList()



                Role role = Role.findByName('Member')
                LibraryPolicy policy = LibraryPolicy.findByRoleAndOrganization(role,usr.organization)
                def reservation_daysLimit = policy?.reservation_validity_days
//                println('reservation_daysLimit------'+reservation_daysLimit)

//                def status = ReservationStatus.findByName('Pending')
                def reserveList = BookReservation.findAllByUserAndIsactive(usr,true)
                def book_reservation_limit = policy?.book_reservation_limit
                for(reserve_book in reserveList) {
                    HashMap temp1 = new HashMap()

                    def book = Book.findById(reserve_book?.book?.id)

                    temp1.put('srno', srno++)
                    temp1.put('id', book?.id)
                    temp1.put('title', book?.title)
                    temp1.put('isbn', book?.isbn)
                    temp1.put('edition', book?.edition)
                    temp1.put('bookType', book?.booktype?.name)
                    temp1.put('category', book?.bookcategory?.name)
                    temp1.put('publisher', book?.publisher?.name)
                    temp1.put('copies', book?.total_books)
                    temp1.put('reservation_status', reserve_book?.reservationstatus?.name)
                    temp1.put('isReserve', true)
                    BookStatus bookStatus = BookStatus.findByName('Available')
                    def bookItems = BookItem.findAllByBookAndBookstatus(book, bookStatus)
                    temp1.put('available', bookItems.size())
                    def authorArr = book?.author
                    ArrayList arr = new ArrayList()
                    for (author in authorArr) {
                        def authorName = Author.findById(author?.id)
                        arr.add(authorName?.name)
                    }

                    temp1.put('authors',arr)
                    books.add(temp1)
                }
                hm.put("reserved_count",books.size())

                if(book_reservation_limit > books.size()){
                    hm.put('isReservation_limit_exeed',false)
                    hm.put('Reservation_limit',book_reservation_limit)
                }else {
                    hm.put('isReservation_limit_exeed',true)
                    hm.put('Reservation_limit',book_reservation_limit)
                }



                def bookList = Book.findAllByOrganization(usr.organization)
//                println('bookList============'+bookList)
                for(book in bookList){
                    HashMap temp2 = new HashMap()
                    temp2.put('srno',srno++)
                    temp2.put('id',book?.id)
                    temp2.put('title',book?.title)
                    temp2.put('isbn',book?.isbn)
                    temp2.put('edition',book?.edition)
//                    temp.put('bookFormat',book?.isbn)
                    temp2.put('bookType',book?.booktype?.name)
                    temp2.put('category',book?.bookcategory?.name)
                    temp2.put('publisher',book?.publisher?.name)
                    temp2.put('copies',book?.total_books)
                    BookReservation brstatus = BookReservation.findByBook(book)
                   // println("brstatus : "+brstatus)
                    if(brstatus!= null){
                        temp2.put('isReserve', brstatus.isactive)
                        temp2.put('reservation_status', brstatus?.reservationstatus?.name)
                    }
                    else{
                        temp2.put('isReserve', false)
                    temp2.put('reservation_status', '-')
                    }
                    BookStatus bookStatus = BookStatus.findByName('Available')
                    def bookItems = BookItem.findAllByBookAndBookstatusAndOrganization(book,bookStatus,usr.organization)
                    temp2.put('available',bookItems.size())
                    def authorArr = book?.author
                    ArrayList arr = new ArrayList()
                    for(author in authorArr){
                        def authorName = Author.findById(author?.id)
                        arr.add(authorName?.name)
                    }
                    temp2.put('authors',arr)
                    books.add(temp2)
//                    if(brstatus!= null){
//                        println("temp2 : "+temp2)
//                    }

                }
                books = books.unique{it.isbn}

                hm.put('bookList',books)

//                println hm
                hm.put("msg", "200")
                return hm
            }
            return hm
        }
    }
    def getEmpMemBookLendingInformation(request , userid)
    {
        println("in Book lending member information")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String purpose = "fetch employee data"
        HashMap hm = new HashMap()
        //auth.checkauth(token, uid, url, request, purpose)//401;200;token-refreshed ->msg
        def bookinfo = []
        //if (hm.get("msg") != "401" && hm.get("msg") != "403") {
        Login log = Login.findByUsername(uid)
        Tenants.withId(log.tenantclient.tenantid) {
            def user = User.findByUsername(log.username)
            def usr = User.findById(userid)
            def booklendinginformation = BookLending.findAllByUserAndIsactive(usr,true)
            int count = 1
            for(blf in booklendinginformation) {
                HashMap temp = new HashMap()
                temp.put("srno", count++)
                temp.put("blfid", blf?.id)
                temp.put("accession_number", blf?.bookitem?.accession_number)
                temp.put("title", blf?.bookitem?.book?.title)
                String authornames =""
                for(author in blf?.bookitem?.book?.author)
                {
//                    println "author.name"+author.name
                    authornames = authornames + author.name+","
                }
                temp.put("author", authornames)
                temp.put("publisher", blf?.bookitem?.book?.publisher?.name)
                temp.put("booktype", blf?.bookitem?.book?.booktype?.name)
                temp.put("bookformat", blf?.bookitem?.bookformat?.name)
                temp.put("borrowed_date",dateToString(blf?.borrowing_date))
                temp.put("return_date", dateToString(blf?.return_date))
                //temp.put("empname", employee?.name)
                temp.put("due_date", dateToString(blf?.due_date))
                SimpleDateFormat sdformat = new SimpleDateFormat("dd-MM-yyyy");
                Date d1 = sdformat.parse(dateToString(blf?.due_date));
                Date d2 = sdformat.parse(dateToString(new Date()));
//                    System.out.println("The date 1 is: " + sdformat.format(d1));
//                    System.out.println("The date 2 is: " + sdformat.format(d2));
//                    if(d1.compareTo(d2) > 0) {
//                        System.out.println("Date 1 occurs after Date 2");
//                    } else if(d1.compareTo(d2) < 0) {
//                        System.out.println("Date 1 occurs before Date 2");
//                    } else if(d1.compareTo(d2) == 0) {
//                        System.out.println("Both dates are equal");
//                    }
                if(d1.compareTo(d2) > 0)
                {
                    temp.put("fine", "NA")
                }
                else{
                    LibraryPolicy libraryPolicy
                    if(usr.role.size()>=1)
                    {
                        libraryPolicy = LibraryPolicy.findByRoleAndOrganization(usr.role[0],usr.organization)
                    }
                    if(libraryPolicy?.fine_rate_per_day) {
                        long difference_In_Time = d2.getTime() - d1.getTime()
                        long difference_In_Days = (difference_In_Time / (1000 * 60 * 60 * 24))
                        long fine = libraryPolicy.fine_rate_per_day * difference_In_Days
                        blf.fine_amount = fine
                        blf.save(flush:true,failOnError: true)
                        temp.put("fine",fine)
                    }
                    else
                        temp.put("fine","Fine rate not set/ either it is zero...")

                }
                bookinfo.add(temp)
            }
            hm.put("booklendinginfo",bookinfo)
            return bookinfo
        }
        return bookinfo
    }
    def dateToString(Date date)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String strDate = formatter.format(date);
//        println strDate
        return strDate
    }

    def getReserveList(request,hm)
    {
        println("in getReserveList")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        def userid =  dataresponse["userid"]

        String purpose = "getReserveList"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                int srno = 1
                ArrayList books = new ArrayList()


                User member = User.findByUsername(userid)
                Role role = Role.findByName('Member')
                LibraryPolicy policy = LibraryPolicy.findByRoleAndOrganization(role,member?.organization)
                def reservation_daysLimit = policy?.reservation_validity_days
//                println('reservation_daysLimit------'+reservation_daysLimit)

                def reserveList = BookReservation.findAllByUserAndIsactive(member,true)
                def book_reservation_limit = policy?.book_reservation_limit
                for(reserve_book in reserveList) {
                    HashMap temp1 = new HashMap()

                    def book = Book.findById(reserve_book?.book?.id)

                    temp1.put('srno', srno++)
                    temp1.put('id', book?.id)
                    temp1.put('title', book?.title)
                    temp1.put('isbn', book?.isbn)
                    temp1.put('edition', book?.edition)
                    temp1.put('bookType', book?.booktype?.name)
                    temp1.put('category', book?.bookcategory?.name)
                    temp1.put('publisher', book?.publisher?.name)
                    temp1.put('copies', book?.total_books)
                    temp1.put('reservation_status', reserve_book?.reservationstatus?.name)
                    temp1.put('isReserve', true)
                    BookStatus bookStatus = BookStatus.findByName('Available')
                    def bookItems = BookItem.findAllByBookAndBookstatus(book, bookStatus)
                    temp1.put('available', bookItems.size())
                    def authorArr = book?.author
                    ArrayList arr = new ArrayList()
                    for (author in authorArr) {
                        def authorName = Author.findById(author?.id)
                        arr.add(authorName?.name)
                    }

                    temp1.put('authors',arr)
                    books.add(temp1)
                }
                if(book_reservation_limit > books.size()){
                    hm.put('isReservation_limit_exeed',false)
                }else {
                    hm.put('isReservation_limit_exeed',true)
                }

println("yds syuifcsuid fuisudyf uishd ufhs hhhhhyahs alekar")
                def bookList = Book.findAllByOrganization(member?.organization)
//                println('bookList============'+bookList)
                for(book in bookList){
                    HashMap temp2 = new HashMap()
                    temp2.put('srno',srno++)
                    temp2.put('id',book?.id)
                    temp2.put('title',book?.title)
                    temp2.put('isbn',book?.isbn)
                    temp2.put('edition',book?.edition)
//                    temp.put('bookFormat',book?.isbn)
                    temp2.put('bookType',book?.booktype?.name)
                    temp2.put('category',book?.bookcategory?.name)
                    temp2.put('reservation_status', '-')
                    temp2.put('publisher',book?.publisher?.name)
                    temp2.put('copies',book?.total_books)
                    temp2.put('isReserve', false)
                    BookStatus bookStatus = BookStatus.findByName('Available')
                    def bookItems = BookItem.findAllByBookAndBookstatus(book,bookStatus)
                    temp2.put('available',bookItems.size())
                    def authorArr = book?.author
                    ArrayList arr = new ArrayList()
                    for(author in authorArr){
                        def authorName = Author.findById(author?.id)
                        arr.add(authorName?.name)
                    }
                    temp2.put('authors',arr)
                    books.add(temp2)

                }
                books = books.unique{it.isbn}

                hm.put('bookList',books)

//                println hm
                hm.put("msg", "200")
                return hm
            }
            return hm
        }
    }

    def syncReservationDaysLimit(uid,ip,indexConfig,org){
        ArrayList reservation_request = new ArrayList()
        BookStatus status_available = BookStatus.findByName('Available')
        BookStatus status_reserved = BookStatus.findByName('Reserved')


        def reservationList = BookReservation.findAllByIsactive(true)
        for(book_reserve in reservationList){
            def policy = LibraryPolicy.findByRoleAndOrganization(book_reserve?.user?.role,book_reserve?.user?.organization)
            def validity = 0
            if(policy?.reservation_validity_days)
                validity = policy?.reservation_validity_days
            Calendar c = Calendar.getInstance();
            c.setTime(book_reserve?.reservation_date);
            c.add(Calendar.DATE, validity); // Adding days
            Date today = new Date()

            if(today >  c.getTime()){
                ReservationStatus status_cancled  = ReservationStatus.findByName('Canceled')
                book_reserve?.isactive = false
                book_reserve?.updation_username = uid
                book_reserve?.updation_date = new Date()
                book_reserve?.reservationstatus = status_cancled
                book_reserve?.updation_ip_address = ip
                book_reserve.save(flush: true, failOnError: true)

                BookItem bookItem = BookItem.findByBookAndBookstatusAndOrganization(book_reserve?.book,status_reserved,org)
                if(bookItem) {
                    bookItem.bookstatus = status_available
                    bookItem.updation_username = uid
                    bookItem.updation_date = new Date()
                    bookItem.updation_ip_address = ip
                    bookItem.save(flush: true, failOnError: true)
                }
            }
           HashMap reserve_data = new HashMap()
            if(book_reserve?.reservationstatus?.name == 'Waiting'){
//                reserve_data.put('reservation_id',book_reserve?.id)
//                reserve_data.put('bookId',book_reserve?.book)
//                reserve_data.put('userId',book_reserve?.user)
                reservation_request.add(book_reserve)
            }

        }
        indexConfig.isReservationSink = true
        indexConfig.save(flush: true, failOnError: true)

        reservation_request.sort({it?.reservation_date})
//        println('reservation_request--------'+ reservation_request.size())
//        println('reservation_request---------'+ reservation_request)

        //book available student request in pending (notification)
        ReservationStatus status_pending = ReservationStatus.findByName('Pending')
        for(book_request in reservation_request){
            def bookItem_available = BookItem.findByBookAndBookstatus(book_request?.book,status_available)
            if(bookItem_available){
                book_request?.updation_username = uid
                book_request?.updation_date = new Date()
                book_request?.reservationstatus = status_pending
                book_request?.updation_ip_address = ip
                book_request.save(flush: true, failOnError: true)

                bookItem_available.bookstatus = status_reserved
                bookItem_available.updation_username = uid
                bookItem_available.updation_date = new Date()
                bookItem_available.updation_ip_address = ip
                bookItem_available.save(flush: true, failOnError: true)
            }
        }
    }
}
