package library
import grails.gorm.MultiTenant
class Member implements MultiTenant<Member>
{
    String registration_number
    String name
    Date date_of_membership
    boolean isbookbankpolicy
    boolean ispassout
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,user:User,person:Person,program:Program]
    static constraints = {
        name nullable: true
        registration_number nullable: true
        date_of_membership nullable:true
        program nullable:true
    }
    static mapping = {
        isbookbankpolicy defaultValue: false
        ispassout defaultValue: false
    }
}
