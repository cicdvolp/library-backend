package library
import java.text.SimpleDateFormat
import grails.gorm.transactions.Transactional
import grails.gorm.multitenancy.Tenants
import grails.converters.JSON
import groovy.json.JsonSlurper
import com.opencsv.CSVReader
import javax.servlet.http.Part;
import grails.io.IOUtils
import java.nio.file.Paths
import java.nio.file.Files
import java.nio.file.StandardCopyOption
import java.util.Calendar;
@Transactional
class MemberProcessService {

    def serviceMethod() {}
    //get Issued Books Data
    def getBooksData(request, hm,ip) {
        println("in getBooksData:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "Get Books Data"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                ArrayList books_list = new ArrayList()
                ArrayList auths = new ArrayList()
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                BookStatus bs = BookStatus.findByName('Issued')
                LibraryConfiguration lc=LibraryConfiguration.findByNameAndIsactive("RENEW",true)
                User user=User.findByUsername(log?.username)
                //def bookitems=BookItem.list()
                def bookitems=BookItem.findAllByBookstatus(bs)
                /*def c = BookItem.createCriteria()
                def results = c.list {
                    or {
                        eq("bookstatus", bs)
                        eq("bookstatus", renew)
                    }
                }*/
                //println("results ::"+results)
                //println("lc?.value ::"+lc?.value)
                int conut
                for(bi in bookitems) {
                    //def bookLending = BookLending.findAllByUserAndBookitemAndIsactive(user,bi,true)
                    UserBookLog userBookLog=UserBookLog.findByUserAndBookitem(user,bi)
                    if (userBookLog){
                        conut=userBookLog.renew_counter
                    }
                    def bookLending = BookLending.findAllByUserAndBookitemAndIsactive(user,bi,true)
                    //println("bookLending ::" + bookLending)
                    for (bl in bookLending) {
                        /*if (bl?.bookitem?.bookstatus?.id == renew?.id) {
                            conut++
                            //println("in Renew")
                        }*/
                        if (bl?.isactive == true) {
                            HashMap temp = new HashMap()
                            temp.put('bl_Id', bl?.id)
                            temp.put('accession_number', bl?.bookitem?.accession_number)
                            temp.put('title', bl?.bookitem?.book?.title)
                            for (au in bl?.bookitem?.book?.author) {
                                HashMap temp2 = new HashMap()
                                temp2.put('auth_name', au?.name)
                                auths.add(temp2)
                            }
                            temp.put('author', auths)
                            temp.put('bookcategory', bl?.bookitem?.book?.bookcategory?.name)
                            temp.put('publisher', bl?.bookitem?.book?.publisher?.name)
                            temp.put('booktype', bl?.bookitem?.book?.booktype?.name)
                            temp.put('price', bl?.bookitem?.price)
                            temp.put('numberofpages', bl?.bookitem?.numberofpages)
                            temp.put('bookformat', bl?.bookitem?.bookformat?.name)
                            SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd")
                            temp.put('borrowing_date', dateFormat1.format(bl?.borrowing_date))
                            temp.put('display_borrowing_date', dateFormat.format(bl?.borrowing_date))
                            boolean renewbtn
                            //Date due_date = dateFormat.parse(dateFormat1.format(bl?.due_date))
                            //Date submission_date = dateFormat.parse(dateFormat1.format(new java.util.Date()))
                            //println("due_date.compareTo(submission_date):" + due_date.compareTo(submission_date))
                            /*if (due_date.compareTo(submission_date) == 0) {
                            renewbtn=true
                            temp.put("renewbtn",renewbtn)
                        } else {
                            renewbtn=false
                            temp.put("renewbtn",renewbtn)
                        }*/
                            println("conut >>>:"+conut)
                            println("lc?.value ::"+lc?.value)
                            int lc1=Integer.parseInt(lc?.value)
                            if (conut == lc1) {
                                renewbtn = true
                                //println("if")
                                temp.put("renewbtn", renewbtn)
                            } else {
                                //println("else")
                                renewbtn = false
                                temp.put("renewbtn", renewbtn)
                            }
                            temp.put('due_date', dateFormat1.format(bl?.due_date))
                            temp.put('displey_due_date', dateFormat.format(bl?.due_date))
                            //temp.put('return_date', dateFormat.format(bl?.return_date))
                            temp.put('fine_amount', bl?.fine_amount)
                            books_list.add(temp)
                        }
                    }

                }
                //println("Count:::"+conut)
                hm.put('msg','200')
                hm.put('booksList',books_list)
                //println("HM::"+hm)
                return hm
            }
            return hm
        }
        return hm
    }
    //renew issued Book
    def renewbook_old(request, hm,ip) {
        println("in renewbook:"+request)
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "Renew Books "
        String body =  request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse ::"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
                Calendar cal = Calendar.getInstance();
                User user=User.findByUsername(log?.username)
                BookLending bookLending=BookLending.findById(dataresponse.item)
                println("bookLending ::"+bookLending)
                int role
                for(r in user.role)
                {
                    if(r.name=='Member')
                        role=r.id
                    else
                        continue
                }
                Role role1=Role.findById(role)
                LibraryPolicy lp=LibraryPolicy.findByRoleAndOrganization(role1,user.organization)
                int rvd=lp?.reservation_validity_days
                //new Code For Date
                Calendar c = Calendar.getInstance();
                c.setTime(new Date()); // Now use today date.
                c.add(Calendar.DATE, rvd)
               /* String oldDate = dataresponse?.due_date
                cal.setTime(formatter.parse(oldDate))
                cal.add(Calendar.DAY_OF_MONTH, rvd)
                String new_date=formatter.format(cal.getTime())
                println("New Data ::"+new_date)*/
                if (bookLending!=null)
                {
                    bookLending.isactive=false
                    bookLending.return_date=new java.util.Date()
                    bookLending.updation_username = log.username
                    bookLending.updation_date = new java.util.Date()
                    bookLending.updation_ip_address = request.getRemoteAddr()
                    bookLending.save(flush:true,failOnError:true)
                }
                BookLending bookLending1=BookLending.findById(dataresponse.item)
                bookLending=new BookLending()
                bookLending.isactive=true
                bookLending.borrowing_date=new java.util.Date()
                /*SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
                Date date3 = formatter1.parse(new_date)
                println("Date  ::"+date3)*/
                bookLending.due_date = c.getTime()
                bookLending.bookitem=bookLending1?.bookitem
                bookLending.user=user
                bookLending.organization = user?.organization
                bookLending.fine_amount=0.0
                bookLending.creation_username = log.username
                bookLending.updation_username = log.username
                bookLending.creation_date = new java.util.Date()
                bookLending.updation_date = new java.util.Date()
                bookLending.creation_ip_address = request.getRemoteAddr()
                bookLending.updation_ip_address = request.getRemoteAddr()
                bookLending.save(flush:true,failOnError:true)
                hm.put('msg','200')
                return hm
            }
            return hm
        }
        return hm
    }
    //renew issued Book
    def renewbook(request, hm,ip) {
        println("in renewbook:"+request)
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "Renew Books "
        String body =  request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse ::"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
                Calendar cal = Calendar.getInstance();
                BookStatus renew = BookStatus.findByName('Renew')
                LibraryConfiguration lc=LibraryConfiguration.findByNameAndIsactive("RENEW",true)
                User user=User.findByUsername(log?.username)
                BookLending bookLending=BookLending.findById(dataresponse.item)
                println("bookLending ::"+bookLending)
                //BookItem bookItem=BookItem.findById(bookLending?.bookitem?.id)
                UserBookLog userBookLog=UserBookLog.findByUserAndBookitem(user,bookLending?.bookitem)
                if (userBookLog){
                    userBookLog.renew_counter=userBookLog.renew_counter+1
                    userBookLog.updation_username = log.username
                    userBookLog.updation_date = new java.util.Date()
                    userBookLog.updation_ip_address = request.getRemoteAddr()
                    userBookLog.save(flush:true,failOnError:true)
                }
                int role
                for(r in user.role)
                {
                    if(r.name=='Member')
                        role=r.id
                    else
                        continue
                }
                Role role1=Role.findById(role)
                LibraryPolicy lp=LibraryPolicy.findByRoleAndOrganization(role1,user.organization)
                int rvd=lp?.reservation_validity_days
                //new Code For Date
                Calendar c = Calendar.getInstance();
                c.setTime(new Date()); // Now use today date.
                c.add(Calendar.DATE, rvd)
               /* String oldDate = dataresponse?.due_date
                cal.setTime(formatter.parse(oldDate))
                cal.add(Calendar.DAY_OF_MONTH, rvd)
                String new_date=formatter.format(cal.getTime())
                println("New Data ::"+new_date)*/
                if (bookLending!=null)
                {
                    bookLending.isactive=false
                    bookLending.return_date=new java.util.Date()
                    bookLending.updation_username = log.username
                    bookLending.updation_date = new java.util.Date()
                    bookLending.updation_ip_address = request.getRemoteAddr()
                    bookLending.save(flush:true,failOnError:true)
                }
                BookLending bookLending1=BookLending.findById(dataresponse.item)
                bookLending=new BookLending()
                bookLending.isactive=true
                bookLending.borrowing_date=new java.util.Date()
                /*SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
                Date date3 = formatter1.parse(new_date)
                println("Date  ::"+date3)*/
                bookLending.due_date = c.getTime()
                bookLending.bookitem=bookLending1?.bookitem
                bookLending.user=user
                bookLending.organization = user?.organization
                bookLending.fine_amount=0.0
                bookLending.creation_username = log.username
                bookLending.updation_username = log.username
                bookLending.creation_date = new java.util.Date()
                bookLending.updation_date = new java.util.Date()
                bookLending.creation_ip_address = request.getRemoteAddr()
                bookLending.updation_ip_address = request.getRemoteAddr()
                bookLending.save(flush:true,failOnError:true)
                hm.put('msg','200')
                return hm
            }
            return hm
        }
        return hm
    }
    //get bookDonation Data
    def bookDonation(request, hm,ip) {
        println("in bookDonation:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "book Donation"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                ArrayList books_list = new ArrayList()
                ArrayList auths = new ArrayList()
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                User user=User.findByUsername(log?.username)
                //def  bookDonationRequest = BookDonationRequest.findAllByUserAndIsactive(user,true)
                def  bookDonationRequest = BookDonationRequest.findAllByUser(user)
                int i=0
                println("bookDonationRequest ::"+bookDonationRequest)
                for (bdr in bookDonationRequest) {
                    HashMap temp = new HashMap()
                    temp.put('bdr_Id', bdr?.id)
                    temp.put('srno', i++)
                    temp.put('number_of_copies', bdr?.number_of_copies)
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd")
                    temp.put('request_date', dateFormat1.format(bdr?.request_date))
                    temp.put('request_date_date', dateFormat.format(bdr?.request_date))
                    temp.put('expected_delivery_date', dateFormat1.format(bdr?.expected_delivery_date))
                    temp.put('displey_expected_delivery_date', dateFormat.format(bdr?.expected_delivery_date))
                    temp.put('bookcondition',bdr?.bookcondition?.name)
                    temp.put('bookcondition_id', bdr?.bookcondition?.id)
                    temp.put('isactive', bdr?.isactive)
                    books_list.add(temp)

                }
                def  bookCondition=BookCondition.findAllByIsactive(true)
                def allbc=[]
                for(bc in bookCondition)
                {
                    HashMap temp1= new HashMap()
                    temp1.put('bc_Id', bc?.id)
                    temp1.put('name', bc?.name)
                    allbc.add(temp1)
                }
                hm.put('msg','200')
                hm.put('booksList',books_list)
                hm.put('allbc',allbc)
                return hm
            }
            return hm
        }
        return hm
    }
    //Save Book Donate Data.
    def savebookdata(request, hm, ip) {
        println("in savebookdata:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:" + dataresponse
        AuthService auth = new AuthService()
        String purpose = "savebookdata"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
                BookDonationRequest bookDonationRequest=new BookDonationRequest()
                bookDonationRequest.number_of_copies = Integer.parseInt(dataresponse?.noofcopy)
                bookDonationRequest.request_date = new java.util.Date()
                Date date2 = formatter.parse(dataresponse?.delv_date);
                bookDonationRequest.expected_delivery_date = date2
                bookDonationRequest.isactive = true
                bookDonationRequest.user = User.findByUsername(log?.username)
                def condition = BookCondition.findByName(dataresponse.bc_id)
                if(!condition){
                    condition = new BookCondition()
                    condition.isactive = true
                    condition.name = dataresponse.bc_id?.trim()
                    condition.updation_ip_address = ip
                    condition.creation_ip_address = ip
                    condition.updation_date = new Date()
                    condition.creation_date = new Date()
                    condition.updation_username = uid
                    condition.creation_username = uid
                    condition.save(flush: true, failOnError: true)
                }
                bookDonationRequest.bookcondition = condition
                bookDonationRequest.creation_username = uid
                bookDonationRequest.updation_username = uid
                bookDonationRequest.creation_date = new java.util.Date()
                bookDonationRequest.updation_date = new java.util.Date()
                bookDonationRequest.creation_ip_address = ip
                bookDonationRequest.updation_ip_address = ip
                bookDonationRequest.organization = user?.organization
                bookDonationRequest.save(failOnError: true, flush: true)
                HashMap status = new HashMap()
                hm.put("msg", "200")
                hm.put("message", "Book Donation Request added successfully...")
                hm.put("status", status)
                return hm
            }
            return hm

        }
        return hm
    }
    //Actie In-Active
    def donatebookIsactive(request, hm,ip) {
        println "i am in donatebookIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "donatebookIsactive."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def bookDonationRequest = BookDonationRequest.findById(dataresponse?.bdr_Id)
                if (bookDonationRequest != null) {
                    if (bookDonationRequest?.isactive == false) {
                        bookDonationRequest.isactive = true
                        bookDonationRequest.updation_ip_address = request.getRemoteAddr()
                        bookDonationRequest.updation_date = new Date()
                        bookDonationRequest.updation_username = uid
                        bookDonationRequest.save(flush: true, failOnError: true)
                    } else {
                        bookDonationRequest.isactive = false
                        bookDonationRequest.updation_ip_address = request.getRemoteAddr()
                        bookDonationRequest.updation_date = new Date()
                        bookDonationRequest.updation_username = uid
                        bookDonationRequest.save(flush: true, failOnError: true)
                    }
                }
            }
            hm.put("status", "200")
            return hm

        }
        return hm
    }
    //Update Donation
    def updatedata(request, hm,ip) {
        println("Update Book Donate")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:" + dataresponse
        AuthService auth = new AuthService()
        String purpose = "update book data"
        auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
                BookDonationRequest bookDonationRequest= BookDonationRequest.findById(dataresponse?.bdid)
                println("bookDonationRequest::"+bookDonationRequest.number_of_copies)
                if (bookDonationRequest!=null) {
                    if(bookDonationRequest.number_of_copies!=dataresponse?.noofcopy){
                        println("if")
                        bookDonationRequest.number_of_copies = Integer.parseInt(dataresponse?.noofcopy)
                    }
                    else
                    {
                        println("else")
                        bookDonationRequest.number_of_copies = dataresponse?.noofcopy
                    }
                    Date date2 = formatter.parse(dataresponse?.delv_date);
                    bookDonationRequest.expected_delivery_date = date2
                    bookDonationRequest.bookcondition = BookCondition.findById(dataresponse.bc_id)
                    bookDonationRequest.updation_username = uid
                    bookDonationRequest.updation_date = new java.util.Date()
                    bookDonationRequest.updation_ip_address = ip
                    bookDonationRequest.save(failOnError: true, flush: true)
                    HashMap status = new HashMap()
                    hm.put("msg", "200")
                    hm.put("message", "Book Donation Request added successfully...")
                    hm.put("status", status)
                    return hm
                }
            }
        }
    }
}
