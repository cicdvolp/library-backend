package library
import grails.gorm.MultiTenant
class Publisher implements MultiTenant<Publisher>
{
    String name     //Tata Macgrawl Hill
    boolean isactive
    int display_order
    String short_name
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static mapping = {
        isactive defaultValue: true
        display_order defaultValue: 0

    }
    static constraints = {

        short_name nullable: true
    }
}
