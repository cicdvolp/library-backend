package library
import grails.gorm.MultiTenant
class Organization implements MultiTenant<Organization>
{
    boolean isactive
    boolean ismanagement
    String tenantid    //TenantClient
    String tenantname  //TenantClient

    String name
    String displayname
    String address
    String contact_person_name
    String email
    String contact
    String website
    String country
    String loginattachementdomain   //@vit.edu
    String logofilepath
    String logofilename

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    String erpwebsiteurl

    static constraints = {
        displayname nullable: true
        tenantid nullable: true
        tenantname nullable: true
        name nullable: true
        address nullable: true
        contact_person_name nullable: true
        email nullable: true
        contact nullable: true
        website nullable: true
        country nullable: true
        loginattachementdomain nullable: true
        library nullable: true
        erpwebsiteurl nullable: true
    }

    static belongsTo=[library:Library]
    static mapping = {
        isactive defaultValue: true
        ismanagement defaultValue: false
    }
}
