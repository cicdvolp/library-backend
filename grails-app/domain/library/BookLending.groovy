package library
import grails.gorm.MultiTenant
class BookLending implements MultiTenant<BookLending>
{
    Date borrowing_date
    Date due_date
    Date return_date

    Double fine_amount
    boolean isactive
    String remark

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[user:User,bookitem:BookItem,organization:Organization]
    static constraints = {
        borrowing_date nullable:true
        due_date nullable:true
        return_date nullable:true
        remark nullable: true
        organization nullable: true
    }
    static mapping = {
        isactive defaultValue: false
        fine_amount defaultValue: 0
    }
}
