package library
import grails.gorm.multitenancy.Tenants
import grails.converters.JSON
import grails.io.IOUtils
import groovy.json.JsonSlurper
import grails.gorm.transactions.Transactional
import com.opencsv.CSVReader

import javax.servlet.http.Part
import java.nio.file.Paths;
@Transactional
class AdminService {

    def serviceMethod() {

    }

    def findusertype(request, hm)
    {
        println("Inside AdminService/findusertype...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def usertype = UserType.findAll()
                for (ut in usertype) {
                    HashMap temp = new HashMap()
                    temp.put("userType", ut.name)
                    temp.put("userTypeId", ut.id)
                    utlist.add(temp)
                }
                hm.put('userType', utlist)

                def program = Program.findAll()
                ArrayList programList = new ArrayList()
                for (item in program) {
                    HashMap temp = new HashMap()
                    temp.put("program", item.name)
                    temp.put("id", item.id)
                    programList.add(temp)
                }
                // find all user of the organization
                println "user.organization------" + user.organization
                def alluser = User.findAllByOrganization(user.organization)
                def userlist = []
                for (usr in alluser) {
                    HashMap temp = new HashMap()
                    temp.put("userid", usr.id)
                    temp.put("user", usr.username)
                    HashMap ut = new HashMap()
                    ut.put('userTypeId', usr?.usertype?.id)
                    ut.put('userType', usr.usertype.name)
                    temp.put("usertype", ut)
                    Employee employee = Employee.findByUserAndOrganization(usr, user.organization)
                    temp.put("regno", employee?.employee_code)
                    userlist.add(temp)
                }
                hm.put('userlist', userlist)
                hm.put('programList', programList)
                //end of code
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }
    def findusertype_new(request, hm) {
        println("Inside AdminService/findusertype_new...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            def orglist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def usertype = UserType.findAll()
                for (ut in usertype) {
                    HashMap temp = new HashMap()
                    temp.put("userType", ut.name)
                    temp.put("userTypeId", ut.id)
                    utlist.add(temp)
                }
                hm.put('userType', utlist)
                def allorg=Organization.list()
                for(org in allorg){
                    HashMap temp=new HashMap()
                    temp.put('org_id',org?.id)
                    temp.put('org_name',org?.name)
                    orglist.add(temp)
                }
                hm.put('orglist', orglist)
                println "user.organization------" + user.organization
                //def alluser = User.findAllByOrganization(user.organization)
                def alluser = User.list()
                def userlist = []
                for (usr in alluser) {
                    HashMap temp = new HashMap()
                    temp.put("userid", usr?.id)
                    temp.put("user", usr?.username)
                    temp.put("organization", usr?.organization?.name)
                    temp.put("org", usr?.organization?.id)
                    HashMap ut = new HashMap()
                    ut.put('userTypeId', usr?.usertype?.id)
                    ut.put('userType', usr?.usertype.name)
                    temp.put("usertype", ut)
                    Employee employee = Employee.findByUser(usr)
                    temp.put("regno", employee?.employee_code)
                    userlist.add(temp)
                }
                hm.put('userlist', userlist)
                //end of code
                //println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def findusername(request, hm) {
        println("Inside AdminService/find username...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        def username = dataresponse["username"]
        def memberId = dataresponse["memberId"]

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)

            //def organization = user.organization
            Tenants.withId(log.tenantclient.tenantid) {
                User logeduser = User.findByUsername(log.username)
                User user = User.findByUsernameAndOrganization(username + logeduser.organization.loginattachementdomain, logeduser.organization)

                Employee employee = Employee.findByEmployee_codeAndOrganization(memberId, logeduser.organization)
                if (user) {
                    hm.put('message', 'User name is not available!')
                    hm.put('code', 1)
                } else {
                    if (employee) {
                        hm.put('message', 'User already registered')
                        hm.put('code', 1)
                    } else {
                        hm.put('message', 'User name is available')
                        hm.put('code', 2)
                    }
                }
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }
    def findusername_new(request, hm) {
        println("Inside AdminService/findusername_new...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        String purpose = 'Add New User'
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        def username = dataresponse["username"]
        def memberId = dataresponse["memberId"]
        println("dataresponse :"+dataresponse)
        //auth.checkauth(token, uid, hm, url, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)

            //def organization = user.organization
            Tenants.withId(log.tenantclient.tenantid) {
                User logeduser = User.findByUsername(log.username)
                Organization org=Organization.findById(dataresponse.org)
                User user = User.findByUsernameAndOrganization(username + logeduser.organization.loginattachementdomain, org)
                Employee employee = Employee.findByEmployee_codeAndOrganization(memberId, org)
                if (user) {
                    hm.put('message', 'User name is not available!')
                    hm.put('code', 1)
                } else {
                    if (employee) {
                        hm.put('message', 'User already registered')
                        hm.put('code', 1)
                    } else {
                        hm.put('message', 'User name is available')
                        hm.put('code', 2)
                    }
                }
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def saveUsername(request, ip, hm) {
        println("Inside AdminService/save username...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        String loginattachmentdomainname = ""

        def username = dataresponse["username"]
        def memberId = dataresponse["memberId"]
        def selectedusertype = dataresponse["selectedusertype"]

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            //def organization = user.organization
            Tenants.withId(log.tenantclient.tenantid) {
                def logeduser = User.findByUsername(log.username)
                UserType usertype = UserType.findById(selectedusertype.userTypeId)
                loginattachmentdomainname = logeduser.organization.loginattachementdomain
                createLogin(uid, username, ip, loginattachmentdomainname, log.tenantclient,nextRecord[0].trim())
                User user = User.findByUsernameAndOrganization(username + logeduser.organization.loginattachementdomain, logeduser.organization)

                if (user) {
                    hm.put('message', 'User already available!')
                    hm.put('code', 1)
                } else {
                    user = new User()
                    user.username = username + loginattachmentdomainname
                    user.total_issued_books = 0
                    user.creation_username = uid
                    user.updation_username = uid
                    user.creation_date = new Date()
                    user.updation_date = new Date()
                    user.creation_ip_address = ip
                    user.updation_ip_address = ip
                    user.organization = logeduser.organization
                    user.usertype = usertype
                    user.save(flush: true, failOnError: true)

                    Person person = new Person()
                    person.creation_username = uid
                    person.updation_username = uid
                    person.creation_date = new Date()
                    person.updation_date = new Date()
                    person.creation_ip_address = ip
                    person.updation_ip_address = ip
                    person.user = user
                    person.save(flush: true, failOnError: true)

                    Program program =  Program.findById(dataresponse.programId)
                    if(usertype?.name == 'Member'){
                        Member member = new Member()
                        member.registration_number = memberId
                        member.name = username
                        member.program =program
                        member.date_of_membership = new Date()
                        member.creation_username = uid
                        member.updation_username = uid
                        member.creation_date = new Date()
                        member.updation_date = new Date()
                        member.creation_ip_address = ip
                        member.updation_ip_address = ip
                        member.organization = logeduser.organization
                        member.user = user
                        member.person = person
                        member.save(flush: true, failOnError: true)
                    }
                    if(usertype?.name == 'Employee'){
                        Employee employee = new Employee()
                        employee.employee_code = memberId
                        employee.name = username
                        employee.program =program
                        employee.creation_username = uid
                        employee.updation_username = uid
                        employee.creation_date = new Date()
                        employee.updation_date = new Date()
                        employee.creation_ip_address = ip
                        employee.updation_ip_address = ip
                        employee.organization = logeduser.organization
                        employee.user = user
                        employee.person = person
                        employee.save(flush: true, failOnError: true)
                    }


                    //Role role = Role.findByNameAndUsertype
                    Role role
                    if (usertype.name.equalsIgnoreCase("Employee")) {
                        //role = Role.findByNameIlikeAndUsertype("%Teacher%",usertype)
                        role = Role.findByNameIlike("%Teacher%")
                    } else {
                        role = Role.findByNameIlike("%Member%")
                        //role = Role.findByNameIlikeAndUsertype("%Member%",usertype)
                    }
                    user.addToRole(role)
                    user.save(flush: true, failOnError: true)
                }
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }
    def saveUsername_new(request, ip, hm) {
        println("Inside saveUsername_new...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        String loginattachmentdomainname = ""
        String loginattachmentdomainname_selected_org = ""

        def username = dataresponse["username"]
        def memberId = dataresponse["memberId"]
        def selectedusertype = dataresponse["selectedusertype"]

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            //def organization = user.organization
            Tenants.withId(log.tenantclient.tenantid) {
                def logeduser = User.findByUsername(log.username)
                Organization org=Organization.findById(dataresponse.org)
                UserType usertype = UserType.findById(selectedusertype.userTypeId)
                loginattachmentdomainname = logeduser.organization.loginattachementdomain
                loginattachmentdomainname_selected_org = org.loginattachementdomain
                createLogin_other_org(uid, username, ip, loginattachmentdomainname_selected_org, log.tenantclient,memberId)
                User user = User.findByUsernameAndOrganization(username + logeduser.organization.loginattachementdomain, org)

                if (user) {
                    hm.put('message', 'User already available!')
                    hm.put('code', 1)
                } else {
                    user = new User()
                    //user.username = username + loginattachmentdomainname
                    user.username = username + org?.loginattachementdomain
                    user.total_issued_books = 0
                    user.creation_username = uid
                    user.updation_username = uid
                    user.creation_date = new Date()
                    user.updation_date = new Date()
                    user.creation_ip_address = ip
                    user.updation_ip_address = ip
                    //user.organization = logeduser.organization
                    user.organization = org
                    user.usertype = usertype
                    user.save(flush: true, failOnError: true)

                    Person person = new Person()
                    person.creation_username = uid
                    person.updation_username = uid
                    person.creation_date = new Date()
                    person.updation_date = new Date()
                    person.creation_ip_address = ip
                    person.updation_ip_address = ip
                    person.user = user
                    person.save(flush: true, failOnError: true)

                    Employee employee = new Employee()
                    employee.employee_code = memberId
                    employee.name = username
                    employee.creation_username = uid
                    employee.updation_username = uid
                    employee.creation_date = new Date()
                    employee.updation_date = new Date()
                    employee.creation_ip_address = ip
                    employee.updation_ip_address = ip
                    //employee.organization = logeduser.organization
                    employee.organization = org
                    employee.user = user
                    employee.person = person
                    employee.save(flush: true, failOnError: true)

                    //Role role = Role.findByNameAndUsertype
                    Role role
                    if (usertype.name.equalsIgnoreCase("Employee")) {
                        //role = Role.findByNameIlikeAndUsertype("%Teacher%",usertype)
                        role = Role.findByNameIlike("%Teacher%")
                    } else {
                        role = Role.findByNameIlike("%Member%")
                        //role = Role.findByNameIlikeAndUsertype("%Member%",usertype)
                    }
                    user.addToRole(role)
                    user.save(flush: true, failOnError: true)
                }
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def createLogin(String uid, String username, String ip, String loginattachmentname, TenantClient tenantclient,String grno) {
        println "i am in login creation"
        Login login
        if(username?.contains("@"))
            login = Login.findByUsername(username)
        else
            login = Login.findByUsername(username + loginattachmentname)
        if (login == null) {
            login = new Login()
            if(username?.contains("@")){
                login.username = username
                login.password = username
            }else {
                login.username = username + loginattachmentname
                login.password = username + loginattachmentname
            }
            login.grno_empid = grno
            login.creation_username = uid
            login.updation_username = uid
            login.creation_date = new Date()
            login.updation_date = new Date()
            login.creation_ip_address = ip
            login.updation_ip_address = ip
            login.tenantclient = tenantclient
            login.save(flush: true, failOnError: true)
        }
    }
    def createLogin_other_org(String uid, String username, String ip, String loginattachmentname, TenantClient tenantclient,grno) {
        println "i am in login creation"
        Login login = Login.findByUsername(username + loginattachmentname)
        if (login == null) {
            login = new Login()
            login.username = username + loginattachmentname
            login.password = username + loginattachmentname
            login.grno_empid = grno
            login.creation_username = uid
            login.updation_username = uid
            login.creation_date = new Date()
            login.updation_date = new Date()
            login.creation_ip_address = ip
            login.updation_ip_address = ip
            login.tenantclient = tenantclient
            login.save(flush: true, failOnError: true)
        }
    }

    def updateUtype(request, ip, hm) {
        println("Inside AdminService/update utype registration number and emp code...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        println dataresponse

        String loginattachmentdomainname = ""

        def regno = dataresponse["regno"]
        def userid = dataresponse["userid"]
        def usertypeid = dataresponse["usertype"]

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            //def organization = user.organization
            Tenants.withId(log.tenantclient.tenantid) {
                def logeduser = User.findByUsername(log.username)
                UserType usertype
                User user = User.findById(userid)
                Employee employee = Employee.findByUserAndOrganization(user, user.organization)
                Employee same_employee_code = Employee.findByEmployee_codeAndOrganization(regno, user.organization)
                println "same emp code" + same_employee_code
                if (employee && same_employee_code == null) {
                    //Role role = Role.findByNameAndUsertype
                    employee.employee_code = regno
                    employee.save(flush: true, failOnError: true)
                    Role role
                    usertype = UserType.findById(usertypeid.userTypeId)
                    if (usertype.name.equalsIgnoreCase("Employee")) {
                        //role = Role.findByNameIlikeAndUsertype("%Teacher%",usertype)
                        role = Role.findByNameIlike("%Teacher%")
                    } else {
                        role = Role.findByNameIlike("%Member%")
                        //role = Role.findByNameIlikeAndUsertype("%Member%",usertype)
                    }
//                        for (r in user.role) {
//                            user.removeFromRole(r)
//                        }
                    user.addToRole(role)
                    user.usertype = usertype
                    user.save(flush: true, failOnError: true)
                    hm.put('message', 'Successfully updated record!')
                    hm.put('code', 1)
                } else {
                    hm.put('message', 'Employee entry is not found/ Employee code or registration number already present!')
                    hm.put('code', 2)
                }
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }
    def updateUtype_new(request, ip, hm) {
        println("Inside AdminService/update utype registration number and emp code New...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        println dataresponse

        String loginattachmentdomainname = ""
        println("dataresponse ::"+dataresponse)
        def regno = dataresponse["regno"]
        def userid = dataresponse["userid"]
        def usertypeid = dataresponse["usertype"]

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            //def organization = user.organization
            Tenants.withId(log.tenantclient.tenantid) {
                def logeduser = User.findByUsername(log.username)
                UserType usertype
                Organization org=Organization.findById(dataresponse.org)
                User user = User.findById(userid)
                //Employee employee = Employee.findByUserAndOrganization(user,org)
                Employee employee = Employee.findByUser(user)
                //Employee same_employee_code = Employee.findByEmployee_codeAndOrganization(regno, org)
                Employee same_employee_code = Employee.findByEmployee_code(regno)
                println "same emp code ::" + same_employee_code
                println "employee ::" + employee
                if (employee != null && same_employee_code != null) {
                    //Role role = Role.findByNameAndUsertype
                    employee.employee_code = dataresponse.regno
                    employee.organization = org
                    employee.save(flush: true, failOnError: true)
                    Role role
                    usertype = UserType.findById(usertypeid.userTypeId)
                    if (usertype.name.equalsIgnoreCase("Employee")) {
                        //role = Role.findByNameIlikeAndUsertype("%Teacher%",usertype)
                        role = Role.findByNameIlike("%Teacher%")
                    } else {
                        role = Role.findByNameIlike("%Member%")
                        //role = Role.findByNameIlikeAndUsertype("%Member%",usertype)
                    }
//                        for (r in user.role) {
//                            user.removeFromRole(r)
//                        }
                    user.addToRole(role)
                    user.usertype = usertype
                    user.organization = org
                    user.save(flush: true, failOnError: true)
                    hm.put('message', 'Successfully updated record!')
                    hm.put('code', 1)
                } else {
                    hm.put('message', 'Employee entry is not found/ Employee code or registration number already present!')
                    hm.put('code', 2)
                }
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def adminMasterMenu(request, ip, hm) {
        println "i am in AdminMasterMenu"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String purpose = "Getting Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList routerLinkList = new ArrayList()
                int col_size = 3
                HashMap temp = new HashMap()
                temp.put('name', 'user-type')
                temp.put('textval', 'User Type')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'UT')
                temp.put('flex', col_size)
                temp.put('size', UserType.findAllWhere(
                        isactive: true
                ).size())
                routerLinkList.add(temp)
                temp = new HashMap()
                temp.put('name', 'publisher')
                temp.put('textval', 'Publisher')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'P')
                temp.put('flex', col_size)
                temp.put('size', Publisher.findAllWhere(
                        isactive: true
                ).size())
                routerLinkList.add(temp)
                temp = new HashMap()
                temp.put('name', 'author')
                temp.put('textval', 'Author')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'A')
                temp.put('flex', col_size)
                temp.put('size', Author.findAllWhere(
                        isactive: true
                ).size())

                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'book-type')
                temp.put('textval', 'Book Type')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'BT')
                temp.put('flex', col_size)
                temp.put('size', BookType.findAllWhere(
                        organization : user?.organization,
                        isactive: true
                ).size())

                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'erp-department')
                temp.put('textval', 'ERPDepartment')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'L')
                temp.put('flex', col_size)
                temp.put('size', ERPDepartment.findAllWhere(organization: user?.organization).size())

                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'role')
                temp.put('textval', 'Role')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'R')
                temp.put('flex', col_size)
                temp.put('size', Role.findAllWhere(
                        isactive: true
                ).size())

                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'book-category')
                temp.put('textval', 'Book Category')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'BC')
                temp.put('flex', col_size)
                temp.put('size', BookCategory.findAllWhere(
                        organization : user?.organization,
                        isactive: true
                ).size())

                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'role-liks-master')
                temp.put('textval', 'Role Links')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'RL')
                temp.put('flex', col_size)
                temp.put('size', RoleLinks.findAllWhere(
                        isactive: true
                ).size())
                routerLinkList.add(temp)


                temp = new HashMap()
                temp.put('name', 'book-format')
                temp.put('textval', 'Book Format')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'BF')
                temp.put('flex', col_size)
                temp.put('size', BookFormat.findAllWhere(
                        isactive: true
                ).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'book-status')
                temp.put('textval', 'Book Status')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'BS')
                temp.put('flex', col_size)
                temp.put('size', BookStatus.findAllWhere(
                        isactive: true
                ).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'reservation-status')
                temp.put('textval', 'Reservation Status')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'RS')
                temp.put('flex', col_size)
                temp.put('size', ReservationStatus.findAllWhere(
                        isactive: true
                ).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'account-status')
                temp.put('textval', 'Account Status')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'AS')
                temp.put('flex', col_size)
                temp.put('size', AccountStatus.findAllWhere(
                        isactive: true
                ).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'book-condition')
                temp.put('textval', 'Book Condition')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'BC')
                temp.put('flex', col_size)
                temp.put('size', BookCondition.findAllWhere(
                        isactive: true
                ).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'currency')
                temp.put('textval', 'Currency')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'C')
                temp.put('flex', col_size)
                temp.put('size', Currency.findAllWhere(
                        isactive: true
                ).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'transaction-type')
                temp.put('textval', 'Transaction Type')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'TT')
                temp.put('flex', col_size)
                temp.put('size', TransactionType.findAllWhere(
                        isactive: true
                ).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'notification-type')
                temp.put('textval', 'Notification Type')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'NT')
                temp.put('flex', col_size)
                temp.put('size', NotificationType.findAllWhere(
                        isactive: true
                ).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'rack')
                temp.put('textval', 'Rack')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'R')
                temp.put('flex', col_size)
                temp.put('size', Rack.findAllWhere(
                        isactive: true
                ).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'notification-template')
                temp.put('textval', 'Notification template')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'NT')
                temp.put('flex', col_size)
                temp.put('size', NotificationTemplate.findAllWhere(
                        isactive: true
                ).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'notification-method')
                temp.put('textval', 'Notification method')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'NM')
                temp.put('flex', col_size)
                temp.put('size', NotificationMethod.findAllWhere(
                        isactive: true
                ).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'program')
                temp.put('textval', 'Program')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'P')
                temp.put('flex', col_size)
                temp.put('size', Program.findAllByOrganization(user.organization).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'programType')
                temp.put('textval', 'Program Type')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'PT')
                temp.put('flex', col_size)
                temp.put('size', ProgramType.findAllByOrganization(user.organization).size())
                routerLinkList.add(temp)

                
                temp = new HashMap()
                temp.put('name', 'book-series')
                temp.put('textval', 'Book Series')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'BS')
                temp.put('flex', col_size)
                temp.put('size', BookSeries.findAll().size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'physicalDetails')
                temp.put('textval', 'Physical Details')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'PD')
                temp.put('flex', col_size)
                temp.put('size', PhysicalDetails.findAll().size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'accompanyingMaterials')
                temp.put('textval', 'Accompanying Materials')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'AM')
                temp.put('flex', col_size)
                temp.put('size', AccompanyingMaterials.findAll().size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'Series')
                temp.put('textval', 'Series')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'S')
                temp.put('flex', col_size)
                temp.put('size', Series.findAllByOrganization(user?.organization).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'vendor-type')
                temp.put('textval', 'Book Vendor Type')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'VT')
                temp.put('flex', col_size)
                temp.put('size', BookVendorType.findAll().size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'vendor')
                temp.put('textval', 'Book Vendor')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'BV')
                temp.put('flex', col_size)
                temp.put('size', BookVendor.findAll().size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'library-department')
                temp.put('textval', 'Library Department')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'LD')
                temp.put('flex', col_size)
                temp.put('size', LibraryDepartment.findAllByOrganization(user?.organization).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'AcademicYear')
                temp.put('textval', 'Academic Year')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'AY')
                temp.put('flex', col_size)
                temp.put('size', AcademicYear.findAll().size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'Year')
                temp.put('textval', 'Year')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'Y')
                temp.put('flex', col_size)
                temp.put('size', Year.findAllByOrganization(user?.organization).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'Semester')
                temp.put('textval', 'Semester')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'S')
                temp.put('flex', col_size)
                temp.put('size', Semester.findAllByOrganization(user?.organization).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'Shift')
                temp.put('textval', 'Shift')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'S')
                temp.put('flex', col_size)
                temp.put('size', Shift.findAllByOrganization(user?.organization).size())
                routerLinkList.add(temp)

                //madhuri
                temp = new HashMap()
                temp.put('name', 'book-classification')
                temp.put('textval', 'Book Classification')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'B')
                temp.put('flex', col_size)
                temp.put('size', BookClassification.findAllByOrganization(user?.organization).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'weedoutcode')
                temp.put('textval', 'Weedoutcode')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'W')
                temp.put('flex', col_size)
                temp.put('size', Weedoutcode.findAllByOrganization(user?.organization).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'frequency')
                temp.put('textval', 'Frequency')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'F')
                temp.put('flex', col_size)
                temp.put('size', Frequency.findAllByOrganization(user?.organization).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'libraryPolicyType')
                temp.put('textval', 'LibraryPolicyType')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'LPT')
                temp.put('flex', col_size)
                temp.put('size', LibraryPolicyType.findAllByOrganization(user?.organization).size())
                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'Language')
                temp.put('textval', 'Language')
                temp.put('icon', 'mdi-alpha-l-circle')
                temp.put('icontext', 'L')
                temp.put('flex', col_size)
                temp.put('size', Language.findAllByOrganization(user?.organization).size())
                routerLinkList.add(temp)
                //end
//                routerLinkList= routerLinkList.sort({it.icontext})
                hm.put('routerLinkList', routerLinkList)

                return hm

            }
        }
        return hm
    }

    def getMasterUserType(request, hm) {
        println("Inside AdminService/findusertype...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)



        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {

            def usertype_SA = UserTypeSuperMaster.findAll()
            def utlist_SA = []
            for (ut in usertype_SA) {
                HashMap temp = new HashMap()
                temp.put("userType", ut.name)
                temp.put("userTypeId", ut.id)
                utlist_SA.add(temp)
            }
            hm.put('userType_SA', utlist_SA)

            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def usertype = UserType.findAll()
                int srno = 1
                for (ut in usertype) {
                    HashMap temp = new HashMap()
                    temp.put("userType", ut.name)
                    temp.put("userTypeId", ut.id)
                    temp.put("userTypeIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    utlist.add(temp)
                }
                hm.put('userType', utlist)
                println "hm: " + hm
                hm.put("msg", "200")



                return hm
            }
        }
    }

    def editAdminUserTypeOperation(request, hm) {
        println "i am in editAdminUserTypeOperation"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "Edit User Types from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                hm.put("org", org)

                def userType = UserType.findById(dataresponse?.userTypeId)
                if (userType != null) {
                    if (userType?.isactive == false) {
                        userType.isactive = true
                        userType.updation_ip_address = request.getRemoteAddr()
                        userType.updation_date = new Date()
                        userType.updation_username = uid
                        userType.save(flush: true, failOnError: true)
                    } else {
                        userType.isactive = false
                        userType.updation_ip_address = request.getRemoteAddr()
                        userType.updation_date = new Date()
                        userType.updation_username = uid
                        userType.save(flush: true, failOnError: true)
                    }
                }

                def usertype = UserType.findAll()
                int srno = 1
                for (ut in usertype) {
                    HashMap temp = new HashMap()
                    temp.put("userType", ut.name)
                    temp.put("userTypeId", ut.id)
                    temp.put("userTypeIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    utlist.add(temp)
                }
                hm.put("user_list", utlist)
                hm.put("msg", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def addUserType(request, hm, ip) {
        println "i am in addUserType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "Edit User Types from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
//                def org = user.organization
//                hm.put("org",org)

                UserType userType = UserType.findByName(dataresponse?.userType_name)
                if (userType != null) {
                    hm.put("msg", "User type already exist!!")
                } else {
                    userType = new UserType()
                    userType.isactive = true
                    userType.name = dataresponse?.userType_name
                    userType.updation_ip_address = ip
                    userType.creation_ip_address = ip
                    userType.updation_date = new Date()
                    userType.creation_date = new Date()
                    userType.updation_username = uid
                    userType.creation_username = uid
                    userType.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def usertypeList = UserType.findAll()
                int srno = 1
                for (ut in usertypeList) {
                    HashMap temp = new HashMap()
                    temp.put("userType", ut.name)
                    temp.put("userTypeId", ut.id)
                    temp.put("userTypeIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    utlist.add(temp)
                }
                hm.put("user_list", utlist)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def getMasterPublisher(request, hm) {
        println("Inside AdminService/findusertype...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def usertype = UserType.findAll()
                int srno = 1
                for (ut in usertype) {
                    HashMap temp = new HashMap()
                    temp.put("userType", ut.name)
                    temp.put("userTypeId", ut.id)
                    temp.put("userTypeIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    utlist.add(temp)
                }
                hm.put('userType', utlist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def editUsertype(request, hm, ip) {
        println "i am in addUserType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "Edit User Types from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                UserType userType = UserType.findById(dataresponse?.userTypeId)

                def utype = UserType.findByName(dataresponse?.usertType)
                println('dataresponse?.usertType---' + dataresponse?.usertType)
                println('dataresponse?.id---' + dataresponse?.userTypeId)

                if (utype != null) {
                    hm.put("msg", "User type already exist!!")
                } else {
                    userType.name = dataresponse?.usertType
                    userType.updation_ip_address = ip
                    userType.updation_date = new Date()
                    userType.updation_username = uid
                    userType.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def usertypeList = UserType.findAll()
                int srno = 1
                for (ut in usertypeList) {
                    HashMap temp = new HashMap()
                    temp.put("userType", ut.name)
                    temp.put("userTypeId", ut.id)
                    temp.put("userTypeIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    utlist.add(temp)
                }
                hm.put("user_list", utlist)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def deleteUserType(request ,hm){
        println "i am in deleteUserType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteUserType from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                UserType userType = UserType.findById(dataresponse?.userTypeId)
                println("userType : " + userType)
                if(userType!=null) {
                    try{
                        userType.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return
            }
        }
    }
    //madhuri
    def bookclassification(request, hm){
        println("Inside bookclassification...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)
        String purpose = "add classification "
        auth.checkauth(token, uid, hm, url, request, purpose)

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization
                def bookClassificationList = []

                def bclist = BookClassification.findAllByOrganization(organization)
                def srno = 1
                for (bc in bclist) {
                    HashMap temp = new HashMap()
                    temp.put("name", bc.name)
                    temp.put("Id", bc.id)
                    temp.put("Isactive", bc.isactive)
                    temp.put("srno", srno++)
                    temp.put("organization",bc.organization)
                    bookClassificationList.add(temp)
                }
                hm.put("bookClassificationList",bookClassificationList)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }

    }
    def savebookclassification(request, hm ,ip){
        println("Inside bookclassification...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        String purpose = "save savebookclassification."
        println("savebookclassification dataresponse"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)

        println("savebookclassification dataresponse"+dataresponse)

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization

                BookClassification bkc=BookClassification.findByNameAndOrganization(dataresponse.bookclassification,organization)
                if(bkc==null){
                    bkc=new BookClassification()
                    bkc.name=dataresponse.bookclassification
                    bkc.isactive=true
                    bkc.organization=organization
                    bkc.creation_username = uid
                    bkc.updation_username = uid
                    bkc.creation_date = new Date()
                    bkc.updation_date = new Date()
                    bkc.creation_ip_address = ip
                    bkc.updation_ip_address = ip
                    bkc.save(flush: true, failOnError: true)
                    hm.put("code", "200")
                    hm.put("msg", "saved")
                }
                else{
                    hm.put("code", "200")
                    hm.put("msg", "already exist")
                }
                println "hm: " + hm
                return hm
            }
        }

    }
    def editbookclassification(request, hm,ip){
        println("edit bookclassification...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        String purpose = "edit bookclassification."
        println("editbookclassification dataresponse"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
//            println("dataresponse",+dataresponse.name)
            Tenants.withId(log.tenantclient.tenantid) {
//                println("dataresponse")
                def user = User.findByUsername(log.username)
                def organization = user.organization
//                println("dataresponse",+dataresponse.name)
               // BookClassification bclist = BookClassification.findByOrganizationAndId(organization,id)
                BookClassification bkc=BookClassification.findById(dataresponse.Id)
//                println("dataresponse",+dataresponse.name)
                if(bkc!=null){
                    bkc.name=dataresponse.name
                    bkc.organization=organization
                    bkc.updation_username = uid
                    bkc.updation_date = new Date()
                    bkc.updation_ip_address = ip
                    bkc.save(flush: true, failOnError: true)
                    hm.put("code", "200")
                    hm.put("msg", "saved")
                }
                else{
                    hm.put("code", "200")
                    hm.put("msg", "already exist")
                }
                println "hm: " + hm
                return hm
            }
        }


    }
    def deletebookclassification(request, hm, ip){
        println "delete bookclassification"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "delete book classification from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def bclist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def bookClassification = BookClassification.findById(dataresponse?.bookclassificationId)

                def btype = BookClassification.findByName(dataresponse?.bookClassification)
                println('dataresponse?.BookClassification---' + dataresponse?.bookClassification)
              //  println('dataresponse?.id---' + dataresponse?.bookclassificationId)

                println(dataresponse.deleteitem)
                def d = BookClassification.findById(dataresponse.deleteitem)
                if (d != null) {

                    d.delete(flush: true, failOnError: true)
                    hm.put("msg", "200")
                } else {
                    hm.put("msg", "error")
                }
                return hm
            }
        }

    }
    def bookclassificationIsactive(request, hm, ip){
        println("Isactive bookclassification...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        String purpose = "Isactive bookclassification."
        println("Isactive bookclassification dataresponse"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
//            println("dataresponse",+dataresponse.name)
            Tenants.withId(log.tenantclient.tenantid) {
//                println("dataresponse")
                def user = User.findByUsername(log.username)
                def organization = user.organization
//                println("dataresponse",+dataresponse.name)
                // BookClassification bclist = BookClassification.findByOrganizationAndId(organization,id)
                BookClassification bkc=BookClassification.findById(dataresponse.Id)

                if(bkc!=null) {
                    if(dataresponse.Isactive == false) {
                        bkc.isactive = false
                        bkc.organization = organization
                        bkc.updation_username = uid
                        bkc.updation_date = new Date()
                        bkc.updation_ip_address = ip
                        bkc.save(flush: true, failOnError: true)
                        hm.put("msg","200")
                    }
                    else{
                        bkc.isactive = true
                        bkc.organization = organization
                        bkc.updation_username = uid
                        bkc.updation_date = new Date()
                        bkc.updation_ip_address = ip
                        bkc.save(flush: true, failOnError: true)
                        hm.put("msg","200")
                    }
                }
                println "hm: " + hm
                return hm
            }
        }
    }
    //end

    def deletePublisher(request, hm){
        println "i am in deletePublisher"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deletePublisher from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                Publisher publisher = Publisher.findByName(dataresponse?.publisherName)
                println("publisher : " + publisher)
                if(publisher!=null) {
                    try{
                        publisher.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return
            }
        }
    }

    def fetchAllUser(request, hm) {
        println("Inside AdminService/find all user...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def userlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def loged_user = User.findByUsername(log.username)
//                def organization = user.organization
                def users = User.findAllByOrganization(loged_user.organization)
                //def users = User.list()
                for (user in users) {
                    def member =null
                    def name=""
                     member=Member.findByUserAndOrganization(user,user.organization)
                    if(member!=null) {
                        name = member.registration_number  +" : " + member?.name
                    }
                    if(member==null)
                    {
                        member=Employee.findByUserAndOrganization(user,user.organization)
                        if(member!=null)
                        {
                            name=member.employee_code+ " : " +member.name
                        }
                    }

                    HashMap temp = new HashMap()
                    temp.put("username", user.username)
                    temp.put("displayname", name)
                    temp.put("userid", user.id)
                    userlist.add(temp)
                }

                hm.put('userlist', userlist)
                def plist =[]
                def programlist = Program.findAllByOrganization(loged_user.organization)
                for(i in programlist){
                    HashMap temp = new HashMap()
                    temp.put("pname", i.name)
                    temp.put("pid", i.id)
                    plist.add(temp)
                }
                def ylist =[]
                def yearlist = Year.findAllByOrganization(loged_user.organization)
                for(i in yearlist){
                    HashMap temp = new HashMap()
                    temp.put("yname", i.year)
                    temp.put("yid", i.id)
                    ylist.add(temp)
                }

                hm.put('programList', plist)
                hm.put('yearList', ylist)
                println("hm.programList"+hm.programList)
                // find all role

//                def role_elements = []
//                int i = 1
//                for(r in rolelist)
//                {
//                    HashMap temp = new HashMap()
//                    temp.put("rolename",r?.name)
//                    temp.put("roledisplayname",r?.displayname)
//                    temp.put("roleid",r?.id)
//                    temp.put("check"+i,"check"+i)
//                    role_elements.add(temp)
//                    i++
//                }
//                hm.put('rolelist', role_elements)
                // all role end
                hm.put("msg", "200")
                return hm
            }
        }
    }
    def fetchAllUserwithname(request, hm) {
        println("Inside AdminService/find all user...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def userlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def loged_user = User.findByUsername(log.username)
//                def organization = user.organization
                def users = User.findAllByOrganization(loged_user.organization)
                //def users = User.list()
                for (user in users) {
                    def member =null
                    def name=""
                     member=Member.findByUserAndOrganization(user,user.organization)
                    if(member==null)
                        member=Employee.findByUserAndOrganization(user,user.organization)

                    HashMap temp = new HashMap()
                    temp.put("displayname", user?.username+":"+member?.name)
                    temp.put("id", user.id)
                    userlist.add(temp)
                }

                hm.put('userlist', userlist)

                hm.put("msg", "200")
                return hm
            }
        }
    }

    def fetchUserType(request, hm) {
        println("Inside AdminService/find user type...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        def userid = dataresponse["userid"]
        def role_elements = []
        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def loged_user = User.findByUsername(log.username)
//                def organization = user.organization
                User user = User.findById(userid)
                hm.put('usertype', user.usertype.name)
                int i = 1
                for (r in user.role) {
                    HashMap temp = new HashMap()
                    temp.put("rolename", r?.name)
                    temp.put("roledisplayname", r?.displayname)
                    temp.put("roleid", r?.id)
                    temp.put("check" + i, "check" + i)
                    role_elements.add(temp)
                    i++
                }
                //end of code
                hm.put("userrole", role_elements)
                def rolelist = Role.findAll()
                def role_element = []
                int j = 1
                for (r in rolelist) {
                    if (!(r.id in user.role.id)) {
                        HashMap temp = new HashMap()
                        temp.put("rolename", r?.name)
                        temp.put("roledisplayname", r?.displayname)
                        temp.put("roleid", r?.id)
                        temp.put("check" + j, "check" + j)
                        role_element.add(temp)
                        j++
                    }
                }
                hm.put('rolelist', role_element)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def assignRole(request, hm, ip) {
        println("Inside AdminService/find assign role...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        def userid = dataresponse["selecteduser"]
        def selectedroleid = dataresponse["selectedchkbox"]

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def loged_user = User.findByUsername(log.username)
                def organization = loged_user.organization
                User user = User.findById(userid.userid)
                for (r in selectedroleid) {
                    Role role = Role.findById(r)
                    user.addToRole(role)
                }
                user.updation_username = loged_user.username
                user.updation_ip_address = ip
                user.save(flush: true, failOnError: true)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def deleteUserRole(request, hm, ip) {
        println("Inside AdminService/delete user role...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        def roleid = dataresponse["item"].roleid
        def userid = dataresponse["selecteduser"].userid
        println dataresponse
        println roleid
        println userid
        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def loged_user = User.findByUsername(log.username)
                def organization = loged_user.organization
                User user = User.findById(userid)
                Role role = Role.findById(roleid)
                println user.role
                for (r in user?.role) {
                    if (r.id == role.id) {
                        user.removeFromRole(r)
                        user.updation_username = loged_user.username
                        user.updation_ip_address = ip
                        user.save(flush: true, failOnError: true)
                        println "id match"

                    }
                }
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def getPublisher(request, hm) {
        println("Inside AdminService/findusertype...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def publisherlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def publisher = Publisher.findAll()
                int srno = 1
                for (ut in publisher) {
                    HashMap temp = new HashMap()
                    temp.put("publisher", ut.name)
                    temp.put("display_order", ut.display_order)
                    temp.put("short_name", ut.short_name)
                    temp.put("publisherId", ut.id)
                    temp.put("publisherIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    publisherlist.add(temp)
                }
                hm.put('publisher', publisherlist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def addPublisher(request, hm, ip) {
        println "i am in addUserType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "Edit User Types from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def publisherlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
//                def org = user.organization
//                hm.put("org",org)

                Publisher publisher = Publisher.findByName(dataresponse?.publisher_name)
                if (publisher != null) {
                    hm.put("msg", "Publisher already exist!!")
                } else {
                    publisher = new Publisher()
                    publisher.isactive = true
                    publisher.name = dataresponse?.publisher_name
                    publisher.display_order = dataresponse?.publisherDisplayOrder
                    publisher.short_name = dataresponse?.publisherShortName
                    publisher.updation_ip_address = ip
                    publisher.creation_ip_address = ip
                    publisher.updation_date = new Date()
                    publisher.creation_date = new Date()
                    publisher.updation_username = uid
                    publisher.creation_username = uid
                    publisher.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def allP_List = Publisher.findAll()
                int srno = 1
                for (pub in allP_List) {
                    HashMap temp = new HashMap()
                    temp.put("publisher", pub.name)
                    temp.put("display_order", pub.display_order)
                    temp.put("short_name", pub.short_name)
                    temp.put("publisherId", pub.id)
                    temp.put("publisherIsactive", pub.isactive)
                    temp.put("srno", srno++)
                    publisherlist.add(temp)
                }
                hm.put("publisher_list", publisherlist)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def publisherIsactive(request, hm) {
        println "i am in publisherIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "Edit User Types from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                hm.put("org", org)

                def publisher = Publisher.findById(dataresponse?.publisherId)
                if (publisher != null) {
                    if (publisher?.isactive == false) {
                        publisher.isactive = true
                        publisher.updation_ip_address = request.getRemoteAddr()
                        publisher.updation_date = new Date()
                        publisher.updation_username = uid
                        publisher.save(flush: true, failOnError: true)
                    } else {
                        publisher.isactive = false
                        publisher.updation_ip_address = request.getRemoteAddr()
                        publisher.updation_date = new Date()
                        publisher.updation_username = uid
                        publisher.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def editPublisher(request, hm, ip) {
        println "i am in editPublisher"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "Edit publisher from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                Publisher publisher = Publisher.findById(dataresponse?.publisherId)
                def exist = Publisher.findByNameAndShort_name(dataresponse?.publisherName.trim(),dataresponse?.publisherShortName.trim())
                if (exist != null) {
                    hm.put("msg", "Publisher type already exist!!")
                } else {
                    publisher.name = dataresponse?.publisherName
                    publisher.display_order = dataresponse?.publisherDisplayOrder
                    publisher.short_name = dataresponse?.publisherShortName
                    publisher.updation_ip_address = ip
                    publisher.updation_date = new Date()
                    publisher.updation_username = uid
                    publisher.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm
                }
            }
            return hm
        }
    }

    def getAuthor(request, hm) {
        println("Inside getAuthor...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def authorlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def author = Author.findAll()
                int srno = 1
                for (auth in author) {
                    HashMap temp = new HashMap()
                    temp.put("author", auth.name)
                    temp.put("authorId", auth.id)
                    temp.put("authorDes", auth.description)
                    temp.put("authorIsactive", auth.isactive)
                    temp.put("displayorder", auth.display_order)
                    temp.put("shortname", auth.short_name)
                    temp.put("srno", srno++)
                    authorlist.add(temp)
                }
                hm.put('author', authorlist)
                //println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def addAuthor(request, hm, ip) {
        println "i am in addAuthor"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "Edit User Types from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def authorlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
//                def org = user.organization
//                hm.put("org",org)

                Author author = Author.findByName(dataresponse?.authorName)
                if (author != null) {
                    hm.put("msg", "Author already exist!!")
                } else {
                    author = new Author()
                    author.isactive = true
                    author.name = dataresponse?.author_name
                    if (dataresponse?.author_des)
                        author.description = dataresponse?.author_des
                    if (dataresponse?.displayorder)
                        author.display_order = Integer.parseInt(dataresponse?.displayorder)
                    if (dataresponse?.shortname)
                        author.short_name = dataresponse?.shortname
                    author.updation_ip_address = ip
                    author.creation_ip_address = ip
                    author.updation_date = new Date()
                    author.creation_date = new Date()
                    author.updation_username = uid
                    author.creation_username = uid
                    author.save(flush: true, failOnError: true)


                    def authors = Author.findAll()
                    int srno = 1
                    for (authorDetail in authors) {
                        HashMap temp = new HashMap()
                        temp.put("author", authorDetail.name)
                        temp.put("authorId", authorDetail.id)
                        temp.put("authorDes", authorDetail.description)
                        temp.put("authorIsactive", authorDetail.isactive)
                        temp.put("displayorder", authorDetail.display_order)
                        temp.put("shortname", authorDetail.short_name)
                        temp.put("srno", srno++)
                        authorlist.add(temp)
                    }
                    hm.put("author_list", authorlist)

                    hm.put("msg", "success")
                    //println hm
                    return hm
                }



            }
        }
        return hm
    }

    def authorIsactive(request, hm) {
        println "i am in authorIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "Edit User Types from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                hm.put("org", org)

                def author = Author.findById(dataresponse?.authorId)
                if (author != null) {
                    if (author?.isactive == false) {
                        author.isactive = true
                        author.updation_ip_address = request.getRemoteAddr()
                        author.updation_date = new Date()
                        author.updation_username = uid
                        author.save(flush: true, failOnError: true)
                    } else {
                        author.isactive = false
                        author.updation_ip_address = request.getRemoteAddr()
                        author.updation_date = new Date()
                        author.updation_username = uid
                        author.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def editAuthor(request, hm, ip) {
        println "i am in editAuthor"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "Edit editAuthor from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                Author author = Author.findById(dataresponse?.authorId)
                if (dataresponse?.authorName)
                    author.name = dataresponse?.authorName
                if (dataresponse?.authorDes)
                    author.description = dataresponse?.authorDes
                if (dataresponse?.displayorder)
                    author.display_order = dataresponse?.displayorder
                if (dataresponse?.shortname)
                    author.short_name = dataresponse?.shortname
                author.updation_ip_address = ip
                author.updation_date = new Date()
                author.updation_username = uid
                author.save(flush: true, failOnError: true)
                hm.put("msg", "success")
                hm.put("status", "200")
                return hm
            }
        }
        return hm
    }

    def importAuthor(request, ip ,hm){
        println(' in  import author :')
        def uid = request.getHeader("EPS-uid")
        Part filePart = request.getPart('file')
        InputStream fs = filePart.getInputStream()
        File file = File.createTempFile("s3test", "");
        OutputStream outputStream = new FileOutputStream(file)
        IOUtils.copy(fs, outputStream);
        def reader = new CSVReader(new FileReader(file))
        AdminService adminServicev= new AdminService()
        println('fs----'+ fs)
        String loginattachmentdomainname = ""
        String[] nextRecord;
        boolean header = true
        ArrayList failList = new ArrayList()

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def logeduser = User.findByUsername(log.username)
                //UserType usertype = UserType.findByNameIlike("%Employee%")
                loginattachmentdomainname = logeduser.organization.loginattachementdomain

                while ((nextRecord = reader.readNext()) != null) {
                    println("nextRecord :--- "+nextRecord)
                    String reason = 'Data is Missing - '
                    HashMap temp = new HashMap()
                    if (header) //skip headers
                    {
                        header = false
                        continue
                    }

                    if (nextRecord[0] != ''){
//                        User user = User.findByUsernameAndOrganization(nextRecord[0].trim()+logeduser.organization.loginattachementdomain,logeduser.organization)
                        Author author = Author.findByName(nextRecord[0].trim())

                        if (author == null) {
//                            adminServicev.createLogin(uid, nextRecord[0].trim(), ip, loginattachmentdomainname,log.tenantclient)
//                            adminServicev.createLogin(uid, nextRecord[2].trim(), ip, loginattachmentdomainname,log.tenantclient)
                            author = new Author()
                            author.name = nextRecord[0].trim()
                            if (nextRecord[1])
                                author.description = nextRecord[1].trim()
                            if (nextRecord[2])
                                author.display_order = Integer.parseInt(nextRecord[2].trim())
                            if (nextRecord[3])
                                author.short_name = nextRecord[3].trim()
                            author.isactive = true
                            author.creation_username = uid
                            author.updation_username = uid
                            author.creation_date = new Date()
                            author.updation_date = new Date()
                            author.creation_ip_address = ip
                            author.updation_ip_address = ip
                            author.save(flush: true, failOnError: true)
                        } else {
                            reason = "Author already present"
                            temp.put('reason', reason)
                            temp.put('name', nextRecord[0].trim())
                            failList.add(temp)
                        }
                    }
                }
                hm.put("status", "200")
            }
        }
        outputStream.flush()
        outputStream.close()
        fs.close()
        file.delete()
        hm.put('failList',failList)
        return hm
    }

    def deleteAuthor(request, hm){
        println "i am in deleteAuthor"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteAuthor from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                Author author = Author.findById(dataresponse?.authorId)
                println("author : " + author)
                if(author!=null) {
                    try{
                        author.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return
            }
        }
    }

    def getBookType(request, hm) {
        println("Inside getBookType...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def booktypeList = []
            def bookCl_list=[]
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def bookType = BookType.findAllByOrganization(user?.organization)
                int srno = 1
                for (book in bookType) {
                    HashMap temp = new HashMap()
                    temp.put("bookType", book.name)
                    temp.put("bookTypeId", book.id)
                    temp.put("displayorder", book.display_order)
                    temp.put("shortname", book.short_name)
                    temp.put("bookTypeIsactive", book.isactive)
                    temp.put("clid", book?.bookclassification?.id)
                    temp.put("cl", book?.bookclassification?.name)
                    temp.put("srno", srno++)
                    booktypeList.add(temp)
                }
                hm.put('bookType', booktypeList)
                def bookclassificationlist = BookClassification.findAllByOrganization(user?.organization)
                int srNo =1
                for(bookcl in bookclassificationlist){
                    HashMap temp = new HashMap()
                    temp.put("bookClName", bookcl.name)
                    temp.put("bookClId", bookcl.id)
                    temp.put("srno", srNo++)
                    bookCl_list.add(temp)
                }
                hm.put('bookCl_list', bookCl_list)

                println "hm new: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def addBookType(request, hm, ip) {
        println "i am in addBookType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addBookType from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def bookTypelist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
//                def org = user.organization
//                hm.put("org",org)

                BookType bookType = BookType.findByNameAndOrganization(dataresponse?.bookType_name,user?.organization)

                if (bookType != null) {
                    hm.put("msg", "Book Type already exist!!")
                } else {
                    bookType = new BookType()
                    bookType.isactive = true
                    bookType.name = dataresponse?.bookType_name
                    if (dataresponse?.displayorder)
                        bookType.display_order = Integer.parseInt(dataresponse?.displayorder)
                    if (dataresponse?.shortname)
                        bookType.short_name = dataresponse?.shortname
                    if(dataresponse?.bookCL){
                        BookClassification cl = BookClassification.findByIdAndOrganization(dataresponse?.bookCL,user?.organization)
                        println("cl : "+cl)
                        bookType.bookclassification = cl
                    }
                    bookType.organization= user?.organization
                    bookType.updation_ip_address = ip
                    bookType.creation_ip_address = ip
                    bookType.updation_date = new Date()
                    bookType.creation_date = new Date()
                    bookType.updation_username = uid
                    bookType.creation_username = uid
                    bookType.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def bookTypes = BookType.findAllByOrganization(user?.organization)
                int srno = 1
                for (b_type in bookTypes) {
                    HashMap temp = new HashMap()
                    temp.put("bookType", b_type.name)
                    temp.put("bookTypeId", b_type.id)
                    temp.put("bookTypeIsactive", b_type.isactive)
                    temp.put("srno", srno++)
                    bookTypelist.add(temp)
                }
                hm.put("bookType_list", bookTypelist)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def bookTypeIsactive(request, hm) {
        println "i am in bookTypeIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "bookTypeIsactive"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                hm.put("org", org)

                def bookType = BookType.findById(dataresponse?.bookTypeId)
                if (bookType != null) {
                    if (bookType?.isactive == false) {
                        bookType.isactive = true
                        bookType.updation_ip_address = request.getRemoteAddr()
                        bookType.updation_date = new Date()
                        bookType.updation_username = uid
                        bookType.save(flush: true, failOnError: true)
                    } else {
                        bookType.isactive = false
                        bookType.updation_ip_address = request.getRemoteAddr()
                        bookType.updation_date = new Date()
                        bookType.updation_username = uid
                        bookType.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def editBookType(request, hm, ip) {
        println "i am in editBookType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "Edit BookType from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                BookType bookType = BookType.findByIdAndOrganization(dataresponse?.bookTypeId,user?.organization)
                // def exist = BookType.findByName(dataresponse?.bookTypeName)
                if (bookType != null) {
                    if (dataresponse?.bookTypeName)
                        bookType.name = dataresponse?.bookTypeName
                    if (dataresponse?.displayorder)
                    {
                        println("dataresponse?.displayorder.getClass().getSimpleName()")
                        println(dataresponse?.displayorder.getClass().getSimpleName())
                        if(dataresponse?.displayorder.getClass().getSimpleName()=='Integer')
                            bookType.display_order=dataresponse?.displayorder
                        else
                            bookType.display_order = Integer.parseInt(dataresponse?.displayorder)
                    }

                    if (dataresponse?.shortname)
                        bookType.short_name = dataresponse?.shortname
                    if(dataresponse?.bookCL){
                        BookClassification cl = BookClassification.findByIdAndOrganization(dataresponse?.bookCL,user?.organization)
                        println("cl : "+cl)
                        bookType.bookclassification = cl
                    }
                    bookType.updation_ip_address = ip
                    bookType.updation_date = new Date()
                    bookType.updation_username = uid
                    bookType.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm

                } else {
                    hm.put("msg", "Book type does not exist!!")
                }
            }
        }
        return hm
    }

    def deleteBookType(request ,hm){
        println "i am in deleteBookType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteBookType from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                BookType bookType = BookType.findByIdAndOrganization(dataresponse?.bookTypeId,user?.organization)
                println("bookType : " + bookType)
                if(bookType!=null) {
                    try{
                        bookType.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }

                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return
            }
        }
    }

    def getLanguage(request, hm) {
        println("Inside getLanguage...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def langList = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def language = Language.findAllByOrganization(user.organization)
                int srno = 1
                for (lang in language) {
                    HashMap temp = new HashMap()
                    temp.put("language", lang.name)
                    temp.put("languageId", lang.id)
                    temp.put("languageIsactive", lang.isactive)
                    temp.put("srno", srno++)
                    langList.add(temp)
                }
                hm.put('language', langList)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def getBookSeries(request, hm) {
        println("Inside getBookSeries...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def langList = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def series = BookSeries.findAll()
                int srno = 1
                for (lang in series) {
                    HashMap temp = new HashMap()
                    temp.put("series", lang.name)
                    temp.put("seriesId", lang.id)
                    temp.put("displayorder", lang.display_order)
                    temp.put("shortname", lang.short_name)
                    temp.put("seriesIsactive", lang.isactive)
                    temp.put("srno", srno++)
                    langList.add(temp)
                }
                hm.put('series', langList)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def addLanguage(request, hm, ip) {
        println "i am in addLanguage"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addLanguage from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def langList = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
//                hm.put("org",org)

                Language language = Language.findByNameAndOrganization(dataresponse?.language_name,org)
                println('language---' + language)
                if (language != null) {
                    hm.put("msg", "Language already exist!!")
                } else {
                    language = new Language()
                    language.isactive = true
                    language.name = dataresponse?.language_name
                    language.organization = user.organization
                    language.updation_ip_address = ip
                    language.creation_ip_address = ip
                    language.updation_date = new Date()
                    language.creation_date = new Date()
                    language.updation_username = uid
                    language.creation_username = uid
                    language.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def languages = Language.findAllByOrganization(org)
                int srno = 1
                for (lang in languages) {
                    HashMap temp = new HashMap()
                    temp.put("language", lang.name)
                    temp.put("languageId", lang.id)
                    temp.put("languageIsactive", lang.isactive)
                    temp.put("srno", srno++)
                    langList.add(temp)
                }
                hm.put('language', langList)
                println "hm: " + hm
                hm.put("status", "200")
                return hm
            }
        }
        return hm
    }

    def languageIsactive(request, hm) {
        println "i am in languageIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "languageIsactive"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                hm.put("org", org)

                def language = Language.findById(dataresponse?.languageId)
                if (language != null) {
                    if (language?.isactive == false) {
                        language.isactive = true
                        language.updation_ip_address = request.getRemoteAddr()
                        language.updation_date = new Date()
                        language.updation_username = uid
                        language.save(flush: true, failOnError: true)
                    } else {
                        language.isactive = false
                        language.updation_ip_address = request.getRemoteAddr()
                        language.updation_date = new Date()
                        language.updation_username = uid
                        language.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def editLanguage(request, hm, ip) {
        println "i am in editLanguage"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "Edit editLanguage from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                Language language = Language.findById(dataresponse?.languageId)

                def exist = Language.findByNameAndOrganization(dataresponse?.languageName,user.organization)
                if (exist != null) {
                    hm.put("msg", "Language already exist!!")
                } else {
                    if (dataresponse?.languageName)
                        language.name = dataresponse?.languageName
                    language.organization = user.organization
                    language.updation_ip_address = ip
                    language.updation_date = new Date()
                    language.updation_username = uid
                    language.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm
                }
            }
        }
        return hm
    }

    def deleteLanguage(request ,hm){
        println "i am in deleteLanguage"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteLanguage from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                Language language = Language.findById(dataresponse?.languageId)
                println("language : " + language)
                if(language!=null) {
                    try{
                        language.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }

                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return
            }
        }
    }

    def getRole(request, hm) {
        println("Inside getRole")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def rolelist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def roles = Role.findAll()
                int srno = 1
                for (role in roles) {
                    HashMap temp = new HashMap()
                    temp.put("role", role.name)
                    temp.put("roleId", role.id)
                    temp.put("usertypeId", role?.usertype?.id)
                    temp.put("usertypeName", role?.usertype?.name)
                    temp.put("roleIsactive", role.isactive)
                    temp.put("srno", srno++)
                    rolelist.add(temp)
                }
                hm.put('role', rolelist)

                ArrayList usertype_list = new ArrayList()
                for(item in UserType.findAll()){
                    HashMap temp = new HashMap()
                    temp.put('usertype',item?.name)
                    temp.put('id',item?.id)
                    usertype_list.add(temp)
                }
                hm.put('usertype_list',usertype_list)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def addRole(request, hm, ip) {
        println "i am in addRole"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addRole from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def rolelist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
//                def org = user.organization
//                hm.put("org",org)

                Role role = Role.findByName(dataresponse?.role_name)
                if (role != null) {
                    hm.put("msg", "Role already exist!!")
                } else {
                    UserType userType = UserType.findById(dataresponse.usertype)

                    role = new Role()
                    role.isactive = true
                    role.name = dataresponse?.role_name
                    role.usertype = userType
                    role.updation_ip_address = ip
                    role.creation_ip_address = ip
                    role.updation_date = new Date()
                    role.creation_date = new Date()
                    role.updation_username = uid
                    role.creation_username = uid
                    role.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def allrole_List = Role.findAll()
                int srno = 1
                for (roledata in allrole_List) {
                    HashMap temp = new HashMap()
                    temp.put("role", roledata.name)
                    temp.put("roleId", roledata.id)
                    temp.put("roleIsactive", roledata.isactive)
                    temp.put("srno", srno++)
                    rolelist.add(temp)
                }
                hm.put("role_list", rolelist)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def roleIsactive(request, hm) {
        println "i am in roleIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "roleIsactive from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                hm.put("org", org)

                def role = Role.findById(dataresponse?.roleId)
                if (role != null) {
                    if (role?.isactive == false) {
                        role.isactive = true
                        role.updation_ip_address = request.getRemoteAddr()
                        role.updation_date = new Date()
                        role.updation_username = uid
                        role.save(flush: true, failOnError: true)
                    } else {
                        role.isactive = false
                        role.updation_ip_address = request.getRemoteAddr()
                        role.updation_date = new Date()
                        role.updation_username = uid
                        role.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def editRole(request, hm, ip) {
        println "i am in editRole"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editRole from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                Role role = Role.findById(dataresponse?.roleId)
                UserType userType = UserType.findById(dataresponse.usertypeId)
                println('userType-------'+userType)
                def exist = Role.findByNameAndUsertype(dataresponse?.roleName,userType)
                println('exist-------------'+ exist)
                if (exist != null) {
                    hm.put("msg", "Role already exist!!")
                } else {
                    role.name = dataresponse?.roleName
                    role.usertype = userType
                    role.updation_ip_address = ip
                    role.updation_date = new Date()
                    role.updation_username = uid
                    role.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm
                }
            }
        }
        return hm
    }

    def deleteRole(request ,hm){
        println "i am in deleteRole"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteRole from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                Role role = Role.findById(dataresponse?.roleId)
                println("role : " + role)
                if(role!=null) {
                    try{role.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }

                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return
            }
        }
    }


    def getBookCat(request, hm) {
        println("Inside getBookCat")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def bookCatlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def erpDepartment_list = LibraryDepartment.findAllByOrganization(user?.organization)
                def department_list = []
                for (dept in erpDepartment_list) {
                    HashMap temp = new HashMap()
                    temp.put("department", dept.name)
                    temp.put("departmentId", dept.id)
                    department_list.add(temp)
                }
                hm.put('department_list', department_list)
                def bookCat = BookCategory.findAllByOrganization(user?.organization)
                int srno = 1
                for (ut in bookCat) {
                    HashMap temp = new HashMap()
                    temp.put("bookCat", ut.name)
                    temp.put("bookCatId", ut.id)
                    temp.put("displayorder", ut.display_order)
                    temp.put("shortname", ut.short_name)
                    temp.put("erpdepartment", ut.librarydepartment?.name)
                    temp.put("erpdepartmentId", ut.librarydepartment?.id)
                    temp.put("bookCatIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    bookCatlist.add(temp)

                }
                hm.put('bookCat', bookCatlist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def addBookCat(request, hm, ip) {
        println "i am in addBookCat"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addBookCat from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def bookCatlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
//                def org = user.organization
//                hm.put("org",org)
                LibraryDepartment libraryDepartment= LibraryDepartment.findByIdAndOrganization(dataresponse?.erpDepartment,user?.organization)
                println("libraryDepartment : "+libraryDepartment)
                BookCategory bookCat = BookCategory.findByNameAndOrganizationAndLibrarydepartment(dataresponse?.bookCat_name,user?.organization,libraryDepartment)
                if (bookCat != null) {
                    hm.put("msg", "Book Category already exist!!")
                    return hm
                } else {
                    bookCat = new BookCategory()
                    bookCat.isactive = true
                    bookCat.name = dataresponse?.bookCat_name
                    if (dataresponse?.displayorder)
                        bookCat.display_order = java.lang.Integer.parseInt(dataresponse?.displayorder)
                    if (dataresponse?.shortname)
                        bookCat.short_name = dataresponse?.shortname
                    bookCat.librarydepartment = libraryDepartment
                    bookCat.organization = user?.organization
                    bookCat.updation_ip_address = ip
                    bookCat.creation_ip_address = ip
                    bookCat.updation_date = new Date()
                    bookCat.creation_date = new Date()
                    bookCat.updation_username = uid
                    bookCat.creation_username = uid
                    bookCat.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }
                def allP_List = BookCategory.findAllByOrganization(user?.organization)
                int srno = 1
                for (pub in allP_List) {
                    HashMap temp = new HashMap()
                    temp.put("bookCat", pub.name)
                    temp.put("bookCatId", pub.id)
                    temp.put("displayorder", pub.display_order)
                    temp.put("shortname", pub.short_name)
                    temp.put("erpdepartment", pub.librarydepartment?.name)
                    temp.put("bookCatIsactive", pub.isactive)
                    temp.put("srno", srno++)
                    bookCatlist.add(temp)
                }
                hm.put("bookCat_list", bookCatlist)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }


    def bookCatIsactive(request, hm) {
        println "i am in bookCatIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "Edit User Types from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                hm.put("org", org)

                def bookCat = BookCategory.findById(dataresponse?.bookCatId)
                if (bookCat != null) {
                    if (bookCat?.isactive == false) {
                        bookCat.isactive = true
                        bookCat.updation_ip_address = request.getRemoteAddr()
                        bookCat.updation_date = new Date()
                        bookCat.updation_username = uid
                        bookCat.save(flush: true, failOnError: true)
                    } else {
                        bookCat.isactive = false
                        bookCat.updation_ip_address = request.getRemoteAddr()
                        bookCat.updation_date = new Date()
                        bookCat.updation_username = uid
                        bookCat.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def editBookCat(request, hm, ip) {
        println "i am in editBookCat"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editBookCat from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                BookCategory bookCat = BookCategory.findByIdAndOrganization(dataresponse?.bookCatId,user?.organization)


                def exist = BookCategory.findByNameAndOrganization(dataresponse?.bookCatName,user?.organization)
                if (bookCat != null) {

                    bookCat.name = dataresponse?.bookCatName
                    if (dataresponse?.displayorder)
                        bookCat.display_order = dataresponse?.displayorder
                    if (dataresponse?.shortname)
                        bookCat.short_name = dataresponse?.shortname

                    LibraryDepartment libDept = LibraryDepartment.findByIdAndOrganization(dataresponse?.erpDepartment, user?.organization)
                        bookCat.librarydepartment = libDept
                    
                    bookCat.updation_ip_address = ip
                    bookCat.updation_date = new Date()
                    bookCat.updation_username = uid
                    bookCat.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm
                } else {
                    hm.put("msg", "Book category does not exist!!")
                }
            }
        }
        return hm
    }

    def deleteBookCategory(request ,hm){
        println "i am in deleteBookCategory"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteBookCategory from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                BookCategory bookCategory = BookCategory.findById(dataresponse?.bookCatId)
                println("bookCategory : " + bookCategory)
                if(bookCategory!=null) {
                    try{
                        bookCategory.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    } catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return
            }
        }
    }


    def adminAddRoleLink(request, hm, ip) {
        println("in adminAddRole:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String purpose = "Getting Role Link from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList role_list = new ArrayList()
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization.name
//                for (item in RoleLinks.findAllByOrganization(user.organization)) {
                for (item in RoleLinks.findAll()) {
                    HashMap temp = new HashMap()
                    temp.put("roleLinkId", item.id)
                    temp.put("roleLinkName", item.linkname)
                    temp.put("roleLink", item.link_display_name)
                    temp.put("roleLinkIcon", item.icon_file_name)
                    temp.put("roleLinkDescription", item.linkdescription)
                    temp.put("roleLinkSortOrder", item.sort_order)
                    temp.put("role", item?.role?.name)
                    temp.put("actionName", item?.action_name)
                    temp.put("controllerName", item?.controller_name)
                    temp.put("isactive", item.isactive)
                    role_list.add(temp)
                }

                hm.put("roleLink_list", role_list)
                hm.put("org", org)
                hm.put("role_list", Role.findAllByIsactive(true))

            }//println"hm: "+hm
            return hm

        }
        return hm
    }

    def editAdminAddRoleLink(request, hm, ip) {
        println("in editAdminAddRole:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:" + dataresponse

        AuthService auth = new AuthService()

        String purpose = "Editing Role from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                RoleLinks roleObj = RoleLinks.findById(dataresponse.roleLinkId)
                def role = Role.findById(dataresponse.roleId)
                if (roleObj != null) {
                    String oldName = roleObj.linkname
                    println "oldName " + oldName
                    roleObj.role = role
                    roleObj.linkname = dataresponse.roleLinkName
                    roleObj.link_display_name = dataresponse.roleLink
                    roleObj.icon_file_name = dataresponse.roleLinkIcon
                    roleObj.controller_name = dataresponse.controllerName
                    roleObj.action_name = dataresponse.actionName
                    roleObj.linkdescription = dataresponse.roleLinkDescription
                    String order = dataresponse.roleLinkSortOrder
                    roleObj.sort_order = Integer.parseInt(order)

                    roleObj.isactive = dataresponse.isactive
                    roleObj.updation_username = uid
                    roleObj.updation_date = new java.util.Date()
                    roleObj.updation_ip_address = request.getRemoteAddr()
                    roleObj.save(failOnError: true, flush: true)

                    def listForDuplicate = RoleLinks.findAllByLinknameAndIsactive(dataresponse.roleLinkName, dataresponse.isactive)
                    if (listForDuplicate.size() > 1) {
                        println "1"
                        roleObj.linkname = oldName
                        roleObj.updation_username = uid
                        roleObj.updation_date = new java.util.Date()
                        roleObj.updation_ip_address = request.getRemoteAddr()
                        roleObj.save(failOnError: true, flush: true)

                        HashMap status = new HashMap()
                        status.put("code", "NA")
                        hm.put("status", status)
                    } else {
                        println "2"

                        HashMap status = new HashMap()
                        status.put("code", "SUCCESS")
                        hm.put("status", status)
                    }
                }
                return hm
            }
        }
        return hm
    }


    def saveAdminAddRoleLink(request, hm, ip) {
        println("in saveAdminAddRoleLink:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:" + dataresponse

        AuthService auth = new AuthService()

        String purpose = "Saving Role Link from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        println("dataresponse")
        println(dataresponse)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def role = Role.findById(dataresponse.roleId)
                RoleLinks rolelink = RoleLinks.findByLinkname(dataresponse.roleLinkName)

                if (rolelink == null) {
                    rolelink = new RoleLinks()
                    rolelink.linkname = dataresponse.roleLinkName
                    rolelink.link_display_name = dataresponse.roleLink
                    rolelink.icon_file_name = dataresponse.roleLinkIcon
                    rolelink.linkdescription = dataresponse.roleLinkDescription
                    rolelink.sort_order = Integer.parseInt(dataresponse.roleLinkSortOrder)
                    rolelink.role = role
                    rolelink.isactive = dataresponse.isactive
                    rolelink.controller_name = dataresponse.controllerName
                    rolelink.action_name = dataresponse.actionName
                    rolelink.usertype = user?.usertype
//                    rolelink.organization = user.organization
                    rolelink.creation_username = uid
                    rolelink.updation_username = uid
                    rolelink.creation_date = new java.util.Date()
                    rolelink.updation_date = new java.util.Date()
                    rolelink.creation_ip_address = ip
                    rolelink.updation_ip_address = ip
                    rolelink.save(failOnError: true, flush: true)

                    HashMap status = new HashMap()
                    status.put("code", "SUCCESS")
                    status.put("message", "Role added successfully...")
                    hm.put("status", status)
                } else {
                    HashMap status = new HashMap()
                    status.put("code", "NA")
                    status.put("message", "Role Already present!!!")
                    hm.put("status", status)
                }
            }
            return hm

        }
        return hm
    }

    def getBookFormat(request, hm) {
        println("Inside getBookFormat")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def bookFormatlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def bookFormat = BookFormat.findAll()
                int srno = 1
                for (ut in bookFormat) {
                    HashMap temp = new HashMap()
                    temp.put("bookFormat", ut.name)
                    temp.put("bookFormatId", ut.id)
                    temp.put("displayorder", ut.display_order)
                    temp.put("shortname", ut.short_name)
                    temp.put("bookFormatIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    bookFormatlist.add(temp)
                }
                hm.put('bookFormat', bookFormatlist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def addBookFormat(request, hm, ip) {
        println "i am in addBookFormat"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addBookFormat from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def bookFormatlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
//                def org = user.organization
//                hm.put("org",org)

                BookFormat bookFormat = BookFormat.findByName(dataresponse?.bookFormat_name)
                if (bookFormat != null) {
                    hm.put("msg", "Book format already exist!!")
                } else {
                    bookFormat = new BookFormat()
                    bookFormat.isactive = true
                    bookFormat.name = dataresponse?.bookFormat_name
                    if (dataresponse?.displayorder)
                        bookFormat.display_order = Integer.parseInt(dataresponse?.displayorder)
                    if (dataresponse?.shortname)
                        bookFormat.short_name = dataresponse?.shortname
                    bookFormat.updation_ip_address = ip
                    bookFormat.creation_ip_address = ip
                    bookFormat.updation_date = new Date()
                    bookFormat.creation_date = new Date()
                    bookFormat.updation_username = uid
                    bookFormat.creation_username = uid
                    bookFormat.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def allP_List = BookFormat.findAll()
                int srno = 1
                for (pub in allP_List) {
                    HashMap temp = new HashMap()
                    temp.put("bookFormat", pub.name)
                    temp.put("bookFormatId", pub.id)
                    temp.put("bookFormatIsactive", pub.isactive)
                    temp.put("srno", srno++)
                    bookFormatlist.add(temp)
                }
                hm.put("bookFormat_list", bookFormatlist)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def bookFormatIsactive(request, hm) {
        println "i am in bookFormatIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "bookFormatIsactive from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                hm.put("org", org)

                def bookFormat = BookFormat.findById(dataresponse?.bookFormatId)
                if (bookFormat != null) {
                    if (bookFormat?.isactive == false) {
                        bookFormat.isactive = true
                        bookFormat.updation_ip_address = request.getRemoteAddr()
                        bookFormat.updation_date = new Date()
                        bookFormat.updation_username = uid
                        bookFormat.save(flush: true, failOnError: true)
                    } else {
                        bookFormat.isactive = false
                        bookFormat.updation_ip_address = request.getRemoteAddr()
                        bookFormat.updation_date = new Date()
                        bookFormat.updation_username = uid
                        bookFormat.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def editBookFormat(request, hm, ip) {
        println "i am in editBookFormat"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editBookFormat from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                BookFormat bookFormat = BookFormat.findById(dataresponse?.bookFormatId)

                //def exist = BookFormat.findByName(dataresponse?.bookFormatName)
                if (bookFormat != null) {
                    bookFormat.name = dataresponse?.bookFormatName
                    bookFormat.short_name = dataresponse?.shortname
                    bookFormat.updation_ip_address = ip
                    bookFormat.updation_date = new Date()
                    bookFormat.updation_username = uid
                    bookFormat.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm

                } else {

                    hm.put("msg", "Book Format does not exist!!")
                }
            }
        }
        return hm
    }

    def deleteBookFormat(request ,hm){
        println "i am in deleteBookFormat"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteBookFormat from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                BookFormat bookFormat = BookFormat.findById(dataresponse?.bookFormatId)
                println("bookFormat : " + bookFormat)
                if(bookFormat!=null) {
                    try {
                        bookFormat.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    } catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return
            }
        }
    }

    def getBookStatus(request, hm) {
        println("Inside getBookStatus")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def bookStatuslist_SA = []
        def bookStatus_SA = BookStatusSuperMaster.findAll()
        for (ut in bookStatus_SA) {
            HashMap temp = new HashMap()
            temp.put("bookStatus", ut.name)
            temp.put("display_name", ut.displayname)
            temp.put("bookStatusId", ut.id)
            temp.put("bookStatusIsactive", ut.isactive)
            bookStatuslist_SA.add(temp)
        }
        hm.put('bookStatus_SA', bookStatuslist_SA)
        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def bookStatuslist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def bookStatus = BookStatus.findAll()
                int srno = 1
                for (ut in bookStatus) {
                    HashMap temp = new HashMap()
                    temp.put("bookStatus", ut.name)
                    temp.put("bookStatusId", ut.id)
                    temp.put("display_name", ut.displayname)
                    temp.put("bookStatusIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    bookStatuslist.add(temp)
                }
                hm.put('bookStatus_list', bookStatuslist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def addBookStatus(request, hm, ip) {
        println "i am in addBookStatus"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addBookStatus from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def bookStatuslist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
//                def org = user.organization
//                hm.put("org",org)

                BookStatus bookStatus = BookStatus.findByName(dataresponse?.bookStatus,dataresponse?.display_name)
                if (bookStatus != null) {
                    hm.put("msg", "Book Status already exist!!")
                } else {
                    bookStatus = new BookStatus()
                    bookStatus.isactive = true
                    bookStatus.name = dataresponse?.bookStatus
                    bookStatus.displayname = dataresponse?.display_name
                    bookStatus.updation_ip_address = ip
                    bookStatus.creation_ip_address = ip
                    bookStatus.updation_date = new Date()
                    bookStatus.creation_date = new Date()
                    bookStatus.updation_username = uid
                    bookStatus.creation_username = uid
                    bookStatus.save(flush: true, failOnError: true)
                    hm.put("msg", "200")
                   // hm.put("msg", "success")
                    hm.put("status", "success")

                }

//                def allP_List = BookStatus.findAll()
//                int srno = 1
//                for (pub in allP_List) {
//                    HashMap temp = new HashMap()
//                    temp.put("bookStatus", pub.name)
//                    temp.put("display_name", pub.displayname)
//                    temp.put("bookStatusId", pub.id)
//                    temp.put("bookStatusIsactive", pub.isactive)
//                    temp.put("srno", srno++)
//                    bookStatuslist.add(temp)
//                }
               // hm.put("bookStatus_list", bookStatuslist)
               // hm.put("status", "200")
                hm.put("msg", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def bookStatusIsactive(request, hm) {
        println "i am in bookStatusIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "bookStatusIsactive from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                hm.put("org", org)

                def bookStatus = BookStatus.findById(dataresponse?.bookStatusId)
                if (bookStatus != null) {
                    if (bookStatus?.isactive == false) {
                        bookStatus.isactive = true
                        bookStatus.updation_ip_address = request.getRemoteAddr()
                        bookStatus.updation_date = new Date()
                        bookStatus.updation_username = uid
                        bookStatus.save(flush: true, failOnError: true)
                    } else {
                        bookStatus.isactive = false
                        bookStatus.updation_ip_address = request.getRemoteAddr()
                        bookStatus.updation_date = new Date()
                        bookStatus.updation_username = uid
                        bookStatus.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def editBookStatus(request, hm, ip) {
        println "i am in editBookStatus"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editBookStatus from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                BookStatus bookStatus = BookStatus.findById(dataresponse?.bookStatusId)

//                def exist = BookStatus.findByName(dataresponse?.bookStatusName)
//                if (exist != null) {
//                    hm.put("msg", "Book status already exist!!")
//                }
//                else {
//                    bookStatus.name = dataresponse?.bookStatus
//                    bookStatus.displayname = dataresponse?.bookStatusName
//                    bookStatus.updation_ip_address = ip
//                    bookStatus.updation_date = new Date()
//                    bookStatus.updation_username = uid
//                    bookStatus.save(flush: true, failOnError: true)
//                    hm.put("msg", "success")
//                    hm.put("status", "200")
//                    return hm
//                }

                if (bookStatus != null)
                {
                    bookStatus.name = dataresponse?.bookStatus
                    bookStatus.displayname = dataresponse?.display_name
                    bookStatus.updation_ip_address = ip
                    bookStatus.updation_date = new Date()
                    bookStatus.updation_username = uid
                    bookStatus.save(flush: true, failOnError: true)
                    hm.put("status", "success")
                    //hm.put("status", "200")

                }
                else{
                    hm.put("status", "name not found")
                }
                hm.put("msg", "200")
                println('hm---'+hm)
                return hm
            }
        }
        return hm
    }

    def deleteBookstatus(request ,hm){
        println "i am in deleteBookFormat"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteBookFormat from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                BookStatus bookStatus = BookStatus.findById(dataresponse?.bookStatusId)
                println("bookStatus : " + bookStatus)
                if(bookStatus!=null) {
                    try{
                        bookStatus.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }

                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return
            }
        }
    }
    //madhuri
    def getWeedoutcode(request, hm) {
    println("Inside getWeedoutcode")
    def token = request.getHeader("EPS-token")
    def uid = request.getHeader("EPS-uid")
    String url = request.getHeader("router-path")
    AuthService auth = new AuthService()
    String body = request.getReader().text
    def jsonSluper = new JsonSlurper()
    String purpose = " add weedoutcode"
    auth.checkauth(token, uid, hm, url, request, purpose)
    if (hm.get("msg") != "401" && hm.get("msg") != "403") {
        Login log = Login.findByUsername(uid)

        Tenants.withId(log.tenantclient.tenantid) {
            def user = User.findByUsername(log.username)
            def organization = user.organization
            def weedoutcodelist = []
            def weedoutlist = Weedoutcode.findAllByOrganization(organization)
            int srno = 1
            for (ut in weedoutlist) {
                HashMap temp = new HashMap()
                temp.put("weedoutcode", ut.name)
                temp.put("weedoutcodeId", ut.id)
                temp.put("display_name", ut.display_name)
                temp.put("organization", ut.organization)
                temp.put("srno", srno++)
                weedoutcodelist.add(temp)
            }
            hm.put('weedoutcode_list', weedoutcodelist)
            println "hm: " + hm
            hm.put("msg", "200")
            return hm
        }
    }
}
    def addWeedoutcode(request, hm, ip){
    println "i am in addWeedoutcode"

    def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
    def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
    String url = request.getHeader("router-path")
    AuthService auth = new AuthService()

    String body = request.getReader().text
    def jsonSluper = new JsonSlurper()
    def dataresponse = jsonSluper.parseText(body)
    println dataresponse

    String purpose = "addWeedoutcode from Admin Master Menu."
    println("addWeedoutcode..."+dataresponse)
    auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
    if (hm.get("msg") != "401" && hm.get("msg") != "403") {
        Login log = Login.findByUsername(uid)
        //def weedoutcodelist = []
        Tenants.withId(log.tenantclient.tenantid) {
            def user = User.findByUsername(log.username)
            def organization = user.organization
//                hm.put("org",org)

            Weedoutcode weedout = Weedoutcode.findByNameAndOrganization(dataresponse?.weedoutcode,organization)
            if (weedout != null) {
                hm.put("status", "Weedoutcode already exist!!")}
            else
            {
                weedout = new Weedoutcode()
                weedout.name = dataresponse?.weedoutcode
                weedout.organization = organization
                weedout.display_name = dataresponse?.display_name
                weedout.updation_ip_address = ip
                weedout.creation_ip_address = ip
                weedout.updation_date = new Date()
                weedout.creation_date = new Date()
                weedout.updation_username = uid
                weedout.creation_username = uid
                weedout.save(flush: true, failOnError: true)
                hm.put("status", "200")
                // hm.put("msg", "success")
                //hm.put("status", "success")
            }
           // hm.put("msg", "200")
            println('hm---' + hm)
            return hm
        }
    }
    return hm
}
    def editWeedoutcode(request ,hm,ip) {
        println "i am in editWeedoutcode"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editWeedoutcode from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)

                Weedoutcode weedout = Weedoutcode.findById(dataresponse?.weedoutcodeId)

//                def exist = BookStatus.findByName(dataresponse?.bookStatusName)
//                if (exist != null) {
//                    hm.put("msg", "Book status already exist!!")
//                }
//                else {
//                    bookStatus.name = dataresponse?.bookStatus
//                    bookStatus.displayname = dataresponse?.bookStatusName
//                    bookStatus.updation_ip_address = ip
//                    bookStatus.updation_date = new Date()
//                    bookStatus.updation_username = uid
//                    bookStatus.save(flush: true, failOnError: true)
//                    hm.put("msg", "success")
//                    hm.put("status", "200")
//                    return hm
//                }

                if (weedout != null)
                {
                    weedout.name = dataresponse?.weedoutcode
                    weedout.display_name = dataresponse?.display_name
                    weedout.updation_ip_address = ip
                    weedout.updation_date = new Date()
                    weedout.updation_username = uid
                    weedout.save(flush: true, failOnError: true)
                    hm.put("status", "success")
                    //hm.put("status", "200")
                }
                else{
                    hm.put("status", "name not found")
                }
                hm.put("msg", "200")
                println('hm---'+hm)
                return hm
            }
        }
        return hm
    }
    def deleteWeedoutcode(request ,hm,ip) {
        println "i am in deleteWeedoutcode"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteWeedoutcode from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                Weedoutcode weedout = Weedoutcode.findById(dataresponse?.weedoutcodeId)
                println("weedout : " + weedout)
                if(weedout!=null) {
                    try{
                        weedout.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }

                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return
            }
        }
    }
    //end

    def getReservationStatus(request, hm) {
        println("Inside getBookStatus")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
//            def reservationStatus_SA = []
//            def reservationStatuslist_SA = ReservationStatusSuperMaster.findAll()
//            for (ut in reservationStatuslist_SA) {
//                HashMap temp = new HashMap()
//                temp.put("reservationStatus", ut.name)
//                temp.put("reservationStatusId", ut.id)
//                temp.put("reservationStatusIsactive", ut.isactive)
//                reservationStatus_SA.add(temp)
//            }
//            hm.put('reservationStatus_SA', reservationStatus_SA)


            Login log = Login.findByUsername(uid)
            def reservationStatuslist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def reservationStatus = ReservationStatus.findAll()
                int srno = 1
                for (ut in reservationStatus) {
                    HashMap temp = new HashMap()
                    temp.put("reservationStatus", ut.name)
                    temp.put("reservationStatusId", ut.id)
                    temp.put("reservationStatusIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    reservationStatuslist.add(temp)
                }
                hm.put('reservationStatus', reservationStatuslist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def addReservationStatus(request, hm, ip) {
        println "i am in addReservationStatus"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addReservationStatus from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def reservationStatuslist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
//                def org = user.organization
//                hm.put("org",org)

//                ReservationStatus reservationStatus = ReservationStatus.findByName(dataresponse?.reservationStatus_name)
//                if (reservationStatus != null) {
//                    hm.put("msg", "Book Status already exist!!")
//                } else {
//                    reservationStatus = new ReservationStatus()
//                    reservationStatus.isactive = true
//                    reservationStatus.name = dataresponse?.reservationStatus_name
//                    reservationStatus.updation_ip_address = ip
//                    reservationStatus.creation_ip_address = ip
//                    reservationStatus.updation_date = new Date()
//                    reservationStatus.creation_date = new Date()
//                    reservationStatus.updation_username = uid
//                    reservationStatus.creation_username = uid
//                    reservationStatus.save(flush: true, failOnError: true)
//                    hm.put("msg", "success")
//                }

//                def allP_List = ReservationStatus.findAll()
//                int srno = 1
//                for (pub in allP_List) {
//                    HashMap temp = new HashMap()
//                    temp.put("reservationStatus", pub.name)
//                    temp.put("reservationStatusId", pub.id)
//                    temp.put("reservationStatusIsactive", pub.isactive)
//                    temp.put("srno", srno++)
//                    reservationStatuslist.add(temp)
//                }
                //madhuri
                ReservationStatus reservationStatus = ReservationStatus.findByName(dataresponse?.reservationStatus_name)
                if (reservationStatus == null) {
                    reservationStatus = new ReservationStatus()
                    reservationStatus.isactive = true
                    reservationStatus.name = dataresponse?.reservationStatus_name
                    reservationStatus.updation_ip_address = ip
                    reservationStatus.creation_ip_address = ip
                    reservationStatus.updation_date = new Date()
                    reservationStatus.creation_date = new Date()
                    reservationStatus.updation_username = uid
                    reservationStatus.creation_username = uid
                    reservationStatus.save(flush: true, failOnError: true)
                    hm.put("code", "200")
                    hm.put("msg", "Added successfully")
                }
                 else {
                    hm.put("msg", "200")
                    hm.put("msg", "Already exist")
            }

                //end
                //hm.put("reservationStatus_list", reservationStatuslist)
//                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
//        return hm
    }

    def reservationStatusIsactive(request, hm) {
        println "i am in reservationStatusIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "reservationStatusIsactive from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                hm.put("org", org)

                def reservationStatus = ReservationStatus.findById(dataresponse?.reservationStatusId)
                if (reservationStatus != null) {
                    if (reservationStatus?.isactive == false) {
                        reservationStatus.isactive = true
                        reservationStatus.updation_ip_address = request.getRemoteAddr()
                        reservationStatus.updation_date = new Date()
                        reservationStatus.updation_username = uid
                        reservationStatus.save(flush: true, failOnError: true)
                    } else {
                        reservationStatus.isactive = false
                        reservationStatus.updation_ip_address = request.getRemoteAddr()
                        reservationStatus.updation_date = new Date()
                        reservationStatus.updation_username = uid
                        reservationStatus.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def editReservationStatus(request, hm, ip) {
        println "i am in editReservationStatus"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editReservationStatus from Admin Master Menu."
        //auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ReservationStatus reservationStatus = ReservationStatus.findById(dataresponse?.reservationStatusId)
                println("reservationStatus///////////"+reservationStatus)

//                def exist = ReservationStatus.findByName(dataresponse?.reservationStatusName)
//                if (exist != null) {
//                    hm.put("msg", "Reservation Status already exist!!")
//                } else {
//                    reservationStatus.name = dataresponse?.reservationStatusName
//                    reservationStatus.updation_ip_address = ip
//                    reservationStatus.updation_date = new Date()
//                    reservationStatus.updation_username = uid
//                    reservationStatus.save(flush: true, failOnError: true)
//                    hm.put("msg", "success")
//                    hm.put("status", "200")
//                    return hm
//                }
            //madhuri
                if(reservationStatus != null) {
                    reservationStatus.name = dataresponse?.reservationStatus
                    reservationStatus.updation_ip_address = ip
                    reservationStatus.updation_date = new Date()
                    reservationStatus.updation_username = uid
                    reservationStatus.save(flush: true, failOnError: true)
                    hm.put("code", "200")
                    hm.put("msg", "edited successfully")
                }
                else {
                    hm.put("code", "200")
                    hm.put("msg", "Already exist")
                }
                println("hm"+hm)
                return hm
                //end
            }
        }
//        return hm
    }

    def deleteReservationStatus(request ,hm){
        println "i am in deleteBookFormat"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteBookFormat from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ReservationStatus reservationStatus = ReservationStatus.findById(dataresponse?.reservationStatusId)
                println("reservationStatus : " + reservationStatus)
                if(reservationStatus!=null) {
                    try {
                        reservationStatus.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return
            }
        }
    }

    def getAccountStatus(request, hm) {
        println("Inside getAccountStatus")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            def accountStatuslist_SA = []
            def accountStatus_SA = AccountStatusSuperMaster.findAll()
            for (ut in accountStatus_SA) {
                HashMap temp = new HashMap()
                temp.put("accountStatus", ut.name)
                temp.put("accountStatusId", ut.id)
                temp.put("accountStatusIsactive", ut.isactive)
                accountStatuslist_SA.add(temp)
            }
            hm.put('accountStatus_SA', accountStatuslist_SA)


            Login log = Login.findByUsername(uid)
            def accountStatuslist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def accountStatus = AccountStatus.findAll()
                int srno = 1
                for (ut in accountStatus) {
                    HashMap temp = new HashMap()
                    temp.put("accountStatus", ut.name)
                    temp.put("accountStatusId", ut.id)
                    temp.put("accountStatusIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    accountStatuslist.add(temp)
                }
                hm.put('accountStatus', accountStatuslist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def addAccountStatus(request, hm, ip) {
        println "i am in addAccountStatus"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addAccountStatus from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def accountStatuslist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
//                def org = user.organization
//                hm.put("org",org)

                AccountStatus accountStatus = AccountStatus.findByName(dataresponse?.accountStatus_name)
                if (accountStatus != null) {
                    hm.put("msg", "Account Status already exist!!")
                } else {
                    accountStatus = new AccountStatus()
                    accountStatus.isactive = true
                    accountStatus.name = dataresponse?.accountStatus_name
                    accountStatus.updation_ip_address = ip
                    accountStatus.creation_ip_address = ip
                    accountStatus.updation_date = new Date()
                    accountStatus.creation_date = new Date()
                    accountStatus.updation_username = uid
                    accountStatus.creation_username = uid
                    accountStatus.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def allP_List = AccountStatus.findAll()
                int srno = 1
                for (pub in allP_List) {
                    HashMap temp = new HashMap()
                    temp.put("accountStatus", pub.name)
                    temp.put("accountStatusId", pub.id)
                    temp.put("accountStatusIsactive", pub.isactive)
                    temp.put("srno", srno++)
                    accountStatuslist.add(temp)
                }
                hm.put("accountStatus_list", accountStatuslist)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def accountStatusIsactive(request, hm) {
        println "i am in accountStatusIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "accountStatusIsactive from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                hm.put("org", org)

                def accountStatus = AccountStatus.findById(dataresponse?.accountStatusId)
                if (accountStatus != null) {
                    if (accountStatus?.isactive == false) {
                        accountStatus.isactive = true
                        accountStatus.updation_ip_address = request.getRemoteAddr()
                        accountStatus.updation_date = new Date()
                        accountStatus.updation_username = uid
                        accountStatus.save(flush: true, failOnError: true)
                    } else {
                        accountStatus.isactive = false
                        accountStatus.updation_ip_address = request.getRemoteAddr()
                        accountStatus.updation_date = new Date()
                        accountStatus.updation_username = uid
                        accountStatus.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def editAccountStatus(request, hm, ip) {
        println "i am in editAccountStatus"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editAccountStatus from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                AccountStatus accountStatus = AccountStatus.findById(dataresponse?.accountStatusId)

                def exist = AccountStatus.findByName(dataresponse?.accountStatusName)
                if (exist != null) {
                    hm.put("msg", "Account Status already exist!!")
                } else {
                    accountStatus.name = dataresponse?.accountStatusName
                    accountStatus.updation_ip_address = ip
                    accountStatus.updation_date = new Date()
                    accountStatus.updation_username = uid
                    accountStatus.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm
                }
            }
        }
        return hm
    }

    def deleteAccountStatus(request ,hm){
        println "i am in deleteAccountStatus"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteAccountStatus from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                AccountStatus accountStatus = AccountStatus.findById(dataresponse?.accountStatusId)
                println("accountStatus : " + accountStatus)
                println("accountStatus : " + accountStatus)
                if(accountStatus!=null) {
                    try {
                        accountStatus.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return
            }
        }
    }

    def getCurrency(request, hm) {
        println("Inside getCurrency")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def currencylist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def currency = Currency.findAll()
                int srno = 1
                for (ut in currency) {
                    HashMap temp = new HashMap()
                    temp.put("currency", ut.name)
                    temp.put("currencyId", ut.id)
                    temp.put("currencyIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    currencylist.add(temp)
                }
                hm.put('currency', currencylist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def addCurrency(request, hm, ip) {
        println "i am in addCurrency"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addCurrency from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def currencylist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
//                def org = user.organization
//                hm.put("org",org)

                Currency currency = Currency.findByName(dataresponse?.currency_name)
                if (currency != null) {
                    hm.put("msg", "Account Status already exist!!")
                } else {
                    currency = new Currency()
                    currency.isactive = true
                    currency.name = dataresponse?.currency_name
                    currency.updation_ip_address = ip
                    currency.creation_ip_address = ip
                    currency.updation_date = new Date()
                    currency.creation_date = new Date()
                    currency.updation_username = uid
                    currency.creation_username = uid
                    currency.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def allP_List = Currency.findAll()
                int srno = 1
                for (pub in allP_List) {
                    HashMap temp = new HashMap()
                    temp.put("currency", pub.name)
                    temp.put("currencyId", pub.id)
                    temp.put("currencyIsactive", pub.isactive)
                    temp.put("srno", srno++)
                    currencylist.add(temp)
                }
                hm.put("currency_list", currencylist)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def currencyIsactive(request, hm) {
        println "i am in currencyIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "currencyIsactive from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                hm.put("org", org)

                def currency = Currency.findById(dataresponse?.currencyId)
                if (currency != null) {
                    if (currency?.isactive == false) {
                        currency.isactive = true
                        currency.updation_ip_address = request.getRemoteAddr()
                        currency.updation_date = new Date()
                        currency.updation_username = uid
                        currency.save(flush: true, failOnError: true)
                    } else {
                        currency.isactive = false
                        currency.updation_ip_address = request.getRemoteAddr()
                        currency.updation_date = new Date()
                        currency.updation_username = uid
                        currency.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def editCurrency(request, hm, ip) {
        println "i am in editCurrency"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editCurrency from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                Currency currency = Currency.findById(dataresponse?.currencyId)

                def exist = Currency.findByName(dataresponse?.currencyName)
                if (exist != null) {
                    hm.put("msg", "Currency already exist!!")
                } else {
                    currency.name = dataresponse?.currencyName
                    currency.updation_ip_address = ip
                    currency.updation_date = new Date()
                    currency.updation_username = uid
                    currency.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm
                }
            }
        }
        return hm
    }

    def deleteCurrency(request ,hm){
        println "in deleteCurrency"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        String purpose = "deleteCurrency from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                Currency currency = Currency.findById(dataresponse?.currencyId)
                println("currency : " + currency)
                if(currency!=null) {
                    try{
                        currency.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return
            }
        }
    }

    def getBookCondition(request, hm) {
        println("Inside getBookCondition")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def bookConditionlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def bookCondition = BookCondition.findAll()
                int srno = 1
                for (ut in bookCondition) {
                    HashMap temp = new HashMap()
                    temp.put("bookCondition", ut.name)
                    temp.put("bookConditionId", ut.id)
                    temp.put("bookConditionIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    bookConditionlist.add(temp)
                }
                hm.put('bookCondition', bookConditionlist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def addBookCondition(request, hm, ip) {
        println "i am in addBookCondition"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addBookCondition from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def bookConditionlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
//                def org = user.organization
//                hm.put("org",org)

                BookCondition bookCondition = BookCondition.findByName(dataresponse?.bookCondition_name)
                if (bookCondition != null) {
                    hm.put("msg", "Account Status already exist!!")
                } else {
                    bookCondition = new BookCondition()
                    bookCondition.isactive = true
                    bookCondition.name = dataresponse?.bookCondition_name
                    bookCondition.updation_ip_address = ip
                    bookCondition.creation_ip_address = ip
                    bookCondition.updation_date = new Date()
                    bookCondition.creation_date = new Date()
                    bookCondition.updation_username = uid
                    bookCondition.creation_username = uid
                    bookCondition.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def allP_List = BookCondition.findAll()
                int srno = 1
                for (pub in allP_List) {
                    HashMap temp = new HashMap()
                    temp.put("bookCondition", pub.name)
                    temp.put("bookConditionId", pub.id)
                    temp.put("bookConditionIsactive", pub.isactive)
                    temp.put("srno", srno++)
                    bookConditionlist.add(temp)
                }
                hm.put("bookCondition_list", bookConditionlist)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def bookConditionIsactive(request, hm) {
        println "i am in bookConditionIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "bookConditionIsactive from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                hm.put("org", org)

                def bookCondition = BookCondition.findById(dataresponse?.bookConditionId)
                if (bookCondition != null) {
                    if (bookCondition?.isactive == false) {
                        bookCondition.isactive = true
                        bookCondition.updation_ip_address = request.getRemoteAddr()
                        bookCondition.updation_date = new Date()
                        bookCondition.updation_username = uid
                        bookCondition.save(flush: true, failOnError: true)
                    } else {
                        bookCondition.isactive = false
                        bookCondition.updation_ip_address = request.getRemoteAddr()
                        bookCondition.updation_date = new Date()
                        bookCondition.updation_username = uid
                        bookCondition.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def editBookCondition(request, hm, ip) {
        println "i am in editBookCondition"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editBookCondition from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                BookCondition bookCondition = BookCondition.findById(dataresponse?.bookConditionId)

                def exist = BookCondition.findByName(dataresponse?.bookConditionName)
                if (exist != null) {
                    hm.put("msg", "Book Condition already exist!!")
                } else {
                    bookCondition.name = dataresponse?.bookConditionName
                    bookCondition.updation_ip_address = ip
                    bookCondition.updation_date = new Date()
                    bookCondition.updation_username = uid
                    bookCondition.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm
                }
            }
        }
        return hm
    }

    def deleteBookCondition(request ,hm){
        println "i am in deleteBookFormat"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteBookFormat from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                BookCondition bookCondition = BookCondition.findById(dataresponse?.bookConditionId)
                println("bookCondition : " + bookCondition)
                if(bookCondition!=null) {
                    try{ bookCondition.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return
            }
        }
    }

    def getNotifyType(request, hm) {
        println("Inside getNotifyType")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            def notifyTypelist_SA = []
            for (ut in NotificationTypeSuperMaster.findAll()) {
                HashMap temp = new HashMap()
                temp.put("notifyType", ut.name)
                notifyTypelist_SA.add(temp)
            }
            hm.put('notifyTypelist_SA', notifyTypelist_SA)

            Login log = Login.findByUsername(uid)
            def notifyTypelist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def notifyType = NotificationType.findAll()
                int srno = 1
                for (ut in notifyType) {
                    HashMap temp = new HashMap()
                    temp.put("notifyType", ut.name)
                    temp.put("notifyTypeId", ut.id)
                    temp.put("notifyTypeIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    notifyTypelist.add(temp)
                }
                hm.put('notifyType', notifyTypelist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def addNotifyType(request, hm, ip) {
        println "i am in addNotifyType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addNotifyType from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def notifyTypelist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
//                def org = user.organization
//                hm.put("org",org)

                NotificationType notifyType = NotificationType.findByName(dataresponse?.notifyType_name)
                if (notifyType != null) {
                    hm.put("msg", "Notification type already exist..")
                } else {
                    notifyType = new NotificationType()
                    notifyType.isactive = true
                    notifyType.name = dataresponse?.notifyType_name
                    notifyType.updation_ip_address = ip
                    notifyType.creation_ip_address = ip
                    notifyType.updation_date = new Date()
                    notifyType.creation_date = new Date()
                    notifyType.updation_username = uid
                    notifyType.creation_username = uid
                    notifyType.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def allP_List = NotificationType.findAll()
                int srno = 1
                for (pub in allP_List) {
                    HashMap temp = new HashMap()
                    temp.put("notifyType", pub.name)
                    temp.put("notifyTypeId", pub.id)
                    temp.put("notifyTypeIsactive", pub.isactive)
                    temp.put("srno", srno++)
                    notifyTypelist.add(temp)
                }
                hm.put("notifyType_list", notifyTypelist)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def notifyTypeIsactive(request, hm) {
        println "i am in notifyTypeIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "notifyTypeIsactive from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                hm.put("org", org)

                def notifyType = NotificationType.findById(dataresponse?.notifyTypeId)
                if (notifyType != null) {
                    if (notifyType?.isactive == false) {
                        notifyType.isactive = true
                        notifyType.updation_ip_address = request.getRemoteAddr()
                        notifyType.updation_date = new Date()
                        notifyType.updation_username = uid
                        notifyType.save(flush: true, failOnError: true)
                    } else {
                        notifyType.isactive = false
                        notifyType.updation_ip_address = request.getRemoteAddr()
                        notifyType.updation_date = new Date()
                        notifyType.updation_username = uid
                        notifyType.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def editNotifyType(request, hm, ip) {
        println "i am in editNotifyType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editNotifyType from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                NotificationType notifyType = NotificationType.findById(dataresponse?.notifyTypeId)

                def exist = NotificationType.findByName(dataresponse?.notifyTypeName)
                if (exist != null) {
                    hm.put("msg", "Notification type already exist!!")
                } else {
                    notifyType.name = dataresponse?.notifyTypeName
                    notifyType.updation_ip_address = ip
                    notifyType.updation_date = new Date()
                    notifyType.updation_username = uid
                    notifyType.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm
                }
            }
        }
        return hm
    }

    def deleteNotificationType(request ,hm){
        println "i am in deleteNotificationType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteNotificationType from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                NotificationType notificationType = NotificationType.findById(dataresponse?.notifyTypeId)
                println("notificationType : " + notificationType)
                if(notificationType!=null) {
                    try {
                        notificationType.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return
            }
        }
    }

    def getTranType(request, hm) {
        println("Inside getTranType")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            def tranTypelist_SA =[]
            def tranType_SA = TransactionTypeSuperMaster.findAll()
            for (ut in tranType_SA) {
                HashMap temp = new HashMap()
                temp.put("tranType", ut.name)
                temp.put("tranTypeId", ut.id)
                temp.put("tranTypeIsactive", ut.isactive)
                tranTypelist_SA.add(temp)
            }
            hm.put('tranTypelist_SA', tranTypelist_SA)

            Login log = Login.findByUsername(uid)
            def tranTypelist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def tranType = TransactionType.findAll()
                int srno = 1
                for (ut in tranType) {
                    HashMap temp = new HashMap()
                    temp.put("tranType", ut.name)
                    temp.put("tranTypeId", ut.id)
                    temp.put("short_name", ut.short_name)
                    temp.put("display_order", ut.display_order)
                    temp.put("tranTypeIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    tranTypelist.add(temp)
                }
                hm.put('tranType', tranTypelist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def addTranType(request, hm, ip) {
        println "i am in addTranType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addTranType from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def tranTypelist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                TransactionType tranType = TransactionType.findByName(dataresponse?.tranType_name)
                if (tranType != null) {
                    hm.put("msg", "Account Status already exist!!")
                } else {
                    tranType = new TransactionType()
                    tranType.isactive = true
                    tranType.name = dataresponse?.tranType_name
                    tranType.display_order = dataresponse?.display_order
                    tranType.short_name = dataresponse?.short_name
                    tranType.updation_ip_address = ip
                    tranType.creation_ip_address = ip
                    tranType.updation_date = new Date()
                    tranType.creation_date = new Date()
                    tranType.updation_username = uid
                    tranType.creation_username = uid
                    tranType.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def allP_List = TransactionType.findAll()
                int srno = 1
                for (pub in allP_List) {
                    HashMap temp = new HashMap()
                    temp.put("tranType", pub.name)
                    temp.put("tranTypeId", pub.id)
                    temp.put("tranTypeIsactive", pub.isactive)
                    temp.put("srno", srno++)
                    tranTypelist.add(temp)
                }
                hm.put("tranType_list", tranTypelist)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def tranTypeIsactive(request, hm) {
        println "i am in tranTypeIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "tranTypeIsactive from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                hm.put("org", org)

                def transType = TransactionType.findById(dataresponse?.tranTypeId)
                if (transType != null) {
                    if (transType?.isactive == false) {
                        transType.isactive = true
                        transType.updation_ip_address = request.getRemoteAddr()
                        transType.updation_date = new Date()
                        transType.updation_username = uid
                        transType.save(flush: true, failOnError: true)
                    } else {
                        transType.isactive = false
                        transType.updation_ip_address = request.getRemoteAddr()
                        transType.updation_date = new Date()
                        transType.updation_username = uid
                        transType.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def editTranType(request, hm, ip) {
        println "i am in editTranType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println"dataresponse-->"+ dataresponse

        String purpose = "editTranType from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                TransactionType transType = TransactionType.findById(dataresponse?.tranTypeId)
                def exist = TransactionType.findByNameAndShort_name(dataresponse?.tranTypeName,dataresponse.short_name)
                if (exist != null) {
                    hm.put("msg", "Transaction Type already exist!!")
                } else {
                    transType.name = dataresponse?.tranTypeName
                    transType.display_order = dataresponse?.display_order
                    transType.short_name = dataresponse?.short_name
                    transType.updation_ip_address = ip
                    transType.updation_date = new Date()
                    transType.updation_username = uid
                    transType.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm
                }
            }
        }
        return hm
    }

    def deleteTransactionType(request ,hm){
        println "i am in deleteTransactionType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteTransactionType from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                TransactionType transactionType = TransactionType.findById(dataresponse?.tranTypeId)
                println("transactionType : " + transactionType)
                if(transactionType!=null) {
                    try {
                        transactionType.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return
            }
        }
    }

    def getRackData(request, hm, ip) {
        println("in getRackData:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String purpose = "getRackData from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList rack_list = new ArrayList()
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization.name
//                for (item in RoleLinks.findAllByOrganization(user.organization)) {
                int srno = 1
                for (item in Rack.findAll()) {
                    HashMap temp = new HashMap()
                    temp.put("rackId", item.id)
                    temp.put("displayOrder", item.display_order)
                    temp.put("rackNo", item.rack_number)
                    temp.put("blockNo", item.block_number)
                    temp.put("location", item.location_identifier)
                    temp.put("capacity", item.capacity)
                    temp.put("bookCategory", item.bookcategory?.name)
                    temp.put("bookCategoryId", item.bookcategory?.id)
                    temp.put("isactive", item.isactive)
                    temp.put("srno", srno++)
                    rack_list.add(temp)
                }

                def bookCategory = BookCategory.findAll()
                def bookCatArr = []
                for (book in bookCategory) {
                    HashMap temp = new HashMap()
                    temp.put('bookCategoryName', book?.name)
                    temp.put('bookCategoryId', book?.id)
                    bookCatArr.add(temp)
                }

                hm.put("rack_list", rack_list)
                hm.put("bookCategory_list", bookCatArr)

            }//println"hm: "+hm
            return hm

        }
        return hm
    }

    def editRack(request, hm, ip) {
        println("in editRack:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:" + dataresponse

        AuthService auth = new AuthService()

        String purpose = "Editing rack from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
//                Rack rack = Rack.findById(dataresponse.rackId)
                def rack = Rack.findByRack_numberAndBlock_number(dataresponse.rackNo, dataresponse.blockNo)
                def bookCategory = BookCategory.findByName(dataresponse.bookCategory)
                if (rack != null) {
                    rack.rack_number = dataresponse.rackNo
                    rack.block_number = dataresponse.blockNo
                    rack.display_order = dataresponse.displayOrder
                    rack.capacity = dataresponse.capacity
                    rack.available = dataresponse.capacity
                    rack.bookcategory = bookCategory
                    rack.location_identifier = dataresponse.location
                    rack.updation_username = uid
                    rack.updation_date = new java.util.Date()
                    rack.updation_ip_address = ip
                    rack.save(failOnError: true, flush: true)

                    HashMap status = new HashMap()
                    status.put("code", "SUCCESS")
                    hm.put("status", status)
                }
//                else {
//                    HashMap status = new HashMap()
//                    status.put("code", "NA")
//                    status.put("message", "Rack Already present!!!")
//                    hm.put("status", status)
//                }
                return hm
            }
        }
        return hm
    }

    def saveRack(request, hm, ip) {
        println("in saveRack:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:" + dataresponse

        AuthService auth = new AuthService()

        String purpose = "save Rack from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        println("dataresponse")
        println(dataresponse)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def bookCategory = BookCategory.findByName(dataresponse.bookCategory)
                println('bookCategory----' + bookCategory)
                def rack = Rack.findByRack_numberAndBlock_number(dataresponse.rackNo, dataresponse.blockNo)
                if (rack == null) {
                    rack = new Rack()
                    rack.rack_number = dataresponse.rackNo
                    rack.block_number = dataresponse.blockNo
                    rack.display_order = dataresponse.displayOrder
                    rack.capacity = dataresponse.capacity
                    rack.available = dataresponse.capacity
                    rack.bookcategory = bookCategory
                    rack.location_identifier = dataresponse.location
                    rack.isactive = true
                    rack.creation_username = uid
                    rack.updation_username = uid
                    rack.creation_date = new java.util.Date()
                    rack.updation_date = new java.util.Date()
                    rack.creation_ip_address = ip
                    rack.updation_ip_address = ip
                    rack.save(failOnError: true, flush: true)

                    HashMap status = new HashMap()
                    status.put("code", "SUCCESS")
                    status.put("message", "Rack added successfully...")
                    hm.put("status", status)
                    return hm
                } else {
                    HashMap status = new HashMap()
                    status.put("code", "NA")
                    status.put("message", "Rack Already present!!!")
                    hm.put("status", status)
                }
            }
//            }
            return hm

        }
        return hm
    }

    def rackIsactive(request, hm, ip) {
        println "i am in rackIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "rackIsactive from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                hm.put("org", org)

                def rack = Rack.findById(dataresponse?.rackId)
                if (rack != null) {
                    if (rack?.isactive == false) {
                        rack.isactive = true
                        rack.updation_ip_address = request.getRemoteAddr()
                        rack.updation_date = new Date()
                        rack.updation_username = uid
                        rack.save(flush: true, failOnError: true)
                    } else {
                        rack.isactive = false
                        rack.updation_ip_address = request.getRemoteAddr()
                        rack.updation_date = new Date()
                        rack.updation_username = uid
                        rack.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }
    def deleteRack(request ,hm,ip){
        println "i am in deleteRack service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "rack"+dataresponse

        String purpose = "deleteRack from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            println("log.tenantclient.tenantid : "+log.tenantclient.tenantid)
            Tenants.withId(log.tenantclient.tenantid) {
                println("log.username : "+log.username)
                def user = User.findByUsername(log.username)
                Rack rack = Rack.findById(dataresponse?.rackId)
                println("rack : " + rack)
                if(rack!=null) {
                    try{
                        rack.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }

            }
        }
    }

    def fetchAllUsertype(request, hm) {
        println("Inside AdminService/find all user type...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        //String body = request.getReader().text
        //def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        def usertypelist = []
        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def loged_user = User.findByUsername(log.username)
//                def organization = user.organization
                def usertype = Role.findAll()
                int i = 1
                for (ut in usertype) {
                    HashMap temp = new HashMap()
                    temp.put("usertype", ut?.name)
                    temp.put("usertypeid", ut?.id)
                    temp.put("srno", i)
                    usertypelist.add(temp)
                    i++
                }
                hm.put("usertypelist", usertypelist)

                //library policy
                def librarypolicy = []
                def libraryPolicy = LibraryPolicy.findAllByOrganization(loged_user?.organization)
                int j = 1
                for (lp in libraryPolicy) {
                    HashMap temp = new HashMap()
                    temp.put("libpolicyid", lp?.id)
                    temp.put("role", lp?.role.name)
                    temp.put("number_of_books", lp?.number_of_books)
                    temp.put("number_of_days", lp?.number_of_days)
                    temp.put("fine_rate_per_day", lp?.fine_rate_per_day)
                    temp.put("reservation_validity_days", lp?.reservation_validity_days)
                    temp.put("book_reservation_limit", lp?.book_reservation_limit)
                    temp.put("isactive", lp?.isactive)
                    temp.put("serialno", j)
                    HashMap temp1 = new HashMap()
                    temp1.put("usertype", lp?.role?.name)
                    temp1.put("usertypeid", lp?.role?.id)
                    temp1.put("srno", j)
                    temp.put("usertypelist", temp1)
                    librarypolicy.add(temp)
                    j++
                }
                hm.put("librarypolicy", librarypolicy)
                //
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def savelibrarypolicy(request, hm, ip) {
        println("in saveAdminAddRoleLink:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:" + dataresponse

        def selectedusertype = dataresponse["selectedusertype"]
        def bookborrowlimit = dataresponse["bookborrowlimit"]
        def bookholdingdays = dataresponse["bookholdingdays"]
        def fineperday = dataresponse["fineperday"]
        def reservationvalidityday = dataresponse["reservationvalidityday"]
        def reservationlimit = dataresponse["reservationlimit"]
        AuthService auth = new AuthService()
        String purpose = "Saving library policy "
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                Role role = Role.findById(selectedusertype.usertypeid)
                LibraryPolicy libraryPolicy = LibraryPolicy.findByRoleAndOrganization(role,user.organization)

                if (libraryPolicy == null) {
                    libraryPolicy = new LibraryPolicy()
                    libraryPolicy.number_of_books = Integer.parseInt(bookborrowlimit)
                    libraryPolicy.number_of_days = Integer.parseInt(bookholdingdays)
                    libraryPolicy.fine_rate_per_day = Integer.parseInt(fineperday)
                    libraryPolicy.reservation_validity_days = Integer.parseInt(reservationvalidityday)
                    libraryPolicy.book_reservation_limit = Integer.parseInt(reservationlimit)
                    libraryPolicy.isactive = true
                    libraryPolicy.role = role
                    libraryPolicy.organization  = user.organization
                    libraryPolicy.creation_username = uid
                    libraryPolicy.updation_username = uid
                    libraryPolicy.creation_date = new java.util.Date()
                    libraryPolicy.updation_date = new java.util.Date()
                    libraryPolicy.creation_ip_address = ip
                    libraryPolicy.updation_ip_address = ip
                    libraryPolicy.save(failOnError: true, flush: true)
                    HashMap status = new HashMap()
                    status.put("code", "SUCCESS")
                    status.put("message", "Role added successfully...")
                    hm.put("status", status)
                    hm.put("msg", "200")
                }
                return hm
            }
            return hm
        }
    }

    def updateLibraryPolicy(request, hm, ip) {
        println("in service update Library Policy:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:" + dataresponse


        def libpolicyid = dataresponse["libpolicyid"]
        def number_of_books = dataresponse["number_of_books"]
        def number_of_days = dataresponse["number_of_days"]
        def fine_rate_per_day = dataresponse["fine_rate_per_day"]
        def reservation_validity_days = dataresponse["reservation_validity_days"]
        def book_reservation_limit = dataresponse["book_reservation_limit"]

        def reservationlimit = dataresponse["reservationlimit"]
        AuthService auth = new AuthService()
        String purpose = "Saving library policy "
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                LibraryPolicy libraryPolicy = LibraryPolicy.findById(libpolicyid)
                libraryPolicy.number_of_books = dataresponse.number_of_books
                libraryPolicy.number_of_days = dataresponse.number_of_days
                libraryPolicy.fine_rate_per_day = dataresponse.fineperday
                libraryPolicy.reservation_validity_days = dataresponse.reservationvalidityday
                libraryPolicy.book_reservation_limit = dataresponse.reservationlimit
                libraryPolicy.isactive = true
                libraryPolicy.organization  = user.organization
                libraryPolicy.updation_username = uid
                libraryPolicy.updation_date = new java.util.Date()
                libraryPolicy.updation_ip_address = ip
                libraryPolicy.save(failOnError: true, flush: true)
                HashMap status = new HashMap()
                status.put("code", "SUCCESS")
                status.put("message", "Library policy updated successfully...")
                hm.put("status", status)
                hm.put("msg", "200")
                return hm
            }
            return hm
        }
    }

    def isactivelibrarypolicy(request, hm, ip) {
        println("in service update Library Policy:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:" + dataresponse


        def libpolicyid = dataresponse["libpolicyid"]
        def isactive = dataresponse["isactive"]

        println "------------------isactive-----------------" + isactive
        println "------------------libpolicyid-----------------" + libpolicyid

        AuthService auth = new AuthService()
        String purpose = "Saving library policy "
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                LibraryPolicy libraryPolicy = LibraryPolicy.findById(libpolicyid)
                println "<<<<<<<<<<<<<<<libraryPolicy>>>>>>>>>>>>>>>>"
                if (isactive == false) {
                    println "false"
                    libraryPolicy.isactive = false
                } else {
                    println "true"
                    libraryPolicy.isactive = true
                }
                libraryPolicy.updation_username = uid
                libraryPolicy.updation_date = new java.util.Date()
                libraryPolicy.updation_ip_address = ip
                libraryPolicy.save(failOnError: true, flush: true)
                HashMap status = new HashMap()
                status.put("code", "SUCCESS")
                status.put("message", "Library policy updated successfully...")
                hm.put("status", status)
                hm.put("msg", "200")
                return hm
            }
            return hm
        }
    }


    def getTemplateData(request, hm, ip) {
        println("in getTemplateData:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String purpose = "getTemplateData from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList template_list = new ArrayList()
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization.name
                int srno = 1
                for (item in NotificationTemplate.findAll()) {
                    HashMap temp = new HashMap()
                    temp.put("srno", srno++)
                    temp.put("id", item.id)
                    temp.put("templateName", item.template_name)
                    temp.put("displayOrder", item.display_order)
                    temp.put("shortName", item.short_name)
                    temp.put("subject", item.subject)
                    temp.put("body", item.message)
                    temp.put("isactive", item.isactive)
                    temp.put("commModeName", item?.notificationmethod?.name)
                    temp.put("notifyType", item?.notificationtype?.name)
                    temp.put("notifyTypeId", item?.notificationtype?.id)
                    temp.put("commMode", item?.notificationmethod?.id)
                    template_list.add(temp)
                }

                def communicationMethod = NotificationMethod.findAll()
                def notifyArr = []
                for (mode in communicationMethod) {
                    HashMap temp = new HashMap()
                    temp.put('modeName', mode?.name)
                    temp.put('modeId', mode?.id)
                    notifyArr.add(temp)
                }

                def notifyType = NotificationType.findAll()
                def notifyTypeArr = []
                for (n_type in notifyType) {
                    HashMap temp = new HashMap()
                    temp.put('typeName', n_type?.name)
                    temp.put('typeId', n_type?.id)
                    notifyTypeArr.add(temp)
                }

                hm.put("template_list", template_list)
                hm.put("notifyTypes", notifyTypeArr)
                hm.put("communicationMethod", notifyArr)

            }//println"hm: "+hm
            return hm

        }
        return hm
    }

    def editTemplate(request, hm, ip) {
        println("in editTemplate:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:" + dataresponse

        AuthService auth = new AuthService()

        String purpose = "Editing rack from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def commMode = NotificationMethod.findById(dataresponse.commMode)
                def notifyType = NotificationType.findById(dataresponse.notifyTypeId)
                def notifyTemplate = NotificationTemplate.findByTemplate_name(dataresponse.templateName)
                if (notifyTemplate != null) {
                    notifyTemplate.template_name = dataresponse.templateName
                    notifyTemplate.message = dataresponse.body
                    notifyTemplate.display_order = dataresponse.displayOrder
                    notifyTemplate.short_name = dataresponse.shortName
                    println"ShortName---->"+dataresponse.shortName
                    notifyTemplate.subject = dataresponse.subject
                    notifyTemplate.notificationmethod = commMode
                    notifyTemplate.notificationtype = notifyType
                    notifyTemplate.updation_username = uid
                    notifyTemplate.updation_date = new java.util.Date()
                    notifyTemplate.updation_ip_address = ip
                    notifyTemplate.save(failOnError: true, flush: true)

                    HashMap status = new HashMap()
                    status.put("code", "SUCCESS")
                    hm.put("status", status)
                }
                return hm
            }
        }
        return hm
    }

    def saveTemplate(request, hm, ip) {
        println("in saveTemplate:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:" + dataresponse

        AuthService auth = new AuthService()

        String purpose = "saveTemplate from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        println("dataresponse in saveTemplate-->")
        println(dataresponse)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def commMode = NotificationMethod.findById(dataresponse.commMode)
                def notifyType = NotificationType.findById(dataresponse.notifyTypeId)
                def notifyTemplate = NotificationTemplate.findByTemplate_name(dataresponse.templateName)
                if (notifyTemplate == null) {
                    notifyTemplate = new NotificationTemplate()

                    notifyTemplate.template_name = dataresponse.templateName
                    notifyTemplate.display_order = java.lang.Integer.parseInt(dataresponse.displayOrder)
                    notifyTemplate.short_name = dataresponse.shortName
                    notifyTemplate.message = dataresponse.body
                    notifyTemplate.subject = dataresponse.subject
                    notifyTemplate.notificationmethod = commMode
                    notifyTemplate.notificationtype = notifyType
                    notifyTemplate.isactive = true
                    notifyTemplate.creation_username = uid
                    notifyTemplate.updation_username = uid
                    notifyTemplate.creation_date = new java.util.Date()
                    notifyTemplate.updation_date = new java.util.Date()
                    notifyTemplate.creation_ip_address = ip
                    notifyTemplate.updation_ip_address = ip
                    notifyTemplate.save(failOnError: true, flush: true)

                    HashMap status = new HashMap()
                    status.put("code", "SUCCESS")
                    status.put("message", "Notification Template added successfully...")
                    hm.put("status", status)
                    return hm
                } else {
                    HashMap status = new HashMap()
                    status.put("code", "NA")
                    status.put("message", "Notification Template Already present!!!")
                    hm.put("status", status)
                }
            }
//            }
            return hm

        }
        return hm
    }

    def templateIsactive(request, hm, ip) {
        println "i am in templateIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "templateIsactive from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                hm.put("org", org)

                def template = NotificationTemplate.findById(dataresponse?.id)
                if (template != null) {
                    if (template?.isactive == false) {
                        template.isactive = true
                        template.updation_ip_address = request.getRemoteAddr()
                        template.updation_date = new Date()
                        template.updation_username = uid
                        template.save(flush: true, failOnError: true)
                    } else {
                        template.isactive = false
                        template.updation_ip_address = request.getRemoteAddr()
                        template.updation_date = new Date()
                        template.updation_username = uid
                        template.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def deleteNotificationTemp(request ,hm){
        println "i am in deleteNotificationTemp"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteNotificationTemp from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                NotificationTemplate notificationTemplate = NotificationTemplate.findById(dataresponse?.id)
                println("notificationTemplate : " + notificationTemplate)
                if(notificationTemplate!=null) {
                    try{
                        notificationTemplate.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    } catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }
                render hm
                return
            }
        }
    }

    def getNotifyMethodData(request, hm, ip) {
        println("in getNotifyMethodData:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String purpose = "getNotifyMethodData from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            def methods_list_SA = []
            for (item in NotificationMethodSuperMaster.findAll()) {
                HashMap temp = new HashMap()
                temp.put("name", item.name)
                methods_list_SA.add(temp)
            }

            hm.put("methods_list_SA", methods_list_SA)

            Login log = Login.findByUsername(uid)
            ArrayList methods_list = new ArrayList()
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization.name
                int srno = 1
                for (item in NotificationMethod.findAll()) {
                    HashMap temp = new HashMap()
                    temp.put("srno", srno++)
                    temp.put("id", item.id)
                    temp.put("name", item.name)
                    temp.put("from", item.from_user)
                    temp.put("from_Username", item.from_username)
                    temp.put("from_password", item.from_password)
                    temp.put("isactive", item.isactive)
                    methods_list.add(temp)
                }

                hm.put("methods_list", methods_list)
            }//println"hm: "+hm
            return hm

        }
        return hm
    }

    def editNotifyMethod(request, hm, ip) {
        println("in editNotifyMethod:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:" + dataresponse

        AuthService auth = new AuthService()

        String purpose = "editNotifyMethod  from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def notifyMethod = NotificationMethod.findById(dataresponse.id)
                println('notifyMethod---------'+notifyMethod)
                if (notifyMethod != null) {
//                    notifyMethod.name = dataresponse.name
                    notifyMethod.from_user = dataresponse.from
                    notifyMethod.from_username = dataresponse.from_Username
                    notifyMethod.from_password = dataresponse.from_password
                    notifyMethod.updation_username = uid
                    notifyMethod.updation_date = new java.util.Date()
                    notifyMethod.updation_ip_address = ip
                    notifyMethod.save(failOnError: true, flush: true)

                    HashMap status = new HashMap()
                    status.put("code", "SUCCESS")
                    hm.put("status", status)
                }
                return hm
            }
        }
        return hm
    }

    def saveNotifyMethod(request, hm, ip) {
        println("in saveNotifyMethod:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:" + dataresponse

        AuthService auth = new AuthService()

        String purpose = "saveNotifyMethod from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        println("dataresponse")
        println(dataresponse)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def notifyMethod = NotificationMethod.findByName(dataresponse.name)
                if (notifyMethod == null) {
                    notifyMethod = new NotificationMethod()


                    notifyMethod.name = dataresponse.name
                    notifyMethod.from_user = dataresponse.from
                    notifyMethod.from_username = dataresponse.from_Username
                    notifyMethod.from_password = dataresponse.from_password
                    notifyMethod.isactive = true
                    notifyMethod.creation_username = uid
                    notifyMethod.updation_username = uid
                    notifyMethod.creation_date = new java.util.Date()
                    notifyMethod.updation_date = new java.util.Date()
                    notifyMethod.creation_ip_address = ip
                    notifyMethod.updation_ip_address = ip
                    notifyMethod.save(failOnError: true, flush: true)

                    HashMap status = new HashMap()
                    status.put("code", "SUCCESS")
                    status.put("message", "Notification method added successfully...")
                    hm.put("status", status)
                    return hm
                } else {
                    HashMap status = new HashMap()
                    status.put("code", "NA")
                    status.put("message", "Notification method Already present!!!")
                    hm.put("status", status)
                }
            }
//            }
            return hm

        }
        return hm
    }

    def nMethodIsactive(request, hm, ip) {
        println "i am in nMethodIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "nMethodIsactive from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                hm.put("org", org)

                def method = NotificationMethod.findById(dataresponse?.id)
                if (method != null) {
                    if (method?.isactive == false) {
                        method.isactive = true
                        method.updation_ip_address = request.getRemoteAddr()
                        method.updation_date = new Date()
                        method.updation_username = uid
                        method.save(flush: true, failOnError: true)
                    } else {
                        method.isactive = false
                        method.updation_ip_address = request.getRemoteAddr()
                        method.updation_date = new Date()
                        method.updation_username = uid
                        method.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def deleteNotificationMethod(request ,hm){
        println "i am in deleteNotificationMethod"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        println dataresponse
        String purpose = "deleteNotificationMethod from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                NotificationMethod notificationMethod = NotificationMethod.findById(dataresponse?.id)
                println("notificationMethod : " + notificationMethod)
                if(notificationMethod!=null) {
                    try {
                        notificationMethod.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        println("hm : "+hm)
                        return hm
                    }catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return
            }
        }
    }

    def getLibConfig(request, hm) {
        println("Inside getLibConfig.")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {

            def libConfiglist_SA = []
            def libConfig_SA = LibraryConfigurationSuperMaster.findAll()
            for (ut in libConfig_SA) {
                HashMap temp = new HashMap()
                temp.put("libConfig", ut.name)
                temp.put("libConfigId", ut.id)
                temp.put("libConfigValue", ut.value)
                libConfiglist_SA.add(temp)
            }
            hm.put('libConfiglist_SA', libConfiglist_SA)

            Login log = Login.findByUsername(uid)
            def libConfiglist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def libConfig = LibraryConfiguration.findAll()
                int srno = 1
                for (ut in libConfig) {
                    HashMap temp = new HashMap()
                    temp.put("libConfig", ut.name)
                    temp.put("libConfigId", ut.id)
                    temp.put("libConfigValue", ut.value)
                    temp.put("libConfigIsactive", ut.isactive)
                    temp.put("srno", srno++)
                    libConfiglist.add(temp)
                }
                hm.put('libConfig', libConfiglist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def addLibConfig(request, hm, ip) {
        println "i am in addLibConfig"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "Edit addLibConfig from Admin  Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def libConfiglist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
//                def org = user.organization
//                hm.put("org",org)

                LibraryConfiguration libConfig = LibraryConfiguration.findByName(dataresponse?.libConfig_name)
                if (libConfig != null) {
                    hm.put("msg", "Library Configuration already exist!!")
                } else {
                    libConfig = new LibraryConfiguration()
                    libConfig.isactive = true
                    libConfig.name = dataresponse?.libConfig_name
                    libConfig.value = dataresponse?.libConfigValue
                    libConfig.updation_ip_address = ip
                    libConfig.creation_ip_address = ip
                    libConfig.updation_date = new Date()
                    libConfig.creation_date = new Date()
                    libConfig.updation_username = uid
                    libConfig.creation_username = uid
                    libConfig.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def allP_List = LibraryConfiguration.findAll()
                int srno = 1
                for (config in allP_List) {
                    HashMap temp = new HashMap()
                    temp.put("libConfig", config.name)
                    temp.put("libConfigId", config.id)
                    temp.put("libConfigValue", config.value)
                    temp.put("libConfigIsactive", config.isactive)
                    temp.put("srno", srno++)
                    libConfiglist.add(temp)
                }
                hm.put("libConfig_list", libConfiglist)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def LibConfigIsactive(request, hm) {
        println "i am in LibConfigIsactive  "

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "Edit User Types from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                hm.put("org", org)

                def libConfig = LibraryConfiguration.findById(dataresponse?.libConfigId)
                if (libConfig != null) {
                    if (libConfig?.isactive == false) {
                        libConfig.isactive = true
                        libConfig.updation_ip_address = request.getRemoteAddr()
                        libConfig.updation_date = new Date()
                        libConfig.updation_username = uid
                        libConfig.save(flush: true, failOnError: true)
                    } else {
                        libConfig.isactive = false
                        libConfig.updation_ip_address = request.getRemoteAddr()
                        libConfig.updation_date = new Date()
                        libConfig.updation_username = uid
                        libConfig.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def editLibConfig(request, hm, ip) {
        println "i am in editLibConfig"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editLibConfig from Admin  Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                LibraryConfiguration libConfig = LibraryConfiguration.findById(dataresponse?.libConfigId)
                libConfig.name = dataresponse?.libConfigName
                libConfig.value = dataresponse?.libConfigValue
                libConfig.updation_ip_address = ip
                libConfig.updation_date = new Date()
                libConfig.updation_username = uid
                libConfig.save(flush: true, failOnError: true)
                hm.put("msg", "success")
                hm.put("status", "200")
                return hm
            }
        }
        return hm
    }

    def getOrgData(request, hm, ip) {
        println("in getTemplateData:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String purpose = "getTemplateData from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList org_list = new ArrayList()
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization.name
                int srno = 1
                for (item in Organization.findAll()) {
                    HashMap temp = new HashMap()
                    temp.put("srno", srno++)
                    temp.put("id", item.id)
                    temp.put("name", item.name)
                    temp.put("display_name", item.displayname)
                    temp.put("tenant_name", item.tenantname)
                    temp.put("address", item.address)
                    temp.put("cp_name", item?.contact_person_name)
                    temp.put("email", item?.email)
                    temp.put("contact", item?.contact)
                    temp.put("domain", item?.loginattachementdomain)
                    temp.put("website", item?.website)
                    temp.put("country", item?.country)
                    temp.put("isactive", item?.isactive)
                    org_list.add(temp)
                }
                hm.put("org_list", org_list)

            }//println"hm: "+hm
            return hm

        }
        return hm
    }

    def editOrg(request, hm, ip) {
        println("in editOrg:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:" + dataresponse

        AuthService auth = new AuthService()

        String purpose = "edit Org from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def org = Organization.findByName(dataresponse.name)
                if (org != null) {
                    org.tenantname = dataresponse.tenant_name
                    org.name = dataresponse.name
                    org.displayname = dataresponse.display_name
                    org.address = dataresponse.address
                    org.contact_person_name = dataresponse.cp_name
                    org.isactive = true
                    org.email = dataresponse.email
                    org.contact = dataresponse.contact
                    org.website = dataresponse.website
                    org.country = dataresponse.country
                    org.loginattachementdomain = dataresponse.domain
                    org.updation_username = uid
                    org.updation_date = new java.util.Date()
                    org.updation_ip_address = ip
                    org.save(failOnError: true, flush: true)

                    HashMap status = new HashMap()
                    status.put("code", "SUCCESS")
                    hm.put("status", status)
                }
                return hm
            }
        }
        return hm
    }

    def saveOrg(request, hm, ip) {
        println("in saveOrg:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")


        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:" + dataresponse

        AuthService auth = new AuthService()

        String purpose = "saveOrg from admin  menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        println("request")
        println(request)

        def tenant = TenantClient.findByName(dataresponse.tenant_name)
        println('tenant----'+tenant)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {

//                def folderPath
//                def fileName
//                def filePath
//                def fileExtention
////                for (Part filePart : request.getParts()) {
////                    InputStream fs = filePart.getInputStream()
//                    String filePartName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
//                    int indexOfDot = filePartName.lastIndexOf('.')
//                    if (indexOfDot > 0) {
//                        fileExtention = filePartName.substring(indexOfDot + 1)
//                    }
//                    String fileGR = filePartName.substring(0, filePartName.lastIndexOf("."));
//                    String filename = fileGR
//                    String username = filename
////                  folderPath = log.tenantclient.tenantid+"/Student_photo/"+user.organization.id+'/'
//                    folderPath = Library_Organization_logo + '/'
//                    fileName = filename + "." + fileExtention // question no
//                    println('filename----' + filename)
//
//                    filePath = folderPath + fileName
//                    println('filePath----' + filePath)
//                    println('folderPath----' + folderPath)

//                    AWSUploadPhotosService awsUploadDocumentsService = new AWSUploadPhotosService()
//                        boolean isUpload = awsUploadDocumentsService.uploadPhoto(filePart, folderPath, fileName, filePath)
//                }





                def org = Organization.findByName(dataresponse.name)
                if (org == null) {
                    org = new Organization()
                    if(tenant){
                        org.tenantid = tenant?.id
                    }

//                    if (isUpload) {
//                        org.profilephotofilename=fileName
//                        org.profilephotofilepath=folderPath
//                        println "Image uploaded....."
//                    } else {
//                        println 'Image not uploaded'
//                    }
                    org.tenantname = dataresponse.tenant_name
                    org.name = dataresponse.name
                    org.displayname = dataresponse.display_name
                    org.address = dataresponse.address
                    org.contact_person_name = dataresponse.cp_name
                    org.isactive = true
                    org.email = dataresponse.email
                    org.contact = dataresponse.contact
                    org.website = dataresponse.website
                    org.country = dataresponse.country
                    org.loginattachementdomain = dataresponse.domain
                    org.logofilepath = 'NA'
                    org.logofilename = 'NA'
                    org.creation_username = uid
                    org.updation_username = uid
                    org.creation_date = new java.util.Date()
                    org.updation_date = new java.util.Date()
                    org.creation_ip_address = ip
                    org.updation_ip_address = ip
                    org.save(failOnError: true, flush: true)

                    HashMap status = new HashMap()
                    status.put("code", "SUCCESS")
                    status.put("message", "Organization added successfully...")
                    hm.put("status", status)
                    return hm
                } else {
                    HashMap status = new HashMap()
                    status.put("code", "NA")
                    status.put("message", "Organization Already present!!!")
                    hm.put("status", status)
                }
            }
//            }
            return hm

        }
        return hm
    }

    def orgIsactive(request, hm) {
        println "i am in orgIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "orgIsactive from Admin  Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = Organization.findById(dataresponse?.id)
                if (org != null) {
                    if (org?.isactive == false) {
                        org.isactive = true
                        org.updation_ip_address = request.getRemoteAddr()
                        org.updation_date = new Date()
                        org.updation_username = uid
                        org.save(flush: true, failOnError: true)
                    } else {
                        org.isactive = false
                        org.updation_ip_address = request.getRemoteAddr()
                        org.updation_date = new Date()
                        org.updation_username = uid
                        org.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }


    def importOrg(request, hm, ip) {
        println(' in  importOrg :')
        def uid = request.getHeader("EPS-uid")
        Part filePart = request.getPart('file')
        InputStream fs = filePart.getInputStream()
        File file = File.createTempFile("s3test", "");
        OutputStream outputStream = new FileOutputStream(file)
        IOUtils.copy(fs, outputStream);
        def reader = new CSVReader(new FileReader(file))

        println('fs----'+ fs)

        String[] nextRecord;
        boolean header = true
        ArrayList failList = new ArrayList()
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            while ((nextRecord = reader.readNext()) != null) {
                def tenant = TenantClient.findByName(nextRecord[2].trim())
                String reason = 'Data is Missing '
                HashMap temp = new HashMap()
                if (header) //skip headers
                {
                    header = false
                    continue
                }
                Tenants.withId(log.tenantclient.tenantid) {
//                while ((nextRecord = reader.readNext()) != null) {
//                    String reason = 'Data is Missing '
//                    HashMap temp = new HashMap()
//                    if (header) //skip headers
//                    {
//                        header = false
//                        continue
//                    }
                    println('7--------'+nextRecord[7])
                    if (nextRecord[0] != '' && nextRecord[1] != '' && nextRecord[2] != ''  && nextRecord[4] != '' && nextRecord[7] != '') {
                        Organization org = Organization.findByName(nextRecord[0].trim())
                        println("org :" + org)
                        if (org == null) {
                            org = new Organization()

                            if(tenant){
                                org.tenantid = tenant?.id
                            }
                            org.tenantname = nextRecord[2].trim()
                            org.name = nextRecord[0].trim()
                            org.displayname = nextRecord[1].trim()
                            org.address = nextRecord[3].trim()
                            org.contact_person_name = nextRecord[4].trim()
                            org.isactive = true
                            org.email = nextRecord[5].trim()
                            org.contact = nextRecord[6].trim()
                            org.website = nextRecord[8].trim()
                            org.country = nextRecord[9].trim()
                            org.loginattachementdomain = nextRecord[7].trim()
                            org.logofilepath = 'NA'
                            org.logofilename = 'NA'
                            org.creation_username = uid
                            org.updation_username = uid
                            org.creation_date = new java.util.Date()
                            org.updation_date = new java.util.Date()
                            org.creation_ip_address = ip
                            org.updation_ip_address = ip
                            org.save(flush: true, failOnError: true)
                            hm.put("status", "200")
                        } else {
                            reason = "organization already present"
                            temp.put('reason', reason)
                            temp.put('name', nextRecord[0].trim())
                            temp.put('displayname', nextRecord[1].trim())
                            temp.put('tenantname', nextRecord[2].trim())
                            temp.put('domain', nextRecord[7].trim())
                            temp.put('cp_name', nextRecord[4].trim())
                            failList.add(temp)
                        }
                    }else {
                        if(nextRecord[0]){
                            temp.put('name', nextRecord[0].trim())
                        }else {
                            temp.put('name','Missing')
                        }

                        if(nextRecord[1]){
                            temp.put('displayname', nextRecord[1].trim())
                        }else {
                            temp.put('displayname','Missing')
                        }

                        if(nextRecord[2]){
                            temp.put('tenantname', nextRecord[2].trim())
                        }else {
                            temp.put('tenantname','Missing')
                        }

                        if(nextRecord[4]){
                            temp.put('cp_name', nextRecord[4].trim())
                        }else {
                            temp.put('cp_name','Missing')
                        }

                        if(nextRecord[7]){
                            temp.put('domain', nextRecord[7].trim())
                        }else {
                            temp.put('domain','Missing')
                        }

                        temp.put('reason', reason)
                        failList.add(temp)
                    }
                }
            }
        }
        outputStream.flush()
        outputStream.close()
        fs.close()
        file.delete()
        hm.put('failList',failList)
        return hm
    }

    def getLibraryData(request, hm, ip) {
        println("in getLibraryData:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String purpose = "getLibraryData from admin  menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList lib_list = new ArrayList()
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization.name
                int srno = 1
                for (item in Library.findAll()) {
                    HashMap temp = new HashMap()
                    temp.put("srno", srno++)
                    temp.put("id", item.id)
                    temp.put("name", item.name)
                    temp.put("city", item.city)
                    temp.put("state", item.state)
                    temp.put("pin", item.pin)
                    temp.put("address", item.address)
                    temp.put("contact", item?.contact)
                    temp.put("country", item?.country)
                    temp.put("isactive", item?.isactive)
                    lib_list.add(temp)
                }
                hm.put("lib_list", lib_list)

            }//println"hm: "+hm
            return hm

        }
        return hm
    }

    def editLibrary(request, hm, ip) {
        println("in editLibrary:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:--" + dataresponse

        AuthService auth = new AuthService()

        String purpose = "edit Library from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def lib = Library.findByName(dataresponse.name)
                if (lib != null) {
                    lib.city = dataresponse.city
                    lib.state = dataresponse.state
                    lib.pin = dataresponse.pin
                    lib.name = dataresponse.name
                    lib.address = dataresponse.address
                    lib.contact = dataresponse.contact
                    lib.country = dataresponse.country
                    lib.updation_username = uid
                    lib.updation_date = new java.util.Date()
                    lib.updation_ip_address = ip
                    lib.save(failOnError: true, flush: true)

                    HashMap status = new HashMap()
                    status.put("code", "SUCCESS")
                    hm.put("status", status)
                }
                return hm
            }
        }
        return hm
    }

    def saveLibrary(request, hm, ip) {
        println("in saveLibrary:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse:" + dataresponse

        AuthService auth = new AuthService()

        String purpose = "saveLibrary from admin  menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        println("dataresponse")
        println(dataresponse)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def lib = Library.findByName(dataresponse.name)
                if (lib == null) {
                    lib = new Library()

                    lib.city = dataresponse.city
                    lib.state = dataresponse.state
                    lib.pin = dataresponse.pin
                    lib.name = dataresponse.name
                    lib.address = dataresponse.address
                    lib.isactive = true
                    lib.contact = dataresponse.contact
                    lib.country = dataresponse.country
                    lib.creation_username = uid
                    lib.updation_username = uid
                    lib.creation_date = new java.util.Date()
                    lib.updation_date = new java.util.Date()
                    lib.creation_ip_address = ip
                    lib.updation_ip_address = ip
                    lib.save(failOnError: true, flush: true)

                    HashMap status = new HashMap()
                    status.put("code", "SUCCESS")
                    status.put("message", "Library added successfully...")
                    hm.put("status", status)
                    return hm
                } else {
                    HashMap status = new HashMap()
                    status.put("code", "NA")
                    status.put("message", "Library Already present!!!")
                    hm.put("status", status)
                }
            }
//            }
            return hm

        }
        return hm
    }

    def libraryIsactive(request, hm) {
        println "i am in orgIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "orgIsactive from Admin  Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def lib = Library.findById(dataresponse?.id)
                if (lib != null) {
                    if (lib?.isactive == false) {
                        lib.isactive = true
                        lib.updation_ip_address = request.getRemoteAddr()
                        lib.updation_date = new Date()
                        lib.updation_username = uid
                        lib.save(flush: true, failOnError: true)
                    } else {
                        lib.isactive = false
                        lib.updation_ip_address = request.getRemoteAddr()
                        lib.updation_date = new Date()
                        lib.updation_username = uid
                        lib.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }


    def importLibrary(request, hm, ip) {
        println(' in  importLibrary :')
        def uid = request.getHeader("EPS-uid")
        Part filePart = request.getPart('file')
        InputStream fs = filePart.getInputStream()
        File file = File.createTempFile("s3test", "");
        OutputStream outputStream = new FileOutputStream(file)
        IOUtils.copy(fs, outputStream);
        def reader = new CSVReader(new FileReader(file))

        println('fs----'+ fs)

        String[] nextRecord;
        boolean header = true
        ArrayList failList = new ArrayList()
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                while ((nextRecord = reader.readNext()) != null) {
                    String reason = 'Data is Missing'
                    HashMap temp = new HashMap()
                    if (header) //skip headers
                    {
                        header = false
                        continue
                    }
                    if (nextRecord[0] != '' && nextRecord[1] != '' && nextRecord[2] != '' && nextRecord[3] != '' && nextRecord[4] != '' && nextRecord[5] != '' && nextRecord[6] != '') {
                        Library lib = Library.findByName(nextRecord[0].trim())
                        println("lib :" + lib)
                        if (lib == null) {
                            lib = new Library()
                            lib.name = nextRecord[0].trim()
                            lib.address = nextRecord[1].trim()
                            lib.city = nextRecord[2].trim()
                            lib.state = nextRecord[3].trim()
                            lib.country = nextRecord[4].trim()
                            lib.pin = nextRecord[5].trim()
                            lib.contact = nextRecord[6].trim()
                            lib.isactive = true
                            lib.creation_username = uid
                            lib.updation_username = uid
                            lib.creation_date = new java.util.Date()
                            lib.updation_date = new java.util.Date()
                            lib.creation_ip_address = ip
                            lib.updation_ip_address = ip
                            lib.save(flush: true, failOnError: true)
                            hm.put("status", "200")
                        } else {
                            temp.put('name', nextRecord[0].trim())
                            temp.put('address', nextRecord[1].trim())
                            temp.put('city', nextRecord[2].trim())
                            temp.put('state', nextRecord[3].trim())
                            temp.put('country', nextRecord[4].trim())
                            temp.put('pin', nextRecord[5].trim())
                            temp.put('contact', nextRecord[6].trim())
                            temp.put('reason', 'Library already exist!!')

                            failList.add(temp)
                        }
                    } else {
                        if(nextRecord[0]){
                            temp.put('name', nextRecord[0].trim())
                        }else {
                            temp.put('name','Missing')
                        }

                        if(nextRecord[1]){
                            temp.put('address', nextRecord[1].trim())
                        }else {
                            temp.put('address','Missing')
                        }

                        if(nextRecord[2]){
                            temp.put('city', nextRecord[2].trim())
                        }else {
                            temp.put('city','Missing')
                        }

                        if(nextRecord[3]){
                            temp.put('state', nextRecord[3].trim())
                        }else {
                            temp.put('state','Missing')
                        }

                        if(nextRecord[4]){
                            temp.put('country', nextRecord[4].trim())
                        }else {
                            temp.put('country','Missing')
                        }

                        if(nextRecord[5]){
                            temp.put('pin', nextRecord[5].trim())
                        }else {
                            temp.put('pin','Missing')
                        }

                        if(nextRecord[6]){
                            temp.put('contact', nextRecord[6].trim())
                        }else {
                            temp.put('contact','Missing')
                        }
                        temp.put('reason', reason)
                        failList.add(temp)
                    }
                }
            }
        }
        outputStream.flush()
        outputStream.close()
        fs.close()
        file.delete()
        hm.put('failList',failList)
        return hm
    }

    def roleLinksIsactive(request, hm) {
        println "i am in roleLinksIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "roleLinks Isactive from Admin  Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def utlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def roleLink = RoleLinks.findById(dataresponse?.roleLinkId)
                if (roleLink != null) {
                    if (roleLink?.isactive == false) {
                        roleLink.isactive = true
                        roleLink.updation_ip_address = request.getRemoteAddr()
                        roleLink.updation_date = new Date()
                        roleLink.updation_username = uid
                        roleLink.save(flush: true, failOnError: true)
                    } else {
                        roleLink.isactive = false
                        roleLink.updation_ip_address = request.getRemoteAddr()
                        roleLink.updation_date = new Date()
                        roleLink.updation_username = uid
                        roleLink.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }


    def addProgramType(request, hm, ip) {
        println "i am in addProgramType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addProgramType from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def programtype_list = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization
                ProgramType pt = ProgramType.findByName(dataresponse?.currency_name)
                if (pt != null) {
                    hm.put("msg", "Account Status already exist!!")
                } else {
                    pt = new ProgramType()
                    pt.name = dataresponse?.currency_name
                    pt.organization = user?.organization
                    pt.updation_ip_address = ip
                    pt.creation_ip_address = ip
                    pt.updation_date = new Date()
                    pt.creation_date = new Date()
                    pt.updation_username = uid
                    pt.creation_username = uid
                    pt.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def allP_List = ProgramType.findAllByOrganization(organization)
                int srno = 1
                for (pub in allP_List) {
                    HashMap temp = new HashMap()
                    temp.put("programtype", pub.name)
                    temp.put("programtypeId", pub.id)
                    temp.put("srno", srno++)
                    programtype_list.add(temp)
                }
                hm.put("programtype_list", programtype_list)
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }
    def saveacademicyear(request, hm, ip) {
        println "i am in saveacademicyear"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "saveacademicyear from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def programtype_list = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization
                AcademicYear pt = AcademicYear.findByAy(dataresponse?.ay)
                AcademicYear next
                AcademicYear prev
                if(dataresponse?.next!="")
                 next = AcademicYear.findById(dataresponse?.next)
                if(dataresponse?.prev!="")
                 prev = AcademicYear.findById(dataresponse?.prev)
                if (pt != null) {
                    hm.put("status", "Ay already exist!!")
                } else {
                    pt = new AcademicYear()
                    pt.ay = dataresponse?.ay
                    pt.nextyear = next
                    pt.previousyear = prev
                    pt.display_name = dataresponse?.ay

                    pt.updation_ip_address = ip
                    pt.creation_ip_address = ip
                    pt.updation_date = new Date()
                    pt.creation_date = new Date()
                    pt.updation_username = uid
                    pt.creation_username = uid
                    pt.save(flush: true, failOnError: true)
                    hm.put("status", "success")
                }



                hm.put("msg", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }
    def academicyearIsactive(request, hm, ip){
        println("Isactive academicyear...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        String purpose = "Isactive academicyear."
        println("Isactive academicyear dataresponse"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
//            println("dataresponse",+dataresponse.name)
            Tenants.withId(log.tenantclient.tenantid) {
//                println("dataresponse")
                def user = User.findByUsername(log.username)
                def organization = user.organization

                AcademicYear pt = AcademicYear.findById(dataresponse.id)
                if(pt!=null){
                if (dataresponse.Isactive==false)
                {
                    pt.isactive=false
                    pt.updation_ip_address = ip
                    pt.updation_date = new Date()
                    pt.updation_username = uid
                    pt.save(flush: true, failOnError: true)
                    hm.put("msg", "200")
                } else if(dataresponse.Isactive==true){
                    pt.isactive = true
                    pt.updation_ip_address = ip
                    pt.updation_date = new Date()
                    pt.updation_username = uid
                    pt.save(flush: true, failOnError: true)
                    hm.put("msg", "200")
                }
                    else{
                    hm.put("msg","error")
                }
                }
                println "hm: " + hm
                return hm
            }
        }
}
    def academicyearIscurrent(request, hm, ip){
        println("Isactive Frequency...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        String purpose = "Isactive Frequency."
        println("Isactive Frequency dataresponse"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
//            println("dataresponse",+dataresponse.name)
            Tenants.withId(log.tenantclient.tenantid) {
//                println("dataresponse")
                def user = User.findByUsername(log.username)
                def organization = user.organization

                AcademicYear pt = AcademicYear.findById(dataresponse.id)
                if(pt!=null){
                    if (dataresponse.Iscurrent==false)
                    {
                        pt.iscurrent=false
                        pt.updation_ip_address = ip
                        pt.updation_date = new Date()
                        pt.updation_username = uid
                        pt.save(flush: true, failOnError: true)
                        hm.put("msg", "200")
                    } else {
                        pt.iscurrent = true
                        pt.updation_ip_address = ip
                        pt.updation_date = new Date()
                        pt.updation_username = uid
                        pt.save(flush: true, failOnError: true)
                        hm.put("msg", "200")
                    }
                }
                println "hm: " + hm
                return hm
            }
        }
    }

    def saveyear(request, hm, ip) {
        println "i am in saveyear"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "saveyear from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def programtype_list = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization
                Year pt = Year.findByYearAndOrganization(dataresponse?.year,organization)


                if (pt != null) {
                    hm.put("status", "Year already exist!!")
                } else {
                    pt = new Year()
                    pt.year = dataresponse?.year
                    pt.display_name = dataresponse?.display_name
                    pt.organization = organization
                    pt.updation_ip_address = ip
                    pt.creation_ip_address = ip
                    pt.updation_date = new Date()
                    pt.creation_date = new Date()
                    pt.updation_username = uid
                    pt.creation_username = uid
                    pt.save(flush: true, failOnError: true)
                    hm.put("status", "success")
                }



                hm.put("msg", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def savesemester(request, hm, ip) {
        println "i am in savesemester"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "savesemester from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def programtype_list = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization
                Semester pt = Semester.findBySemAndOrganization(dataresponse?.year,organization)


                if (pt != null) {
                    hm.put("status", "Semester already exist!!")
                } else {
                    pt = new Semester()
                    pt.sem = dataresponse?.year
                    pt.display_name = dataresponse?.display_name
                    pt.organization = organization
                    pt.updation_ip_address = ip
                    pt.creation_ip_address = ip
                    pt.updation_date = new Date()
                    pt.creation_date = new Date()
                    pt.updation_username = uid
                    pt.creation_username = uid
                    pt.save(flush: true, failOnError: true)
                    hm.put("status", "success")
                }



                hm.put("msg", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def saveshift(request, hm, ip) {
        println "i am in saveshift"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "savesemester from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def programtype_list = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization
                Shift pt = Shift.findByShiftAndOrganization(dataresponse?.year,organization)


                if (pt != null) {
                    hm.put("status", "Shift already exist!!")
                } else {
                    pt = new Shift()
                    pt.shift = dataresponse?.year
                    pt.display_name = dataresponse?.display_name
                    pt.organization = organization
                    pt.updation_ip_address = ip
                    pt.creation_ip_address = ip
                    pt.updation_date = new Date()
                    pt.creation_date = new Date()
                    pt.updation_username = uid
                    pt.creation_username = uid
                    pt.save(flush: true, failOnError: true)
                    hm.put("status", "success")
                }



                hm.put("msg", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def editacademicyear(request, hm, ip) {
        println "i am in editacademicyear"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "edit academic year from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def programtype_list = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization
                AcademicYear pt = AcademicYear.findById(dataresponse?.ayid)
                AcademicYear next
                AcademicYear prev
                if(dataresponse?.next!="")
                    next = AcademicYear.findById(dataresponse?.next)
                if(dataresponse?.prev!="")
                    prev = AcademicYear.findById(dataresponse?.prev)
                if (pt != null) {


                    pt.ay = dataresponse?.ay
                    pt.nextyear = next
                    pt.previousyear = prev
                    pt.display_name = dataresponse?.ay

                    pt.updation_ip_address = ip
                    pt.creation_ip_address = ip
                    pt.updation_date = new Date()
                    pt.creation_date = new Date()
                    pt.updation_username = uid
                    pt.creation_username = uid
                    pt.save(flush: true, failOnError: true)
                    hm.put("status", "success")
                } else {
                    hm.put("status", "Ay not found")
                }



                hm.put("msg", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def edityear(request, hm, ip) {
        println "i am in edityear"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "edit year from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def programtype_list = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization
                Year pt = Year.findById(dataresponse?.yearid)

                if (pt != null) {


                    pt.year = dataresponse?.year
                    pt.display_name = dataresponse?.display_name
                    pt.organization = organization


                    pt.updation_ip_address = ip
                    pt.creation_ip_address = ip
                    pt.updation_date = new Date()
                    pt.creation_date = new Date()
                    pt.updation_username = uid
                    pt.creation_username = uid
                    pt.save(flush: true, failOnError: true)
                    hm.put("status", "success")
                } else {
                    hm.put("status", "Year not found")
                }



                hm.put("msg", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }
    def editsemester(request, hm, ip) {
        println "i am in editsemester"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "edit Semester from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def programtype_list = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization
                Semester pt = Semester.findById(dataresponse?.yearid)

                if (pt != null) {


                    pt.sem = dataresponse?.year
                    pt.display_name = dataresponse?.display_name
                    pt.organization = organization


                    pt.updation_ip_address = ip
                    pt.creation_ip_address = ip
                    pt.updation_date = new Date()
                    pt.creation_date = new Date()
                    pt.updation_username = uid
                    pt.creation_username = uid
                    pt.save(flush: true, failOnError: true)
                    hm.put("status", "success")
                } else {
                    hm.put("status", "Ay not found")
                }



                hm.put("msg", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }
    def editshift(request, hm, ip) {
        println "i am in editshift"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "edit editshift from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def programtype_list = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization
                Shift pt = Shift.findById(dataresponse?.yearid)

                if (pt != null) {


                    pt.shift = dataresponse?.year
                    pt.display_name = dataresponse?.display_name
                    pt.organization = organization


                    pt.updation_ip_address = ip
                    pt.creation_ip_address = ip
                    pt.updation_date = new Date()
                    pt.creation_date = new Date()
                    pt.updation_username = uid
                    pt.creation_username = uid
                    pt.save(flush: true, failOnError: true)
                    hm.put("status", "success")
                } else {
                    hm.put("status", "Shift not found")
                }



                hm.put("msg", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def getProgramType(request, hm) {
        println("Inside getProgramType")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        println("uid : "+uid)

        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def  programtypelist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization
                def programtype = ProgramType.findAllByOrganization(organization)
                int srno = 1
                for (ut in programtype) {
                    HashMap temp = new HashMap()
                    temp.put("programtype", ut.name)
                    temp.put("programtypeId", ut.id)
                    temp.put("srno", srno++)
                    programtypelist.add(temp)
                }
                hm.put('programtype', programtypelist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }
    def getacademicyearlist(request, hm) {
        println("Inside getacademicyearlist")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        println("uid : "+uid)

        String url = request.getHeader("router-path")


        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def  aylist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization
                def aylist1 = AcademicYear.findAll()
                int srno = 1
                for (ut in aylist1) {
                    HashMap temp = new HashMap()
                    temp.put("ay", ut.ay)
                    temp.put("id", ut.id)
                    temp.put("isactive", ut.isactive)
                    temp.put("iscurrent", ut.iscurrent)
                    aylist.add(temp)
                }
                hm.put('aylist', aylist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }
    def getprogramlist(request, hm) {
        println("Inside getacademicyearlist")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        println("uid : "+uid)

        String url = request.getHeader("router-path")


        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def  aylist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization
                def aylist1 = Program.findAllByOrganization(organization)
                int srno = 1
                for (ut in aylist1) {
                    HashMap temp = new HashMap()
                    temp.put("name", ut.name)
                    temp.put("id", ut.id)
                    aylist.add(temp)
                }
                hm.put('programlist', aylist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }
    def getyearlist(request, hm) {
        println("Inside getyearlist")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        println("uid : "+uid)

        String url = request.getHeader("router-path")


        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def  aylist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization
                def aylist1 = Year.findAllByOrganization(organization)
                int srno = 1
                for (ut in aylist1) {
                    HashMap temp = new HashMap()
                    temp.put("name", ut.year)
                    temp.put("id", ut.id)
                    aylist.add(temp)
                }
                hm.put('yearlist', aylist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }
    def getsemesterlist(request, hm) {
        println("Inside getacademicyearlist")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        println("uid : "+uid)

        String url = request.getHeader("router-path")


        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def  aylist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization
                def aylist1 = Semester.findAllByOrganization(organization)
                int srno = 1
                for (ut in aylist1) {
                    HashMap temp = new HashMap()
                    temp.put("name", ut.sem)
                    temp.put("id", ut.id)
                    aylist.add(temp)
                }
                hm.put('semesterlist', aylist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }
    def getalllearnerdivision(request, hm) {
        println("Inside getacademicyearlist")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        println("uid : "+uid)

        String url = request.getHeader("router-path")


        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def  aylist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization
                def aylist1 = LearnerDivision.findAllByOrganization(organization)
                int srno = 1
                for (ut in aylist1) {
                    HashMap temp = new HashMap()
                    temp.put("srno", srno++)
                    temp.put("prn", ut?.member?.registration_number)
                    temp.put("name", ut?.member?.name)
                    temp.put("ay", ut?.academic?.ay)
                    temp.put("division", ut?.division)
                    temp.put("rollno", ut?.rollno)
                    temp.put("program", ut?.program?.name)
                    temp.put("Semester", ut?.semester?.sem)
                    temp.put("year", ut?.year?.year)

                    temp.put("id", ut.id)
                    aylist.add(temp)
                }
                hm.put('learnerdivisiondata', aylist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def getacademicyeardata(request, hm) {
        println("Inside getProgramType")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        println("uid : "+uid)

        String url = request.getHeader("router-path")


        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def  aylist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization
                def aylist1 = AcademicYear.findAll()
                int srno = 1
                for (ut in aylist1) {
                    HashMap temp = new HashMap()
                    temp.put("ay", ut.ay)
                    temp.put("id", ut.id)
                    temp.put("isactive", ut.isactive)
                    temp.put("iscurrent", ut.iscurrent)
                    temp.put("next", ut?.nextyear?.ay)
                    temp.put("nextid", ut?.nextyear?.id)
                    temp.put("prev", ut?.previousyear?.ay)
                    temp.put("previd", ut?.previousyear?.id)
                    temp.put("srno", srno++)
                    aylist.add(temp)
                }
                hm.put('aydata', aylist)

                hm.put("msg", "200")
                return hm
            }
        }
    }

    def getyeardata(request, hm) {
        println("Inside getProgramType")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        println("uid : "+uid)

        String url = request.getHeader("router-path")


        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def  aylist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization
                def yearlist = Year.findAllByOrganization(organization)
                int srno = 1
                for (ut in yearlist) {
                    HashMap temp = new HashMap()
                    temp.put("year", ut.year)
                    temp.put("id", ut.id)
                    temp.put("display_name", ut.display_name)

                    temp.put("srno", srno++)
                    aylist.add(temp)
                }
                hm.put('yeardata', aylist)

                hm.put("msg", "200")
                return hm
            }
        }
    }
    def getsemesterdata(request, hm) {
        println("Inside getsemesterdata")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        println("uid : "+uid)

        String url = request.getHeader("router-path")


        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def  aylist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization
                def yearlist = Semester.findAllByOrganization(organization)
                int srno = 1
                for (ut in yearlist) {
                    HashMap temp = new HashMap()
                    temp.put("year", ut.sem)
                    temp.put("id", ut.id)
                    temp.put("display_name", ut.display_name)

                    temp.put("srno", srno++)
                    aylist.add(temp)
                }
                hm.put('yeardata', aylist)

                hm.put("msg", "200")
                return hm
            }
        }
    }
    def getshiftdata(request, hm) {
        println("Inside getshiftdata")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        println("uid : "+uid)

        String url = request.getHeader("router-path")


        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def  aylist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization
                def yearlist = Shift.findAllByOrganization(organization)
                int srno = 1
                for (ut in yearlist) {
                    HashMap temp = new HashMap()
                    temp.put("year", ut.shift)
                    temp.put("id", ut.id)
                    temp.put("display_name", ut.display_name)

                    temp.put("srno", srno++)
                    aylist.add(temp)
                }
                hm.put('yeardata', aylist)

                hm.put("msg", "200")
                return hm
            }
        }
    }
    def deleteAcademicyear(request, hm){
        println "i am in deleteAcademicyear"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteAcademicyear from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                AcademicYear ay = AcademicYear.findById(dataresponse?.ayid)

                if(ay!=null) {
                    ay.previousyear=null
                    ay.nextyear=null
                    ay.save(failOnError: true,flush: true)
                    AcademicYear ay1 = AcademicYear.findById(dataresponse?.ayid)
                    ay1.delete(failOnError: true,flush: true)
                    hm.put("msg", "200")
                    hm.put("status", "success")
                    return hm
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return
            }
        }
    }
    def deleteyear(request, hm){
        println "i am in deleteyear"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "delete year from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                Year yr = Year.findById(dataresponse?.yearid)

                if(yr!=null) {


                    yr.delete(failOnError: true,flush: true)
                    hm.put("msg", "200")
                    hm.put("status", "success")
                    return hm
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return
            }
        }
    }
    def deletesemester(request, hm){
        println "i am in deletesemester"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "delete year from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                Semester yr = Semester.findById(dataresponse?.yearid)

                if(yr!=null) {


                    yr.delete(failOnError: true,flush: true)
                    hm.put("msg", "200")
                    hm.put("status", "success")
                    return hm
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return
            }
        }
    }

    def deleteshift(request, hm){
        println "i am in deleteshift"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "delete shift from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                Shift yr = Shift.findById(dataresponse?.yearid)

                if(yr!=null) {


                    yr.delete(failOnError: true,flush: true)
                    hm.put("msg", "200")
                    hm.put("status", "success")
                    return hm
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return
            }
        }
    }

    def editProgramtype(request, hm, ip) {
        println "i am in editProgramtype"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editProgramtype from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ProgramType pt = ProgramType.findById(dataresponse?.programtypeId)

                def exist = ProgramType.findByName(dataresponse?.programtypeName)
                if (exist != null) {
                    hm.put("msg", "Program type already exist!!")
                } else {
                    pt.name = dataresponse?.programtypeName
                    pt.updation_ip_address = ip
                    pt.updation_date = new Date()
                    pt.updation_username = uid
                    pt.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm
                }
            }
        }
        return hm
    }

    def deleteProgramType(request, hm){
        println "i am in deleteProgramType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteProgramType from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ProgramType programType = ProgramType.findByName(dataresponse?.programtype)
                println("programType : " + programType)
                if(programType!=null) {
                    programType.delete(flush: true)
                    hm.put("msg", "200")
                    hm.put("status", "success")
                    return hm
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return
            }
        }
    }

    def addProgram(request, hm, ip) {
        println "i am in addProgram"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addProgram from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def program_list = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization
                Program pt = Program.findByName(dataresponse?.program_name)
                if (pt != null) {
                    hm.put("msg", "Program already exist!!")
                } else {
                    pt = new Program()
                    pt.name = dataresponse?.program_name
                    pt.short_name = dataresponse?.short_name
                    pt.display_order = Integer.parseInt(dataresponse?.display_order)
                    pt.organization = user?.organization
                    ProgramType prog_type = ProgramType.findById(dataresponse?.program_type)
                    pt.programType = prog_type
                    pt.updation_ip_address = ip
                    pt.creation_ip_address = ip
                    pt.updation_date = new Date()
                    pt.creation_date = new Date()
                    pt.updation_username = uid
                    pt.creation_username = uid
                    pt.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }
                hm.put("status", "200")
                return hm
            }
        }
        return hm
    }

    def getProgram(request, hm) {
        println("Inside getProgram")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def  programlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization
                def program = Program.findAllByOrganization(organization)
                int srno = 1
                for (ut in program) {
                    HashMap temp = new HashMap()
                    temp.put("program", ut.name)
                    temp.put("programId", ut.id)
                    temp.put("short_name", ut.short_name)
                    temp.put("display_order", ut.display_order)
                    temp.put("program_type", ut?.programType?.name)
                    temp.put("srno", srno++)
                    programlist.add(temp)
                }
                hm.put('program', programlist)

                ArrayList pt = new ArrayList()
                def ptype = ProgramType.findAll()
                for (item in ptype) {
                    HashMap temp = new HashMap()
                    temp.put("name", item.name)
                    temp.put("id", item.id)
                    pt.add(temp)
                }
                hm.put('programtypelist', pt)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def editProgram(request, hm, ip) {
        println "i am in editProgram"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editProgram from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                Program pt = Program.findById(dataresponse?.programId)
                ProgramType programType = ProgramType.findById(dataresponse?.program_type)
                Program exist = Program.findByNameAndProgramTypeAndShort_name(dataresponse?.programName,programType,dataresponse.short_name)
                if (exist != null) {
                    hm.put("msg", "Program  already exist!!")
                } else {
                    pt.name = dataresponse?.programName
                    pt.display_order = dataresponse?.display_order
                    pt.short_name = dataresponse?.short_name
                    pt.programType = programType
                    pt.updation_ip_address = ip
                    pt.updation_date = new Date()
                    pt.updation_username = uid
                    pt.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm
                }
            }
        }
        return hm
    }

    def deleteprogram(request ,hm,ip) {
        println "i am in deleteprogram"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteprogram from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                Program pt = Program.findByName(dataresponse?.program)
                println("pt : " + pt)
                //def exist = Program.findByName(dataresponse?.program)
                if(pt!=null) {
                    pt.delete(flush: true)
                    hm.put("msg", "200")
                    hm.put("status", "success")
                    return hm
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return

            }

        }
    }

    def addBookSeries(request, hm, ip) {
        println "i am in addBookSeries"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addBookSeries from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def langList = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
//                def org = user.organization
//                hm.put("org",org)

                BookSeries bs = BookSeries.findByName(dataresponse?.series_name)
                if (bs != null) {
                    hm.put("msg", "Language already exist!!")
                } else {
                    bs = new BookSeries()
                    bs.isactive = true
                    bs.name = dataresponse?.series_name
                    if (dataresponse?.displayorder)
                        bs.display_order = Integer.parseInt(dataresponse?.displayorder)
                    if (dataresponse?.shortname)
                        bs.short_name = dataresponse?.shortname
                    bs.updation_ip_address = ip
                    bs.creation_ip_address = ip
                    bs.updation_date = new Date()
                    bs.creation_date = new Date()
                    bs.updation_username = uid
                    bs.creation_username = uid
                    bs.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def series = BookSeries.findAll()
                int srno = 1
                for (lang in series) {
                    HashMap temp = new HashMap()
                    temp.put("series", lang.name)
                    temp.put("seriesId", lang.id)
                    temp.put("seriesIsactive", lang.isactive)
                    temp.put("srno", srno++)
                    langList.add(temp)
                }
                hm.put('series', langList)
                println "hm: " + hm
                hm.put("status", "200")
                return hm
            }
        }
        return hm
    }

    def bookSeriesIsactive(request, hm) {
        println "i am in bookSeriesIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "languageIsactive"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                hm.put("org", org)

                def bs = BookSeries.findById(dataresponse?.seriesId)
                if (bs != null) {
                    if (bs?.isactive == false) {
                        bs.isactive = true
                        bs.updation_ip_address = request.getRemoteAddr()
                        bs.updation_date = new Date()
                        bs.updation_username = uid
                        bs.save(flush: true, failOnError: true)
                    } else {
                        bs.isactive = false
                        bs.updation_ip_address = request.getRemoteAddr()
                        bs.updation_date = new Date()
                        bs.updation_username = uid
                        bs.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def editBookSeries(request, hm, ip) {
        println "i am in editBookSeries"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "Edit editBookSeries from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                BookSeries bs = BookSeries.findById(dataresponse?.seriesId)

                //def exist = BookSeries.findByName(dataresponse?.bookSeriesName)
                if (bs != null) {
                    if (dataresponse?.bookSeriesName)
                        bs.name = dataresponse?.bookSeriesName
                    if (dataresponse?.displayorder)
                        bs.display_order = Integer.parseInt(dataresponse?.displayorder)
                    if (dataresponse?.shortname)
                        bs.short_name = dataresponse?.shortname
                    bs.updation_ip_address = ip
                    bs.updation_date = new Date()
                    bs.updation_username = uid
                    bs.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm


                } else {
                    hm.put("msg", "Book Series does not exist!!")
                }
            }
        }
        return hm
    }

    def getPhysiacalDetails(request, hm) {
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def physicaldetailslist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def physicaldetails = PhysicalDetails.findAll()
                int srno = 1
                for (phy in physicaldetails) {
                    HashMap temp = new HashMap()
                    temp.put("phyname", phy.name)
                    temp.put("pysicaldetailsId", phy.id)
                    temp.put("physicaldetailsIsactive", phy.isactive)
                    temp.put("srno", srno++)
                    physicaldetailslist.add(temp)
                }
                hm.put('physicaldetails', physicaldetailslist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def addPhysiacalDetails(request, hm, ip) {
        println "i am in addPhysiacalDetails"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse :"+dataresponse)

        String purpose = "addLanguage from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def physicaldetailslist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)


                PhysicalDetails physicalDetails = PhysicalDetails.findByName(dataresponse?.phyicaldetails_name)
                println('physicalDetails---' + physicalDetails)
                if (physicalDetails != null) {
                    hm.put("msg", "Physical Details already exist!!")
                } else {
                    physicalDetails = new PhysicalDetails()
                    physicalDetails.isactive = true
                    physicalDetails.name = dataresponse?.phyicaldetails_name
                    physicalDetails.updation_ip_address = ip
                    physicalDetails.creation_ip_address = ip
                    physicalDetails.updation_date = new Date()
                    physicalDetails.creation_date = new Date()
                    physicalDetails.updation_username = uid
                    physicalDetails.creation_username = uid
                    physicalDetails.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def physicaldetails = PhysicalDetails.findAll()
                int srno = 1
                for (phy in physicaldetails) {
                    HashMap temp = new HashMap()
                    temp.put("phyname", phy.name)
                    temp.put("pysicaldetailsId", phy.id)
                    temp.put("physicaldetailsIsactive", phy.isactive)
                    temp.put("srno", srno++)
                    physicaldetailslist.add(temp)
                }
                hm.put('physicaldetails', physicaldetailslist)
                println "hm: " + hm
                hm.put("status", "200")
                return hm
            }
        }
        return hm
    }

    def physicaldetailsIsactive(request, hm) {
        println "i am in physicaldetailsIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "physiacalDetailsIsactive"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                hm.put("org", org)

                def physicaldetails = PhysicalDetails.findById(dataresponse?.pysicaldetailsId)
                if (physicaldetails != null) {
                    if (physicaldetails?.isactive == false) {
                        physicaldetails.isactive = true
                        physicaldetails.updation_ip_address = request.getRemoteAddr()
                        physicaldetails.updation_date = new Date()
                        physicaldetails.updation_username = uid
                        physicaldetails.save(flush: true, failOnError: true)
                    } else {
                        physicaldetails.isactive = false
                        physicaldetails.updation_ip_address = request.getRemoteAddr()
                        physicaldetails.updation_date = new Date()
                        physicaldetails.updation_username = uid
                        physicaldetails.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def editPhysiacalDetails(request, hm, ip) {
        println "i am in editPhysiacalDetails"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "Edit editLanguage from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                PhysicalDetails physicaldetails = PhysicalDetails.findById(dataresponse?.pysicaldetailsId)

                def exist = PhysicalDetails.findByName(dataresponse?.phyname)
                if (exist != null) {
                    hm.put("msg", "Physical Details already exist!!")
                } else {
                    if (dataresponse?.phyname)
                        physicaldetails.name = dataresponse?.phyname
                    physicaldetails.updation_ip_address = ip
                    physicaldetails.updation_date = new Date()
                    physicaldetails.updation_username = uid
                    physicaldetails.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm
                }
            }
        }
        return hm
    }

    def deletePhysicalDetails(request ,hm){
        println "i am in deletePhysicalDetails"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deletePhysicalDetails from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                PhysicalDetails physicalDetails = PhysicalDetails.findById(dataresponse?.pysicaldetailsId)
                println("physicalDetails : " + physicalDetails)
                if(physicalDetails!=null) {
                    try{
                        physicalDetails.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }

                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return

            }

        }
    }

    def getAccompanyingMaterials(request, hm) {
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def physicaldetailslist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def physicaldetails = AccompanyingMaterials.findAll()
                int srno = 1
                for (phy in physicaldetails) {
                    HashMap temp = new HashMap()
                    temp.put("phyname", phy.name)
                    temp.put("pysicaldetailsId", phy.id)
                    temp.put("accompanyingMaterialsIsactive", phy.isactive)
                    temp.put("srno", srno++)
                    physicaldetailslist.add(temp)
                }
                hm.put('physicaldetails', physicaldetailslist)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def addAccompanyingMaterials(request, hm, ip) {
        println "i am in addPhysiacalDetails"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse :"+dataresponse)

        String purpose = "addLanguage from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def physicaldetailslist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)


                AccompanyingMaterials physicalDetails = AccompanyingMaterials.findByName(dataresponse?.phyicaldetails_name)
                println('physicalDetails---' + physicalDetails)
                if (physicalDetails != null) {
                    hm.put("msg", "Accompanying Materials already exist!!")
                } else {
                    physicalDetails = new AccompanyingMaterials()
                    physicalDetails.isactive = true
                    physicalDetails.name = dataresponse?.phyicaldetails_name
                    physicalDetails.updation_ip_address = ip
                    physicalDetails.creation_ip_address = ip
                    physicalDetails.updation_date = new Date()
                    physicalDetails.creation_date = new Date()
                    physicalDetails.updation_username = uid
                    physicalDetails.creation_username = uid
                    physicalDetails.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }

                def physicaldetails = AccompanyingMaterials.findAll()
                int srno = 1
                for (phy in physicaldetails) {
                    HashMap temp = new HashMap()
                    temp.put("phyname", phy.name)
                    temp.put("pysicaldetailsId", phy.id)
                    temp.put("accompanyingMaterialsIsactive", phy.isactive)
                    temp.put("srno", srno++)
                    physicaldetailslist.add(temp)
                }
                hm.put('physicaldetails', physicaldetailslist)
                println "hm: " + hm
                hm.put("status", "200")
                return hm
            }
        }
        return hm
    }

    def accompanyingMaterialsIsactive(request, hm) {
        println "i am in accompanyingMaterialsIsactive"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "physiacalDetailsIsactive"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                hm.put("org", org)

                def physicaldetails = AccompanyingMaterials.findById(dataresponse?.pysicaldetailsId)
                if (physicaldetails != null) {
                    if (physicaldetails?.isactive == false) {
                        physicaldetails.isactive = true
                        physicaldetails.updation_ip_address = request.getRemoteAddr()
                        physicaldetails.updation_date = new Date()
                        physicaldetails.updation_username = uid
                        physicaldetails.save(flush: true, failOnError: true)
                    } else {
                        physicaldetails.isactive = false
                        physicaldetails.updation_ip_address = request.getRemoteAddr()
                        physicaldetails.updation_date = new Date()
                        physicaldetails.updation_username = uid
                        physicaldetails.save(flush: true, failOnError: true)
                    }
                }
                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def editAccompanyingMaterials(request, hm, ip) {
        println "i am in editPhysiacalDetails"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "Edit editLanguage from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                AccompanyingMaterials physicaldetails = AccompanyingMaterials.findById(dataresponse?.pysicaldetailsId)

                def exist = AccompanyingMaterials.findByName(dataresponse?.phyname)
                if (exist != null) {
                    hm.put("msg", "Accompanying Materials already exist!!")
                } else {
                    if (dataresponse?.phyname)
                        physicaldetails.name = dataresponse?.phyname
                    physicaldetails.updation_ip_address = ip
                    physicaldetails.updation_date = new Date()
                    physicaldetails.updation_username = uid
                    physicaldetails.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm
                }
            }
        }
        return hm
    }

    def deleteAccompanyingMaterials(request ,hm,ip){
        println "i am in deleteAccompanyingMaterials"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteAccompanyingMaterials from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                AccompanyingMaterials accompanyingMaterials = AccompanyingMaterials.findById(dataresponse?.pysicaldetailsId)
                println("accompanyingMaterials : " + accompanyingMaterials)
                if(accompanyingMaterials!=null) {
                    try {
                        accompanyingMaterials.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return

            }

        }
    }
    //
    def getSeries(request, hm) {
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def physicaldetailslist = []
            def libDept_list =[]
            def bookType_list =[]
            def bookCl_list=[]
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def physicaldetails = Series.findAllByOrganization(user?.organization)
                int srno = 1
                for (phy in physicaldetails) {
                    HashMap temp = new HashMap()
                    temp.put("phyname", phy.name)
                    temp.put("display_order", phy.display_order)
                    temp.put("short_name", phy.short_name)
                    temp.put("physicaldetailsId", phy.id)
                    temp.put("seriesIsactive", phy.isactive)
                    temp.put("librarydepartment", phy.librarydepartment?.name)
                    temp.put("booktype", phy.booktype?.name)
                    temp.put("clid", phy?.bookclassification?.id)
                    temp.put("cl", phy?.bookclassification?.name)
                    temp.put("srno", srno++)
                    physicaldetailslist.add(temp)
                }

                hm.put('physicaldetails', physicaldetailslist)

                def libraryDept_list = LibraryDepartment.findAllByOrganization(user?.organization)
                int srn =1
                for(libdept in libraryDept_list){
                    HashMap temp = new HashMap()
                    temp.put("libDepartment", libdept.name)
                    temp.put("libDepartmentId", libdept.id)
                    temp.put("srno", srn++)
                    libDept_list.add(temp)
                }
                hm.put('libDept_list', libDept_list)
                def bookTypelist = BookType.findAllByOrganization(user?.organization)
                int sr_No =1
                for(libdept in bookTypelist){
                    HashMap temp = new HashMap()
                    temp.put("bookType", libdept.name)
                    temp.put("bookTypeId", libdept.id)
                    temp.put("srno", sr_No++)
                    bookType_list.add(temp)
                }
                hm.put('bookType_list', bookType_list)

                def bookclassificationlist = BookClassification.findAllByOrganization(user?.organization)
                int srNo =1
                for(bookcl in bookclassificationlist){
                    HashMap temp = new HashMap()
                    temp.put("bookClName", bookcl.name)
                    temp.put("bookClId", bookcl.id)
                    temp.put("srno", srNo++)
                    bookCl_list.add(temp)
                }
                hm.put('bookCl_list', bookCl_list)



                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def addSeries(request, hm, ip) {
        println "i am in addSeries"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse :"+dataresponse)
        println("display_order class---> :"+(dataresponse?.display_order).getClass())

        String purpose = "addSeries from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def physicaldetailslist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                Series physicalDetails = Series.findByNameAndOrganization(dataresponse?.seriesName,user?.organization)
                if (physicalDetails != null) {
                    hm.put("msg", "Series already exist!!")
                } else {
                    physicalDetails = new Series()
                    physicalDetails.isactive = true
                    physicalDetails.name = dataresponse?.seriesName
                    if(dataresponse?.display_order)
                    physicalDetails.display_order = Integer.parseInt(dataresponse?.display_order)
                    if(dataresponse?.short_name)
                    physicalDetails.short_name = dataresponse?.short_name
                    physicalDetails.organization = user?.organization
                    if(dataresponse?.librarydepartment){
                        LibraryDepartment libraryDepartment = LibraryDepartment.findByOrganizationAndName(user?.organization,dataresponse?.librarydepartment)
                        physicalDetails.librarydepartment = libraryDepartment
                    }
                    if(dataresponse?.bookType){
                        BookType bookType = BookType.findByOrganizationAndName(user?.organization,dataresponse?.bookType)
                        physicalDetails.booktype = bookType
                    }
                    if(dataresponse?.bookCL){
                        BookClassification cl = BookClassification.findByIdAndOrganization(dataresponse?.bookCL,user?.organization)
                        println("cl : "+cl)
                        physicalDetails.bookclassification = cl
                    }
                    physicalDetails.updation_ip_address = ip
                    physicalDetails.creation_ip_address = ip
                    physicalDetails.updation_date = new Date()
                    physicalDetails.creation_date = new Date()
                    physicalDetails.updation_username = uid
                    physicalDetails.creation_username = uid
                    physicalDetails.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }
                hm.put("status", "200")
                return hm
            }
        }
        return hm
    }

    def seriesIsactive(request, hm) {
        println "i am in seriesIsactive"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        String purpose = "physiacalDetailsIsactive"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def physicaldetails = Series.findById(dataresponse?.physicaldetailsId)
                if (physicaldetails != null) {
                    if (physicaldetails?.isactive == false) {
                        physicaldetails.isactive = true
                        physicaldetails.updation_ip_address = request.getRemoteAddr()
                        physicaldetails.updation_date = new Date()
                        physicaldetails.updation_username = uid
                        physicaldetails.save(flush: true, failOnError: true)
                    } else {
                        physicaldetails.isactive = false
                        physicaldetails.updation_ip_address = request.getRemoteAddr()
                        physicaldetails.updation_date = new Date()
                        physicaldetails.updation_username = uid
                        physicaldetails.save(flush: true, failOnError: true)
                    }
                    hm.put("status", "200")
                }

                return hm
            }
        }
        return hm
    }

    def editSeries(request, hm, ip) {
        println "i am in editSeries"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println"dataresponse in editSeries-->"+ dataresponse

        String purpose = "Edit editLanguage from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                Series physicaldetails = Series.findByIdAndOrganization(dataresponse?.phyId,user?.organization)
                println"physicaldetails----->"+physicaldetails
                def exist = Series.findByNameAndOrganization(dataresponse?.phyname,user?.organization)
                if (exist != null) {
                    if (dataresponse?.phyname)
                        physicaldetails.name = dataresponse?.phyname
                    if (dataresponse?.display_order)
                    physicaldetails.display_order = (dataresponse?.display_order)
                    if (dataresponse?.short_name)
                    physicaldetails.short_name = dataresponse?.short_name
                    if(dataresponse?.librarydepartment){
                        LibraryDepartment libraryDepartment = LibraryDepartment.findByOrganizationAndName(user?.organization,dataresponse?.librarydepartment)
                        physicaldetails.librarydepartment = libraryDepartment
                    }
                    if(dataresponse?.bookType){
                        BookType bookType = BookType.findByOrganizationAndName(user?.organization,dataresponse?.bookType)
                        physicaldetails.booktype = bookType
                    }
                    if(dataresponse?.bookCL){
                        BookClassification cl = BookClassification.findByIdAndOrganization(dataresponse?.bookCL,user?.organization)
                        println("cl : "+cl)
                        physicaldetails.bookclassification = cl
                    }
                    physicaldetails.updation_ip_address = ip
                    physicaldetails.updation_date = new Date()
                    physicaldetails.updation_username = uid
                    physicaldetails.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm
                } else {
                    hm.put("msg", "Series does not exist!!")
                    return hm
                }
            }
        }
        return hm
    }

    def deleteSeries(request ,hm, ip){
        println "i am in deleteSeries"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteSeries from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                Series series = Series.findByIdAndOrganization(dataresponse?.physicaldetailsId,user?.organization)
                println("series : " + series)
                if(series!=null) {
                    try{
                        series.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch (Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }
                else {
                    hm.put("msg", "A")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return

            }

        }
    }

    def deleteBookSeries(request ,hm , ip){
        println "i am in deleteBookSeries"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteBookSeries from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                BookSeries bookSeries = BookSeries.findById(dataresponse?.seriesId)
                println("bookSeries : " + bookSeries)
                if(bookSeries!=null) {
                    try {
                        bookSeries.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return

            }

        }
    }

    //ERPDepartment Master
    def getErpDepartment(request ,hm){
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def physicaldetailslist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                int srno = 1
                def erpdList = ERPDepartment.findAllWhere(organization: user?.organization)
                ArrayList ERPDepartmentList = new ArrayList()
                for (depts in erpdList) {
                    HashMap temp = new HashMap()
                    temp.put("id", depts.id)
                    temp.put("name", depts.name)
                    temp.put("display_order", depts.display_order)
                    temp.put("short_name", depts.short_name)
                    temp.put("srno", srno++)
                    ERPDepartmentList.add(temp)
                }
                hm.put('ERPDepartmentList', ERPDepartmentList)
                hm.put("msg", "200")
                return hm
            }
        }
    }
    def saveERPDepartment(request,hm,ip){
        println "i am in saveERPDepartment"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse :"+dataresponse)
        String purpose = "saveERPDepartment from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ERPDepartment erpDepartment = ERPDepartment.findByName(dataresponse?.erpDepartmentName)
                if (erpDepartment != null) {
                    hm.put("msg", "Department Already already exist!!")
                } else {
                    erpDepartment = new ERPDepartment()
                    erpDepartment.name = dataresponse?.erpDepartmentName
                    erpDepartment.display_order = dataresponse?.display_order
                    erpDepartment.short_name = dataresponse?.short_name
                    erpDepartment.organization = user?.organization
                    erpDepartment.updation_ip_address = ip
                    erpDepartment.creation_ip_address = ip
                    erpDepartment.updation_date = new Date()
                    erpDepartment.creation_date = new Date()
                    erpDepartment.updation_username = uid
                    erpDepartment.creation_username = uid
                    erpDepartment.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }
                hm.put("status", "200")
                return hm
            }
        }
        return hm
    }
    def editERPDepartment(request,hm,ip){
        println "i am in editERPDepartment in service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println"dataresponse--->" +dataresponse
        String purpose = "Edit editERPDepartment from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def dept = ERPDepartment.findById(dataresponse.erpDepartmentId)
                def dept1 = ERPDepartment.findByNameAndShort_name(dataresponse.erpDepartmentName,dataresponse.short_name)
                if (dept1 != null) {
                    hm.put("msg", "ERP-Department already exist!!")
                }
                else {
                    dept.name = dataresponse.erpDepartmentName
                    dept.display_order = dataresponse.display_order
                    dept.short_name = dataresponse.short_name
                    dept.organization = user?.organization
                    dept.updation_ip_address = ip
                    dept.updation_date = new Date()
                    dept.updation_username = uid
                    dept.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                    hm.put("status", "200")
                    return hm
                }
            }
        }
        return hm
    }
    def deleteErpDepartment(request,hm,ip){
        println "i am in deleteErpDepartment in service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        String purpose = "deleteErpDepartment from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ERPDepartment erpDepartment = ERPDepartment.findById(dataresponse.erpDepartmetnId)
                //println("erpDepartment : "+erpDepartment)
                if(erpDepartment!=null) {
                    try{
                        erpDepartment.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch (Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }
                else {
                    hm.put("msg", "A")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return

            }

        }
    }

    //book Vendor Type
    def getVendorType(request ,hm){
        println("Inside getVendorType...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def vendortypeList = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def bookVendorType = BookVendorType.findAll()
                int srno = 1
                for (vendor in bookVendorType) {
                    HashMap temp = new HashMap()
                    temp.put("vendorType", vendor.name)
                    temp.put("vendorTypeId", vendor.id)
                    temp.put("displayorder", vendor.display_order)
                    temp.put("shortname", vendor.short_name)
                    temp.put("description", vendor.description)
                    temp.put("srno", srno++)
                    vendortypeList.add(temp)
                }
                hm.put('vendorType', vendortypeList)
                println "hm new: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }
    def addVendorType(request ,hm,ip){
        println "i am in addVendorType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addVendorType from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def vendorTypelist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
//                def org = user.organization
//                hm.put("org",org)

                BookVendorType bookVendorType = BookVendorType.findByName(dataresponse?.vendorType)

                if (bookVendorType != null) {
                    hm.put("msg", "Book Type already exist!!")
                } else {
                    bookVendorType = new BookVendorType()
                    bookVendorType.name = dataresponse?.vendorType
                    if (dataresponse?.displayorder)
                        bookVendorType.display_order = Integer.parseInt(dataresponse?.displayorder)
                    if (dataresponse?.shortname)
                        bookVendorType.short_name = dataresponse?.shortname
                    bookVendorType.description= dataresponse?.description
                    bookVendorType.updation_ip_address = ip
                    bookVendorType.creation_ip_address = ip
                    bookVendorType.updation_date = new Date()
                    bookVendorType.creation_date = new Date()
                    bookVendorType.updation_username = uid
                    bookVendorType.creation_username = uid
                    bookVendorType.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }


                def bookVendorTypes = BookVendorType.findAll()
                int srno = 1
                for (b_type in bookVendorTypes) {
                    HashMap temp = new HashMap()
                    temp.put("vendorType", b_type.name)
                    temp.put("vendorTypeId", b_type.id)
                    temp.put("srno", srno++)
                    vendorTypelist.add(temp)
                }
                hm.put("vendorType_list", vendorTypelist)
                hm.put("status", "200")
                //println('hm---' + hm)
                return hm
            }
        }
        return hm
    }
    def deleteVendorType(request ,hm, ip){
        println "i am in deleteVendorType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteVendorType from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                BookVendorType bookVendorType = BookVendorType.findById(dataresponse?.vendorTypeId)
                println("bookVendorType : " + bookVendorType)
                if(bookVendorType!=null) {
                    try {
                        bookVendorType.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return

            }

        }
    }
    def editVendorType(request ,hm,ip){
        println "i am in editVendorType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editVendorType from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def vendorTypelist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                BookVendorType bookVendorType = BookVendorType.findById(dataresponse?.vendorTypeId)

                if (bookVendorType != null) {
                    bookVendorType.name = dataresponse?.vendorType
                    if (dataresponse?.displayorder)
                        bookVendorType.display_order = Integer.parseInt(dataresponse?.displayorder)
                    if (dataresponse?.shortname)
                        bookVendorType.short_name = dataresponse?.shortname
                    bookVendorType.description= dataresponse?.description
                    bookVendorType.updation_ip_address = ip
                    bookVendorType.updation_date = new Date()
                    bookVendorType.updation_username = uid
                    bookVendorType.save(flush: true, failOnError: true)
                    hm.put("msg", "success")

                } else {
                    hm.put("msg", "Vendor Type does not exist!!")
                }


                def bookVendorTypes = BookVendorType.findAll()
                int srno = 1
                for (b_type in bookVendorTypes) {
                    HashMap temp = new HashMap()
                    temp.put("vendorType", b_type.name)
                    temp.put("vendorTypeId", b_type.id)
                    temp.put("srno", srno++)
                    vendorTypelist.add(temp)
                }
                hm.put("vendorType_list", vendorTypelist)
                hm.put("status", "200")
                //println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def getBookVendor(request ,hm){
        println("Inside getVendorType...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def vendorList = []
            def vedortypelist=[]
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def bookVendor = BookVendor.findAll()
                def vendorTypelist = BookVendorType.findAll()
                for (vendor in vendorTypelist) {
                    HashMap temp = new HashMap()
                    temp.put("vendortype", vendor.name)
                    temp.put("vendortypeId", vendor.id)
                    vedortypelist.add(temp)
                }
                hm.put('vendortypelist', vedortypelist)
                int srno = 1
                for (vendor in bookVendor) {
                    HashMap temp = new HashMap()
                    temp.put("vendor", vendor.name)
                    temp.put("vendorId", vendor.id)
                    temp.put("bookvendortype", vendor.bookvendortype?.name)
                    temp.put("displayorder", vendor.display_order)
                    temp.put("shortname", vendor.short_name)
                    temp.put("description", vendor.description)
                    temp.put("srno", srno++)
                    vendorList.add(temp)
                }
                hm.put('vendor', vendorList)
               // println "hm new: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }
    def addBookVendor(request ,hm,ip){
        println "i am in addVendor"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addVendorType from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def vendorlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                BookVendorType bookVendorType = BookVendorType.findByName(dataresponse?.vendortype)
                BookVendor bookVendor = BookVendor.findByName(dataresponse?.vendor)

                if (bookVendor != null) {
                    hm.put("msg", "Book Vendor already exist!!")
                } else {
                    bookVendor = new BookVendor()
                    bookVendor.name = dataresponse?.vendor
                    if (dataresponse?.displayorder)
                        bookVendor.display_order = Integer.parseInt(dataresponse?.displayorder)
                    if (dataresponse?.shortname)
                        bookVendor.short_name = dataresponse?.shortname
                    bookVendor.description= dataresponse?.description
                    bookVendor.bookvendortype= bookVendorType
                    bookVendor.updation_ip_address = ip
                    bookVendor.creation_ip_address = ip
                    bookVendor.updation_date = new Date()
                    bookVendor.creation_date = new Date()
                    bookVendor.updation_username = uid
                    bookVendor.creation_username = uid
                    bookVendor.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }


                def bookVendors = BookVendor.findAll()
                int srno = 1
                for (b_type in bookVendors) {
                    HashMap temp = new HashMap()
                    temp.put("vendorType", b_type.name)
                    temp.put("vendorTypeId", b_type.id)
                    temp.put("srno", srno++)
                    vendorlist.add(temp)
                }
                hm.put("vendor_list", vendorlist)
                hm.put("status", "200")
                //println('hm---' + hm)
                return hm
            }
        }
        return hm
    }
    def deleteBookVendor(request ,hm, ip){
        println "i am in deleteVendorType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteVendorType from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                BookVendor bookVendor = BookVendor.findById(dataresponse?.vendorId)
                println("bookVendor : " + bookVendor)
                if(bookVendor!=null) {
                    try {
                        bookVendor.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return

            }

        }
    }
    def editBookVendor(request ,hm,ip){
        println "i am in editVendorType"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editVendorType from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def vendorlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                BookVendorType bookVendorType = BookVendorType.findByName(dataresponse?.vendortype)
                BookVendor bookVendor = BookVendor.findById(dataresponse?.vendorId)

                if (bookVendor != null) {
                    bookVendor.name = dataresponse?.vendor
                    if (dataresponse?.displayorder)
                        bookVendor.display_order = dataresponse?.displayorder
                    if (dataresponse?.shortname)
                        bookVendor.short_name = dataresponse?.shortname
                    bookVendor.description= dataresponse?.description
                    bookVendor.updation_ip_address = ip
                    bookVendor.updation_date = new Date()
                    bookVendor.updation_username = uid
                    bookVendor.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                } else {
                    hm.put("msg", "Vendor does not exist!!")
                }
                def bookVendors = BookVendor.findAll()
                int srno = 1
                for (b_type in bookVendors) {
                    HashMap temp = new HashMap()
                    temp.put("vendor", b_type.name)
                    temp.put("vendorId", b_type.id)
                    temp.put("srno", srno++)
                    vendorlist.add(temp)
                }
                hm.put("vendor_list", vendorlist)
                hm.put("status", "200")
                //println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def getLibraryDepartment(request ,hm){
        println("Inside getLibraryDepartment...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def erpDepartmentList = []
            def libDepartmentlist=[]
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def erpDepartment_List = ERPDepartment.findAllByOrganization(user?.organization)
                def libDepartment_list = LibraryDepartment.findAllByOrganization(user?.organization)
                for (erpDept in erpDepartment_List) {
                    HashMap temp = new HashMap()
                    temp.put("erpDepartment", erpDept.name)
                    temp.put("erpDepartmentId", erpDept.id)
                    erpDepartmentList.add(temp)
                }
                hm.put('erpDepartmentList', erpDepartmentList)
                int srno = 1
                for (libDept in libDepartment_list) {
                    HashMap temp = new HashMap()
                    temp.put("libDepartment", libDept.name)
                    temp.put("libDepartmentId", libDept.id)
                    temp.put("erpdepartment", libDept.erpdepartment?.name)
                    temp.put("displayorder", libDept.display_order)
                    temp.put("shortname", libDept.short_name)
                    temp.put("description", libDept.description)
                    temp.put("srno", srno++)
                    libDepartmentlist.add(temp)
                }
                hm.put('libDepartmentlist', libDepartmentlist)
                println "hm new: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }
    def addLibraryDepartment(request ,hm,ip){
        println "i am in addLibraryDepartment"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "addLibraryDepartment from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def libDepartmentlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ERPDepartment erpDepartment = ERPDepartment.findByNameAndOrganization(dataresponse?.erpDepartmentList,user?.organization)
                LibraryDepartment libraryDepartment = LibraryDepartment.findByNameAndOrganization(dataresponse?.libDepartment,user?.organization)

                if (libraryDepartment != null) {
                    hm.put("msg", "Department already exist!!")
                } else {
                    libraryDepartment = new LibraryDepartment()
                    libraryDepartment.name = dataresponse?.libDepartment
                    if (dataresponse?.displayorder)
                        libraryDepartment.display_order = Integer.parseInt(dataresponse?.displayorder)
                    if (dataresponse?.shortname)
                        libraryDepartment.short_name = dataresponse?.shortname
                    libraryDepartment.description= dataresponse?.description
                    libraryDepartment.erpdepartment= erpDepartment
                    libraryDepartment.organization = user?.organization
                    libraryDepartment.updation_ip_address = ip
                    libraryDepartment.creation_ip_address = ip
                    libraryDepartment.updation_date = new Date()
                    libraryDepartment.creation_date = new Date()
                    libraryDepartment.updation_username = uid
                    libraryDepartment.creation_username = uid
                    libraryDepartment.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                }
                def libDepartment_list = LibraryDepartment.findAllByOrganization(user.organization)
                int srno = 1
                for (libDept in libDepartment_list) {
                    HashMap temp = new HashMap()
                    temp.put("libDepartment", libDept.name)
                    temp.put("libDepartmentId", libDept.id)
                    temp.put("srno", srno++)
                    libDepartmentlist.add(temp)
                }
                hm.put("libDepartmentlist", libDepartmentlist)
                hm.put("status", "200")
                //println('hm---' + hm)
                return hm
            }
        }
        return hm
    }
    def deleteLibraryDepartment(request ,hm, ip){
        println "i am in deleteLibraryDepartment"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "deleteLibraryDepartment from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                LibraryDepartment libraryDepartment = LibraryDepartment.findById(dataresponse?.libDepartmentId)
                println("libraryDepartment : " + libraryDepartment)
                if(libraryDepartment!=null) {
                    try {
                        libraryDepartment.delete(flush: true)
                        hm.put("msg", "200")
                        hm.put("status", "success")
                        return hm
                    }catch(Exception e){
                        hm.put("msg", "exp")
                        hm.put("status", "Excep")
                        return hm
                    }
                }
                else {
                    hm.put("msg", "NA")
                    hm.put("status", "Error")
                    return hm
                }
                render hm
                return

            }

        }
    }
    def editLibraryDepartment(request ,hm,ip){
        println "i am in editLibraryDepartment"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse

        String purpose = "editLibraryDepartment from Admin Master Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def libDepartmentlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ERPDepartment erpDepartment = ERPDepartment.findByNameAndOrganization(dataresponse?.erpDepartment,user?.organization)
                LibraryDepartment libraryDepartment = LibraryDepartment.findByIdAndOrganization(dataresponse?.libDepartmentId,user?.organization)

                if (libraryDepartment != null) {
                    libraryDepartment.name = dataresponse?.libDepartment
                    if (dataresponse?.displayorder)
                        libraryDepartment.display_order = dataresponse?.displayorder
                    if (dataresponse?.shortname)
                        libraryDepartment.short_name = dataresponse?.shortname
                    libraryDepartment.description= dataresponse?.description
                    libraryDepartment.erpdepartment= erpDepartment
                    libraryDepartment.organization = user?.organization
                    libraryDepartment.updation_ip_address = ip
                    libraryDepartment.updation_date = new Date()
                    libraryDepartment.updation_username = uid
                    libraryDepartment.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                } else {
                    hm.put("msg", "Department does not exist!!")
                }
                def libDepartment_list = LibraryDepartment.findAllByOrganization(user.organization)
                int srno = 1
                for (libDept in libDepartment_list) {
                    HashMap temp = new HashMap()
                    temp.put("libDepartment", libDept.name)
                    temp.put("libDepartmentId", libDept.id)
                    temp.put("srno", srno++)
                    libDepartmentlist.add(temp)
                }
                hm.put("libDepartmentlist", libDepartmentlist)
                hm.put("status", "200")
                //println('hm---' + hm)
                return hm
            }
        }
        return hm
    }
    def importLibraryDepartment(request, ip ,hm){
        println(' in  importLibraryDepartment :')
        def uid = request.getHeader("EPS-uid")
        Part filePart = request.getPart('file')
        InputStream fs = filePart.getInputStream()
        File file = File.createTempFile("s3test", "");
        OutputStream outputStream = new FileOutputStream(file)
        IOUtils.copy(fs, outputStream);
        def reader = new CSVReader(new FileReader(file))
        AdminService adminServicev= new AdminService()
        println('fs----'+ fs)
        String loginattachmentdomainname = ""
        String[] nextRecord;
        boolean header = true
        ArrayList failList = new ArrayList()

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def logeduser = User.findByUsername(log.username)
                //UserType usertype = UserType.findByNameIlike("%Employee%")
                loginattachmentdomainname = logeduser.organization.loginattachementdomain

                while ((nextRecord = reader.readNext()) != null) {
                    println("nextRecord :--- "+nextRecord)
                    String reason = 'Data is Missing - '
                    HashMap temp = new HashMap()
                    if (header) //skip headers
                    {
                        header = false
                        continue
                    }
                    if (nextRecord[0] != ''){
                        println("nextRecord[4] : "+nextRecord)
//                        User user = User.findByUsernameAndOrganization(nextRecord[0].trim()+logeduser.organization.loginattachementdomain,logeduser.organization)
                        LibraryDepartment libraryDepartment = LibraryDepartment.findByNameAndOrganization(nextRecord[0].trim(),logeduser?.organization)
                        if (libraryDepartment == null) {
                            libraryDepartment = new LibraryDepartment()
                            libraryDepartment.name = nextRecord[0].trim()
                            if (nextRecord[1])
                                libraryDepartment.description = nextRecord[1].trim()
                            if (nextRecord[2])
                                libraryDepartment.display_order = Integer.parseInt(nextRecord[2].trim())
                            if (nextRecord[3])
                                libraryDepartment.short_name = nextRecord[3].trim()
                            println("sh dfjhsjdhfj s f sj hfh sjkdhf jhs jdfh jksh djkfh "+nextRecord[4])
                            if (nextRecord[4] && nextRecord[4]!=''){
                                ERPDepartment erpDepartment = ERPDepartment.findByNameAndOrganization(nextRecord[4].trim(),logeduser?.organization)
                                libraryDepartment.erpdepartment = erpDepartment
                            }
                            libraryDepartment.organization = logeduser?.organization
                            libraryDepartment.creation_username = uid
                            libraryDepartment.updation_username = uid
                            libraryDepartment.creation_date = new Date()
                            libraryDepartment.updation_date = new Date()
                            libraryDepartment.creation_ip_address = ip
                            libraryDepartment.updation_ip_address = ip
                            libraryDepartment.save(flush: true, failOnError: true)
                        } else {
                            reason = "Library Department already present"
                            temp.put('reason', reason)
                            temp.put('name', nextRecord[0].trim())
                            failList.add(temp)
                        }
                    }
                }
                hm.put("status", "200")
            }
        }
        outputStream.flush()
        outputStream.close()
        fs.close()
        file.delete()
        hm.put('failList',failList)
        return hm
    }

    //-------------Frequency

    //getFrequency
    def getFrequency(request, ip ,hm) {
        println("Inside getFrequency")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def frequency_list = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
//                def org = user.organization
                def frequency = Frequency.findAllByOrganization(user.organization)
                int srno = 1
                for (f in frequency) {
                    HashMap temp = new HashMap()
                    temp.put("name", f.name)
                    temp.put("id", f.id)
                    temp.put("isactive", f.isactive)
                    temp.put("org", f.organization)
                    temp.put("srno", srno++)
                    frequency_list.add(temp)
                }
                hm.put('frequency_list', frequency_list)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }
    def saveFrequency(request, ip ,hm){
        println("Inside saveFrequency")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse : "+dataresponse)
        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def frequency_list = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
//                def org = user.organization
                Frequency frequency = Frequency.findByOrganizationAndName(user.organization,dataresponse.frqName)
                if(frequency == null){
                    frequency = new Frequency()
                    frequency.name = dataresponse.frqName
                    frequency.isactive = true
                    frequency.organization = user.organization
                    frequency.updation_ip_address = ip
                    frequency.creation_ip_address = ip
                    frequency.updation_date = new Date()
                    frequency.creation_date = new Date()
                    frequency.updation_username = uid
                    frequency.creation_username = uid
                    frequency.save(flush: true, failOnError: true)
                    hm.put("code", "200")
                    hm.put("msg","Added Successfully")
                }else{
                    hm.put("msg", "200")
                    hm.put("msg","Already Exists")
                }
                return hm
            }
        }
    }
    def editFrequency(request, ip ,hm){
        println("edit Frequency")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse : "+dataresponse)
        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def frequency_list = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization

                Frequency frequency = Frequency.findById(dataresponse.id)
                // println("dataresponse",+dataresponse.name)
                if(frequency != null){
                    frequency.name = dataresponse.name
//                    frequency.isactive = true
                    frequency.organization = organization
                    frequency.updation_ip_address = ip
                    frequency.updation_date = new Date()
                    frequency.updation_username = uid
                    frequency.save(flush: true, failOnError: true)
                    hm.put("code", "200")
                    hm.put("msg","edited Successfully")
                }else{
                    hm.put("code", "200")
                    hm.put("msg","Already Exists")
                }
                println "hm: " + hm
                return hm
            }
        }
    }
    def deleteFrequency(request, ip ,hm){
        println("delete Frequency")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse : "+dataresponse)
        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def frequency_list = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
//                def org = user.organization
                Frequency frequency = Frequency.findById(dataresponse.editedFrequencyId)
                def freq = Frequency.findByName(dataresponse?.frequency)
                println('dataresponse?.frequency---' + dataresponse?.frequency)

                println(dataresponse.deleteitem)
                def d = Frequency.findById(dataresponse.deleteitem)
                if (d != null) {

                    d.delete(flush: true, failOnError: true)
                    hm.put("msg", "200")
                } else {
                    hm.put("msg", "error")
                }

                return hm
            }
        }
    }
    def frequencyIsactive(request, ip ,hm){
        println("Isactive Frequency...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        String purpose = "Isactive Frequency."
        println("Isactive Frequency dataresponse"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
//            println("dataresponse",+dataresponse.name)
            Tenants.withId(log.tenantclient.tenantid) {
//                println("dataresponse")
                def user = User.findByUsername(log.username)
                def organization = user.organization
//                println("dataresponse",+dataresponse.name)
                // BookClassification bclist = BookClassification.findByOrganizationAndId(organization,id)
                Frequency frequency=Frequency.findById(dataresponse.id)

                if(frequency!=null) {
                    if(dataresponse.Isactive == false) {
                        frequency.isactive = false
                        frequency.organization = organization
                        frequency.updation_username = uid
                        frequency.updation_date = new Date()
                        frequency.updation_ip_address = ip
                        frequency.save(flush: true, failOnError: true)
                        hm.put("msg","200")
                    }
                    else{
                        frequency.isactive = true
                        frequency.organization = organization
                        frequency.updation_username = uid
                        frequency.updation_date = new Date()
                        frequency.updation_ip_address = ip
                        frequency.save(flush: true, failOnError: true)
                        hm.put("msg","200")
                    }

                }
                println "hm: " + hm
                return hm
            }
        }
    }

    //.....................LibraryPolicyType
    def getLibraryPolicyType(request, ip ,hm){
        println("Inside getLibraryPolicyType")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def libraryPolicyType_list = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
//                def org = user.organization
                def libraryPolicyType = LibraryPolicyType.findAllByOrganization(user.organization)
                int srno = 1
                for (l in libraryPolicyType) {
                    HashMap temp = new HashMap()
                    temp.put("name", l.name)
                    temp.put("id", l.id)
                    temp.put("isactive", l.isactive)
                    temp.put("org", l.organization)
                    temp.put("srno", srno++)
                    libraryPolicyType_list.add(temp)
                }
                hm.put('libraryPolicyType_list', libraryPolicyType_list)
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }
    def saveLibraryPolicyType(request, ip ,hm){
        println("Inside saveLibraryPolicyType")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse : "+dataresponse)
        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def frequency_list = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
//                def org = user.organization
                LibraryPolicyType libraryPolicyType = LibraryPolicyType.findByOrganizationAndName(user.organization,dataresponse.lptName)

                if(libraryPolicyType == null){
                    libraryPolicyType = new LibraryPolicyType()
                    libraryPolicyType.name = dataresponse.lptName
                    libraryPolicyType.isactive = true
                    libraryPolicyType.organization = user.organization
                    libraryPolicyType.updation_ip_address = ip
                    libraryPolicyType.creation_ip_address = ip
                    libraryPolicyType.updation_date = new Date()
                    libraryPolicyType.creation_date = new Date()
                    libraryPolicyType.updation_username = uid
                    libraryPolicyType.creation_username = uid
                    libraryPolicyType.save(flush: true, failOnError: true)
                    hm.put("code", "200")
                    hm.put("msg","Added Successfully")
                }else{
                    hm.put("msg", "200")
                    hm.put("msg","Already Exists")
                }
                return hm
            }
        }
    }
    def editLibraryPolicyType(request, ip ,hm){
        println("edit LibraryPolicyType")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse : "+dataresponse)
        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def libraryPolicyType_list = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def organization = user.organization

                LibraryPolicyType libraryPolicyType = LibraryPolicyType.findById(dataresponse.id)
                // println("dataresponse",+dataresponse.name)
                if(libraryPolicyType != null){
                    libraryPolicyType.name = dataresponse.name
//                    frequency.isactive = true
                    libraryPolicyType.organization = organization
                    libraryPolicyType.updation_ip_address = ip
                    libraryPolicyType.updation_date = new Date()
                    libraryPolicyType.updation_username = uid
                    libraryPolicyType.save(flush: true, failOnError: true)
                    hm.put("code", "200")
                    hm.put("msg","edited Successfully")
                }else{
                    hm.put("code", "200")
                    hm.put("msg","Already Exists")
                }
                println "hm: " + hm
                return hm
            }
        }
    }
    def deleteLibraryPolicyType(request, ip ,hm){
        println("delete LibraryPolicyType")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse : "+dataresponse)
        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def libraryPolicyType_list = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
//                def org = user.organization
                LibraryPolicyType libraryPolicyType = LibraryPolicyType.findById(dataresponse.editedLibraryPolicyTypeId)
                def lpt = LibraryPolicyType.findByName(dataresponse?.libraryPolicyType)
                println('dataresponse?.frequency---' + dataresponse?.libraryPolicyType)

                println(dataresponse.deleteitem)
                def d = LibraryPolicyType.findById(dataresponse.deleteitem)
                if (d != null) {
                    d.delete(flush: true, failOnError: true)
                    hm.put("msg", "200")
                } else {
                    hm.put("msg", "error")
                }

                return hm
            }
        }
    }
    def LibraryPolicyTypeIsactive(request, ip ,hm){
        println("Isactive LibraryPolicyType...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        String purpose = "Isactive LibraryPolicyType."
        println("Isactive Frequency dataresponse"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
//            println("dataresponse",+dataresponse.name)
            Tenants.withId(log.tenantclient.tenantid) {
//                println("dataresponse")
                def user = User.findByUsername(log.username)
                def organization = user.organization
//                println("dataresponse",+dataresponse.name)
                // BookClassification bclist = BookClassification.findByOrganizationAndId(organization,id)
                LibraryPolicyType libraryPolicyType=LibraryPolicyType.findById(dataresponse.id)

                if(libraryPolicyType!=null) {
                    if(dataresponse.Isactive == false) {
                        libraryPolicyType.isactive = false
                        libraryPolicyType.organization = organization
                        libraryPolicyType.updation_username = uid
                        libraryPolicyType.updation_date = new Date()
                        libraryPolicyType.updation_ip_address = ip
                        libraryPolicyType.save(flush: true, failOnError: true)
                        hm.put("msg","200")
                    }
                    else{
                        libraryPolicyType.isactive = true
                        libraryPolicyType.organization = organization
                        libraryPolicyType.updation_username = uid
                        libraryPolicyType.updation_date = new Date()
                        libraryPolicyType.updation_ip_address = ip
                        libraryPolicyType.save(flush: true, failOnError: true)
                        hm.put("msg","200")
                    }

                }
                println "hm: " + hm
                return hm
            }
        }
    }


}
