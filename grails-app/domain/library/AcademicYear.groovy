package library

import grails.gorm.MultiTenant
class AcademicYear implements MultiTenant<LearnerDivision> {
    String ay
    String display_name
    boolean isactive
    boolean iscurrent
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo = [
        nextyear:AcademicYear,previousyear:AcademicYear,
    ]
    static constraints = {
        nextyear nullable: true
        previousyear nullable: true
        display_name nullable: true
    }
    static mapping = {
        isactive defaultValue: true
        iscurrent defaultValue: false
    }
}
