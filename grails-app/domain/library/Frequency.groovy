package library
import grails.gorm.MultiTenant
class Frequency implements MultiTenant<Frequency>
{
    String name   //Monthly/Yearly/Half-Yearly/Quarterly/Daily
    boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
}


