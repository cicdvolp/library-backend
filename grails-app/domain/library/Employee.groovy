package library
import grails.gorm.MultiTenant
class Employee implements MultiTenant<Employee>
{
    String employee_code // registration number or employee code
    String name
    boolean isretired
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,user:User,person:Person,program:Program]
    static constraints = {
        name nullable: true
        employee_code nullable: true
        program nullable:true
    }
    static mapping = {
        isretired defaultValue: false
    }
}
