package library

import com.opencsv.CSVReader
import grails.gorm.multitenancy.Tenants
import grails.gorm.transactions.Transactional
import grails.io.IOUtils
import groovy.json.JsonSlurper
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.hibernate.Session
import org.hibernate.SessionFactory
import org.springframework.http.codec.multipart.FilePart
import java.util.Date;
import javax.servlet.http.Part
import java.text.SimpleDateFormat
import java.time.Period

@Transactional
class LibrarianService {

    def serviceMethod() {

    }
    def fetchfilter(request, hm, ip) {
        println("in fetchfilter:")

        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        AuthService auth = new AuthService()
        println(dataresponse)
        String purpose = "getBookFilter"
        auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                User logeduser = User.findByUsername(log.username)
                HashMap temp2 =new HashMap()
                temp2.put("name",'ALL')
                temp2.put("id",'ALL')

                Series series = Series.findById(dataresponse.seriesid)

                def publisherlist = []
                def departmentlist = []
                def titlelist = []
                def isbnlist = []

                publisherlist.add(temp2)
                titlelist.add(temp2)
                departmentlist.add(temp2)
                isbnlist.add(temp2)

                def dept = Book.createCriteria().list(){
                    projections{
                        distinct('librarydepartment')
                    }
                    'in'('organization', logeduser.organization)
                    and{
                        'in'('series', series)
                    }
                }

                for(item in dept) {
                    HashMap temp =new HashMap()
                    temp.put("name", item?.name)
                    temp.put("id", item?.id)
                    departmentlist.addAll(dept)
                }

                def publisher = Book.createCriteria().list(){
                    projections{
                        distinct('publisher')
                    }
                    'in'('organization', logeduser.organization)
                    and{
                        'in'('series', series)
                    }
                }

                for(item in publisher) {
                    HashMap temp =new HashMap()
                    temp.put("name", item?.name)
                    temp.put("id", item?.id)
                    publisherlist.addAll(dept)
                }

                def isbn = Book.createCriteria().list(){
                    projections{
                        distinct('isbn')
                    }
                    'in'('organization', logeduser.organization)
                    and{
                        'in'('series', series)
                    }
                }
                for(item in isbn) {
                    HashMap temp =new HashMap()
                    temp.put("name", item)
                    isbnlist.add(temp)
                }

                def title = Book.createCriteria().list(){

                    'in'('organization', logeduser.organization)
                    and{
                        'in'('series', series)
                    }
                }

                for(item in title) {
                    HashMap temp =new HashMap()
                    temp.put("name", item.title)
                    temp.put("id", item.id)
                    titlelist.add(temp)
                }

                hm.put("departmentlist",departmentlist)
                hm.put("publisherlist",publisherlist)
                hm.put('isbnlist',isbnlist)
                hm.put('titlelist',titlelist)
                hm.put('msg','200')
            }
            return hm
        }
        return hm
    }


    def editbulkbook(request, hm, ip,params) {

        println(' in  editbulkbook :',)
        def uid = request.getHeader("EPS-uid")
        println('---------uid' + uid)

        ArrayList faillist  = new ArrayList()
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)


                def file = request.getFile("file")
                int count=0
                if(params.count)
                    count= Integer.parseInt( params.count)
                println("counr")
                println(count)
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(params.data.toString())
                println(dataresponse)
                def sheetheader = []
                def values = []

                def fileinput=file.getInputStream()
                def workbook = new XSSFWorkbook(fileinput)
                def sheet = workbook.getSheetAt(0)


                for (cell in sheet.getRow(0).cellIterator()) {
                    sheetheader << cell.stringCellValue

                }
                def headerFlag = true
                for (row in sheet.rowIterator()) {

                    if (headerFlag) {
                        headerFlag = false
                        continue
                    }
                    def value=''
                    def map = [:]
//                        int z = 0
                    for (cell in row.cellIterator()) {

                        switch (cell.cellType) {
                            case 1:
                                value = cell.stringCellValue
                                map["${sheetheader[cell.columnIndex]}"] = value
                                break
                            case 0:
                                cell.setCellType(cell.CELL_TYPE_STRING)
                                value = cell.stringCellValue
                                map["${sheetheader[cell.columnIndex]}"] = value
                                break
                            case 2:
                                cell.setCellType(cell.CELL_TYPE_STRING)
                                value = cell.stringCellValue
                                map["${sheetheader[cell.columnIndex]}"] = value
                                break
                            case 3:
                                value = cell.stringCellValue
                                map["${sheetheader[cell.columnIndex]}"] = value
                                break
                            case 4:
                                value = cell.stringCellValue
                                map["${sheetheader[cell.columnIndex]}"] = value
                                break
                        }


                    }

                    if (values.size() > 0) {

                        if (values[0].keySet().size() == map.size())
                            values.add(map)
                    } else {
                        values.add(map)
                    }
                }


                def array=[]
                for(m in dataresponse)
                {
                    if(m.status)
                        array.add(m)
                }
                if(count==-1){
                    hm.put("count",count+1)
                    hm.put("successrecord",0)
                    hm.put("isdone",false)
                    hm.put("failrecord",0)
                    hm.put("totalbooksize",values.size())
                    hm.put("status", "200")
                    return
                }
                int m=1
                if(count<values.size())
                {
                    for (int i = count; i < values.size(); i++) {
                        println(i)
                        m=m+1;
                        count=count+1;
                        if(m==500)
                        {

                            break
                        }


                        def accession_no = values[i].get("Accession No")
                        println(i+" : "+accession_no)
                        if(accession_no==null || accession_no=='')
                        {
                            HashMap error =new HashMap()
                            error.put("error","Please check old Assection no")
                            error.put("rowno",i+2)
                            faillist.add(error)
                            continue
                        }
                        else {
                            def newassectionNO = values[i].get("New_Accession_No")
                            def series = values[i].get("Series")
                            def libdept = values[i].get("LibraryDepartment")
                            def barcode_no = values[i].get("Barcode_Number")
                            def author = values[i].get("Author")
                            def Title = values[i].get("title")
                            def Date_of_entry = values[i].get("date_of_entry")
                            def Medium = values[i].get("medium")
                            def Cno = values[i].get("classification_no")
                            def Keyword = values[i].get("keyword")
                            def Book_Price = values[i].get("book_price")
                            def Display_Order = values[i].get("display_order")
                            def Subject = values[i].get("subject")
                            def Book_type = values[i].get("book_type")
                            def Bill_No = values[i].get("bill_no")
                            def Vendor = values[i].get("vendor")
                            def Date_Of_Purchase = values[i].get("date_of_purchase")
                            for(x in array)
                            {


                                if(x.name=='Series')
                                {
                                    if(series!=null)
                                    {


                                        BookItem bookItem= BookItem.findByAccession_numberAndOrganization(accession_no,user.organization)
                                        println(bookItem)
                                        if(bookItem==null){
                                            HashMap error =new HashMap()
                                            error.put("error","while editing series Book Item No Found."+ accession_no)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                        Series series1= Series.findByNameAndOrganization(series,user.organization)
                                        if(series1==null){
                                            HashMap error =new HashMap()
                                            error.put("error","series not found in master table" +series)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                        Book book =Book.findById(bookItem.book.id)
                                        if(book==null){
                                            HashMap error =new HashMap()
                                            error.put("error","Title Book not founf.")
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                        try {
                                            bookItem.series = series1
                                            bookItem.save(flush: true, failOnError: true)

                                            book.series=series1
                                            book.save(flush: true, failOnError: true)

                                        }catch(Exception e)
                                        {
                                            HashMap error =new HashMap()
                                            error.put("error",e)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                    }
                                    else {
                                        HashMap error =new HashMap()
                                        error.put("error","Please enter Series")
                                        error.put("rowno",i+2)
                                        faillist.add(error)
                                        continue
                                    }

                                }
                                if(x.name=='New_Accession_No')
                                {
                                    if(newassectionNO!=null)
                                    {
                                        BookItem bookItem= BookItem.findByAccession_numberAndOrganization(accession_no,user.organization)
                                        if(bookItem==null){
                                            HashMap error =new HashMap()
                                            error.put("error","while editing new assection Book Item No Found."+ accession_no)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                        try {
                                            bookItem.accession_number = newassectionNO
                                            bookItem.save(flush: true, failOnError: true)
                                        }catch(Exception e)
                                        {
                                            HashMap error =new HashMap()
                                            error.put("error",e)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                    }
                                    else {
                                        HashMap error =new HashMap()
                                        error.put("error","Please enter new Assection No")
                                        error.put("rowno",i+2)
                                        faillist.add(error)
                                        continue
                                    }

                                }
                                if(x.name=='LibraryDepartment')
                                {
                                    if(libdept!=null)
                                    {
                                        println(accession_no)
                                        BookItem bookItem= BookItem.findByAccession_numberAndOrganization(accession_no,user.organization)
                                        println(bookItem)
                                        if(bookItem==null){
                                            HashMap error =new HashMap()
                                            error.put("error","while editing Library Department Book Item No Found."+ accession_no)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                        LibraryDepartment libraryDepartment=  LibraryDepartment.findByNameAndOrganization(libdept,user.organization)
                                        if(libraryDepartment==null){
                                            HashMap error =new HashMap()
                                            error.put("error","Library Department not found in master table" +libdept)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }

                                        try {
                                            bookItem.librarydepartment = libraryDepartment
                                            bookItem.save(flush: true, failOnError: true)

                                        }catch(Exception e)
                                        {
                                            HashMap error =new HashMap()
                                            error.put("error",e)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                    }
                                    else {
                                        HashMap error =new HashMap()
                                        error.put("error","Please enter new Assection No")
                                        error.put("rowno",i+2)
                                        faillist.add(error)
                                        continue
                                    }

                                }
                                if(x.name=='Barcode_Number')
                                {
                                    if(barcode_no!=null)
                                    {
                                        BookItem bookItem= BookItem.findByAccession_numberAndOrganization(accession_no,user.organization)
                                        if(bookItem==null){
                                            HashMap error =new HashMap()
                                            error.put("error","while editing new barcode Book Item No Found."+ accession_no)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                        try {
                                            bookItem.barocde = barcode_no
                                            bookItem.save(flush: true, failOnError: true)
                                        }catch(Exception e)
                                        {
                                            HashMap error =new HashMap()
                                            error.put("error",e)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                    }
                                    else {
                                        HashMap error =new HashMap()
                                        error.put("error","Please enter Barcode No")
                                        error.put("rowno",i+2)
                                        faillist.add(error)
                                        continue
                                    }

                                }
                                if(x.name=='Author')
                                {
                                    if(author!=null)
                                    {
                                        BookItem bookItem= BookItem.findByAccession_numberAndOrganization(accession_no,user.organization)
                                        if(bookItem==null){
                                            HashMap error =new HashMap()
                                            error.put("error","while editing Author Book Item No Found. "+ accession_no)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                        try {
                                            Book book= bookItem.book
                                            def book_authors=BooksAuthors.findAllByBookAndOrganization(book,user.organization)
                                            for(auther in book_authors)
                                                auther.delete(flush:true)
                                            if(author){
                                                String[] authorArray = author.split("|");
                                                for (int author_i = 0; author_i < authorArray.size(); author_i++) {
                                                    println('author_i=====' + author_i)
                                                    println('authorArray[author_i]=======' + authorArray[author_i])
                                                    def authorId = Author.findByName(authorArray[author_i])
                                                    if(authorId != null){
                                                        BooksAuthors bookAuthor=new BooksAuthors()
                                                        bookAuthor. creation_username=uid
                                                        bookAuthor. updation_username=uid
                                                        bookAuthor. creation_date=new Date()
                                                        bookAuthor. updation_date=new Date()
                                                        bookAuthor. creation_ip_address=ip
                                                        bookAuthor. updation_ip_address=ip
                                                        bookAuthor. organization=user.organization
                                                        bookAuthor.book=book
                                                        bookAuthor.author=authorId
                                                        bookAuthor.save(flush: true, failOnError: true)
                                                    }else{
                                                        authorId = new Author()
                                                        authorId.isactive = true
                                                        authorId.name = authorArray[author_i]
                                                        authorId.updation_ip_address = ip
                                                        authorId.creation_ip_address = ip
                                                        authorId.updation_date = new Date()
                                                        authorId.creation_date = new Date()
                                                        authorId.updation_username = uid
                                                        authorId.creation_username = uid
                                                        authorId.save(flush: true, failOnError: true)

                                                        BooksAuthors bookAuthor=new BooksAuthors()
                                                        bookAuthor. creation_username=uid
                                                        bookAuthor. updation_username=uid
                                                        bookAuthor. creation_date=new Date()
                                                        bookAuthor. updation_date=new Date()
                                                        bookAuthor. creation_ip_address=ip
                                                        bookAuthor. updation_ip_address=ip
                                                        bookAuthor. organization=user.organization
                                                        bookAuthor.book=book
                                                        bookAuthor.author=authorId
                                                        bookAuthor.save(flush: true, failOnError: true)
                                                    }
                                                }
                                            }



                                        }catch(Exception e)
                                        {
                                            HashMap error =new HashMap()
                                            error.put("error",e)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                    }
                                    else {
                                        HashMap error =new HashMap()
                                        error.put("error","Please enter Author Name")
                                        error.put("rowno",i+2)
                                        faillist.add(error)
                                        continue
                                    }

                                }
                                if(x.name=='title')
                                {
                                    if(Title!=null)
                                    {
                                        BookItem bookItem= BookItem.findByAccession_numberAndOrganization(accession_no,user.organization)
                                        if(bookItem==null){
                                            HashMap error =new HashMap()
                                            error.put("error","while editing Title Book Item No Found. "+ accession_no)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                        try {
                                            Book book= bookItem?.book


                                            if(Title){
                                                book.title=Title
                                                book.save(flush: true, failOnError: true)
                                            }

                                        }catch(Exception e)
                                        {
                                            HashMap error =new HashMap()
                                            error.put("error",e)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                    }
                                    else {
                                        HashMap error =new HashMap()
                                        error.put("error","Please enter Title Name")
                                        error.put("rowno",i+2)
                                        faillist.add(error)
                                        continue
                                    }

                                }
                                if(x.name=='classification_no')
                                {
                                    if(Cno!=null)
                                    {
                                        BookItem bookItem= BookItem.findByAccession_numberAndOrganization(accession_no,user.organization)
                                        if(bookItem==null){
                                            HashMap error =new HashMap()
                                            error.put("error","while editing Classification No Book Item Not Found. "+ accession_no)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                        try {
                                            Book book= bookItem?.book


                                            if(Cno){
                                                book.classificationno=Cno
                                                book.save(flush: true, failOnError: true)
                                            }

                                        }catch(Exception e)
                                        {
                                            HashMap error =new HashMap()
                                            error.put("error",e)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                    }
                                    else {
                                        HashMap error =new HashMap()
                                        error.put("error","Please enter Classification No")
                                        error.put("rowno",i+2)
                                        faillist.add(error)
                                        continue
                                    }

                                }
                                if(x.name=='date_of_entry')
                                {
                                    if(Date_of_entry!=null)
                                    {
                                        BookItem bookItem= BookItem.findByAccession_numberAndOrganization(accession_no,user.organization)
                                        if(bookItem==null){
                                            HashMap error =new HashMap()
                                            error.put("error","while editing Date of Entry Book Item No Found. "+ accession_no)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                        try {
                                            if(Date_of_entry){
                                                if (Date_of_entry && Date_of_entry != 'Invalid date') {
                                                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
//                                                    Date date2 = formatter.parse(Date_of_entry);

                                                    Calendar c = Calendar.getInstance();
                                                    c.setTime(formatter.parse(Date_of_entry))
                                                    c.add(Calendar.DATE, 1);  // number of days to add

                                                    bookItem.date_of_entry = c.getTime()
                                                    bookItem.save(flush: true, failOnError: true)
                                                }
                                            }

                                        }catch(Exception e)
                                        {
                                            HashMap error =new HashMap()
                                            error.put("error",e)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                    }
                                    else {
                                        HashMap error =new HashMap()
                                        error.put("error","Please enter Date of Entry")
                                        error.put("rowno",i+2)
                                        faillist.add(error)
                                        continue
                                    }

                                }
                                if(x.name=='medium')
                                {
                                    if(Medium!=null)
                                    {
                                        BookItem bookItem= BookItem.findByAccession_numberAndOrganization(accession_no,user.organization)
                                        if(bookItem==null){
                                            HashMap error =new HashMap()
                                            error.put("error","while editing Medium Book Item No Found. "+ accession_no)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                        try {
                                            Language language=Language.findByNameAndOrganization(Medium,user.organization)
                                            Book book= bookItem?.book


                                            if(language){
                                                book.language=language
                                                book.save(flush: true, failOnError: true)
                                            }

                                        }catch(Exception e)
                                        {
                                            HashMap error =new HashMap()
                                            error.put("error",e)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                    }
                                    else {
                                        HashMap error =new HashMap()
                                        error.put("error","Please enter Medium")
                                        error.put("rowno",i+2)
                                        faillist.add(error)
                                        continue
                                    }

                                }
                                if(x.name=='keyword')
                                {
                                    if(Keyword!=null)
                                    {
                                        BookItem bookItem= BookItem.findByAccession_numberAndOrganization(accession_no,user.organization)
                                        if(bookItem==null){
                                            HashMap error =new HashMap()
                                            error.put("error","while editing Keyword Book Item No Found. "+ accession_no)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                        try {
                                            if(Keyword){
                                                bookItem.keyword = Keyword
                                                bookItem.save(flush: true, failOnError: true)
                                            }
                                        }catch(Exception e)
                                        {
                                            HashMap error =new HashMap()
                                            error.put("error",e)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                    }
                                    else {
                                        HashMap error =new HashMap()
                                        error.put("error","Please enter keyword")
                                        error.put("rowno",i+2)
                                        faillist.add(error)
                                        continue
                                    }

                                }
                                if(x.name=='book_price')
                                {
                                    if(Book_Price!=null)
                                    {
                                        BookItem bookItem= BookItem.findByAccession_numberAndOrganization(accession_no,user.organization)
                                        if(bookItem==null){
                                            HashMap error =new HashMap()
                                            error.put("error","while editing Book Price Book Item Not Found. "+ accession_no)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                        try {
                                            if(Book_Price){
                                                bookItem.book_price = Double.parseDouble(Book_Price)
                                                bookItem.save(flush: true, failOnError: true)
                                            }
                                        }catch(Exception e)
                                        {
                                            HashMap error =new HashMap()
                                            error.put("error",e)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                    }
                                    else {
                                        HashMap error =new HashMap()
                                        error.put("error","Please enter Book Price")
                                        error.put("rowno",i+2)
                                        faillist.add(error)
                                        continue
                                    }

                                }
                                if(x.name=='display_order')
                                {
                                    if(Display_Order!=null)
                                    {
                                        BookItem bookItem= BookItem.findByAccession_numberAndOrganization(accession_no,user.organization)
                                        if(bookItem==null){
                                            HashMap error =new HashMap()
                                            error.put("error","while editing display order Book Item Not Found. "+ accession_no)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                        try {
                                            if(Display_Order){
                                                bookItem.display_order = Long.parseLong(Display_Order)
                                                bookItem.save(flush: true, failOnError: true)
                                            }
                                        }catch(Exception e)
                                        {
                                            HashMap error =new HashMap()
                                            error.put("error",e)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                    }
                                    else {
                                        HashMap error =new HashMap()
                                        error.put("error","Please enter Book Price")
                                        error.put("rowno",i+2)
                                        faillist.add(error)
                                        continue
                                    }

                                }
                                if(x.name=='subject')
                                {
                                    if(Subject!=null)
                                    {
                                        BookItem bookItem= BookItem.findByAccession_numberAndOrganization(accession_no,user.organization)
                                        if(bookItem==null){
                                            HashMap error =new HashMap()
                                            error.put("error","while editing Subject Book Item No Found. "+ accession_no)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                        try {
                                            if(Subject){
                                                bookItem.subject = Subject
                                                bookItem.save(flush: true, failOnError: true)
                                            }
                                        }catch(Exception e)
                                        {
                                            HashMap error =new HashMap()
                                            error.put("error",e)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                    }
                                    else {
                                        HashMap error =new HashMap()
                                        error.put("error","Please enter Subject")
                                        error.put("rowno",i+2)
                                        faillist.add(error)
                                        continue
                                    }

                                }
                                if(x.name=='book_type')
                                {
                                    if(Book_type!=null)
                                    {
                                        BookItem bookItem= BookItem.findByAccession_numberAndOrganization(accession_no,user.organization)
                                        if(bookItem==null){
                                            HashMap error =new HashMap()
                                            error.put("error","while editing Book Type Book Item Not Found. "+ accession_no)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                        try {
                                            BookType  booktype=BookType.findByOrganizationAndName(user.organization,Book_type)
                                            if(booktype==null)
                                            {
                                                HashMap error =new HashMap()
                                                error.put("error","Please add Book type in Master First ("+ Book_type+")")
                                                error.put("rowno",i+2)
                                                faillist.add(error)
                                                continue
                                            }
                                            Book book= bookItem?.book


                                            if(booktype){
                                                book.booktype=booktype
                                                book.save(flush: true, failOnError: true)
                                            }

                                        }catch(Exception e)
                                        {
                                            HashMap error =new HashMap()
                                            error.put("error",e)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                    }
                                    else {
                                        HashMap error =new HashMap()
                                        error.put("error","Please enter Medium")
                                        error.put("rowno",i+2)
                                        faillist.add(error)
                                        continue
                                    }

                                }
                                if(x.name=='bill_no')
                                {
                                    if(Bill_No !=null)
                                    {
                                        BookItem bookItem= BookItem.findByAccession_numberAndOrganization(accession_no,user.organization)
                                        if(bookItem==null){
                                            HashMap error =new HashMap()
                                            error.put("error","while editing Bill No Book Item No Found. "+ accession_no)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                        try {
                                            if(Bill_No){
                                                bookItem.bill_number = Bill_No
                                                bookItem.save(flush: true, failOnError: true)
                                            }
                                        }catch(Exception e)
                                        {
                                            HashMap error =new HashMap()
                                            error.put("error",e)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                    }
                                    else {
                                        HashMap error =new HashMap()
                                        error.put("error","Please enter Bill No")
                                        error.put("rowno",i+2)
                                        faillist.add(error)
                                        continue
                                    }

                                }
                                if(x.name=='vendor')
                                {
                                    if(Vendor !=null)
                                    {
                                        BookItem bookItem= BookItem.findByAccession_numberAndOrganization(accession_no,user.organization)
                                        if(bookItem==null){
                                            HashMap error =new HashMap()
                                            error.put("error","while editing Vender Book Item No Found. "+ accession_no)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                        try {
                                            if(Vendor){
                                                bookItem.vendor_name = Vendor
                                                bookItem.save(flush: true, failOnError: true)
                                            }
                                        }catch(Exception e)
                                        {
                                            HashMap error =new HashMap()
                                            error.put("error",e)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                    }
                                    else {
                                        HashMap error =new HashMap()
                                        error.put("error","Please enter Vendor")
                                        error.put("rowno",i+2)
                                        faillist.add(error)
                                        continue
                                    }

                                }

                                if(x.name=='date_of_purchase')
                                {
                                    if(Date_Of_Purchase!=null)
                                    {
                                        BookItem bookItem= BookItem.findByAccession_numberAndOrganization(accession_no,user.organization)
                                        if(bookItem==null){
                                            HashMap error =new HashMap()
                                            error.put("error","while editing Date Of Purchase Book Item No Found. "+ accession_no)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                        try {
                                            if(Date_Of_Purchase){
                                                if (Date_Of_Purchase && Date_Of_Purchase != 'Invalid date') {
                                                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
//                                                    Date date2 = formatter.parse(Date_of_entry);

                                                    Calendar c = Calendar.getInstance();
                                                    c.setTime(formatter.parse(Date_Of_Purchase))
                                                    c.add(Calendar.DATE, 1);  // number of days to add

                                                    bookItem.date_of_purchase = c.getTime()
                                                    bookItem.save(flush: true, failOnError: true)
                                                }
                                            }

                                        }catch(Exception e)
                                        {
                                            HashMap error =new HashMap()
                                            error.put("error",e)
                                            error.put("rowno",i+2)
                                            faillist.add(error)
                                            continue
                                        }
                                    }
                                    else {
                                        HashMap error =new HashMap()
                                        error.put("error","Please enter Date of purchase")
                                        error.put("rowno",i+2)
                                        faillist.add(error)
                                        continue
                                    }

                                }


                            }
                        }

                    }
                }

                if(count>=values.size())
                    hm.put('isdone', true)
                else
                {

                    hm.put('isdone', false)
                }


                hm.put('failList', faillist)

                hm.put("count",count==-1?0:count)
                hm.put("totalbooksize",values.size())
                hm.put("status", "200")
                fileinput.close()

            }



        }
        return hm
    }
    def bulkbookedit(request, hm) {
        println("in bulkbookedit:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String purpose = "getBookData from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList book_list = new ArrayList()
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def dataarray=['Series','New_Accession_No','LibraryDepartment','Barcode_Number',"Author","date_of_entry","title","medium","classification_no","keyword","book_price","display_order","subject",'book_type',"bill_no","vendor","date_of_purchase"]

                def temparray=[]
                for(i in dataarray)
                {
                    HashMap temp = new HashMap()
                    temp.put("name",i)
                    temp.put("status",false)
                    temparray.add(temp)
                }

                def assectionnolist=BookItem.findAllByOrganization(user.organization).sort({it.accession_number})

                HashMap mainhm = new HashMap()
                mainhm.put("tablecolumn",temparray)
                mainhm.put("assectionnolist",assectionnolist)

                hm.put("filedata",mainhm)
            }
        }
        hm.put("status","200")
        return hm
    }

    def markstusentforbookbank(request, hm, ip) {
        println "i am in getprogram"

        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        String purpose = "get markstusentforbookbank"
        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def authorlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org =user.organization


                def array=[]
                for(i in dataresponse.studentlist)
                {
                    def member=Member.findById(i.id)
                    if(i.isbookbankpolicy)
                        member.isbookbankpolicy=true
                    else
                        member.isbookbankpolicy=false
                    member.save(flush: true, failOnError: true)

                }

                hm.put("status", "200")
                // println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def getprogramtype(request, hm) {
        println("in getprogramtype:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String purpose = "getBookData from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList book_list = new ArrayList()
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization

                def programtypelist=ProgramType.findAllByOrganization(org)
//                def array=[]
//                for(i in programtypelist)
//                {
//                    HashMap hashMap=new HashMap()
//                    hashMap.put("name",i?.name)
//                    hashMap.put("id",i?.id)
//                    array.add(hashMap)
//                }
                hm.put("programtypelist",programtypelist)
            }
        }
        return hm
    }

    def getprogram(request, hm, ip) {
        println "i am in getprogram"

        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println(dataresponse)
        String purpose = "get program"
        auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def authorlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org =user.organization
                def programtype=ProgramType.findById(dataresponse.programtypeid)
                def programlist=Program.findAllByOrganizationAndProgramType(org,programtype)
//                def array=[]
//                for(i in programlist)
//                {
//                    HashMap hashMap=new HashMap()
//                    hashMap.put("name",i?.name)

//                    hashMap.put("id",i?.id)
//                    array.add(hashMap)
//                }
                println("Fsd jfjsd fjsjdf js")
                println(programlist)
                hm.put("programlist",programlist)

                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }

    def getstudent(request, hm, ip) {
        println "i am in getstudent"

        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        String purpose = "get program"
        auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def authorlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org =user.organization
                def member
                if(dataresponse.prn!=null &&  dataresponse.prn!='')
                {

                    member = Member.createCriteria().list() {

                        like("registration_number", "%"+dataresponse.prn+"%")
                        and {
                            'in'('organization', user.organization)
                        }
                    }
                }
                else{
                    def program=Program.findAllById(dataresponse.program)
                    member = Member.findAllByProgramAndOrganization(program,org)
                }
                if(member.isEmpty())
                {
                    hm.put("status", "404")
                    return hm
                }


                def array=[]
                for(i in member)
                {
                    HashMap hashMap=new HashMap()
                    hashMap.put("id",i?.id)
                    hashMap.put("name",i?.name)
                    hashMap.put("prn",i?.registration_number)
                    hashMap.put("program",i?.program?.name)
                    hashMap.put("username",i?.user?.username)
                    hashMap.put("isbookbankpolicy",i?.isbookbankpolicy)
                    println( i.isbookbankpolicy)
                    array.add(hashMap)
                }

                hm.put("studentlist",array)

                hm.put("status", "200")

                return hm
            }
        }
        return hm
    }

    def getnewacc_no(request, hm, ip) {
        println(' getnewacc_no')
        def uid = request.getHeader("EPS-uid")
        println('---------uid' + uid)

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)


        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(uid)
                def org=user.organization
                def headerArray = []
                println(" shdf jsh dfh sj dazt dataresponce")
                println(dataresponse.seriesid)
                Series series  =Series.findById(dataresponse.seriesid)
                if(series==null)
                {
                    hm.put("msg", "Series not Found")
                    return hm
                }
                long i=0;
                def bookItem1 =BookItem.findAllByOrganizationAndSeries(org,series)
                while(true) {
                    i=i+1
                    int total= bookItem1.size()+i
                    def newacc_no=series?.short_name+total
                    println(newacc_no)
                    BookItem bookItem =BookItem.findByOrganizationAndAccession_number(org,newacc_no)
                    if(bookItem==null)
                    {

                        int x=1
                        def book=Book.findAllByOrganization(org)
                        while(true) {
                            int newisbn= book.size()+x
                            Book book1=Book.findByIsbnAndOrganization(newisbn,org)
                            if(book1==null)
                            {
                                hm.put("msg", "200")
                                hm.put("acc_no", newacc_no)
                                hm.put("isbn", newisbn)
                                return hm
                                break
                            }
                            x=x+1
                        }

                    }
                }

            }
        }
        return hm
    }

    def deleteselectedbook(request, hm, ip) {
        println(' bulkdelete')
        def uid = request.getHeader("EPS-uid")
        println('---------uid' + uid)

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)

        ArrayList failList = new ArrayList()
        ArrayList successlist = new ArrayList()
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(uid)
                def headerArray = []
                println(" shdf jsh dfh sj dazt dataresponce")
                println(dataresponse.bookid)
                for(y in dataresponse.bookid){

                    String reason = 'Data is Missing'

                    HashMap temp = new HashMap()
                    BookItem bookItem
//                        try{
                    bookItem=BookItem.findByIdAndOrganization(y,user.organization)
                    if(bookItem==null)
                    {
                        HashMap error =new HashMap()

                        error.put("error","Book Item Not Found ")
                        failList.add(error)
                        continue
                    }
                    def assectionno=bookItem?.accession_number+":"+bookItem.book?.title
                    BookLending bookLending =BookLending.findByOrganizationAndBookitem(user.organization,bookItem)
                    if(bookLending!=null)
                    {
                        println("deletring book landing")
                        bookLending.delete(flush: true)
                    }
                    UserBookLog userBookLog =UserBookLog.findByBookitem(bookItem)
                    if(userBookLog!=null)
                    {
                        println("deletring bookuser log")
                        userBookLog.delete(flush: true)
                    }
                    println("deletring bookitem")
                    def book=bookItem.book
                    book.total_books=book.total_books-1
                    book.save(flush: true,failOnError: true)
                    bookItem.delete(flush:true)
                    BookItem bookItem1= BookItem.findByBookAndOrganization(book,user.organization)
                    if(bookItem1==null)
                    {
                        println("deletring book")
//                            Book book=Book.findById(bookItem1.book.id)
                        BookReservation bookReservation =BookReservation.findByBook(book)
                        if(bookReservation!=null)
                        {
                            println("deletring book reservation")
                            bookReservation.delete(flush: true)
                        }
                        book.delete(flush:true)

                    }
                    BookItem bi=BookItem.findByIdAndOrganization(y,user.organization)
                    if(bi==null)
                    {
                        successlist.add(assectionno +" Deleted Successfully.")
                    }
//                        }catch(Exception e)
//                        {
//                            HashMap error =new HashMap()
//
//                            error.put("error", bookItem?.accession_number+":"+e.toString())
//                            failList.add(error)
//                            continue
//                        }
                }
                hm.put("status", "200")
            }

            hm.put('failList', failList)
            hm.put('successlist', successlist)
        }
        return hm
    }


    def getBookItemData(request, hm, ip) {
        println("in getBookItemData in getBookData service")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        String purpose = "book item from librarian"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList book_list = new ArrayList()
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization.name
                int srno = 1

                ArrayList bookItemlist = new ArrayList()
                def book = Book.findById(dataresponse.bookId)
                HashMap bookData = new HashMap()
                bookData.put('isbn', book.isbn)
                bookData.put('title', book.title)
//                bookData.put('author',book.author)
                bookData.put('edition', book?.edition)
                bookData.put('volume', book?.volume)
                bookData.put('bookFormat', 'format')
                bookData.put('bookType', book?.booktype?.name)
                bookData.put('category', book?.bookcategory?.name)
                bookData.put('publisher', book?.publisher?.name)
                bookData.put('subscription_from', dateToStringYY(book?.subscription_from))
                bookData.put('subscription_to',dateToStringYY( book?.subscription_to))
                bookData.put('volume_to', book?.volume_to)
                bookData.put('volume_from', book?.volume_from)
                bookData.put('issue_from',book?.issue_from)
                bookData.put('issue_to',book?.issue_to)
                bookData.put('copies', book?.total_books)
                def authorArr = book?.author
                ArrayList arr = new ArrayList()
                for (author in authorArr) {
                    def authorName = Author.findById(author?.id)
                    arr.add(authorName?.name)
                }
                bookData.put('authors', arr)

                def items = BookItem.findAllByBookAndOrganization(book,user.organization)
                for (item in items) {
                    HashMap temp = new HashMap()
                    temp.put("srno", srno++)
                    temp.put("id", item?.id)
                    temp.put("access_no", item?.accession_number)
                    temp.put("acc_no", item?.accession_number)

                    temp.put("book_status", item?.bookstatus?.displayname)
                    temp.put("book_statusId", item?.bookstatus?.id)

                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    def b_date
                    def d_date

                    if (item?.borrowed_date) {
                        temp.put("borrowed_date", dateToString(item?.borrowed_date))
                        BookLending bookLending = BookLending.findByBookitemAndIsactiveAndBorrowing_date(item, true, item?.borrowed_date)
                        temp.put("member", bookLending?.user?.username)
                    }

                    if (item?.due_date)
                        temp.put("due_date", dateToString(item?.due_date))

                    if (item?.withdrawal_date)
                        temp.put("w_date", dateToString(item?.withdrawal_date))

                    temp.put("w_no", item?.withdrawal_no)

                    temp.put("periodic_entry_date", dateToString(item?.date_of_entry)=='NA' || dateToString(item?.date_of_entry)==null || dateToString(item?.date_of_entry)==''?"":dateToString(item?.date_of_entry))
                    temp.put("p_place", item?.publication_place)
                    temp.put("rackNo", item?.rack?.rack_number)
                    temp.put("rackId", item?.rack?.id)
                    temp.put("block_no", item?.rack?.block_number)
                    bookItemlist.add(temp)
                }

                ArrayList bookStatuslist = new ArrayList()
                def bookStatus = BookStatus.findAllByIsactive(true)
                for (ut in bookStatus) {
                    HashMap temp = new HashMap()
                    temp.put("bookStatus", ut.name)
                    temp.put("bookStatusIsactive", ut.isactive)
                    temp.put("book_statusId", ut.id)
                    temp.put("displayname", ut.displayname)
                    bookStatuslist.add(temp)
                }

                ArrayList rack_list = new ArrayList()
                def racks = Rack.findAllByIsactive(true)
                for (item in racks) {
                    HashMap temp = new HashMap()
                    temp.put("rackId", item.id)
                    temp.put("rackNo", item.rack_number)
                    temp.put("blockNo", item.block_number)
                    temp.put("capacity", item.capacity)
                    rack_list.add(temp)
                }

                hm.put('bookStatus', bookStatuslist)
                hm.put('bookItemlist', bookItemlist)
                hm.put('bookData', bookData)
                hm.put('racklist', rack_list)

            }//println"hm: "+hm
            return hm
        }
        return hm
    }

    def getBookassectionnumber(request, hm, ip) {
        println("in getBookItemData in getBookData service")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()


        String purpose = "librarian"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList book_list = new ArrayList()
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                def array=[]
                def BI=BookItem.findAllByOrganization(org)
                for(i in BI)
                {
                    HashMap hashMap=new HashMap()
                    hashMap.put("name",i?.accession_number+" "+i.book?.title)
                    hashMap.put("id",i.id)
                    array.add(hashMap)
                }
                hm.put("itemlist",array)

            }
            println"hm: "+hm
            return hm
        }
        return hm
    }

    def opachitsreport(request, hm) {
        println("in BookOperationService member information")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "data response ------>" + dataresponse
        def bookitemid = dataresponse["blfid"]

        String purpose = "fetch book data"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)



                def bookhistory=[]
                def bookLending1=[]
                if(dataresponse.fromdate==null && dataresponse.todate==null)
                {

                }
                else {
                    Date fromdate = new SimpleDateFormat("yyyy-MM-dd").parse(dataresponse.fromdate)
                    Date todate = new SimpleDateFormat("yyyy-MM-dd").parse(dataresponse.todate)
                    bookLending1 = OPACHits.createCriteria().list {

                        'in'("organization", user.organization)
                        and {
                            between("hitsdate", fromdate, todate)

                        }
                    }
                }

                bookLending1=bookLending1.sort({it.hitsdate})
                for( i  in bookLending1)
                {
                    HashMap hashMap =new HashMap()
                    hashMap.put("org",user?.organization?.name)
                    hashMap.put("noofhits",i.noofhits)

                    hashMap.put("date",i.hitsdate)

                    bookhistory.add(hashMap)


                }
                hm.put("bookhistory", bookhistory)
                println hm
                hm.put("msg", "200")
                return hm
            }
            return hm
        }
    }

    def getBoohistory(request, hm) {
        println("in BookOperationService member information")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "data response ------>" + dataresponse
        def bookitemid = dataresponse["blfid"]

        String purpose = "fetch book data"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def bookid = dataresponse.blfid.blfid
                def bi = BookItem.findById(bookid)
                BookLending bookLending = BookLending.findByBookitem(bi)

                def bookhistory=[]
                def bookLending1=[]
                if(dataresponse.fromdate==null && dataresponse.todate==null)
                {
                    bookLending1 = BookLending.findAllByBookitem(bi)
                }
                else {
                    Date fromdate = new SimpleDateFormat("yyyy-MM-dd").parse(dataresponse.fromdate)
                    Date todate = new SimpleDateFormat("yyyy-MM-dd").parse(dataresponse.todate)
                    bookLending1 = BookLending.createCriteria().list {
                        'in'('bookitem', bi)
                        'in'("organization", user.organization)
                        and {
                            between("borrowing_date", fromdate, todate)

                        }
                    }
                }

                bookLending1=bookLending1.sort({it.return_date})
                for( i  in bookLending1)
                {
                    HashMap hashMap =new HashMap()
                    hashMap.put("iscurrent",i.isactive)
                    hashMap.put("username",i.user.username)

                    hashMap.put("return_date",i?.return_date==null?"":i?.return_date)
                    Employee employee =Employee.findByUser(i?.user)
                    if(employee==null){
                        Member member =Member.findByUser(i?.user)
                        hashMap.put("name",member?.name)
                        hashMap.put("grno_empid",member?.registration_number)
                    }
                    else {
                        hashMap.put("name",employee?.name)
                        hashMap.put("grno_empid",employee?.employee_code)
                    }
                    bookhistory.add(hashMap)


                }
                hm.put("bookhistory", bookhistory)
                println hm
                hm.put("msg", "200")
                return hm
            }
            return hm
        }
    }

    def getissuereport(request, hm) {
        println("in BookOperationService member information")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "data response ------>" + dataresponse
        def bookitemid = dataresponse["blfid"]

        String purpose = "fetch book data"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def bi
                def in_count =0
                def out_count = 0
                def bookhistory=[]
                def bookLending1=[]
                def series
                def status
                def usertype
                def person
                def userfromtype
                if(dataresponse.fromdate==null || dataresponse.todate==null)
                {
                    bi = BookItem.findAllByOrganization(user.organization)
                    bookLending1 = BookLending.createCriteria().list {
                        'in'('bookitem', bi)
                        and {
                            'in'("organization", user.organization)
                        }
                    }
                }
                else {
                    if(dataresponse.series == "ALL"){
                        series= Series.findAllByOrganization(user.organization)
                        bi = BookItem.findAllByOrganization(user.organization)
                    }else{
                        series= Series.findById(dataresponse.series)
                        bi = BookItem.findAllBySeries(series)
                    }
                    if(dataresponse.person){
                        person= Member.findByRegistration_number(dataresponse.person)
                        if(person == null){
                            person= Employee.findByEmployee_code(dataresponse.person)
                        }
                        println("person : "+person)
                        userfromtype = person?.user
                    }else{
                        if(dataresponse.utype=="ALL"){
                            usertype=UserType.findAll()
                            userfromtype = User.findAllByOrganization(user.organization)
                        }else{
                            usertype= UserType.findById(dataresponse.utype)
                            userfromtype = User.findAllByUsertype(usertype)
                        }
                    }
                    if(dataresponse.status == "checkIn"){
                        status=false
                    }else if(dataresponse.status == "checkOut"){
                        status=true
                    }
                    println("status : "+status)
                    // println("userfromtype : "+userfromtype)
                    // println("bi : "+bi)
                    Date fromdate = new SimpleDateFormat("yyyy-MM-dd").parse(dataresponse.fromdate)
                    //Date todate = new SimpleDateFormat("yyyy-MM-dd").parse(dataresponse.todate)
                    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
                    Date date1 = formatter1.parse(dataresponse?.todate);
                    Calendar c1 = Calendar.getInstance();
                    c1.setTime(date1)
                    c1.add(Calendar.DATE, 1);
                    println("date1 : " + c1.getTime())
                    Date todate=c1.getTime()
                    bookLending1 = BookLending.createCriteria().list {
                        'in'('bookitem', bi)
                        'in'("organization", user.organization)
                        'in'("user", userfromtype)
                        and {
                            between("borrowing_date", fromdate, todate)
                            if(status == true || status == false){
                                'in'("isactive", status)
                            }
                        }
                    }

//                        if (dataresponse.status == "ALL") {
//                            println("in if")
//                            bookLending1 = BookLending.createCriteria().list {
//                                'in'('bookitem', bi)
//                                'in'("organization", user.organization)
//                                'in'("user", userfromtype)
//                                and {
//                                    between("borrowing_date", fromdate, todate)
//                                }
//                            }
//                        }
//                        else {
//                            println("in else")
//                            bookLending1 = BookLending.createCriteria().list {
//                                'in'('bookitem', bi)
//                                'in'("organization", user.organization)
//                                'in'("user", userfromtype)
//                                'in'("isactive", status)
//                                and {
//                                    between("borrowing_date", fromdate, todate)
//
//                                }
//                            }
//                        }

                }
                println("bookLending1 :"+bookLending1)
                bookLending1=bookLending1.sort({it.return_date})
                long srno=1
                for( i  in bookLending1)
                {
                    HashMap hashMap =new HashMap()
                    hashMap.put("srno",srno++)
                    hashMap.put("acc_no",i?.bookitem?.accession_number)
                    hashMap.put("author",i?.bookitem?.book?.author?.name)
                    hashMap.put("isbn_no",i?.bookitem?.book?.isbn)
                    hashMap.put("book_title",i?.bookitem?.book?.title)
                    hashMap.put("issue_date",i?.borrowing_date==null?"":i?.borrowing_date)
                    if(i.isactive == true || status == true){
                        hashMap.put("iscurrent",'OUT')
                        out_count++
                    }
                    else if(i.isactive == false || status == false) {
                        hashMap.put("iscurrent",'IN')
                        in_count++
                    }
                    hashMap.put("username",i.user.username)
                    hashMap.put("return_date",i?.return_date==null?"":i?.return_date)
                    Employee employee =Employee.findByUser(i?.user)
                    if(employee==null){
                        Member member =Member.findByUser(i?.user)
                        hashMap.put("name",member?.name)
                        hashMap.put("grno_empid",member?.registration_number)
                        hashMap.put("usertype",member?.user?.usertype?.name)
                    }
                    else {
                        hashMap.put("name",employee?.name)
                        hashMap.put("grno_empid",employee?.employee_code)
                        hashMap.put("usertype",employee?.user?.usertype?.name)
                    }
                    bookhistory.add(hashMap)
                }
                HashMap hashMap =new HashMap()
                hashMap.put("out_count","Out Count : "+out_count)
                hashMap.put("in_count","In Count : "+in_count)
                bookhistory.add(hashMap)
                hm.put("bookhistory", bookhistory)
                hm.put("out_count",out_count)
                hm.put("in_count",in_count)
                hm.put("code", "200")
                return hm
            }
            return hm
        }
    }

    def getissuebookdata(request, hm) {
        println("in getissuebookdata")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String purpose = "fetch book data"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org=user.organization
                def users = User.findAllByOrganization(user.organization)
                //def users = User.list()
                def userlist=[]
                for (u in users) {
                    def member =null
                    def name=""
                    member=Member.findByUserAndOrganization(u,u.organization)
                    if(member!=null) {
                        name = member.registration_number  +" : " + member?.name
                    }
                    if(member==null)
                    {
                        member=Employee.findByUserAndOrganization(u,u.organization)
                        if(member!=null)
                        {
                            name=member.employee_code+ " : " +member.name
                        }
                    }
                    HashMap temp = new HashMap()
                    temp.put("username", u.username)
                    temp.put("displayname", name)
                    temp.put("userid", u.id)
                    userlist.add(temp)
                }
                hm.put('userlist', userlist)
                def series_data = Series.findAllByOrganization(org)
                def utype_data=UserType.list()
                def status_data = BookStatus.list()
                // println("lists : "+userlist)
                hm.put("utype_data", utype_data)
                hm.put("series_data",series_data)
                hm.put("status_data",status_data)
                hm.put("code", "200")
                return hm
            }
            return hm
        }
    }


    def getRackBlockNo(request, hm, ip) {
        println("Inside get Rack BlockNo")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println('dataresponse---' + dataresponse)
        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def blockList = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def rackName = Rack.findById(dataresponse.rackId)
                def rack = Rack.findAllByRack_number(rackName?.rack_number)
                for (block in rack) {
                    HashMap temp = new HashMap()
                    temp.put("block_no", block?.block_number)
                    blockList.add(temp)
                }
                hm.put('blockList', blockList)

                def rackNo = Rack.findById(dataresponse.rackId)
                def rackInfo = Rack.findByRack_numberAndBlock_number(rackNo?.rack_number, dataresponse.block_no)
                if (rackInfo?.available == 0) {
                    hm.put('isRackAvailable', false)
                } else {
                    hm.put('isRackAvailable', true)
                }
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def editBookItem(request, hm, ip) {
        println("Inside editBookItem")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println('dataresponse--' + dataresponse)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def bookItem = BookItem.findById(dataresponse.bookitem_id)
                if (bookItem != null) {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");
                    def rackName = Rack.findById(dataresponse.rackId)
                    def rack = Rack.findByRack_numberAndBlock_number(rackName?.rack_number, dataresponse.block_no)
                    def previous_rackNo = bookItem?.rack?.rack_number
                    def previous_blockNo = bookItem?.rack?.block_number
                    println('------' + previous_rackNo != rackName?.rack_number)
                    println(previous_blockNo != dataresponse.block_no)

                    Rack old_rack = Rack.findByRack_numberAndBlock_number(previous_rackNo, previous_blockNo)
                    println 'old_rack===' + old_rack
                    println 'old_rack===' + old_rack != null
//                    if (old_rack != null) {
//                        if (previous_rackNo != rackName?.rack_number || previous_blockNo != dataresponse.block_no) {
//                            bookItem.rack = rack
//                            rack?.available = rack?.available - 1
//                            rack.save(flush: true, failOnError: true)
//                            println('rack saved----' + rack)
//                            old_rack?.available = old_rack?.available + 1
//                            old_rack.save(flush: true, failOnError: true)
//                        }
//                    } else {
//                        if(rack)
//                        {
//                            bookItem.rack = rack
//                            rack?.available = rack?.available - 1
//                            rack.save(flush: true, failOnError: true)
//                        }
//                    }

                    if(dataresponse.book_statusId){
                        def bookStatus = BookStatus.findById(dataresponse.book_statusId)
                        bookItem.bookstatus = bookStatus
                    }


                    if(dataresponse.w_date){
                        SimpleDateFormat formatter1 = new SimpleDateFormat("dd-mm-yyyy");
                        Date date2 = formatter1.parse(dataresponse.w_date);
                        bookItem.withdrawal_date = date2
                    }
                    if(dataresponse.w_no){
                        bookItem.withdrawal_no = dataresponse.w_no

                    }
                    if(dataresponse.p_place){
                        bookItem.publication_place = dataresponse.p_place

                    }

                    bookItem.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                } else {
                    hm.put("msg", "Book not found!!")
                }
                println "hm: " + hm
                hm.put("status", "200")
                return hm
            }
        }
    }
    def editperiodicalsItem(request, hm, ip) {
        println("Inside editBookItem")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println('dataresponse--' + dataresponse)

        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                //def organization = user.organization
                def bookItem = BookItem.findById(dataresponse.id)
                if (bookItem != null) {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");





                    if(dataresponse.book_statusId){
                        def bookStatus = BookStatus.findById(dataresponse.book_statusId)
                        bookItem.bookstatus = bookStatus
                    }


                    if(dataresponse.periodic_entry_date){
                        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
                        Date date2 = formatter1.parse(dataresponse.periodic_entry_date);
                        bookItem.date_of_entry = date2
                    }


                    bookItem.save(flush: true, failOnError: true)
                    hm.put("msg", "success")
                } else {
                    hm.put("msg", "Book not found!!")
                }
                println "hm: " + hm
                hm.put("status", "200")
                return hm
            }
        }
    }

    def getBookRackData(request, hm, ip) {
        println("in getBookRackData:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        String purpose = "book getBookRackData"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList book_list = new ArrayList()
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization.name
                int srno = 1

                ArrayList bookItemlist = new ArrayList()
                def book = Book.findById(dataresponse.bookId)
                HashMap bookData = new HashMap()
                bookData.put('isbn', book.isbn)
                bookData.put('title', book.title)
//                bookData.put('author',book.author)
                bookData.put('edition', book?.edition)
                bookData.put('volume', book?.volume)
                bookData.put('bookFormat', 'format')
                bookData.put('bookType', book?.booktype?.name)
                bookData.put('category', book?.bookcategory?.name)
                bookData.put('publisher', book?.publisher?.name)
                def bookNotHavingRack = BookItem.findAllByBookAndRackIsNull(book)
                bookData.put('bookNotHavingRack', bookNotHavingRack.size())
                bookData.put('copies', book?.total_books)
                def authorArr = book?.author
                ArrayList arr = new ArrayList()
                for (author in authorArr) {
                    def authorName = Author.findById(author?.id)
                    arr.add(authorName?.name)
                }
                bookData.put('authors', arr)

                ArrayList rackList = new ArrayList()
                def racks = Rack.findAllByIsactive(true)
                int srno_rack = 1

                for (r in racks) {
                    HashMap temp = new HashMap()
                    temp.put('srno', srno_rack++)
                    temp.put('rackNo', r?.rack_number)
                    temp.put('rackId', r?.id)
                    temp.put('blockNo', r?.block_number)
                    temp.put('locationI', r?.location_identifier)
                    temp.put('bookCat', r?.bookcategory?.name)
                    temp.put('bookCatId', r?.bookcategory?.id)
                    temp.put('capacity', r?.capacity)
                    temp.put('available', r?.available)
                    temp.put('proposed', '')
                    rackList.add(temp)
                }
                hm.put('bookData', bookData)
                hm.put('rackList', rackList)

            }//println"hm: "+hm
            return hm

        }
        return hm
    }

    def mapBookLocation(request, hm, ip) {
        println("in mapBookLocation:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        String purpose = "mapBookLocation"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {


                for (rack in dataresponse.arr) {
                    def book = Book.findById(dataresponse.bookId)
                    def bookItems = BookItem.findAllByBookAndRackIsNull(book)
                    int proposed = 0
                    Rack rackAllocated = Rack.findById(rack?.rackId)

                    for (item in bookItems) {
                        proposed = proposed + 1
                        if (Integer.parseInt(rack?.proposed) >= proposed) {
                            item.rack = rackAllocated
                            item.save(flush: true, failOnError: true)
                        }

                    }
                    rackAllocated.available = rackAllocated.available - Integer.parseInt(rack?.proposed)
                    rackAllocated.save(flush: true, failOnError: true)
                }
                hm.put('msg', '200')
            }//println"hm: "+hm
            return hm

        }
        return hm
    }

    def dateToString(Date date) {
        if(!date){
            return 'NA'
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String strDate = formatter.format(date);
        println strDate
        return strDate
    }
    def dateToStringYY(Date date) {
        if(!date){
            return ''
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = formatter.format(date);
        println strDate
        return strDate
    }

    def getA_noList(request, hm, ip) {
        println("in get accession no:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String purpose = "get accession no"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {

                def a_noList = []
                def bookItem = BookItem.findAll()
                for (bi in bookItem) {
                    HashMap temp = new HashMap()
                    temp.put('accession_number', '[ ' + bi?.accession_number + ' ]' + ' : ' + bi?.book?.title)
                    temp.put("a_no", bi?.accession_number)
                    temp.put("id", bi?.id)
                    temp.put("title", bi?.book?.title)
                    a_noList.add(temp)
                }
                hm.put('a_noList', a_noList)
                hm.put('msg', '200')
            }//println"hm: "+hm
            return hm

        }
        return hm
    }

    def getBookInfo(request, hm, ip) {
        println("in getBookData:>>>")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String purpose = "getBookData"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            String body = request.getReader().text
            def jsonSluper = new JsonSlurper()
            def dataresponse = jsonSluper.parseText(body)
            println dataresponse
            Tenants.withId(log.tenantclient.tenantid) {
                BookItem bookItem = BookItem.findByAccession_number(dataresponse.a_no)
                HashMap temp = new HashMap()
                temp.put('bookItemId', bookItem?.id)
                temp.put('title', bookItem?.book?.title)
                temp.put('category', bookItem?.book?.bookcategory?.name)
                temp.put('publisher', bookItem?.book?.publisher?.name)
                temp.put('bookType', bookItem?.book?.booktype?.name)
                temp.put('bookFormat', bookItem?.bookformat?.name)
                temp.put('price', bookItem?.price)
                temp.put('bookprice', bookItem?.book_price)
                temp.put('discountPrice', bookItem?.discount_percentage)
                temp.put('copiesAvailable', '')
                temp.put("rackNo", bookItem?.rack?.rack_number)
                temp.put("rackId", bookItem?.rack?.id)
                temp.put("rackCat", bookItem?.rack?.bookcategory?.name)
                temp.put("block_no", bookItem?.rack?.block_number)


                def authorArr = bookItem?.book?.author
                ArrayList arr = new ArrayList()
                for (author in authorArr) {
                    arr.add(author?.name)
                }
                temp.put('authors', arr)

                ArrayList rack_list = new ArrayList()
                ArrayList block_list = new ArrayList()
                def racks = Rack.findAllByIsactive(true)
                for (item in racks) {
                    HashMap temp1 = new HashMap()
                    temp1.put("rackId", item.id)
                    temp1.put("rackNo", item.rack_number)
                    temp1.put("blockNo", item.block_number)
                    temp1.put("capacity", item.capacity)
                    rack_list.add(temp1)
                    block_list.add(item.block_number)
                }

                hm.put('racklist', rack_list)
                hm.put('block_list', block_list)
                hm.put('book_info', temp)
                hm.put('msg', '200')
            }
            return hm
        }
        return hm
    }

    def addBookLocation(request, hm, ip) {
        println("in add Book Location:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        String purpose = "add Book Location"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {

                def bookItems = BookItem.findByAccession_number(dataresponse.a_no)
                def previous_rack = bookItems?.rack
                if (previous_rack) {
                    previous_rack?.available = previous_rack?.available + 1
                    previous_rack.updation_ip_address = ip
                    previous_rack.updation_date = new Date()
                    previous_rack.updation_username = uid
                    previous_rack.save(flush: true, failOnError: true)
                }
                def rack = Rack.findById(dataresponse.rackNo)
                Rack rackAllocated = Rack.findByRack_numberAndBlock_number(rack?.rack_number, dataresponse.blockNo)
                println('rackAllocated----------' + rackAllocated)
                rackAllocated?.available = rackAllocated?.available - 1
                rackAllocated.updation_ip_address = ip
                rackAllocated.updation_date = new Date()
                rackAllocated.updation_username = uid
                rackAllocated.save(flush: true, failOnError: true)

                bookItems.rack = rackAllocated
                bookItems.updation_ip_address = ip
                bookItems.updation_date = new Date()
                bookItems.updation_username = uid
                bookItems.save(flush: true, failOnError: true)
                hm.put('msg', '200')
            }//println"hm: "+hm
            return hm

        }
        return hm
    }

    def checkRackAvailability(request, hm, ip) {
        println("check Rack Availability")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println('dataresponse---' + dataresponse)
        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def blockList = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def rackNo = Rack.findById(dataresponse.rackId)
                def rackInfo = Rack.findByRack_numberAndBlock_number(rackNo?.rack_number, dataresponse.block_no)
                if (rackInfo?.available == 0) {
                    hm.put('isRackAvailable', false)
                } else {
                    hm.put('isRackAvailable', true)
                }
                println "hm: " + hm
                hm.put("msg", "200")
                return hm
            }
        }
    }

    def reports(request, ip, hm) {
        println "i am in reports"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String purpose = "Getting reports  Menu."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList routerLinkList = new ArrayList()
                int col_size = 3
                HashMap temp = new HashMap()
                temp.put('name', 'total-books')
                temp.put('textval', 'Accession Register')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'A')
                temp.put('flex', col_size)

                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'accession_register_new')
                temp.put('textval', 'Accession Register 2')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'A')
                temp.put('flex', col_size)

                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'over-due')
                temp.put('textval', 'Over Due Member Report')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'ODM')
                temp.put('flex', col_size)

                routerLinkList.add(temp)


                temp = new HashMap()
                temp.put('name', 'daily-transaction')
                temp.put('textval', 'Daily Check In/Out Register')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'DR')
                temp.put('flex', col_size)

                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'memberwise-report')
                temp.put('textval', 'Member Wise Library Utilization Report')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'MWU')
                temp.put('flex', col_size)

                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'title-report')
                temp.put('textval', 'Title Report')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'TR')
                temp.put('flex', col_size)

                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'issue-book-report')
                temp.put('textval', 'Daily Check In/Out Report')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'D')
                temp.put('flex', col_size)

                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'series-report')
                temp.put('textval', 'Series Wise Report')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'SR')
                temp.put('flex', col_size)

                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'new_arrival_report')
                temp.put('textval', 'New Arrival Report')
                temp.put('icon', 'mdi-alpha-N-circle')
                temp.put('icontext', 'N')
                temp.put('flex', col_size)
                routerLinkList.add(temp)


                temp = new HashMap()
                temp.put('name', 'opac-hits')
                temp.put('textval', 'OPAC Hit Report')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'OH')
                temp.put('flex', col_size)

                routerLinkList.add(temp)


                temp = new HashMap()
                temp.put('name', 'view-book-chart')
                temp.put('textval', 'Book Usage Statistics')
                temp.put('icon', 'mdi-alpha-e-circle')
                temp.put('icontext', 'BUS')
                temp.put('flex', col_size)

                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'barcode_report')
                temp.put('textval', 'Barcode Report')
                temp.put('icon', 'mdi-alpha-B-circle')
                temp.put('icontext', 'B')
                temp.put('flex', col_size)

                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'periodical-report')
                temp.put('textval', 'Periodical Report')
                temp.put('icon', 'mdi-alpha-B-circle')
                temp.put('icontext', 'PR')
                temp.put('flex', col_size)

                routerLinkList.add(temp)


                hm.put('routerLinkList', routerLinkList)
                temp = new HashMap()
                temp.put('name', 'barcode_report2')
                temp.put('textval', 'Barcode Report 2')
                temp.put('icon', 'mdi-alpha-B-circle')
                temp.put('icontext', 'B')
                temp.put('flex', col_size)

                routerLinkList.add(temp)


                hm.put('routerLinkList', routerLinkList)

                temp = new HashMap()
                temp.put('name', 'spine_label_report')
                temp.put('textval', 'Spine Lable Report')
                temp.put('icon', 'mdi-alpha-S-circle')
                temp.put('icontext', 'SL')
                temp.put('flex', col_size)

                routerLinkList.add(temp)


                hm.put('routerLinkList', routerLinkList)

                temp = new HashMap()
                temp.put('name', 'spine_label_report2')
                temp.put('textval', 'Spine Lable Report 2')
                temp.put('icon', 'mdi-alpha-S-circle')
                temp.put('icontext', 'SL2')
                temp.put('flex', col_size)

                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'statuswise-report')
                temp.put('textval', 'Status Wise Report')
                temp.put('icon', 'mdi-alpha-S-circle')
                temp.put('icontext', 'ST')
                temp.put('flex', col_size)

                routerLinkList.add(temp)

                temp = new HashMap()
                temp.put('name', 'mediumwise-report')
                temp.put('textval', 'Medium Wise Report')
                temp.put('icon', 'mdi-alpha-M-circle')
                temp.put('icontext', 'MW')
                temp.put('flex', col_size)

                routerLinkList.add(temp)


                hm.put('routerLinkList', routerLinkList)

                return hm

            }
        }
        return hm
    }


    def accessionReport(request, ip, hm) {
        println "i am in accession Report"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        String purpose = "Getting reports  accessionReport."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)

                def books = Book.findAll()
                def only_one = false

                ArrayList report_data = new ArrayList()
                ArrayList table_header_data = new ArrayList()
                ArrayList csv_header_data = new ArrayList()
                int srno = 1;
                for (book in books) {
                    def items = BookItem.findAllByBook(book)
                    for (bookItem in items) {
                        HashMap temp = new HashMap()
                        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                        String strDate = formatter.format(bookItem?.creation_date);

                        temp.put('srno', srno++)

                        if (dataresponse?.AccessionNo) {
                            temp.put('acc_no', bookItem?.accession_number)

                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Accession No')
                                table_header.put('value', 'acc_no')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Accession No')
                                csv_header.put('dataKey', 'acc_no')
                                csv_header_data.add(csv_header)
                            }

                        }

                        if (dataresponse?.EntryDate) {
                            temp.put('entry_date', strDate)

                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Date of Entry')
                                table_header.put('value', 'entry_date')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Date of Entry')
                                csv_header.put('dataKey', 'entry_date')
                                csv_header_data.add(csv_header)
                            }

                        }

                        if (dataresponse?.Title) {
                            temp.put('title', bookItem?.book?.title)

                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Title')
                                table_header.put('value', 'title')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Title')
                                csv_header.put('dataKey', 'title')
                                csv_header_data.add(csv_header)
                            }

                        }

                        if (dataresponse?.Edition) {
                            temp.put('edition', bookItem?.book?.edition)

                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Edition')
                                table_header.put('value', 'edition')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Edition')
                                csv_header.put('dataKey', 'edition')
                                csv_header_data.add(csv_header)
                            }

                        }

                        if (dataresponse?.Publisher) {
                            temp.put('pub_year', bookItem?.book?.publisher?.name + ',' + bookItem?.publication_year)

                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Publisher')
                                table_header.put('value', 'pub_year')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Publisher')
                                csv_header.put('dataKey', 'pub_year')
                                csv_header_data.add(csv_header)
                            }

                        }

//                        if(dataresponse.data?.AccessionNo) {
//                            temp.put('publication', bookItem?.book?.publisher?.name)
//                        }

                        if (dataresponse?.Pages) {
                            temp.put('pages', bookItem?.numberofpages)

                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Pages')
                                table_header.put('value', 'pages')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Pages')
                                csv_header.put('dataKey', 'pages')
                                csv_header_data.add(csv_header)
                            }

                        }

                        if (dataresponse?.BillNo) {
                            temp.put('bill_no', bookItem?.bill_number)

                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Bill No')
                                table_header.put('value', 'bill_no')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Bill No')
                                csv_header.put('dataKey', 'bill_no')
                                csv_header_data.add(csv_header)
                            }

                        }

                        if (dataresponse?.BookCost) {
                            temp.put('cost', bookItem?.book_price)

                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Book Cost')
                                table_header.put('value', 'cost')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Book Cost')
                                csv_header.put('dataKey', 'cost')
                                csv_header_data.add(csv_header)
                            }

                        }

                        if (dataresponse?.BookDiscount) {
                            temp.put('discount', bookItem?.discount_percentage)

                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Discount %')
                                table_header.put('value', 'discount')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Discount %')
                                csv_header.put('dataKey', 'discount')
                                csv_header_data.add(csv_header)
                            }

                        }

                        if (dataresponse?.netAmt) {
                            temp.put('purchase_price', bookItem?.price)

                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Book Net Amount')
                                table_header.put('value', 'purchase_price')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Book Net Amount')
                                csv_header.put('dataKey', 'purchase_price')
                                csv_header_data.add(csv_header)
                            }

                        }

                        if (dataresponse?.BookFormat) {
                            temp.put('book_format', bookItem?.bookformat?.name)

                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Book Format')
                                table_header.put('value', 'book_format')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Book Format')
                                csv_header.put('dataKey', 'book_format')
                                csv_header_data.add(csv_header)
                            }

                        }

                        if (dataresponse?.BookType) {
                            temp.put('book_type', book?.booktype?.name)

                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Book Type')
                                table_header.put('value', 'book_type')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Book Type')
                                csv_header.put('dataKey', 'book_type')
                                csv_header_data.add(csv_header)
                            }

                        }

                        if (dataresponse?.Category) {
                            temp.put('book_cat', book?.bookcategory?.name)

                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Book Category')
                                table_header.put('value', 'book_cat')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Book Category')
                                csv_header.put('dataKey', 'book_cat')
                                csv_header_data.add(csv_header)
                            }

                        }

                        if (dataresponse?.Volume) {
                            temp.put('volume', book?.volume)

                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Volume')
                                table_header.put('value', 'volume')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Volume')
                                csv_header.put('dataKey', 'volume')
                                csv_header_data.add(csv_header)
                            }

                        }

                        if (dataresponse?.ISBN) {
                            temp.put('isbn', book?.isbn)

                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'ISBN')
                                table_header.put('value', 'isbn')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'ISBN')
                                csv_header.put('dataKey', 'isbn')
                                csv_header_data.add(csv_header)
                            }

                        }
//                        temp.put('allcost',bookItem?.book_price+','+bookItem?.discount_percentage+'%,'+bookItem?.price)

                        if (dataresponse?.Author) {
                            def authorArr = bookItem?.book?.author
                            ArrayList arr = new ArrayList()
                            for (author in authorArr) {
                                def authorName = Author.findById(author?.id)
                                arr.add(authorName?.name)
                            }
                            temp.put('authors', arr)

                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Authors')
                                table_header.put('value', 'authors')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Authors')
                                csv_header.put('dataKey', 'authors')
                                csv_header_data.add(csv_header)
                            }

                        }
                        only_one = true
                        report_data.add(temp)
                    }

                }
                hm.put('report_data', report_data)
                hm.put('csv_header_data', csv_header_data)
                hm.put('table_header_data', table_header_data)
                return hm
            }
        }
        return hm
    }

    def overdueData(request, ip, hm) {
        println "i am in overdueData Report"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        ArrayList table_header_data = new ArrayList()
        ArrayList csv_header_data = new ArrayList()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println(" overdueData dataresponse : "+dataresponse)
        String purpose = "Getting reports  overdueData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList fetchdata = new ArrayList()
            ArrayList bl_final_list=[]
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)

//                BookCategory book_cat = BookCategory.findById(dataresponse.dept_id)
                //   Program book_cat = Program.findById(dataresponse.dept_id)
                def user_type
                if(dataresponse.user_id == 'All'){
                    user_type = UserType.findAll()
                }else{
                    user_type = UserType.findById(dataresponse.user_id)
                }
                def u = User.createCriteria()
                def userforblending = u.list(){
                    'in'('usertype', user_type)
                }
                // println("userforblending ------- : "+userforblending)

                def series
                if(dataresponse.series_id == 'All'){
                    series = Series.findAllByOrganization(user.organization)
                }else{
                    series = Series.findById(dataresponse.series_id)
                }
                def bookitemforblending = BookItem.createCriteria().list(){
                    'in'('series', series)
                }
                //println("bookitemforblending : "+bookitemforblending)
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-M-yyyy");
//                String dateInString = "15-10-2015";
                Date date1 = sdf1.parse(dataresponse.firstDate);
                Date date2 = sdf1.parse(dataresponse.lastDate);
                def bl = BookLending.createCriteria()
                def bl_data = bl.list {
                    eq("isactive", true)
                    eq("organization", user?.organization)
                    'in'("user",userforblending)
                    'in'("bookitem",bookitemforblending)
                    and {
//                        between("borrowing_date", date1,date2)
                        between("due_date", date1, date2)
                    }
                    lt("due_date", date2)

                }
                //  println('bl_data----------' + bl_data)
                println("for loop ")
                def srn=1
                for(std in bl_data){

                    UserType utM = UserType.findByName("Member")
                    UserType utE = UserType.findByName("Employee")
                    if(std?.user?.usertype.id == utM.id) {
                        if(std?.return_date != null && std?.return_date <= std?.due_date){
                            println("no fine needed")
                        }
                        else {
                            HashMap temp = new HashMap()
                            temp.put("srno",srn++)
                            temp.put("accession",std.bookitem?.accession_number)
                            temp.put("title",std.bookitem?.book?.title)
                            println("author--------  : "+std.bookitem?.book?.author)
                            temp.put("author",std.bookitem?.book?.author)
                            Member stdname = Member.findByUser(std?.user)
                            temp.put("PRN_EMP", stdname?.registration_number)
                            temp.put("name", stdname?.name)
                            temp.put("brw_date", std?.borrowing_date)
                            if (std?.return_date == null) {
                                temp.put("return_date", '-')
                            } else {
                                temp.put("return_date", std?.return_date)
                            }
                            temp.put("duedate", std?.due_date)
                            temp.put("dept", stdname?.program?.name)
                            LearnerDivision learnerclass = LearnerDivision.findByProgram(stdname?.program)
                            //def year = learnerclass?.year?.year
                            def year = learnerclass?.year?.year + " : " + learnerclass?.division
                            println("year ")
                            println("learnerclass?.year?.year : " + learnerclass?.year?.year + " learnerclass?.division :  " + learnerclass?.division)
                            if (year != 'null : null')
                                temp.put("year", year)
                            //Date nextday = std?.due_date.plusDays(1)
                            // temp.put("startdate",)
                            SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
                            int daysBetween = 0;
                            Date dateBefore = std?.due_date
                            println("dateBefore : " + dateBefore)
                            Date dateAfter
                            if (std?.return_date == null) {
                                dateAfter = new Date()
                            } else {
                                dateAfter = std?.return_date
                            }
                            // println("dateAfter : " + dateAfter)
                            long difference = dateAfter.getTime() - dateBefore.getTime();
                            // println("difference / (1000 * 60 * 60 * 24) "+difference / (1000 * 60 * 60 * 24))
                            daysBetween = Math.round(difference / (1000 * 60 * 60 * 24))
                            temp.put("late_days", daysBetween)
                            Role role = Role.findByUsertype(std?.user?.usertype)
                            LibraryPolicy policy = LibraryPolicy.findByRoleAndOrganization(role, user.organization)
                            def fine = policy.fine_rate_per_day * daysBetween
                            temp.put("fine", fine)
                            bl_final_list.add(temp)
                        }
                    }else if(std?.user?.usertype.id == utE.id){
                        if(std?.return_date != null && std?.return_date <= std?.due_date){
                            println("no fine needed")
                        }
                        else {
                            HashMap temp = new HashMap()
                            temp.put("srno",srn++)
                            temp.put("accession",std.bookitem?.accession_number)
                            temp.put("title",std.bookitem?.book?.title)
                            println("author--------  : "+std.bookitem?.book?.author)
                            temp.put("author",std.bookitem?.book?.author)
                            Employee stdname = Employee.findByUser(std?.user)
                            temp.put("PRN_EMP", stdname?.employee_code)
                            temp.put("name", stdname?.name)
                            temp.put("brw_date", std?.borrowing_date)
                            if (std?.return_date == null)
                                temp.put("return_date", '-')
                            else
                                temp.put("return_date", std?.return_date)
                            temp.put("duedate", std?.due_date)
                            temp.put("dept", stdname?.program?.name)

                            SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
                            int daysBetween = 0;
                            Date dateBefore = std?.due_date
                            //  println("dateBefore : " + dateBefore)
                            Date dateAfter
                            if (std?.return_date == null) {
                                dateAfter = new Date()
                            } else {
                                dateAfter = std?.return_date
                            }
                            // println("dateAfter : " + dateAfter)
                            long difference = dateAfter.getTime() - dateBefore.getTime();
                            // println("difference / (1000 * 60 * 60 * 24) "+difference / (1000 * 60 * 60 * 24))
                            daysBetween = (int) (difference / (1000 * 60 * 60 * 24));
                            temp.put("late_days", daysBetween)
                            def role = Role.findAllByUsertype(stdname?.user?.usertype)
                            def allroles = stdname?.user?.role
                            Role rl_Admin= Role.findByNameAndIsactive('Admin',true)
                            Role rl_Teacher= Role.findByNameAndIsactive('Teacher',true)
                            Role rl_Librarian= Role.findByNameAndIsactive('Librarian',true)
                            LibraryPolicy policy
                            if(rl_Admin.id in allroles.id){
                                policy = LibraryPolicy.findByRoleAndOrganization(rl_Admin, stdname?.user?.organization)
                                println("policy admin : "+policy)
                            }else if(rl_Librarian.id in allroles.id){
                                policy = LibraryPolicy.findByRoleAndOrganization(rl_Librarian, stdname?.user?.organization)
                                println("policy libary: "+policy)
                            }else if(rl_Teacher.id in allroles.id){
                                policy = LibraryPolicy.findByRoleAndOrganization(rl_Teacher, stdname?.user?.organization)
                                println("policy teacher : "+policy)
                            }
                            println("policy : "+policy)
                            println("policy : "+policy.fine_rate_per_day+"role  : "+role)
                            def fine = policy.fine_rate_per_day * daysBetween
                            // println("fine :"+fine)
                            temp.put("fine", fine)
                            bl_final_list.add(temp)
                        }
                    }
                }
                // println("after for loop "+bl_final_list)
//                def only_one = false
//                int srno = 1
//                for (item in bl_data) {
//                    HashMap temp = new HashMap()
////                    temp.put('srno',srno++)
//
//                    if (dataresponse?.colData?.AccessionNo) {
//                        temp.put('acc_no', item?.bookitem?.accession_number)
//                        if (only_one == false) {
//                            HashMap table_header = new HashMap()
//                            table_header.put('text', 'Accession No')
//                            table_header.put('value', 'acc_no')
//                            table_header_data.add(table_header)
//
//                            HashMap csv_header = new HashMap()
//                            csv_header.put('title', 'Accession No')
//                            csv_header.put('dataKey', 'acc_no')
//                            csv_header_data.add(csv_header)
//                        }
//                    }
//
//                    if (dataresponse?.colData?.Category) {
//                        temp.put('cat', item?.bookitem?.book?.bookcategory?.name)
//                        if (only_one == false) {
//                            HashMap table_header = new HashMap()
//                            table_header.put('text', 'Category')
//                            table_header.put('value', 'cat')
//                            table_header_data.add(table_header)
//
//                            HashMap csv_header = new HashMap()
//                            csv_header.put('title', 'Category')
//                            csv_header.put('dataKey', 'cat')
//                            csv_header_data.add(csv_header)
//                        }
//                    }
//                    if (dataresponse?.colData?.Title) {
//                        temp.put('title', item?.bookitem?.book?.title)
//                        if (only_one == false) {
//                            HashMap table_header = new HashMap()
//                            table_header.put('text', 'Title')
//                            table_header.put('value', 'title')
//                            table_header_data.add(table_header)
//
//                            HashMap csv_header = new HashMap()
//                            csv_header.put('title', 'Title')
//                            csv_header.put('dataKey', 'title')
//                            csv_header_data.add(csv_header)
//                        }
//                    }
//
//                    if (dataresponse?.colData?.Publisher) {
//                        temp.put('publisher', item?.bookitem?.book?.publisher?.name)
//                        if (only_one == false) {
//                            HashMap table_header = new HashMap()
//                            table_header.put('text', 'Publisher')
//                            table_header.put('value', 'publisher')
//                            table_header_data.add(table_header)
//
//                            HashMap csv_header = new HashMap()
//                            csv_header.put('title', 'Publisher')
//                            csv_header.put('dataKey', 'publisher')
//                            csv_header_data.add(csv_header)
//                        }
//                    }
//                    def mem
//                    UserType member = UserType.findByName('Member')
//                    UserType employee = UserType.findByName('Employee')
//                    if (item?.user?.usertype?.name == member?.name) {
//                        mem = Member.findByUser(item?.user)
//
//                        if (dataresponse?.colData?.memberName) {
//                            temp.put('member_name', mem?.name)
//                            if (only_one == false) {
//                                HashMap table_header = new HashMap()
//                                table_header.put('text', 'Name')
//                                table_header.put('value', 'member_name')
//                                table_header_data.add(table_header)
//
//                                HashMap csv_header = new HashMap()
//                                csv_header.put('title', 'Name')
//                                csv_header.put('dataKey', 'member_name')
//                                csv_header_data.add(csv_header)
//                            }
//                        }
//                    }
//                    def emp
//                    if (item?.user?.usertype?.name == employee?.name) {
//                        emp = Employee.findByUser(item?.user)
//
//                        if (dataresponse?.colData?.memberName) {
//                            temp.put('member_name', emp?.name)
//                            if (only_one == false) {
//                                HashMap table_header = new HashMap()
//                                table_header.put('text', 'Name')
//                                table_header.put('value', 'member_name')
//                                table_header_data.add(table_header)
//
//                                HashMap csv_header = new HashMap()
//                                csv_header.put('title', 'Name')
//                                csv_header.put('dataKey', 'member_name')
//                                csv_header_data.add(csv_header)
//                            }
//                        }
//                    }
////                    Employee emp = Employee.findByUser(item?.user)
////                    temp.put('member_name',emp?.name)
//                    SimpleDateFormat sdformat = new SimpleDateFormat("dd-MM-yyyy");
//                    String d1 = sdformat.format(item?.borrowing_date);
//                    String d2 = sdformat.format(item?.due_date);
//
//                    if (dataresponse?.colData?.startDate) {
//                        temp.put('startdate', d1)
//                        if (only_one == false) {
//                            HashMap table_header = new HashMap()
//                            table_header.put('text', 'Start Date')
//                            table_header.put('value', 'startdate')
//                            table_header_data.add(table_header)
//
//                            HashMap csv_header = new HashMap()
//                            csv_header.put('title', 'Start Date')
//                            csv_header.put('dataKey', 'startdate')
//                            csv_header_data.add(csv_header)
//                        }
//                    }
//
//                    if (dataresponse?.colData?.dueDate) {
//                        temp.put('duedate', d2)
//                        if (only_one == false) {
//                            HashMap table_header = new HashMap()
//                            table_header.put('text', 'Due Date')
//                            table_header.put('value', 'duedate')
//                            table_header_data.add(table_header)
//
//                            HashMap csv_header = new HashMap()
//                            csv_header.put('title', 'Due Date')
//                            csv_header.put('dataKey', 'duedate')
//                            csv_header_data.add(csv_header)
//                        }
//                    }
//
//                    def authorArr = item?.bookitem?.book?.author
//                    ArrayList arr = new ArrayList()
//                    for (author in authorArr) {
//                        def authorName = Author.findById(author?.id)
//                        arr.add(authorName?.name)
//                    }
//
//                    if (dataresponse?.colData?.AccessionNo) {
//                        temp.put('authors', arr)
//                        if (only_one == false) {
//                            HashMap table_header = new HashMap()
//                            table_header.put('text', 'Author')
//                            table_header.put('value', 'authors')
//                            table_header_data.add(table_header)
//
//                            HashMap csv_header = new HashMap()
//                            csv_header.put('title', 'Author')
//                            csv_header.put('dataKey', 'authors')
//                            csv_header_data.add(csv_header)
//                        }
//                    }
//
//
//                    def lastWeek = item?.due_date;
//                    def today = new Date()
//
//                    if (dataresponse?.colData?.lateDays) {
//                        temp.put('lateDays', daysBetween(lastWeek, today))
//                        if (only_one == false) {
//                            HashMap table_header = new HashMap()
//                            table_header.put('text', 'Late Days')
//                            table_header.put('value', 'lateDays')
//                            table_header_data.add(table_header)
//
//                            HashMap csv_header = new HashMap()
//                            csv_header.put('title', 'Late Days')
//                            csv_header.put('dataKey', 'lateDays')
//                            csv_header_data.add(csv_header)
//                        }
//                    }
//                    only_one = true
//
//                    if (item?.user?.usertype?.name == member?.name) {
//                        if (book_cat?.name == mem?.program.name && user_type?.name == item?.user?.usertype?.name) {
//                            temp.put('srno', srno++)
//                            fetchdata.add(temp)
//                        }
//                    }
//
//                    if (item?.user?.usertype?.name == employee?.name) {
//                        if (book_cat?.name == emp?.program.name && user_type?.name == item?.user?.usertype?.name) {
//                            temp.put('srno', srno++)
//                            fetchdata.add(temp)
//                        }
//                    }
//
////                    if(book_cat?.name == item?.user && user_type?.name == item?.user?.usertype?.name ){
//////                    if(book_cat?.name == item?.bookitem?.book?.bookcategory?.name && user_type?.name == item?.user?.usertype?.name ){
////                        temp.put('srno',srno++)
////                        fetchdata.add(temp)
////                    }
//                }

                // hm.put('dept', book_cat?.name)
                // hm.put('usertype', user_type?.name)

                //           }

//            hm.put('csv_header_data', csv_header_data)
//            hm.put('table_header_data', table_header_data)
//            hm.put('overdue_data', fetchdata)

                //  println("bl_final_list : "+bl_final_list)
                hm.put('bl_data', bl_final_list)
                hm.put('msg', "200")
                return hm
            }
        }
        return hm
    }

    def daysBetween(def startDate, def endDate) {
        use(groovy.time.TimeCategory) {
            def duration = endDate - startDate
            return duration.days
        }
    }

    def getdeptData(request, ip, hm) {
        println "i am in getdeptData "

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String purpose = "Getting getdeptData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)

                ArrayList dept_data = new ArrayList()
                ArrayList series_data = new ArrayList()
                ArrayList usertype_data = new ArrayList()

                def dept = LibraryDepartment.findAllByOrganization(user.organization)
                for (item in dept) {
                    HashMap temp = new HashMap()
                    temp.put('name', item?.name)
                    temp.put('id', item?.id)
                    dept_data.add(temp)
                }
                def series = Series.findAllByOrganization(user.organization)
                for (item in series) {
                    HashMap temp = new HashMap()
                    temp.put('name', item?.name)
                    temp.put('id', item?.id)
                    series_data.add(temp)
                }

                ArrayList dept_data_all =  new ArrayList()

                HashMap temp1 = new HashMap()
                temp1.put('name', 'All')
                temp1.put('id', 'All')
                dept_data_all.add(0,temp1)
                for (item in dept) {
                    HashMap temp = new HashMap()
                    temp.put('name', item?.name)
                    temp.put('id', item?.id)
                    dept_data_all.add(temp)
                }


                def usertype = UserType.findAll()
                for (item in usertype) {
                    HashMap temp = new HashMap()
                    temp.put('name', item?.name)
                    temp.put('id', item?.id)
                    usertype_data.add(temp)
                }

                hm.put('dept_data', dept_data)
                hm.put('series_data',series_data)
                hm.put('usertype_data', usertype_data)
                hm.put('dept_data_all', dept_data_all)
            }
        }
        return hm
    }

    def dailyData(request, ip, hm) {
        println "i am in dailyData Report"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

//        ArrayList table_header_data = new ArrayList()
//        ArrayList csv_header_data = new ArrayList()

        HashSet<HashMap> table_header_data = new HashSet<HashMap>()
        HashSet<HashMap> csv_header_data = new HashSet<HashMap>()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        HashSet<HashMap> set = new HashSet<HashMap>()


        String purpose = "Getting reports  dailyData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList fetchdata = new ArrayList()

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)

                BookCategory book_cat = BookCategory.findById(dataresponse.dept_id)
                UserType user_type = UserType.findById(dataresponse.user_id)
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-M-yyyy");
//                String dateInString = "15-10-2015";
                Date date1 = sdf1.parse(dataresponse.firstDate);
                Date date2 = sdf1.parse(dataresponse.lastDate);
                println('user?.organization--------'+user?.organization)
                def bl = BookLending.createCriteria()
                def bl_data = bl.list {
                    'in'("organization", user?.organization)
                    or {
                        between("borrowing_date", date1, date2)
                        between("due_date", date1, date2)
                    }
//                    lt("due_date", date2)

                }
                println('bl_data----------' + bl_data)
                def only_one = false
                int srno = 1
                def dept = Program.findAll()
//                for(item in bl_data){
                for (department in dept) {

                    HashMap temp = new HashMap()
                    int StudOutBooks = 1
                    int StudInBooks = 1
                    int StaffInBooks = 1
                    int StaffOutBooks = 1


                    for (item in bl_data) {
                        def user1
//                    for(department in dept){

                        UserType emp_ut = UserType.findByName('Employee')
                        UserType mem_ut = UserType.findByName('Member')
                        if (item?.user?.usertype.name == mem_ut?.name) {
                            user1 = Member.findByUser(item?.user)
                        } else {
                            user1 = Employee.findByUser(item?.user)
                        }

                        if (user1?.program == department) {
//                                    && !table_header_data.isEmpty()
                            if (only_one == false) {
                                temp.put('department', department?.name)

//                                HashMap table_header = new HashMap()
//                                table_header.put('text', 'Department')
//                                table_header.put('value', 'department')
//                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Department')
                                csv_header.put('dataKey', 'department')
                                csv_header_data.add(csv_header)
                            }

                            println('member---------')
                            println(dataresponse?.colData?.StaffOutBooks && item?.user?.usertype.name == mem_ut?.name)
                            if (dataresponse?.colData?.StudOutBooks && item?.user?.usertype.name == mem_ut?.name) {
                                temp.put('StudOutBooks', StudOutBooks++)
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Student Out Books')
                                table_header.put('value', 'StudOutBooks')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Student Out Books')
                                csv_header.put('dataKey', 'StudOutBooks')
                                csv_header_data.add(csv_header)
                            } else {
                                temp.put('StudOutBooks', StudOutBooks)
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Student Out Books')
                                table_header.put('value', 'StudOutBooks')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Student Out Books')
                                csv_header.put('dataKey', 'StudOutBooks')
                                csv_header_data.add(csv_header)
                            }


                            if (dataresponse?.colData?.StudInBooks && item?.user?.usertype.name == mem_ut?.name) {
                                if (item?.isactive == false) {
                                    temp.put('StudInBooks', StudInBooks++)
                                } else {
                                    temp.put('StudInBooks', 0)
                                }
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Student In Books')
                                table_header.put('value', 'StudInBooks')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Student In Books')
                                csv_header.put('dataKey', 'StudInBooks')
                                csv_header_data.add(csv_header)
                            } else if (dataresponse?.colData?.StudInBooks) {
                                temp.put('StudInBooks', 0)
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Student In Books')
                                table_header.put('value', 'StudInBooks')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Student In Books')
                                csv_header.put('dataKey', 'StudInBooks')
                                csv_header_data.add(csv_header)
                            }

                            if (dataresponse?.colData?.StaffOutBooks && item?.user?.usertype.name == emp_ut?.name) {
                                temp.put('StaffOutBooks', StaffOutBooks++)
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Staff Out Books')
                                table_header.put('value', 'StaffOutBooks')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Staff Out Books')
                                csv_header.put('dataKey', 'StaffOutBooks')
                                csv_header_data.add(csv_header)
                            } else {
                                temp.put('StaffOutBooks', 0)
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Staff Out Books')
                                table_header.put('value', 'StaffOutBooks')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Staff Out Books')
                                csv_header.put('dataKey', 'StaffOutBooks')
                                csv_header_data.add(csv_header)
                            }

                            if (dataresponse?.colData?.StaffInBooks && item?.user?.usertype.name == emp_ut?.name) {
                                if (item?.isactive == false) {
                                    temp.put('StaffInBooks', StaffInBooks++)
                                } else {
                                    temp.put('StaffInBooks', 0)
                                }
//                                if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Staff In Books')
                                table_header.put('value', 'StaffInBooks')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Staff In Books')
                                csv_header.put('dataKey', 'StaffInBooks')
                                csv_header_data.add(csv_header)
//                                }
                            } else if (dataresponse?.colData?.StaffInBooks) {
                                temp.put('StaffInBooks', 0)
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Staff In Books')
                                table_header.put('value', 'StaffInBooks')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Staff In Books')
                                csv_header.put('dataKey', 'StaffInBooks')
                                csv_header_data.add(csv_header)
                            }

//                            only_one = true
                        }

                    }
//                    if(temp.isEmpty() == false){
                    fetchdata.add(temp)
                    if (temp.isEmpty() == false) {
                        set.add(temp)
                    }

                }

            }

            for (item in set) {
                item?.totalOut = item?.StaffOutBooks + item?.StudOutBooks
                item?.totalBooks = (item?.StaffOutBooks) + (item?.StudOutBooks) + (item?.StaffInBooks) + (item?.StudInBooks)
            }
            HashMap table_header = new HashMap()
            table_header.put('text', 'Out Books')
            table_header.put('value', 'totalOut')
            table_header_data.add(table_header)

            HashMap csv_header = new HashMap()
            csv_header.put('title', 'Out Books')
            csv_header.put('dataKey', 'totalOut')
            csv_header_data.add(csv_header)


            HashMap table_header1 = new HashMap()
            table_header1.put('text', 'Total Books')
            table_header1.put('value', 'totalBooks')
            table_header_data.add(table_header1)

            HashMap csv_header1 = new HashMap()
            csv_header1.put('title', 'Total Books')
            csv_header1.put('dataKey', 'totalBooks')
            csv_header_data.add(csv_header1)

//            hm.put('csv_header_data',csv_header_data)
//            hm.put('table_header_data',table_header_data)
//            hm.put('overdue_data',fetchdata)
            hm.put('csv_header_data', csv_header_data)
            hm.put('table_header_data', table_header_data)
            hm.put('overdue_data', set)


            return hm
//            }
        }
        return hm
    }



    public static List<Date> getDaysBetweenDates(Date startdate, Date enddate)
    {
        List<Date> dates = new ArrayList<Date>();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startdate);

        while (calendar.getTime().before(enddate))
        {
            Date result = calendar.getTime();
            dates.add(result);
            calendar.add(Calendar.DATE, 1);
        }
        return dates;
    }

    def getseriesData(request, ip, hm) {
        println "i am in getdeptData "

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String purpose = "Getting getdeptData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def series_data = Series.findAllByOrganization(user.organization)
                hm.put('series_data', series_data)
                def dept_list= LibraryDepartment.findAllByOrganization(user.organization)
                hm.put('dept_list', dept_list)
                LibraryConfiguration libraryConfiguration =LibraryConfiguration.findByName("new_arrivals_book_no_of_days")
                hm.put('no_of_days', libraryConfiguration?.value)
            }
        }
        return hm
    }
    def getERPDeprtmentData(request, ip, hm) {
        println "i am in getERPDeprtmentData "

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String purpose = "Getting getERPDeprtmentData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def program = Program.findAllByOrganization(user.organization)
                hm.put('program_data', program)
                def utype_list = UserType.findAll()
                hm.put('utype_list', utype_list)
            }
        }
        return hm
    }

    def new_arrival_data(request, ip, hm) {
        println "i am in new_arrival_data Report"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

//        ArrayList table_header_data = new ArrayList()
//        ArrayList csv_header_data = new ArrayList()

        HashSet<HashMap> table_header_data = new HashSet<HashMap>()
        HashSet<HashMap> csv_header_data = new HashSet<HashMap>()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        HashSet<HashMap> set = new HashSet<HashMap>()


        String purpose = "Getting reports  dailyData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList fetchdata = new ArrayList()

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)

                BookCategory book_cat = BookCategory.findById(dataresponse.dept_id)
                UserType user_type = UserType.findById(dataresponse.user_id)
                def lib_dept
                if(dataresponse.department == 'ALL'){

                    lib_dept = LibraryDepartment.findAllByOrganization(user.organization)
                }else{
                    lib_dept= LibraryDepartment.findById(dataresponse.department)
                }
                println("lib_dept : "+lib_dept?.name)
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
//                String dateInString = "15-10-2015";
                Date date1 = sdf1.parse(dataresponse.firstDate);
                Date date2 = sdf1.parse(dataresponse.lastDate);
                def mainarray = []
                def series
                if(dataresponse.series_id=='ALL')
                    series=Series.findAllByOrganization(user.organization)
                else
                    series=Series.findById(dataresponse.series_id)

                if(dataresponse.flag)
                {
//                    def bi_data = BookItem.createCriteria().list {
//                        'in'("organization", user.organization)
//                        projections{
//                            distinct('date_of_entry')
//                        }
//                        and {
//
//                            projections{
//                                distinct('book')
//                            }
//                            'in'('series',series)
//                            between("date_of_entry", date1, date2)
//                        }
//                    }
//                    int x = 1
//
//                    for (i in bi_data) {
//                        HashMap temp=new HashMap()
//                        temp.put("srno",x++)
//                        temp.put("title",i?.title)
//                        temp.put("isbn",i?.isbn)
//                        temp.put("author",i?.author?.name)
//                        temp.put("dept",i?.librarydepartment?.name)
//                        temp.put("publisher",i?.publisher?.name)
//
//                        def bookItem = BookItem.createCriteria().list {
//                            'in'("organization", user.organization)
//
//                            and {
//                                'in'('series',series)
//                                'in'('book',i)
//                                between("date_of_entry", date1, date2)
//                            }
//                        }
////                        BookItem bookItem=BookItem.findByDate_of_entryBetweenAndBookAndOrganization(date1,date2,i,user.organization)
////                        def bookitem1=BookItem.findAllByDate_of_entryBetweenAndBookAndOrganization(date1,date2,i,user.organization)
//                        if(!bookItem.isEmpty()){
//                            temp.put("copies",bookItem.size())
//                            temp.put("price",bookItem[0].book_price)
//
//                            temp.put("total_price",bookItem.size()*bookItem[0].book_price)
//                            temp.put("date_of_entry",bookItem[0]?.date_of_entry==null?null:bookItem[0]?.date_of_entry)
//                        }
//
//                        mainarray.add(temp)
//                        hm.put("data", mainarray)
//                    }
                    println("in if ")
                    def list= getDaysBetweenDates(date1,date2)
                    int x=1
                    for(d in list){
                        Calendar calendar = new GregorianCalendar();
                        calendar.setTime(d);
                        calendar.add(Calendar.DATE, 1);
                        Date result = calendar.getTime();
                        def fromdate=d
                        def todate=result
                        def bi_data = BookItem.createCriteria().list {
                            'in'("organization", user.organization)
                            and {
                                'in'('series',series)
                                if(lib_dept != null){
                                    'in'('librarydepartment',lib_dept)
                                }
                                between("date_of_entry", fromdate, todate)
                            }
                        }
                        println("c sdjc sd sjd hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh")
//                        println(d)
                        println(fromdate)
                        println(todate)
//                        println(bi_data)
                        if(bi_data.isEmpty())
                            continue
                        else {
                            def book_data = Book.createCriteria().list {
                                'in'("organization", user.organization)
                                and {
                                    'in'('id',bi_data?.book?.id)
                                    'in'('series',series)
                                    if(lib_dept != null){
                                        'in'('librarydepartment',lib_dept)
                                    }

                                }
                            }.sort{it.title}

                            for(i in book_data){
                                HashMap temp=new HashMap()
                                temp.put("srno",x++)
                                temp.put("title",i?.title)
                                temp.put("isbn",i?.isbn)
                                temp.put("author",i?.author?.name)
                                temp.put("dept",i?.librarydepartment?.name)
                                temp.put("publisher",i?.publisher?.name)
                                def bookItem = BookItem.createCriteria().list {
                                    'in'("organization", user.organization)
                                    and {
                                        between("date_of_entry", fromdate, todate)
                                        'in'('series',series)
                                        'in'('book',i)
                                        if(lib_dept != null){
                                            'in'('librarydepartment',lib_dept)
                                        }
                                    }
                                }
                                if(!bookItem.isEmpty()){
                                    temp.put("copies",bookItem.size())
                                    temp.put("price",bookItem[0].book_price)
                                    temp.put("total_price",bookItem.size()*bookItem[0].book_price)
                                }
                                temp.put("date_of_entry",fromdate)
                                mainarray.add(temp)
                            }
                        }
                    }
                    hm.put("data", mainarray)
                }else{
                    println("in else")
                    def list= getDaysBetweenDates(date1,date2)
                    int x=1
                    for(d in list){
                        Calendar calendar = new GregorianCalendar();
                        calendar.setTime(d);
                        calendar.add(Calendar.DATE, 1);
                        Date result = calendar.getTime();
                        def fromdate=d
                        def todate=result
                        def bi_data = BookItem.createCriteria().list {
                            'in'("organization", user.organization)

                            and {
                                between("date_of_entry", fromdate, todate)
                                'in'('series', series)
                                if(lib_dept != null){
                                    'in'('librarydepartment',lib_dept)
                                }
                            }
                        }
                        for (i in bi_data) {
                            HashMap temp = new HashMap()
                            temp.put("srno", x++)
                            temp.put("title", i?.book?.title)
                            temp.put("author", i?.book?.author?.name)
                            temp.put("dept", i?.book?.librarydepartment?.name)
                            temp.put("publisher", i?.book?.publisher?.name)
                            temp.put("copies", i?.book?.total_books)
                            temp.put("acc_no", i?.accession_number)
                            temp.put("price", i?.book_price)
                            temp.put("total_price", i?.book?.total_books * i?.book_price)
                            temp.put("date_of_entry", i?.date_of_entry)
                            mainarray.add(temp)
                        }
                    }
                    mainarray = mainarray.sort({it.title})
                    hm.put("data", mainarray)
                }
                return hm
            }
            return hm
        }
    }
    def title_wise_dataold(request, ip, hm) {
        println "i am in new_arrival_data Report"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

//        ArrayList table_header_data = new ArrayList()
//        ArrayList csv_header_data = new ArrayList()

        HashSet<HashMap> table_header_data = new HashSet<HashMap>()
        HashSet<HashMap> csv_header_data = new HashSet<HashMap>()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        HashSet<HashMap> set = new HashSet<HashMap>()


        String purpose = "Getting reports  dailyData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList fetchdata = new ArrayList()

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)

                BookCategory book_cat = BookCategory.findById(dataresponse.dept_id)
                UserType user_type = UserType.findById(dataresponse.user_id)
                def lib_dept
                if(dataresponse.department == 'ALL'){

                    lib_dept = LibraryDepartment.findAllByOrganization(user.organization)
                }else{
                    lib_dept= LibraryDepartment.findById(dataresponse.department)
                }
                println("lib_dept : "+lib_dept?.name)
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
//                String dateInString = "15-10-2015";
                Date date1 = sdf1.parse(dataresponse.firstDate);
                Date date2 = sdf1.parse(dataresponse.lastDate);
                def mainarray = []
                def series
                if(dataresponse.series_id=='ALL')
                    series=Series.findAllByOrganization(user.organization)
                else
                    series=Series.findById(dataresponse.series_id)

                if(dataresponse.flag)
                {
//                    def bi_data = BookItem.createCriteria().list {
//                        'in'("organization", user.organization)
//                        projections{
//                            distinct('date_of_entry')
//                        }
//                        and {
//
//                            projections{
//                                distinct('book')
//                            }
//                            'in'('series',series)
//                            between("date_of_entry", date1, date2)
//                        }
//                    }
//                    int x = 1
//
//                    for (i in bi_data) {
//                        HashMap temp=new HashMap()
//                        temp.put("srno",x++)
//                        temp.put("title",i?.title)
//                        temp.put("isbn",i?.isbn)
//                        temp.put("author",i?.author?.name)
//                        temp.put("dept",i?.librarydepartment?.name)
//                        temp.put("publisher",i?.publisher?.name)
//
//                        def bookItem = BookItem.createCriteria().list {
//                            'in'("organization", user.organization)
//
//                            and {
//                                'in'('series',series)
//                                'in'('book',i)
//                                between("date_of_entry", date1, date2)
//                            }
//                        }
////                        BookItem bookItem=BookItem.findByDate_of_entryBetweenAndBookAndOrganization(date1,date2,i,user.organization)
////                        def bookitem1=BookItem.findAllByDate_of_entryBetweenAndBookAndOrganization(date1,date2,i,user.organization)
//                        if(!bookItem.isEmpty()){
//                            temp.put("copies",bookItem.size())
//                            temp.put("price",bookItem[0].book_price)
//
//                            temp.put("total_price",bookItem.size()*bookItem[0].book_price)
//                            temp.put("date_of_entry",bookItem[0]?.date_of_entry==null?null:bookItem[0]?.date_of_entry)
//                        }
//
//                        mainarray.add(temp)
//                        hm.put("data", mainarray)
//                    }
                    println("in if ")
                    def list= getDaysBetweenDates(date1,date2)
                    int x=1
                    for(d in list){
                        Calendar calendar = new GregorianCalendar();
                        calendar.setTime(d);
                        calendar.add(Calendar.DATE, 1);
                        Date result = calendar.getTime();
                        def fromdate=d
                        def todate=result
                        def bi_data = BookItem.createCriteria().list {
                            'in'("organization", user.organization)
                            and {
                                'in'('series',series)
                                if(lib_dept != null){
                                    'in'('librarydepartment',lib_dept)
                                }
                                between("date_of_entry", fromdate, todate)
                            }
                        }
                        println("c sdjc sd sjd hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh")
//                        println(d)
                        println(fromdate)
                        println(todate)
//                        println(bi_data)
                        if(bi_data.isEmpty())
                            continue
                        else {
                            def book_data = Book.createCriteria().list {
                                'in'("organization", user.organization)
                                and {
                                    'in'('id',bi_data?.book?.id)
                                    'in'('series',series)
                                    if(lib_dept != null){
                                        'in'('librarydepartment',lib_dept)
                                    }

                                }
                            }.sort{it.title}

                            for(i in book_data){
                                HashMap temp=new HashMap()
                                temp.put("srno",x++)
                                temp.put("title",i?.title)
                                temp.put("isbn",i?.isbn)
                                temp.put("author",i?.author?.name)
                                temp.put("dept",i?.librarydepartment?.name)
                                temp.put("publisher",i?.publisher?.name)
                                def bookItem = BookItem.createCriteria().list {
                                    'in'("organization", user.organization)
                                    and {
                                        between("date_of_entry", fromdate, todate)
                                        'in'('series',series)
                                        'in'('book',i)
                                        if(lib_dept != null){
                                            'in'('librarydepartment',lib_dept)
                                        }
                                    }
                                }
                                if(!bookItem.isEmpty()){
                                    temp.put("copies",bookItem.size())
                                    temp.put("price",bookItem[0].book_price)
                                    temp.put("total_price",bookItem.size()*bookItem[0].book_price)
                                }
                                temp.put("date_of_entry",fromdate)
                                mainarray.add(temp)
                            }
                        }
                    }
                    hm.put("data", mainarray)
                }
                else{
                    println("in else")
                    def list= getDaysBetweenDates(date1,date2)
                    int x=1
                    for(d in list){
                        Calendar calendar = new GregorianCalendar();
                        calendar.setTime(d);
                        calendar.add(Calendar.DATE, 1);
                        Date result = calendar.getTime();
                        def fromdate=d
                        def todate=result
                        def bi_data = BookItem.createCriteria().list {
                            'in'("organization", user.organization)

                            and {
                                between("date_of_entry", fromdate, todate)
                                'in'('series', series)

                                'in'('librarydepartment',lib_dept)

                            }
                        }
                        for (i in bi_data) {
                            HashMap temp = new HashMap()
                            temp.put("srno", x++)
                            temp.put("title", i?.book?.title)
                            temp.put("author", i?.book?.author?.name)
                            temp.put("dept", i?.librarydepartment?.name)
                            temp.put("publisher", i?.book?.publisher?.name)
                            temp.put("copies", i?.book?.total_books)
                            temp.put("acc_no", i?.accession_number)
                            temp.put("price", i?.book_price)
                            temp.put("total_price", i?.book?.total_books * i?.book_price)
                            temp.put("date_of_entry", i?.date_of_entry)
                            mainarray.add(temp)
                        }
                    }
                    mainarray = mainarray.sort({it.title})
                    hm.put("data", mainarray)
                }
                return hm
            }
            return hm
        }
    }
    def title_wise_data(request, ip, hm) {
        println "i am in new_arrival_data Report"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

//        ArrayList table_header_data = new ArrayList()
//        ArrayList csv_header_data = new ArrayList()

        HashSet<HashMap> table_header_data = new HashSet<HashMap>()
        HashSet<HashMap> csv_header_data = new HashSet<HashMap>()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        HashSet<HashMap> set = new HashSet<HashMap>()


        String purpose = "Getting reports  dailyData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList fetchdata = new ArrayList()

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)

                BookCategory book_cat = BookCategory.findById(dataresponse.dept_id)
                UserType user_type = UserType.findById(dataresponse.user_id)
                def lib_dept
                if(dataresponse.department == 'ALL'){

                    lib_dept = LibraryDepartment.findAllByOrganization(user.organization)
                }else{
                    lib_dept= LibraryDepartment.findById(dataresponse.department)
                }
                println("lib_dept : "+lib_dept?.name)
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
//                String dateInString = "15-10-2015";
                Date date1 = sdf1.parse(dataresponse.firstDate);
                Date date2 = sdf1.parse(dataresponse.lastDate);
                def mainarray = []
                def series
                if(dataresponse.series_id=='ALL')
                    series=Series.findAllByOrganization(user.organization)
                else
                    series=Series.findById(dataresponse.series_id)

                if(dataresponse.flag)
                {

                    println("in if ")

                    int x=1

                    def bi_data1 = Book.createCriteria().list {
                        projections{
                            distinct('title')
                        }
                        and {
                            'in'("organization", user.organization)

                        }
                    }

                    def book = Book.createCriteria().list {
                        projections {
                            groupProperty("title")
                            groupProperty("publisher")


                        }
                        and {

                            'in'("organization", user.organization)
                            'in'('title',bi_data1)

                        }
                    }
//                   book= book.unique { it.title }
                    println(book)

                    def bi_data = BookItem.createCriteria().list {
                        projections{
                            distinct('book')
                        }
                        and {
                            'in'("book", book)
                            'in'("organization", user.organization)
                            'in'('series',series)
                            if(lib_dept != null){
                                'in'('librarydepartment',lib_dept)
                            }
                            between("date_of_entry", date1, date2)
                        }
                    }


                    for(i in bi_data){
                        HashMap temp=new HashMap()
                        temp.put("srno",x++)
                        temp.put("title",i?.title)
                        temp.put("isbn",i?.isbn)
                        temp.put("author",i?.author?.name)
                        temp.put("dept",i?.librarydepartment?.name)
                        temp.put("publisher",i?.publisher?.name)
                        def bookItem = BookItem.createCriteria().list {
                            'in'("organization", user.organization)
                            and {
                                'in'('book',i)
                            }
                        }
                        if(!bookItem.isEmpty()){
                            temp.put("copies",bookItem.size())
                            temp.put("price",bookItem[0].book_price)
                            temp.put("disprice",bookItem.size()*bookItem[0].price)
                            temp.put("total_price",bookItem.size()*bookItem[0].book_price)
                        }
                        temp.put("date_of_entry",bookItem?.date_of_entry[0])
                        mainarray.add(temp)
                    }


                    hm.put("data", mainarray)
                }
                else{
                    println("in else")
                    def list= getDaysBetweenDates(date1,date2)
                    int x=1
                    for(d in list){
                        Calendar calendar = new GregorianCalendar();
                        calendar.setTime(d);
                        calendar.add(Calendar.DATE, 1);
                        Date result = calendar.getTime();
                        def fromdate=d
                        def todate=result
                        def bi_data = BookItem.createCriteria().list {
                            'in'("organization", user.organization)

                            and {
                                between("date_of_entry", fromdate, todate)
                                'in'('series', series)

                                'in'('librarydepartment',lib_dept)

                            }
                        }
                        for (i in bi_data) {
                            HashMap temp = new HashMap()
                            temp.put("srno", x++)
                            temp.put("title", i?.book?.title)
                            temp.put("author", i?.book?.author?.name)
                            temp.put("dept", i?.librarydepartment?.name)
                            temp.put("publisher", i?.book?.publisher?.name)
                            temp.put("copies", i?.book?.total_books)
                            temp.put("acc_no", i?.accession_number)
                            temp.put("price", i?.book_price)
                            temp.put("total_price", i?.book?.total_books * i?.book_price)
                            temp.put("date_of_entry", i?.date_of_entry)
                            mainarray.add(temp)
                        }
                    }
                    mainarray = mainarray.sort({it.title})
                    hm.put("data", mainarray)
                }
                return hm
            }
            return hm
        }
    }
    def member_wise_Library_util_data(request, ip, hm) {
        println "i am in new_arrival_data Report"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

//        ArrayList table_header_data = new ArrayList()
//        ArrayList csv_header_data = new ArrayList()

        HashSet<HashMap> table_header_data = new HashSet<HashMap>()
        HashSet<HashMap> csv_header_data = new HashSet<HashMap>()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        HashSet<HashMap> set = new HashSet<HashMap>()


        String purpose = "Getting reports  dailyData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList fetchdata = new ArrayList()

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)

                BookCategory book_cat = BookCategory.findById(dataresponse.dept_id)
                //   UserType user_type = UserType.findById(dataresponse.ut_id)
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
//                String dateInString = "15-10-2015";
                Date date1 = sdf1.parse(dataresponse.firstDate);
                //Date date2 = sdf1.parse(dataresponse.lastDate);
                Date daten = sdf1.parse(dataresponse?.lastDate);
                Calendar c1 = Calendar.getInstance();
                c1.setTime(daten)
                c1.add(Calendar.DATE, 1);
                println("date2 : " + c1.getTime())
                Date date2 = c1.getTime();


                def mainarray = []
                def program
                if(dataresponse.program_id=='ALL')
                    program=Program.findAllByOrganization(user.organization)
                else
                    program=Program.findById(dataresponse.program_id)

                UserType userType
                def userlist
                if(dataresponse.ut_id!='ALL') {
                    userType = UserType.findById(dataresponse.ut_id)
                    userlist = User.findAllByUsertype(userType)
                    for (prog in program) {
                        def memberlist
                        if (userType?.name == 'Employee') {
                            memberlist = Employee.createCriteria().list {
                                'in'("organization", user.organization)
                                and {
                                    'in'('program', prog)
                                    'in'('user', userlist)
                                }
                            }
                        } else {
                            memberlist = Member.createCriteria().list {
                                'in'("organization", user.organization)
                                and {
                                    'in'('program', prog)
                                    'in'('user', userlist)
                                }
                            }
                        }

                        if (memberlist) {
                            int x = 1
                            for (mem in memberlist) {
                                def book_lendingings = BookLending.createCriteria().list {
                                    'in'("organization", user.organization)
                                    and {
                                        'in'('user', mem?.user)
                                        between('borrowing_date', date1, date2)
                                    }
                                }
                                if (book_lendingings == null || book_lendingings.size() == 0)
                                    continue

                                def div = ""
                                HashMap temp = new HashMap()
                                temp.put("srno", x++)
                                temp.put("name", mem?.name)
                                temp.put("program", prog?.name)
                                if (userType?.name != 'Employee') {
                                    LearnerDivision learnerDivision = LearnerDivision.findByMemberAndOrganization(mem, user.organization)
                                    if (learnerDivision) {
                                        div = learnerDivision?.division
                                    }
                                }
                                temp.put("usertype", mem?.user?.usertype?.name)
                                temp.put("division", div)
                                temp.put("total_issue", book_lendingings.size())
                                mainarray.add(temp)
                                hm.put("data", mainarray)
                                // println("hm------------"+hm)
                            }

                        }
                    }

                }
                else {
                    def memberlist
                    for (prog in program) {
                        println("dj jfsddddddddddddddjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj")
                        println(user.organization)
                        println(prog)
                        println(userlist)
                        memberlist = Employee.createCriteria().list {
                            'in'("organization", user.organization)
                            and {
                                'in'('program', prog)
                            }
                        }

                        if (memberlist) {
                            int x = 1
                            for (mem in memberlist) {
                                def book_lendingings = BookLending.createCriteria().list {
                                    'in'("organization", user.organization)
                                    and {
                                        'in'('user', mem?.user)
                                        between('borrowing_date', date1, date2)
                                    }
                                }
                                if (book_lendingings == null || book_lendingings.size() == 0)
                                    continue
                                def div = ""
                                HashMap temp = new HashMap()
                                temp.put("srno", x++)
                                temp.put("name", mem?.name)
                                temp.put("program", prog?.name)
                                if (dataresponse.ut_id != 'ALL') {
                                    LearnerDivision learnerDivision = LearnerDivision.findByMemberAndOrganization(mem, user.organization)
                                    if (learnerDivision) {
                                        div = learnerDivision?.division
                                    }
                                }
                                temp.put("division", div)
                                temp.put("usertype", mem?.user?.usertype?.name)
                                temp.put("total_issue", book_lendingings.size())
                                mainarray.add(temp)
                            }

                        }

                        memberlist = Member.createCriteria().list {
                            'in'("organization", user.organization)
                            and {
                                'in'('program', prog)

                            }
                        }

                        if (memberlist) {
                            int x = 1
                            for (mem in memberlist) {
                                def book_lendingings = BookLending.createCriteria().list {
                                    'in'("organization", user.organization)
                                    and {
                                        'in'('user', mem?.user)
                                        between('borrowing_date', date1, date2)
                                    }
                                }
                                if (book_lendingings == null || book_lendingings.size() == 0)
                                    continue

                                def div = ""
                                HashMap temp = new HashMap()
                                temp.put("srno", x++)
                                temp.put("name", mem?.name)
                                temp.put("program", prog?.name)
                                if (userType?.name != 'Employee') {
                                    LearnerDivision learnerDivision = LearnerDivision.findByMemberAndOrganization(mem, user.organization)
                                    if (learnerDivision) {
                                        div = learnerDivision?.division
                                    }
                                }
                                temp.put("division", div)
                                temp.put("usertype", mem?.user?.usertype?.name)
                                temp.put("total_issue", book_lendingings.size())
                                mainarray.add(temp)


                            }

                        }
                    }

                    hm.put("data", mainarray)

                }

            }



            return hm

        }
        return hm
    }

    def getserieswiseaccno(request, ip, hm) {
        println "getserieswiseaccno"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

//        ArrayList table_header_data = new ArrayList()
//        ArrayList csv_header_data = new ArrayList()

        HashSet<HashMap> table_header_data = new HashSet<HashMap>()
        HashSet<HashMap> csv_header_data = new HashSet<HashMap>()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        HashSet<HashMap> set = new HashSet<HashMap>()


        String purpose = "Getting reports  dailyData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList fetchdata = new ArrayList()

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)


                def mainarray = []
                def series
                if(dataresponse.series_id=='ALL')
                    series=Series.findAllByOrganization(user.organization)
                else
                    series=Series.findById(dataresponse.series_id)
                def bi_data

                if(dataresponse.accno!="") {
                    bi_data = BookItem.createCriteria().list {
                        'in'("organization", user.organization)

                        and {
                            'in'('accession_number', dataresponse.accno)

                        }
                    }
                }
                else {
                    if(dataresponse.isrange)
                    {

                        def from
                        def to
//                        if(dataresponse.from.getClass().getSimpleName()=="String")
                        from=Long.parseLong(dataresponse.from.toString())
//                        if(dataresponse.to.getClass().getSimpleName()=="String")
                        to=Long.parseLong(dataresponse.to.toString())
//                        BookItem bookItem=BookItem.findBySeriesAndOrganization(series,user.organization)
//                        from=from+bookItem.id-1
//                        to=to+bookItem.id-1
//                        def acc_list=[]
//                        if(dataresponse.series_id=='ALL')
//                        {
//                            for (se in series)
//                                for (int y =from;y<=to;y++)
//                                    acc_list.add(se?.short_name+y)
//                        }
//                        else {
//                            for (int y =from;y<=to;y++)
//                            {
//                                if(series?.short_name==null)
//                                    acc_list.add(series?.name+y)
//                                else
//                                    acc_list.add(series?.short_name+y)
//                            }
//
//
//                        }
                        println(from)
                        println(to)


                        bi_data = BookItem.createCriteria().list {
                            'in'("organization", user.organization)
                            and {
                                'in'('series',series)
                                between("display_order", from, to)
                            }
                        }
                    }
                    else {
                        bi_data = BookItem.createCriteria().list {
                            'in'("organization", user.organization)

                            and {
                                'in'('series',series)

                            }
                        }
                    }

                }

                long x=1
                for (i in bi_data) {
                    HashMap temp=new HashMap()
                    temp.put("srno",x++)
                    temp.put("title",i?.book?.title)
                    def spine=""
                    if(i?.book?.author==null && i?.book?.author.isEmpty())
                        spine="-NA-"
                    else if(i?.book?.author[0]?.name==null )
                        spine="-NA-"
                    else if(i?.book?.author[0]?.name.size()>=3 )
                        spine=i.book?.author[0]?.name.toString().substring(0, 3)
                    else if(i?.book?.author[0]?.name.size()==2 )
                        spine=i.book?.author[0]?.name.toString().substring(0, 2)
                    else if(i?.book?.author[0]?.name.size()==1 )
                        spine=i.book?.author[0]?.name.toString().substring(0, 1)
                    else
                        spine="-NA-"
                    temp.put("spine",spine)
                    temp.put("cno",i?.book?.classificationno=="" || i?.book?.classificationno==null ?"-NA-":i?.book?.classificationno)
                    temp.put("dept",i?.book?.librarydepartment==null || i?.book?.librarydepartment?.short_name=="" || i?.book?.librarydepartment?.short_name==null ?"-NA-":i?.book?.librarydepartment?.short_name)
                    temp.put("series",i?.series?.name)
                    temp.put("acc_no",i?.accession_number)
                    temp.put("barcode_no",i?.barocde)
                    temp.put("barcode",i?.barocde)
                    mainarray.add(temp)
                    hm.put("data", mainarray)
                }

                return hm

            }
            return hm
        }
    }
    def get_acc_register(request, ip, hm) {
        println "get_acc_register"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

//        ArrayList table_header_data = new ArrayList()
//        ArrayList csv_header_data = new ArrayList()

        HashSet<HashMap> table_header_data = new HashSet<HashMap>()
        HashSet<HashMap> csv_header_data = new HashSet<HashMap>()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        HashSet<HashMap> set = new HashSet<HashMap>()


        String purpose = "Getting reports  get_acc_register."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList fetchdata = new ArrayList()

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)


                def mainarray = []
                def series
                if(dataresponse.series_id=='ALL')
                    series=Series.findAllByOrganization(user.organization)
                else
                    series=Series.findById(dataresponse.series_id)
                def bi_data

                if(dataresponse.accno!="") {
                    bi_data = BookItem.createCriteria().list {
                        'in'("organization", user.organization)

                        and {
                            'in'('accession_number', dataresponse.accno)

                        }
                    }
                }
                else {
                    if(dataresponse.isrange)
                    {

                        def from
                        def to
//                        if(dataresponse.from.getClass().getSimpleName()=="String")
                        from=Long.parseLong(dataresponse.from.toString())
//                        if(dataresponse.to.getClass().getSimpleName()=="String")
                        to=Long.parseLong(dataresponse.to.toString())
//                        BookItem bookItem=BookItem.findBySeriesAndOrganization(series,user.organization)
//                        from=from+bookItem.id-1
//                        to=to+bookItem.id-1
//                        def acc_list=[]
//                        if(dataresponse.series_id=='ALL')
//                        {
//                            for (se in series)
//                                for (int y =from;y<=to;y++)
//                                    acc_list.add(se?.short_name+y)
//                        }
//                        else {
//                            for (int y =from;y<=to;y++)
//                            {
//                                if(series?.short_name==null)
//                                    acc_list.add(series?.name+y)
//                                else
//                                    acc_list.add(series?.short_name+y)
//                            }
//
//
//                        }
                        println(from)
                        println(to)


                        bi_data = BookItem.createCriteria().list {
                            'in'("organization", user.organization)
                            and {
                                'in'('series',series)
                                between("display_order", from, to)
                            }
                        }
                    }
                    else {
                        bi_data = BookItem.createCriteria().list {
                            'in'("organization", user.organization)

                            and {
                                'in'('series',series)

                            }
                        }
                    }

                }
                if(bi_data.isEmpty())
                {
                    hm.put("msg","No Data Found.")
                    return hm
                }
                int i=0
                def orgbookdetails=[]
                if(!bi_data.isEmpty())
                    bi_data=bi_data.sort{it?.display_order}
                int maxauther=0
                for(bi in bi_data) {
                    def booksAuthors=BooksAuthors.findAllByOrganizationAndBook(user.organization,bi?.book)?.author
                    if(booksAuthors && booksAuthors.size()>maxauther)
                    {
                        maxauther=booksAuthors?.size()
                    }
                    HashMap temp=new HashMap()
                    i++
                    temp.put("srno",i)
                    int y=1
                    for(x in booksAuthors)
                    {
                        temp.put("author"+y,x?.name)
                        y=y+1
                    }
                    temp.put("orgName", bi?.organization?.name)
                    temp.put("date_of_entry", bi?.date_of_entry)
                    temp.put("bill_date", bi?.date_of_purchase?bi?.date_of_purchase:"")
                    temp.put("accession_number", bi?.accession_number)
                    temp.put("subject", bi?.subject?bi?.subject:"")
                    temp.put("isbn", bi?.book?.isbn)
                    temp.put("title", bi?.book?.title)
                    temp.put("author", bi?.book?.author?.name)
                    temp.put("edition", bi?.book?.edition)
                    temp.put("medium", bi?.book?.language?.name)
                    temp.put("bookformat", bi?.bookformat?.name)
                    temp.put("booktype", bi?.book?.booktype?.name)
                    temp.put("bookcategory", bi?.book?.bookcategory?.name)
                    temp.put("publisher", bi?.book?.publisher?.name)
                    temp.put("Series", bi?.series?.name)
                    temp.put("actualprice", bi?.book_price)
                    temp.put("price", bi?.price)
                    temp.put("discount", bi?.discount_percentage)
                    temp.put("publisher_year", bi?.publication_year)
                    temp.put("localtion", bi?.location)
                    temp.put("billno", bi?.bill_number)
                    temp.put("cno", bi?.book?.classificationno)
                    temp.put("dept", bi?.librarydepartment?.name)
                    temp.put("pages", bi?.numberofpages)
                    temp.put("barcode", bi?.barocde)
                    temp.put("remark", bi?.remarks)
                    temp.put("vendor", bi?.vendor_name)
                    temp.put("status", bi?.bookstatus?.name)
                    orgbookdetails.add(temp)
                }
                //}
                hm.put("orgbookdetails",orgbookdetails)

                return hm

            }
            return hm
        }
    }

    def dailyDatanew(request, ip, hm) {
        println "i am in dailyDatanew"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

//        ArrayList table_header_data = new ArrayList()
//        ArrayList csv_header_data = new ArrayList()

        HashSet<HashMap> table_header_data = new HashSet<HashMap>()
        HashSet<HashMap> csv_header_data = new HashSet<HashMap>()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        HashSet<HashMap> set = new HashSet<HashMap>()


        String purpose = "Getting reports  dailyData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList fetchdata = new ArrayList()

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)

                BookCategory book_cat = BookCategory.findById(dataresponse.dept_id)
                UserType user_type = UserType.findById(dataresponse.user_id)
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
                // SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
                Date daten = sdf1.parse(dataresponse?.lastDate);
                Calendar c1 = Calendar.getInstance();
                c1.setTime(daten)
                c1.add(Calendar.DATE, 1);
                println("daten : " + c1.getTime())
//                String dateInString = "15-10-2015";
                Date date1 = sdf1.parse(dataresponse.firstDate);
                Date date2 = c1.getTime();

                println('user?.organization--------'+user?.organization)
                UserType emp_ut = UserType.findByName('Employee')
                def emp=User.findAllByOrganizationAndUsertype(user?.organization,emp_ut)
                UserType mem_ut = UserType.findByName('Member')
                def member=User.findAllByOrganizationAndUsertype(user?.organization,mem_ut)

                def mainarray=[]
                def list= getDaysBetweenDates(date1,date2)
                def series
                if(dataresponse.series_id == "ALL"){
                    series = Series.findAllByOrganizationAndIsactive(user?.organization,true)
                }else{
                    series = Series.findById(dataresponse.series_id)
                }
                println("series  : "+series)
                for(s in series) {
                    int x=1
                    println("S : " + s)
                    def bitem = []
                    bitem = BookItem.createCriteria().list {
                        'in'("series", s)
                    }
                    println("bitem : "+bitem.isEmpty())
                    HashMap t = new HashMap()
                    t.put("total_issue", "")
                    t.put("total_return", "")
                    t.put("total_issue_member", "")
                    t.put("total_issue_emp", "")
                    t.put("total_return_member", "")
                    t.put("total_return_emp", "")
                    t.put("date", "")
                    t.put("srno", s?.name)
                    mainarray.add(t)
                    for (i in list) {
                        Calendar calendar = new GregorianCalendar();
                        calendar.setTime(i);
                        calendar.add(Calendar.DATE, 1);
                        Date result = calendar.getTime();
                        println("result")
                        println(result)
                        println(i)
                        def fromdate = i
                        def todate = result
//                    todate.setHours(23);
//                    todate.setMinutes(59);
//                    todate.setSeconds(59);
//                    fromdate.setHours(0);
//                    fromdate.setMinutes(0);
//                    fromdate.setSeconds(0);

                        HashMap temp = new HashMap()

                        def issuebook = BookLending.createCriteria().list {
                            'in'("organization", user?.organization)
                            and {
                                between('borrowing_date', fromdate, todate)
                                if(bitem != null || bitem.isEmpty() == false){
                                    'in'("bookitem", bitem)
                                }
                            }
                        }

                        def returnbook = BookLending.createCriteria().list {
                            'in'("organization", user?.organization)
                            and {
                                between('return_date', fromdate, todate)
                                if(bitem != null ||  bitem.isEmpty() == false){
                                    'in'("bookitem", bitem)
                                }
                            }
                        }

                        def memberissue = BookLending.createCriteria().list {
                            'in'("organization", user?.organization)
                            and {
                                between('borrowing_date', fromdate, todate)
                                'in'("user", member)
                                if(bitem != null || bitem.isEmpty() == false){
                                    'in'("bookitem", bitem)
                                }
                            }
                        }

                        def memberretuen = BookLending.createCriteria().list {
                            'in'("organization", user?.organization)
                            and {
                                between('return_date', fromdate, todate)
                                'in'("user", member)
                                if(bitem != null || bitem.isEmpty() == false){
                                    'in'("bookitem", bitem)
                                }
                            }
                        }

                        def empissue = BookLending.createCriteria().list {
                            'in'("organization", user?.organization)
                            and {
                                between('borrowing_date', fromdate, todate)
                                'in'("user", emp)
                                if(bitem != null || bitem.isEmpty() == false){
                                    'in'("bookitem", bitem)
                                }
                            }
                        }

                        def empretuen = BookLending.createCriteria().list {
                            'in'("organization", user?.organization)
                            and {
                                between('return_date', fromdate, todate)
                                'in'("user", emp)
                                if(bitem != null || bitem.isEmpty() == false){
                                    'in'("bookitem", bitem)
                                }
                            }
                        }
                        if(empretuen?.size() !=0 || empissue?.size() != 0 || memberretuen?.size() != 0 ||
                                memberissue?.size() != 0  || returnbook?.size() != 0 || issuebook?.size() != 0){
                            temp.put("total_issue_member", memberissue?.size())
                            temp.put("total_issue_emp", empissue?.size())
                            temp.put("total_return_emp", empretuen?.size())
                            temp.put("total_return_member", memberretuen?.size())
                            temp.put("total_member", memberretuen?.size()+memberissue?.size())
                            temp.put("total_emp", empretuen?.size()+empissue?.size())
                            temp.put("total_return", returnbook?.size())
                            temp.put("total_issue", issuebook?.size())
                            temp.put("total_count", issuebook?.size()+returnbook?.size())
                            temp.put("date", result)
                            temp.put("series_id", s.id)
                            temp.put("fromDate", i)
                            temp.put("toDate", result)
                            temp.put("srno", x++)
                            mainarray.add(temp)
                        }
                    }
                }
                // println("mainarray : "+mainarray)
                hm.put("data",mainarray)
            }

            return hm

        }
        return hm
    }
    def getmembers(request,ip, hm){
        println "i am in getmembers"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

//        ArrayList table_header_data = new ArrayList()
//        ArrayList csv_header_data = new ArrayList()
        HashSet<HashMap> table_header_data = new HashSet<HashMap>()
        HashSet<HashMap> csv_header_data = new HashSet<HashMap>()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse-------"+dataresponse
        HashSet<HashMap> set = new HashSet<HashMap>()

        String purpose = "Getting reports  dailyData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList fetchdata = new ArrayList()

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def utype
                if(dataresponse.utype == "employee"){
                    utype = UserType.findByName('Employee')
                }else if(dataresponse.utype == "student"){
                    utype = UserType.findByName('Member')
                }else if(dataresponse.utype == "all"){
                    utype = UserType.findAll()
                }
                println("utype : "+utype?.name)
                Date date = new SimpleDateFormat("yyyy-MM-dd").parse(dataresponse.item.date)
                Date todate = new SimpleDateFormat("yyyy-MM-dd").parse(dataresponse.item.date)
//                def todate = date.next().previous()

                todate.setHours(23)
                todate.setMinutes(59)
                todate.setSeconds(59)
                //SimpleDateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
                //Date date = formatter.parse(dataresponse.item.date)
                println("date ------- "+date)
                println("todate ------- "+todate)
                def users =User.createCriteria().list {
                    'in'("organization", user?.organization)
                    'in'("usertype",utype)
                }
                Series series = Series.findById(dataresponse.item.series_id)
                println("series :"+series?.name)
                def bookitem=BookItem.findAllBySeries(series)
                def booklending_list
                def mainarray=[]
                if(dataresponse.status=="issue" ){
                    println("in if issuw")
                    booklending_list = BookLending.createCriteria().list {
                        'in'("organization", user?.organization)
                        and {
                            between('borrowing_date', date, todate)
                            'in'("user", users)
                            'in'("bookitem", bookitem)
                        }
                    }
                }else if(dataresponse.status=="return"){
                    println("in els if retuen")
                    booklending_list = BookLending.createCriteria().list {
                        'in'("organization", user?.organization)
                        and {
                            between('return_date', date, todate)
                            'in'("user", users)
                            'in'("bookitem", bookitem)
                        }
                    }
                }else if(dataresponse.status=="all"){
                    println("else if all")
                    booklending_list = BookLending.createCriteria().list {
                        'in'("organization", user?.organization)
                        or{
                            between('return_date', date, todate)
                            between('borrowing_date', date, todate)
                        }
                        and {
                            'in'("user", users)
                            'in'("bookitem", bookitem)
                        }
                    }
                }
                def srno=1
                println("booklending_list : "+booklending_list)
                for(i in booklending_list){
                    HashMap temp = new HashMap()
                    temp.put("srno",srno++)
                    temp.put("acc_no",i.bookitem?.accession_number)
                    temp.put("title",i.bookitem?.book?.title)
                    def authorArr = i?.bookitem?.book?.author
                    ArrayList arr = new ArrayList()
                    for(author in authorArr){
                        arr.add(author?.name)
                    }
                    temp.put('authors',arr)
                    temp.put("publisher",i.bookitem?.book?.publisher?.name)
                    Employee e= Employee.findByUser(i?.user)
                    if(e== null){
                        Member m = Member.findByUser(i?.user)
                        temp.put("member_name",m?.name)
                    }else{
                        temp.put("member_name",e?.name)
                    }
                    mainarray.add(temp)
                }
                hm.put("data",mainarray)
                hm.put("code","200")
                println("hm----------"+hm)
            }
            return hm
        }
        return hm
    }

    def memberWise(request, ip, hm) {
        println "i am in memberWise Report"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

//        ArrayList table_header_data = new ArrayList()
//        ArrayList csv_header_data = new ArrayList()

        HashSet<HashMap> table_header_data = new HashSet<HashMap>()
        HashSet<HashMap> csv_header_data = new HashSet<HashMap>()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        String purpose = "Getting reports  memberWise."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
//            ArrayList fetchdata = new ArrayList()
            HashSet<HashMap> fetchdata = new HashSet<HashMap>()
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)

                Program book_cat = Program.findById(dataresponse.dept_id)
                UserType user_type = UserType.findById(dataresponse.user_id)
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-M-yyyy");
//                String dateInString = "15-10-2015";
                Date date1 = sdf1.parse(dataresponse.firstDate);
                Date date2 = sdf1.parse(dataresponse.lastDate);
//                def bl = BookLending.createCriteria()
//                def bl_data = bl.list{
////                    eq("isactive", true)
//                    and {
//                        between("borrowing_date", date1,date2)
//                        between("due_date", date1,date2)
//                    }
//                }
                def only_one = false


                UserType ut = UserType.findById(dataresponse.user_id)
                def useradata = User.findAllByUsertypeAndOrganization(ut,user?.organization)

                for (usr in useradata) {
                    println('----------usr' + usr)
                    def bl = BookLending.createCriteria()

                    def bl_data = bl.list {
                        eq("user", usr)
                        and {
                            between("borrowing_date", date1, date2)
                            between("due_date", date1, date2)
                        }
                    }
                    int issueCount = 0
                    int index = 0

                    for (item in bl_data) {
//        int srno = 1

                        index++
                        HashMap temp = new HashMap()
//                    temp.put('srno',srno++)
                        def user_data
                        UserType member = UserType.findByName('Member')
                        UserType employee = UserType.findByName('Employee')
                        if (item?.user?.usertype?.name == member?.name) {
                            Member mem = Member.findByUser(item?.user)
                            user_data = mem
                            if (dataresponse?.colData?.memberName) {
                                temp.put('member_name', mem?.name)
                                if (only_one == false) {
                                    HashMap table_header = new HashMap()
                                    table_header.put('text', 'Name')
                                    table_header.put('value', 'member_name')
                                    table_header_data.add(table_header)

                                    HashMap csv_header = new HashMap()
                                    csv_header.put('title', 'Name')
                                    csv_header.put('dataKey', 'member_name')
                                    csv_header_data.add(csv_header)
                                }
                            }
                        }

                        if (item?.user?.usertype?.name == employee?.name) {
                            Employee emp = Employee.findByUser(item?.user)
                            user_data = emp
                            if (dataresponse?.colData?.memberName) {
                                temp.put('member_name', emp?.name)
                                if (only_one == false) {
                                    HashMap table_header = new HashMap()
                                    table_header.put('text', 'Name')
                                    table_header.put('value', 'member_name')
                                    table_header_data.add(table_header)

                                    HashMap csv_header = new HashMap()
                                    csv_header.put('title', 'Name')
                                    csv_header.put('dataKey', 'member_name')
                                    csv_header_data.add(csv_header)
                                }
                            }
                        }
                        if (dataresponse?.colData?.outDate) {
                            def outDate = dateToString(item?.borrowing_date)
                            temp.put('outDate', outDate)
                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'Out Date')
                                table_header.put('value', 'outDate')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'Out Date')
                                csv_header.put('dataKey', 'outDate')
                                csv_header_data.add(csv_header)
                            }
                        }

                        int count = issueCount++
                        if (dataresponse?.colData?.bookIssuedCount) {
                            temp.put('bookIssuedCount', count + 1)
                            if (only_one == false) {
                                HashMap table_header = new HashMap()
                                table_header.put('text', 'No.of Times Books Issued')
                                table_header.put('value', 'bookIssuedCount')
                                table_header_data.add(table_header)

                                HashMap csv_header = new HashMap()
                                csv_header.put('title', 'No.of Times Books Issued')
                                csv_header.put('dataKey', 'bookIssuedCount')
                                csv_header_data.add(csv_header)
                            }
                        }

                        only_one = true
                        Program pg = Program.findById(dataresponse.dept_id)


                        if (bl_data.size() == index && user_data?.program?.name == pg?.name) {
                            temp.put('srno', fetchdata.size() + 1)
                            fetchdata.add(temp)
                        }
//                }
                    }
                    hm.put('dept', book_cat?.name)
                    hm.put('usertype', user_type?.name)

                }

                hm.put('csv_header_data', csv_header_data)
                hm.put('table_header_data', table_header_data)
                hm.put('overdue_data', fetchdata)
                return hm
            }
        }
        return hm
    }

    def titleReport(request, ip, hm) {
        println "i am in titleReport Report"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        HashSet<HashMap> table_header_data = new HashSet<HashMap>()
        HashSet<HashMap> csv_header_data = new HashSet<HashMap>()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-M-yyyy");
        Date date1 = sdf1.parse(dataresponse.firstDate);
        Date date2 = sdf1.parse(dataresponse.lastDate);
        String purpose = "Getting reports  titleReport."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
//            ArrayList fetchdata = new ArrayList()
            HashSet<HashMap> fetchdata = new HashSet<HashMap>()
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def only_one = false
                def department


                if (dataresponse.dept_id != 'All') {
                    int allPrice = 0
                    int volume = 0
                    department = Program.findById(dataresponse.dept_id)
//                    def books = Book.findAllByProgram(department)
                    def b = Book.createCriteria()
                    def books = b.list {
                        eq("organization",user?.organization )
                        eq("program", department)
                        and {
                            between("creation_date", date1, date2)
                        }
                    }


                    for (book in books) {
                        int book_price = 0
                        def bi = BookItem.findAllByBook(book)
                        if(bi[0]?.book_price)
                            book_price = bi[0]?.book_price

                        if(book_price && bi.size() && allPrice)
                            allPrice = allPrice + book_price * bi.size()
                        volume = volume + bi.size()
                    }
                    HashMap temp = new HashMap()

                    if (dataresponse?.colData?.dept) {
                        temp.put('dept', department?.name)
//                        if(only_one == false){
                        HashMap table_header = new HashMap()
                        table_header.put('text', 'Department')
                        table_header.put('value', 'dept')
                        table_header_data.add(table_header)

                        HashMap csv_header = new HashMap()
                        csv_header.put('title', 'Department')
                        csv_header.put('dataKey', 'dept')
                        csv_header_data.add(csv_header)
//                        }
                    }


                    if (dataresponse?.colData?.title) {
                        temp.put('title', books.size())
//                        if(only_one == false){
                        HashMap table_header = new HashMap()
                        table_header.put('text', 'Title')
                        table_header.put('value', 'title')
                        table_header_data.add(table_header)

                        HashMap csv_header = new HashMap()
                        csv_header.put('title', 'Title')
                        csv_header.put('dataKey', 'title')
                        csv_header_data.add(csv_header)
//                        }
                    }

                    if (dataresponse?.colData?.volume) {
                        temp.put('volume', volume)
//                        if(only_one == false){
                        HashMap table_header = new HashMap()
                        table_header.put('text', 'Volume')
                        table_header.put('value', 'volume')
                        table_header_data.add(table_header)

                        HashMap csv_header = new HashMap()
                        csv_header.put('title', 'Volume')
                        csv_header.put('dataKey', 'volume')
                        csv_header_data.add(csv_header)
//                        }
                    }

                    if (dataresponse?.colData?.price) {
                        temp.put('allPrice', allPrice)
//                        if(only_one == false){
                        HashMap table_header = new HashMap()
                        table_header.put('text', 'Price')
                        table_header.put('value', 'allPrice')
                        table_header_data.add(table_header)

                        HashMap csv_header = new HashMap()
                        csv_header.put('title', 'Price')
                        csv_header.put('dataKey', 'allPrice')
                        csv_header_data.add(csv_header)
//                        }
                    }
                    println('temp-----------' + temp)
                    fetchdata.add(temp)
                }else {
                    department = Program.findAll()

                    for(dept in department){
                        int allPrice = 0
                        int volume = 0
//                        def books = Book.findAllByProgram(dept)
                        println('user?.organization==========='+user?.organization)
                        def b = Book.createCriteria()
                        def books = b.list {
                            eq("program", dept)
                            eq("organization", user?.organization)
                            and {
                                between("creation_date", date1, date2)
                            }
                        }
                        for (book in books) {
                            int book_price = 0
                            def bi = BookItem.findAllByBook(book)
                            if(bi[0]?.book_price)
                                book_price = bi[0]?.book_price
                            if(book_price && bi.size() && allPrice)
                                allPrice = allPrice + book_price * bi.size()
                            volume = volume + bi.size()
                        }
                        HashMap temp = new HashMap()

                        if (dataresponse?.colData?.dept) {
                            temp.put('dept', dept?.name)
//                        if(only_one == false){
                            HashMap table_header = new HashMap()
                            table_header.put('text', 'Department')
                            table_header.put('value', 'dept')
                            table_header_data.add(table_header)

                            HashMap csv_header = new HashMap()
                            csv_header.put('title', 'Department')
                            csv_header.put('dataKey', 'dept')
                            csv_header_data.add(csv_header)
//                        }
                        }


                        if (dataresponse?.colData?.title) {
                            temp.put('title', books.size())
//                        if(only_one == false){
                            HashMap table_header = new HashMap()
                            table_header.put('text', 'Title')
                            table_header.put('value', 'title')
                            table_header_data.add(table_header)

                            HashMap csv_header = new HashMap()
                            csv_header.put('title', 'Title')
                            csv_header.put('dataKey', 'title')
                            csv_header_data.add(csv_header)
//                        }
                        }

                        if (dataresponse?.colData?.volume) {
                            temp.put('volume', volume)
//                        if(only_one == false){
                            HashMap table_header = new HashMap()
                            table_header.put('text', 'Volume')
                            table_header.put('value', 'volume')
                            table_header_data.add(table_header)

                            HashMap csv_header = new HashMap()
                            csv_header.put('title', 'Volume')
                            csv_header.put('dataKey', 'volume')
                            csv_header_data.add(csv_header)
//                        }
                        }

                        if (dataresponse?.colData?.price) {
                            temp.put('allPrice', allPrice)
//                        if(only_one == false){
                            HashMap table_header = new HashMap()
                            table_header.put('text', 'Price')
                            table_header.put('value', 'allPrice')
                            table_header_data.add(table_header)

                            HashMap csv_header = new HashMap()
                            csv_header.put('title', 'Price')
                            csv_header.put('dataKey', 'allPrice')
                            csv_header_data.add(csv_header)
//                        }
                        }
                        println('temp-----------' + temp)
                        fetchdata.add(temp)
                    }
                }

                println("fetchdata : "+fetchdata)

                hm.put('csv_header_data', csv_header_data)
                hm.put('table_header_data', table_header_data)
                hm.put('titlereport_data', fetchdata)
                return hm
            }
            return hm
        }
    }

    def getFineRecord(request, hm, ip) {
        println("in  getFineRecord :")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        String purpose = "getFineRecord"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {

                User usr = User.findById(dataresponse?.user)

                def usrbooks1 = BookLending.findAllByUser(usr)
                println('usrbooks1-------'+usrbooks1)
                ArrayList fineRecord = new ArrayList()
                for(item in usrbooks1){
                    HashMap finedata =  new HashMap()
                    def fine_transaction = FineTransaction.findByBooklendingAndIsactive(item,true)
                    if(fine_transaction){
                        def fine_transaction1 = FineTransaction.findByBooklendingAndIsactive(item,false)
                        finedata.put('title',item?.bookitem?.book?.title)
                        finedata.put('acc_no',item?.bookitem?.accession_number)
                        finedata.put('paid_amt',fine_transaction1?.amount)
                        finedata.put('unpaid_amt',fine_transaction?.amount)
                        finedata.put('bookLId',item?.id)

                        fineRecord.add(finedata)
                    }else {
                        if(item?.fine_amount > 0){
                            HashMap finedata1=  new HashMap()

                            finedata1.put('title',item?.bookitem?.book?.title)
                            finedata1.put('acc_no',item?.bookitem?.accession_number)
                            finedata1.put('paid_amt','-')
                            finedata1.put('unpaid_amt',item?.fine_amount)
                            finedata1.put('bookLId',item?.id)

                            fineRecord.add(finedata1)

                        }
                    }
                }

                hm.put('fineRecord', fineRecord)
                hm.put('msg', '200')
            }//println"hm: "+hm
            return hm

        }
        return hm
    }

    def payFineData(request, hm,ip) {
        println("in payFineData::")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        String purpose = "payFineData"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(uid)

                BookLending bl = BookLending.findById(dataresponse.bookLId)
                HashMap temp = new HashMap()
                println('bl--------'+bl)
                if(bl?.user?.usertype?.name == "Member"){
                    println('in member')
                    Member member = Member.findByUser(bl?.user)
                    temp.put('memberName',member?.name)
                    temp.put('memberCode',member?.registration_number)
                }

                if(bl?.user?.usertype?.name == "Employee"){
                    println('in employee')

                    Employee employee = Employee.findByUser(bl?.user)
                    temp.put('memberName',employee?.name)
                    temp.put('memberCode',employee?.employee_code)
                }
                Date today = new Date()

                temp.put('title',bl?.bookitem?.book?.title)
                temp.put('checkOut_date',dateToString(bl?.borrowing_date));
                temp.put('due_date',dateToString(bl?.due_date));
                temp.put('return_date',dateToString(today));

                def transaction_type = TransactionType.findAllByIsactive(true)
                ArrayList arr = new ArrayList()
                for(item in transaction_type){
                    HashMap data = new HashMap()
                    data.put('name',item?.name)
                    data.put('id',item?.id)
                    arr.add(data)
                }

                hm.put('transactionType_data',arr)
                hm.put('data',temp)
                hm.put('msg','200')
            }
            return hm

        }
        return hm
    }

    def getBookData1(request, hm,ip) {
        println("in getBookData1:>>>")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String purpose = "getBookData1"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            String body = request.getReader().text
            def jsonSluper = new JsonSlurper()
            def dataresponse = jsonSluper.parseText(body)
            println dataresponse
            Tenants.withId(log.tenantclient.tenantid) {
                //BookItem bookItem = BookItem.findByAccession_number(dataresponse.a_no)
                User loged_user = User.findByUsername(log.username)
                BookItem bookItem = BookItem.findByAccession_numberAndOrganization(dataresponse.a_no,loged_user.organization)
                HashMap temp = new HashMap()
                temp.put('bookItemId',bookItem?.id)
                temp.put('title',bookItem?.book?.title)
                temp.put('category',bookItem?.book?.bookcategory?.name)
                temp.put('publisher',bookItem?.book?.publisher?.name)
                temp.put('bookType',bookItem?.book?.booktype?.name)
                temp.put('bookFormat',bookItem?.bookformat?.name)
                temp.put('price',bookItem?.price)
                temp.put('copiesAvailable','')
                int conut
                boolean renewbtn
                BookLending bookL = BookLending.findByBookitemAndIsactive(bookItem,true)
                LibraryConfiguration lc=LibraryConfiguration.findByNameAndIsactive("RENEW",true)
                UserBookLog userBookLog=UserBookLog.findByBookitem(bookItem)
                if (userBookLog){
                    conut=userBookLog.renew_counter
                }
                println("conut >>>:"+conut)
                println("lc?.value ::"+lc?.value)
                int lc1=Integer.parseInt(lc?.value)
                if (conut == lc1) {
                    renewbtn = true
                    temp.put("renewbtn", renewbtn)
                } else {
                    renewbtn = false
                    temp.put("renewbtn", renewbtn)
                }
                SimpleDateFormat sdformat = new SimpleDateFormat("dd-MM-yyyy");
                Date d1 = sdformat.parse(dateToString(bookL?.due_date));
                Date d2 = sdformat.parse(dateToString(new Date()));
                temp.put('bookLId',bookL?.id)

                FineTransaction FT = FineTransaction.findByBooklending(bookL)
//                println('ft============'+FT)
//                println(FT == null)
//                println(FT)
//                println(FT == 'null')

//                if(d1.compareTo(d2) > 0 || FT != null)
//                {
//                    temp.put("fine", "NA")
//                }
//                else{
//                    LibraryPolicy libraryPolicy
//                    if(bookL?.user.role.size()>=1)
//                    {
//                        libraryPolicy = LibraryPolicy.findByRole(bookL?.user.role[0])
//                    }
//                    long difference_In_Time =  d2.getTime() - d1.getTime();
//                    long difference_In_Days = (difference_In_Time/ (1000 * 60 * 60 * 24))
//                    long fine = libraryPolicy.fine_rate_per_day * difference_In_Days
//                    bookL.fine_amount = fine
//                    bookL.save(flush:true,failOnError: true)
//                    temp.put("fine",fine)
//                }
                if(bookL?.due_date)
                    temp.put('dueDate',dateToString(bookL?.due_date))
                if(bookL?.borrowing_date)
                    temp.put('borrowed_date',dateToString(bookL?.borrowing_date))
                def authorArr = bookItem?.book?.author
                ArrayList arr = new ArrayList()
                for(author in authorArr){
                    arr.add(author?.name)
                }
                temp.put('authors',arr)
                hm.put('book_info',temp)
                hm.put('msg','200')
            }
            return hm
        }
        return hm
    }

    def getSeriesreport(request, hm,ip){
        println "i in get getSeriesreport:: Service"
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String purpose = "dept Details."
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        // def dataresponse = jsonSluper.parseText(body)
        // println("dataresponse ::"+dataresponse)
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                ArrayList orgbookdetails = new ArrayList()
                def org=user.organization
                hm.put("orgName", org?.displayname+" ("+org?.name+")")
                int i=0
                BookClassification bookType= BookClassification.findByOrganizationAndName(org,"Periodic")
                def dept
                if(bookType==null)
                    dept=Series.findAllByOrganization(org)
                else
                {
                    dept=Series.findAllByBookclassificationNotEqualAndOrganization(bookType,org)

                }
                for(d in dept) {
                    HashMap temp=new HashMap()
                    i++
                    temp.put("srno",i)
                    temp.put("seriesname", d?.name)
                    def bookitem=BookItem.findAllByOrganizationAndSeries(org,d)
                    temp.put("Book_count", bookitem.size())
                    def bAvlstatus= BookStatus.findByName('Available')
                    def bookAvlitem=BookItem.findAllByOrganizationAndSeriesAndBookstatus(org,d,bAvlstatus)
                    temp.put("bookAvlitem", bookAvlitem.size())
                    def bIssuedtatus= BookStatus.findByName('Issued')
                    def bookIssueditem=BookItem.findAllByOrganizationAndSeriesAndBookstatus(org,d,bIssuedtatus)
                    temp.put("bookIssueditem", bookIssueditem.size())
                    def bReservedtatus= BookStatus.findByName('Reserved')
                    def bookReserveditem=BookItem.findAllByOrganizationAndSeriesAndBookstatus(org,d,bReservedtatus)
                    temp.put("bookReserveditem", bookReserveditem.size())
                    orgbookdetails.add(temp)
                }
                println(orgbookdetails)
                //}
                hm.put("orgseriesdetails",orgbookdetails)
                hm.put("msg",'200')
                println("hm.orgseriesdetails :"+hm.orgseriesdetails)

            }
        }
        return hm
    }

    def getBookData (request, hm) {
        println("in getBookData:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String purpose = "getBookData from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList book_list = new ArrayList()
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                int srno = 1
//                for (item in Book.findAllByOrganization(user.organization)) {
//                    def bookitem = BookItem.findByBook(item)
//
//
//                    HashMap temp = new HashMap()
//                    temp.put("srno", srno++)
//                    temp.put("id", item.id)
//                    temp.put("isbn", item.isbn)
//                    temp.put("title", item.title)
//                    temp.put("edition", item.edition)
//                    temp.put("volume", item.volume)
//                    temp.put("program", item?.program?.id)
//                    temp.put("program_name", item?.program?.name)
//                    temp.put("description", item?.description)
//                    temp.put("copies", item?.total_books)
//                    temp.put("bookType", item?.booktype?.name)
//                    temp.put("bookTypeId", item?.booktype?.id)
//                    temp.put("bookCat", item?.bookcategory?.name)
//                    temp.put("bookCatId", item?.bookcategory?.id)
//                    temp.put("publisher", item?.publisher?.name)
//                    temp.put("publisherId", item?.publisher?.id)
//                    temp.put("library", item?.library?.name)
//                    temp.put("libraryId", item?.library?.id)
//                    temp.put("isactive", item?.isactive)
//                    temp.put('volume', item?.volume)
//                    temp.put('vendor', item?.vendor_name)
//
//                    def book = Book.findById(item?.id)
//                    def mapLocation = BookItem.findAllByBookAndRackIsNull(book)
//                    temp.put('mapLocation', mapLocation.size())
//
//                    def authorArr = item?.author
//                    ArrayList arr = new ArrayList()
//                    for (author in authorArr) {
//                        HashMap temp1 = new HashMap()
//                        arr.add(author?.id)
//                    }
//                    //println('authorArr----' + authorArr)
////                    println('author----',item?.author)
////
//                    temp.put('authors', arr)
////                   // temp.put('authorId',item?.author?.id)
//                    temp.put('languageId', item?.language?.id)
//                    temp.put('language', item?.language?.name)
//
//
//                    temp.put('rack', bookitem?.rack?.rack_number)
////                    temp.put('purchaseDate', bookitem?.date_of_purchase)
//                    temp.put('purchaseDate', dateToStringYY( bookitem?.date_of_purchase))
////                    temp.put('publicationDate', bookitem?.publication_date)
//                    temp.put('publicationDate',dateToStringYY( bookitem?.publication_date))
//                    temp.put('price', bookitem?.price)
//                    temp.put('bookprice', bookitem?.book_price)
//                    temp.put('discountPrice', bookitem?.discount_percentage)
//                    temp.put('isDonated', bookitem?.is_received_from_donation)
//                    temp.put('price', bookitem?.price)
//                    temp.put('billNo', bookitem?.bill_number)
//                    temp.put('pages', bookitem?.numberofpages)
//                    temp.put('year', bookitem?.publication_year)
//                    temp.put('bookFormat', bookitem?.bookformat?.name)
//                    temp.put('bookFormatId', bookitem?.bookformat?.id)
//                    book_list.add(temp)
//                }
//                hm.put("book_list", book_list)

                ArrayList booktypeList = new ArrayList()
                def bookType = BookType.findAllByIsactiveAndOrganization(true,org)
                for (book in bookType) {
                    HashMap temp = new HashMap()
                    temp.put("bookType", book.name)
                    temp.put("bookTypeId", book.id)
                    booktypeList.add(temp)
                }
                hm.put('bookTypeList', booktypeList)


                def bookFormat = BookFormat.findAllByIsactive(true)

                hm.put('bookFormatList', bookFormat)

                ArrayList bookCatlist = new ArrayList()
                def bookCat = BookCategory.findAllByIsactiveAndOrganization(true,org)
                for (ut in bookCat) {
                    HashMap temp = new HashMap()
                    temp.put("bookCat", ut.name)
                    temp.put("bookCatId", ut.id)
                    bookCatlist.add(temp)
                }
                hm.put('bookCatList', bookCatlist)

                ArrayList langList = new ArrayList()
                def language = Language.findAllByIsactiveAndOrganization(true,org)
                for (lang in language) {
                    HashMap temp = new HashMap()
                    temp.put("language", lang.name)
                    temp.put("languageId", lang.id)
                    langList.add(temp)
                }
                hm.put('languageList', langList)

                ArrayList publisherlist = new ArrayList()
                def publisher = Publisher.findAllByIsactive(true)
                for (ut in publisher) {
                    HashMap temp = new HashMap()
                    temp.put("publisher", ut.name)
                    temp.put("publisherId", ut.id)
                    publisherlist.add(temp)
                }
                hm.put('publisherList', publisherlist)

                ArrayList lib_list = new ArrayList()
                def userdata = User.findByUsername(uid)
                def lib = userdata?.organization?.library
                for (item in lib) {
                    HashMap temp = new HashMap()
                    temp.put("id", item.id)
                    temp.put("name", item.name)
                    lib_list.add(temp)
                }

                hm.put("lib_list", lib)

                ArrayList authorlist = new ArrayList()
                def author = Author.findAllByIsactive(true)
                for (item in author) {
                    HashMap temp = new HashMap()
                    temp.put("author", item.name)
                    temp.put("authorId", item.id)
                    authorlist.add(temp)
                }
                hm.put('authorList', authorlist)


                ArrayList programList = new ArrayList()
                def program = Program.findAllByOrganization(org)
                for (ut in program) {
                    HashMap temp = new HashMap()
                    temp.put("name", ut.name)
                    temp.put("id", ut.id)
                    programList.add(temp)
                }
                hm.put('programList', programList)

                ArrayList deptlist = new ArrayList()
                def libdept = LibraryDepartment.findAllByOrganization(org)
                for (ut in libdept) {
                    HashMap temp = new HashMap()
                    temp.put("name", ut.name)
                    temp.put("id", ut.id)
                    deptlist.add(temp)
                }
                hm.put('librarydeptlist', deptlist)


                ArrayList bookSeriesList = new ArrayList()
                def series = Series.findAllByOrganization(user.organization)
                for (bs in series) {
                    HashMap temp = new HashMap()
                    temp.put("seriesName", bs.name)
                    temp.put("seriesId", bs.id)
                    bookSeriesList.add(temp)
                }
                hm.put('bookSeriesList', bookSeriesList)
                BookClassification bookClassification=BookClassification.findByNameAndOrganization("Periodic",user.organization)
                def series_periodics = Series.findAllByOrganizationAndBookclassification(user.organization,bookClassification)

                hm.put('bookSeriesList_periodics', series_periodics)

                ArrayList frequency_list = new ArrayList()
                def freqlist = Frequency.findAllByOrganization(user.organization)

                hm.put('frequency_list', freqlist)

                Library library=Library.findByIsactive(true)
                hm.put('libid', library)

            }//println"hm: "+hm
            return hm

        }
        return hm
    }

    def getBookDataWithfilter (request, hm,ip) {
        println("in getBookData:")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        AuthService auth = new AuthService()

        String purpose = "getBookData from admin master menu"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList book_list = new ArrayList()
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)


                Author author1=null
                LibraryDepartment department =null
                Publisher publisher=null
                Series series=null
                def bookList=null
                println(dataresponse)
                if(dataresponse.Acc_no!="")
                {
                    bookList = BookItem.createCriteria().list {
                        'in'("organization", user.organization)
                        and{
                            'in'("accession_number", dataresponse.Acc_no)
                        }
                    }?.book
                }
                else {
                    if(dataresponse.selectedisbn!='ALL'){

                    }
                    if(dataresponse.selectedtitle!='ALL'){

                    }
//                if(dataresponse.selectedauther!='ALL'){
//                    author1=Author.findById(dataresponse.selectedauther)
//                }
                    if(dataresponse.selecteddepartment!='ALL'){
                        department=LibraryDepartment.findById(dataresponse.selecteddepartment)
                    }
                    if(dataresponse.selectedpublisher!='ALL')
                    {
                        publisher=Publisher.findById(dataresponse.selectedpublisher)
                    }
                    if(dataresponse.selectedseries!='ALL')
                    {
                        series=Series.findById(dataresponse.selectedseries)
                    }
                    bookList = Book.createCriteria().list {
                        'in'("organization", user.organization)
                        and{
                            if(dataresponse.selectedseries!='ALL')
                                'in'('series', series)
                            if(dataresponse.selectedpublisher!='ALL')
                                'in'('publisher', publisher)
                            if(dataresponse.selecteddepartment!='ALL')
                                'in'("librarydepartment", department)
                        }
                    }

                }

                int srno = 1
                for (item in bookList) {
                    def bookitem = BookItem.findByBook(item)
                    HashMap temp = new HashMap()
                    temp.put("srno", srno++)
                    temp.put("id", item.id)
                    temp.put("isbn", item.isbn)
                    temp.put("title", item.title)
                    temp.put("cno", item?.classificationno)
                    temp.put("edition", item.edition)
                    temp.put("volume", item.volume)
                    temp.put("program", item?.program?.id)
                    temp.put("program_name", item?.program?.name)
                    temp.put("description", item?.description)
                    temp.put("copies", item?.total_books)
                    temp.put("bookType", item?.booktype?.name)
                    temp.put("bookTypeId", item?.booktype?.id)
                    temp.put("bookCat", item?.bookcategory?.name)
                    temp.put("bookCatId", item?.bookcategory?.id)
                    temp.put("publisher", item?.publisher?.name)
                    temp.put("publisherId", item?.publisher?.id)
                    temp.put("library", item?.library?.name)
                    temp.put("libraryId", item?.library?.id)
                    temp.put("isactive", item?.isactive)
                    temp.put('volume', item?.volume)
                    temp.put('vendor', item?.vendor_name)
                    temp.put('seriesId', item?.series?.id)

                    def book = Book.findById(item?.id)
                    def mapLocation = BookItem.findAllByBookAndRackIsNull(book)
                    temp.put('mapLocation', mapLocation.size())

                    def authorArr=BooksAuthors.findAllByOrganizationAndBook(user.organization,book)?.author
                    ArrayList arr = new ArrayList()
                    for (author in authorArr) {
                        HashMap temp1 = new HashMap()
                        arr.add(author?.id)
                    }
                    //println('authorArr----' + authorArr)
//                    println('author----',item?.author)
//
                    temp.put('authors', arr)
//                   // temp.put('authorId',item?.author?.id)
                    temp.put('languageId', item?.language?.id)
                    temp.put('language', item?.language?.name)


                    temp.put('rack', bookitem?.rack?.rack_number)
//                    temp.put('purchaseDate', bookitem?.date_of_purchase)
                    temp.put('purchaseDate', dateToStringYY( bookitem?.date_of_purchase))
                    temp.put('date_of_entry',bookitem?.date_of_entry==null?"": dateToStringYY( bookitem?.date_of_entry))
                    temp.put('subscription_to',item?.subscription_to==null?"": dateToStringYY( item?.subscription_to))
                    temp.put('subscription_from',item?.subscription_from==null?"": dateToStringYY( item?.subscription_from))
                    temp.put('date_of_entry1',bookitem?.date_of_entry==null?"":dateToStringYY(bookitem?.date_of_entry))
                    temp.put('volume_from',item?.volume_from)
                    temp.put('volume_to',item?.volume_to)
                    temp.put('issue_from',item?.issue_from)
                    temp.put('issue_to',item?.issue_from)
                    temp.put('frequency',item?.frequency?.id)
                    temp.put('show_opac',item?.is_considered_in_opac)
//                    temp.put('publicationDate', bookitem?.publication_date)
                    temp.put('publicationDate',dateToStringYY( bookitem?.publication_date))
//                    temp.put('price', bookitem?.price)
                    temp.put('price', bookitem?.book_price)
                    temp.put('discount', bookitem?.discount_percentage)
                    temp.put('isDonated', bookitem?.is_received_from_donation)
                    temp.put('bookprice', bookitem?.price)
                    temp.put('place', item?.placeofpublication)
                    temp.put('subject', bookitem?.subject)
                    temp.put('keywords', bookitem?.keyword)
                    temp.put('billNo', bookitem?.bill_number)
                    temp.put('pages', bookitem?.numberofpages)
                    temp.put('year', bookitem?.publication_year)
                    temp.put('bookFormat', bookitem?.bookformat?.id)
                    temp.put('bookFormatId', bookitem?.bookformat?.id)
                    temp.put('library', Library.findByIsactive(true).id)
                    temp.put('libdept', item?.librarydepartment?.id)
                    book_list.add(temp)
                }
                hm.put("book_list", book_list)



            }//println"hm: "+hm
            return hm

        }
        return hm
    }

    def saveBook(request, hm, ip) {
        println "i am in saveBook"

        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        println('dataresponse.auth.size()-----------' + dataresponse?.authors?.size())
        String purpose = "save Book"
        auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def authorlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                Book book = Book.findByIsbnAndOrganization(dataresponse?.isbn, user?.organization)
                if (book != null) {
                    hm.put("msg", "Book already exist!!")
                } else {
                    book = new Book()
                    book.isactive = true
                    if(dataresponse?.title)
                        book.title = dataresponse?.title
                    if(dataresponse?.cno)
                        book.classificationno = dataresponse?.cno
                    if(dataresponse?.edition)
                        book.edition = dataresponse?.edition
                    if(dataresponse?.description)
                        book.description = dataresponse?.description
                    if(dataresponse?.copies)
                        book.total_books = dataresponse?.copies
                    book.isbn = dataresponse?.isbn
                    if(dataresponse?.volume)
                        book.volume = dataresponse?.volume


                    println('dataresponse.auth.size()-----------' + dataresponse?.authors?.size())

//                    if(dataresponse?.authorText == true){
//                        String author = dataresponse?.authors
//                        String[] authorArray = author.trim().split("\\s*,\\s*");
//                        for(int author_i=0;author_i< authorArray.size();author_i++){
//                            println('author_i====='+author_i)
//                            println('authorArray[author_i]======='+authorArray[author_i])
//                            def authorId = Author.findByName(authorArray[author_i])
//                            if(authorId != null){
//                                book.addToAuthor(authorId)
//                            }else{
//                                authorId = new Author()
//                                authorId.isactive = true
//                                authorId.name = authorArray[author_i]
//                                authorId.updation_ip_address = ip
//                                authorId.creation_ip_address = ip
//                                authorId.updation_date = new Date()
//                                authorId.creation_date = new Date()
//                                authorId.updation_username = uid
//                                authorId.creation_username = uid
//                                authorId.save(flush: true, failOnError: true)
//
//                                book.addToAuthor(authorId)
//                            }
//                        }
//                    }else {
//                        for (int author = 0; author < dataresponse?.authors?.size(); author++) {
//                            def authorId = Author.findById(dataresponse.authors[author])
//                            println('authorId---' + authorId)
//                            book.addToAuthor(authorId)
//                        }
//                    }

                    if (dataresponse.bookTypeId) {
                        def bt = BookType.findById(dataresponse.bookTypeId)
                        book.booktype = bt
                    }
                    if (dataresponse.bookCatId) {
                        def bc = BookCategory.findById(dataresponse.bookCatId)
                        book.bookcategory = bc
                    }

                    if (dataresponse.languageId) {
                        def lang = Language.findById(dataresponse.languageId)
                        book.language = lang
                    }
                    if (dataresponse.publisherId) {
                        if(dataresponse?.publisherText == true){
                            Publisher publisher = Publisher.findByName(dataresponse.publisherId)
                            if (publisher) {
                                book.publisher = publisher
                            } else {
                                publisher = new Publisher()
                                publisher.isactive = true
                                publisher.name = dataresponse.publisherId
                                publisher.updation_ip_address = ip
                                publisher.creation_ip_address = ip
                                publisher.updation_date = new Date()
                                publisher.creation_date = new Date()
                                publisher.updation_username = uid
                                publisher.creation_username = uid
                                publisher.save(flush: true, failOnError: true)

                                book.publisher = publisher
                            }
                        }else {
                            def publisher = Publisher.findById(dataresponse.publisherId)
                            book.publisher = publisher
                        }
                    }
                    if (dataresponse.library) {
                        def library = Library.findById(dataresponse.library)
                        book.library = library
                    }
                    if (dataresponse.program) {
                        def program = Program.findById(dataresponse.program)
                        book.program = program
                    }
                    if (dataresponse.libdept) {
                        def librarydept = LibraryDepartment.findById(dataresponse.libdept)
                        book.librarydepartment = librarydept
                    }
                    if (dataresponse.seriesId) {
                        def bseries = Series.findById(dataresponse.seriesId)
                        book.series = bseries
                    }
                    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
                    if(dataresponse?.subscription_to && dataresponse?.subscription_to != 'Invalid date') {
                        Date date1 = formatter1.parse(dataresponse?.subscription_to);
                        book.subscription_to = date1
                    }
                    if(dataresponse?.subscription_from && dataresponse?.subscription_from != 'Invalid date') {
                        Date date1 = formatter1.parse(dataresponse?.subscription_from);
                        book.subscription_from = date1
                    }
                    if(dataresponse?.volume_from){
                        book.volume_from = dataresponse?.volume_from
                    }
                    if(dataresponse?.place){
                        book.placeofpublication = dataresponse?.place
                    }
                    if(dataresponse?.volume_to){
                        book.volume_to = dataresponse?.volume_to
                    }
                    if(dataresponse?.issue_from){
                        book.issue_from = dataresponse?.issue_from
                    }
                    if(dataresponse?.issue_to){
                        book.issue_to = dataresponse?.issue_to
                    }
                    if(dataresponse?.show_opac || dataresponse?.show_opac==null || dataresponse?.show_opac==''){
                        book.is_considered_in_opac = true
                    }
                    if(dataresponse?.show_opac==false || dataresponse?.show_opac=='false' ){
                        book.is_considered_in_opac = false
                    }
                    if(dataresponse.frequency)
                    {
                        Frequency frequency=Frequency.findById(dataresponse.frequency)
                        book.frequency=frequency
                    }
                    book.organization = user?.organization
                    book.keywords = dataresponse.keywords

                    book.edition_year = dataresponse.year
                    book.updation_ip_address = ip
                    book.creation_ip_address = ip
                    book.updation_date = new Date()
                    book.creation_date = new Date()
                    book.updation_username = uid
                    book.creation_username = uid
                    book.save(flush: true, failOnError: true)

                    Book current_book = Book.findByIsbnAndOrganization(dataresponse?.isbn,user?.organization)

                    if(dataresponse?.authorText == true){
                        String author = dataresponse?.authors
                        String[] authorArray = author.trim().split("\\s*,\\s*");
                        for(int author_i=0;author_i< authorArray.size();author_i++){
                            println('author_i====='+author_i)
                            println('authorArray[author_i]======='+authorArray[author_i])
                            def authorId = Author.findByName(authorArray[author_i])
                            if(authorId != null){
                                BooksAuthors bookAuthor=new BooksAuthors()
                                bookAuthor. creation_username=uid
                                bookAuthor. updation_username=uid
                                bookAuthor. creation_date=new Date()
                                bookAuthor. updation_date=new Date()
                                bookAuthor. creation_ip_address=ip
                                bookAuthor. updation_ip_address=ip
                                bookAuthor. organization=user.organization
                                bookAuthor.book=current_book
                                bookAuthor.author=authorId
                                bookAuthor.save(flush: true, failOnError: true)
                            }else{
                                authorId = new Author()
                                authorId.isactive = true
                                authorId.name = authorArray[author_i]
                                authorId.updation_ip_address = ip
                                authorId.creation_ip_address = ip
                                authorId.updation_date = new Date()
                                authorId.creation_date = new Date()
                                authorId.updation_username = uid
                                authorId.creation_username = uid
                                authorId.save(flush: true, failOnError: true)

                                BooksAuthors bookAuthor=new BooksAuthors()
                                bookAuthor. creation_username=uid
                                bookAuthor. updation_username=uid
                                bookAuthor. creation_date=new Date()
                                bookAuthor. updation_date=new Date()
                                bookAuthor. creation_ip_address=ip
                                bookAuthor. updation_ip_address=ip
                                bookAuthor. organization=user.organization
                                bookAuthor.book=current_book
                                bookAuthor.author=authorId
                                bookAuthor.save(flush: true, failOnError: true)
                            }
                        }
                    }
                    else {
                        for (int author = 0; author < dataresponse?.authors?.size(); author++) {
                            def authorId = Author.findById(dataresponse.authors[author])
                            BooksAuthors bookAuthor=new BooksAuthors()
                            bookAuthor.creation_username=uid
                            bookAuthor.updation_username=uid
                            bookAuthor.creation_date=new Date()
                            bookAuthor.updation_date=new Date()
                            bookAuthor.creation_ip_address=ip
                            bookAuthor.updation_ip_address=ip
                            bookAuthor.organization=user.organization
                            bookAuthor.book=current_book
                            bookAuthor.author=authorId
                            bookAuthor.save(flush: true, failOnError: true)
                        }
                    }

                    if (current_book != null) {
                        int item_no = current_book?.total_books

                        def index_config = IndexConfig.list()
                        println('index_config---' + index_config)
                        def newacc = dataresponse.acc
                        def part = newacc.split("(?<=\\D)(?=\\d)")
                        def no = Integer.parseInt(part[1])
                        Series series  =Series.findById(dataresponse.seriesId)
                        if(series==null)
                        {
                            hm.put("msg", "Series not Found")
                            return hm
                        }
                        for (int item = 0; item < item_no; item++) {
                            BookItem bookItem = new BookItem()
                            if(dataresponse.year)
                                bookItem.publication_year = dataresponse.year

                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                            if(dataresponse?.publicationDate && dataresponse?.publicationDate != 'Invalid date') {
                                Date date1 = formatter.parse(dataresponse?.publicationDate);
                                bookItem.publication_date = date1
                            }
                            if(dataresponse?.vendor)
                                bookItem.vendor_name = dataresponse?.vendor
                            if (dataresponse?.pages) {
                                bookItem.numberofpages = dataresponse?.pages
                            }
                            if(dataresponse.subject)
                            {
                                bookItem.subject = dataresponse.subject
                            }

                            println(" sjdjf sh djfh sjd hfh sjdfjshdhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh")
                            println(dataresponse?.price.getClass().getSimpleName())
                            println(dataresponse?.bookprice.getClass().getSimpleName())
                            println(dataresponse?.discount.getClass().getSimpleName())

                            if (dataresponse?.price ) {
                                if(dataresponse?.price.getClass().getSimpleName()=='Integer')
                                    bookItem.book_price = dataresponse.price
                                else if(dataresponse?.price.getClass().getSimpleName()=='BigDecimal')
                                    bookItem.book_price = dataresponse.price.doubleValue()
                                else
                                    bookItem.book_price = Double.parseDouble(dataresponse.price)
                            }
                            if (dataresponse?.bookprice ) {
                                if(dataresponse?.bookprice.getClass().getSimpleName()=='Integer')
                                    bookItem.price = dataresponse.bookprice
                                else if(dataresponse?.bookprice.getClass().getSimpleName()=='BigDecimal')
                                    bookItem.price = dataresponse.bookprice.doubleValue()
                                else
                                    bookItem.price = Double.parseDouble(dataresponse.bookprice)
                            }
                            if(dataresponse.keywords)
                                bookItem.keyword = dataresponse.keywords
                            if (dataresponse?.discount ) {
                                if(dataresponse?.discount.getClass().getSimpleName()=='Integer')
                                    bookItem.discount_percentage = dataresponse.discount
                                else if(dataresponse?.discount.getClass().getSimpleName()=='BigDecimal')
                                    bookItem.discount_percentage = dataresponse.discount.doubleValue()
                                else
                                    bookItem.discount_percentage = Double.parseDouble(dataresponse.discount)
                            }

                            if (dataresponse.seriesId) {
                                def bseries = Series.findById(dataresponse.seriesId)
                                bookItem.series = bseries

                                def a_no = no
                                bookItem.accession_number = part[0] + a_no
                                bookItem.barocde = part[0] + a_no
                                no=no+1
                            }else {
                                println('index_config-------' + index_config?.last_accession_number)
                                bookItem.accession_number = no++
                                bookItem.barocde = no++
                            }
                            def bs = BookStatus.findByName('Available')
                            if(dataresponse.perioic_or_book=='periodic')
                                bs = BookStatus.findByName('Coming')

                            bookItem.bookstatus = bs
                            println("dataresponse?.purchaseDate : "+dataresponse?.purchaseDate+" dataresponse.isDonated :"+dataresponse.isDonated)
                            if(dataresponse?.purchaseDate && dataresponse?.purchaseDate != 'Invalid date' && dataresponse.isDonated != true) {
                                Date date2 = formatter.parse(dataresponse?.purchaseDate);
                                bookItem.date_of_purchase = date2
                            }
                            if(dataresponse?.date_of_entry && dataresponse?.date_of_entry != 'Invalid date' ) {
                                Date date2 = formatter.parse(dataresponse?.date_of_entry);
                                bookItem.date_of_entry = date2
                            }

                            if (dataresponse.isDonated == true) {
                                bookItem.is_received_from_donation = 1
                            } else {
                                bookItem.is_received_from_donation = 0
                            }
                            if (dataresponse.libdept) {
                                def librarydept = LibraryDepartment.findById(dataresponse.libdept)
                                bookItem.librarydepartment = librarydept
                            }
                            bookItem.isactive = true
                            bookItem.book = current_book
                            if (dataresponse?.billNo && dataresponse.isDonated == false) {
                                bookItem.bill_number = dataresponse.billNo
                            }
                            if (dataresponse?.bookFormatId) {
                                def bookFormat = BookFormat.findById(dataresponse.bookFormatId)
                                bookItem.bookformat = bookFormat
                            }

                            def user_org = User.findByUsername(uid)
                            bookItem.organization = user_org?.organization
                            if(dataresponse.publicationYear)
                            {
                                def pyear = dataresponse.publicationYear.split("-")[0]
                                bookItem.publication_year = pyear
                            }
                            bookItem.location = dataresponse.place
                            bookItem.display_order = 9999999
                            bookItem.updation_ip_address = ip
                            bookItem.creation_ip_address = ip
                            bookItem.updation_date = new Date()
                            bookItem.creation_date = new Date()
                            bookItem.updation_username = uid
                            bookItem.creation_username = uid
                            bookItem.save(flush: true, failOnError: true)
                        }
                        index_config[0].last_accession_number = no
                        index_config[0].save(flush: true, failOnError: true)
                    }
                    ArrayList book_data = new ArrayList()
                    def bi_data = BookItem.findAllByBook(current_book)
                    for(bi in bi_data) {
                        HashMap data = new HashMap()
                        data.put('title',current_book?.title)
                        data.put('acc_no',bi?.accession_number)
                        data.put('status',bi?.bookstatus?.name)
                        book_data.add(data)
                    }
                    hm.put("msg", "success")
                    hm.put("book_data", book_data)

                }

                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }
    def getBookFilter(request, hm, ip) {
        println("in getBookFilter:")

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")

        AuthService auth = new AuthService()

        String purpose = "getBookFilter"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                User logeduser = User.findByUsername(log.username)
                def publisherlist=[]
                def departmentlist=[]
                def autherlist=[]
                def titlelist=[]
                def isbnlist=[]
                HashMap temp2 =new HashMap()
                temp2.put("name",'ALL')
                temp2.put("id",'ALL')

                titlelist.add(temp2)
                isbnlist.add(temp2)
//
//                def booklist=Book.findAllByOrganization(logeduser.organization)
//                for(x in booklist)
//                {
//                    HashMap temp =new HashMap()
//                    temp.put("name",x.isbn)
//                    temp.put("id",x.id)
//                    isbnlist.add(temp)
//
//                    HashMap temp1 =new HashMap()
//                    temp1.put("name",x.title)
//                    temp1.put("id",x.id)
//                    titlelist.add(temp1)
//
//                }

                def publisher=[]
                publisher.add(temp2)
                hm.put("publisherlist",publisher)

                def libdept=[]
                libdept.add(temp2)
                hm.put("departmentlist",libdept)

                def series=Series.findAllByOrganization(logeduser.organization)
                series.add(temp2)
                hm.put("serieslist",series)

                def author=[]
                author.add(temp2)
                hm.put("autherlist",author)

                hm.put('isbnlist',isbnlist)
                hm.put('titlelist',titlelist)

                hm.put('msg','200')
            }
            return hm
        }
        return hm
    }
    def editBook(request, hm, ip) {
        println "i am in editBook"

        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        println("s dkcjs dcjs dkcsjdcj sd jcsj dkc ks dj dksj ")
        String purpose = "edit Book"
        auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            def authorlist = []
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                Book book = Book.findById(dataresponse?.id)
                if (book != null) {
                    book.isactive = true
                    if(dataresponse?.title)
                        book.title = dataresponse?.title
                    if(dataresponse?.edition)
                        book.edition = dataresponse?.edition
                    if(dataresponse?.description)
                        book.description = dataresponse?.description
                    if(dataresponse?.copies)
                        book.total_books = dataresponse?.copies

                    if(dataresponse?.cno)
                        book.classificationno = dataresponse?.cno

                    if(book.isbn != dataresponse?.isbn){
                        Book book_isbn = Book.findByIsbn(dataresponse?.isbn)
                        if(book_isbn != null){
                            hm.put('isbnExistMsg','ISBN already exists!!!')
                        }else {
                            book.isbn = dataresponse?.isbn
                        }

                    }

//                    book.author.clear()
//                    for (int author = 0; author < dataresponse?.authors?.size(); author++) {
//                        def authorId = Author.findById(dataresponse.authors[author])
//                        println('authorId---' + authorId)
//                        book.addToAuthor(authorId)
//                    }

                    if(dataresponse?.volume)
                        book.volume = dataresponse?.volume

                    if (dataresponse.bookTypeId) {
                        def bt = BookType.findById(dataresponse.bookTypeId)
                        book.booktype = bt
                    }
                    if (dataresponse.bookCatId) {
                        def bc = BookCategory.findById(dataresponse.bookCatId)
                        book.bookcategory = bc
                    }
                    if (dataresponse.languageId) {
                        def lang = Language.findById(dataresponse.languageId)
                        book.language = lang
                    }
                    if (dataresponse.publisherId) {
                        def publisher = Publisher.findById(dataresponse.publisherId)
                        book.publisher = publisher
                    }
                    if (dataresponse.libraryId) {
                        def library = Library.findById(dataresponse.libraryId)
                        book.library = library
                    }
                    if (dataresponse.program) {
                        def program = Program.findById(dataresponse.program)
                        book.program = program
                    }
                    if (dataresponse.libdept) {
                        def librarydept = LibraryDepartment.findById(dataresponse.libdept)
                        book.librarydepartment = librarydept
                    }

                    if (dataresponse.seriesId) {
                        def bseries = Series.findById(dataresponse.seriesId)
                        book.series = bseries
                    }

                    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
                    if(dataresponse?.subscription_to && dataresponse?.subscription_to != 'Invalid date') {
                        Date date1 = formatter1.parse(dataresponse?.subscription_to);
                        book.subscription_to = date1
                    }
                    if(dataresponse?.subscription_from && dataresponse?.subscription_from != 'Invalid date') {
                        Date date1 = formatter1.parse(dataresponse?.subscription_from);
                        book.subscription_from = date1
                    }
                    if(dataresponse?.volume_from){
                        book.volume_from = dataresponse?.volume_from
                    }
                    if(dataresponse?.place){
                        book.placeofpublication = dataresponse?.place
                    }
                    if(dataresponse?.volume_to){
                        book.volume_to = dataresponse?.volume_to
                    }
                    if(dataresponse?.issue_from){
                        book.issue_from = dataresponse?.issue_from
                    }
                    if(dataresponse?.issue_to){
                        book.issue_to = dataresponse?.issue_to
                    }
                    if(dataresponse?.show_opac || dataresponse?.show_opac==null || dataresponse?.show_opac==''){
                        book.is_considered_in_opac = true
                    }
                    if(dataresponse?.show_opac==false || dataresponse?.show_opac=='false' ){
                        book.is_considered_in_opac = false
                    }
                    if(dataresponse.frequency)
                    {
                        Frequency frequency=Frequency.findById(dataresponse.frequency)
                        book.frequency=frequency
                    }

                    book.updation_ip_address = ip
                    book.creation_ip_address = ip
                    book.updation_date = new Date()
                    book.creation_date = new Date()
                    book.updation_username = uid
                    book.creation_username = uid
                    book.save(flush: true, failOnError: true)

                    Book current_book = Book.findByIsbnAndOrganization(dataresponse?.isbn,user.organization)
                    println(current_book)
                    println(user.organization)
                    def booksAuthors=BooksAuthors.findAllByOrganizationAndBook(user.organization,current_book)
                    for(i in booksAuthors)
                        i.delete(flush:true,failOnError: true)

                    if(dataresponse?.authorText == true){
                        String author = dataresponse?.authors
                        String[] authorArray = author.trim().split("\\s*,\\s*");
                        for(int author_i=0;author_i< authorArray.size();author_i++){
                            println('author_i====='+author_i)
                            println('authorArray[author_i]======='+authorArray[author_i])
                            def authorId = Author.findByName(authorArray[author_i])
                            if(authorId != null){
                                BooksAuthors bookAuthor=new BooksAuthors()
                                bookAuthor. creation_username=uid
                                bookAuthor. updation_username=uid
                                bookAuthor. creation_date=new Date()
                                bookAuthor. updation_date=new Date()
                                bookAuthor. creation_ip_address=ip
                                bookAuthor. updation_ip_address=ip
                                bookAuthor. organization=user.organization
                                bookAuthor.book=current_book
                                bookAuthor.author=authorId
                                bookAuthor.save(flush: true, failOnError: true)
                            }else{
                                authorId = new Author()
                                authorId.isactive = true
                                authorId.name = authorArray[author_i]
                                authorId.updation_ip_address = ip
                                authorId.creation_ip_address = ip
                                authorId.updation_date = new Date()
                                authorId.creation_date = new Date()
                                authorId.updation_username = uid
                                authorId.creation_username = uid
                                authorId.save(flush: true, failOnError: true)

                                BooksAuthors bookAuthor=new BooksAuthors()
                                bookAuthor. creation_username=uid
                                bookAuthor. updation_username=uid
                                bookAuthor. creation_date=new Date()
                                bookAuthor. updation_date=new Date()
                                bookAuthor. creation_ip_address=ip
                                bookAuthor. updation_ip_address=ip
                                bookAuthor. organization=user.organization
                                bookAuthor.book=current_book
                                bookAuthor.author=authorId
                                bookAuthor.save(flush: true, failOnError: true)
                            }
                        }
                    }
                    else {
                        for (int author = 0; author < dataresponse?.authors?.size(); author++) {
                            def authorId = Author.findById(dataresponse.authors[author])
                            BooksAuthors bookAuthor=new BooksAuthors()
                            bookAuthor.creation_username=uid
                            bookAuthor.updation_username=uid
                            bookAuthor.creation_date=new Date()
                            bookAuthor.updation_date=new Date()
                            bookAuthor.creation_ip_address=ip
                            bookAuthor.updation_ip_address=ip
                            bookAuthor.organization=user.organization
                            bookAuthor.book=current_book
                            bookAuthor.author=authorId
                            bookAuthor.save(flush: true, failOnError: true)
                        }
                    }

                    def books = BookItem.findAllByBookAndOrganization(current_book,user.organization)
                    if (current_book != null) {
                        for (bookItem in books) {

                            if(dataresponse.year)
                                bookItem.publication_year = dataresponse.year

                            SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");

                            if(dataresponse?.publicationDate && dataresponse?.publicationDate != 'Invalid date') {
                                Date date1 = formatter.parse(dataresponse?.publicationDate);
                                bookItem.publication_date = date1
                            }
                            if(dataresponse?.vendor)
                                bookItem.vendor_name = dataresponse?.vendor
                            if(dataresponse.pages)
                                bookItem.numberofpages = dataresponse.pages

                            println(" sjdjf sh djfh sjd hfh sjdfjshdhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh")
                            println(dataresponse?.price.getClass().getSimpleName())
                            println(dataresponse?.bookprice.getClass().getSimpleName())
                            println(dataresponse?.discount.getClass().getSimpleName())

                            if (dataresponse?.price ) {
                                if(dataresponse?.price.getClass().getSimpleName()=='Integer')
                                    bookItem.book_price = dataresponse.price
                                else if(dataresponse?.price.getClass().getSimpleName()=='BigDecimal')
                                    bookItem.book_price = dataresponse.price.doubleValue()
                                else
                                    bookItem.book_price = Double.parseDouble(dataresponse.price)
                            }
                            if (dataresponse?.bookprice ) {
                                if(dataresponse?.bookprice.getClass().getSimpleName()=='Integer')
                                    bookItem.price = dataresponse.bookprice
                                else if(dataresponse?.bookprice.getClass().getSimpleName()=='BigDecimal')
                                    bookItem.price = dataresponse.bookprice.doubleValue()
                                else
                                    bookItem.price = Double.parseDouble(dataresponse.bookprice)
                            }
                            if (dataresponse?.discount ) {
                                if(dataresponse?.discount.getClass().getSimpleName()=='Integer')
                                    bookItem.discount_percentage = dataresponse.discount
                                else if(dataresponse?.discount.getClass().getSimpleName()=='BigDecimal')
                                    bookItem.discount_percentage = dataresponse.discount.doubleValue()
                                else
                                    bookItem.discount_percentage = Double.parseDouble(dataresponse.discount)
                            }


//                            def bs = BookStatus.findByName('Available')
//                            bookItem.bookstatus = bs

                            if(dataresponse?.purchaseDate && dataresponse?.purchaseDate != 'Invalid date' && dataresponse.isDonated == false) {
                                Date date2 = formatter.parse(dataresponse?.purchaseDate);
                                bookItem.date_of_purchase = date2
                            }

                            if (dataresponse.isDonated == true) {
                                bookItem.is_received_from_donation = 1
                            } else {
                                bookItem.is_received_from_donation = 0
                            }

                            if (dataresponse.seriesId) {
                                def bseries = Series.findById(dataresponse.seriesId)
                                bookItem.series = bseries
                            }
                            bookItem.isactive = true
                            bookItem.book = current_book

                            if(dataresponse.billNo && dataresponse.isDonated == false)
                                bookItem.bill_number = dataresponse.billNo

                            if (dataresponse.bookFormatId) {
                                def bookFormat = BookFormat.findById(dataresponse.bookFormatId)
                                bookItem.bookformat = bookFormat
                            }
                            if (dataresponse.libdept) {
                                def librarydept = LibraryDepartment.findById(dataresponse.libdept)
                                bookItem.librarydepartment = librarydept
                            }
                            if (dataresponse.subject) {

                                bookItem.subject = dataresponse.subject
                            }

                            bookItem.updation_ip_address = ip
                            bookItem.creation_ip_address = ip
                            bookItem.updation_date = new Date()
                            bookItem.creation_date = new Date()
                            bookItem.updation_username = uid
                            bookItem.creation_username = uid
                            bookItem.save(flush: true, failOnError: true)
                        }
                    }
                    hm.put("msg", "success")
                }

                hm.put("status", "200")
                println('hm---' + hm)
                return hm
            }
        }
        return hm
    }


    //getPeriodicalReportList
    def getPeriodicalReportList(request, hm, ip){
        println "i am in getPeriodicalReportList "

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String purpose = "Getting getPeriodicalReportList."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                BookClassification bcl = BookClassification.findByNameAndOrganization('Periodic',user.organization)
                def series = Series.findAllByBookclassificationAndOrganization(bcl,user.organization)
                hm.put('series_data', series)
            }
        }
        return hm
    }

    def periodics_report_data(request,hm,ip){
        println "i am in periodics_report_data "

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse : "+dataresponse
        String purpose = "Getting getPeriodicalReportList."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
//                String dateInString = "15-10-2015";
                Date date1 = sdf1.parse(dataresponse.firstDate);
                Date date2 = sdf1.parse(dataresponse.lastDate);


                def mainarray = []
                def series
                BookClassification bcl = BookClassification.findByOrganizationAndName(user.organization,'Periodic')
                if(dataresponse.series_id=='ALL')
                    series=Series.findAllByOrganizationAndBookclassification(user.organization,bcl)
                else
                    series=Series.findById(dataresponse.series_id)

                println("series : "+series)
                for(sr in series)
                {
                    println("sr : "+sr)
                    def bookitem_list = BookItem.createCriteria().list {
                        'in'("organization", user.organization)
                        and{
                            'in'('series',sr)
                            between('date_of_entry',date1,date2)
                        }
                    }
                    if(bookitem_list)
                    { int x = 1
                        for(item in bookitem_list)
                        {
                            HashMap temp=new HashMap()
                            temp.put("srno",x++)
                            temp.put("title",item?.book?.title)
                            temp.put("publisher",item?.book?.publisher?.name)
                            temp.put("source",item?.vendor_name)
                            temp.put("frequency",item?.book?.frequency?.name)
                            if(item?.book?.volume_from != null){
                                temp.put("volume_no",item?.book?.volume_from+" to "+item?.book?.volume_to)
                            }else{
                                temp.put("volume_no","--")
                            }
                            if(item?.book?.issue_from != null){
                                temp.put("issue_no",item?.book?.issue_from+" to "+item?.book?.issue_to)
                            }else{
                                temp.put("issue_no",'--')
                            }
                            temp.put("department",item?.book?.librarydepartment?.name)
                            temp.put("type",item?.book?.booktype?.name)
                            temp.put("date_of_purchase",item?.date_of_purchase==null?'':item?.date_of_purchase)
                            temp.put("subscription_from",item?.book?.subscription_from==null?'':item?.book?.subscription_from)
                            temp.put("subscription_to",item?.book?.subscription_to==null?'':item?.book?.subscription_to)
                            //temp.put("subscription_from",item?.book?.subscription_from==null?'':item?.book?.subscription_from+" to "+item?.book?.subscription_to==null?'':item?.book?.subscription_from)
                            mainarray.add(temp)
                            hm.put("pdata", mainarray)

                        }

                    }
                }
            }

        }
        println("hm : "+hm)
        return hm
    }

    //status wise report
    def getstatusData(request, ip, hm) {
        println "i am in getstatusData "

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String purpose = "Getting getdeptData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def series_data = Series.findAllByOrganization(user.organization)
                hm.put('series_data', series_data)
                def statuslist =BookStatus.findAll()
                hm.put('statuslist', statuslist)
                def dept_list = LibraryDepartment.findAllByOrganization(user.organization)
                hm.put('dept_list', dept_list)

            }
        }
        return hm
    }
    def get_statuswise_data(request, ip, hm) {
        println "i am in get_statuswise_data Report"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        HashSet<HashMap> table_header_data = new HashSet<HashMap>()
        HashSet<HashMap> csv_header_data = new HashSet<HashMap>()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        HashSet<HashMap> set = new HashSet<HashMap>()


        String purpose = "Getting reports  statuswise."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList fetchdata = new ArrayList()

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                UserType user_type = UserType.findById(dataresponse.user_id)
                def mainarray = []
                def series
                if (dataresponse.series_id == 'ALL'){
                    series = Series.findAllByOrganization(user.organization)
                } else{
                    series = Series.findById(dataresponse.series_id)
                }
                println("series : "+series)
                println("user.organization : "+user.organization)
                def dept
                if (dataresponse.dept_id == 'ALL'){
                    dept = LibraryDepartment.findAllByOrganization(user.organization)
                } else{
                    dept = LibraryDepartment.findById(dataresponse.dept_id)
                }
                println("dept : "+dept)
                def status
//                if (dataresponse.status_id == 'ALL')
//                    status = BookStatus.findAllByOrganization(user.organization)
//                else
                status = BookStatus.findById(dataresponse.status_id)
                println("status : "+status)

                def bi_data = BookItem.createCriteria().list {
                    'in'("organization", user.organization)
                    and {
                        'in'('series', series)
                        'in'('bookstatus', status)
                        'in'('librarydepartment', dept)
                    }
                }
                println("bi_data.length"+bi_data.size())
                int x = 1
                for (i in bi_data) {
                    HashMap temp = new HashMap()
                    temp.put("srno", x++)
                    temp.put("title", i?.book?.title)
                    temp.put("author", i?.book?.author?.name)
                    temp.put("dept", i?.book?.librarydepartment?.name)
                    temp.put("publisher", i?.book?.publisher?.name)
                    temp.put("copies", i?.book?.total_books)
                    temp.put("acc_no", i?.accession_number)
                    temp.put("price", i?.book_price)
                    temp.put("total_price", i?.book?.total_books * i?.book_price)
                    temp.put("date_of_entry", i?.date_of_entry)
                    temp.put("status", i?.bookstatus?.name)
                    mainarray.add(temp)
                    hm.put("data", mainarray)
                }
            }
            // println("hm,data "+hm.data)
            return hm
        }
    }

    // medium wise report
    def getmediumReportData(request,ip, hm) {
        println "i am in getmediumReportData "

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String purpose = "Getting getdeptData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def series_data = Series.findAllByOrganization(user.organization)
                hm.put('series_data', series_data)
                def medium_list= Language.findAllByIsactiveAnd(true,user.organization)
                hm.put('medium_list', medium_list)
//                LibraryConfiguration libraryConfiguration =LibraryConfiguration.findByName("new_arrivals_book_no_of_days")
//                hm.put('no_of_days', libraryConfiguration?.value)
            }
        }
        return hm
    }
    def fetchmediumReport(request,ip, hm){
        println "i am in fetchmediumReport"

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        HashSet<HashMap> table_header_data = new HashSet<HashMap>()
        HashSet<HashMap> csv_header_data = new HashSet<HashMap>()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println dataresponse
        HashSet<HashMap> set = new HashSet<HashMap>()

        String purpose = "Getting reports  dailyData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            ArrayList fetchdata = new ArrayList()

            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)

                BookCategory book_cat = BookCategory.findById(dataresponse.dept_id)
                UserType user_type = UserType.findById(dataresponse.user_id)
                Language medium
//                if(dataresponse.medium == 'ALL'){
//                    medium = Language.findAll()
                medium= Language.findById(dataresponse.medium)
                println("medium : "+medium)
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
//                String dateInString = "15-10-2015";
                Date date1 = sdf1.parse(dataresponse.firstDate);
                Date date2 = sdf1.parse(dataresponse.lastDate);


                def mainarray = []
                def series
                if(dataresponse.series_id=='ALL')
                    series=Series.findAllByOrganization(user.organization)
                else
                    series=Series.findById(dataresponse.series_id)

                if(dataresponse.flag)
                {
//                    def bi_data = BookItem.createCriteria().list {
//                        'in'("organization", user.organization)
//                        projections{
//                            distinct('date_of_entry')
//                        }
//                        and {
//
//                            projections{
//                                distinct('book')
//                            }
//                            'in'('series',series)
//                            between("date_of_entry", date1, date2)
//                        }
//                    }
//                    int x = 1
//
//                    for (i in bi_data) {
//                        HashMap temp=new HashMap()
//                        temp.put("srno",x++)
//                        temp.put("title",i?.title)
//                        temp.put("isbn",i?.isbn)
//                        temp.put("author",i?.author?.name)
//                        temp.put("dept",i?.librarydepartment?.name)
//                        temp.put("publisher",i?.publisher?.name)
//
//                        def bookItem = BookItem.createCriteria().list {
//                            'in'("organization", user.organization)
//
//                            and {
//                                'in'('series',series)
//                                'in'('book',i)
//                                between("date_of_entry", date1, date2)
//                            }
//                        }
////                        BookItem bookItem=BookItem.findByDate_of_entryBetweenAndBookAndOrganization(date1,date2,i,user.organization)
////                        def bookitem1=BookItem.findAllByDate_of_entryBetweenAndBookAndOrganization(date1,date2,i,user.organization)
//                        if(!bookItem.isEmpty()){
//                            temp.put("copies",bookItem.size())
//                            temp.put("price",bookItem[0].book_price)
//
//                            temp.put("total_price",bookItem.size()*bookItem[0].book_price)
//                            temp.put("date_of_entry",bookItem[0]?.date_of_entry==null?null:bookItem[0]?.date_of_entry)
//                        }
//
//                        mainarray.add(temp)
//                        hm.put("data", mainarray)
//                    }
                    println("in if ")
                    def list= getDaysBetweenDates(date1,date2)
                    int x=1
                    for(d in list){

                        Calendar calendar = new GregorianCalendar();
                        calendar.setTime(d);
                        calendar.add(Calendar.DATE, 1);
                        Date result = calendar.getTime();
                        def fromdate=d
                        def todate=result
                        def bi_data = BookItem.createCriteria().list {
                            'in'("organization", user.organization)
                            and {
                                'in'('series',series)
                                between("date_of_entry", fromdate, todate)
                            }
                        }
                        println("c sdjc sd sjd hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh")
//                        println(d)
                        println(fromdate)
//                        println(todate)
//                        println(bi_data)
                        if(bi_data.isEmpty())
                            continue
                        else {
                            def book_data = Book.createCriteria().list {
                                'in'("organization", user.organization)
                                and {
                                    'in'('id', bi_data?.book?.id)
                                    'in'('series', series)
//                                    'in'('language',medium)
                                }
                            }.sort { it.title }
                            for (i in book_data) {
                                if(medium.id == i?.language?.id)
                                {
                                    HashMap temp = new HashMap()
                                    temp.put("srno", x++)
                                    temp.put("title", i?.title)
                                    temp.put("isbn", i?.isbn)
                                    temp.put("author", i?.author?.name)
                                    temp.put("dept", i?.librarydepartment?.name)
                                    temp.put("publisher", i?.publisher?.name)
                                    temp.put("language", i?.language?.name)
                                    def bookItem = BookItem.createCriteria().list {
                                        'in'("organization", user.organization)

                                        and {
                                            'in'('series', series)
                                            'in'('book', i)
                                            between("date_of_entry", fromdate, todate)
                                        }
                                    }
                                    if (!bookItem.isEmpty()) {
                                        temp.put("copies", bookItem.size())
                                        temp.put("price", bookItem[0].book_price)
                                        temp.put("total_price", bookItem.size() * bookItem[0].book_price)
                                    }
                                    temp.put("date_of_entry", fromdate)
                                    mainarray.add(temp)
                                }
                            }
                        }
                    }
                    hm.put("data", mainarray)
                    //  println("mainarray : "+mainarray)
                }
                else{
                    println("in else")
                    def bi_data = BookItem.createCriteria().list {
                        'in'("organization", user.organization)

                        and {
                            'in'('series',series)
                            between("date_of_entry", date1, date2)
                        }
                    }
                    int x = 1

                    for (i in bi_data) {
                        if(medium.id == i?.book?.language.id) {
                            HashMap temp = new HashMap()
                            temp.put("srno", x++)
                            temp.put("title", i?.book?.title)
                            temp.put("language",i?.book?.language?.name)
                            temp.put("author", i?.book?.author?.name)
                            temp.put("dept", i?.book?.librarydepartment?.name)
                            temp.put("publisher", i?.book?.publisher?.name)
                            temp.put("copies", i?.book?.total_books)
                            temp.put("acc_no", i?.accession_number)
                            temp.put("price", i?.book_price)
                            temp.put("total_price", i?.book?.total_books * i?.book_price)
                            temp.put("date_of_entry", i?.date_of_entry)
                            mainarray.add(temp)
                        }
                    }
                    mainarray = mainarray.sort({it.title})
                    // println("mainarray : "+mainarray)
                    hm.put("data", mainarray)
                }
                println("hm : "+hm)
                return hm
            }
            return hm
        }
    }

    //getWeedoutpagedata
    def getWeedoutpagedata(request,ip, hm) {
        println "i am in getWeedoutpagedata "

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String purpose = "Getting getdeptData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def series_data = Series.findAllByOrganization(user.organization)
                hm.put('series_data', series_data)
                println("series_data :"+series_data)
                def weedoutcode = Weedoutcode.findAllByOrganization(user.organization)
                if(weedoutcode.size() == 0){
                    println("in weedout 0 ")
                    hm.put('data', "Please fill Weed Out Master First")
                }
            }
        }
        return hm
    }
    def getbookdataWeedout(request, hm){
        println("in getbookdataWeedout")
        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()

        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "data response ------>" + dataresponse
        def bookitemid = dataresponse["accno"]

        String purpose = "fetch book data"
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg

        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                BookStatus status = BookStatus.findByName("WeedOut")
                def bi = BookItem.findByAccession_numberAndOrganization(bookitemid,user.organization)
                if(bi==null){
                    hm.put("msg","Book Not Found")
                    return hm
                }
                def code_list = Weedoutcode.findAllByOrganization(user.organization)

                BookLending bookLending2=BookLending.findByBookitemAndOrganizationAndIsactive(bi,user.organization,true)
                if(bookLending2!=null){
                    def me=Member.findByUserAndOrganization(bookLending2.user,user.organization)
                    if(me==null)
                        me=Employee.findByUserAndOrganization(bookLending2.user,user.organization)
                    hm.put("msg","Book Already Issue TO "+me.name)
                    return hm
                }

                def allBook = BookItem.findAllByBook(bi?.book)
                HashMap temp = new HashMap()
                temp.put("accession_number", bi?.accession_number)
                def is_img=false
                if(bi?.filename && bi?.filepath)
                    is_img=true
                temp.put('is_img', is_img)
                temp.put("blfid", bi?.id)
                temp.put("title", bi?.book?.title)

                temp.put("bookcategory", bi?.book?.bookcategory?.name)
                String authornames = ""
                for (author in bi?.book?.author) {
                    authornames += author.name + "\n"
                }
                temp.put("author", authornames)
                temp.put("publisher", bi?.book?.publisher?.name)
                temp.put("booktype", bi?.book?.booktype?.name)
                temp.put("bookformat", bi?.bookformat?.name)
                temp.put("bookprice", bi?.price)
                //temp.put("total_books", bi?.book?.total_books)
                temp.put("total_books", allBook.findAll { it?.bookstatus?.name == "Available" }.size())
                BookLending bookLending = BookLending.findByBookitem(bi)
                temp.put("member", user?.username)
//                def bookhistory=[]
//                def bookLending1 = BookLending.findAllByBookitem(bi)
//                bookLending1=bookLending1.sort({it.return_date})
//                for( i  in bookLending1)
//                {
//                    HashMap hashMap =new HashMap()
//                    hashMap.put("username",i.user.username)
//                    hashMap.put("return_date",i.return_date)
//                    Employee employee =Employee.findByUser(i?.user)
//                    if(employee==null){
//                        Member member =Member.findByUser(i?.user)
//                        hashMap.put("name",member?.name)
//                        hashMap.put("grno_empid",member?.registration_number)
//                    }
//                    else {
//                        hashMap.put("name",employee?.name)
//                        hashMap.put("grno_empid",employee?.employee_code)
//                    }
//                    bookhistory.add(hashMap)
//
//
//                }
//                hm.put("bookhistory", bookhistory)
                hm.put("bookinformation", temp)
                println("code_list : "+code_list)
                hm.put("code_list", code_list)
                hm.put("bfid", bi.id)
                println hm
                def weedoutArray =[]
                int srno = 1
                Series series = Series.findById(dataresponse.selectedseries)
                def bookitemslist = BookItem.createCriteria().list(){
                    isNotNull("weedoutcode")
                    and{
                        'in'('series', series)
                        'in'('bookstatus', status)
                    }
                }.sort({it.creation_date}).reverse(true)
                for(book in bookitemslist){
                    HashMap temp1 = new HashMap()
                    temp1.put("code",book?.weedoutcode?.name)
                    temp1.put("srno",srno++)
                    temp1.put("acc_no",book?.accession_number)
                    temp1.put("title",book?.book?.title)
                    def arr = ""
                    for (author in book?.book?.author.sort { it?.id })
                        arr = arr + author?.name + ","
                    arr = arr.replaceAll(',$', "")
                    arr = arr.replaceAll(",,", ",")
                    temp1.put("author",arr)
                    temp1.put("cost",book?.book_price)
                    temp1.put("weedoutremark",book?.weedout_remark)
                    temp1.put("rctnumber",book?.weedout_receiptnumber)
//                    Date date3 = formatter.parse(book?.weedout_date);
//                    Date date4 = formatter.parse(book?.weedout_receiptdate);
                    if(book?.weedout_date)
                        temp1.put("wo_date", book?.weedout_date)
                    if(book?.weedout_receiptdate)
                        temp1.put("wo_RCTdate", book?.weedout_receiptdate)
                    weedoutArray.add(temp1)
                }
                hm.put("weedoutArray",weedoutArray)
                hm.put("msg", "200")
                return hm
            }
            return hm
        }
    }
    def approveweedout(request,ip, hm) {
        println "i am in getWeedoutpagedata "

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println "dataresponse"+dataresponse

        String purpose = "Getting getdeptData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                BookItem bookitem = BookItem.findByAccession_number(dataresponse.accno)
                println("bookitem L "+bookitem)
                BookStatus status = BookStatus.findByName("WeedOut")
                Weedoutcode code = Weedoutcode.findById(dataresponse.code)
                if(bookitem && bookitem?.bookstatus?.name !="Issued"){
                    Date date1 = formatter.parse(dataresponse.weedoutdate);
                    if(dataresponse.weedoutRctdate){
                        // Date date2 = formatter.parse(dataresponse.weedoutRctdate);
                        bookitem.weedout_receiptdate = dataresponse.weedoutRctdate
                    }
                    if(dataresponse.rctnum)
                        bookitem.weedout_receiptnumber = dataresponse.rctnum
                    bookitem.weedout_date = dataresponse.weedoutdate
                    bookitem.weedout_remark = dataresponse.remark
                    bookitem.weedoutcode = code
                    bookitem.bookstatus= status
                    bookitem.creation_username = uid
                    bookitem.updation_username = uid
                    bookitem.creation_date = new Date()
                    bookitem.updation_date = new Date()
                    bookitem.creation_ip_address = ip
                    bookitem.updation_ip_address = ip
                    bookitem.save(flush: true, failOnError: true)
                }
                Series series = Series.findById(dataresponse.selectedseries)
                def weedoutArray=[]
                int srno = 1
                def bookitemslist = BookItem.createCriteria().list(){
                    isNotNull("weedoutcode")
                    and{
                        'in'('bookstatus', status)
                        'in'('series', series)
                    }
                }
                def bookitemslist1 = bookitemslist.sort({it.creation_date}).reverse(true)
                for(book in bookitemslist1){
                    HashMap temp = new HashMap()
                    temp.put("code",book?.weedoutcode?.name)
                    temp.put("srno",srno++)
                    temp.put("acc_no",book?.accession_number)
                    temp.put("title",book?.book?.title)
                    def arr = ""
                    for (author in book?.book?.author.sort { it?.id })
                        arr = arr + author?.name + ","
                    arr = arr.replaceAll(',$', "")
                    arr = arr.replaceAll(",,", ",")
                    temp.put("author",arr)
                    temp.put("cost",book?.book_price)
                    temp.put("weedoutremark",book?.weedout_remark)
                    temp.put("rctnumber",book?.weedout_receiptnumber)
//                    Date date3 = formatter.parse(book?.weedout_date);
//                    Date date4 = formatter.parse(book?.weedout_receiptdate);
                    if(book?.weedout_date)
                        temp.put("wo_date", book?.weedout_date)
                    if(book?.weedout_receiptdate)
                        temp.put("wo_RCTdate", book?.weedout_receiptdate)
                    weedoutArray.add(temp)
                }
                hm.put("weedoutArray",weedoutArray)
                hm.put("code","success")
            }
        }
        //println("hm "+hm.weedoutArray)
        return hm
    }
    def getallWeedoutbooks(request,ip, hm) {
        println "i am in getWeedoutpagedata "

        def token = request.getHeader("EPS-token")//'123'//request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")//"admin@veis.com"//request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()


        String purpose = "Getting getdeptData."
        auth.checkauth(token, uid, hm, url, request, purpose)//401;200;token-refreshed ->msg
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def weedoutArray =[]
                BookStatus status = BookStatus.findByName("WeedOut")
                def bookitemslist = BookItem.createCriteria().list(){
                    isNotNull("weedoutcode")
                    and{
                        'in'('bookstatus', status)
                        'in'('organization', user?.organization)
                    }
                }
                def srno=1
                def bookitemslist1 = bookitemslist.sort({it.creation_date}).reverse(true)
                for(book in bookitemslist1){
                    HashMap temp = new HashMap()
                    temp.put("code",book?.weedoutcode?.name)
                    temp.put("srno",srno++)
                    temp.put("acc_no",book?.accession_number)
                    temp.put("title",book?.book?.title)
                    def arr = ""
                    for (author in book?.book?.author.sort { it?.id })
                        arr = arr + author?.name + ","
                    arr = arr.replaceAll(',$', "")
                    arr = arr.replaceAll(",,", ",")
                    temp.put("author",arr)
                    temp.put("cost",book?.book_price)
                    temp.put("weedoutremark",book?.weedout_remark)
                    temp.put("rctnumber",book?.weedout_receiptnumber)
//                    Date date3 = formatter.parse(book?.weedout_date);
//                    Date date4 = formatter.parse(book?.weedout_receiptdate);
                    if(book?.weedout_date)
                        temp.put("wo_date", book?.weedout_date)
                    if(book?.weedout_receiptdate)
                        temp.put("wo_RCTdate", book?.weedout_receiptdate)
                    weedoutArray.add(temp)
                }
                hm.put("allweedoutArray",weedoutArray)
                hm.put("code","success")
            }
        }
        //println("hm "+hm.weedoutArray)
        return hm
    }
}