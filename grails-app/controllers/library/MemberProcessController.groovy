package library
import grails.converters.JSON
class MemberProcessController {

    def index() {}
    //Process Super Admin Login
    def getBooksData() {
        println "getBooksData"
        MemberProcessService memberProcessService = new MemberProcessService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        memberProcessService.getBooksData(request, hm, ip)
        render hm as JSON
    }
    //Renew Book
    def renewbook() {
        println "renewbook"
        MemberProcessService memberProcessService = new MemberProcessService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        memberProcessService.renewbook(request, hm, ip)
        render hm as JSON
    }
    // Book Donation
    def bookDonation() {
        println "bookDonation"
        MemberProcessService memberProcessService = new MemberProcessService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        memberProcessService.bookDonation(request, hm, ip)
        render hm as JSON
    }
    // Save Book Donation
    def savebookdata() {
        println "savebookdata"
        MemberProcessService memberProcessService = new MemberProcessService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        memberProcessService.savebookdata(request, hm, ip)
        render hm as JSON
    }
    // Save Book Donation
    def donatebookIsactive() {
        println "donatebookIsactive::"
        MemberProcessService memberProcessService = new MemberProcessService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        memberProcessService.donatebookIsactive(request, hm, ip)
        render hm as JSON
    }
    // update Book Donation
    def updatedata() {
        println "updatedata::"
        MemberProcessService memberProcessService = new MemberProcessService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        memberProcessService.updatedata(request, hm, ip)
        render hm as JSON
    }
}