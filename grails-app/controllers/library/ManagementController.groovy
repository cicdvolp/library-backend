package library

import grails.converters.JSON
import grails.io.IOUtils
import groovy.json.JsonSlurper

import javax.servlet.http.Part
import java.nio.file.Paths
class ManagementController {

    def index() { }
    //get Management Link
    def getManagementLink()
    {
        println "In Management Link"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getManagementLink(request,hm)
        render hm as JSON
    }
    //Get All Org. Books
    def getAllOrgBooksData()
    {
        println "In getAllOrgBooksData"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getAllOrgBooksData(request,hm)
        render hm as JSON
    }
    def getAllOrgPeriodicalsData()
    {
        println "In getAllOrgBooksData"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getAllOrgPeriodicalsData(request,hm)
        render hm as JSON
    }
    def getAllOrgBookWithCDData()
    {
        println "In getAllOrgBooksData"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getAllOrgBookWithCDData(request,hm)
        render hm as JSON
    }
    //Get All Org. Books
    def getOrgBooksDetails()
    {
        println "In getAllOrgBooksDetails"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getOrgBooksDetails(request,hm)
        render hm as JSON
    }
    def getOrgPeriodicalsDetails()
    {
        println "In getAllOrgBooksDetails"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getOrgPeriodicalsDetails(request,hm)
        render hm as JSON
    }
    def getOrgBookWithCDDetails()
    {
        println "In getAllOrgBooksDetails"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getOrgBookWithCDDetails(request,hm)
        render hm as JSON
    }
    def getorgseriesdata()
    {
        println "In getAllOrgBooksDetails"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getorgseriesdata(request,hm)
        render hm as JSON
    }
    def getorgdeptdata()
    {
        println "In getAllOrgBooksDetails"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getorgdeptdata(request,hm)
        render hm as JSON
    }
    def orgdeptdetails()
    {
        println "In getAllOrgBooksDetails"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.orgdeptdetails(request,hm)
        render hm as JSON
    }
    def getorgseriesdetails()
    {
        println "In getAllOrgBooksDetails"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getorgseriesdetails(request,hm)
        render hm as JSON
    }
    def getorgseriesdetailsperiodicals()
    {
        println "In getAllOrgBooksDetails"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getorgseriesdetailsperiodicals(request,hm)
        render hm as JSON
    }
    def getorgseriesdetailsBookWithCD()
    {
        println "In getAllOrgBooksDetails"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getorgseriesdetailsBookWithCD(request,hm)
        render hm as JSON
    }
    //Get All Org. Emp
    def getAllOrgEmpData()
    {
        println "In getAllOrgEmpData :"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getAllOrgEmpData(request,hm)
        render hm as JSON
    }
    //Get All Emp Details
    def getEmployeeDetails()
    {
        println "In getEmployeeDetails"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getEmployeeDetails(request,hm)
        render hm as JSON
    }
    //Get All Org. Student
    def getAllOrgStudData()
    {
        println "In getAllOrgStudData :"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getAllOrgStudData(request,hm)
        render hm as JSON
    }
    //Get All Student Details
    def getStudentsDetails()
    {
        println "In getStudentsDetails"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getStudentsDetails(request,hm)
        render hm as JSON
    }
    //Get All Org. Student
    def getAllOrgData()
    {
        println "In getAllOrgData :"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getAllOrgData(request,hm)
        render hm as JSON
    }
    //Get All Student Details
    def getDetails()
    {
        println "In getDetails"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getDetails(request,hm)
        render hm as JSON
    }
    //Get All Auth Data
    def getAllPubData()
    {
        println "In getAllPubData :"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getAllPubData(request,hm)
        render hm as JSON
    }
    def getorglist()
    {
        println "In getorglist :"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getorglist(request,hm)
        render hm as JSON
    }
    //Get All Pub Details
    def getPubDetails()
    {
        println "In getPubDetails"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getPubDetails(request,hm)
        render hm as JSON
    }
    //Get All Title Data
    def getAllTitleData()
    {
        println "In getAllTitleData :"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getAllTitleData(request,hm)
        render hm as JSON
    }
    //Get All Title Details
    def getTitleDetails()
    {
        println "In getTitleDetails"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getTitleDetails(request,hm)
        render hm as JSON
    }
    //Get All Donation Data
    def getAllDonationData()
    {
        println "In getAllDonationData :"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getAllDonationData(request,hm)
        render hm as JSON
    }
    //Get All Donation Details
    def getDonationDetails()
    {
        println "In getDonationDetails"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getDonationDetails(request,hm)
        render hm as JSON
    }
    def getAllOrgCategoryData(){
        println "In getAllOrgCategoryData :"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getAllOrgCategoryData(request,hm)
        render hm as JSON

    }
    def getCatDetails()
    {
        println "In getCatDetails"
        ManagementService managementService=new ManagementService()
        HashMap hm = new HashMap()
        managementService.getCatDetails(request,hm)
        render hm as JSON
    }
}
