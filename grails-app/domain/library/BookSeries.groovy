package library
import grails.gorm.MultiTenant

class BookSeries implements MultiTenant<BookStatus>
{
    String name     //EC,SW,MCA,MBA
    boolean isactive
    int display_order
    String short_name
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
        short_name nullable: true
    }
    static mapping = {
        isactive defaultValue: true
        display_order defaultValue: 0
    }
}
