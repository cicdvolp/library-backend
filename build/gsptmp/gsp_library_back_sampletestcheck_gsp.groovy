import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_library_back_sampletestcheck_gsp extends org.grails.gsp.GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/sample/testcheck.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createClosureForHtmlPart(1, 1)
invokeTag('captureHead','sitemesh',6,[:],1)
printHtmlPart(2)
createTagBody(1, {->
printHtmlPart(3)
loop:{
int i = 0
for( sub in (submissions) ) {
printHtmlPart(4)
expressionOut.print(i+1)
printHtmlPart(5)
expressionOut.print(sub?.learner?.person?.firstName)
printHtmlPart(6)
expressionOut.print(sub?.learner?.person?.lastName)
printHtmlPart(5)
expressionOut.print(sub?.learner?.uid)
printHtmlPart(5)
expressionOut.print(sub?.learner?.standard?.standard)
printHtmlPart(5)
expressionOut.print(sub?.courseoffering?.course?.course_name)
printHtmlPart(5)
expressionOut.print(sub?.quizoffering?.title)
printHtmlPart(5)
expressionOut.print(sub?.marks)
printHtmlPart(5)
invokeTag('formatDate','g',32,['format':("dd-MM-yyyy"),'date':(sub?.start_date)],-1)
printHtmlPart(5)
invokeTag('formatDate','g',33,['format':("dd-MM-yyyy"),'date':(sub?.end_date)],-1)
printHtmlPart(7)
i++
}
}
printHtmlPart(8)
})
invokeTag('captureBody','sitemesh',38,[:],1)
printHtmlPart(9)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1636605186494L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
