package library
import grails.gorm.MultiTenant
class Library implements MultiTenant<Library>
{
    String name
    String contact
    String address
    String city
    String state
    String country
    String pin
    Boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
        contact nullable:true
        address nullable:true
        city nullable:true
        state nullable:true
        country nullable:true
        pin nullable:true
        organization nullable:true
    }
    static belongsTo=[organization:Organization]
    static mapping = {
        isactive defaultValue: true
    }
}
