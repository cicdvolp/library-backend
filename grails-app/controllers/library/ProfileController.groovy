package library
import grails.converters.JSON
class ProfileController {

    def index() { }
    //Get Data Profile
    def getProfileData(){
        println "getProfileData"
        ProfileService profileService=new ProfileService()
        HashMap hm = new HashMap()
        profileService.getProfileData(request, hm)
        render hm as JSON
    }
    //Save Profile Data And Upload Photo
    def saveProfileData(){
        println "saveProfileData ::"+params
        ProfileService profileService=new ProfileService()
        HashMap hm = new HashMap()
        String ip = request.getRemoteAddr()
        profileService.saveProfileData(request, hm,ip,params)
        render hm as JSON
    }
}
