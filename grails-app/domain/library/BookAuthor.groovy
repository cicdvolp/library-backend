package library

import grails.gorm.MultiTenant

class BooksAuthors implements MultiTenant<BooksAuthors> {

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,book:Book,author:Author]
    static constraints = {
    }
}
