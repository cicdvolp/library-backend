package library
import grails.gorm.MultiTenant
class Author implements MultiTenant<Author>
{
    String name
    String description
    int display_order
    String short_name
    boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static constraints = {
        description nullable:true
        short_name nullable: true
    }
    static mapping = {
        isactive defaultValue: true
        display_order defaultValue: 0
    }
}
