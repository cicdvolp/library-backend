package library
import grails.gorm.MultiTenant
class BookCondition implements MultiTenant<BookCondition>
{
    String name   //New/Old
    boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
}
