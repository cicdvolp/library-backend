package library

import grails.gorm.multitenancy.Tenants
import grails.gorm.transactions.Transactional
import groovy.json.JsonSlurper



import grails.gorm.multitenancy.*
import grails.converters.JSON

import com.opencsv.CSVReader;
import javax.servlet.http.Part
import java.nio.file.Paths
import grails.io.IOUtils

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

///
import grails.gorm.multitenancy.*
import grails.converters.JSON
import groovy.json.JsonSlurper
import grails.gorm.transactions.Transactional

import com.opencsv.CSVReader;
import javax.servlet.http.Part
import java.nio.file.Paths
import grails.io.IOUtils
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.DateFormat;

@Transactional
class ProfileService {

    def serviceMethod() {}

    //Get Member Data
    def getProfileData(request, hm) {
        println("Inside Profile/getProfileData...")
        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        //AuthService auth = new AuthService()
        String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        //def dataresponse = jsonSluper.parseText(body)
        //auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                AWSBucket awsBucket = AWSBucket.findByContent("documents")
                String bucketname = awsBucket.bucketname
                AWSBucketService photoUrl=new AWSBucketService()
                String photo = null
                if (user?.profilephotofilepath != null && user?.profilephotofilename != null) {
                    String awsimagelink = user?.profilephotofilepath + user?.profilephotofilename
                    photo = photoUrl.getPresignedUrl(bucketname, awsimagelink, awsBucket.region)
                    hm.put("profilephotofilename", user?.profilephotofilename)
                } else {
                    hm.put("profilephotofilename", null)
                }
                hm.put("photoPath", photo)
                if (user?.usertype?.name == 'Member') {
                    println("In Member :")
                    Person person = Person.findByUser(user)
                    println("Persone :" + person)
                    Member member = Member.findByUserAndPersonAndOrganization(user, person, org)
                    if(member?.name!=null){
                        hm.put("name", member?.name)
                    }else {
                        hm.put("name", 'NA')
                    }
                    if(person?.mobile_number!=null){
                        hm.put("mobile_number", person?.mobile_number)
                    }else {
                        hm.put("mobile_number",'NA')
                    }
                    if(person?.email!=null){
                        hm.put("email", person?.email)
                    }else {
                        hm.put("email",'NA')
                    }
                    if( person?.address!=null){
                        hm.put("address", person?.address)
                    }else {
                        hm.put("address",'NA')
                    }
                    if(person?.city!=null){
                        hm.put("city", person?.city)
                    }else {
                        hm.put("city",'NA')
                    }
                    if(person?.state!=null){
                        hm.put("state", person?.state)
                    }else {
                        hm.put("state",'NA')
                    }
                    if(person?.country!=null){
                        hm.put("country", person?.country)
                    }else {
                        hm.put("country",'NA')
                    }
                    if(person?.pin!=null){
                        hm.put("pin", person?.pin)
                    }else {
                        hm.put("pin",'NA')
                    }
                } else {
                    println("In Employee")
                    Person person = Person.findByUser(user)
                    Employee employee = Employee.findByUserAndPersonAndOrganization(user, person, org)
                    if(employee?.name!=null){
                        hm.put("name", employee?.name)
                    }else {
                        hm.put("name", 'NA')
                    }
                    if(person?.mobile_number!=null){
                        hm.put("mobile_number", person?.mobile_number)
                    }else {
                        hm.put("mobile_number",'NA')
                    }
                    if(person?.email!=null){
                        hm.put("email", person?.email)
                    }else {
                        hm.put("email",'NA')
                    }
                    if( person?.address!=null){
                        hm.put("address", person?.address)
                    }else {
                        hm.put("address",'NA')
                    }
                    if(person?.city!=null){
                        hm.put("city", person?.city)
                    }else {
                        hm.put("city",'NA')
                    }
                    if(person?.state!=null){
                        hm.put("state", person?.state)
                    }else {
                        hm.put("state",'NA')
                    }
                    if(person?.country!=null){
                        hm.put("country", person?.country)
                    }else {
                        hm.put("country",'NA')
                    }
                    if(person?.pin!=null){
                        hm.put("pin", person?.pin)
                    }else {
                        hm.put("pin",'NA')
                    }
                }

                hm.put("msg", "200")
                return hm
            }
        }
    }
    //Save Profile Data
    def saveProfileData(request, hm,ip,params) {

        def token = request.getHeader("EPS-token")
        def uid = request.getHeader("EPS-uid")
        String url = request.getHeader("router-path")
        AuthService auth = new AuthService()
       /* String body = request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        println("dataresponse ::"+dataresponse)*/
        String purpose = 'Save Profile'
        auth.checkauth(token, uid, hm, url, request, purpose)
        if (hm.get("msg") != "401" && hm.get("msg") != "403") {
            Login log = Login.findByUsername(uid)
            Tenants.withId(log.tenantclient.tenantid) {
                def user = User.findByUsername(log.username)
                def org = user.organization
                AWSBucket awsBucket = AWSBucket.findByContent("documents")
                String bucketname = awsBucket.bucketname
                Person person
                Member member
                Employee employee
                String regId=null
                AWSBucketService photoUrl = new AWSBucketService()
                if (user?.usertype?.name == 'Member') {
                    println("In Member :")
                    person = Person.findByUser(user)
                    if(person!=null){
                        person.mobile_number = params.mobileno
                        person.email = params.email
                        person.address = params.address
                        person.city = params.city
                        person.state = params.state
                        person.country = params.country
                        person.pin = params.pin
                        person.updation_username = uid
                        person.updation_date = new Date()
                        person.updation_ip_address = ip
                        person.save(failOnError: true,flush: true)
                    }
                    member = Member.findByUserAndPersonAndOrganization(user, person, org)
                    regId=member?.registration_number
                    if(member!=null){
                        member.name = params.name
                        member.updation_username = uid
                        member.updation_date = new Date()
                        member.updation_ip_address = ip
                        member.save(failOnError: true,flush: true)
                    }
                } else {
                    println("In Employee")
                    person = Person.findByUser(user)
                    if(person!=null){
                        person.mobile_number = params.mobileno
                        person.email = params.email
                        person.address = params.address
                        person.city = params.city
                        person.state = params.state
                        person.country = params.country
                        person.pin = params.pin
                        person.updation_username = uid
                        person.updation_date = new Date()
                        person.updation_ip_address = ip
                        person.save(failOnError: true,flush: true)
                    }
                    employee = Employee.findByUserAndPersonAndOrganization(user, person, org)
                    regId=employee?.employee_code
                    if(employee!=null){
                        employee.name = params.name
                        employee.updation_username = uid
                        employee.updation_date = new Date()
                        employee.updation_ip_address = ip
                        employee.save(failOnError: true,flush: true)
                    }
                }
                //Photo Upload Code

                Part photofilePart1 = request.getPart('photoFile')
                def filePart1 = request.getFile('photoFile')
                if (filePart1) {
                    println("in aws upload")
                    def folderPath
                    def fileName
                    def filePath
                    InputStream fs = filePart1.getInputStream()
                    String contentType = filePart1.getContentType();
                    fileName = regId+filePart1.originalFilename
                    folderPath = "profilephoto/"+log.tenantclient?.tenantid+"/"
                    filePath = folderPath + fileName
                    AWSUploadDocumentsService awsUploadDocumentsService = new AWSUploadDocumentsService()
                    boolean isUpload = awsUploadDocumentsService.uploadDocument(photofilePart1, folderPath, fileName, filePath)
                        if (isUpload) {
                            user.profilephotofilepath=folderPath
                            user.profilephotofilename=fileName
                            user.save(flush: true, failOnError: true)
                            println(" Profile Photo Uploaded successfully...")

                        } else {
                            println("Error Uploading Profile Photo...")
                        }
                    }
                }
            }
            hm.put("msg", "200")
            return hm
        }
    }



