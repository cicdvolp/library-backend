package library

import grails.converters.JSON

class ReserveBookController {

    def index() { }
    def reserveBookInit()
    {
        println " reserveBookInit in controller"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        adminService.fetchAllUser(request ,hm)
        ReserveBookService reserveBookService = new ReserveBookService()
        String ip = request.getRemoteAddr()
        reserveBookService.reserveBookInit(request, hm,ip)
        render hm as JSON
    }

    def reserveBook()
    {
        println " reserve Book"
        HashMap hm = new HashMap()
        ReserveBookService reserveBookService = new ReserveBookService()
        String ip = request.getRemoteAddr()
        reserveBookService.reserveBook(request, hm,ip)
        render hm as JSON
    }

    def getEmpMemInformation()
    {
        println " get EmpMem Information"
        HashMap hm = new HashMap()
        ReserveBookService reserveBookService = new ReserveBookService()
        String ip = request.getRemoteAddr()
        reserveBookService.getEmpMemInformation1(request, hm)
        render hm as JSON
    }


    def getReserveList()
    {
        println " get Reserve List"
        HashMap hm = new HashMap()
        ReserveBookService reserveBookService = new ReserveBookService()
        String ip = request.getRemoteAddr()
        reserveBookService.getReserveList(request, hm)
        render hm as JSON
    }

    def fetchSingleBookDetails(){
        println " fetchSingleBookDetails in controller"
        AdminService adminService = new AdminService()
        HashMap hm = new HashMap()
        adminService.fetchAllUser(request ,hm)
//        ReserveBookService reserveBookService = new ReserveBookService()
//        String ip = request.getRemoteAddr()
//        hm.put("userlist",User.list())
        //reserveBookService.reserveBookInit(request, hm,ip)
        render hm as JSON
    }
}
